﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>


template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R, typename T1>
struct VirtFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R, typename T1>
struct InterfaceFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};

// System.Collections.Generic.EqualityComparer`1<System.Int32>
struct EqualityComparer_1_t20B8E5927E151143D1CBD8554CAF17F0EAC1CF62;
// System.Collections.Generic.EqualityComparer`1<System.String>
struct EqualityComparer_1_tDC2082D4D5947A0F76D6FA7870E09811B1A8B69E;
// Google.Protobuf.FieldCodec`1<System.Int32>
struct FieldCodec_1_tAB7BF6BD67CE831FB7E8A437894272BD9C511681;
// Google.Protobuf.FieldCodec`1<System.Object>
struct FieldCodec_1_tB53B2BCC80953659808E7925A1937759BD679A5B;
// Google.Protobuf.FieldCodec`1<System.String>
struct FieldCodec_1_t379D692C872A4BD7DE0539465056B8EF49D48FA0;
// System.Func`1<Google.Protobuf.IMessage>
struct Func_1_t44DCED5EAA5278823CED11B2E1EEBC31C8BD7ABB;
// System.Func`1<System.Object>
struct Func_1_t807CEE610086E24A0167BAA97A64062016E09D49;
// System.Func`1<Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange>
struct Func_1_tB4DEFF82E390DE2B16A177BF95E2A660E016FC83;
// System.Func`1<Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation>
struct Func_1_t8A740DFC3D657757DA5C263E40CD639547B59CB4;
// System.Func`1<Google.Protobuf.Reflection.SourceCodeInfo/Types/Location>
struct Func_1_t08ED00E63E70686FA636AFAACFA58BD631D35696;
// System.Func`1<Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart>
struct Func_1_t732F679FB575BFC105666600B1FC8E28B480BBAB;
// System.Func`2<System.Reflection.FieldInfo,System.Boolean>
struct Func_2_t0BE5B54DD65017DAF1DC5DEC5A896A2B0550F8AE;
// System.Func`2<System.Reflection.FieldInfo,System.Object>
struct Func_2_t531FF6D55AC985F40B486650A477C6789A3384E0;
// System.Func`2<System.Reflection.FieldInfo,System.String>
struct Func_2_t3E9169486444D508EC295C4254DF9267CEE35CCE;
// System.Func`2<System.Int32,System.Int32>
struct Func_2_tFF6AE79EFD0857556AD37A1A1594C43F76012FEA;
// System.Func`2<System.String,System.Int32>
struct Func_2_t22FDC42FE24524FB9989A84A6707691EFD7A5AAC;
// System.Collections.Generic.HashSet`1<System.String>
struct HashSet_1_t45F75268054D01D9E70EB33D7F6D2FA609DB9229;
// System.Buffers.IBufferWriter`1<System.Byte>
struct IBufferWriter_1_t63F02C77711682A2CEF308B05BA53D9A7AB05ABD;
// System.Collections.Generic.IDictionary`2<System.Int32,Google.Protobuf.UnknownField>
struct IDictionary_2_t7730999C7CCABA609099AA4F56C9E014422916A8;
// System.Collections.Generic.IDictionary`2<System.String,Google.Protobuf.Reflection.FieldDescriptor>
struct IDictionary_2_t297D05B6B40C37D86A3FA02F46CB18715A710C6D;
// System.Collections.Generic.IEnumerable`1<System.Int32>
struct IEnumerable_1_t60929E1AA80B46746F987B99A4EBD004FD72D370;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t52B1AC8D9E5E1ED28DF6C46A37C9A1B00B394F9D;
// System.Collections.Generic.IEnumerable`1<Google.Protobuf.Reflection.OriginalNameAttribute>
struct IEnumerable_1_tAEB16B1E00EEC3B85E9103B52267A17CB47644DA;
// System.Collections.Generic.IEnumerable`1<System.String>
struct IEnumerable_1_tBD60400523D840591A17E4CBBACC79397F68FAA2;
// System.Collections.Generic.IList`1<Google.Protobuf.Reflection.EnumDescriptor>
struct IList_1_t4FF6746534D0820C8DCB86061DE3282C1CD1DBBC;
// System.Collections.Generic.IList`1<Google.Protobuf.Reflection.FieldDescriptor>
struct IList_1_t418F7AE8DD4F972460934162F27C5FC1B5646D54;
// System.Collections.Generic.IList`1<Google.Protobuf.Reflection.MessageDescriptor>
struct IList_1_tACE6E5172CFB9B21666313779F73E7389A285312;
// System.Collections.Generic.IList`1<Google.Protobuf.Reflection.OneofDescriptor>
struct IList_1_tED967C3667C695EE117ECAAEA9DBCD24CDE286C1;
// Google.Protobuf.FieldCodec`1/InputMerger<System.Int32>
struct InputMerger_tEF5BC15A76618C41C971045DA73971DEDF79E542;
// Google.Protobuf.FieldCodec`1/InputMerger<System.String>
struct InputMerger_t7ED4F32EAAD2118D35203402C77E3EA57AA9A412;
// Google.Protobuf.MessageParser`1<System.Object>
struct MessageParser_1_tA9AC1CA23E91C173440DD761C276E7BE47F12B18;
// Google.Protobuf.MessageParser`1<Google.Protobuf.Reflection.DescriptorProto/Types/ExtensionRange>
struct MessageParser_1_t98C9C5E3B0B505B0A1C5C7E1646762D1C4C24242;
// Google.Protobuf.MessageParser`1<Google.Protobuf.Reflection.DescriptorProto/Types/ReservedRange>
struct MessageParser_1_t38AEDCF2E2057B00B1E8F484B575E1B5709FA070;
// Google.Protobuf.MessageParser`1<Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange>
struct MessageParser_1_tCCF97EFBB4482323562EEB1F40E83233B2D32D55;
// Google.Protobuf.MessageParser`1<Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation>
struct MessageParser_1_t422AF617132E681AA126A8C774D49BC4101614D0;
// Google.Protobuf.MessageParser`1<Google.Protobuf.Reflection.SourceCodeInfo/Types/Location>
struct MessageParser_1_t09C483F589C454D2B080958F963D017315003983;
// Google.Protobuf.MessageParser`1<Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart>
struct MessageParser_1_tE61087F0F9CB9EE7534691A864269E589F1453DF;
// System.Pinnable`1<System.Byte>
struct Pinnable_1_t3FA89DC88CD7499604577377AB0B618C80108110;
// Google.Protobuf.Collections.RepeatedField`1<System.Int32>
struct RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419;
// Google.Protobuf.Collections.RepeatedField`1<System.Object>
struct RepeatedField_1_tD5A1E0061331823102895A0C66DF000A633AB967;
// Google.Protobuf.Collections.RepeatedField`1<System.String>
struct RepeatedField_1_t03A3C968DD0FF896E3F108784B11A7CBD313CDE1;
// Google.Protobuf.ValueReader`1<System.Int32>
struct ValueReader_1_t1E7F93DEF92B86B5F20A5D5389DD2177B12FCD5B;
// Google.Protobuf.ValueReader`1<System.String>
struct ValueReader_1_t154CA09B7811C1E306E80047C70D6E0E951238EA;
// Google.Protobuf.ValueWriter`1<System.Int32>
struct ValueWriter_1_tC41F81E592D60666D75FA6C5DABBD2812B7520EF;
// Google.Protobuf.ValueWriter`1<System.String>
struct ValueWriter_1_tC232F67C3C6E9B72DEEA6BCA79D911579F1A34BB;
// Google.Protobuf.FieldCodec`1/ValuesMerger<System.Int32>
struct ValuesMerger_t16A151956E6723ADA82E83DF32C09D0448E813FB;
// Google.Protobuf.FieldCodec`1/ValuesMerger<System.String>
struct ValuesMerger_t6FF6E3B9F719B0F3E83909292DDFD9401FF9E6AE;
// System.Byte[]
struct ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726;
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Delegate[]
struct DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8;
// System.Int32[]
struct Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32;
// System.String[]
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A;
// System.AsyncCallback
struct AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA;
// Google.Protobuf.CodedInputStream
struct CodedInputStream_tC76F9515605395C11D7F9437E0E06C828232B0C8;
// Google.Protobuf.CodedOutputStream
struct CodedOutputStream_t2871B0D17F920BFB54B564E2295D9753721642E6;
// System.DelegateData
struct DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288;
// Google.Protobuf.Reflection.DescriptorProto
struct DescriptorProto_tB03AF5EFB75071C3E950F726F78461450496C898;
// Google.Protobuf.Reflection.ExtensionCollection
struct ExtensionCollection_t52756747D345D5B934DCD6FF9876823214980772;
// Google.Protobuf.Reflection.ExtensionRangeOptions
struct ExtensionRangeOptions_t497AE48098D601FD2AB661860BDA91704902E99F;
// Google.Protobuf.ExtensionRegistry
struct ExtensionRegistry_tA5BBE047B436970D8BE3DBBE73D5BBFEAFA1CE47;
// System.Reflection.FieldInfo
struct FieldInfo_t;
// Google.Protobuf.Reflection.FileDescriptor
struct FileDescriptor_tA2E229ECE64C897445BBCA0229BD41F8E028ECD4;
// System.IAsyncResult
struct IAsyncResult_tC9F97BF36FCF122D29D3101D80642278297BF370;
// Google.Protobuf.IMessage
struct IMessage_tCB517E43DF18FEE492437790ED05B787439B142A;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// Google.Protobuf.Reflection.MessageDescriptor
struct MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13;
// Google.Protobuf.MessageParser
struct MessageParser_tB0F97A0DCF31AD5D21F69684004C9745C88099FC;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// Google.Protobuf.Reflection.OriginalNameAttribute
struct OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664;
// System.IO.Stream
struct Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB;
// System.String
struct String_t;
// System.Type
struct Type_t;
// Google.Protobuf.UnknownField
struct UnknownField_tA1C0671D4A8CE8D36DE75746AF1940C2923FCFE7;
// Google.Protobuf.UnknownFieldSet
struct UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;
// Google.Protobuf.Reflection.MessageDescriptor/FieldCollection
struct FieldCollection_tF44D33A936EF52997608D546A3213EBE6C88F089;
// Google.Protobuf.Reflection.DescriptorProto/Types/ExtensionRange
struct ExtensionRange_t355AED379D3458AC80443047C2DF72AA44639D84;
// Google.Protobuf.Reflection.DescriptorProto/Types/ReservedRange
struct ReservedRange_tAED8F62AF040F5F133732196696893B313EA458B;
// Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange
struct EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41;
// Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation
struct Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405;
// Google.Protobuf.JsonFormatter/OriginalEnumValueHelper/<>c
struct U3CU3Ec_t5555F991758A0DD73D48BCE4AF9A1B14B7F05FA3;
// Google.Protobuf.Reflection.SourceCodeInfo/Types/Location
struct Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4;
// Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart
struct NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22;
// Google.Protobuf.Reflection.DescriptorProto/Types/ExtensionRange/<>c
struct U3CU3Ec_tC3789B2A2639D39522B7FC88CE44B0FF78387D85;
// Google.Protobuf.Reflection.DescriptorProto/Types/ReservedRange/<>c
struct U3CU3Ec_t14ED7AF2EE0452B68867D51843E5E3FBF40F7D2D;
// Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange/<>c
struct U3CU3Ec_t0FB8D11271B41B39C63CE8A142DE7F8F87630794;
// Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation/<>c
struct U3CU3Ec_t48164166658FE6E4AA4BC81699272A7660D5A345;
// Google.Protobuf.Reflection.SourceCodeInfo/Types/Location/<>c
struct U3CU3Ec_tE6E47C051562B8F8FCE9347E1B99439CF27AB1AF;
// Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart/<>c
struct U3CU3Ec_t70C29F3AE73CD37C0D77DCC96A139645BE59A453;

IL2CPP_EXTERN_C RuntimeClass* Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* CodedOutputStream_t2871B0D17F920BFB54B564E2295D9753721642E6_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* EnumDescriptorProto_t0E60F3628E51BC270FDA740DCBC83DBF04FA0478_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ExtensionRange_t355AED379D3458AC80443047C2DF72AA44639D84_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_1_t08ED00E63E70686FA636AFAACFA58BD631D35696_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_1_t732F679FB575BFC105666600B1FC8E28B480BBAB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_1_t8A740DFC3D657757DA5C263E40CD639547B59CB4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_1_tB4DEFF82E390DE2B16A177BF95E2A660E016FC83_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* GeneratedCodeInfo_t88B2B7880C3E0445510519523DE05618C3B1A315_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IList_1_tACE6E5172CFB9B21666313779F73E7389A285312_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* JsonFormatter_t29EDD2B5D01B4FD88FD4DBF274554818F8ED11C7_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* MessageParser_1_t09C483F589C454D2B080958F963D017315003983_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* MessageParser_1_t422AF617132E681AA126A8C774D49BC4101614D0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* MessageParser_1_tCCF97EFBB4482323562EEB1F40E83233B2D32D55_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* MessageParser_1_tE61087F0F9CB9EE7534691A864269E589F1453DF_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* RepeatedField_1_t03A3C968DD0FF896E3F108784B11A7CBD313CDE1_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ReservedRange_tAED8F62AF040F5F133732196696893B313EA458B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* SourceCodeInfo_tE9110A5CAA36A130E03CC9F3DE6E0EF7E7BF412B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec_t0FB8D11271B41B39C63CE8A142DE7F8F87630794_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec_t14ED7AF2EE0452B68867D51843E5E3FBF40F7D2D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec_t48164166658FE6E4AA4BC81699272A7660D5A345_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec_t5555F991758A0DD73D48BCE4AF9A1B14B7F05FA3_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec_t70C29F3AE73CD37C0D77DCC96A139645BE59A453_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec_tC3789B2A2639D39522B7FC88CE44B0FF78387D85_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec_tE6E47C051562B8F8FCE9347E1B99439CF27AB1AF_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral46F273EF641E07D271D91E0DC24A4392582671F8;
IL2CPP_EXTERN_C String_t* _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
IL2CPP_EXTERN_C const RuntimeMethod* CustomAttributeExtensions_GetCustomAttributes_TisOriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664_mF22DB14B5869097D2C07559341A761D0ADAAF3EB_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerable_FirstOrDefault_TisOriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664_m20BF8C804DDE669AD4A21AB34682851EE714877E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Func_1__ctor_m2DA47B0B49E42E600E186F7701E4C322C935D1D4_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Func_1__ctor_mCCFD409A1FBA40DE4A8770B3B56B638652558C3D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Func_1__ctor_mE88B85D2A25CDAB279765DBE7C9E71B06D0C4C31_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Func_1__ctor_mFFBE264549A7AF17CBECB2E724E4B75F436FD857_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* MessageParser_1__ctor_m1927E72A8975A7E3D3A15FDFB5DCD64879E50B6E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* MessageParser_1__ctor_m58870D9692E7115AE9C0CE22F33EF7950ED7BF19_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* MessageParser_1__ctor_m6B193EF7595551528130D40FD5AF408E99171248_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* MessageParser_1__ctor_mE62242286F7EBCC8F4B3B117434CF74E088C0C26_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ProtoPreconditions_CheckNotNull_TisString_t_m80AE3118869FAEF4DF83AF0D521B284CF725144E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* RepeatedField_1_AddEntriesFrom_m1CD228D26654811414016D368DDCDD8D3AFC85A4_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* RepeatedField_1_AddEntriesFrom_m5AFE58E765285935D3E71F09A425AD7BE2D56674_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* RepeatedField_1_Add_m65F8D4082B764C2A1CC6367692FB566C2A0D75F8_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* RepeatedField_1_Add_m9B3360A97708D5F88CB74E729387922F7CFFF0FE_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* RepeatedField_1_CalculateSize_m67536E773582CAB8A68E5056E9A1D318FAE1942B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* RepeatedField_1_CalculateSize_mBD445305760770A9C733116354E5B30CD5DC2EE3_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* RepeatedField_1_Clone_mCCDDBCC8A031EB0A89878CA4C025F8B396DD9348_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* RepeatedField_1_Clone_mF6A2C055409968D973A30440A0F282A762BE6601_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* RepeatedField_1_Equals_m2D0B705880C1B033423F8A185C0D0DFE9EEDAADA_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* RepeatedField_1_Equals_m4322AAE559CBB034117AE082A33552981014A7AF_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* RepeatedField_1_WriteTo_m2D9D72EB2016DEE58A88C1C8873C2D512C638215_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* RepeatedField_1_WriteTo_mC5C250B204D43F5D814F1C19A1337853828264B8_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* RepeatedField_1__ctor_m28E20B7724ACD650AE80B8E03BCB4A9412BFB63F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* RepeatedField_1__ctor_m397DAD9DD9B6E1993CE6E7CEF796BD2443BA0F44_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec_U3C_cctorU3Eb__41_0_m0FBA9F917C842E30E53B32B2B8E897821F32A8C3_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec_U3C_cctorU3Eb__41_0_m613BA3E40E15DB658425AEB73890F58D554969E4_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec_U3C_cctorU3Eb__55_0_m3FCF05CD6B82754BB25E87E1A3A8225796468906_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec_U3C_cctorU3Eb__55_0_m4EB0DCECA4202B15C7EC20815CEFDEBD1E0AFB9B_RuntimeMethod_var;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// Google.Protobuf.FieldCodec`1<System.Int32>
struct FieldCodec_1_tAB7BF6BD67CE831FB7E8A437894272BD9C511681  : public RuntimeObject
{
public:
	// System.Boolean Google.Protobuf.FieldCodec`1::<PackedRepeatedField>k__BackingField
	bool ___U3CPackedRepeatedFieldU3Ek__BackingField_3;
	// Google.Protobuf.ValueWriter`1<T> Google.Protobuf.FieldCodec`1::<ValueWriter>k__BackingField
	ValueWriter_1_tC41F81E592D60666D75FA6C5DABBD2812B7520EF * ___U3CValueWriterU3Ek__BackingField_4;
	// System.Func`2<T,System.Int32> Google.Protobuf.FieldCodec`1::<ValueSizeCalculator>k__BackingField
	Func_2_tFF6AE79EFD0857556AD37A1A1594C43F76012FEA * ___U3CValueSizeCalculatorU3Ek__BackingField_5;
	// Google.Protobuf.ValueReader`1<T> Google.Protobuf.FieldCodec`1::<ValueReader>k__BackingField
	ValueReader_1_t1E7F93DEF92B86B5F20A5D5389DD2177B12FCD5B * ___U3CValueReaderU3Ek__BackingField_6;
	// Google.Protobuf.FieldCodec`1/InputMerger<T> Google.Protobuf.FieldCodec`1::<ValueMerger>k__BackingField
	InputMerger_tEF5BC15A76618C41C971045DA73971DEDF79E542 * ___U3CValueMergerU3Ek__BackingField_7;
	// Google.Protobuf.FieldCodec`1/ValuesMerger<T> Google.Protobuf.FieldCodec`1::<FieldMerger>k__BackingField
	ValuesMerger_t16A151956E6723ADA82E83DF32C09D0448E813FB * ___U3CFieldMergerU3Ek__BackingField_8;
	// System.Int32 Google.Protobuf.FieldCodec`1::<FixedSize>k__BackingField
	int32_t ___U3CFixedSizeU3Ek__BackingField_9;
	// System.UInt32 Google.Protobuf.FieldCodec`1::<Tag>k__BackingField
	uint32_t ___U3CTagU3Ek__BackingField_10;
	// System.UInt32 Google.Protobuf.FieldCodec`1::<EndTag>k__BackingField
	uint32_t ___U3CEndTagU3Ek__BackingField_11;
	// T Google.Protobuf.FieldCodec`1::<DefaultValue>k__BackingField
	int32_t ___U3CDefaultValueU3Ek__BackingField_12;
	// System.Int32 Google.Protobuf.FieldCodec`1::tagSize
	int32_t ___tagSize_13;

public:
	inline static int32_t get_offset_of_U3CPackedRepeatedFieldU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(FieldCodec_1_tAB7BF6BD67CE831FB7E8A437894272BD9C511681, ___U3CPackedRepeatedFieldU3Ek__BackingField_3)); }
	inline bool get_U3CPackedRepeatedFieldU3Ek__BackingField_3() const { return ___U3CPackedRepeatedFieldU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CPackedRepeatedFieldU3Ek__BackingField_3() { return &___U3CPackedRepeatedFieldU3Ek__BackingField_3; }
	inline void set_U3CPackedRepeatedFieldU3Ek__BackingField_3(bool value)
	{
		___U3CPackedRepeatedFieldU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CValueWriterU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(FieldCodec_1_tAB7BF6BD67CE831FB7E8A437894272BD9C511681, ___U3CValueWriterU3Ek__BackingField_4)); }
	inline ValueWriter_1_tC41F81E592D60666D75FA6C5DABBD2812B7520EF * get_U3CValueWriterU3Ek__BackingField_4() const { return ___U3CValueWriterU3Ek__BackingField_4; }
	inline ValueWriter_1_tC41F81E592D60666D75FA6C5DABBD2812B7520EF ** get_address_of_U3CValueWriterU3Ek__BackingField_4() { return &___U3CValueWriterU3Ek__BackingField_4; }
	inline void set_U3CValueWriterU3Ek__BackingField_4(ValueWriter_1_tC41F81E592D60666D75FA6C5DABBD2812B7520EF * value)
	{
		___U3CValueWriterU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CValueWriterU3Ek__BackingField_4), (void*)value);
	}

	inline static int32_t get_offset_of_U3CValueSizeCalculatorU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(FieldCodec_1_tAB7BF6BD67CE831FB7E8A437894272BD9C511681, ___U3CValueSizeCalculatorU3Ek__BackingField_5)); }
	inline Func_2_tFF6AE79EFD0857556AD37A1A1594C43F76012FEA * get_U3CValueSizeCalculatorU3Ek__BackingField_5() const { return ___U3CValueSizeCalculatorU3Ek__BackingField_5; }
	inline Func_2_tFF6AE79EFD0857556AD37A1A1594C43F76012FEA ** get_address_of_U3CValueSizeCalculatorU3Ek__BackingField_5() { return &___U3CValueSizeCalculatorU3Ek__BackingField_5; }
	inline void set_U3CValueSizeCalculatorU3Ek__BackingField_5(Func_2_tFF6AE79EFD0857556AD37A1A1594C43F76012FEA * value)
	{
		___U3CValueSizeCalculatorU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CValueSizeCalculatorU3Ek__BackingField_5), (void*)value);
	}

	inline static int32_t get_offset_of_U3CValueReaderU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(FieldCodec_1_tAB7BF6BD67CE831FB7E8A437894272BD9C511681, ___U3CValueReaderU3Ek__BackingField_6)); }
	inline ValueReader_1_t1E7F93DEF92B86B5F20A5D5389DD2177B12FCD5B * get_U3CValueReaderU3Ek__BackingField_6() const { return ___U3CValueReaderU3Ek__BackingField_6; }
	inline ValueReader_1_t1E7F93DEF92B86B5F20A5D5389DD2177B12FCD5B ** get_address_of_U3CValueReaderU3Ek__BackingField_6() { return &___U3CValueReaderU3Ek__BackingField_6; }
	inline void set_U3CValueReaderU3Ek__BackingField_6(ValueReader_1_t1E7F93DEF92B86B5F20A5D5389DD2177B12FCD5B * value)
	{
		___U3CValueReaderU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CValueReaderU3Ek__BackingField_6), (void*)value);
	}

	inline static int32_t get_offset_of_U3CValueMergerU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(FieldCodec_1_tAB7BF6BD67CE831FB7E8A437894272BD9C511681, ___U3CValueMergerU3Ek__BackingField_7)); }
	inline InputMerger_tEF5BC15A76618C41C971045DA73971DEDF79E542 * get_U3CValueMergerU3Ek__BackingField_7() const { return ___U3CValueMergerU3Ek__BackingField_7; }
	inline InputMerger_tEF5BC15A76618C41C971045DA73971DEDF79E542 ** get_address_of_U3CValueMergerU3Ek__BackingField_7() { return &___U3CValueMergerU3Ek__BackingField_7; }
	inline void set_U3CValueMergerU3Ek__BackingField_7(InputMerger_tEF5BC15A76618C41C971045DA73971DEDF79E542 * value)
	{
		___U3CValueMergerU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CValueMergerU3Ek__BackingField_7), (void*)value);
	}

	inline static int32_t get_offset_of_U3CFieldMergerU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(FieldCodec_1_tAB7BF6BD67CE831FB7E8A437894272BD9C511681, ___U3CFieldMergerU3Ek__BackingField_8)); }
	inline ValuesMerger_t16A151956E6723ADA82E83DF32C09D0448E813FB * get_U3CFieldMergerU3Ek__BackingField_8() const { return ___U3CFieldMergerU3Ek__BackingField_8; }
	inline ValuesMerger_t16A151956E6723ADA82E83DF32C09D0448E813FB ** get_address_of_U3CFieldMergerU3Ek__BackingField_8() { return &___U3CFieldMergerU3Ek__BackingField_8; }
	inline void set_U3CFieldMergerU3Ek__BackingField_8(ValuesMerger_t16A151956E6723ADA82E83DF32C09D0448E813FB * value)
	{
		___U3CFieldMergerU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CFieldMergerU3Ek__BackingField_8), (void*)value);
	}

	inline static int32_t get_offset_of_U3CFixedSizeU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(FieldCodec_1_tAB7BF6BD67CE831FB7E8A437894272BD9C511681, ___U3CFixedSizeU3Ek__BackingField_9)); }
	inline int32_t get_U3CFixedSizeU3Ek__BackingField_9() const { return ___U3CFixedSizeU3Ek__BackingField_9; }
	inline int32_t* get_address_of_U3CFixedSizeU3Ek__BackingField_9() { return &___U3CFixedSizeU3Ek__BackingField_9; }
	inline void set_U3CFixedSizeU3Ek__BackingField_9(int32_t value)
	{
		___U3CFixedSizeU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_U3CTagU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(FieldCodec_1_tAB7BF6BD67CE831FB7E8A437894272BD9C511681, ___U3CTagU3Ek__BackingField_10)); }
	inline uint32_t get_U3CTagU3Ek__BackingField_10() const { return ___U3CTagU3Ek__BackingField_10; }
	inline uint32_t* get_address_of_U3CTagU3Ek__BackingField_10() { return &___U3CTagU3Ek__BackingField_10; }
	inline void set_U3CTagU3Ek__BackingField_10(uint32_t value)
	{
		___U3CTagU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CEndTagU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(FieldCodec_1_tAB7BF6BD67CE831FB7E8A437894272BD9C511681, ___U3CEndTagU3Ek__BackingField_11)); }
	inline uint32_t get_U3CEndTagU3Ek__BackingField_11() const { return ___U3CEndTagU3Ek__BackingField_11; }
	inline uint32_t* get_address_of_U3CEndTagU3Ek__BackingField_11() { return &___U3CEndTagU3Ek__BackingField_11; }
	inline void set_U3CEndTagU3Ek__BackingField_11(uint32_t value)
	{
		___U3CEndTagU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CDefaultValueU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(FieldCodec_1_tAB7BF6BD67CE831FB7E8A437894272BD9C511681, ___U3CDefaultValueU3Ek__BackingField_12)); }
	inline int32_t get_U3CDefaultValueU3Ek__BackingField_12() const { return ___U3CDefaultValueU3Ek__BackingField_12; }
	inline int32_t* get_address_of_U3CDefaultValueU3Ek__BackingField_12() { return &___U3CDefaultValueU3Ek__BackingField_12; }
	inline void set_U3CDefaultValueU3Ek__BackingField_12(int32_t value)
	{
		___U3CDefaultValueU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_tagSize_13() { return static_cast<int32_t>(offsetof(FieldCodec_1_tAB7BF6BD67CE831FB7E8A437894272BD9C511681, ___tagSize_13)); }
	inline int32_t get_tagSize_13() const { return ___tagSize_13; }
	inline int32_t* get_address_of_tagSize_13() { return &___tagSize_13; }
	inline void set_tagSize_13(int32_t value)
	{
		___tagSize_13 = value;
	}
};

struct FieldCodec_1_tAB7BF6BD67CE831FB7E8A437894272BD9C511681_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> Google.Protobuf.FieldCodec`1::EqualityComparer
	EqualityComparer_1_t20B8E5927E151143D1CBD8554CAF17F0EAC1CF62 * ___EqualityComparer_0;
	// T Google.Protobuf.FieldCodec`1::DefaultDefault
	int32_t ___DefaultDefault_1;
	// System.Boolean Google.Protobuf.FieldCodec`1::TypeSupportsPacking
	bool ___TypeSupportsPacking_2;

public:
	inline static int32_t get_offset_of_EqualityComparer_0() { return static_cast<int32_t>(offsetof(FieldCodec_1_tAB7BF6BD67CE831FB7E8A437894272BD9C511681_StaticFields, ___EqualityComparer_0)); }
	inline EqualityComparer_1_t20B8E5927E151143D1CBD8554CAF17F0EAC1CF62 * get_EqualityComparer_0() const { return ___EqualityComparer_0; }
	inline EqualityComparer_1_t20B8E5927E151143D1CBD8554CAF17F0EAC1CF62 ** get_address_of_EqualityComparer_0() { return &___EqualityComparer_0; }
	inline void set_EqualityComparer_0(EqualityComparer_1_t20B8E5927E151143D1CBD8554CAF17F0EAC1CF62 * value)
	{
		___EqualityComparer_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EqualityComparer_0), (void*)value);
	}

	inline static int32_t get_offset_of_DefaultDefault_1() { return static_cast<int32_t>(offsetof(FieldCodec_1_tAB7BF6BD67CE831FB7E8A437894272BD9C511681_StaticFields, ___DefaultDefault_1)); }
	inline int32_t get_DefaultDefault_1() const { return ___DefaultDefault_1; }
	inline int32_t* get_address_of_DefaultDefault_1() { return &___DefaultDefault_1; }
	inline void set_DefaultDefault_1(int32_t value)
	{
		___DefaultDefault_1 = value;
	}

	inline static int32_t get_offset_of_TypeSupportsPacking_2() { return static_cast<int32_t>(offsetof(FieldCodec_1_tAB7BF6BD67CE831FB7E8A437894272BD9C511681_StaticFields, ___TypeSupportsPacking_2)); }
	inline bool get_TypeSupportsPacking_2() const { return ___TypeSupportsPacking_2; }
	inline bool* get_address_of_TypeSupportsPacking_2() { return &___TypeSupportsPacking_2; }
	inline void set_TypeSupportsPacking_2(bool value)
	{
		___TypeSupportsPacking_2 = value;
	}
};


// Google.Protobuf.FieldCodec`1<System.String>
struct FieldCodec_1_t379D692C872A4BD7DE0539465056B8EF49D48FA0  : public RuntimeObject
{
public:
	// System.Boolean Google.Protobuf.FieldCodec`1::<PackedRepeatedField>k__BackingField
	bool ___U3CPackedRepeatedFieldU3Ek__BackingField_3;
	// Google.Protobuf.ValueWriter`1<T> Google.Protobuf.FieldCodec`1::<ValueWriter>k__BackingField
	ValueWriter_1_tC232F67C3C6E9B72DEEA6BCA79D911579F1A34BB * ___U3CValueWriterU3Ek__BackingField_4;
	// System.Func`2<T,System.Int32> Google.Protobuf.FieldCodec`1::<ValueSizeCalculator>k__BackingField
	Func_2_t22FDC42FE24524FB9989A84A6707691EFD7A5AAC * ___U3CValueSizeCalculatorU3Ek__BackingField_5;
	// Google.Protobuf.ValueReader`1<T> Google.Protobuf.FieldCodec`1::<ValueReader>k__BackingField
	ValueReader_1_t154CA09B7811C1E306E80047C70D6E0E951238EA * ___U3CValueReaderU3Ek__BackingField_6;
	// Google.Protobuf.FieldCodec`1/InputMerger<T> Google.Protobuf.FieldCodec`1::<ValueMerger>k__BackingField
	InputMerger_t7ED4F32EAAD2118D35203402C77E3EA57AA9A412 * ___U3CValueMergerU3Ek__BackingField_7;
	// Google.Protobuf.FieldCodec`1/ValuesMerger<T> Google.Protobuf.FieldCodec`1::<FieldMerger>k__BackingField
	ValuesMerger_t6FF6E3B9F719B0F3E83909292DDFD9401FF9E6AE * ___U3CFieldMergerU3Ek__BackingField_8;
	// System.Int32 Google.Protobuf.FieldCodec`1::<FixedSize>k__BackingField
	int32_t ___U3CFixedSizeU3Ek__BackingField_9;
	// System.UInt32 Google.Protobuf.FieldCodec`1::<Tag>k__BackingField
	uint32_t ___U3CTagU3Ek__BackingField_10;
	// System.UInt32 Google.Protobuf.FieldCodec`1::<EndTag>k__BackingField
	uint32_t ___U3CEndTagU3Ek__BackingField_11;
	// T Google.Protobuf.FieldCodec`1::<DefaultValue>k__BackingField
	String_t* ___U3CDefaultValueU3Ek__BackingField_12;
	// System.Int32 Google.Protobuf.FieldCodec`1::tagSize
	int32_t ___tagSize_13;

public:
	inline static int32_t get_offset_of_U3CPackedRepeatedFieldU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(FieldCodec_1_t379D692C872A4BD7DE0539465056B8EF49D48FA0, ___U3CPackedRepeatedFieldU3Ek__BackingField_3)); }
	inline bool get_U3CPackedRepeatedFieldU3Ek__BackingField_3() const { return ___U3CPackedRepeatedFieldU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CPackedRepeatedFieldU3Ek__BackingField_3() { return &___U3CPackedRepeatedFieldU3Ek__BackingField_3; }
	inline void set_U3CPackedRepeatedFieldU3Ek__BackingField_3(bool value)
	{
		___U3CPackedRepeatedFieldU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CValueWriterU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(FieldCodec_1_t379D692C872A4BD7DE0539465056B8EF49D48FA0, ___U3CValueWriterU3Ek__BackingField_4)); }
	inline ValueWriter_1_tC232F67C3C6E9B72DEEA6BCA79D911579F1A34BB * get_U3CValueWriterU3Ek__BackingField_4() const { return ___U3CValueWriterU3Ek__BackingField_4; }
	inline ValueWriter_1_tC232F67C3C6E9B72DEEA6BCA79D911579F1A34BB ** get_address_of_U3CValueWriterU3Ek__BackingField_4() { return &___U3CValueWriterU3Ek__BackingField_4; }
	inline void set_U3CValueWriterU3Ek__BackingField_4(ValueWriter_1_tC232F67C3C6E9B72DEEA6BCA79D911579F1A34BB * value)
	{
		___U3CValueWriterU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CValueWriterU3Ek__BackingField_4), (void*)value);
	}

	inline static int32_t get_offset_of_U3CValueSizeCalculatorU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(FieldCodec_1_t379D692C872A4BD7DE0539465056B8EF49D48FA0, ___U3CValueSizeCalculatorU3Ek__BackingField_5)); }
	inline Func_2_t22FDC42FE24524FB9989A84A6707691EFD7A5AAC * get_U3CValueSizeCalculatorU3Ek__BackingField_5() const { return ___U3CValueSizeCalculatorU3Ek__BackingField_5; }
	inline Func_2_t22FDC42FE24524FB9989A84A6707691EFD7A5AAC ** get_address_of_U3CValueSizeCalculatorU3Ek__BackingField_5() { return &___U3CValueSizeCalculatorU3Ek__BackingField_5; }
	inline void set_U3CValueSizeCalculatorU3Ek__BackingField_5(Func_2_t22FDC42FE24524FB9989A84A6707691EFD7A5AAC * value)
	{
		___U3CValueSizeCalculatorU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CValueSizeCalculatorU3Ek__BackingField_5), (void*)value);
	}

	inline static int32_t get_offset_of_U3CValueReaderU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(FieldCodec_1_t379D692C872A4BD7DE0539465056B8EF49D48FA0, ___U3CValueReaderU3Ek__BackingField_6)); }
	inline ValueReader_1_t154CA09B7811C1E306E80047C70D6E0E951238EA * get_U3CValueReaderU3Ek__BackingField_6() const { return ___U3CValueReaderU3Ek__BackingField_6; }
	inline ValueReader_1_t154CA09B7811C1E306E80047C70D6E0E951238EA ** get_address_of_U3CValueReaderU3Ek__BackingField_6() { return &___U3CValueReaderU3Ek__BackingField_6; }
	inline void set_U3CValueReaderU3Ek__BackingField_6(ValueReader_1_t154CA09B7811C1E306E80047C70D6E0E951238EA * value)
	{
		___U3CValueReaderU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CValueReaderU3Ek__BackingField_6), (void*)value);
	}

	inline static int32_t get_offset_of_U3CValueMergerU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(FieldCodec_1_t379D692C872A4BD7DE0539465056B8EF49D48FA0, ___U3CValueMergerU3Ek__BackingField_7)); }
	inline InputMerger_t7ED4F32EAAD2118D35203402C77E3EA57AA9A412 * get_U3CValueMergerU3Ek__BackingField_7() const { return ___U3CValueMergerU3Ek__BackingField_7; }
	inline InputMerger_t7ED4F32EAAD2118D35203402C77E3EA57AA9A412 ** get_address_of_U3CValueMergerU3Ek__BackingField_7() { return &___U3CValueMergerU3Ek__BackingField_7; }
	inline void set_U3CValueMergerU3Ek__BackingField_7(InputMerger_t7ED4F32EAAD2118D35203402C77E3EA57AA9A412 * value)
	{
		___U3CValueMergerU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CValueMergerU3Ek__BackingField_7), (void*)value);
	}

	inline static int32_t get_offset_of_U3CFieldMergerU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(FieldCodec_1_t379D692C872A4BD7DE0539465056B8EF49D48FA0, ___U3CFieldMergerU3Ek__BackingField_8)); }
	inline ValuesMerger_t6FF6E3B9F719B0F3E83909292DDFD9401FF9E6AE * get_U3CFieldMergerU3Ek__BackingField_8() const { return ___U3CFieldMergerU3Ek__BackingField_8; }
	inline ValuesMerger_t6FF6E3B9F719B0F3E83909292DDFD9401FF9E6AE ** get_address_of_U3CFieldMergerU3Ek__BackingField_8() { return &___U3CFieldMergerU3Ek__BackingField_8; }
	inline void set_U3CFieldMergerU3Ek__BackingField_8(ValuesMerger_t6FF6E3B9F719B0F3E83909292DDFD9401FF9E6AE * value)
	{
		___U3CFieldMergerU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CFieldMergerU3Ek__BackingField_8), (void*)value);
	}

	inline static int32_t get_offset_of_U3CFixedSizeU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(FieldCodec_1_t379D692C872A4BD7DE0539465056B8EF49D48FA0, ___U3CFixedSizeU3Ek__BackingField_9)); }
	inline int32_t get_U3CFixedSizeU3Ek__BackingField_9() const { return ___U3CFixedSizeU3Ek__BackingField_9; }
	inline int32_t* get_address_of_U3CFixedSizeU3Ek__BackingField_9() { return &___U3CFixedSizeU3Ek__BackingField_9; }
	inline void set_U3CFixedSizeU3Ek__BackingField_9(int32_t value)
	{
		___U3CFixedSizeU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_U3CTagU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(FieldCodec_1_t379D692C872A4BD7DE0539465056B8EF49D48FA0, ___U3CTagU3Ek__BackingField_10)); }
	inline uint32_t get_U3CTagU3Ek__BackingField_10() const { return ___U3CTagU3Ek__BackingField_10; }
	inline uint32_t* get_address_of_U3CTagU3Ek__BackingField_10() { return &___U3CTagU3Ek__BackingField_10; }
	inline void set_U3CTagU3Ek__BackingField_10(uint32_t value)
	{
		___U3CTagU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CEndTagU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(FieldCodec_1_t379D692C872A4BD7DE0539465056B8EF49D48FA0, ___U3CEndTagU3Ek__BackingField_11)); }
	inline uint32_t get_U3CEndTagU3Ek__BackingField_11() const { return ___U3CEndTagU3Ek__BackingField_11; }
	inline uint32_t* get_address_of_U3CEndTagU3Ek__BackingField_11() { return &___U3CEndTagU3Ek__BackingField_11; }
	inline void set_U3CEndTagU3Ek__BackingField_11(uint32_t value)
	{
		___U3CEndTagU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CDefaultValueU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(FieldCodec_1_t379D692C872A4BD7DE0539465056B8EF49D48FA0, ___U3CDefaultValueU3Ek__BackingField_12)); }
	inline String_t* get_U3CDefaultValueU3Ek__BackingField_12() const { return ___U3CDefaultValueU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CDefaultValueU3Ek__BackingField_12() { return &___U3CDefaultValueU3Ek__BackingField_12; }
	inline void set_U3CDefaultValueU3Ek__BackingField_12(String_t* value)
	{
		___U3CDefaultValueU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CDefaultValueU3Ek__BackingField_12), (void*)value);
	}

	inline static int32_t get_offset_of_tagSize_13() { return static_cast<int32_t>(offsetof(FieldCodec_1_t379D692C872A4BD7DE0539465056B8EF49D48FA0, ___tagSize_13)); }
	inline int32_t get_tagSize_13() const { return ___tagSize_13; }
	inline int32_t* get_address_of_tagSize_13() { return &___tagSize_13; }
	inline void set_tagSize_13(int32_t value)
	{
		___tagSize_13 = value;
	}
};

struct FieldCodec_1_t379D692C872A4BD7DE0539465056B8EF49D48FA0_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> Google.Protobuf.FieldCodec`1::EqualityComparer
	EqualityComparer_1_tDC2082D4D5947A0F76D6FA7870E09811B1A8B69E * ___EqualityComparer_0;
	// T Google.Protobuf.FieldCodec`1::DefaultDefault
	String_t* ___DefaultDefault_1;
	// System.Boolean Google.Protobuf.FieldCodec`1::TypeSupportsPacking
	bool ___TypeSupportsPacking_2;

public:
	inline static int32_t get_offset_of_EqualityComparer_0() { return static_cast<int32_t>(offsetof(FieldCodec_1_t379D692C872A4BD7DE0539465056B8EF49D48FA0_StaticFields, ___EqualityComparer_0)); }
	inline EqualityComparer_1_tDC2082D4D5947A0F76D6FA7870E09811B1A8B69E * get_EqualityComparer_0() const { return ___EqualityComparer_0; }
	inline EqualityComparer_1_tDC2082D4D5947A0F76D6FA7870E09811B1A8B69E ** get_address_of_EqualityComparer_0() { return &___EqualityComparer_0; }
	inline void set_EqualityComparer_0(EqualityComparer_1_tDC2082D4D5947A0F76D6FA7870E09811B1A8B69E * value)
	{
		___EqualityComparer_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EqualityComparer_0), (void*)value);
	}

	inline static int32_t get_offset_of_DefaultDefault_1() { return static_cast<int32_t>(offsetof(FieldCodec_1_t379D692C872A4BD7DE0539465056B8EF49D48FA0_StaticFields, ___DefaultDefault_1)); }
	inline String_t* get_DefaultDefault_1() const { return ___DefaultDefault_1; }
	inline String_t** get_address_of_DefaultDefault_1() { return &___DefaultDefault_1; }
	inline void set_DefaultDefault_1(String_t* value)
	{
		___DefaultDefault_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DefaultDefault_1), (void*)value);
	}

	inline static int32_t get_offset_of_TypeSupportsPacking_2() { return static_cast<int32_t>(offsetof(FieldCodec_1_t379D692C872A4BD7DE0539465056B8EF49D48FA0_StaticFields, ___TypeSupportsPacking_2)); }
	inline bool get_TypeSupportsPacking_2() const { return ___TypeSupportsPacking_2; }
	inline bool* get_address_of_TypeSupportsPacking_2() { return &___TypeSupportsPacking_2; }
	inline void set_TypeSupportsPacking_2(bool value)
	{
		___TypeSupportsPacking_2 = value;
	}
};


// Google.Protobuf.Collections.RepeatedField`1<System.Int32>
struct RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419  : public RuntimeObject
{
public:
	// T[] Google.Protobuf.Collections.RepeatedField`1::array
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___array_3;
	// System.Int32 Google.Protobuf.Collections.RepeatedField`1::count
	int32_t ___count_4;

public:
	inline static int32_t get_offset_of_array_3() { return static_cast<int32_t>(offsetof(RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419, ___array_3)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_array_3() const { return ___array_3; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_array_3() { return &___array_3; }
	inline void set_array_3(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___array_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___array_3), (void*)value);
	}

	inline static int32_t get_offset_of_count_4() { return static_cast<int32_t>(offsetof(RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419, ___count_4)); }
	inline int32_t get_count_4() const { return ___count_4; }
	inline int32_t* get_address_of_count_4() { return &___count_4; }
	inline void set_count_4(int32_t value)
	{
		___count_4 = value;
	}
};

struct RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> Google.Protobuf.Collections.RepeatedField`1::EqualityComparer
	EqualityComparer_1_t20B8E5927E151143D1CBD8554CAF17F0EAC1CF62 * ___EqualityComparer_0;
	// T[] Google.Protobuf.Collections.RepeatedField`1::EmptyArray
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___EmptyArray_1;

public:
	inline static int32_t get_offset_of_EqualityComparer_0() { return static_cast<int32_t>(offsetof(RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419_StaticFields, ___EqualityComparer_0)); }
	inline EqualityComparer_1_t20B8E5927E151143D1CBD8554CAF17F0EAC1CF62 * get_EqualityComparer_0() const { return ___EqualityComparer_0; }
	inline EqualityComparer_1_t20B8E5927E151143D1CBD8554CAF17F0EAC1CF62 ** get_address_of_EqualityComparer_0() { return &___EqualityComparer_0; }
	inline void set_EqualityComparer_0(EqualityComparer_1_t20B8E5927E151143D1CBD8554CAF17F0EAC1CF62 * value)
	{
		___EqualityComparer_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EqualityComparer_0), (void*)value);
	}

	inline static int32_t get_offset_of_EmptyArray_1() { return static_cast<int32_t>(offsetof(RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419_StaticFields, ___EmptyArray_1)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_EmptyArray_1() const { return ___EmptyArray_1; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_EmptyArray_1() { return &___EmptyArray_1; }
	inline void set_EmptyArray_1(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___EmptyArray_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyArray_1), (void*)value);
	}
};


// Google.Protobuf.Collections.RepeatedField`1<System.String>
struct RepeatedField_1_t03A3C968DD0FF896E3F108784B11A7CBD313CDE1  : public RuntimeObject
{
public:
	// T[] Google.Protobuf.Collections.RepeatedField`1::array
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___array_3;
	// System.Int32 Google.Protobuf.Collections.RepeatedField`1::count
	int32_t ___count_4;

public:
	inline static int32_t get_offset_of_array_3() { return static_cast<int32_t>(offsetof(RepeatedField_1_t03A3C968DD0FF896E3F108784B11A7CBD313CDE1, ___array_3)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_array_3() const { return ___array_3; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_array_3() { return &___array_3; }
	inline void set_array_3(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___array_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___array_3), (void*)value);
	}

	inline static int32_t get_offset_of_count_4() { return static_cast<int32_t>(offsetof(RepeatedField_1_t03A3C968DD0FF896E3F108784B11A7CBD313CDE1, ___count_4)); }
	inline int32_t get_count_4() const { return ___count_4; }
	inline int32_t* get_address_of_count_4() { return &___count_4; }
	inline void set_count_4(int32_t value)
	{
		___count_4 = value;
	}
};

struct RepeatedField_1_t03A3C968DD0FF896E3F108784B11A7CBD313CDE1_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> Google.Protobuf.Collections.RepeatedField`1::EqualityComparer
	EqualityComparer_1_tDC2082D4D5947A0F76D6FA7870E09811B1A8B69E * ___EqualityComparer_0;
	// T[] Google.Protobuf.Collections.RepeatedField`1::EmptyArray
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___EmptyArray_1;

public:
	inline static int32_t get_offset_of_EqualityComparer_0() { return static_cast<int32_t>(offsetof(RepeatedField_1_t03A3C968DD0FF896E3F108784B11A7CBD313CDE1_StaticFields, ___EqualityComparer_0)); }
	inline EqualityComparer_1_tDC2082D4D5947A0F76D6FA7870E09811B1A8B69E * get_EqualityComparer_0() const { return ___EqualityComparer_0; }
	inline EqualityComparer_1_tDC2082D4D5947A0F76D6FA7870E09811B1A8B69E ** get_address_of_EqualityComparer_0() { return &___EqualityComparer_0; }
	inline void set_EqualityComparer_0(EqualityComparer_1_tDC2082D4D5947A0F76D6FA7870E09811B1A8B69E * value)
	{
		___EqualityComparer_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EqualityComparer_0), (void*)value);
	}

	inline static int32_t get_offset_of_EmptyArray_1() { return static_cast<int32_t>(offsetof(RepeatedField_1_t03A3C968DD0FF896E3F108784B11A7CBD313CDE1_StaticFields, ___EmptyArray_1)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_EmptyArray_1() const { return ___EmptyArray_1; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_EmptyArray_1() { return &___EmptyArray_1; }
	inline void set_EmptyArray_1(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___EmptyArray_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyArray_1), (void*)value);
	}
};

struct Il2CppArrayBounds;

// System.Array


// System.Attribute
struct Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// Google.Protobuf.Reflection.DescriptorBase
struct DescriptorBase_tF6C2BA893A9882D75C7CD171CB21EBA88FA82CEA  : public RuntimeObject
{
public:
	// System.Int32 Google.Protobuf.Reflection.DescriptorBase::<Index>k__BackingField
	int32_t ___U3CIndexU3Ek__BackingField_0;
	// System.String Google.Protobuf.Reflection.DescriptorBase::<FullName>k__BackingField
	String_t* ___U3CFullNameU3Ek__BackingField_1;
	// Google.Protobuf.Reflection.FileDescriptor Google.Protobuf.Reflection.DescriptorBase::<File>k__BackingField
	FileDescriptor_tA2E229ECE64C897445BBCA0229BD41F8E028ECD4 * ___U3CFileU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CIndexU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(DescriptorBase_tF6C2BA893A9882D75C7CD171CB21EBA88FA82CEA, ___U3CIndexU3Ek__BackingField_0)); }
	inline int32_t get_U3CIndexU3Ek__BackingField_0() const { return ___U3CIndexU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CIndexU3Ek__BackingField_0() { return &___U3CIndexU3Ek__BackingField_0; }
	inline void set_U3CIndexU3Ek__BackingField_0(int32_t value)
	{
		___U3CIndexU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CFullNameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(DescriptorBase_tF6C2BA893A9882D75C7CD171CB21EBA88FA82CEA, ___U3CFullNameU3Ek__BackingField_1)); }
	inline String_t* get_U3CFullNameU3Ek__BackingField_1() const { return ___U3CFullNameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CFullNameU3Ek__BackingField_1() { return &___U3CFullNameU3Ek__BackingField_1; }
	inline void set_U3CFullNameU3Ek__BackingField_1(String_t* value)
	{
		___U3CFullNameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CFullNameU3Ek__BackingField_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CFileU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(DescriptorBase_tF6C2BA893A9882D75C7CD171CB21EBA88FA82CEA, ___U3CFileU3Ek__BackingField_2)); }
	inline FileDescriptor_tA2E229ECE64C897445BBCA0229BD41F8E028ECD4 * get_U3CFileU3Ek__BackingField_2() const { return ___U3CFileU3Ek__BackingField_2; }
	inline FileDescriptor_tA2E229ECE64C897445BBCA0229BD41F8E028ECD4 ** get_address_of_U3CFileU3Ek__BackingField_2() { return &___U3CFileU3Ek__BackingField_2; }
	inline void set_U3CFileU3Ek__BackingField_2(FileDescriptor_tA2E229ECE64C897445BBCA0229BD41F8E028ECD4 * value)
	{
		___U3CFileU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CFileU3Ek__BackingField_2), (void*)value);
	}
};


// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// Google.Protobuf.MessageParser
struct MessageParser_tB0F97A0DCF31AD5D21F69684004C9745C88099FC  : public RuntimeObject
{
public:
	// System.Func`1<Google.Protobuf.IMessage> Google.Protobuf.MessageParser::factory
	Func_1_t44DCED5EAA5278823CED11B2E1EEBC31C8BD7ABB * ___factory_0;
	// System.Boolean Google.Protobuf.MessageParser::<DiscardUnknownFields>k__BackingField
	bool ___U3CDiscardUnknownFieldsU3Ek__BackingField_1;
	// Google.Protobuf.ExtensionRegistry Google.Protobuf.MessageParser::<Extensions>k__BackingField
	ExtensionRegistry_tA5BBE047B436970D8BE3DBBE73D5BBFEAFA1CE47 * ___U3CExtensionsU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_factory_0() { return static_cast<int32_t>(offsetof(MessageParser_tB0F97A0DCF31AD5D21F69684004C9745C88099FC, ___factory_0)); }
	inline Func_1_t44DCED5EAA5278823CED11B2E1EEBC31C8BD7ABB * get_factory_0() const { return ___factory_0; }
	inline Func_1_t44DCED5EAA5278823CED11B2E1EEBC31C8BD7ABB ** get_address_of_factory_0() { return &___factory_0; }
	inline void set_factory_0(Func_1_t44DCED5EAA5278823CED11B2E1EEBC31C8BD7ABB * value)
	{
		___factory_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___factory_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CDiscardUnknownFieldsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(MessageParser_tB0F97A0DCF31AD5D21F69684004C9745C88099FC, ___U3CDiscardUnknownFieldsU3Ek__BackingField_1)); }
	inline bool get_U3CDiscardUnknownFieldsU3Ek__BackingField_1() const { return ___U3CDiscardUnknownFieldsU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CDiscardUnknownFieldsU3Ek__BackingField_1() { return &___U3CDiscardUnknownFieldsU3Ek__BackingField_1; }
	inline void set_U3CDiscardUnknownFieldsU3Ek__BackingField_1(bool value)
	{
		___U3CDiscardUnknownFieldsU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CExtensionsU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(MessageParser_tB0F97A0DCF31AD5D21F69684004C9745C88099FC, ___U3CExtensionsU3Ek__BackingField_2)); }
	inline ExtensionRegistry_tA5BBE047B436970D8BE3DBBE73D5BBFEAFA1CE47 * get_U3CExtensionsU3Ek__BackingField_2() const { return ___U3CExtensionsU3Ek__BackingField_2; }
	inline ExtensionRegistry_tA5BBE047B436970D8BE3DBBE73D5BBFEAFA1CE47 ** get_address_of_U3CExtensionsU3Ek__BackingField_2() { return &___U3CExtensionsU3Ek__BackingField_2; }
	inline void set_U3CExtensionsU3Ek__BackingField_2(ExtensionRegistry_tA5BBE047B436970D8BE3DBBE73D5BBFEAFA1CE47 * value)
	{
		___U3CExtensionsU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CExtensionsU3Ek__BackingField_2), (void*)value);
	}
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// Google.Protobuf.UnknownFieldSet
struct UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72  : public RuntimeObject
{
public:
	// System.Collections.Generic.IDictionary`2<System.Int32,Google.Protobuf.UnknownField> Google.Protobuf.UnknownFieldSet::fields
	RuntimeObject* ___fields_0;
	// System.Int32 Google.Protobuf.UnknownFieldSet::lastFieldNumber
	int32_t ___lastFieldNumber_1;
	// Google.Protobuf.UnknownField Google.Protobuf.UnknownFieldSet::lastField
	UnknownField_tA1C0671D4A8CE8D36DE75746AF1940C2923FCFE7 * ___lastField_2;

public:
	inline static int32_t get_offset_of_fields_0() { return static_cast<int32_t>(offsetof(UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72, ___fields_0)); }
	inline RuntimeObject* get_fields_0() const { return ___fields_0; }
	inline RuntimeObject** get_address_of_fields_0() { return &___fields_0; }
	inline void set_fields_0(RuntimeObject* value)
	{
		___fields_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___fields_0), (void*)value);
	}

	inline static int32_t get_offset_of_lastFieldNumber_1() { return static_cast<int32_t>(offsetof(UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72, ___lastFieldNumber_1)); }
	inline int32_t get_lastFieldNumber_1() const { return ___lastFieldNumber_1; }
	inline int32_t* get_address_of_lastFieldNumber_1() { return &___lastFieldNumber_1; }
	inline void set_lastFieldNumber_1(int32_t value)
	{
		___lastFieldNumber_1 = value;
	}

	inline static int32_t get_offset_of_lastField_2() { return static_cast<int32_t>(offsetof(UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72, ___lastField_2)); }
	inline UnknownField_tA1C0671D4A8CE8D36DE75746AF1940C2923FCFE7 * get_lastField_2() const { return ___lastField_2; }
	inline UnknownField_tA1C0671D4A8CE8D36DE75746AF1940C2923FCFE7 ** get_address_of_lastField_2() { return &___lastField_2; }
	inline void set_lastField_2(UnknownField_tA1C0671D4A8CE8D36DE75746AF1940C2923FCFE7 * value)
	{
		___lastField_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lastField_2), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// Google.Protobuf.Reflection.DescriptorProto/Types/ExtensionRange
struct ExtensionRange_t355AED379D3458AC80443047C2DF72AA44639D84  : public RuntimeObject
{
public:
	// Google.Protobuf.UnknownFieldSet Google.Protobuf.Reflection.DescriptorProto/Types/ExtensionRange::_unknownFields
	UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 * ____unknownFields_1;
	// System.Int32 Google.Protobuf.Reflection.DescriptorProto/Types/ExtensionRange::_hasBits0
	int32_t ____hasBits0_2;
	// System.Int32 Google.Protobuf.Reflection.DescriptorProto/Types/ExtensionRange::start_
	int32_t ___start__4;
	// System.Int32 Google.Protobuf.Reflection.DescriptorProto/Types/ExtensionRange::end_
	int32_t ___end__6;
	// Google.Protobuf.Reflection.ExtensionRangeOptions Google.Protobuf.Reflection.DescriptorProto/Types/ExtensionRange::options_
	ExtensionRangeOptions_t497AE48098D601FD2AB661860BDA91704902E99F * ___options__7;

public:
	inline static int32_t get_offset_of__unknownFields_1() { return static_cast<int32_t>(offsetof(ExtensionRange_t355AED379D3458AC80443047C2DF72AA44639D84, ____unknownFields_1)); }
	inline UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 * get__unknownFields_1() const { return ____unknownFields_1; }
	inline UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 ** get_address_of__unknownFields_1() { return &____unknownFields_1; }
	inline void set__unknownFields_1(UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 * value)
	{
		____unknownFields_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____unknownFields_1), (void*)value);
	}

	inline static int32_t get_offset_of__hasBits0_2() { return static_cast<int32_t>(offsetof(ExtensionRange_t355AED379D3458AC80443047C2DF72AA44639D84, ____hasBits0_2)); }
	inline int32_t get__hasBits0_2() const { return ____hasBits0_2; }
	inline int32_t* get_address_of__hasBits0_2() { return &____hasBits0_2; }
	inline void set__hasBits0_2(int32_t value)
	{
		____hasBits0_2 = value;
	}

	inline static int32_t get_offset_of_start__4() { return static_cast<int32_t>(offsetof(ExtensionRange_t355AED379D3458AC80443047C2DF72AA44639D84, ___start__4)); }
	inline int32_t get_start__4() const { return ___start__4; }
	inline int32_t* get_address_of_start__4() { return &___start__4; }
	inline void set_start__4(int32_t value)
	{
		___start__4 = value;
	}

	inline static int32_t get_offset_of_end__6() { return static_cast<int32_t>(offsetof(ExtensionRange_t355AED379D3458AC80443047C2DF72AA44639D84, ___end__6)); }
	inline int32_t get_end__6() const { return ___end__6; }
	inline int32_t* get_address_of_end__6() { return &___end__6; }
	inline void set_end__6(int32_t value)
	{
		___end__6 = value;
	}

	inline static int32_t get_offset_of_options__7() { return static_cast<int32_t>(offsetof(ExtensionRange_t355AED379D3458AC80443047C2DF72AA44639D84, ___options__7)); }
	inline ExtensionRangeOptions_t497AE48098D601FD2AB661860BDA91704902E99F * get_options__7() const { return ___options__7; }
	inline ExtensionRangeOptions_t497AE48098D601FD2AB661860BDA91704902E99F ** get_address_of_options__7() { return &___options__7; }
	inline void set_options__7(ExtensionRangeOptions_t497AE48098D601FD2AB661860BDA91704902E99F * value)
	{
		___options__7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___options__7), (void*)value);
	}
};

struct ExtensionRange_t355AED379D3458AC80443047C2DF72AA44639D84_StaticFields
{
public:
	// Google.Protobuf.MessageParser`1<Google.Protobuf.Reflection.DescriptorProto/Types/ExtensionRange> Google.Protobuf.Reflection.DescriptorProto/Types/ExtensionRange::_parser
	MessageParser_1_t98C9C5E3B0B505B0A1C5C7E1646762D1C4C24242 * ____parser_0;
	// System.Int32 Google.Protobuf.Reflection.DescriptorProto/Types/ExtensionRange::StartDefaultValue
	int32_t ___StartDefaultValue_3;
	// System.Int32 Google.Protobuf.Reflection.DescriptorProto/Types/ExtensionRange::EndDefaultValue
	int32_t ___EndDefaultValue_5;

public:
	inline static int32_t get_offset_of__parser_0() { return static_cast<int32_t>(offsetof(ExtensionRange_t355AED379D3458AC80443047C2DF72AA44639D84_StaticFields, ____parser_0)); }
	inline MessageParser_1_t98C9C5E3B0B505B0A1C5C7E1646762D1C4C24242 * get__parser_0() const { return ____parser_0; }
	inline MessageParser_1_t98C9C5E3B0B505B0A1C5C7E1646762D1C4C24242 ** get_address_of__parser_0() { return &____parser_0; }
	inline void set__parser_0(MessageParser_1_t98C9C5E3B0B505B0A1C5C7E1646762D1C4C24242 * value)
	{
		____parser_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____parser_0), (void*)value);
	}

	inline static int32_t get_offset_of_StartDefaultValue_3() { return static_cast<int32_t>(offsetof(ExtensionRange_t355AED379D3458AC80443047C2DF72AA44639D84_StaticFields, ___StartDefaultValue_3)); }
	inline int32_t get_StartDefaultValue_3() const { return ___StartDefaultValue_3; }
	inline int32_t* get_address_of_StartDefaultValue_3() { return &___StartDefaultValue_3; }
	inline void set_StartDefaultValue_3(int32_t value)
	{
		___StartDefaultValue_3 = value;
	}

	inline static int32_t get_offset_of_EndDefaultValue_5() { return static_cast<int32_t>(offsetof(ExtensionRange_t355AED379D3458AC80443047C2DF72AA44639D84_StaticFields, ___EndDefaultValue_5)); }
	inline int32_t get_EndDefaultValue_5() const { return ___EndDefaultValue_5; }
	inline int32_t* get_address_of_EndDefaultValue_5() { return &___EndDefaultValue_5; }
	inline void set_EndDefaultValue_5(int32_t value)
	{
		___EndDefaultValue_5 = value;
	}
};


// Google.Protobuf.Reflection.DescriptorProto/Types/ReservedRange
struct ReservedRange_tAED8F62AF040F5F133732196696893B313EA458B  : public RuntimeObject
{
public:
	// Google.Protobuf.UnknownFieldSet Google.Protobuf.Reflection.DescriptorProto/Types/ReservedRange::_unknownFields
	UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 * ____unknownFields_1;
	// System.Int32 Google.Protobuf.Reflection.DescriptorProto/Types/ReservedRange::_hasBits0
	int32_t ____hasBits0_2;
	// System.Int32 Google.Protobuf.Reflection.DescriptorProto/Types/ReservedRange::start_
	int32_t ___start__4;
	// System.Int32 Google.Protobuf.Reflection.DescriptorProto/Types/ReservedRange::end_
	int32_t ___end__6;

public:
	inline static int32_t get_offset_of__unknownFields_1() { return static_cast<int32_t>(offsetof(ReservedRange_tAED8F62AF040F5F133732196696893B313EA458B, ____unknownFields_1)); }
	inline UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 * get__unknownFields_1() const { return ____unknownFields_1; }
	inline UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 ** get_address_of__unknownFields_1() { return &____unknownFields_1; }
	inline void set__unknownFields_1(UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 * value)
	{
		____unknownFields_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____unknownFields_1), (void*)value);
	}

	inline static int32_t get_offset_of__hasBits0_2() { return static_cast<int32_t>(offsetof(ReservedRange_tAED8F62AF040F5F133732196696893B313EA458B, ____hasBits0_2)); }
	inline int32_t get__hasBits0_2() const { return ____hasBits0_2; }
	inline int32_t* get_address_of__hasBits0_2() { return &____hasBits0_2; }
	inline void set__hasBits0_2(int32_t value)
	{
		____hasBits0_2 = value;
	}

	inline static int32_t get_offset_of_start__4() { return static_cast<int32_t>(offsetof(ReservedRange_tAED8F62AF040F5F133732196696893B313EA458B, ___start__4)); }
	inline int32_t get_start__4() const { return ___start__4; }
	inline int32_t* get_address_of_start__4() { return &___start__4; }
	inline void set_start__4(int32_t value)
	{
		___start__4 = value;
	}

	inline static int32_t get_offset_of_end__6() { return static_cast<int32_t>(offsetof(ReservedRange_tAED8F62AF040F5F133732196696893B313EA458B, ___end__6)); }
	inline int32_t get_end__6() const { return ___end__6; }
	inline int32_t* get_address_of_end__6() { return &___end__6; }
	inline void set_end__6(int32_t value)
	{
		___end__6 = value;
	}
};

struct ReservedRange_tAED8F62AF040F5F133732196696893B313EA458B_StaticFields
{
public:
	// Google.Protobuf.MessageParser`1<Google.Protobuf.Reflection.DescriptorProto/Types/ReservedRange> Google.Protobuf.Reflection.DescriptorProto/Types/ReservedRange::_parser
	MessageParser_1_t38AEDCF2E2057B00B1E8F484B575E1B5709FA070 * ____parser_0;
	// System.Int32 Google.Protobuf.Reflection.DescriptorProto/Types/ReservedRange::StartDefaultValue
	int32_t ___StartDefaultValue_3;
	// System.Int32 Google.Protobuf.Reflection.DescriptorProto/Types/ReservedRange::EndDefaultValue
	int32_t ___EndDefaultValue_5;

public:
	inline static int32_t get_offset_of__parser_0() { return static_cast<int32_t>(offsetof(ReservedRange_tAED8F62AF040F5F133732196696893B313EA458B_StaticFields, ____parser_0)); }
	inline MessageParser_1_t38AEDCF2E2057B00B1E8F484B575E1B5709FA070 * get__parser_0() const { return ____parser_0; }
	inline MessageParser_1_t38AEDCF2E2057B00B1E8F484B575E1B5709FA070 ** get_address_of__parser_0() { return &____parser_0; }
	inline void set__parser_0(MessageParser_1_t38AEDCF2E2057B00B1E8F484B575E1B5709FA070 * value)
	{
		____parser_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____parser_0), (void*)value);
	}

	inline static int32_t get_offset_of_StartDefaultValue_3() { return static_cast<int32_t>(offsetof(ReservedRange_tAED8F62AF040F5F133732196696893B313EA458B_StaticFields, ___StartDefaultValue_3)); }
	inline int32_t get_StartDefaultValue_3() const { return ___StartDefaultValue_3; }
	inline int32_t* get_address_of_StartDefaultValue_3() { return &___StartDefaultValue_3; }
	inline void set_StartDefaultValue_3(int32_t value)
	{
		___StartDefaultValue_3 = value;
	}

	inline static int32_t get_offset_of_EndDefaultValue_5() { return static_cast<int32_t>(offsetof(ReservedRange_tAED8F62AF040F5F133732196696893B313EA458B_StaticFields, ___EndDefaultValue_5)); }
	inline int32_t get_EndDefaultValue_5() const { return ___EndDefaultValue_5; }
	inline int32_t* get_address_of_EndDefaultValue_5() { return &___EndDefaultValue_5; }
	inline void set_EndDefaultValue_5(int32_t value)
	{
		___EndDefaultValue_5 = value;
	}
};


// Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange
struct EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41  : public RuntimeObject
{
public:
	// Google.Protobuf.UnknownFieldSet Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange::_unknownFields
	UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 * ____unknownFields_1;
	// System.Int32 Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange::_hasBits0
	int32_t ____hasBits0_2;
	// System.Int32 Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange::start_
	int32_t ___start__4;
	// System.Int32 Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange::end_
	int32_t ___end__6;

public:
	inline static int32_t get_offset_of__unknownFields_1() { return static_cast<int32_t>(offsetof(EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41, ____unknownFields_1)); }
	inline UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 * get__unknownFields_1() const { return ____unknownFields_1; }
	inline UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 ** get_address_of__unknownFields_1() { return &____unknownFields_1; }
	inline void set__unknownFields_1(UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 * value)
	{
		____unknownFields_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____unknownFields_1), (void*)value);
	}

	inline static int32_t get_offset_of__hasBits0_2() { return static_cast<int32_t>(offsetof(EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41, ____hasBits0_2)); }
	inline int32_t get__hasBits0_2() const { return ____hasBits0_2; }
	inline int32_t* get_address_of__hasBits0_2() { return &____hasBits0_2; }
	inline void set__hasBits0_2(int32_t value)
	{
		____hasBits0_2 = value;
	}

	inline static int32_t get_offset_of_start__4() { return static_cast<int32_t>(offsetof(EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41, ___start__4)); }
	inline int32_t get_start__4() const { return ___start__4; }
	inline int32_t* get_address_of_start__4() { return &___start__4; }
	inline void set_start__4(int32_t value)
	{
		___start__4 = value;
	}

	inline static int32_t get_offset_of_end__6() { return static_cast<int32_t>(offsetof(EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41, ___end__6)); }
	inline int32_t get_end__6() const { return ___end__6; }
	inline int32_t* get_address_of_end__6() { return &___end__6; }
	inline void set_end__6(int32_t value)
	{
		___end__6 = value;
	}
};

struct EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41_StaticFields
{
public:
	// Google.Protobuf.MessageParser`1<Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange> Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange::_parser
	MessageParser_1_tCCF97EFBB4482323562EEB1F40E83233B2D32D55 * ____parser_0;
	// System.Int32 Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange::StartDefaultValue
	int32_t ___StartDefaultValue_3;
	// System.Int32 Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange::EndDefaultValue
	int32_t ___EndDefaultValue_5;

public:
	inline static int32_t get_offset_of__parser_0() { return static_cast<int32_t>(offsetof(EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41_StaticFields, ____parser_0)); }
	inline MessageParser_1_tCCF97EFBB4482323562EEB1F40E83233B2D32D55 * get__parser_0() const { return ____parser_0; }
	inline MessageParser_1_tCCF97EFBB4482323562EEB1F40E83233B2D32D55 ** get_address_of__parser_0() { return &____parser_0; }
	inline void set__parser_0(MessageParser_1_tCCF97EFBB4482323562EEB1F40E83233B2D32D55 * value)
	{
		____parser_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____parser_0), (void*)value);
	}

	inline static int32_t get_offset_of_StartDefaultValue_3() { return static_cast<int32_t>(offsetof(EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41_StaticFields, ___StartDefaultValue_3)); }
	inline int32_t get_StartDefaultValue_3() const { return ___StartDefaultValue_3; }
	inline int32_t* get_address_of_StartDefaultValue_3() { return &___StartDefaultValue_3; }
	inline void set_StartDefaultValue_3(int32_t value)
	{
		___StartDefaultValue_3 = value;
	}

	inline static int32_t get_offset_of_EndDefaultValue_5() { return static_cast<int32_t>(offsetof(EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41_StaticFields, ___EndDefaultValue_5)); }
	inline int32_t get_EndDefaultValue_5() const { return ___EndDefaultValue_5; }
	inline int32_t* get_address_of_EndDefaultValue_5() { return &___EndDefaultValue_5; }
	inline void set_EndDefaultValue_5(int32_t value)
	{
		___EndDefaultValue_5 = value;
	}
};


// Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation
struct Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405  : public RuntimeObject
{
public:
	// Google.Protobuf.UnknownFieldSet Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation::_unknownFields
	UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 * ____unknownFields_1;
	// System.Int32 Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation::_hasBits0
	int32_t ____hasBits0_2;
	// Google.Protobuf.Collections.RepeatedField`1<System.Int32> Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation::path_
	RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419 * ___path__4;
	// System.String Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation::sourceFile_
	String_t* ___sourceFile__6;
	// System.Int32 Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation::begin_
	int32_t ___begin__8;
	// System.Int32 Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation::end_
	int32_t ___end__10;

public:
	inline static int32_t get_offset_of__unknownFields_1() { return static_cast<int32_t>(offsetof(Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405, ____unknownFields_1)); }
	inline UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 * get__unknownFields_1() const { return ____unknownFields_1; }
	inline UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 ** get_address_of__unknownFields_1() { return &____unknownFields_1; }
	inline void set__unknownFields_1(UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 * value)
	{
		____unknownFields_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____unknownFields_1), (void*)value);
	}

	inline static int32_t get_offset_of__hasBits0_2() { return static_cast<int32_t>(offsetof(Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405, ____hasBits0_2)); }
	inline int32_t get__hasBits0_2() const { return ____hasBits0_2; }
	inline int32_t* get_address_of__hasBits0_2() { return &____hasBits0_2; }
	inline void set__hasBits0_2(int32_t value)
	{
		____hasBits0_2 = value;
	}

	inline static int32_t get_offset_of_path__4() { return static_cast<int32_t>(offsetof(Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405, ___path__4)); }
	inline RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419 * get_path__4() const { return ___path__4; }
	inline RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419 ** get_address_of_path__4() { return &___path__4; }
	inline void set_path__4(RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419 * value)
	{
		___path__4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___path__4), (void*)value);
	}

	inline static int32_t get_offset_of_sourceFile__6() { return static_cast<int32_t>(offsetof(Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405, ___sourceFile__6)); }
	inline String_t* get_sourceFile__6() const { return ___sourceFile__6; }
	inline String_t** get_address_of_sourceFile__6() { return &___sourceFile__6; }
	inline void set_sourceFile__6(String_t* value)
	{
		___sourceFile__6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sourceFile__6), (void*)value);
	}

	inline static int32_t get_offset_of_begin__8() { return static_cast<int32_t>(offsetof(Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405, ___begin__8)); }
	inline int32_t get_begin__8() const { return ___begin__8; }
	inline int32_t* get_address_of_begin__8() { return &___begin__8; }
	inline void set_begin__8(int32_t value)
	{
		___begin__8 = value;
	}

	inline static int32_t get_offset_of_end__10() { return static_cast<int32_t>(offsetof(Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405, ___end__10)); }
	inline int32_t get_end__10() const { return ___end__10; }
	inline int32_t* get_address_of_end__10() { return &___end__10; }
	inline void set_end__10(int32_t value)
	{
		___end__10 = value;
	}
};

struct Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_StaticFields
{
public:
	// Google.Protobuf.MessageParser`1<Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation> Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation::_parser
	MessageParser_1_t422AF617132E681AA126A8C774D49BC4101614D0 * ____parser_0;
	// Google.Protobuf.FieldCodec`1<System.Int32> Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation::_repeated_path_codec
	FieldCodec_1_tAB7BF6BD67CE831FB7E8A437894272BD9C511681 * ____repeated_path_codec_3;
	// System.String Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation::SourceFileDefaultValue
	String_t* ___SourceFileDefaultValue_5;
	// System.Int32 Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation::BeginDefaultValue
	int32_t ___BeginDefaultValue_7;
	// System.Int32 Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation::EndDefaultValue
	int32_t ___EndDefaultValue_9;

public:
	inline static int32_t get_offset_of__parser_0() { return static_cast<int32_t>(offsetof(Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_StaticFields, ____parser_0)); }
	inline MessageParser_1_t422AF617132E681AA126A8C774D49BC4101614D0 * get__parser_0() const { return ____parser_0; }
	inline MessageParser_1_t422AF617132E681AA126A8C774D49BC4101614D0 ** get_address_of__parser_0() { return &____parser_0; }
	inline void set__parser_0(MessageParser_1_t422AF617132E681AA126A8C774D49BC4101614D0 * value)
	{
		____parser_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____parser_0), (void*)value);
	}

	inline static int32_t get_offset_of__repeated_path_codec_3() { return static_cast<int32_t>(offsetof(Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_StaticFields, ____repeated_path_codec_3)); }
	inline FieldCodec_1_tAB7BF6BD67CE831FB7E8A437894272BD9C511681 * get__repeated_path_codec_3() const { return ____repeated_path_codec_3; }
	inline FieldCodec_1_tAB7BF6BD67CE831FB7E8A437894272BD9C511681 ** get_address_of__repeated_path_codec_3() { return &____repeated_path_codec_3; }
	inline void set__repeated_path_codec_3(FieldCodec_1_tAB7BF6BD67CE831FB7E8A437894272BD9C511681 * value)
	{
		____repeated_path_codec_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____repeated_path_codec_3), (void*)value);
	}

	inline static int32_t get_offset_of_SourceFileDefaultValue_5() { return static_cast<int32_t>(offsetof(Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_StaticFields, ___SourceFileDefaultValue_5)); }
	inline String_t* get_SourceFileDefaultValue_5() const { return ___SourceFileDefaultValue_5; }
	inline String_t** get_address_of_SourceFileDefaultValue_5() { return &___SourceFileDefaultValue_5; }
	inline void set_SourceFileDefaultValue_5(String_t* value)
	{
		___SourceFileDefaultValue_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___SourceFileDefaultValue_5), (void*)value);
	}

	inline static int32_t get_offset_of_BeginDefaultValue_7() { return static_cast<int32_t>(offsetof(Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_StaticFields, ___BeginDefaultValue_7)); }
	inline int32_t get_BeginDefaultValue_7() const { return ___BeginDefaultValue_7; }
	inline int32_t* get_address_of_BeginDefaultValue_7() { return &___BeginDefaultValue_7; }
	inline void set_BeginDefaultValue_7(int32_t value)
	{
		___BeginDefaultValue_7 = value;
	}

	inline static int32_t get_offset_of_EndDefaultValue_9() { return static_cast<int32_t>(offsetof(Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_StaticFields, ___EndDefaultValue_9)); }
	inline int32_t get_EndDefaultValue_9() const { return ___EndDefaultValue_9; }
	inline int32_t* get_address_of_EndDefaultValue_9() { return &___EndDefaultValue_9; }
	inline void set_EndDefaultValue_9(int32_t value)
	{
		___EndDefaultValue_9 = value;
	}
};


// Google.Protobuf.JsonFormatter/OriginalEnumValueHelper/<>c
struct U3CU3Ec_t5555F991758A0DD73D48BCE4AF9A1B14B7F05FA3  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t5555F991758A0DD73D48BCE4AF9A1B14B7F05FA3_StaticFields
{
public:
	// Google.Protobuf.JsonFormatter/OriginalEnumValueHelper/<>c Google.Protobuf.JsonFormatter/OriginalEnumValueHelper/<>c::<>9
	U3CU3Ec_t5555F991758A0DD73D48BCE4AF9A1B14B7F05FA3 * ___U3CU3E9_0;
	// System.Func`2<System.Reflection.FieldInfo,System.Boolean> Google.Protobuf.JsonFormatter/OriginalEnumValueHelper/<>c::<>9__2_0
	Func_2_t0BE5B54DD65017DAF1DC5DEC5A896A2B0550F8AE * ___U3CU3E9__2_0_1;
	// System.Func`2<System.Reflection.FieldInfo,System.Boolean> Google.Protobuf.JsonFormatter/OriginalEnumValueHelper/<>c::<>9__2_1
	Func_2_t0BE5B54DD65017DAF1DC5DEC5A896A2B0550F8AE * ___U3CU3E9__2_1_2;
	// System.Func`2<System.Reflection.FieldInfo,System.Object> Google.Protobuf.JsonFormatter/OriginalEnumValueHelper/<>c::<>9__2_2
	Func_2_t531FF6D55AC985F40B486650A477C6789A3384E0 * ___U3CU3E9__2_2_3;
	// System.Func`2<System.Reflection.FieldInfo,System.String> Google.Protobuf.JsonFormatter/OriginalEnumValueHelper/<>c::<>9__2_3
	Func_2_t3E9169486444D508EC295C4254DF9267CEE35CCE * ___U3CU3E9__2_3_4;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t5555F991758A0DD73D48BCE4AF9A1B14B7F05FA3_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t5555F991758A0DD73D48BCE4AF9A1B14B7F05FA3 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t5555F991758A0DD73D48BCE4AF9A1B14B7F05FA3 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t5555F991758A0DD73D48BCE4AF9A1B14B7F05FA3 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__2_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t5555F991758A0DD73D48BCE4AF9A1B14B7F05FA3_StaticFields, ___U3CU3E9__2_0_1)); }
	inline Func_2_t0BE5B54DD65017DAF1DC5DEC5A896A2B0550F8AE * get_U3CU3E9__2_0_1() const { return ___U3CU3E9__2_0_1; }
	inline Func_2_t0BE5B54DD65017DAF1DC5DEC5A896A2B0550F8AE ** get_address_of_U3CU3E9__2_0_1() { return &___U3CU3E9__2_0_1; }
	inline void set_U3CU3E9__2_0_1(Func_2_t0BE5B54DD65017DAF1DC5DEC5A896A2B0550F8AE * value)
	{
		___U3CU3E9__2_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__2_0_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__2_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t5555F991758A0DD73D48BCE4AF9A1B14B7F05FA3_StaticFields, ___U3CU3E9__2_1_2)); }
	inline Func_2_t0BE5B54DD65017DAF1DC5DEC5A896A2B0550F8AE * get_U3CU3E9__2_1_2() const { return ___U3CU3E9__2_1_2; }
	inline Func_2_t0BE5B54DD65017DAF1DC5DEC5A896A2B0550F8AE ** get_address_of_U3CU3E9__2_1_2() { return &___U3CU3E9__2_1_2; }
	inline void set_U3CU3E9__2_1_2(Func_2_t0BE5B54DD65017DAF1DC5DEC5A896A2B0550F8AE * value)
	{
		___U3CU3E9__2_1_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__2_1_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__2_2_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_t5555F991758A0DD73D48BCE4AF9A1B14B7F05FA3_StaticFields, ___U3CU3E9__2_2_3)); }
	inline Func_2_t531FF6D55AC985F40B486650A477C6789A3384E0 * get_U3CU3E9__2_2_3() const { return ___U3CU3E9__2_2_3; }
	inline Func_2_t531FF6D55AC985F40B486650A477C6789A3384E0 ** get_address_of_U3CU3E9__2_2_3() { return &___U3CU3E9__2_2_3; }
	inline void set_U3CU3E9__2_2_3(Func_2_t531FF6D55AC985F40B486650A477C6789A3384E0 * value)
	{
		___U3CU3E9__2_2_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__2_2_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__2_3_4() { return static_cast<int32_t>(offsetof(U3CU3Ec_t5555F991758A0DD73D48BCE4AF9A1B14B7F05FA3_StaticFields, ___U3CU3E9__2_3_4)); }
	inline Func_2_t3E9169486444D508EC295C4254DF9267CEE35CCE * get_U3CU3E9__2_3_4() const { return ___U3CU3E9__2_3_4; }
	inline Func_2_t3E9169486444D508EC295C4254DF9267CEE35CCE ** get_address_of_U3CU3E9__2_3_4() { return &___U3CU3E9__2_3_4; }
	inline void set_U3CU3E9__2_3_4(Func_2_t3E9169486444D508EC295C4254DF9267CEE35CCE * value)
	{
		___U3CU3E9__2_3_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__2_3_4), (void*)value);
	}
};


// Google.Protobuf.Reflection.SourceCodeInfo/Types/Location
struct Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4  : public RuntimeObject
{
public:
	// Google.Protobuf.UnknownFieldSet Google.Protobuf.Reflection.SourceCodeInfo/Types/Location::_unknownFields
	UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 * ____unknownFields_1;
	// Google.Protobuf.Collections.RepeatedField`1<System.Int32> Google.Protobuf.Reflection.SourceCodeInfo/Types/Location::path_
	RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419 * ___path__3;
	// Google.Protobuf.Collections.RepeatedField`1<System.Int32> Google.Protobuf.Reflection.SourceCodeInfo/Types/Location::span_
	RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419 * ___span__5;
	// System.String Google.Protobuf.Reflection.SourceCodeInfo/Types/Location::leadingComments_
	String_t* ___leadingComments__7;
	// System.String Google.Protobuf.Reflection.SourceCodeInfo/Types/Location::trailingComments_
	String_t* ___trailingComments__9;
	// Google.Protobuf.Collections.RepeatedField`1<System.String> Google.Protobuf.Reflection.SourceCodeInfo/Types/Location::leadingDetachedComments_
	RepeatedField_1_t03A3C968DD0FF896E3F108784B11A7CBD313CDE1 * ___leadingDetachedComments__11;

public:
	inline static int32_t get_offset_of__unknownFields_1() { return static_cast<int32_t>(offsetof(Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4, ____unknownFields_1)); }
	inline UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 * get__unknownFields_1() const { return ____unknownFields_1; }
	inline UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 ** get_address_of__unknownFields_1() { return &____unknownFields_1; }
	inline void set__unknownFields_1(UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 * value)
	{
		____unknownFields_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____unknownFields_1), (void*)value);
	}

	inline static int32_t get_offset_of_path__3() { return static_cast<int32_t>(offsetof(Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4, ___path__3)); }
	inline RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419 * get_path__3() const { return ___path__3; }
	inline RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419 ** get_address_of_path__3() { return &___path__3; }
	inline void set_path__3(RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419 * value)
	{
		___path__3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___path__3), (void*)value);
	}

	inline static int32_t get_offset_of_span__5() { return static_cast<int32_t>(offsetof(Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4, ___span__5)); }
	inline RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419 * get_span__5() const { return ___span__5; }
	inline RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419 ** get_address_of_span__5() { return &___span__5; }
	inline void set_span__5(RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419 * value)
	{
		___span__5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___span__5), (void*)value);
	}

	inline static int32_t get_offset_of_leadingComments__7() { return static_cast<int32_t>(offsetof(Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4, ___leadingComments__7)); }
	inline String_t* get_leadingComments__7() const { return ___leadingComments__7; }
	inline String_t** get_address_of_leadingComments__7() { return &___leadingComments__7; }
	inline void set_leadingComments__7(String_t* value)
	{
		___leadingComments__7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___leadingComments__7), (void*)value);
	}

	inline static int32_t get_offset_of_trailingComments__9() { return static_cast<int32_t>(offsetof(Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4, ___trailingComments__9)); }
	inline String_t* get_trailingComments__9() const { return ___trailingComments__9; }
	inline String_t** get_address_of_trailingComments__9() { return &___trailingComments__9; }
	inline void set_trailingComments__9(String_t* value)
	{
		___trailingComments__9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___trailingComments__9), (void*)value);
	}

	inline static int32_t get_offset_of_leadingDetachedComments__11() { return static_cast<int32_t>(offsetof(Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4, ___leadingDetachedComments__11)); }
	inline RepeatedField_1_t03A3C968DD0FF896E3F108784B11A7CBD313CDE1 * get_leadingDetachedComments__11() const { return ___leadingDetachedComments__11; }
	inline RepeatedField_1_t03A3C968DD0FF896E3F108784B11A7CBD313CDE1 ** get_address_of_leadingDetachedComments__11() { return &___leadingDetachedComments__11; }
	inline void set_leadingDetachedComments__11(RepeatedField_1_t03A3C968DD0FF896E3F108784B11A7CBD313CDE1 * value)
	{
		___leadingDetachedComments__11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___leadingDetachedComments__11), (void*)value);
	}
};

struct Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_StaticFields
{
public:
	// Google.Protobuf.MessageParser`1<Google.Protobuf.Reflection.SourceCodeInfo/Types/Location> Google.Protobuf.Reflection.SourceCodeInfo/Types/Location::_parser
	MessageParser_1_t09C483F589C454D2B080958F963D017315003983 * ____parser_0;
	// Google.Protobuf.FieldCodec`1<System.Int32> Google.Protobuf.Reflection.SourceCodeInfo/Types/Location::_repeated_path_codec
	FieldCodec_1_tAB7BF6BD67CE831FB7E8A437894272BD9C511681 * ____repeated_path_codec_2;
	// Google.Protobuf.FieldCodec`1<System.Int32> Google.Protobuf.Reflection.SourceCodeInfo/Types/Location::_repeated_span_codec
	FieldCodec_1_tAB7BF6BD67CE831FB7E8A437894272BD9C511681 * ____repeated_span_codec_4;
	// System.String Google.Protobuf.Reflection.SourceCodeInfo/Types/Location::LeadingCommentsDefaultValue
	String_t* ___LeadingCommentsDefaultValue_6;
	// System.String Google.Protobuf.Reflection.SourceCodeInfo/Types/Location::TrailingCommentsDefaultValue
	String_t* ___TrailingCommentsDefaultValue_8;
	// Google.Protobuf.FieldCodec`1<System.String> Google.Protobuf.Reflection.SourceCodeInfo/Types/Location::_repeated_leadingDetachedComments_codec
	FieldCodec_1_t379D692C872A4BD7DE0539465056B8EF49D48FA0 * ____repeated_leadingDetachedComments_codec_10;

public:
	inline static int32_t get_offset_of__parser_0() { return static_cast<int32_t>(offsetof(Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_StaticFields, ____parser_0)); }
	inline MessageParser_1_t09C483F589C454D2B080958F963D017315003983 * get__parser_0() const { return ____parser_0; }
	inline MessageParser_1_t09C483F589C454D2B080958F963D017315003983 ** get_address_of__parser_0() { return &____parser_0; }
	inline void set__parser_0(MessageParser_1_t09C483F589C454D2B080958F963D017315003983 * value)
	{
		____parser_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____parser_0), (void*)value);
	}

	inline static int32_t get_offset_of__repeated_path_codec_2() { return static_cast<int32_t>(offsetof(Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_StaticFields, ____repeated_path_codec_2)); }
	inline FieldCodec_1_tAB7BF6BD67CE831FB7E8A437894272BD9C511681 * get__repeated_path_codec_2() const { return ____repeated_path_codec_2; }
	inline FieldCodec_1_tAB7BF6BD67CE831FB7E8A437894272BD9C511681 ** get_address_of__repeated_path_codec_2() { return &____repeated_path_codec_2; }
	inline void set__repeated_path_codec_2(FieldCodec_1_tAB7BF6BD67CE831FB7E8A437894272BD9C511681 * value)
	{
		____repeated_path_codec_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____repeated_path_codec_2), (void*)value);
	}

	inline static int32_t get_offset_of__repeated_span_codec_4() { return static_cast<int32_t>(offsetof(Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_StaticFields, ____repeated_span_codec_4)); }
	inline FieldCodec_1_tAB7BF6BD67CE831FB7E8A437894272BD9C511681 * get__repeated_span_codec_4() const { return ____repeated_span_codec_4; }
	inline FieldCodec_1_tAB7BF6BD67CE831FB7E8A437894272BD9C511681 ** get_address_of__repeated_span_codec_4() { return &____repeated_span_codec_4; }
	inline void set__repeated_span_codec_4(FieldCodec_1_tAB7BF6BD67CE831FB7E8A437894272BD9C511681 * value)
	{
		____repeated_span_codec_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____repeated_span_codec_4), (void*)value);
	}

	inline static int32_t get_offset_of_LeadingCommentsDefaultValue_6() { return static_cast<int32_t>(offsetof(Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_StaticFields, ___LeadingCommentsDefaultValue_6)); }
	inline String_t* get_LeadingCommentsDefaultValue_6() const { return ___LeadingCommentsDefaultValue_6; }
	inline String_t** get_address_of_LeadingCommentsDefaultValue_6() { return &___LeadingCommentsDefaultValue_6; }
	inline void set_LeadingCommentsDefaultValue_6(String_t* value)
	{
		___LeadingCommentsDefaultValue_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___LeadingCommentsDefaultValue_6), (void*)value);
	}

	inline static int32_t get_offset_of_TrailingCommentsDefaultValue_8() { return static_cast<int32_t>(offsetof(Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_StaticFields, ___TrailingCommentsDefaultValue_8)); }
	inline String_t* get_TrailingCommentsDefaultValue_8() const { return ___TrailingCommentsDefaultValue_8; }
	inline String_t** get_address_of_TrailingCommentsDefaultValue_8() { return &___TrailingCommentsDefaultValue_8; }
	inline void set_TrailingCommentsDefaultValue_8(String_t* value)
	{
		___TrailingCommentsDefaultValue_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrailingCommentsDefaultValue_8), (void*)value);
	}

	inline static int32_t get_offset_of__repeated_leadingDetachedComments_codec_10() { return static_cast<int32_t>(offsetof(Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_StaticFields, ____repeated_leadingDetachedComments_codec_10)); }
	inline FieldCodec_1_t379D692C872A4BD7DE0539465056B8EF49D48FA0 * get__repeated_leadingDetachedComments_codec_10() const { return ____repeated_leadingDetachedComments_codec_10; }
	inline FieldCodec_1_t379D692C872A4BD7DE0539465056B8EF49D48FA0 ** get_address_of__repeated_leadingDetachedComments_codec_10() { return &____repeated_leadingDetachedComments_codec_10; }
	inline void set__repeated_leadingDetachedComments_codec_10(FieldCodec_1_t379D692C872A4BD7DE0539465056B8EF49D48FA0 * value)
	{
		____repeated_leadingDetachedComments_codec_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____repeated_leadingDetachedComments_codec_10), (void*)value);
	}
};


// Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart
struct NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22  : public RuntimeObject
{
public:
	// Google.Protobuf.UnknownFieldSet Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart::_unknownFields
	UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 * ____unknownFields_1;
	// System.Int32 Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart::_hasBits0
	int32_t ____hasBits0_2;
	// System.String Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart::namePart_
	String_t* ___namePart__4;
	// System.Boolean Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart::isExtension_
	bool ___isExtension__6;

public:
	inline static int32_t get_offset_of__unknownFields_1() { return static_cast<int32_t>(offsetof(NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22, ____unknownFields_1)); }
	inline UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 * get__unknownFields_1() const { return ____unknownFields_1; }
	inline UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 ** get_address_of__unknownFields_1() { return &____unknownFields_1; }
	inline void set__unknownFields_1(UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 * value)
	{
		____unknownFields_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____unknownFields_1), (void*)value);
	}

	inline static int32_t get_offset_of__hasBits0_2() { return static_cast<int32_t>(offsetof(NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22, ____hasBits0_2)); }
	inline int32_t get__hasBits0_2() const { return ____hasBits0_2; }
	inline int32_t* get_address_of__hasBits0_2() { return &____hasBits0_2; }
	inline void set__hasBits0_2(int32_t value)
	{
		____hasBits0_2 = value;
	}

	inline static int32_t get_offset_of_namePart__4() { return static_cast<int32_t>(offsetof(NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22, ___namePart__4)); }
	inline String_t* get_namePart__4() const { return ___namePart__4; }
	inline String_t** get_address_of_namePart__4() { return &___namePart__4; }
	inline void set_namePart__4(String_t* value)
	{
		___namePart__4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___namePart__4), (void*)value);
	}

	inline static int32_t get_offset_of_isExtension__6() { return static_cast<int32_t>(offsetof(NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22, ___isExtension__6)); }
	inline bool get_isExtension__6() const { return ___isExtension__6; }
	inline bool* get_address_of_isExtension__6() { return &___isExtension__6; }
	inline void set_isExtension__6(bool value)
	{
		___isExtension__6 = value;
	}
};

struct NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22_StaticFields
{
public:
	// Google.Protobuf.MessageParser`1<Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart> Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart::_parser
	MessageParser_1_tE61087F0F9CB9EE7534691A864269E589F1453DF * ____parser_0;
	// System.String Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart::NamePart_DefaultValue
	String_t* ___NamePart_DefaultValue_3;
	// System.Boolean Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart::IsExtensionDefaultValue
	bool ___IsExtensionDefaultValue_5;

public:
	inline static int32_t get_offset_of__parser_0() { return static_cast<int32_t>(offsetof(NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22_StaticFields, ____parser_0)); }
	inline MessageParser_1_tE61087F0F9CB9EE7534691A864269E589F1453DF * get__parser_0() const { return ____parser_0; }
	inline MessageParser_1_tE61087F0F9CB9EE7534691A864269E589F1453DF ** get_address_of__parser_0() { return &____parser_0; }
	inline void set__parser_0(MessageParser_1_tE61087F0F9CB9EE7534691A864269E589F1453DF * value)
	{
		____parser_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____parser_0), (void*)value);
	}

	inline static int32_t get_offset_of_NamePart_DefaultValue_3() { return static_cast<int32_t>(offsetof(NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22_StaticFields, ___NamePart_DefaultValue_3)); }
	inline String_t* get_NamePart_DefaultValue_3() const { return ___NamePart_DefaultValue_3; }
	inline String_t** get_address_of_NamePart_DefaultValue_3() { return &___NamePart_DefaultValue_3; }
	inline void set_NamePart_DefaultValue_3(String_t* value)
	{
		___NamePart_DefaultValue_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___NamePart_DefaultValue_3), (void*)value);
	}

	inline static int32_t get_offset_of_IsExtensionDefaultValue_5() { return static_cast<int32_t>(offsetof(NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22_StaticFields, ___IsExtensionDefaultValue_5)); }
	inline bool get_IsExtensionDefaultValue_5() const { return ___IsExtensionDefaultValue_5; }
	inline bool* get_address_of_IsExtensionDefaultValue_5() { return &___IsExtensionDefaultValue_5; }
	inline void set_IsExtensionDefaultValue_5(bool value)
	{
		___IsExtensionDefaultValue_5 = value;
	}
};


// Google.Protobuf.Reflection.DescriptorProto/Types/ExtensionRange/<>c
struct U3CU3Ec_tC3789B2A2639D39522B7FC88CE44B0FF78387D85  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tC3789B2A2639D39522B7FC88CE44B0FF78387D85_StaticFields
{
public:
	// Google.Protobuf.Reflection.DescriptorProto/Types/ExtensionRange/<>c Google.Protobuf.Reflection.DescriptorProto/Types/ExtensionRange/<>c::<>9
	U3CU3Ec_tC3789B2A2639D39522B7FC88CE44B0FF78387D85 * ___U3CU3E9_0;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tC3789B2A2639D39522B7FC88CE44B0FF78387D85_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tC3789B2A2639D39522B7FC88CE44B0FF78387D85 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tC3789B2A2639D39522B7FC88CE44B0FF78387D85 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tC3789B2A2639D39522B7FC88CE44B0FF78387D85 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}
};


// Google.Protobuf.Reflection.DescriptorProto/Types/ReservedRange/<>c
struct U3CU3Ec_t14ED7AF2EE0452B68867D51843E5E3FBF40F7D2D  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t14ED7AF2EE0452B68867D51843E5E3FBF40F7D2D_StaticFields
{
public:
	// Google.Protobuf.Reflection.DescriptorProto/Types/ReservedRange/<>c Google.Protobuf.Reflection.DescriptorProto/Types/ReservedRange/<>c::<>9
	U3CU3Ec_t14ED7AF2EE0452B68867D51843E5E3FBF40F7D2D * ___U3CU3E9_0;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t14ED7AF2EE0452B68867D51843E5E3FBF40F7D2D_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t14ED7AF2EE0452B68867D51843E5E3FBF40F7D2D * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t14ED7AF2EE0452B68867D51843E5E3FBF40F7D2D ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t14ED7AF2EE0452B68867D51843E5E3FBF40F7D2D * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}
};


// Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange/<>c
struct U3CU3Ec_t0FB8D11271B41B39C63CE8A142DE7F8F87630794  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t0FB8D11271B41B39C63CE8A142DE7F8F87630794_StaticFields
{
public:
	// Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange/<>c Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange/<>c::<>9
	U3CU3Ec_t0FB8D11271B41B39C63CE8A142DE7F8F87630794 * ___U3CU3E9_0;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t0FB8D11271B41B39C63CE8A142DE7F8F87630794_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t0FB8D11271B41B39C63CE8A142DE7F8F87630794 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t0FB8D11271B41B39C63CE8A142DE7F8F87630794 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t0FB8D11271B41B39C63CE8A142DE7F8F87630794 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}
};


// Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation/<>c
struct U3CU3Ec_t48164166658FE6E4AA4BC81699272A7660D5A345  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t48164166658FE6E4AA4BC81699272A7660D5A345_StaticFields
{
public:
	// Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation/<>c Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation/<>c::<>9
	U3CU3Ec_t48164166658FE6E4AA4BC81699272A7660D5A345 * ___U3CU3E9_0;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t48164166658FE6E4AA4BC81699272A7660D5A345_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t48164166658FE6E4AA4BC81699272A7660D5A345 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t48164166658FE6E4AA4BC81699272A7660D5A345 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t48164166658FE6E4AA4BC81699272A7660D5A345 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}
};


// Google.Protobuf.Reflection.SourceCodeInfo/Types/Location/<>c
struct U3CU3Ec_tE6E47C051562B8F8FCE9347E1B99439CF27AB1AF  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tE6E47C051562B8F8FCE9347E1B99439CF27AB1AF_StaticFields
{
public:
	// Google.Protobuf.Reflection.SourceCodeInfo/Types/Location/<>c Google.Protobuf.Reflection.SourceCodeInfo/Types/Location/<>c::<>9
	U3CU3Ec_tE6E47C051562B8F8FCE9347E1B99439CF27AB1AF * ___U3CU3E9_0;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tE6E47C051562B8F8FCE9347E1B99439CF27AB1AF_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tE6E47C051562B8F8FCE9347E1B99439CF27AB1AF * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tE6E47C051562B8F8FCE9347E1B99439CF27AB1AF ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tE6E47C051562B8F8FCE9347E1B99439CF27AB1AF * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}
};


// Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart/<>c
struct U3CU3Ec_t70C29F3AE73CD37C0D77DCC96A139645BE59A453  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t70C29F3AE73CD37C0D77DCC96A139645BE59A453_StaticFields
{
public:
	// Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart/<>c Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart/<>c::<>9
	U3CU3Ec_t70C29F3AE73CD37C0D77DCC96A139645BE59A453 * ___U3CU3E9_0;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t70C29F3AE73CD37C0D77DCC96A139645BE59A453_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t70C29F3AE73CD37C0D77DCC96A139645BE59A453 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t70C29F3AE73CD37C0D77DCC96A139645BE59A453 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t70C29F3AE73CD37C0D77DCC96A139645BE59A453 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}
};


// Google.Protobuf.MessageParser`1<Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange>
struct MessageParser_1_tCCF97EFBB4482323562EEB1F40E83233B2D32D55  : public MessageParser_tB0F97A0DCF31AD5D21F69684004C9745C88099FC
{
public:
	// System.Func`1<T> Google.Protobuf.MessageParser`1::factory
	Func_1_tB4DEFF82E390DE2B16A177BF95E2A660E016FC83 * ___factory_3;

public:
	inline static int32_t get_offset_of_factory_3() { return static_cast<int32_t>(offsetof(MessageParser_1_tCCF97EFBB4482323562EEB1F40E83233B2D32D55, ___factory_3)); }
	inline Func_1_tB4DEFF82E390DE2B16A177BF95E2A660E016FC83 * get_factory_3() const { return ___factory_3; }
	inline Func_1_tB4DEFF82E390DE2B16A177BF95E2A660E016FC83 ** get_address_of_factory_3() { return &___factory_3; }
	inline void set_factory_3(Func_1_tB4DEFF82E390DE2B16A177BF95E2A660E016FC83 * value)
	{
		___factory_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___factory_3), (void*)value);
	}
};


// Google.Protobuf.MessageParser`1<Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation>
struct MessageParser_1_t422AF617132E681AA126A8C774D49BC4101614D0  : public MessageParser_tB0F97A0DCF31AD5D21F69684004C9745C88099FC
{
public:
	// System.Func`1<T> Google.Protobuf.MessageParser`1::factory
	Func_1_t8A740DFC3D657757DA5C263E40CD639547B59CB4 * ___factory_3;

public:
	inline static int32_t get_offset_of_factory_3() { return static_cast<int32_t>(offsetof(MessageParser_1_t422AF617132E681AA126A8C774D49BC4101614D0, ___factory_3)); }
	inline Func_1_t8A740DFC3D657757DA5C263E40CD639547B59CB4 * get_factory_3() const { return ___factory_3; }
	inline Func_1_t8A740DFC3D657757DA5C263E40CD639547B59CB4 ** get_address_of_factory_3() { return &___factory_3; }
	inline void set_factory_3(Func_1_t8A740DFC3D657757DA5C263E40CD639547B59CB4 * value)
	{
		___factory_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___factory_3), (void*)value);
	}
};


// Google.Protobuf.MessageParser`1<Google.Protobuf.Reflection.SourceCodeInfo/Types/Location>
struct MessageParser_1_t09C483F589C454D2B080958F963D017315003983  : public MessageParser_tB0F97A0DCF31AD5D21F69684004C9745C88099FC
{
public:
	// System.Func`1<T> Google.Protobuf.MessageParser`1::factory
	Func_1_t08ED00E63E70686FA636AFAACFA58BD631D35696 * ___factory_3;

public:
	inline static int32_t get_offset_of_factory_3() { return static_cast<int32_t>(offsetof(MessageParser_1_t09C483F589C454D2B080958F963D017315003983, ___factory_3)); }
	inline Func_1_t08ED00E63E70686FA636AFAACFA58BD631D35696 * get_factory_3() const { return ___factory_3; }
	inline Func_1_t08ED00E63E70686FA636AFAACFA58BD631D35696 ** get_address_of_factory_3() { return &___factory_3; }
	inline void set_factory_3(Func_1_t08ED00E63E70686FA636AFAACFA58BD631D35696 * value)
	{
		___factory_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___factory_3), (void*)value);
	}
};


// Google.Protobuf.MessageParser`1<Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart>
struct MessageParser_1_tE61087F0F9CB9EE7534691A864269E589F1453DF  : public MessageParser_tB0F97A0DCF31AD5D21F69684004C9745C88099FC
{
public:
	// System.Func`1<T> Google.Protobuf.MessageParser`1::factory
	Func_1_t732F679FB575BFC105666600B1FC8E28B480BBAB * ___factory_3;

public:
	inline static int32_t get_offset_of_factory_3() { return static_cast<int32_t>(offsetof(MessageParser_1_tE61087F0F9CB9EE7534691A864269E589F1453DF, ___factory_3)); }
	inline Func_1_t732F679FB575BFC105666600B1FC8E28B480BBAB * get_factory_3() const { return ___factory_3; }
	inline Func_1_t732F679FB575BFC105666600B1FC8E28B480BBAB ** get_address_of_factory_3() { return &___factory_3; }
	inline void set_factory_3(Func_1_t732F679FB575BFC105666600B1FC8E28B480BBAB * value)
	{
		___factory_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___factory_3), (void*)value);
	}
};


// System.Nullable`1<System.Int32>
struct Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};


// System.ReadOnlyMemory`1<System.Byte>
struct ReadOnlyMemory_1_tCB124A3C39BEB06AC2DA373EBF5A3BECA0168133 
{
public:
	// System.Object System.ReadOnlyMemory`1::_object
	RuntimeObject * ____object_0;
	// System.Int32 System.ReadOnlyMemory`1::_index
	int32_t ____index_1;
	// System.Int32 System.ReadOnlyMemory`1::_length
	int32_t ____length_2;

public:
	inline static int32_t get_offset_of__object_0() { return static_cast<int32_t>(offsetof(ReadOnlyMemory_1_tCB124A3C39BEB06AC2DA373EBF5A3BECA0168133, ____object_0)); }
	inline RuntimeObject * get__object_0() const { return ____object_0; }
	inline RuntimeObject ** get_address_of__object_0() { return &____object_0; }
	inline void set__object_0(RuntimeObject * value)
	{
		____object_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____object_0), (void*)value);
	}

	inline static int32_t get_offset_of__index_1() { return static_cast<int32_t>(offsetof(ReadOnlyMemory_1_tCB124A3C39BEB06AC2DA373EBF5A3BECA0168133, ____index_1)); }
	inline int32_t get__index_1() const { return ____index_1; }
	inline int32_t* get_address_of__index_1() { return &____index_1; }
	inline void set__index_1(int32_t value)
	{
		____index_1 = value;
	}

	inline static int32_t get_offset_of__length_2() { return static_cast<int32_t>(offsetof(ReadOnlyMemory_1_tCB124A3C39BEB06AC2DA373EBF5A3BECA0168133, ____length_2)); }
	inline int32_t get__length_2() const { return ____length_2; }
	inline int32_t* get_address_of__length_2() { return &____length_2; }
	inline void set__length_2(int32_t value)
	{
		____length_2 = value;
	}
};

// Native definition for P/Invoke marshalling of System.ReadOnlyMemory`1
#ifndef ReadOnlyMemory_1_tAAA9CB78753D364C1C68F98E1112328AEB5C264A_marshaled_pinvoke_define
#define ReadOnlyMemory_1_tAAA9CB78753D364C1C68F98E1112328AEB5C264A_marshaled_pinvoke_define
struct ReadOnlyMemory_1_tAAA9CB78753D364C1C68F98E1112328AEB5C264A_marshaled_pinvoke
{
	Il2CppIUnknown* ____object_0;
	int32_t ____index_1;
	int32_t ____length_2;
};
#endif
// Native definition for COM marshalling of System.ReadOnlyMemory`1
#ifndef ReadOnlyMemory_1_tAAA9CB78753D364C1C68F98E1112328AEB5C264A_marshaled_com_define
#define ReadOnlyMemory_1_tAAA9CB78753D364C1C68F98E1112328AEB5C264A_marshaled_com_define
struct ReadOnlyMemory_1_tAAA9CB78753D364C1C68F98E1112328AEB5C264A_marshaled_com
{
	Il2CppIUnknown* ____object_0;
	int32_t ____index_1;
	int32_t ____length_2;
};
#endif

// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Byte
struct Byte_t0111FAB8B8685667EDDAF77683F0D8F86B659056 
{
public:
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Byte_t0111FAB8B8685667EDDAF77683F0D8F86B659056, ___m_value_0)); }
	inline uint8_t get_m_value_0() const { return ___m_value_0; }
	inline uint8_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint8_t value)
	{
		___m_value_0 = value;
	}
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Reflection.FieldInfo
struct FieldInfo_t  : public MemberInfo_t
{
public:

public:
};


// System.Int32
struct Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// Google.Protobuf.Reflection.MessageDescriptor
struct MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13  : public DescriptorBase_tF6C2BA893A9882D75C7CD171CB21EBA88FA82CEA
{
public:
	// System.Collections.Generic.IList`1<Google.Protobuf.Reflection.FieldDescriptor> Google.Protobuf.Reflection.MessageDescriptor::fieldsInDeclarationOrder
	RuntimeObject* ___fieldsInDeclarationOrder_4;
	// System.Collections.Generic.IList`1<Google.Protobuf.Reflection.FieldDescriptor> Google.Protobuf.Reflection.MessageDescriptor::fieldsInNumberOrder
	RuntimeObject* ___fieldsInNumberOrder_5;
	// System.Collections.Generic.IDictionary`2<System.String,Google.Protobuf.Reflection.FieldDescriptor> Google.Protobuf.Reflection.MessageDescriptor::jsonFieldMap
	RuntimeObject* ___jsonFieldMap_6;
	// Google.Protobuf.Reflection.DescriptorProto Google.Protobuf.Reflection.MessageDescriptor::<Proto>k__BackingField
	DescriptorProto_tB03AF5EFB75071C3E950F726F78461450496C898 * ___U3CProtoU3Ek__BackingField_7;
	// System.Type Google.Protobuf.Reflection.MessageDescriptor::<ClrType>k__BackingField
	Type_t * ___U3CClrTypeU3Ek__BackingField_8;
	// Google.Protobuf.MessageParser Google.Protobuf.Reflection.MessageDescriptor::<Parser>k__BackingField
	MessageParser_tB0F97A0DCF31AD5D21F69684004C9745C88099FC * ___U3CParserU3Ek__BackingField_9;
	// Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.Reflection.MessageDescriptor::<ContainingType>k__BackingField
	MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13 * ___U3CContainingTypeU3Ek__BackingField_10;
	// Google.Protobuf.Reflection.MessageDescriptor/FieldCollection Google.Protobuf.Reflection.MessageDescriptor::<Fields>k__BackingField
	FieldCollection_tF44D33A936EF52997608D546A3213EBE6C88F089 * ___U3CFieldsU3Ek__BackingField_11;
	// Google.Protobuf.Reflection.ExtensionCollection Google.Protobuf.Reflection.MessageDescriptor::<Extensions>k__BackingField
	ExtensionCollection_t52756747D345D5B934DCD6FF9876823214980772 * ___U3CExtensionsU3Ek__BackingField_12;
	// System.Collections.Generic.IList`1<Google.Protobuf.Reflection.MessageDescriptor> Google.Protobuf.Reflection.MessageDescriptor::<NestedTypes>k__BackingField
	RuntimeObject* ___U3CNestedTypesU3Ek__BackingField_13;
	// System.Collections.Generic.IList`1<Google.Protobuf.Reflection.EnumDescriptor> Google.Protobuf.Reflection.MessageDescriptor::<EnumTypes>k__BackingField
	RuntimeObject* ___U3CEnumTypesU3Ek__BackingField_14;
	// System.Collections.Generic.IList`1<Google.Protobuf.Reflection.OneofDescriptor> Google.Protobuf.Reflection.MessageDescriptor::<Oneofs>k__BackingField
	RuntimeObject* ___U3COneofsU3Ek__BackingField_15;
	// System.Int32 Google.Protobuf.Reflection.MessageDescriptor::<RealOneofCount>k__BackingField
	int32_t ___U3CRealOneofCountU3Ek__BackingField_16;

public:
	inline static int32_t get_offset_of_fieldsInDeclarationOrder_4() { return static_cast<int32_t>(offsetof(MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13, ___fieldsInDeclarationOrder_4)); }
	inline RuntimeObject* get_fieldsInDeclarationOrder_4() const { return ___fieldsInDeclarationOrder_4; }
	inline RuntimeObject** get_address_of_fieldsInDeclarationOrder_4() { return &___fieldsInDeclarationOrder_4; }
	inline void set_fieldsInDeclarationOrder_4(RuntimeObject* value)
	{
		___fieldsInDeclarationOrder_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___fieldsInDeclarationOrder_4), (void*)value);
	}

	inline static int32_t get_offset_of_fieldsInNumberOrder_5() { return static_cast<int32_t>(offsetof(MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13, ___fieldsInNumberOrder_5)); }
	inline RuntimeObject* get_fieldsInNumberOrder_5() const { return ___fieldsInNumberOrder_5; }
	inline RuntimeObject** get_address_of_fieldsInNumberOrder_5() { return &___fieldsInNumberOrder_5; }
	inline void set_fieldsInNumberOrder_5(RuntimeObject* value)
	{
		___fieldsInNumberOrder_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___fieldsInNumberOrder_5), (void*)value);
	}

	inline static int32_t get_offset_of_jsonFieldMap_6() { return static_cast<int32_t>(offsetof(MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13, ___jsonFieldMap_6)); }
	inline RuntimeObject* get_jsonFieldMap_6() const { return ___jsonFieldMap_6; }
	inline RuntimeObject** get_address_of_jsonFieldMap_6() { return &___jsonFieldMap_6; }
	inline void set_jsonFieldMap_6(RuntimeObject* value)
	{
		___jsonFieldMap_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___jsonFieldMap_6), (void*)value);
	}

	inline static int32_t get_offset_of_U3CProtoU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13, ___U3CProtoU3Ek__BackingField_7)); }
	inline DescriptorProto_tB03AF5EFB75071C3E950F726F78461450496C898 * get_U3CProtoU3Ek__BackingField_7() const { return ___U3CProtoU3Ek__BackingField_7; }
	inline DescriptorProto_tB03AF5EFB75071C3E950F726F78461450496C898 ** get_address_of_U3CProtoU3Ek__BackingField_7() { return &___U3CProtoU3Ek__BackingField_7; }
	inline void set_U3CProtoU3Ek__BackingField_7(DescriptorProto_tB03AF5EFB75071C3E950F726F78461450496C898 * value)
	{
		___U3CProtoU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CProtoU3Ek__BackingField_7), (void*)value);
	}

	inline static int32_t get_offset_of_U3CClrTypeU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13, ___U3CClrTypeU3Ek__BackingField_8)); }
	inline Type_t * get_U3CClrTypeU3Ek__BackingField_8() const { return ___U3CClrTypeU3Ek__BackingField_8; }
	inline Type_t ** get_address_of_U3CClrTypeU3Ek__BackingField_8() { return &___U3CClrTypeU3Ek__BackingField_8; }
	inline void set_U3CClrTypeU3Ek__BackingField_8(Type_t * value)
	{
		___U3CClrTypeU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CClrTypeU3Ek__BackingField_8), (void*)value);
	}

	inline static int32_t get_offset_of_U3CParserU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13, ___U3CParserU3Ek__BackingField_9)); }
	inline MessageParser_tB0F97A0DCF31AD5D21F69684004C9745C88099FC * get_U3CParserU3Ek__BackingField_9() const { return ___U3CParserU3Ek__BackingField_9; }
	inline MessageParser_tB0F97A0DCF31AD5D21F69684004C9745C88099FC ** get_address_of_U3CParserU3Ek__BackingField_9() { return &___U3CParserU3Ek__BackingField_9; }
	inline void set_U3CParserU3Ek__BackingField_9(MessageParser_tB0F97A0DCF31AD5D21F69684004C9745C88099FC * value)
	{
		___U3CParserU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CParserU3Ek__BackingField_9), (void*)value);
	}

	inline static int32_t get_offset_of_U3CContainingTypeU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13, ___U3CContainingTypeU3Ek__BackingField_10)); }
	inline MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13 * get_U3CContainingTypeU3Ek__BackingField_10() const { return ___U3CContainingTypeU3Ek__BackingField_10; }
	inline MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13 ** get_address_of_U3CContainingTypeU3Ek__BackingField_10() { return &___U3CContainingTypeU3Ek__BackingField_10; }
	inline void set_U3CContainingTypeU3Ek__BackingField_10(MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13 * value)
	{
		___U3CContainingTypeU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CContainingTypeU3Ek__BackingField_10), (void*)value);
	}

	inline static int32_t get_offset_of_U3CFieldsU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13, ___U3CFieldsU3Ek__BackingField_11)); }
	inline FieldCollection_tF44D33A936EF52997608D546A3213EBE6C88F089 * get_U3CFieldsU3Ek__BackingField_11() const { return ___U3CFieldsU3Ek__BackingField_11; }
	inline FieldCollection_tF44D33A936EF52997608D546A3213EBE6C88F089 ** get_address_of_U3CFieldsU3Ek__BackingField_11() { return &___U3CFieldsU3Ek__BackingField_11; }
	inline void set_U3CFieldsU3Ek__BackingField_11(FieldCollection_tF44D33A936EF52997608D546A3213EBE6C88F089 * value)
	{
		___U3CFieldsU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CFieldsU3Ek__BackingField_11), (void*)value);
	}

	inline static int32_t get_offset_of_U3CExtensionsU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13, ___U3CExtensionsU3Ek__BackingField_12)); }
	inline ExtensionCollection_t52756747D345D5B934DCD6FF9876823214980772 * get_U3CExtensionsU3Ek__BackingField_12() const { return ___U3CExtensionsU3Ek__BackingField_12; }
	inline ExtensionCollection_t52756747D345D5B934DCD6FF9876823214980772 ** get_address_of_U3CExtensionsU3Ek__BackingField_12() { return &___U3CExtensionsU3Ek__BackingField_12; }
	inline void set_U3CExtensionsU3Ek__BackingField_12(ExtensionCollection_t52756747D345D5B934DCD6FF9876823214980772 * value)
	{
		___U3CExtensionsU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CExtensionsU3Ek__BackingField_12), (void*)value);
	}

	inline static int32_t get_offset_of_U3CNestedTypesU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13, ___U3CNestedTypesU3Ek__BackingField_13)); }
	inline RuntimeObject* get_U3CNestedTypesU3Ek__BackingField_13() const { return ___U3CNestedTypesU3Ek__BackingField_13; }
	inline RuntimeObject** get_address_of_U3CNestedTypesU3Ek__BackingField_13() { return &___U3CNestedTypesU3Ek__BackingField_13; }
	inline void set_U3CNestedTypesU3Ek__BackingField_13(RuntimeObject* value)
	{
		___U3CNestedTypesU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CNestedTypesU3Ek__BackingField_13), (void*)value);
	}

	inline static int32_t get_offset_of_U3CEnumTypesU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13, ___U3CEnumTypesU3Ek__BackingField_14)); }
	inline RuntimeObject* get_U3CEnumTypesU3Ek__BackingField_14() const { return ___U3CEnumTypesU3Ek__BackingField_14; }
	inline RuntimeObject** get_address_of_U3CEnumTypesU3Ek__BackingField_14() { return &___U3CEnumTypesU3Ek__BackingField_14; }
	inline void set_U3CEnumTypesU3Ek__BackingField_14(RuntimeObject* value)
	{
		___U3CEnumTypesU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CEnumTypesU3Ek__BackingField_14), (void*)value);
	}

	inline static int32_t get_offset_of_U3COneofsU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13, ___U3COneofsU3Ek__BackingField_15)); }
	inline RuntimeObject* get_U3COneofsU3Ek__BackingField_15() const { return ___U3COneofsU3Ek__BackingField_15; }
	inline RuntimeObject** get_address_of_U3COneofsU3Ek__BackingField_15() { return &___U3COneofsU3Ek__BackingField_15; }
	inline void set_U3COneofsU3Ek__BackingField_15(RuntimeObject* value)
	{
		___U3COneofsU3Ek__BackingField_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3COneofsU3Ek__BackingField_15), (void*)value);
	}

	inline static int32_t get_offset_of_U3CRealOneofCountU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13, ___U3CRealOneofCountU3Ek__BackingField_16)); }
	inline int32_t get_U3CRealOneofCountU3Ek__BackingField_16() const { return ___U3CRealOneofCountU3Ek__BackingField_16; }
	inline int32_t* get_address_of_U3CRealOneofCountU3Ek__BackingField_16() { return &___U3CRealOneofCountU3Ek__BackingField_16; }
	inline void set_U3CRealOneofCountU3Ek__BackingField_16(int32_t value)
	{
		___U3CRealOneofCountU3Ek__BackingField_16 = value;
	}
};

struct MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13_StaticFields
{
public:
	// System.Collections.Generic.HashSet`1<System.String> Google.Protobuf.Reflection.MessageDescriptor::WellKnownTypeNames
	HashSet_1_t45F75268054D01D9E70EB33D7F6D2FA609DB9229 * ___WellKnownTypeNames_3;

public:
	inline static int32_t get_offset_of_WellKnownTypeNames_3() { return static_cast<int32_t>(offsetof(MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13_StaticFields, ___WellKnownTypeNames_3)); }
	inline HashSet_1_t45F75268054D01D9E70EB33D7F6D2FA609DB9229 * get_WellKnownTypeNames_3() const { return ___WellKnownTypeNames_3; }
	inline HashSet_1_t45F75268054D01D9E70EB33D7F6D2FA609DB9229 ** get_address_of_WellKnownTypeNames_3() { return &___WellKnownTypeNames_3; }
	inline void set_WellKnownTypeNames_3(HashSet_1_t45F75268054D01D9E70EB33D7F6D2FA609DB9229 * value)
	{
		___WellKnownTypeNames_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___WellKnownTypeNames_3), (void*)value);
	}
};


// Google.Protobuf.Reflection.OriginalNameAttribute
struct OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String Google.Protobuf.Reflection.OriginalNameAttribute::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;
	// System.Boolean Google.Protobuf.Reflection.OriginalNameAttribute::<PreferredAlias>k__BackingField
	bool ___U3CPreferredAliasU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664, ___U3CNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CNameU3Ek__BackingField_0() const { return ___U3CNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_0() { return &___U3CNameU3Ek__BackingField_0; }
	inline void set_U3CNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CNameU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CPreferredAliasU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664, ___U3CPreferredAliasU3Ek__BackingField_1)); }
	inline bool get_U3CPreferredAliasU3Ek__BackingField_1() const { return ___U3CPreferredAliasU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CPreferredAliasU3Ek__BackingField_1() { return &___U3CPreferredAliasU3Ek__BackingField_1; }
	inline void set_U3CPreferredAliasU3Ek__BackingField_1(bool value)
	{
		___U3CPreferredAliasU3Ek__BackingField_1 = value;
	}
};


// System.SequencePosition
struct SequencePosition_t60FFA8E61B4E8F902C0B0F1F6DC4494561396F2A 
{
public:
	// System.Object System.SequencePosition::_object
	RuntimeObject * ____object_0;
	// System.Int32 System.SequencePosition::_integer
	int32_t ____integer_1;

public:
	inline static int32_t get_offset_of__object_0() { return static_cast<int32_t>(offsetof(SequencePosition_t60FFA8E61B4E8F902C0B0F1F6DC4494561396F2A, ____object_0)); }
	inline RuntimeObject * get__object_0() const { return ____object_0; }
	inline RuntimeObject ** get_address_of__object_0() { return &____object_0; }
	inline void set__object_0(RuntimeObject * value)
	{
		____object_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____object_0), (void*)value);
	}

	inline static int32_t get_offset_of__integer_1() { return static_cast<int32_t>(offsetof(SequencePosition_t60FFA8E61B4E8F902C0B0F1F6DC4494561396F2A, ____integer_1)); }
	inline int32_t get__integer_1() const { return ____integer_1; }
	inline int32_t* get_address_of__integer_1() { return &____integer_1; }
	inline void set__integer_1(int32_t value)
	{
		____integer_1 = value;
	}
};

// Native definition for P/Invoke marshalling of System.SequencePosition
struct SequencePosition_t60FFA8E61B4E8F902C0B0F1F6DC4494561396F2A_marshaled_pinvoke
{
	Il2CppIUnknown* ____object_0;
	int32_t ____integer_1;
};
// Native definition for COM marshalling of System.SequencePosition
struct SequencePosition_t60FFA8E61B4E8F902C0B0F1F6DC4494561396F2A_marshaled_com
{
	Il2CppIUnknown* ____object_0;
	int32_t ____integer_1;
};

// System.UInt32
struct UInt32_tE60352A06233E4E69DD198BCC67142159F686B15 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt32_tE60352A06233E4E69DD198BCC67142159F686B15, ___m_value_0)); }
	inline uint32_t get_m_value_0() const { return ___m_value_0; }
	inline uint32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint32_t value)
	{
		___m_value_0 = value;
	}
};


// System.UInt64
struct UInt64_tEC57511B3E3CA2DBA1BEBD434C6983E31C943281 
{
public:
	// System.UInt64 System.UInt64::m_value
	uint64_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt64_tEC57511B3E3CA2DBA1BEBD434C6983E31C943281, ___m_value_0)); }
	inline uint64_t get_m_value_0() const { return ___m_value_0; }
	inline uint64_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint64_t value)
	{
		___m_value_0 = value;
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// Google.Protobuf.WriteBufferHelper
struct WriteBufferHelper_t4B2EAF143D71ACC9E773C2F04F1826343EFDCB1A 
{
public:
	// System.Buffers.IBufferWriter`1<System.Byte> Google.Protobuf.WriteBufferHelper::bufferWriter
	RuntimeObject* ___bufferWriter_0;
	// Google.Protobuf.CodedOutputStream Google.Protobuf.WriteBufferHelper::codedOutputStream
	CodedOutputStream_t2871B0D17F920BFB54B564E2295D9753721642E6 * ___codedOutputStream_1;

public:
	inline static int32_t get_offset_of_bufferWriter_0() { return static_cast<int32_t>(offsetof(WriteBufferHelper_t4B2EAF143D71ACC9E773C2F04F1826343EFDCB1A, ___bufferWriter_0)); }
	inline RuntimeObject* get_bufferWriter_0() const { return ___bufferWriter_0; }
	inline RuntimeObject** get_address_of_bufferWriter_0() { return &___bufferWriter_0; }
	inline void set_bufferWriter_0(RuntimeObject* value)
	{
		___bufferWriter_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___bufferWriter_0), (void*)value);
	}

	inline static int32_t get_offset_of_codedOutputStream_1() { return static_cast<int32_t>(offsetof(WriteBufferHelper_t4B2EAF143D71ACC9E773C2F04F1826343EFDCB1A, ___codedOutputStream_1)); }
	inline CodedOutputStream_t2871B0D17F920BFB54B564E2295D9753721642E6 * get_codedOutputStream_1() const { return ___codedOutputStream_1; }
	inline CodedOutputStream_t2871B0D17F920BFB54B564E2295D9753721642E6 ** get_address_of_codedOutputStream_1() { return &___codedOutputStream_1; }
	inline void set_codedOutputStream_1(CodedOutputStream_t2871B0D17F920BFB54B564E2295D9753721642E6 * value)
	{
		___codedOutputStream_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___codedOutputStream_1), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of Google.Protobuf.WriteBufferHelper
struct WriteBufferHelper_t4B2EAF143D71ACC9E773C2F04F1826343EFDCB1A_marshaled_pinvoke
{
	RuntimeObject* ___bufferWriter_0;
	CodedOutputStream_t2871B0D17F920BFB54B564E2295D9753721642E6 * ___codedOutputStream_1;
};
// Native definition for COM marshalling of Google.Protobuf.WriteBufferHelper
struct WriteBufferHelper_t4B2EAF143D71ACC9E773C2F04F1826343EFDCB1A_marshaled_com
{
	RuntimeObject* ___bufferWriter_0;
	CodedOutputStream_t2871B0D17F920BFB54B564E2295D9753721642E6 * ___codedOutputStream_1;
};

// System.Buffers.ReadOnlySequence`1<System.Byte>
struct ReadOnlySequence_1_t189383CAD339D18716638372580ECEAA6265A8CE 
{
public:
	// System.SequencePosition System.Buffers.ReadOnlySequence`1::_sequenceStart
	SequencePosition_t60FFA8E61B4E8F902C0B0F1F6DC4494561396F2A  ____sequenceStart_0;
	// System.SequencePosition System.Buffers.ReadOnlySequence`1::_sequenceEnd
	SequencePosition_t60FFA8E61B4E8F902C0B0F1F6DC4494561396F2A  ____sequenceEnd_1;

public:
	inline static int32_t get_offset_of__sequenceStart_0() { return static_cast<int32_t>(offsetof(ReadOnlySequence_1_t189383CAD339D18716638372580ECEAA6265A8CE, ____sequenceStart_0)); }
	inline SequencePosition_t60FFA8E61B4E8F902C0B0F1F6DC4494561396F2A  get__sequenceStart_0() const { return ____sequenceStart_0; }
	inline SequencePosition_t60FFA8E61B4E8F902C0B0F1F6DC4494561396F2A * get_address_of__sequenceStart_0() { return &____sequenceStart_0; }
	inline void set__sequenceStart_0(SequencePosition_t60FFA8E61B4E8F902C0B0F1F6DC4494561396F2A  value)
	{
		____sequenceStart_0 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&____sequenceStart_0))->____object_0), (void*)NULL);
	}

	inline static int32_t get_offset_of__sequenceEnd_1() { return static_cast<int32_t>(offsetof(ReadOnlySequence_1_t189383CAD339D18716638372580ECEAA6265A8CE, ____sequenceEnd_1)); }
	inline SequencePosition_t60FFA8E61B4E8F902C0B0F1F6DC4494561396F2A  get__sequenceEnd_1() const { return ____sequenceEnd_1; }
	inline SequencePosition_t60FFA8E61B4E8F902C0B0F1F6DC4494561396F2A * get_address_of__sequenceEnd_1() { return &____sequenceEnd_1; }
	inline void set__sequenceEnd_1(SequencePosition_t60FFA8E61B4E8F902C0B0F1F6DC4494561396F2A  value)
	{
		____sequenceEnd_1 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&____sequenceEnd_1))->____object_0), (void*)NULL);
	}
};

struct ReadOnlySequence_1_t189383CAD339D18716638372580ECEAA6265A8CE_StaticFields
{
public:
	// System.Buffers.ReadOnlySequence`1<T> System.Buffers.ReadOnlySequence`1::Empty
	ReadOnlySequence_1_t189383CAD339D18716638372580ECEAA6265A8CE  ___Empty_2;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(ReadOnlySequence_1_t189383CAD339D18716638372580ECEAA6265A8CE_StaticFields, ___Empty_2)); }
	inline ReadOnlySequence_1_t189383CAD339D18716638372580ECEAA6265A8CE  get_Empty_2() const { return ___Empty_2; }
	inline ReadOnlySequence_1_t189383CAD339D18716638372580ECEAA6265A8CE * get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(ReadOnlySequence_1_t189383CAD339D18716638372580ECEAA6265A8CE  value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___Empty_2))->____sequenceStart_0))->____object_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___Empty_2))->____sequenceEnd_1))->____object_0), (void*)NULL);
		#endif
	}
};


// System.ReadOnlySpan`1<System.Byte>
struct ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 
{
public:
	// System.Pinnable`1<T> System.ReadOnlySpan`1::_pinnable
	Pinnable_1_t3FA89DC88CD7499604577377AB0B618C80108110 * ____pinnable_0;
	// System.IntPtr System.ReadOnlySpan`1::_byteOffset
	intptr_t ____byteOffset_1;
	// System.Int32 System.ReadOnlySpan`1::_length
	int32_t ____length_2;

public:
	inline static int32_t get_offset_of__pinnable_0() { return static_cast<int32_t>(offsetof(ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726, ____pinnable_0)); }
	inline Pinnable_1_t3FA89DC88CD7499604577377AB0B618C80108110 * get__pinnable_0() const { return ____pinnable_0; }
	inline Pinnable_1_t3FA89DC88CD7499604577377AB0B618C80108110 ** get_address_of__pinnable_0() { return &____pinnable_0; }
	inline void set__pinnable_0(Pinnable_1_t3FA89DC88CD7499604577377AB0B618C80108110 * value)
	{
		____pinnable_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____pinnable_0), (void*)value);
	}

	inline static int32_t get_offset_of__byteOffset_1() { return static_cast<int32_t>(offsetof(ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726, ____byteOffset_1)); }
	inline intptr_t get__byteOffset_1() const { return ____byteOffset_1; }
	inline intptr_t* get_address_of__byteOffset_1() { return &____byteOffset_1; }
	inline void set__byteOffset_1(intptr_t value)
	{
		____byteOffset_1 = value;
	}

	inline static int32_t get_offset_of__length_2() { return static_cast<int32_t>(offsetof(ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726, ____length_2)); }
	inline int32_t get__length_2() const { return ____length_2; }
	inline int32_t* get_address_of__length_2() { return &____length_2; }
	inline void set__length_2(int32_t value)
	{
		____length_2 = value;
	}
};


// System.Span`1<System.Byte>
struct Span_1_tA2AD9FB303A0BA36B1129534451F6EAE74DFEA83 
{
public:
	// System.Pinnable`1<T> System.Span`1::_pinnable
	Pinnable_1_t3FA89DC88CD7499604577377AB0B618C80108110 * ____pinnable_0;
	// System.IntPtr System.Span`1::_byteOffset
	intptr_t ____byteOffset_1;
	// System.Int32 System.Span`1::_length
	int32_t ____length_2;

public:
	inline static int32_t get_offset_of__pinnable_0() { return static_cast<int32_t>(offsetof(Span_1_tA2AD9FB303A0BA36B1129534451F6EAE74DFEA83, ____pinnable_0)); }
	inline Pinnable_1_t3FA89DC88CD7499604577377AB0B618C80108110 * get__pinnable_0() const { return ____pinnable_0; }
	inline Pinnable_1_t3FA89DC88CD7499604577377AB0B618C80108110 ** get_address_of__pinnable_0() { return &____pinnable_0; }
	inline void set__pinnable_0(Pinnable_1_t3FA89DC88CD7499604577377AB0B618C80108110 * value)
	{
		____pinnable_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____pinnable_0), (void*)value);
	}

	inline static int32_t get_offset_of__byteOffset_1() { return static_cast<int32_t>(offsetof(Span_1_tA2AD9FB303A0BA36B1129534451F6EAE74DFEA83, ____byteOffset_1)); }
	inline intptr_t get__byteOffset_1() const { return ____byteOffset_1; }
	inline intptr_t* get_address_of__byteOffset_1() { return &____byteOffset_1; }
	inline void set__byteOffset_1(intptr_t value)
	{
		____byteOffset_1 = value;
	}

	inline static int32_t get_offset_of__length_2() { return static_cast<int32_t>(offsetof(Span_1_tA2AD9FB303A0BA36B1129534451F6EAE74DFEA83, ____length_2)); }
	inline int32_t get__length_2() const { return ____length_2; }
	inline int32_t* get_address_of__length_2() { return &____length_2; }
	inline void set__length_2(int32_t value)
	{
		____length_2 = value;
	}
};


// System.Delegate
struct Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_target_2), (void*)value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___method_info_7), (void*)value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___original_method_info_8), (void*)value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * get_data_9() const { return ___data_9; }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_9), (void*)value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};

// Google.Protobuf.WriterInternalState
struct WriterInternalState_t8EA46E8D5E8D65BC3D89ECA536512195F33B71E3 
{
public:
	// System.Int32 Google.Protobuf.WriterInternalState::limit
	int32_t ___limit_0;
	// System.Int32 Google.Protobuf.WriterInternalState::position
	int32_t ___position_1;
	// Google.Protobuf.WriteBufferHelper Google.Protobuf.WriterInternalState::writeBufferHelper
	WriteBufferHelper_t4B2EAF143D71ACC9E773C2F04F1826343EFDCB1A  ___writeBufferHelper_2;

public:
	inline static int32_t get_offset_of_limit_0() { return static_cast<int32_t>(offsetof(WriterInternalState_t8EA46E8D5E8D65BC3D89ECA536512195F33B71E3, ___limit_0)); }
	inline int32_t get_limit_0() const { return ___limit_0; }
	inline int32_t* get_address_of_limit_0() { return &___limit_0; }
	inline void set_limit_0(int32_t value)
	{
		___limit_0 = value;
	}

	inline static int32_t get_offset_of_position_1() { return static_cast<int32_t>(offsetof(WriterInternalState_t8EA46E8D5E8D65BC3D89ECA536512195F33B71E3, ___position_1)); }
	inline int32_t get_position_1() const { return ___position_1; }
	inline int32_t* get_address_of_position_1() { return &___position_1; }
	inline void set_position_1(int32_t value)
	{
		___position_1 = value;
	}

	inline static int32_t get_offset_of_writeBufferHelper_2() { return static_cast<int32_t>(offsetof(WriterInternalState_t8EA46E8D5E8D65BC3D89ECA536512195F33B71E3, ___writeBufferHelper_2)); }
	inline WriteBufferHelper_t4B2EAF143D71ACC9E773C2F04F1826343EFDCB1A  get_writeBufferHelper_2() const { return ___writeBufferHelper_2; }
	inline WriteBufferHelper_t4B2EAF143D71ACC9E773C2F04F1826343EFDCB1A * get_address_of_writeBufferHelper_2() { return &___writeBufferHelper_2; }
	inline void set_writeBufferHelper_2(WriteBufferHelper_t4B2EAF143D71ACC9E773C2F04F1826343EFDCB1A  value)
	{
		___writeBufferHelper_2 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___writeBufferHelper_2))->___bufferWriter_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___writeBufferHelper_2))->___codedOutputStream_1), (void*)NULL);
		#endif
	}
};

// Native definition for P/Invoke marshalling of Google.Protobuf.WriterInternalState
struct WriterInternalState_t8EA46E8D5E8D65BC3D89ECA536512195F33B71E3_marshaled_pinvoke
{
	int32_t ___limit_0;
	int32_t ___position_1;
	WriteBufferHelper_t4B2EAF143D71ACC9E773C2F04F1826343EFDCB1A_marshaled_pinvoke ___writeBufferHelper_2;
};
// Native definition for COM marshalling of Google.Protobuf.WriterInternalState
struct WriterInternalState_t8EA46E8D5E8D65BC3D89ECA536512195F33B71E3_marshaled_com
{
	int32_t ___limit_0;
	int32_t ___position_1;
	WriteBufferHelper_t4B2EAF143D71ACC9E773C2F04F1826343EFDCB1A_marshaled_com ___writeBufferHelper_2;
};

// Google.Protobuf.WellKnownTypes.Field/Types/Cardinality
struct Cardinality_tFEB23F7BB6AE0122DE5E13D7756F940FD86759F0 
{
public:
	// System.Int32 Google.Protobuf.WellKnownTypes.Field/Types/Cardinality::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Cardinality_tFEB23F7BB6AE0122DE5E13D7756F940FD86759F0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Google.Protobuf.WellKnownTypes.Field/Types/Kind
struct Kind_tC27F5FC567168FED3D17ACA8C7E54C5769A4D532 
{
public:
	// System.Int32 Google.Protobuf.WellKnownTypes.Field/Types/Kind::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Kind_tC27F5FC567168FED3D17ACA8C7E54C5769A4D532, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Google.Protobuf.Reflection.FieldDescriptorProto/Types/Label
struct Label_t5F834D524D61512BADE59642DE16426A43F3A2F7 
{
public:
	// System.Int32 Google.Protobuf.Reflection.FieldDescriptorProto/Types/Label::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Label_t5F834D524D61512BADE59642DE16426A43F3A2F7, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Google.Protobuf.Reflection.FieldDescriptorProto/Types/Type
struct Type_tA2574C38CEE89946C1AFA608D4182429BB8490A9 
{
public:
	// System.Int32 Google.Protobuf.Reflection.FieldDescriptorProto/Types/Type::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Type_tA2574C38CEE89946C1AFA608D4182429BB8490A9, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Google.Protobuf.Reflection.FieldOptions/Types/CType
struct CType_t8692E658D4DB9762025F3FF59D004E25835CF4FB 
{
public:
	// System.Int32 Google.Protobuf.Reflection.FieldOptions/Types/CType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CType_t8692E658D4DB9762025F3FF59D004E25835CF4FB, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Google.Protobuf.Reflection.FieldOptions/Types/JSType
struct JSType_t6C0B15B541F17DFCB78C389DE391B514E6BCD78B 
{
public:
	// System.Int32 Google.Protobuf.Reflection.FieldOptions/Types/JSType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(JSType_t6C0B15B541F17DFCB78C389DE391B514E6BCD78B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Google.Protobuf.Reflection.FileOptions/Types/OptimizeMode
struct OptimizeMode_tD2A7B67F01AF2A53A5C73E35E3B0AD381FBEAF90 
{
public:
	// System.Int32 Google.Protobuf.Reflection.FileOptions/Types/OptimizeMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(OptimizeMode_tD2A7B67F01AF2A53A5C73E35E3B0AD381FBEAF90, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Google.Protobuf.Reflection.MethodOptions/Types/IdempotencyLevel
struct IdempotencyLevel_tF1CB740909022AF43C9AA8E9B0E20AF1D38649C3 
{
public:
	// System.Int32 Google.Protobuf.Reflection.MethodOptions/Types/IdempotencyLevel::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(IdempotencyLevel_tF1CB740909022AF43C9AA8E9B0E20AF1D38649C3, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Buffers.ReadOnlySequence`1/Enumerator<System.Byte>
struct Enumerator_t4820F5B6D236EC8EF539F8F899F170D4F6B708F4 
{
public:
	// System.Buffers.ReadOnlySequence`1<T> System.Buffers.ReadOnlySequence`1/Enumerator::_sequence
	ReadOnlySequence_1_t189383CAD339D18716638372580ECEAA6265A8CE  ____sequence_0;
	// System.SequencePosition System.Buffers.ReadOnlySequence`1/Enumerator::_next
	SequencePosition_t60FFA8E61B4E8F902C0B0F1F6DC4494561396F2A  ____next_1;
	// System.ReadOnlyMemory`1<T> System.Buffers.ReadOnlySequence`1/Enumerator::_currentMemory
	ReadOnlyMemory_1_tCB124A3C39BEB06AC2DA373EBF5A3BECA0168133  ____currentMemory_2;

public:
	inline static int32_t get_offset_of__sequence_0() { return static_cast<int32_t>(offsetof(Enumerator_t4820F5B6D236EC8EF539F8F899F170D4F6B708F4, ____sequence_0)); }
	inline ReadOnlySequence_1_t189383CAD339D18716638372580ECEAA6265A8CE  get__sequence_0() const { return ____sequence_0; }
	inline ReadOnlySequence_1_t189383CAD339D18716638372580ECEAA6265A8CE * get_address_of__sequence_0() { return &____sequence_0; }
	inline void set__sequence_0(ReadOnlySequence_1_t189383CAD339D18716638372580ECEAA6265A8CE  value)
	{
		____sequence_0 = value;
		Il2CppCodeGenWriteBarrier((void**)&((&(((&____sequence_0))->____sequenceStart_0))->____object_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&____sequence_0))->____sequenceEnd_1))->____object_0), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of__next_1() { return static_cast<int32_t>(offsetof(Enumerator_t4820F5B6D236EC8EF539F8F899F170D4F6B708F4, ____next_1)); }
	inline SequencePosition_t60FFA8E61B4E8F902C0B0F1F6DC4494561396F2A  get__next_1() const { return ____next_1; }
	inline SequencePosition_t60FFA8E61B4E8F902C0B0F1F6DC4494561396F2A * get_address_of__next_1() { return &____next_1; }
	inline void set__next_1(SequencePosition_t60FFA8E61B4E8F902C0B0F1F6DC4494561396F2A  value)
	{
		____next_1 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&____next_1))->____object_0), (void*)NULL);
	}

	inline static int32_t get_offset_of__currentMemory_2() { return static_cast<int32_t>(offsetof(Enumerator_t4820F5B6D236EC8EF539F8F899F170D4F6B708F4, ____currentMemory_2)); }
	inline ReadOnlyMemory_1_tCB124A3C39BEB06AC2DA373EBF5A3BECA0168133  get__currentMemory_2() const { return ____currentMemory_2; }
	inline ReadOnlyMemory_1_tCB124A3C39BEB06AC2DA373EBF5A3BECA0168133 * get_address_of__currentMemory_2() { return &____currentMemory_2; }
	inline void set__currentMemory_2(ReadOnlyMemory_1_tCB124A3C39BEB06AC2DA373EBF5A3BECA0168133  value)
	{
		____currentMemory_2 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&____currentMemory_2))->____object_0), (void*)NULL);
	}
};


// Google.Protobuf.CodedOutputStream
struct CodedOutputStream_t2871B0D17F920BFB54B564E2295D9753721642E6  : public RuntimeObject
{
public:
	// System.Boolean Google.Protobuf.CodedOutputStream::leaveOpen
	bool ___leaveOpen_1;
	// System.Byte[] Google.Protobuf.CodedOutputStream::buffer
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___buffer_2;
	// Google.Protobuf.WriterInternalState Google.Protobuf.CodedOutputStream::state
	WriterInternalState_t8EA46E8D5E8D65BC3D89ECA536512195F33B71E3  ___state_3;
	// System.IO.Stream Google.Protobuf.CodedOutputStream::output
	Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * ___output_4;

public:
	inline static int32_t get_offset_of_leaveOpen_1() { return static_cast<int32_t>(offsetof(CodedOutputStream_t2871B0D17F920BFB54B564E2295D9753721642E6, ___leaveOpen_1)); }
	inline bool get_leaveOpen_1() const { return ___leaveOpen_1; }
	inline bool* get_address_of_leaveOpen_1() { return &___leaveOpen_1; }
	inline void set_leaveOpen_1(bool value)
	{
		___leaveOpen_1 = value;
	}

	inline static int32_t get_offset_of_buffer_2() { return static_cast<int32_t>(offsetof(CodedOutputStream_t2871B0D17F920BFB54B564E2295D9753721642E6, ___buffer_2)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_buffer_2() const { return ___buffer_2; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_buffer_2() { return &___buffer_2; }
	inline void set_buffer_2(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___buffer_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___buffer_2), (void*)value);
	}

	inline static int32_t get_offset_of_state_3() { return static_cast<int32_t>(offsetof(CodedOutputStream_t2871B0D17F920BFB54B564E2295D9753721642E6, ___state_3)); }
	inline WriterInternalState_t8EA46E8D5E8D65BC3D89ECA536512195F33B71E3  get_state_3() const { return ___state_3; }
	inline WriterInternalState_t8EA46E8D5E8D65BC3D89ECA536512195F33B71E3 * get_address_of_state_3() { return &___state_3; }
	inline void set_state_3(WriterInternalState_t8EA46E8D5E8D65BC3D89ECA536512195F33B71E3  value)
	{
		___state_3 = value;
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___state_3))->___writeBufferHelper_2))->___bufferWriter_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___state_3))->___writeBufferHelper_2))->___codedOutputStream_1), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_output_4() { return static_cast<int32_t>(offsetof(CodedOutputStream_t2871B0D17F920BFB54B564E2295D9753721642E6, ___output_4)); }
	inline Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * get_output_4() const { return ___output_4; }
	inline Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB ** get_address_of_output_4() { return &___output_4; }
	inline void set_output_4(Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * value)
	{
		___output_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___output_4), (void*)value);
	}
};

struct CodedOutputStream_t2871B0D17F920BFB54B564E2295D9753721642E6_StaticFields
{
public:
	// System.Int32 Google.Protobuf.CodedOutputStream::DefaultBufferSize
	int32_t ___DefaultBufferSize_0;

public:
	inline static int32_t get_offset_of_DefaultBufferSize_0() { return static_cast<int32_t>(offsetof(CodedOutputStream_t2871B0D17F920BFB54B564E2295D9753721642E6_StaticFields, ___DefaultBufferSize_0)); }
	inline int32_t get_DefaultBufferSize_0() const { return ___DefaultBufferSize_0; }
	inline int32_t* get_address_of_DefaultBufferSize_0() { return &___DefaultBufferSize_0; }
	inline void set_DefaultBufferSize_0(int32_t value)
	{
		___DefaultBufferSize_0 = value;
	}
};


// System.MulticastDelegate
struct MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___delegates_11), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};

// Google.Protobuf.WriteContext
struct WriteContext_t4011B33FF5B289B23B81D9929C82A44F0A24DFCB 
{
public:
	// System.Span`1<System.Byte> Google.Protobuf.WriteContext::buffer
	Span_1_tA2AD9FB303A0BA36B1129534451F6EAE74DFEA83  ___buffer_0;
	// Google.Protobuf.WriterInternalState Google.Protobuf.WriteContext::state
	WriterInternalState_t8EA46E8D5E8D65BC3D89ECA536512195F33B71E3  ___state_1;

public:
	inline static int32_t get_offset_of_buffer_0() { return static_cast<int32_t>(offsetof(WriteContext_t4011B33FF5B289B23B81D9929C82A44F0A24DFCB, ___buffer_0)); }
	inline Span_1_tA2AD9FB303A0BA36B1129534451F6EAE74DFEA83  get_buffer_0() const { return ___buffer_0; }
	inline Span_1_tA2AD9FB303A0BA36B1129534451F6EAE74DFEA83 * get_address_of_buffer_0() { return &___buffer_0; }
	inline void set_buffer_0(Span_1_tA2AD9FB303A0BA36B1129534451F6EAE74DFEA83  value)
	{
		___buffer_0 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___buffer_0))->____pinnable_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_state_1() { return static_cast<int32_t>(offsetof(WriteContext_t4011B33FF5B289B23B81D9929C82A44F0A24DFCB, ___state_1)); }
	inline WriterInternalState_t8EA46E8D5E8D65BC3D89ECA536512195F33B71E3  get_state_1() const { return ___state_1; }
	inline WriterInternalState_t8EA46E8D5E8D65BC3D89ECA536512195F33B71E3 * get_address_of_state_1() { return &___state_1; }
	inline void set_state_1(WriterInternalState_t8EA46E8D5E8D65BC3D89ECA536512195F33B71E3  value)
	{
		___state_1 = value;
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___state_1))->___writeBufferHelper_2))->___bufferWriter_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___state_1))->___writeBufferHelper_2))->___codedOutputStream_1), (void*)NULL);
		#endif
	}
};

// Native definition for P/Invoke marshalling of Google.Protobuf.WriteContext
struct WriteContext_t4011B33FF5B289B23B81D9929C82A44F0A24DFCB_marshaled_pinvoke
{
	Span_1_tA2AD9FB303A0BA36B1129534451F6EAE74DFEA83  ___buffer_0;
	WriterInternalState_t8EA46E8D5E8D65BC3D89ECA536512195F33B71E3_marshaled_pinvoke ___state_1;
};
// Native definition for COM marshalling of Google.Protobuf.WriteContext
struct WriteContext_t4011B33FF5B289B23B81D9929C82A44F0A24DFCB_marshaled_com
{
	Span_1_tA2AD9FB303A0BA36B1129534451F6EAE74DFEA83  ___buffer_0;
	WriterInternalState_t8EA46E8D5E8D65BC3D89ECA536512195F33B71E3_marshaled_com ___state_1;
};

// System.Func`1<Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange>
struct Func_1_tB4DEFF82E390DE2B16A177BF95E2A660E016FC83  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`1<Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation>
struct Func_1_t8A740DFC3D657757DA5C263E40CD639547B59CB4  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`1<Google.Protobuf.Reflection.SourceCodeInfo/Types/Location>
struct Func_1_t08ED00E63E70686FA636AFAACFA58BD631D35696  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`1<Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart>
struct Func_1_t732F679FB575BFC105666600B1FC8E28B480BBAB  : public MulticastDelegate_t
{
public:

public:
};


// Google.Protobuf.SegmentedBufferHelper
struct SegmentedBufferHelper_t885D05BD8C8563F4D75A82B6FB4CE2F009C8B005 
{
public:
	// System.Nullable`1<System.Int32> Google.Protobuf.SegmentedBufferHelper::totalLength
	Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103  ___totalLength_0;
	// System.Buffers.ReadOnlySequence`1/Enumerator<System.Byte> Google.Protobuf.SegmentedBufferHelper::readOnlySequenceEnumerator
	Enumerator_t4820F5B6D236EC8EF539F8F899F170D4F6B708F4  ___readOnlySequenceEnumerator_1;
	// Google.Protobuf.CodedInputStream Google.Protobuf.SegmentedBufferHelper::codedInputStream
	CodedInputStream_tC76F9515605395C11D7F9437E0E06C828232B0C8 * ___codedInputStream_2;

public:
	inline static int32_t get_offset_of_totalLength_0() { return static_cast<int32_t>(offsetof(SegmentedBufferHelper_t885D05BD8C8563F4D75A82B6FB4CE2F009C8B005, ___totalLength_0)); }
	inline Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103  get_totalLength_0() const { return ___totalLength_0; }
	inline Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103 * get_address_of_totalLength_0() { return &___totalLength_0; }
	inline void set_totalLength_0(Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103  value)
	{
		___totalLength_0 = value;
	}

	inline static int32_t get_offset_of_readOnlySequenceEnumerator_1() { return static_cast<int32_t>(offsetof(SegmentedBufferHelper_t885D05BD8C8563F4D75A82B6FB4CE2F009C8B005, ___readOnlySequenceEnumerator_1)); }
	inline Enumerator_t4820F5B6D236EC8EF539F8F899F170D4F6B708F4  get_readOnlySequenceEnumerator_1() const { return ___readOnlySequenceEnumerator_1; }
	inline Enumerator_t4820F5B6D236EC8EF539F8F899F170D4F6B708F4 * get_address_of_readOnlySequenceEnumerator_1() { return &___readOnlySequenceEnumerator_1; }
	inline void set_readOnlySequenceEnumerator_1(Enumerator_t4820F5B6D236EC8EF539F8F899F170D4F6B708F4  value)
	{
		___readOnlySequenceEnumerator_1 = value;
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___readOnlySequenceEnumerator_1))->____sequence_0))->____sequenceStart_0))->____object_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___readOnlySequenceEnumerator_1))->____sequence_0))->____sequenceEnd_1))->____object_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___readOnlySequenceEnumerator_1))->____next_1))->____object_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___readOnlySequenceEnumerator_1))->____currentMemory_2))->____object_0), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_codedInputStream_2() { return static_cast<int32_t>(offsetof(SegmentedBufferHelper_t885D05BD8C8563F4D75A82B6FB4CE2F009C8B005, ___codedInputStream_2)); }
	inline CodedInputStream_tC76F9515605395C11D7F9437E0E06C828232B0C8 * get_codedInputStream_2() const { return ___codedInputStream_2; }
	inline CodedInputStream_tC76F9515605395C11D7F9437E0E06C828232B0C8 ** get_address_of_codedInputStream_2() { return &___codedInputStream_2; }
	inline void set_codedInputStream_2(CodedInputStream_tC76F9515605395C11D7F9437E0E06C828232B0C8 * value)
	{
		___codedInputStream_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___codedInputStream_2), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of Google.Protobuf.SegmentedBufferHelper
struct SegmentedBufferHelper_t885D05BD8C8563F4D75A82B6FB4CE2F009C8B005_marshaled_pinvoke
{
	Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103  ___totalLength_0;
	Enumerator_t4820F5B6D236EC8EF539F8F899F170D4F6B708F4  ___readOnlySequenceEnumerator_1;
	CodedInputStream_tC76F9515605395C11D7F9437E0E06C828232B0C8 * ___codedInputStream_2;
};
// Native definition for COM marshalling of Google.Protobuf.SegmentedBufferHelper
struct SegmentedBufferHelper_t885D05BD8C8563F4D75A82B6FB4CE2F009C8B005_marshaled_com
{
	Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103  ___totalLength_0;
	Enumerator_t4820F5B6D236EC8EF539F8F899F170D4F6B708F4  ___readOnlySequenceEnumerator_1;
	CodedInputStream_tC76F9515605395C11D7F9437E0E06C828232B0C8 * ___codedInputStream_2;
};

// Google.Protobuf.ParserInternalState
struct ParserInternalState_t08A261A9F88B2B769A10BED876C9622FEAFD2596 
{
public:
	// System.Int32 Google.Protobuf.ParserInternalState::bufferPos
	int32_t ___bufferPos_0;
	// System.Int32 Google.Protobuf.ParserInternalState::bufferSize
	int32_t ___bufferSize_1;
	// System.Int32 Google.Protobuf.ParserInternalState::bufferSizeAfterLimit
	int32_t ___bufferSizeAfterLimit_2;
	// System.Int32 Google.Protobuf.ParserInternalState::currentLimit
	int32_t ___currentLimit_3;
	// System.Int32 Google.Protobuf.ParserInternalState::totalBytesRetired
	int32_t ___totalBytesRetired_4;
	// System.Int32 Google.Protobuf.ParserInternalState::recursionDepth
	int32_t ___recursionDepth_5;
	// Google.Protobuf.SegmentedBufferHelper Google.Protobuf.ParserInternalState::segmentedBufferHelper
	SegmentedBufferHelper_t885D05BD8C8563F4D75A82B6FB4CE2F009C8B005  ___segmentedBufferHelper_6;
	// System.UInt32 Google.Protobuf.ParserInternalState::lastTag
	uint32_t ___lastTag_7;
	// System.UInt32 Google.Protobuf.ParserInternalState::nextTag
	uint32_t ___nextTag_8;
	// System.Boolean Google.Protobuf.ParserInternalState::hasNextTag
	bool ___hasNextTag_9;
	// System.Int32 Google.Protobuf.ParserInternalState::sizeLimit
	int32_t ___sizeLimit_10;
	// System.Int32 Google.Protobuf.ParserInternalState::recursionLimit
	int32_t ___recursionLimit_11;
	// System.Boolean Google.Protobuf.ParserInternalState::<DiscardUnknownFields>k__BackingField
	bool ___U3CDiscardUnknownFieldsU3Ek__BackingField_12;
	// Google.Protobuf.ExtensionRegistry Google.Protobuf.ParserInternalState::<ExtensionRegistry>k__BackingField
	ExtensionRegistry_tA5BBE047B436970D8BE3DBBE73D5BBFEAFA1CE47 * ___U3CExtensionRegistryU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of_bufferPos_0() { return static_cast<int32_t>(offsetof(ParserInternalState_t08A261A9F88B2B769A10BED876C9622FEAFD2596, ___bufferPos_0)); }
	inline int32_t get_bufferPos_0() const { return ___bufferPos_0; }
	inline int32_t* get_address_of_bufferPos_0() { return &___bufferPos_0; }
	inline void set_bufferPos_0(int32_t value)
	{
		___bufferPos_0 = value;
	}

	inline static int32_t get_offset_of_bufferSize_1() { return static_cast<int32_t>(offsetof(ParserInternalState_t08A261A9F88B2B769A10BED876C9622FEAFD2596, ___bufferSize_1)); }
	inline int32_t get_bufferSize_1() const { return ___bufferSize_1; }
	inline int32_t* get_address_of_bufferSize_1() { return &___bufferSize_1; }
	inline void set_bufferSize_1(int32_t value)
	{
		___bufferSize_1 = value;
	}

	inline static int32_t get_offset_of_bufferSizeAfterLimit_2() { return static_cast<int32_t>(offsetof(ParserInternalState_t08A261A9F88B2B769A10BED876C9622FEAFD2596, ___bufferSizeAfterLimit_2)); }
	inline int32_t get_bufferSizeAfterLimit_2() const { return ___bufferSizeAfterLimit_2; }
	inline int32_t* get_address_of_bufferSizeAfterLimit_2() { return &___bufferSizeAfterLimit_2; }
	inline void set_bufferSizeAfterLimit_2(int32_t value)
	{
		___bufferSizeAfterLimit_2 = value;
	}

	inline static int32_t get_offset_of_currentLimit_3() { return static_cast<int32_t>(offsetof(ParserInternalState_t08A261A9F88B2B769A10BED876C9622FEAFD2596, ___currentLimit_3)); }
	inline int32_t get_currentLimit_3() const { return ___currentLimit_3; }
	inline int32_t* get_address_of_currentLimit_3() { return &___currentLimit_3; }
	inline void set_currentLimit_3(int32_t value)
	{
		___currentLimit_3 = value;
	}

	inline static int32_t get_offset_of_totalBytesRetired_4() { return static_cast<int32_t>(offsetof(ParserInternalState_t08A261A9F88B2B769A10BED876C9622FEAFD2596, ___totalBytesRetired_4)); }
	inline int32_t get_totalBytesRetired_4() const { return ___totalBytesRetired_4; }
	inline int32_t* get_address_of_totalBytesRetired_4() { return &___totalBytesRetired_4; }
	inline void set_totalBytesRetired_4(int32_t value)
	{
		___totalBytesRetired_4 = value;
	}

	inline static int32_t get_offset_of_recursionDepth_5() { return static_cast<int32_t>(offsetof(ParserInternalState_t08A261A9F88B2B769A10BED876C9622FEAFD2596, ___recursionDepth_5)); }
	inline int32_t get_recursionDepth_5() const { return ___recursionDepth_5; }
	inline int32_t* get_address_of_recursionDepth_5() { return &___recursionDepth_5; }
	inline void set_recursionDepth_5(int32_t value)
	{
		___recursionDepth_5 = value;
	}

	inline static int32_t get_offset_of_segmentedBufferHelper_6() { return static_cast<int32_t>(offsetof(ParserInternalState_t08A261A9F88B2B769A10BED876C9622FEAFD2596, ___segmentedBufferHelper_6)); }
	inline SegmentedBufferHelper_t885D05BD8C8563F4D75A82B6FB4CE2F009C8B005  get_segmentedBufferHelper_6() const { return ___segmentedBufferHelper_6; }
	inline SegmentedBufferHelper_t885D05BD8C8563F4D75A82B6FB4CE2F009C8B005 * get_address_of_segmentedBufferHelper_6() { return &___segmentedBufferHelper_6; }
	inline void set_segmentedBufferHelper_6(SegmentedBufferHelper_t885D05BD8C8563F4D75A82B6FB4CE2F009C8B005  value)
	{
		___segmentedBufferHelper_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&((&((&((&(((&___segmentedBufferHelper_6))->___readOnlySequenceEnumerator_1))->____sequence_0))->____sequenceStart_0))->____object_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&((&(((&___segmentedBufferHelper_6))->___readOnlySequenceEnumerator_1))->____sequence_0))->____sequenceEnd_1))->____object_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___segmentedBufferHelper_6))->___readOnlySequenceEnumerator_1))->____next_1))->____object_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___segmentedBufferHelper_6))->___readOnlySequenceEnumerator_1))->____currentMemory_2))->____object_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___segmentedBufferHelper_6))->___codedInputStream_2), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_lastTag_7() { return static_cast<int32_t>(offsetof(ParserInternalState_t08A261A9F88B2B769A10BED876C9622FEAFD2596, ___lastTag_7)); }
	inline uint32_t get_lastTag_7() const { return ___lastTag_7; }
	inline uint32_t* get_address_of_lastTag_7() { return &___lastTag_7; }
	inline void set_lastTag_7(uint32_t value)
	{
		___lastTag_7 = value;
	}

	inline static int32_t get_offset_of_nextTag_8() { return static_cast<int32_t>(offsetof(ParserInternalState_t08A261A9F88B2B769A10BED876C9622FEAFD2596, ___nextTag_8)); }
	inline uint32_t get_nextTag_8() const { return ___nextTag_8; }
	inline uint32_t* get_address_of_nextTag_8() { return &___nextTag_8; }
	inline void set_nextTag_8(uint32_t value)
	{
		___nextTag_8 = value;
	}

	inline static int32_t get_offset_of_hasNextTag_9() { return static_cast<int32_t>(offsetof(ParserInternalState_t08A261A9F88B2B769A10BED876C9622FEAFD2596, ___hasNextTag_9)); }
	inline bool get_hasNextTag_9() const { return ___hasNextTag_9; }
	inline bool* get_address_of_hasNextTag_9() { return &___hasNextTag_9; }
	inline void set_hasNextTag_9(bool value)
	{
		___hasNextTag_9 = value;
	}

	inline static int32_t get_offset_of_sizeLimit_10() { return static_cast<int32_t>(offsetof(ParserInternalState_t08A261A9F88B2B769A10BED876C9622FEAFD2596, ___sizeLimit_10)); }
	inline int32_t get_sizeLimit_10() const { return ___sizeLimit_10; }
	inline int32_t* get_address_of_sizeLimit_10() { return &___sizeLimit_10; }
	inline void set_sizeLimit_10(int32_t value)
	{
		___sizeLimit_10 = value;
	}

	inline static int32_t get_offset_of_recursionLimit_11() { return static_cast<int32_t>(offsetof(ParserInternalState_t08A261A9F88B2B769A10BED876C9622FEAFD2596, ___recursionLimit_11)); }
	inline int32_t get_recursionLimit_11() const { return ___recursionLimit_11; }
	inline int32_t* get_address_of_recursionLimit_11() { return &___recursionLimit_11; }
	inline void set_recursionLimit_11(int32_t value)
	{
		___recursionLimit_11 = value;
	}

	inline static int32_t get_offset_of_U3CDiscardUnknownFieldsU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(ParserInternalState_t08A261A9F88B2B769A10BED876C9622FEAFD2596, ___U3CDiscardUnknownFieldsU3Ek__BackingField_12)); }
	inline bool get_U3CDiscardUnknownFieldsU3Ek__BackingField_12() const { return ___U3CDiscardUnknownFieldsU3Ek__BackingField_12; }
	inline bool* get_address_of_U3CDiscardUnknownFieldsU3Ek__BackingField_12() { return &___U3CDiscardUnknownFieldsU3Ek__BackingField_12; }
	inline void set_U3CDiscardUnknownFieldsU3Ek__BackingField_12(bool value)
	{
		___U3CDiscardUnknownFieldsU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CExtensionRegistryU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(ParserInternalState_t08A261A9F88B2B769A10BED876C9622FEAFD2596, ___U3CExtensionRegistryU3Ek__BackingField_13)); }
	inline ExtensionRegistry_tA5BBE047B436970D8BE3DBBE73D5BBFEAFA1CE47 * get_U3CExtensionRegistryU3Ek__BackingField_13() const { return ___U3CExtensionRegistryU3Ek__BackingField_13; }
	inline ExtensionRegistry_tA5BBE047B436970D8BE3DBBE73D5BBFEAFA1CE47 ** get_address_of_U3CExtensionRegistryU3Ek__BackingField_13() { return &___U3CExtensionRegistryU3Ek__BackingField_13; }
	inline void set_U3CExtensionRegistryU3Ek__BackingField_13(ExtensionRegistry_tA5BBE047B436970D8BE3DBBE73D5BBFEAFA1CE47 * value)
	{
		___U3CExtensionRegistryU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CExtensionRegistryU3Ek__BackingField_13), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of Google.Protobuf.ParserInternalState
struct ParserInternalState_t08A261A9F88B2B769A10BED876C9622FEAFD2596_marshaled_pinvoke
{
	int32_t ___bufferPos_0;
	int32_t ___bufferSize_1;
	int32_t ___bufferSizeAfterLimit_2;
	int32_t ___currentLimit_3;
	int32_t ___totalBytesRetired_4;
	int32_t ___recursionDepth_5;
	SegmentedBufferHelper_t885D05BD8C8563F4D75A82B6FB4CE2F009C8B005_marshaled_pinvoke ___segmentedBufferHelper_6;
	uint32_t ___lastTag_7;
	uint32_t ___nextTag_8;
	int32_t ___hasNextTag_9;
	int32_t ___sizeLimit_10;
	int32_t ___recursionLimit_11;
	int32_t ___U3CDiscardUnknownFieldsU3Ek__BackingField_12;
	ExtensionRegistry_tA5BBE047B436970D8BE3DBBE73D5BBFEAFA1CE47 * ___U3CExtensionRegistryU3Ek__BackingField_13;
};
// Native definition for COM marshalling of Google.Protobuf.ParserInternalState
struct ParserInternalState_t08A261A9F88B2B769A10BED876C9622FEAFD2596_marshaled_com
{
	int32_t ___bufferPos_0;
	int32_t ___bufferSize_1;
	int32_t ___bufferSizeAfterLimit_2;
	int32_t ___currentLimit_3;
	int32_t ___totalBytesRetired_4;
	int32_t ___recursionDepth_5;
	SegmentedBufferHelper_t885D05BD8C8563F4D75A82B6FB4CE2F009C8B005_marshaled_com ___segmentedBufferHelper_6;
	uint32_t ___lastTag_7;
	uint32_t ___nextTag_8;
	int32_t ___hasNextTag_9;
	int32_t ___sizeLimit_10;
	int32_t ___recursionLimit_11;
	int32_t ___U3CDiscardUnknownFieldsU3Ek__BackingField_12;
	ExtensionRegistry_tA5BBE047B436970D8BE3DBBE73D5BBFEAFA1CE47 * ___U3CExtensionRegistryU3Ek__BackingField_13;
};

// Google.Protobuf.CodedInputStream
struct CodedInputStream_tC76F9515605395C11D7F9437E0E06C828232B0C8  : public RuntimeObject
{
public:
	// System.Boolean Google.Protobuf.CodedInputStream::leaveOpen
	bool ___leaveOpen_0;
	// System.Byte[] Google.Protobuf.CodedInputStream::buffer
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___buffer_1;
	// System.IO.Stream Google.Protobuf.CodedInputStream::input
	Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * ___input_2;
	// Google.Protobuf.ParserInternalState Google.Protobuf.CodedInputStream::state
	ParserInternalState_t08A261A9F88B2B769A10BED876C9622FEAFD2596  ___state_3;

public:
	inline static int32_t get_offset_of_leaveOpen_0() { return static_cast<int32_t>(offsetof(CodedInputStream_tC76F9515605395C11D7F9437E0E06C828232B0C8, ___leaveOpen_0)); }
	inline bool get_leaveOpen_0() const { return ___leaveOpen_0; }
	inline bool* get_address_of_leaveOpen_0() { return &___leaveOpen_0; }
	inline void set_leaveOpen_0(bool value)
	{
		___leaveOpen_0 = value;
	}

	inline static int32_t get_offset_of_buffer_1() { return static_cast<int32_t>(offsetof(CodedInputStream_tC76F9515605395C11D7F9437E0E06C828232B0C8, ___buffer_1)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_buffer_1() const { return ___buffer_1; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_buffer_1() { return &___buffer_1; }
	inline void set_buffer_1(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___buffer_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___buffer_1), (void*)value);
	}

	inline static int32_t get_offset_of_input_2() { return static_cast<int32_t>(offsetof(CodedInputStream_tC76F9515605395C11D7F9437E0E06C828232B0C8, ___input_2)); }
	inline Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * get_input_2() const { return ___input_2; }
	inline Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB ** get_address_of_input_2() { return &___input_2; }
	inline void set_input_2(Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * value)
	{
		___input_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___input_2), (void*)value);
	}

	inline static int32_t get_offset_of_state_3() { return static_cast<int32_t>(offsetof(CodedInputStream_tC76F9515605395C11D7F9437E0E06C828232B0C8, ___state_3)); }
	inline ParserInternalState_t08A261A9F88B2B769A10BED876C9622FEAFD2596  get_state_3() const { return ___state_3; }
	inline ParserInternalState_t08A261A9F88B2B769A10BED876C9622FEAFD2596 * get_address_of_state_3() { return &___state_3; }
	inline void set_state_3(ParserInternalState_t08A261A9F88B2B769A10BED876C9622FEAFD2596  value)
	{
		___state_3 = value;
		Il2CppCodeGenWriteBarrier((void**)&((&((&((&((&(((&___state_3))->___segmentedBufferHelper_6))->___readOnlySequenceEnumerator_1))->____sequence_0))->____sequenceStart_0))->____object_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&((&((&(((&___state_3))->___segmentedBufferHelper_6))->___readOnlySequenceEnumerator_1))->____sequence_0))->____sequenceEnd_1))->____object_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&((&(((&___state_3))->___segmentedBufferHelper_6))->___readOnlySequenceEnumerator_1))->____next_1))->____object_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&((&(((&___state_3))->___segmentedBufferHelper_6))->___readOnlySequenceEnumerator_1))->____currentMemory_2))->____object_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___state_3))->___segmentedBufferHelper_6))->___codedInputStream_2), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___state_3))->___U3CExtensionRegistryU3Ek__BackingField_13), (void*)NULL);
		#endif
	}
};


// Google.Protobuf.ParseContext
struct ParseContext_t288EAC5562A1D7E0FF8A06DBA58B8CF732ECE349 
{
public:
	// System.ReadOnlySpan`1<System.Byte> Google.Protobuf.ParseContext::buffer
	ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726  ___buffer_0;
	// Google.Protobuf.ParserInternalState Google.Protobuf.ParseContext::state
	ParserInternalState_t08A261A9F88B2B769A10BED876C9622FEAFD2596  ___state_1;

public:
	inline static int32_t get_offset_of_buffer_0() { return static_cast<int32_t>(offsetof(ParseContext_t288EAC5562A1D7E0FF8A06DBA58B8CF732ECE349, ___buffer_0)); }
	inline ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726  get_buffer_0() const { return ___buffer_0; }
	inline ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 * get_address_of_buffer_0() { return &___buffer_0; }
	inline void set_buffer_0(ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726  value)
	{
		___buffer_0 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___buffer_0))->____pinnable_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_state_1() { return static_cast<int32_t>(offsetof(ParseContext_t288EAC5562A1D7E0FF8A06DBA58B8CF732ECE349, ___state_1)); }
	inline ParserInternalState_t08A261A9F88B2B769A10BED876C9622FEAFD2596  get_state_1() const { return ___state_1; }
	inline ParserInternalState_t08A261A9F88B2B769A10BED876C9622FEAFD2596 * get_address_of_state_1() { return &___state_1; }
	inline void set_state_1(ParserInternalState_t08A261A9F88B2B769A10BED876C9622FEAFD2596  value)
	{
		___state_1 = value;
		Il2CppCodeGenWriteBarrier((void**)&((&((&((&((&(((&___state_1))->___segmentedBufferHelper_6))->___readOnlySequenceEnumerator_1))->____sequence_0))->____sequenceStart_0))->____object_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&((&((&(((&___state_1))->___segmentedBufferHelper_6))->___readOnlySequenceEnumerator_1))->____sequence_0))->____sequenceEnd_1))->____object_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&((&(((&___state_1))->___segmentedBufferHelper_6))->___readOnlySequenceEnumerator_1))->____next_1))->____object_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&((&(((&___state_1))->___segmentedBufferHelper_6))->___readOnlySequenceEnumerator_1))->____currentMemory_2))->____object_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___state_1))->___segmentedBufferHelper_6))->___codedInputStream_2), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___state_1))->___U3CExtensionRegistryU3Ek__BackingField_13), (void*)NULL);
		#endif
	}
};

// Native definition for P/Invoke marshalling of Google.Protobuf.ParseContext
struct ParseContext_t288EAC5562A1D7E0FF8A06DBA58B8CF732ECE349_marshaled_pinvoke
{
	ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726  ___buffer_0;
	ParserInternalState_t08A261A9F88B2B769A10BED876C9622FEAFD2596_marshaled_pinvoke ___state_1;
};
// Native definition for COM marshalling of Google.Protobuf.ParseContext
struct ParseContext_t288EAC5562A1D7E0FF8A06DBA58B8CF732ECE349_marshaled_com
{
	ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726  ___buffer_0;
	ParserInternalState_t08A261A9F88B2B769A10BED876C9622FEAFD2596_marshaled_com ___state_1;
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif


// System.Void System.Func`1<System.Object>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_1__ctor_m2A4FE889FB540EA198F7757D17DC2290461E5EE9_gshared (Func_1_t807CEE610086E24A0167BAA97A64062016E09D49 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void Google.Protobuf.MessageParser`1<System.Object>::.ctor(System.Func`1<T>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MessageParser_1__ctor_m4B01721875A93C3F52B6AF52315644D793DEBFC5_gshared (MessageParser_1_tA9AC1CA23E91C173440DD761C276E7BE47F12B18 * __this, Func_1_t807CEE610086E24A0167BAA97A64062016E09D49 * ___factory0, const RuntimeMethod* method);
// System.Void Google.Protobuf.Collections.RepeatedField`1<System.Int32>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RepeatedField_1__ctor_m28E20B7724ACD650AE80B8E03BCB4A9412BFB63F_gshared (RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419 * __this, const RuntimeMethod* method);
// Google.Protobuf.Collections.RepeatedField`1<T> Google.Protobuf.Collections.RepeatedField`1<System.Int32>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419 * RepeatedField_1_Clone_mCCDDBCC8A031EB0A89878CA4C025F8B396DD9348_gshared (RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419 * __this, const RuntimeMethod* method);
// T Google.Protobuf.ProtoPreconditions::CheckNotNull<System.Object>(T,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * ProtoPreconditions_CheckNotNull_TisRuntimeObject_m8E4A5370365ECEE37A5B9E417F9BBDA6D7DB8C5D_gshared (RuntimeObject * ___value0, String_t* ___name1, const RuntimeMethod* method);
// System.Boolean Google.Protobuf.Collections.RepeatedField`1<System.Int32>::Equals(Google.Protobuf.Collections.RepeatedField`1<T>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool RepeatedField_1_Equals_m2D0B705880C1B033423F8A185C0D0DFE9EEDAADA_gshared (RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419 * __this, RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419 * ___other0, const RuntimeMethod* method);
// System.Void Google.Protobuf.Collections.RepeatedField`1<System.Int32>::WriteTo(Google.Protobuf.WriteContext&,Google.Protobuf.FieldCodec`1<T>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RepeatedField_1_WriteTo_mC5C250B204D43F5D814F1C19A1337853828264B8_gshared (RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419 * __this, WriteContext_t4011B33FF5B289B23B81D9929C82A44F0A24DFCB * ___ctx0, FieldCodec_1_tAB7BF6BD67CE831FB7E8A437894272BD9C511681 * ___codec1, const RuntimeMethod* method);
// System.Int32 Google.Protobuf.Collections.RepeatedField`1<System.Int32>::CalculateSize(Google.Protobuf.FieldCodec`1<T>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t RepeatedField_1_CalculateSize_m67536E773582CAB8A68E5056E9A1D318FAE1942B_gshared (RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419 * __this, FieldCodec_1_tAB7BF6BD67CE831FB7E8A437894272BD9C511681 * ___codec0, const RuntimeMethod* method);
// System.Void Google.Protobuf.Collections.RepeatedField`1<System.Int32>::Add(System.Collections.Generic.IEnumerable`1<T>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RepeatedField_1_Add_m65F8D4082B764C2A1CC6367692FB566C2A0D75F8_gshared (RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419 * __this, RuntimeObject* ___values0, const RuntimeMethod* method);
// System.Void Google.Protobuf.Collections.RepeatedField`1<System.Int32>::AddEntriesFrom(Google.Protobuf.ParseContext&,Google.Protobuf.FieldCodec`1<T>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RepeatedField_1_AddEntriesFrom_m5AFE58E765285935D3E71F09A425AD7BE2D56674_gshared (RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419 * __this, ParseContext_t288EAC5562A1D7E0FF8A06DBA58B8CF732ECE349 * ___ctx0, FieldCodec_1_tAB7BF6BD67CE831FB7E8A437894272BD9C511681 * ___codec1, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerable`1<!!0> System.Reflection.CustomAttributeExtensions::GetCustomAttributes<System.Object>(System.Reflection.MemberInfo)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* CustomAttributeExtensions_GetCustomAttributes_TisRuntimeObject_m8465B39E995CAEEF162F07ECD2B235B242054872_gshared (MemberInfo_t * ___element0, const RuntimeMethod* method);
// !!0 System.Linq.Enumerable::FirstOrDefault<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Enumerable_FirstOrDefault_TisRuntimeObject_mEA30C68B8D8C7D44DF6690A8FC79E4A48A09C6F9_gshared (RuntimeObject* ___source0, const RuntimeMethod* method);
// System.Void Google.Protobuf.Collections.RepeatedField`1<System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RepeatedField_1__ctor_m91A3D2AF726F859ABF89A26A91DA316F9915FA08_gshared (RepeatedField_1_tD5A1E0061331823102895A0C66DF000A633AB967 * __this, const RuntimeMethod* method);
// Google.Protobuf.Collections.RepeatedField`1<T> Google.Protobuf.Collections.RepeatedField`1<System.Object>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RepeatedField_1_tD5A1E0061331823102895A0C66DF000A633AB967 * RepeatedField_1_Clone_mC5560F6E0AD410679F9126670E9B3EDBB6908F32_gshared (RepeatedField_1_tD5A1E0061331823102895A0C66DF000A633AB967 * __this, const RuntimeMethod* method);
// System.Boolean Google.Protobuf.Collections.RepeatedField`1<System.Object>::Equals(Google.Protobuf.Collections.RepeatedField`1<T>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool RepeatedField_1_Equals_mEFD2E6DC4C9C82361E51451B49F32E125AFC3F55_gshared (RepeatedField_1_tD5A1E0061331823102895A0C66DF000A633AB967 * __this, RepeatedField_1_tD5A1E0061331823102895A0C66DF000A633AB967 * ___other0, const RuntimeMethod* method);
// System.Void Google.Protobuf.Collections.RepeatedField`1<System.Object>::WriteTo(Google.Protobuf.WriteContext&,Google.Protobuf.FieldCodec`1<T>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RepeatedField_1_WriteTo_mE074FA7F4AC1D82D6AC5E3B85819057EE823E6F5_gshared (RepeatedField_1_tD5A1E0061331823102895A0C66DF000A633AB967 * __this, WriteContext_t4011B33FF5B289B23B81D9929C82A44F0A24DFCB * ___ctx0, FieldCodec_1_tB53B2BCC80953659808E7925A1937759BD679A5B * ___codec1, const RuntimeMethod* method);
// System.Int32 Google.Protobuf.Collections.RepeatedField`1<System.Object>::CalculateSize(Google.Protobuf.FieldCodec`1<T>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t RepeatedField_1_CalculateSize_m79E728BA3A01ACB1B539A567DBC0B484DA1A0C4D_gshared (RepeatedField_1_tD5A1E0061331823102895A0C66DF000A633AB967 * __this, FieldCodec_1_tB53B2BCC80953659808E7925A1937759BD679A5B * ___codec0, const RuntimeMethod* method);
// System.Void Google.Protobuf.Collections.RepeatedField`1<System.Object>::Add(System.Collections.Generic.IEnumerable`1<T>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RepeatedField_1_Add_m55B7D26CA8F0985363E10FE56D51F0A78F40847D_gshared (RepeatedField_1_tD5A1E0061331823102895A0C66DF000A633AB967 * __this, RuntimeObject* ___values0, const RuntimeMethod* method);
// System.Void Google.Protobuf.Collections.RepeatedField`1<System.Object>::AddEntriesFrom(Google.Protobuf.ParseContext&,Google.Protobuf.FieldCodec`1<T>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RepeatedField_1_AddEntriesFrom_mC21CCB0B2492BC3101179D04E0C8559EFC644214_gshared (RepeatedField_1_tD5A1E0061331823102895A0C66DF000A633AB967 * __this, ParseContext_t288EAC5562A1D7E0FF8A06DBA58B8CF732ECE349 * ___ctx0, FieldCodec_1_tB53B2BCC80953659808E7925A1937759BD679A5B * ___codec1, const RuntimeMethod* method);

// Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.Reflection.EnumDescriptorProto::get_Descriptor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13 * EnumDescriptorProto_get_Descriptor_mB89737A900858354FD200A724032DF8194CF571C (const RuntimeMethod* method);
// System.Collections.Generic.IList`1<Google.Protobuf.Reflection.MessageDescriptor> Google.Protobuf.Reflection.MessageDescriptor::get_NestedTypes()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject* MessageDescriptor_get_NestedTypes_m34A524E3C7D560C1537685CF6291AB60DA58FE7E_inline (MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13 * __this, const RuntimeMethod* method);
// Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange::get_Descriptor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13 * EnumReservedRange_get_Descriptor_mD31EA935A954A7AAF19EB17AED0FEE0644E39056 (const RuntimeMethod* method);
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Void Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnumReservedRange__ctor_m02904F51FF5FE5B92BAA1E816D3A1B13534A6E0C (EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41 * __this, const RuntimeMethod* method);
// Google.Protobuf.UnknownFieldSet Google.Protobuf.UnknownFieldSet::Clone(Google.Protobuf.UnknownFieldSet)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 * UnknownFieldSet_Clone_mBCB3196178FBF9E17BF3C686C6C6DC214D0BBDEA (UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 * ___other0, const RuntimeMethod* method);
// System.Void Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange::.ctor(Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnumReservedRange__ctor_m4F7BB9BB1007668742F085BDA2A82E889F6D1B6A (EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41 * __this, EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41 * ___other0, const RuntimeMethod* method);
// System.Boolean Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange::Equals(Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool EnumReservedRange_Equals_m1FB9B43A21E9D232DFE20485F50C4673D84948E1 (EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41 * __this, EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41 * ___other0, const RuntimeMethod* method);
// System.Int32 Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange::get_Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t EnumReservedRange_get_Start_m425FFA90089C7FCDD1C01FA5C950E5477F17DCC5 (EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41 * __this, const RuntimeMethod* method);
// System.Int32 Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange::get_End()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t EnumReservedRange_get_End_m012A5FA46B5F0D66FBCEAA9B17BCE31CB105E0D9 (EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41 * __this, const RuntimeMethod* method);
// System.Boolean System.Object::Equals(System.Object,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_Equals_mBE334AF263BDADA1D0F1DE7E14456A63E47F8053 (RuntimeObject * ___objA0, RuntimeObject * ___objB1, const RuntimeMethod* method);
// System.Boolean Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange::get_HasStart()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool EnumReservedRange_get_HasStart_m394D0FDEAAE8B2C54B2B7ACE2DAFD1F354B2AFA8 (EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41 * __this, const RuntimeMethod* method);
// System.Int32 System.Int32::GetHashCode()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Int32_GetHashCode_mEDD3F492A5F7CF021125AE3F38E2B8F8743FC667 (int32_t* __this, const RuntimeMethod* method);
// System.Boolean Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange::get_HasEnd()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool EnumReservedRange_get_HasEnd_m743B6947E1F9A8CD3CB43B5CC0056313942F1AE1 (EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41 * __this, const RuntimeMethod* method);
// System.String Google.Protobuf.JsonFormatter::ToDiagnosticString(Google.Protobuf.IMessage)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* JsonFormatter_ToDiagnosticString_mB0CBCBDBF65F17FC52DEB9E0C49184E44F3F2EF1 (RuntimeObject* ___message0, const RuntimeMethod* method);
// System.Void Google.Protobuf.CodedOutputStream::WriteRawMessage(Google.Protobuf.IMessage)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CodedOutputStream_WriteRawMessage_m05CED72DF9A800E9BFB28956F21E7B4416E03D11 (CodedOutputStream_t2871B0D17F920BFB54B564E2295D9753721642E6 * __this, RuntimeObject* ___value0, const RuntimeMethod* method);
// System.Void Google.Protobuf.WriteContext::WriteRawTag(System.Byte)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WriteContext_WriteRawTag_m3E37F0B83B4E8B7C7E1CC6472D527AA22B11DA5A (WriteContext_t4011B33FF5B289B23B81D9929C82A44F0A24DFCB * __this, uint8_t ___b10, const RuntimeMethod* method);
// System.Void Google.Protobuf.WriteContext::WriteInt32(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WriteContext_WriteInt32_mCC3646C9CF9A13E2BEBC15A8E566669C913D4F65 (WriteContext_t4011B33FF5B289B23B81D9929C82A44F0A24DFCB * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void Google.Protobuf.UnknownFieldSet::WriteTo(Google.Protobuf.WriteContext&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnknownFieldSet_WriteTo_mECF1EFA7AF68EBBEDDD046865B2C17131B969E1B (UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 * __this, WriteContext_t4011B33FF5B289B23B81D9929C82A44F0A24DFCB * ___ctx0, const RuntimeMethod* method);
// System.Int32 Google.Protobuf.CodedOutputStream::ComputeInt32Size(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t CodedOutputStream_ComputeInt32Size_m03C7A226C41F660D0A853D576117A6386B9D0802 (int32_t ___value0, const RuntimeMethod* method);
// System.Int32 Google.Protobuf.UnknownFieldSet::CalculateSize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t UnknownFieldSet_CalculateSize_m0F48042AFDC7E6795E7A99D8B3BE0D5C61C96868 (UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 * __this, const RuntimeMethod* method);
// System.Void Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange::set_Start(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnumReservedRange_set_Start_m69F0A6CB6629086763121A8CD5E295415242BE82 (EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41 * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange::set_End(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnumReservedRange_set_End_mF993946771523BFE0D542CD2C3ACB19ADE9C1CC3 (EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41 * __this, int32_t ___value0, const RuntimeMethod* method);
// Google.Protobuf.UnknownFieldSet Google.Protobuf.UnknownFieldSet::MergeFrom(Google.Protobuf.UnknownFieldSet,Google.Protobuf.UnknownFieldSet)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 * UnknownFieldSet_MergeFrom_m3175C4FF04375A5EF7DC4E3245C6606303344F85 (UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 * ___unknownFields0, UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 * ___other1, const RuntimeMethod* method);
// System.Void Google.Protobuf.CodedInputStream::ReadRawMessage(Google.Protobuf.IMessage)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CodedInputStream_ReadRawMessage_m77E217968CD5654CC717E7DA57906C0219372943 (CodedInputStream_tC76F9515605395C11D7F9437E0E06C828232B0C8 * __this, RuntimeObject* ___message0, const RuntimeMethod* method);
// Google.Protobuf.UnknownFieldSet Google.Protobuf.UnknownFieldSet::MergeFieldFrom(Google.Protobuf.UnknownFieldSet,Google.Protobuf.ParseContext&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 * UnknownFieldSet_MergeFieldFrom_m6D9E32ADBB0194941C65CD43522F53C3A2378283 (UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 * ___unknownFields0, ParseContext_t288EAC5562A1D7E0FF8A06DBA58B8CF732ECE349 * ___ctx1, const RuntimeMethod* method);
// System.Int32 Google.Protobuf.ParseContext::ReadInt32()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t ParseContext_ReadInt32_m275087DD36E27495CC2EE76EEB08BC90EFAF94B9_inline (ParseContext_t288EAC5562A1D7E0FF8A06DBA58B8CF732ECE349 * __this, const RuntimeMethod* method);
// System.UInt32 Google.Protobuf.ParseContext::ReadTag()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint32_t ParseContext_ReadTag_mEE687A18078235454AEA63F1F61E9E3E9EB0260F_inline (ParseContext_t288EAC5562A1D7E0FF8A06DBA58B8CF732ECE349 * __this, const RuntimeMethod* method);
// System.Void System.Func`1<Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange>::.ctor(System.Object,System.IntPtr)
inline void Func_1__ctor_mCCFD409A1FBA40DE4A8770B3B56B638652558C3D (Func_1_tB4DEFF82E390DE2B16A177BF95E2A660E016FC83 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_1_tB4DEFF82E390DE2B16A177BF95E2A660E016FC83 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Func_1__ctor_m2A4FE889FB540EA198F7757D17DC2290461E5EE9_gshared)(__this, ___object0, ___method1, method);
}
// System.Void Google.Protobuf.MessageParser`1<Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange>::.ctor(System.Func`1<T>)
inline void MessageParser_1__ctor_mE62242286F7EBCC8F4B3B117434CF74E088C0C26 (MessageParser_1_tCCF97EFBB4482323562EEB1F40E83233B2D32D55 * __this, Func_1_tB4DEFF82E390DE2B16A177BF95E2A660E016FC83 * ___factory0, const RuntimeMethod* method)
{
	((  void (*) (MessageParser_1_tCCF97EFBB4482323562EEB1F40E83233B2D32D55 *, Func_1_tB4DEFF82E390DE2B16A177BF95E2A660E016FC83 *, const RuntimeMethod*))MessageParser_1__ctor_m4B01721875A93C3F52B6AF52315644D793DEBFC5_gshared)(__this, ___factory0, method);
}
// Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.Reflection.GeneratedCodeInfo::get_Descriptor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13 * GeneratedCodeInfo_get_Descriptor_m2A40BE0B9007A08DDF69F22C9D6B16B364E08361 (const RuntimeMethod* method);
// Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation::get_Descriptor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13 * Annotation_get_Descriptor_m03B3B365C873BFD9BECDEB8D977CF8C3CA4D161A (const RuntimeMethod* method);
// System.Void Google.Protobuf.Collections.RepeatedField`1<System.Int32>::.ctor()
inline void RepeatedField_1__ctor_m28E20B7724ACD650AE80B8E03BCB4A9412BFB63F (RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419 * __this, const RuntimeMethod* method)
{
	((  void (*) (RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419 *, const RuntimeMethod*))RepeatedField_1__ctor_m28E20B7724ACD650AE80B8E03BCB4A9412BFB63F_gshared)(__this, method);
}
// System.Void Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Annotation__ctor_m9ED6A93F900EBFD3D4BC4980C3D9B032B8F9B5D7 (Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405 * __this, const RuntimeMethod* method);
// Google.Protobuf.Collections.RepeatedField`1<T> Google.Protobuf.Collections.RepeatedField`1<System.Int32>::Clone()
inline RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419 * RepeatedField_1_Clone_mCCDDBCC8A031EB0A89878CA4C025F8B396DD9348 (RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419 * __this, const RuntimeMethod* method)
{
	return ((  RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419 * (*) (RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419 *, const RuntimeMethod*))RepeatedField_1_Clone_mCCDDBCC8A031EB0A89878CA4C025F8B396DD9348_gshared)(__this, method);
}
// System.Void Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation::.ctor(Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Annotation__ctor_m61D9641ED59D1CDA766721C1A40198C8BF8F6E45 (Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405 * __this, Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405 * ___other0, const RuntimeMethod* method);
// T Google.Protobuf.ProtoPreconditions::CheckNotNull<System.String>(T,System.String)
inline String_t* ProtoPreconditions_CheckNotNull_TisString_t_m80AE3118869FAEF4DF83AF0D521B284CF725144E (String_t* ___value0, String_t* ___name1, const RuntimeMethod* method)
{
	return ((  String_t* (*) (String_t*, String_t*, const RuntimeMethod*))ProtoPreconditions_CheckNotNull_TisRuntimeObject_m8E4A5370365ECEE37A5B9E417F9BBDA6D7DB8C5D_gshared)(___value0, ___name1, method);
}
// System.Boolean Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation::Equals(Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Annotation_Equals_mD66C9282185C31DF563EED063B1019BE13C2811E (Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405 * __this, Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405 * ___other0, const RuntimeMethod* method);
// System.Boolean Google.Protobuf.Collections.RepeatedField`1<System.Int32>::Equals(Google.Protobuf.Collections.RepeatedField`1<T>)
inline bool RepeatedField_1_Equals_m2D0B705880C1B033423F8A185C0D0DFE9EEDAADA (RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419 * __this, RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419 * ___other0, const RuntimeMethod* method)
{
	return ((  bool (*) (RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419 *, RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419 *, const RuntimeMethod*))RepeatedField_1_Equals_m2D0B705880C1B033423F8A185C0D0DFE9EEDAADA_gshared)(__this, ___other0, method);
}
// System.String Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation::get_SourceFile()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Annotation_get_SourceFile_m91B7098C2E39ECA1A8E13E40DB8A7A740C3C80F6 (Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405 * __this, const RuntimeMethod* method);
// System.Boolean System.String::op_Inequality(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_op_Inequality_mDDA2DDED3E7EF042987EB7180EE3E88105F0AAE2 (String_t* ___a0, String_t* ___b1, const RuntimeMethod* method);
// System.Int32 Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation::get_Begin()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Annotation_get_Begin_m8891E38C06E537DCBAC9BEC72E83162AF4F6E9AC (Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405 * __this, const RuntimeMethod* method);
// System.Int32 Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation::get_End()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Annotation_get_End_m1AB89595BE4271A333A470BDC5206C2814C050D9 (Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405 * __this, const RuntimeMethod* method);
// System.Boolean Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation::get_HasSourceFile()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Annotation_get_HasSourceFile_mD261BC3AC77DB72DB7E1BEE3A7A8955D0913C23E (Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405 * __this, const RuntimeMethod* method);
// System.Boolean Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation::get_HasBegin()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Annotation_get_HasBegin_m86E2171FE0BDCBB1B5A075E45C7A8DDDB931E97B (Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405 * __this, const RuntimeMethod* method);
// System.Boolean Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation::get_HasEnd()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Annotation_get_HasEnd_mF1171BE52E3B2EFB80EFD5480675B2655897470E (Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405 * __this, const RuntimeMethod* method);
// System.Void Google.Protobuf.Collections.RepeatedField`1<System.Int32>::WriteTo(Google.Protobuf.WriteContext&,Google.Protobuf.FieldCodec`1<T>)
inline void RepeatedField_1_WriteTo_mC5C250B204D43F5D814F1C19A1337853828264B8 (RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419 * __this, WriteContext_t4011B33FF5B289B23B81D9929C82A44F0A24DFCB * ___ctx0, FieldCodec_1_tAB7BF6BD67CE831FB7E8A437894272BD9C511681 * ___codec1, const RuntimeMethod* method)
{
	((  void (*) (RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419 *, WriteContext_t4011B33FF5B289B23B81D9929C82A44F0A24DFCB *, FieldCodec_1_tAB7BF6BD67CE831FB7E8A437894272BD9C511681 *, const RuntimeMethod*))RepeatedField_1_WriteTo_mC5C250B204D43F5D814F1C19A1337853828264B8_gshared)(__this, ___ctx0, ___codec1, method);
}
// System.Void Google.Protobuf.WriteContext::WriteString(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WriteContext_WriteString_m1A0F46C5CF383B3F976BFBD24B546EC2E346C68C (WriteContext_t4011B33FF5B289B23B81D9929C82A44F0A24DFCB * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Int32 Google.Protobuf.Collections.RepeatedField`1<System.Int32>::CalculateSize(Google.Protobuf.FieldCodec`1<T>)
inline int32_t RepeatedField_1_CalculateSize_m67536E773582CAB8A68E5056E9A1D318FAE1942B (RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419 * __this, FieldCodec_1_tAB7BF6BD67CE831FB7E8A437894272BD9C511681 * ___codec0, const RuntimeMethod* method)
{
	return ((  int32_t (*) (RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419 *, FieldCodec_1_tAB7BF6BD67CE831FB7E8A437894272BD9C511681 *, const RuntimeMethod*))RepeatedField_1_CalculateSize_m67536E773582CAB8A68E5056E9A1D318FAE1942B_gshared)(__this, ___codec0, method);
}
// System.Int32 Google.Protobuf.CodedOutputStream::ComputeStringSize(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t CodedOutputStream_ComputeStringSize_m3913248CC93D6067C32930171244FD185D818BDF (String_t* ___value0, const RuntimeMethod* method);
// System.Void Google.Protobuf.Collections.RepeatedField`1<System.Int32>::Add(System.Collections.Generic.IEnumerable`1<T>)
inline void RepeatedField_1_Add_m65F8D4082B764C2A1CC6367692FB566C2A0D75F8 (RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419 * __this, RuntimeObject* ___values0, const RuntimeMethod* method)
{
	((  void (*) (RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419 *, RuntimeObject*, const RuntimeMethod*))RepeatedField_1_Add_m65F8D4082B764C2A1CC6367692FB566C2A0D75F8_gshared)(__this, ___values0, method);
}
// System.Void Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation::set_SourceFile(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Annotation_set_SourceFile_mB2AED44E9EFB3CA7D5BF5CA03DE3A8F5CE51723E (Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation::set_Begin(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Annotation_set_Begin_m5AA2016E643BF3BF22002C930EAACD7AE75ABDD5 (Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405 * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation::set_End(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Annotation_set_End_m5BDF9D827DEC0FF6DDA13230D1C99C57817FA7C7 (Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405 * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void Google.Protobuf.Collections.RepeatedField`1<System.Int32>::AddEntriesFrom(Google.Protobuf.ParseContext&,Google.Protobuf.FieldCodec`1<T>)
inline void RepeatedField_1_AddEntriesFrom_m5AFE58E765285935D3E71F09A425AD7BE2D56674 (RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419 * __this, ParseContext_t288EAC5562A1D7E0FF8A06DBA58B8CF732ECE349 * ___ctx0, FieldCodec_1_tAB7BF6BD67CE831FB7E8A437894272BD9C511681 * ___codec1, const RuntimeMethod* method)
{
	((  void (*) (RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419 *, ParseContext_t288EAC5562A1D7E0FF8A06DBA58B8CF732ECE349 *, FieldCodec_1_tAB7BF6BD67CE831FB7E8A437894272BD9C511681 *, const RuntimeMethod*))RepeatedField_1_AddEntriesFrom_m5AFE58E765285935D3E71F09A425AD7BE2D56674_gshared)(__this, ___ctx0, ___codec1, method);
}
// System.String Google.Protobuf.ParseContext::ReadString()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* ParseContext_ReadString_mA21E24240255C281D36669A209628AD6FDBB15AE_inline (ParseContext_t288EAC5562A1D7E0FF8A06DBA58B8CF732ECE349 * __this, const RuntimeMethod* method);
// System.Void System.Func`1<Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation>::.ctor(System.Object,System.IntPtr)
inline void Func_1__ctor_mE88B85D2A25CDAB279765DBE7C9E71B06D0C4C31 (Func_1_t8A740DFC3D657757DA5C263E40CD639547B59CB4 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_1_t8A740DFC3D657757DA5C263E40CD639547B59CB4 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Func_1__ctor_m2A4FE889FB540EA198F7757D17DC2290461E5EE9_gshared)(__this, ___object0, ___method1, method);
}
// System.Void Google.Protobuf.MessageParser`1<Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation>::.ctor(System.Func`1<T>)
inline void MessageParser_1__ctor_m1927E72A8975A7E3D3A15FDFB5DCD64879E50B6E (MessageParser_1_t422AF617132E681AA126A8C774D49BC4101614D0 * __this, Func_1_t8A740DFC3D657757DA5C263E40CD639547B59CB4 * ___factory0, const RuntimeMethod* method)
{
	((  void (*) (MessageParser_1_t422AF617132E681AA126A8C774D49BC4101614D0 *, Func_1_t8A740DFC3D657757DA5C263E40CD639547B59CB4 *, const RuntimeMethod*))MessageParser_1__ctor_m4B01721875A93C3F52B6AF52315644D793DEBFC5_gshared)(__this, ___factory0, method);
}
// Google.Protobuf.FieldCodec`1<System.Int32> Google.Protobuf.FieldCodec::ForInt32(System.UInt32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR FieldCodec_1_tAB7BF6BD67CE831FB7E8A437894272BD9C511681 * FieldCodec_ForInt32_mA8FC008DA1A37C17DED262F93274DACAEAE60621 (uint32_t ___tag0, const RuntimeMethod* method);
// System.Void Google.Protobuf.JsonFormatter/OriginalEnumValueHelper/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m8A4E99CDBE7F880923AF3978955A596A6CFEAD21 (U3CU3Ec_t5555F991758A0DD73D48BCE4AF9A1B14B7F05FA3 * __this, const RuntimeMethod* method);
// System.Boolean System.Reflection.FieldInfo::get_IsStatic()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool FieldInfo_get_IsStatic_mE36F3A5B2DFF613C704AA56989D90D72760391EB (FieldInfo_t * __this, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerable`1<!!0> System.Reflection.CustomAttributeExtensions::GetCustomAttributes<Google.Protobuf.Reflection.OriginalNameAttribute>(System.Reflection.MemberInfo)
inline RuntimeObject* CustomAttributeExtensions_GetCustomAttributes_TisOriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664_mF22DB14B5869097D2C07559341A761D0ADAAF3EB (MemberInfo_t * ___element0, const RuntimeMethod* method)
{
	return ((  RuntimeObject* (*) (MemberInfo_t *, const RuntimeMethod*))CustomAttributeExtensions_GetCustomAttributes_TisRuntimeObject_m8465B39E995CAEEF162F07ECD2B235B242054872_gshared)(___element0, method);
}
// !!0 System.Linq.Enumerable::FirstOrDefault<Google.Protobuf.Reflection.OriginalNameAttribute>(System.Collections.Generic.IEnumerable`1<!!0>)
inline OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * Enumerable_FirstOrDefault_TisOriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664_m20BF8C804DDE669AD4A21AB34682851EE714877E (RuntimeObject* ___source0, const RuntimeMethod* method)
{
	return ((  OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * (*) (RuntimeObject*, const RuntimeMethod*))Enumerable_FirstOrDefault_TisRuntimeObject_mEA30C68B8D8C7D44DF6690A8FC79E4A48A09C6F9_gshared)(___source0, method);
}
// System.Boolean Google.Protobuf.Reflection.OriginalNameAttribute::get_PreferredAlias()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool OriginalNameAttribute_get_PreferredAlias_m8B1D004B47D36373572F40238135A599E23095B8_inline (OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * __this, const RuntimeMethod* method);
// System.String Google.Protobuf.Reflection.OriginalNameAttribute::get_Name()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* OriginalNameAttribute_get_Name_m35E068ED514B51E172CF5FE5DD917801F045B819_inline (OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * __this, const RuntimeMethod* method);
// Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.Reflection.SourceCodeInfo::get_Descriptor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13 * SourceCodeInfo_get_Descriptor_mC3AEF456721098C0D729AFBFFF1365A7928CC547 (const RuntimeMethod* method);
// Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.Reflection.SourceCodeInfo/Types/Location::get_Descriptor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13 * Location_get_Descriptor_mAF9AF217303A7A050DD8AC21A40A039097EBCC08 (const RuntimeMethod* method);
// System.Void Google.Protobuf.Collections.RepeatedField`1<System.String>::.ctor()
inline void RepeatedField_1__ctor_m397DAD9DD9B6E1993CE6E7CEF796BD2443BA0F44 (RepeatedField_1_t03A3C968DD0FF896E3F108784B11A7CBD313CDE1 * __this, const RuntimeMethod* method)
{
	((  void (*) (RepeatedField_1_t03A3C968DD0FF896E3F108784B11A7CBD313CDE1 *, const RuntimeMethod*))RepeatedField_1__ctor_m91A3D2AF726F859ABF89A26A91DA316F9915FA08_gshared)(__this, method);
}
// System.Void Google.Protobuf.Reflection.SourceCodeInfo/Types/Location::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Location__ctor_mFDC279769BEAE3382B8775935C8683621235F622 (Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4 * __this, const RuntimeMethod* method);
// Google.Protobuf.Collections.RepeatedField`1<T> Google.Protobuf.Collections.RepeatedField`1<System.String>::Clone()
inline RepeatedField_1_t03A3C968DD0FF896E3F108784B11A7CBD313CDE1 * RepeatedField_1_Clone_mF6A2C055409968D973A30440A0F282A762BE6601 (RepeatedField_1_t03A3C968DD0FF896E3F108784B11A7CBD313CDE1 * __this, const RuntimeMethod* method)
{
	return ((  RepeatedField_1_t03A3C968DD0FF896E3F108784B11A7CBD313CDE1 * (*) (RepeatedField_1_t03A3C968DD0FF896E3F108784B11A7CBD313CDE1 *, const RuntimeMethod*))RepeatedField_1_Clone_mC5560F6E0AD410679F9126670E9B3EDBB6908F32_gshared)(__this, method);
}
// System.Void Google.Protobuf.Reflection.SourceCodeInfo/Types/Location::.ctor(Google.Protobuf.Reflection.SourceCodeInfo/Types/Location)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Location__ctor_m28EFCE79F70879A58E3CB6F54CE731BD38BE41DD (Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4 * __this, Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4 * ___other0, const RuntimeMethod* method);
// System.Boolean Google.Protobuf.Reflection.SourceCodeInfo/Types/Location::Equals(Google.Protobuf.Reflection.SourceCodeInfo/Types/Location)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Location_Equals_m8D982EACDA1888B6588CD7689C00D5EBB2985368 (Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4 * __this, Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4 * ___other0, const RuntimeMethod* method);
// System.String Google.Protobuf.Reflection.SourceCodeInfo/Types/Location::get_LeadingComments()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Location_get_LeadingComments_m7C54DD44CC201EEEA4654C52684874B58A20217C (Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4 * __this, const RuntimeMethod* method);
// System.String Google.Protobuf.Reflection.SourceCodeInfo/Types/Location::get_TrailingComments()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Location_get_TrailingComments_m46A2E3D945E49EC181F43757653BA3D43A1F3F86 (Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4 * __this, const RuntimeMethod* method);
// System.Boolean Google.Protobuf.Collections.RepeatedField`1<System.String>::Equals(Google.Protobuf.Collections.RepeatedField`1<T>)
inline bool RepeatedField_1_Equals_m4322AAE559CBB034117AE082A33552981014A7AF (RepeatedField_1_t03A3C968DD0FF896E3F108784B11A7CBD313CDE1 * __this, RepeatedField_1_t03A3C968DD0FF896E3F108784B11A7CBD313CDE1 * ___other0, const RuntimeMethod* method)
{
	return ((  bool (*) (RepeatedField_1_t03A3C968DD0FF896E3F108784B11A7CBD313CDE1 *, RepeatedField_1_t03A3C968DD0FF896E3F108784B11A7CBD313CDE1 *, const RuntimeMethod*))RepeatedField_1_Equals_mEFD2E6DC4C9C82361E51451B49F32E125AFC3F55_gshared)(__this, ___other0, method);
}
// System.Boolean Google.Protobuf.Reflection.SourceCodeInfo/Types/Location::get_HasLeadingComments()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Location_get_HasLeadingComments_m3C25DA28CE42357EA74945D022AD19BB7CEC0997 (Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4 * __this, const RuntimeMethod* method);
// System.Boolean Google.Protobuf.Reflection.SourceCodeInfo/Types/Location::get_HasTrailingComments()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Location_get_HasTrailingComments_mED1CB68639231AE0730E2FB6FB2C8875D89EF1F5 (Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4 * __this, const RuntimeMethod* method);
// System.Void Google.Protobuf.Collections.RepeatedField`1<System.String>::WriteTo(Google.Protobuf.WriteContext&,Google.Protobuf.FieldCodec`1<T>)
inline void RepeatedField_1_WriteTo_m2D9D72EB2016DEE58A88C1C8873C2D512C638215 (RepeatedField_1_t03A3C968DD0FF896E3F108784B11A7CBD313CDE1 * __this, WriteContext_t4011B33FF5B289B23B81D9929C82A44F0A24DFCB * ___ctx0, FieldCodec_1_t379D692C872A4BD7DE0539465056B8EF49D48FA0 * ___codec1, const RuntimeMethod* method)
{
	((  void (*) (RepeatedField_1_t03A3C968DD0FF896E3F108784B11A7CBD313CDE1 *, WriteContext_t4011B33FF5B289B23B81D9929C82A44F0A24DFCB *, FieldCodec_1_t379D692C872A4BD7DE0539465056B8EF49D48FA0 *, const RuntimeMethod*))RepeatedField_1_WriteTo_mE074FA7F4AC1D82D6AC5E3B85819057EE823E6F5_gshared)(__this, ___ctx0, ___codec1, method);
}
// System.Int32 Google.Protobuf.Collections.RepeatedField`1<System.String>::CalculateSize(Google.Protobuf.FieldCodec`1<T>)
inline int32_t RepeatedField_1_CalculateSize_mBD445305760770A9C733116354E5B30CD5DC2EE3 (RepeatedField_1_t03A3C968DD0FF896E3F108784B11A7CBD313CDE1 * __this, FieldCodec_1_t379D692C872A4BD7DE0539465056B8EF49D48FA0 * ___codec0, const RuntimeMethod* method)
{
	return ((  int32_t (*) (RepeatedField_1_t03A3C968DD0FF896E3F108784B11A7CBD313CDE1 *, FieldCodec_1_t379D692C872A4BD7DE0539465056B8EF49D48FA0 *, const RuntimeMethod*))RepeatedField_1_CalculateSize_m79E728BA3A01ACB1B539A567DBC0B484DA1A0C4D_gshared)(__this, ___codec0, method);
}
// System.Void Google.Protobuf.Reflection.SourceCodeInfo/Types/Location::set_LeadingComments(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Location_set_LeadingComments_m5C93169FF580ED160690B569AD1DB27A25CF1084 (Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void Google.Protobuf.Reflection.SourceCodeInfo/Types/Location::set_TrailingComments(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Location_set_TrailingComments_mD43CECC13E2083938944FEF61AEA9ADDD195E17A (Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void Google.Protobuf.Collections.RepeatedField`1<System.String>::Add(System.Collections.Generic.IEnumerable`1<T>)
inline void RepeatedField_1_Add_m9B3360A97708D5F88CB74E729387922F7CFFF0FE (RepeatedField_1_t03A3C968DD0FF896E3F108784B11A7CBD313CDE1 * __this, RuntimeObject* ___values0, const RuntimeMethod* method)
{
	((  void (*) (RepeatedField_1_t03A3C968DD0FF896E3F108784B11A7CBD313CDE1 *, RuntimeObject*, const RuntimeMethod*))RepeatedField_1_Add_m55B7D26CA8F0985363E10FE56D51F0A78F40847D_gshared)(__this, ___values0, method);
}
// System.Void Google.Protobuf.Collections.RepeatedField`1<System.String>::AddEntriesFrom(Google.Protobuf.ParseContext&,Google.Protobuf.FieldCodec`1<T>)
inline void RepeatedField_1_AddEntriesFrom_m1CD228D26654811414016D368DDCDD8D3AFC85A4 (RepeatedField_1_t03A3C968DD0FF896E3F108784B11A7CBD313CDE1 * __this, ParseContext_t288EAC5562A1D7E0FF8A06DBA58B8CF732ECE349 * ___ctx0, FieldCodec_1_t379D692C872A4BD7DE0539465056B8EF49D48FA0 * ___codec1, const RuntimeMethod* method)
{
	((  void (*) (RepeatedField_1_t03A3C968DD0FF896E3F108784B11A7CBD313CDE1 *, ParseContext_t288EAC5562A1D7E0FF8A06DBA58B8CF732ECE349 *, FieldCodec_1_t379D692C872A4BD7DE0539465056B8EF49D48FA0 *, const RuntimeMethod*))RepeatedField_1_AddEntriesFrom_mC21CCB0B2492BC3101179D04E0C8559EFC644214_gshared)(__this, ___ctx0, ___codec1, method);
}
// System.Void System.Func`1<Google.Protobuf.Reflection.SourceCodeInfo/Types/Location>::.ctor(System.Object,System.IntPtr)
inline void Func_1__ctor_m2DA47B0B49E42E600E186F7701E4C322C935D1D4 (Func_1_t08ED00E63E70686FA636AFAACFA58BD631D35696 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_1_t08ED00E63E70686FA636AFAACFA58BD631D35696 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Func_1__ctor_m2A4FE889FB540EA198F7757D17DC2290461E5EE9_gshared)(__this, ___object0, ___method1, method);
}
// System.Void Google.Protobuf.MessageParser`1<Google.Protobuf.Reflection.SourceCodeInfo/Types/Location>::.ctor(System.Func`1<T>)
inline void MessageParser_1__ctor_m58870D9692E7115AE9C0CE22F33EF7950ED7BF19 (MessageParser_1_t09C483F589C454D2B080958F963D017315003983 * __this, Func_1_t08ED00E63E70686FA636AFAACFA58BD631D35696 * ___factory0, const RuntimeMethod* method)
{
	((  void (*) (MessageParser_1_t09C483F589C454D2B080958F963D017315003983 *, Func_1_t08ED00E63E70686FA636AFAACFA58BD631D35696 *, const RuntimeMethod*))MessageParser_1__ctor_m4B01721875A93C3F52B6AF52315644D793DEBFC5_gshared)(__this, ___factory0, method);
}
// Google.Protobuf.FieldCodec`1<System.String> Google.Protobuf.FieldCodec::ForString(System.UInt32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR FieldCodec_1_t379D692C872A4BD7DE0539465056B8EF49D48FA0 * FieldCodec_ForString_m686A669CFD9CF6A06AE0329BB75BFF8D20C17A32 (uint32_t ___tag0, const RuntimeMethod* method);
// Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.Reflection.UninterpretedOption::get_Descriptor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13 * UninterpretedOption_get_Descriptor_m178F55E2F36773B937FB50177A2EA1EED60B24BA (const RuntimeMethod* method);
// Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart::get_Descriptor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13 * NamePart_get_Descriptor_mDAF09A4912482470FFDD7664FB4A245050AB4D99 (const RuntimeMethod* method);
// System.Void Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NamePart__ctor_mA9A9103300EE0850829D3BFE67B52EAD4512F085 (NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22 * __this, const RuntimeMethod* method);
// System.Void Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart::.ctor(Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NamePart__ctor_mFBC708208AD503A756034768ABF66A5E5E0AF4DD (NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22 * __this, NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22 * ___other0, const RuntimeMethod* method);
// System.Boolean Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart::Equals(Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool NamePart_Equals_m4F68F7BFA6DC8252C7BF46F9B7B7A4561BC9B297 (NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22 * __this, NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22 * ___other0, const RuntimeMethod* method);
// System.String Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart::get_NamePart_()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* NamePart_get_NamePart__m745E8A77A6D196C10AEBD19CD97CD8FFFD6771F9 (NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22 * __this, const RuntimeMethod* method);
// System.Boolean Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart::get_IsExtension()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool NamePart_get_IsExtension_mF1F181A90E440604432916AA943CE44DA7BADDEA (NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22 * __this, const RuntimeMethod* method);
// System.Boolean Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart::get_HasNamePart_()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool NamePart_get_HasNamePart__mCEDB12E23C8A15CAD08E15A07D98285265637660 (NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22 * __this, const RuntimeMethod* method);
// System.Boolean Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart::get_HasIsExtension()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool NamePart_get_HasIsExtension_m5A187250EBDBA107E51D79C0BD502D42D5965B9D (NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22 * __this, const RuntimeMethod* method);
// System.Int32 System.Boolean::GetHashCode()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Boolean_GetHashCode_m03AF8B3CECAE9106C44A00E3B33E51CBFC45C411 (bool* __this, const RuntimeMethod* method);
// System.Void Google.Protobuf.WriteContext::WriteBool(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WriteContext_WriteBool_m68B7D58774A48FFC3FF34D9A6F770E579EEFA7DF (WriteContext_t4011B33FF5B289B23B81D9929C82A44F0A24DFCB * __this, bool ___value0, const RuntimeMethod* method);
// System.Void Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart::set_NamePart_(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NamePart_set_NamePart__mB05B00F6AEB8BCC4C03176FB92DADB8C7A49B55E (NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart::set_IsExtension(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NamePart_set_IsExtension_m01E6A8E808AB54ACA78194DB40DC1D897F7713A2 (NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22 * __this, bool ___value0, const RuntimeMethod* method);
// System.Boolean Google.Protobuf.ParseContext::ReadBool()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool ParseContext_ReadBool_m097C5083B8554A5F33B1799DD8BF30926A5942BE_inline (ParseContext_t288EAC5562A1D7E0FF8A06DBA58B8CF732ECE349 * __this, const RuntimeMethod* method);
// System.Void System.Func`1<Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart>::.ctor(System.Object,System.IntPtr)
inline void Func_1__ctor_mFFBE264549A7AF17CBECB2E724E4B75F436FD857 (Func_1_t732F679FB575BFC105666600B1FC8E28B480BBAB * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_1_t732F679FB575BFC105666600B1FC8E28B480BBAB *, RuntimeObject *, intptr_t, const RuntimeMethod*))Func_1__ctor_m2A4FE889FB540EA198F7757D17DC2290461E5EE9_gshared)(__this, ___object0, ___method1, method);
}
// System.Void Google.Protobuf.MessageParser`1<Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart>::.ctor(System.Func`1<T>)
inline void MessageParser_1__ctor_m6B193EF7595551528130D40FD5AF408E99171248 (MessageParser_1_tE61087F0F9CB9EE7534691A864269E589F1453DF * __this, Func_1_t732F679FB575BFC105666600B1FC8E28B480BBAB * ___factory0, const RuntimeMethod* method)
{
	((  void (*) (MessageParser_1_tE61087F0F9CB9EE7534691A864269E589F1453DF *, Func_1_t732F679FB575BFC105666600B1FC8E28B480BBAB *, const RuntimeMethod*))MessageParser_1__ctor_m4B01721875A93C3F52B6AF52315644D793DEBFC5_gshared)(__this, ___factory0, method);
}
// System.Void Google.Protobuf.Reflection.DescriptorProto/Types/ExtensionRange/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_mAB207C6304C4309948492B33329C4030BBDA3EB3 (U3CU3Ec_tC3789B2A2639D39522B7FC88CE44B0FF78387D85 * __this, const RuntimeMethod* method);
// System.Void Google.Protobuf.Reflection.DescriptorProto/Types/ExtensionRange::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExtensionRange__ctor_mCE3CBBCDC18FA5EA4ADBF4AF4AF53D13327D323B (ExtensionRange_t355AED379D3458AC80443047C2DF72AA44639D84 * __this, const RuntimeMethod* method);
// System.Void Google.Protobuf.Reflection.DescriptorProto/Types/ReservedRange/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m5826A097FE555448C5C789E783537C4451C2909A (U3CU3Ec_t14ED7AF2EE0452B68867D51843E5E3FBF40F7D2D * __this, const RuntimeMethod* method);
// System.Void Google.Protobuf.Reflection.DescriptorProto/Types/ReservedRange::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ReservedRange__ctor_mB2F8257CCD2E526672CB4B552FD6799706F38CD5 (ReservedRange_tAED8F62AF040F5F133732196696893B313EA458B * __this, const RuntimeMethod* method);
// System.Void Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m9D85A171DC63EF24760F067CDB4D72BDE4E10F18 (U3CU3Ec_t0FB8D11271B41B39C63CE8A142DE7F8F87630794 * __this, const RuntimeMethod* method);
// System.Void Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_mCC6C861EA46D5F38CE7B1D231D970DAFF8929007 (U3CU3Ec_t48164166658FE6E4AA4BC81699272A7660D5A345 * __this, const RuntimeMethod* method);
// System.Void Google.Protobuf.Reflection.SourceCodeInfo/Types/Location/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m0D7F18F9E242952A4718672889E7A6D4750E8C15 (U3CU3Ec_tE6E47C051562B8F8FCE9347E1B99439CF27AB1AF * __this, const RuntimeMethod* method);
// System.Void Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m14121C5533461FB8EFB0C4F01A4D5BD0AD91EA7F (U3CU3Ec_t70C29F3AE73CD37C0D77DCC96A139645BE59A453 * __this, const RuntimeMethod* method);
// System.UInt32 Google.Protobuf.ParsingPrimitives::ParseRawVarint32(System.ReadOnlySpan`1<System.Byte>&,Google.Protobuf.ParserInternalState&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint32_t ParsingPrimitives_ParseRawVarint32_m603BEFB03EAD05D2C5B543DA459AE0E987BB92E7 (ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 * ___buffer0, ParserInternalState_t08A261A9F88B2B769A10BED876C9622FEAFD2596 * ___state1, const RuntimeMethod* method);
// System.UInt32 Google.Protobuf.ParsingPrimitives::ParseTag(System.ReadOnlySpan`1<System.Byte>&,Google.Protobuf.ParserInternalState&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint32_t ParsingPrimitives_ParseTag_mF467A1C5FE6962837D3E138E0D2DD3F159EE6A1E (ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 * ___buffer0, ParserInternalState_t08A261A9F88B2B769A10BED876C9622FEAFD2596 * ___state1, const RuntimeMethod* method);
// System.String Google.Protobuf.ParsingPrimitives::ReadString(System.ReadOnlySpan`1<System.Byte>&,Google.Protobuf.ParserInternalState&)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* ParsingPrimitives_ReadString_mF4065E9BBE004B97FBA2D159B6B175A1876AD495_inline (ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 * ___buffer0, ParserInternalState_t08A261A9F88B2B769A10BED876C9622FEAFD2596 * ___state1, const RuntimeMethod* method);
// System.UInt64 Google.Protobuf.ParsingPrimitives::ParseRawVarint64(System.ReadOnlySpan`1<System.Byte>&,Google.Protobuf.ParserInternalState&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint64_t ParsingPrimitives_ParseRawVarint64_m991C711AC6C490742D2A18D54B518A73603B29CF (ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 * ___buffer0, ParserInternalState_t08A261A9F88B2B769A10BED876C9622FEAFD2596 * ___state1, const RuntimeMethod* method);
// System.Int32 Google.Protobuf.ParsingPrimitives::ParseLength(System.ReadOnlySpan`1<System.Byte>&,Google.Protobuf.ParserInternalState&)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t ParsingPrimitives_ParseLength_mC2FC8A77E6BCAB09D6149C6EDF2D080E76EC945F_inline (ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 * ___buffer0, ParserInternalState_t08A261A9F88B2B769A10BED876C9622FEAFD2596 * ___state1, const RuntimeMethod* method);
// System.String Google.Protobuf.ParsingPrimitives::ReadRawString(System.ReadOnlySpan`1<System.Byte>&,Google.Protobuf.ParserInternalState&,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* ParsingPrimitives_ReadRawString_mFFD978AA6F9021997AA4154B07D4A5B47ABB345C (ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 * ___buffer0, ParserInternalState_t08A261A9F88B2B769A10BED876C9622FEAFD2596 * ___state1, int32_t ___length2, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Google.Protobuf.MessageParser`1<Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange> Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange::get_Parser()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR MessageParser_1_tCCF97EFBB4482323562EEB1F40E83233B2D32D55 * EnumReservedRange_get_Parser_mCA427AC75416A92344F6D66F4CCC3578CFE40DAA (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41_il2cpp_TypeInfo_var);
		MessageParser_1_tCCF97EFBB4482323562EEB1F40E83233B2D32D55 * L_0 = ((EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41_StaticFields*)il2cpp_codegen_static_fields_for(EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41_il2cpp_TypeInfo_var))->get__parser_0();
		return L_0;
	}
}
// Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange::get_Descriptor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13 * EnumReservedRange_get_Descriptor_mD31EA935A954A7AAF19EB17AED0FEE0644E39056 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EnumDescriptorProto_t0E60F3628E51BC270FDA740DCBC83DBF04FA0478_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IList_1_tACE6E5172CFB9B21666313779F73E7389A285312_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(EnumDescriptorProto_t0E60F3628E51BC270FDA740DCBC83DBF04FA0478_il2cpp_TypeInfo_var);
		MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13 * L_0;
		L_0 = EnumDescriptorProto_get_Descriptor_mB89737A900858354FD200A724032DF8194CF571C(/*hidden argument*/NULL);
		NullCheck(L_0);
		RuntimeObject* L_1;
		L_1 = MessageDescriptor_get_NestedTypes_m34A524E3C7D560C1537685CF6291AB60DA58FE7E_inline(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13 * L_2;
		L_2 = InterfaceFuncInvoker1< MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13 *, int32_t >::Invoke(0 /* !0 System.Collections.Generic.IList`1<Google.Protobuf.Reflection.MessageDescriptor>::get_Item(System.Int32) */, IList_1_tACE6E5172CFB9B21666313779F73E7389A285312_il2cpp_TypeInfo_var, L_1, 0);
		return L_2;
	}
}
// Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange::pb::Google.Protobuf.IMessage.get_Descriptor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13 * EnumReservedRange_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_mB1D21B0C42DD2B880258B2F6190BC9EE0D8A98AD (EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41_il2cpp_TypeInfo_var);
		MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13 * L_0;
		L_0 = EnumReservedRange_get_Descriptor_mD31EA935A954A7AAF19EB17AED0FEE0644E39056(/*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnumReservedRange__ctor_m02904F51FF5FE5B92BAA1E816D3A1B13534A6E0C (EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange::.ctor(Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnumReservedRange__ctor_m4F7BB9BB1007668742F085BDA2A82E889F6D1B6A (EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41 * __this, EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41 * ___other0, const RuntimeMethod* method)
{
	{
		EnumReservedRange__ctor_m02904F51FF5FE5B92BAA1E816D3A1B13534A6E0C(__this, /*hidden argument*/NULL);
		EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41 * L_0 = ___other0;
		NullCheck(L_0);
		int32_t L_1 = L_0->get__hasBits0_2();
		__this->set__hasBits0_2(L_1);
		EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41 * L_2 = ___other0;
		NullCheck(L_2);
		int32_t L_3 = L_2->get_start__4();
		__this->set_start__4(L_3);
		EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41 * L_4 = ___other0;
		NullCheck(L_4);
		int32_t L_5 = L_4->get_end__6();
		__this->set_end__6(L_5);
		EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41 * L_6 = ___other0;
		NullCheck(L_6);
		UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 * L_7 = L_6->get__unknownFields_1();
		UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 * L_8;
		L_8 = UnknownFieldSet_Clone_mBCB3196178FBF9E17BF3C686C6C6DC214D0BBDEA(L_7, /*hidden argument*/NULL);
		__this->set__unknownFields_1(L_8);
		return;
	}
}
// Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41 * EnumReservedRange_Clone_m0478CEBDD1CC260098613EC9608FA8DA619521A4 (EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41 * L_0 = (EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41 *)il2cpp_codegen_object_new(EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41_il2cpp_TypeInfo_var);
		EnumReservedRange__ctor_m4F7BB9BB1007668742F085BDA2A82E889F6D1B6A(L_0, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Int32 Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange::get_Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t EnumReservedRange_get_Start_m425FFA90089C7FCDD1C01FA5C950E5477F17DCC5 (EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get__hasBits0_2();
		if (!((int32_t)((int32_t)L_0&(int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_1 = __this->get_start__4();
		return L_1;
	}

IL_0011:
	{
		IL2CPP_RUNTIME_CLASS_INIT(EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41_il2cpp_TypeInfo_var);
		int32_t L_2 = ((EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41_StaticFields*)il2cpp_codegen_static_fields_for(EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41_il2cpp_TypeInfo_var))->get_StartDefaultValue_3();
		return L_2;
	}
}
// System.Void Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange::set_Start(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnumReservedRange_set_Start_m69F0A6CB6629086763121A8CD5E295415242BE82 (EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get__hasBits0_2();
		__this->set__hasBits0_2(((int32_t)((int32_t)L_0|(int32_t)1)));
		int32_t L_1 = ___value0;
		__this->set_start__4(L_1);
		return;
	}
}
// System.Boolean Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange::get_HasStart()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool EnumReservedRange_get_HasStart_m394D0FDEAAE8B2C54B2B7ACE2DAFD1F354B2AFA8 (EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get__hasBits0_2();
		return (bool)((!(((uint32_t)((int32_t)((int32_t)L_0&(int32_t)1))) <= ((uint32_t)0)))? 1 : 0);
	}
}
// System.Int32 Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange::get_End()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t EnumReservedRange_get_End_m012A5FA46B5F0D66FBCEAA9B17BCE31CB105E0D9 (EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get__hasBits0_2();
		if (!((int32_t)((int32_t)L_0&(int32_t)2)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_1 = __this->get_end__6();
		return L_1;
	}

IL_0011:
	{
		IL2CPP_RUNTIME_CLASS_INIT(EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41_il2cpp_TypeInfo_var);
		int32_t L_2 = ((EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41_StaticFields*)il2cpp_codegen_static_fields_for(EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41_il2cpp_TypeInfo_var))->get_EndDefaultValue_5();
		return L_2;
	}
}
// System.Void Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange::set_End(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnumReservedRange_set_End_mF993946771523BFE0D542CD2C3ACB19ADE9C1CC3 (EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get__hasBits0_2();
		__this->set__hasBits0_2(((int32_t)((int32_t)L_0|(int32_t)2)));
		int32_t L_1 = ___value0;
		__this->set_end__6(L_1);
		return;
	}
}
// System.Boolean Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange::get_HasEnd()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool EnumReservedRange_get_HasEnd_m743B6947E1F9A8CD3CB43B5CC0056313942F1AE1 (EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get__hasBits0_2();
		return (bool)((!(((uint32_t)((int32_t)((int32_t)L_0&(int32_t)2))) <= ((uint32_t)0)))? 1 : 0);
	}
}
// System.Boolean Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange::Equals(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool EnumReservedRange_Equals_m3728912F8E0AD0EA5A74FD69323CF9F73465DA04 (EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41 * __this, RuntimeObject * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject * L_0 = ___other0;
		bool L_1;
		L_1 = EnumReservedRange_Equals_m1FB9B43A21E9D232DFE20485F50C4673D84948E1(__this, ((EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41 *)IsInstSealed((RuntimeObject*)L_0, EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange::Equals(Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool EnumReservedRange_Equals_m1FB9B43A21E9D232DFE20485F50C4673D84948E1 (EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41 * __this, EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41 * ___other0, const RuntimeMethod* method)
{
	{
		EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41 * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0005;
		}
	}
	{
		return (bool)0;
	}

IL_0005:
	{
		EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41 * L_1 = ___other0;
		if ((!(((RuntimeObject*)(EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41 *)L_1) == ((RuntimeObject*)(EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41 *)__this))))
		{
			goto IL_000b;
		}
	}
	{
		return (bool)1;
	}

IL_000b:
	{
		int32_t L_2;
		L_2 = EnumReservedRange_get_Start_m425FFA90089C7FCDD1C01FA5C950E5477F17DCC5(__this, /*hidden argument*/NULL);
		EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41 * L_3 = ___other0;
		NullCheck(L_3);
		int32_t L_4;
		L_4 = EnumReservedRange_get_Start_m425FFA90089C7FCDD1C01FA5C950E5477F17DCC5(L_3, /*hidden argument*/NULL);
		if ((((int32_t)L_2) == ((int32_t)L_4)))
		{
			goto IL_001b;
		}
	}
	{
		return (bool)0;
	}

IL_001b:
	{
		int32_t L_5;
		L_5 = EnumReservedRange_get_End_m012A5FA46B5F0D66FBCEAA9B17BCE31CB105E0D9(__this, /*hidden argument*/NULL);
		EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41 * L_6 = ___other0;
		NullCheck(L_6);
		int32_t L_7;
		L_7 = EnumReservedRange_get_End_m012A5FA46B5F0D66FBCEAA9B17BCE31CB105E0D9(L_6, /*hidden argument*/NULL);
		if ((((int32_t)L_5) == ((int32_t)L_7)))
		{
			goto IL_002b;
		}
	}
	{
		return (bool)0;
	}

IL_002b:
	{
		UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 * L_8 = __this->get__unknownFields_1();
		EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41 * L_9 = ___other0;
		NullCheck(L_9);
		UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 * L_10 = L_9->get__unknownFields_1();
		bool L_11;
		L_11 = Object_Equals_mBE334AF263BDADA1D0F1DE7E14456A63E47F8053(L_8, L_10, /*hidden argument*/NULL);
		return L_11;
	}
}
// System.Int32 Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange::GetHashCode()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t EnumReservedRange_GetHashCode_m6415E55F96EE5B92BBE2965CD1FE5D37C2604231 (EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		V_0 = 1;
		bool L_0;
		L_0 = EnumReservedRange_get_HasStart_m394D0FDEAAE8B2C54B2B7ACE2DAFD1F354B2AFA8(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		int32_t L_1 = V_0;
		int32_t L_2;
		L_2 = EnumReservedRange_get_Start_m425FFA90089C7FCDD1C01FA5C950E5477F17DCC5(__this, /*hidden argument*/NULL);
		V_1 = L_2;
		int32_t L_3;
		L_3 = Int32_GetHashCode_mEDD3F492A5F7CF021125AE3F38E2B8F8743FC667((int32_t*)(&V_1), /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)L_1^(int32_t)L_3));
	}

IL_001b:
	{
		bool L_4;
		L_4 = EnumReservedRange_get_HasEnd_m743B6947E1F9A8CD3CB43B5CC0056313942F1AE1(__this, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0034;
		}
	}
	{
		int32_t L_5 = V_0;
		int32_t L_6;
		L_6 = EnumReservedRange_get_End_m012A5FA46B5F0D66FBCEAA9B17BCE31CB105E0D9(__this, /*hidden argument*/NULL);
		V_1 = L_6;
		int32_t L_7;
		L_7 = Int32_GetHashCode_mEDD3F492A5F7CF021125AE3F38E2B8F8743FC667((int32_t*)(&V_1), /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)L_5^(int32_t)L_7));
	}

IL_0034:
	{
		UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 * L_8 = __this->get__unknownFields_1();
		if (!L_8)
		{
			goto IL_004a;
		}
	}
	{
		int32_t L_9 = V_0;
		UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 * L_10 = __this->get__unknownFields_1();
		NullCheck(L_10);
		int32_t L_11;
		L_11 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, L_10);
		V_0 = ((int32_t)((int32_t)L_9^(int32_t)L_11));
	}

IL_004a:
	{
		int32_t L_12 = V_0;
		return L_12;
	}
}
// System.String Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* EnumReservedRange_ToString_mA9995D4DB4A84520B09678054B780C0714C18E32 (EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&JsonFormatter_t29EDD2B5D01B4FD88FD4DBF274554818F8ED11C7_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonFormatter_t29EDD2B5D01B4FD88FD4DBF274554818F8ED11C7_il2cpp_TypeInfo_var);
		String_t* L_0;
		L_0 = JsonFormatter_ToDiagnosticString_mB0CBCBDBF65F17FC52DEB9E0C49184E44F3F2EF1(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange::WriteTo(Google.Protobuf.CodedOutputStream)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnumReservedRange_WriteTo_m7D1FE13C055716582D6BC6B24B10AE1654DA1316 (EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41 * __this, CodedOutputStream_t2871B0D17F920BFB54B564E2295D9753721642E6 * ___output0, const RuntimeMethod* method)
{
	{
		CodedOutputStream_t2871B0D17F920BFB54B564E2295D9753721642E6 * L_0 = ___output0;
		NullCheck(L_0);
		CodedOutputStream_WriteRawMessage_m05CED72DF9A800E9BFB28956F21E7B4416E03D11(L_0, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange::pb::Google.Protobuf.IBufferMessage.InternalWriteTo(Google.Protobuf.WriteContext&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnumReservedRange_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m40335111DC4E167612FEF3662BE9124300D3480A (EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41 * __this, WriteContext_t4011B33FF5B289B23B81D9929C82A44F0A24DFCB * ___output0, const RuntimeMethod* method)
{
	{
		bool L_0;
		L_0 = EnumReservedRange_get_HasStart_m394D0FDEAAE8B2C54B2B7ACE2DAFD1F354B2AFA8(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		WriteContext_t4011B33FF5B289B23B81D9929C82A44F0A24DFCB * L_1 = ___output0;
		WriteContext_WriteRawTag_m3E37F0B83B4E8B7C7E1CC6472D527AA22B11DA5A((WriteContext_t4011B33FF5B289B23B81D9929C82A44F0A24DFCB *)L_1, (uint8_t)8, /*hidden argument*/NULL);
		WriteContext_t4011B33FF5B289B23B81D9929C82A44F0A24DFCB * L_2 = ___output0;
		int32_t L_3;
		L_3 = EnumReservedRange_get_Start_m425FFA90089C7FCDD1C01FA5C950E5477F17DCC5(__this, /*hidden argument*/NULL);
		WriteContext_WriteInt32_mCC3646C9CF9A13E2BEBC15A8E566669C913D4F65((WriteContext_t4011B33FF5B289B23B81D9929C82A44F0A24DFCB *)L_2, L_3, /*hidden argument*/NULL);
	}

IL_001b:
	{
		bool L_4;
		L_4 = EnumReservedRange_get_HasEnd_m743B6947E1F9A8CD3CB43B5CC0056313942F1AE1(__this, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0037;
		}
	}
	{
		WriteContext_t4011B33FF5B289B23B81D9929C82A44F0A24DFCB * L_5 = ___output0;
		WriteContext_WriteRawTag_m3E37F0B83B4E8B7C7E1CC6472D527AA22B11DA5A((WriteContext_t4011B33FF5B289B23B81D9929C82A44F0A24DFCB *)L_5, (uint8_t)((int32_t)16), /*hidden argument*/NULL);
		WriteContext_t4011B33FF5B289B23B81D9929C82A44F0A24DFCB * L_6 = ___output0;
		int32_t L_7;
		L_7 = EnumReservedRange_get_End_m012A5FA46B5F0D66FBCEAA9B17BCE31CB105E0D9(__this, /*hidden argument*/NULL);
		WriteContext_WriteInt32_mCC3646C9CF9A13E2BEBC15A8E566669C913D4F65((WriteContext_t4011B33FF5B289B23B81D9929C82A44F0A24DFCB *)L_6, L_7, /*hidden argument*/NULL);
	}

IL_0037:
	{
		UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 * L_8 = __this->get__unknownFields_1();
		if (!L_8)
		{
			goto IL_004b;
		}
	}
	{
		UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 * L_9 = __this->get__unknownFields_1();
		WriteContext_t4011B33FF5B289B23B81D9929C82A44F0A24DFCB * L_10 = ___output0;
		NullCheck(L_9);
		UnknownFieldSet_WriteTo_mECF1EFA7AF68EBBEDDD046865B2C17131B969E1B(L_9, (WriteContext_t4011B33FF5B289B23B81D9929C82A44F0A24DFCB *)L_10, /*hidden argument*/NULL);
	}

IL_004b:
	{
		return;
	}
}
// System.Int32 Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange::CalculateSize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t EnumReservedRange_CalculateSize_m0A62C551FDA64908B0C58397CD1428C5C71BF2F5 (EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CodedOutputStream_t2871B0D17F920BFB54B564E2295D9753721642E6_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		bool L_0;
		L_0 = EnumReservedRange_get_HasStart_m394D0FDEAAE8B2C54B2B7ACE2DAFD1F354B2AFA8(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		int32_t L_1 = V_0;
		int32_t L_2;
		L_2 = EnumReservedRange_get_Start_m425FFA90089C7FCDD1C01FA5C950E5477F17DCC5(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CodedOutputStream_t2871B0D17F920BFB54B564E2295D9753721642E6_il2cpp_TypeInfo_var);
		int32_t L_3;
		L_3 = CodedOutputStream_ComputeInt32Size_m03C7A226C41F660D0A853D576117A6386B9D0802(L_2, /*hidden argument*/NULL);
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_1, (int32_t)((int32_t)il2cpp_codegen_add((int32_t)1, (int32_t)L_3))));
	}

IL_001a:
	{
		bool L_4;
		L_4 = EnumReservedRange_get_HasEnd_m743B6947E1F9A8CD3CB43B5CC0056313942F1AE1(__this, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0032;
		}
	}
	{
		int32_t L_5 = V_0;
		int32_t L_6;
		L_6 = EnumReservedRange_get_End_m012A5FA46B5F0D66FBCEAA9B17BCE31CB105E0D9(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CodedOutputStream_t2871B0D17F920BFB54B564E2295D9753721642E6_il2cpp_TypeInfo_var);
		int32_t L_7;
		L_7 = CodedOutputStream_ComputeInt32Size_m03C7A226C41F660D0A853D576117A6386B9D0802(L_6, /*hidden argument*/NULL);
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_5, (int32_t)((int32_t)il2cpp_codegen_add((int32_t)1, (int32_t)L_7))));
	}

IL_0032:
	{
		UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 * L_8 = __this->get__unknownFields_1();
		if (!L_8)
		{
			goto IL_0048;
		}
	}
	{
		int32_t L_9 = V_0;
		UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 * L_10 = __this->get__unknownFields_1();
		NullCheck(L_10);
		int32_t L_11;
		L_11 = UnknownFieldSet_CalculateSize_m0F48042AFDC7E6795E7A99D8B3BE0D5C61C96868(L_10, /*hidden argument*/NULL);
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_9, (int32_t)L_11));
	}

IL_0048:
	{
		int32_t L_12 = V_0;
		return L_12;
	}
}
// System.Void Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange::MergeFrom(Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnumReservedRange_MergeFrom_m2FC3A667F2AF4C85C2C7D3AF0E4E471D92019CD2 (EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41 * __this, EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41 * ___other0, const RuntimeMethod* method)
{
	{
		EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41 * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0004;
		}
	}
	{
		return;
	}

IL_0004:
	{
		EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41 * L_1 = ___other0;
		NullCheck(L_1);
		bool L_2;
		L_2 = EnumReservedRange_get_HasStart_m394D0FDEAAE8B2C54B2B7ACE2DAFD1F354B2AFA8(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0018;
		}
	}
	{
		EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41 * L_3 = ___other0;
		NullCheck(L_3);
		int32_t L_4;
		L_4 = EnumReservedRange_get_Start_m425FFA90089C7FCDD1C01FA5C950E5477F17DCC5(L_3, /*hidden argument*/NULL);
		EnumReservedRange_set_Start_m69F0A6CB6629086763121A8CD5E295415242BE82(__this, L_4, /*hidden argument*/NULL);
	}

IL_0018:
	{
		EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41 * L_5 = ___other0;
		NullCheck(L_5);
		bool L_6;
		L_6 = EnumReservedRange_get_HasEnd_m743B6947E1F9A8CD3CB43B5CC0056313942F1AE1(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_002c;
		}
	}
	{
		EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41 * L_7 = ___other0;
		NullCheck(L_7);
		int32_t L_8;
		L_8 = EnumReservedRange_get_End_m012A5FA46B5F0D66FBCEAA9B17BCE31CB105E0D9(L_7, /*hidden argument*/NULL);
		EnumReservedRange_set_End_mF993946771523BFE0D542CD2C3ACB19ADE9C1CC3(__this, L_8, /*hidden argument*/NULL);
	}

IL_002c:
	{
		UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 * L_9 = __this->get__unknownFields_1();
		EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41 * L_10 = ___other0;
		NullCheck(L_10);
		UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 * L_11 = L_10->get__unknownFields_1();
		UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 * L_12;
		L_12 = UnknownFieldSet_MergeFrom_m3175C4FF04375A5EF7DC4E3245C6606303344F85(L_9, L_11, /*hidden argument*/NULL);
		__this->set__unknownFields_1(L_12);
		return;
	}
}
// System.Void Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange::MergeFrom(Google.Protobuf.CodedInputStream)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnumReservedRange_MergeFrom_mCD0205C8ED2CEB21A1C6F16D967019975292C0A0 (EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41 * __this, CodedInputStream_tC76F9515605395C11D7F9437E0E06C828232B0C8 * ___input0, const RuntimeMethod* method)
{
	{
		CodedInputStream_tC76F9515605395C11D7F9437E0E06C828232B0C8 * L_0 = ___input0;
		NullCheck(L_0);
		CodedInputStream_ReadRawMessage_m77E217968CD5654CC717E7DA57906C0219372943(L_0, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange::pb::Google.Protobuf.IBufferMessage.InternalMergeFrom(Google.Protobuf.ParseContext&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnumReservedRange_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_mDCF2D669D389A95648C8864C743EBF87D6CAC659 (EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41 * __this, ParseContext_t288EAC5562A1D7E0FF8A06DBA58B8CF732ECE349 * ___input0, const RuntimeMethod* method)
{
	uint32_t V_0 = 0;
	{
		goto IL_0039;
	}

IL_0002:
	{
		uint32_t L_0 = V_0;
		if ((((int32_t)L_0) == ((int32_t)8)))
		{
			goto IL_001f;
		}
	}
	{
		uint32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)((int32_t)16))))
		{
			goto IL_002d;
		}
	}
	{
		UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 * L_2 = __this->get__unknownFields_1();
		ParseContext_t288EAC5562A1D7E0FF8A06DBA58B8CF732ECE349 * L_3 = ___input0;
		UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 * L_4;
		L_4 = UnknownFieldSet_MergeFieldFrom_m6D9E32ADBB0194941C65CD43522F53C3A2378283(L_2, (ParseContext_t288EAC5562A1D7E0FF8A06DBA58B8CF732ECE349 *)L_3, /*hidden argument*/NULL);
		__this->set__unknownFields_1(L_4);
		goto IL_0039;
	}

IL_001f:
	{
		ParseContext_t288EAC5562A1D7E0FF8A06DBA58B8CF732ECE349 * L_5 = ___input0;
		int32_t L_6;
		L_6 = ParseContext_ReadInt32_m275087DD36E27495CC2EE76EEB08BC90EFAF94B9_inline((ParseContext_t288EAC5562A1D7E0FF8A06DBA58B8CF732ECE349 *)L_5, /*hidden argument*/NULL);
		EnumReservedRange_set_Start_m69F0A6CB6629086763121A8CD5E295415242BE82(__this, L_6, /*hidden argument*/NULL);
		goto IL_0039;
	}

IL_002d:
	{
		ParseContext_t288EAC5562A1D7E0FF8A06DBA58B8CF732ECE349 * L_7 = ___input0;
		int32_t L_8;
		L_8 = ParseContext_ReadInt32_m275087DD36E27495CC2EE76EEB08BC90EFAF94B9_inline((ParseContext_t288EAC5562A1D7E0FF8A06DBA58B8CF732ECE349 *)L_7, /*hidden argument*/NULL);
		EnumReservedRange_set_End_mF993946771523BFE0D542CD2C3ACB19ADE9C1CC3(__this, L_8, /*hidden argument*/NULL);
	}

IL_0039:
	{
		ParseContext_t288EAC5562A1D7E0FF8A06DBA58B8CF732ECE349 * L_9 = ___input0;
		uint32_t L_10;
		L_10 = ParseContext_ReadTag_mEE687A18078235454AEA63F1F61E9E3E9EB0260F_inline((ParseContext_t288EAC5562A1D7E0FF8A06DBA58B8CF732ECE349 *)L_9, /*hidden argument*/NULL);
		uint32_t L_11 = L_10;
		V_0 = L_11;
		if (L_11)
		{
			goto IL_0002;
		}
	}
	{
		return;
	}
}
// System.Void Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnumReservedRange__cctor_m97E50FAA53B7507FE0785E95741F3780277FD731 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_1__ctor_mCCFD409A1FBA40DE4A8770B3B56B638652558C3D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_1_tB4DEFF82E390DE2B16A177BF95E2A660E016FC83_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MessageParser_1__ctor_mE62242286F7EBCC8F4B3B117434CF74E088C0C26_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MessageParser_1_tCCF97EFBB4482323562EEB1F40E83233B2D32D55_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_U3C_cctorU3Eb__41_0_m0FBA9F917C842E30E53B32B2B8E897821F32A8C3_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_t0FB8D11271B41B39C63CE8A142DE7F8F87630794_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t0FB8D11271B41B39C63CE8A142DE7F8F87630794_il2cpp_TypeInfo_var);
		U3CU3Ec_t0FB8D11271B41B39C63CE8A142DE7F8F87630794 * L_0 = ((U3CU3Ec_t0FB8D11271B41B39C63CE8A142DE7F8F87630794_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t0FB8D11271B41B39C63CE8A142DE7F8F87630794_il2cpp_TypeInfo_var))->get_U3CU3E9_0();
		Func_1_tB4DEFF82E390DE2B16A177BF95E2A660E016FC83 * L_1 = (Func_1_tB4DEFF82E390DE2B16A177BF95E2A660E016FC83 *)il2cpp_codegen_object_new(Func_1_tB4DEFF82E390DE2B16A177BF95E2A660E016FC83_il2cpp_TypeInfo_var);
		Func_1__ctor_mCCFD409A1FBA40DE4A8770B3B56B638652558C3D(L_1, L_0, (intptr_t)((intptr_t)U3CU3Ec_U3C_cctorU3Eb__41_0_m0FBA9F917C842E30E53B32B2B8E897821F32A8C3_RuntimeMethod_var), /*hidden argument*/Func_1__ctor_mCCFD409A1FBA40DE4A8770B3B56B638652558C3D_RuntimeMethod_var);
		MessageParser_1_tCCF97EFBB4482323562EEB1F40E83233B2D32D55 * L_2 = (MessageParser_1_tCCF97EFBB4482323562EEB1F40E83233B2D32D55 *)il2cpp_codegen_object_new(MessageParser_1_tCCF97EFBB4482323562EEB1F40E83233B2D32D55_il2cpp_TypeInfo_var);
		MessageParser_1__ctor_mE62242286F7EBCC8F4B3B117434CF74E088C0C26(L_2, L_1, /*hidden argument*/MessageParser_1__ctor_mE62242286F7EBCC8F4B3B117434CF74E088C0C26_RuntimeMethod_var);
		((EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41_StaticFields*)il2cpp_codegen_static_fields_for(EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41_il2cpp_TypeInfo_var))->set__parser_0(L_2);
		((EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41_StaticFields*)il2cpp_codegen_static_fields_for(EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41_il2cpp_TypeInfo_var))->set_StartDefaultValue_3(0);
		((EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41_StaticFields*)il2cpp_codegen_static_fields_for(EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41_il2cpp_TypeInfo_var))->set_EndDefaultValue_5(0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Google.Protobuf.MessageParser`1<Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation> Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation::get_Parser()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR MessageParser_1_t422AF617132E681AA126A8C774D49BC4101614D0 * Annotation_get_Parser_m8134CF19EF949C69B6DDD6103A008A1CF7A51BC7 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_il2cpp_TypeInfo_var);
		MessageParser_1_t422AF617132E681AA126A8C774D49BC4101614D0 * L_0 = ((Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_StaticFields*)il2cpp_codegen_static_fields_for(Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_il2cpp_TypeInfo_var))->get__parser_0();
		return L_0;
	}
}
// Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation::get_Descriptor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13 * Annotation_get_Descriptor_m03B3B365C873BFD9BECDEB8D977CF8C3CA4D161A (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GeneratedCodeInfo_t88B2B7880C3E0445510519523DE05618C3B1A315_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IList_1_tACE6E5172CFB9B21666313779F73E7389A285312_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GeneratedCodeInfo_t88B2B7880C3E0445510519523DE05618C3B1A315_il2cpp_TypeInfo_var);
		MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13 * L_0;
		L_0 = GeneratedCodeInfo_get_Descriptor_m2A40BE0B9007A08DDF69F22C9D6B16B364E08361(/*hidden argument*/NULL);
		NullCheck(L_0);
		RuntimeObject* L_1;
		L_1 = MessageDescriptor_get_NestedTypes_m34A524E3C7D560C1537685CF6291AB60DA58FE7E_inline(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13 * L_2;
		L_2 = InterfaceFuncInvoker1< MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13 *, int32_t >::Invoke(0 /* !0 System.Collections.Generic.IList`1<Google.Protobuf.Reflection.MessageDescriptor>::get_Item(System.Int32) */, IList_1_tACE6E5172CFB9B21666313779F73E7389A285312_il2cpp_TypeInfo_var, L_1, 0);
		return L_2;
	}
}
// Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation::pb::Google.Protobuf.IMessage.get_Descriptor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13 * Annotation_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m838535C2735EA0FFFB83AB7E49836FBF2E1F51E5 (Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_il2cpp_TypeInfo_var);
		MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13 * L_0;
		L_0 = Annotation_get_Descriptor_m03B3B365C873BFD9BECDEB8D977CF8C3CA4D161A(/*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Annotation__ctor_m9ED6A93F900EBFD3D4BC4980C3D9B032B8F9B5D7 (Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&RepeatedField_1__ctor_m28E20B7724ACD650AE80B8E03BCB4A9412BFB63F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419 * L_0 = (RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419 *)il2cpp_codegen_object_new(RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419_il2cpp_TypeInfo_var);
		RepeatedField_1__ctor_m28E20B7724ACD650AE80B8E03BCB4A9412BFB63F(L_0, /*hidden argument*/RepeatedField_1__ctor_m28E20B7724ACD650AE80B8E03BCB4A9412BFB63F_RuntimeMethod_var);
		__this->set_path__4(L_0);
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation::.ctor(Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Annotation__ctor_m61D9641ED59D1CDA766721C1A40198C8BF8F6E45 (Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405 * __this, Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&RepeatedField_1_Clone_mCCDDBCC8A031EB0A89878CA4C025F8B396DD9348_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Annotation__ctor_m9ED6A93F900EBFD3D4BC4980C3D9B032B8F9B5D7(__this, /*hidden argument*/NULL);
		Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405 * L_0 = ___other0;
		NullCheck(L_0);
		int32_t L_1 = L_0->get__hasBits0_2();
		__this->set__hasBits0_2(L_1);
		Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405 * L_2 = ___other0;
		NullCheck(L_2);
		RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419 * L_3 = L_2->get_path__4();
		NullCheck(L_3);
		RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419 * L_4;
		L_4 = RepeatedField_1_Clone_mCCDDBCC8A031EB0A89878CA4C025F8B396DD9348(L_3, /*hidden argument*/RepeatedField_1_Clone_mCCDDBCC8A031EB0A89878CA4C025F8B396DD9348_RuntimeMethod_var);
		__this->set_path__4(L_4);
		Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405 * L_5 = ___other0;
		NullCheck(L_5);
		String_t* L_6 = L_5->get_sourceFile__6();
		__this->set_sourceFile__6(L_6);
		Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405 * L_7 = ___other0;
		NullCheck(L_7);
		int32_t L_8 = L_7->get_begin__8();
		__this->set_begin__8(L_8);
		Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405 * L_9 = ___other0;
		NullCheck(L_9);
		int32_t L_10 = L_9->get_end__10();
		__this->set_end__10(L_10);
		Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405 * L_11 = ___other0;
		NullCheck(L_11);
		UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 * L_12 = L_11->get__unknownFields_1();
		UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 * L_13;
		L_13 = UnknownFieldSet_Clone_mBCB3196178FBF9E17BF3C686C6C6DC214D0BBDEA(L_12, /*hidden argument*/NULL);
		__this->set__unknownFields_1(L_13);
		return;
	}
}
// Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405 * Annotation_Clone_mB86AE67CF1DB8FC7CF33ADDCEF2FEEB74BCDFE5D (Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405 * L_0 = (Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405 *)il2cpp_codegen_object_new(Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_il2cpp_TypeInfo_var);
		Annotation__ctor_m61D9641ED59D1CDA766721C1A40198C8BF8F6E45(L_0, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.String Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation::get_SourceFile()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Annotation_get_SourceFile_m91B7098C2E39ECA1A8E13E40DB8A7A740C3C80F6 (Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	String_t* G_B2_0 = NULL;
	String_t* G_B1_0 = NULL;
	{
		String_t* L_0 = __this->get_sourceFile__6();
		String_t* L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_000f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_il2cpp_TypeInfo_var);
		String_t* L_2 = ((Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_StaticFields*)il2cpp_codegen_static_fields_for(Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_il2cpp_TypeInfo_var))->get_SourceFileDefaultValue_5();
		G_B2_0 = L_2;
	}

IL_000f:
	{
		return G_B2_0;
	}
}
// System.Void Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation::set_SourceFile(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Annotation_set_SourceFile_mB2AED44E9EFB3CA7D5BF5CA03DE3A8F5CE51723E (Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ProtoPreconditions_CheckNotNull_TisString_t_m80AE3118869FAEF4DF83AF0D521B284CF725144E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral46F273EF641E07D271D91E0DC24A4392582671F8);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___value0;
		String_t* L_1;
		L_1 = ProtoPreconditions_CheckNotNull_TisString_t_m80AE3118869FAEF4DF83AF0D521B284CF725144E(L_0, _stringLiteral46F273EF641E07D271D91E0DC24A4392582671F8, /*hidden argument*/ProtoPreconditions_CheckNotNull_TisString_t_m80AE3118869FAEF4DF83AF0D521B284CF725144E_RuntimeMethod_var);
		__this->set_sourceFile__6(L_1);
		return;
	}
}
// System.Boolean Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation::get_HasSourceFile()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Annotation_get_HasSourceFile_mD261BC3AC77DB72DB7E1BEE3A7A8955D0913C23E (Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_sourceFile__6();
		return (bool)((!(((RuntimeObject*)(String_t*)L_0) <= ((RuntimeObject*)(RuntimeObject *)NULL)))? 1 : 0);
	}
}
// System.Int32 Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation::get_Begin()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Annotation_get_Begin_m8891E38C06E537DCBAC9BEC72E83162AF4F6E9AC (Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get__hasBits0_2();
		if (!((int32_t)((int32_t)L_0&(int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_1 = __this->get_begin__8();
		return L_1;
	}

IL_0011:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_il2cpp_TypeInfo_var);
		int32_t L_2 = ((Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_StaticFields*)il2cpp_codegen_static_fields_for(Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_il2cpp_TypeInfo_var))->get_BeginDefaultValue_7();
		return L_2;
	}
}
// System.Void Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation::set_Begin(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Annotation_set_Begin_m5AA2016E643BF3BF22002C930EAACD7AE75ABDD5 (Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get__hasBits0_2();
		__this->set__hasBits0_2(((int32_t)((int32_t)L_0|(int32_t)1)));
		int32_t L_1 = ___value0;
		__this->set_begin__8(L_1);
		return;
	}
}
// System.Boolean Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation::get_HasBegin()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Annotation_get_HasBegin_m86E2171FE0BDCBB1B5A075E45C7A8DDDB931E97B (Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get__hasBits0_2();
		return (bool)((!(((uint32_t)((int32_t)((int32_t)L_0&(int32_t)1))) <= ((uint32_t)0)))? 1 : 0);
	}
}
// System.Int32 Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation::get_End()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Annotation_get_End_m1AB89595BE4271A333A470BDC5206C2814C050D9 (Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get__hasBits0_2();
		if (!((int32_t)((int32_t)L_0&(int32_t)2)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_1 = __this->get_end__10();
		return L_1;
	}

IL_0011:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_il2cpp_TypeInfo_var);
		int32_t L_2 = ((Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_StaticFields*)il2cpp_codegen_static_fields_for(Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_il2cpp_TypeInfo_var))->get_EndDefaultValue_9();
		return L_2;
	}
}
// System.Void Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation::set_End(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Annotation_set_End_m5BDF9D827DEC0FF6DDA13230D1C99C57817FA7C7 (Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get__hasBits0_2();
		__this->set__hasBits0_2(((int32_t)((int32_t)L_0|(int32_t)2)));
		int32_t L_1 = ___value0;
		__this->set_end__10(L_1);
		return;
	}
}
// System.Boolean Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation::get_HasEnd()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Annotation_get_HasEnd_mF1171BE52E3B2EFB80EFD5480675B2655897470E (Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get__hasBits0_2();
		return (bool)((!(((uint32_t)((int32_t)((int32_t)L_0&(int32_t)2))) <= ((uint32_t)0)))? 1 : 0);
	}
}
// System.Boolean Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation::Equals(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Annotation_Equals_m5C1A51E20393172CE798E42CAD95C3DD71E511C8 (Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405 * __this, RuntimeObject * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject * L_0 = ___other0;
		bool L_1;
		L_1 = Annotation_Equals_mD66C9282185C31DF563EED063B1019BE13C2811E(__this, ((Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405 *)IsInstSealed((RuntimeObject*)L_0, Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation::Equals(Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Annotation_Equals_mD66C9282185C31DF563EED063B1019BE13C2811E (Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405 * __this, Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&RepeatedField_1_Equals_m2D0B705880C1B033423F8A185C0D0DFE9EEDAADA_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405 * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0005;
		}
	}
	{
		return (bool)0;
	}

IL_0005:
	{
		Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405 * L_1 = ___other0;
		if ((!(((RuntimeObject*)(Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405 *)L_1) == ((RuntimeObject*)(Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405 *)__this))))
		{
			goto IL_000b;
		}
	}
	{
		return (bool)1;
	}

IL_000b:
	{
		RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419 * L_2 = __this->get_path__4();
		Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405 * L_3 = ___other0;
		NullCheck(L_3);
		RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419 * L_4 = L_3->get_path__4();
		NullCheck(L_2);
		bool L_5;
		L_5 = RepeatedField_1_Equals_m2D0B705880C1B033423F8A185C0D0DFE9EEDAADA(L_2, L_4, /*hidden argument*/RepeatedField_1_Equals_m2D0B705880C1B033423F8A185C0D0DFE9EEDAADA_RuntimeMethod_var);
		if (L_5)
		{
			goto IL_0020;
		}
	}
	{
		return (bool)0;
	}

IL_0020:
	{
		String_t* L_6;
		L_6 = Annotation_get_SourceFile_m91B7098C2E39ECA1A8E13E40DB8A7A740C3C80F6(__this, /*hidden argument*/NULL);
		Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405 * L_7 = ___other0;
		NullCheck(L_7);
		String_t* L_8;
		L_8 = Annotation_get_SourceFile_m91B7098C2E39ECA1A8E13E40DB8A7A740C3C80F6(L_7, /*hidden argument*/NULL);
		bool L_9;
		L_9 = String_op_Inequality_mDDA2DDED3E7EF042987EB7180EE3E88105F0AAE2(L_6, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0035;
		}
	}
	{
		return (bool)0;
	}

IL_0035:
	{
		int32_t L_10;
		L_10 = Annotation_get_Begin_m8891E38C06E537DCBAC9BEC72E83162AF4F6E9AC(__this, /*hidden argument*/NULL);
		Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405 * L_11 = ___other0;
		NullCheck(L_11);
		int32_t L_12;
		L_12 = Annotation_get_Begin_m8891E38C06E537DCBAC9BEC72E83162AF4F6E9AC(L_11, /*hidden argument*/NULL);
		if ((((int32_t)L_10) == ((int32_t)L_12)))
		{
			goto IL_0045;
		}
	}
	{
		return (bool)0;
	}

IL_0045:
	{
		int32_t L_13;
		L_13 = Annotation_get_End_m1AB89595BE4271A333A470BDC5206C2814C050D9(__this, /*hidden argument*/NULL);
		Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405 * L_14 = ___other0;
		NullCheck(L_14);
		int32_t L_15;
		L_15 = Annotation_get_End_m1AB89595BE4271A333A470BDC5206C2814C050D9(L_14, /*hidden argument*/NULL);
		if ((((int32_t)L_13) == ((int32_t)L_15)))
		{
			goto IL_0055;
		}
	}
	{
		return (bool)0;
	}

IL_0055:
	{
		UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 * L_16 = __this->get__unknownFields_1();
		Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405 * L_17 = ___other0;
		NullCheck(L_17);
		UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 * L_18 = L_17->get__unknownFields_1();
		bool L_19;
		L_19 = Object_Equals_mBE334AF263BDADA1D0F1DE7E14456A63E47F8053(L_16, L_18, /*hidden argument*/NULL);
		return L_19;
	}
}
// System.Int32 Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation::GetHashCode()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Annotation_GetHashCode_m6F1BE21B2168F0D61B4E600E21DDC81A22132BF2 (Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		V_0 = 1;
		int32_t L_0 = V_0;
		RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419 * L_1 = __this->get_path__4();
		NullCheck(L_1);
		int32_t L_2;
		L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, L_1);
		V_0 = ((int32_t)((int32_t)L_0^(int32_t)L_2));
		bool L_3;
		L_3 = Annotation_get_HasSourceFile_mD261BC3AC77DB72DB7E1BEE3A7A8955D0913C23E(__this, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0026;
		}
	}
	{
		int32_t L_4 = V_0;
		String_t* L_5;
		L_5 = Annotation_get_SourceFile_m91B7098C2E39ECA1A8E13E40DB8A7A740C3C80F6(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		int32_t L_6;
		L_6 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, L_5);
		V_0 = ((int32_t)((int32_t)L_4^(int32_t)L_6));
	}

IL_0026:
	{
		bool L_7;
		L_7 = Annotation_get_HasBegin_m86E2171FE0BDCBB1B5A075E45C7A8DDDB931E97B(__this, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_003f;
		}
	}
	{
		int32_t L_8 = V_0;
		int32_t L_9;
		L_9 = Annotation_get_Begin_m8891E38C06E537DCBAC9BEC72E83162AF4F6E9AC(__this, /*hidden argument*/NULL);
		V_1 = L_9;
		int32_t L_10;
		L_10 = Int32_GetHashCode_mEDD3F492A5F7CF021125AE3F38E2B8F8743FC667((int32_t*)(&V_1), /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)L_8^(int32_t)L_10));
	}

IL_003f:
	{
		bool L_11;
		L_11 = Annotation_get_HasEnd_mF1171BE52E3B2EFB80EFD5480675B2655897470E(__this, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0058;
		}
	}
	{
		int32_t L_12 = V_0;
		int32_t L_13;
		L_13 = Annotation_get_End_m1AB89595BE4271A333A470BDC5206C2814C050D9(__this, /*hidden argument*/NULL);
		V_1 = L_13;
		int32_t L_14;
		L_14 = Int32_GetHashCode_mEDD3F492A5F7CF021125AE3F38E2B8F8743FC667((int32_t*)(&V_1), /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)L_12^(int32_t)L_14));
	}

IL_0058:
	{
		UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 * L_15 = __this->get__unknownFields_1();
		if (!L_15)
		{
			goto IL_006e;
		}
	}
	{
		int32_t L_16 = V_0;
		UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 * L_17 = __this->get__unknownFields_1();
		NullCheck(L_17);
		int32_t L_18;
		L_18 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, L_17);
		V_0 = ((int32_t)((int32_t)L_16^(int32_t)L_18));
	}

IL_006e:
	{
		int32_t L_19 = V_0;
		return L_19;
	}
}
// System.String Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Annotation_ToString_m2E543D1B02E6207CB9009CEA5A987A6439466064 (Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&JsonFormatter_t29EDD2B5D01B4FD88FD4DBF274554818F8ED11C7_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonFormatter_t29EDD2B5D01B4FD88FD4DBF274554818F8ED11C7_il2cpp_TypeInfo_var);
		String_t* L_0;
		L_0 = JsonFormatter_ToDiagnosticString_mB0CBCBDBF65F17FC52DEB9E0C49184E44F3F2EF1(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation::WriteTo(Google.Protobuf.CodedOutputStream)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Annotation_WriteTo_m108B8AEABCE4F89A6E90997BAC8AC9371A53565D (Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405 * __this, CodedOutputStream_t2871B0D17F920BFB54B564E2295D9753721642E6 * ___output0, const RuntimeMethod* method)
{
	{
		CodedOutputStream_t2871B0D17F920BFB54B564E2295D9753721642E6 * L_0 = ___output0;
		NullCheck(L_0);
		CodedOutputStream_WriteRawMessage_m05CED72DF9A800E9BFB28956F21E7B4416E03D11(L_0, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation::pb::Google.Protobuf.IBufferMessage.InternalWriteTo(Google.Protobuf.WriteContext&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Annotation_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m2D0F50AAD5686905CCAA2959D0C6E112FDA37E85 (Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405 * __this, WriteContext_t4011B33FF5B289B23B81D9929C82A44F0A24DFCB * ___output0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&RepeatedField_1_WriteTo_mC5C250B204D43F5D814F1C19A1337853828264B8_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419 * L_0 = __this->get_path__4();
		WriteContext_t4011B33FF5B289B23B81D9929C82A44F0A24DFCB * L_1 = ___output0;
		IL2CPP_RUNTIME_CLASS_INIT(Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_il2cpp_TypeInfo_var);
		FieldCodec_1_tAB7BF6BD67CE831FB7E8A437894272BD9C511681 * L_2 = ((Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_StaticFields*)il2cpp_codegen_static_fields_for(Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_il2cpp_TypeInfo_var))->get__repeated_path_codec_3();
		NullCheck(L_0);
		RepeatedField_1_WriteTo_mC5C250B204D43F5D814F1C19A1337853828264B8(L_0, (WriteContext_t4011B33FF5B289B23B81D9929C82A44F0A24DFCB *)L_1, L_2, /*hidden argument*/RepeatedField_1_WriteTo_mC5C250B204D43F5D814F1C19A1337853828264B8_RuntimeMethod_var);
		bool L_3;
		L_3 = Annotation_get_HasSourceFile_mD261BC3AC77DB72DB7E1BEE3A7A8955D0913C23E(__this, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_002d;
		}
	}
	{
		WriteContext_t4011B33FF5B289B23B81D9929C82A44F0A24DFCB * L_4 = ___output0;
		WriteContext_WriteRawTag_m3E37F0B83B4E8B7C7E1CC6472D527AA22B11DA5A((WriteContext_t4011B33FF5B289B23B81D9929C82A44F0A24DFCB *)L_4, (uint8_t)((int32_t)18), /*hidden argument*/NULL);
		WriteContext_t4011B33FF5B289B23B81D9929C82A44F0A24DFCB * L_5 = ___output0;
		String_t* L_6;
		L_6 = Annotation_get_SourceFile_m91B7098C2E39ECA1A8E13E40DB8A7A740C3C80F6(__this, /*hidden argument*/NULL);
		WriteContext_WriteString_m1A0F46C5CF383B3F976BFBD24B546EC2E346C68C((WriteContext_t4011B33FF5B289B23B81D9929C82A44F0A24DFCB *)L_5, L_6, /*hidden argument*/NULL);
	}

IL_002d:
	{
		bool L_7;
		L_7 = Annotation_get_HasBegin_m86E2171FE0BDCBB1B5A075E45C7A8DDDB931E97B(__this, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0049;
		}
	}
	{
		WriteContext_t4011B33FF5B289B23B81D9929C82A44F0A24DFCB * L_8 = ___output0;
		WriteContext_WriteRawTag_m3E37F0B83B4E8B7C7E1CC6472D527AA22B11DA5A((WriteContext_t4011B33FF5B289B23B81D9929C82A44F0A24DFCB *)L_8, (uint8_t)((int32_t)24), /*hidden argument*/NULL);
		WriteContext_t4011B33FF5B289B23B81D9929C82A44F0A24DFCB * L_9 = ___output0;
		int32_t L_10;
		L_10 = Annotation_get_Begin_m8891E38C06E537DCBAC9BEC72E83162AF4F6E9AC(__this, /*hidden argument*/NULL);
		WriteContext_WriteInt32_mCC3646C9CF9A13E2BEBC15A8E566669C913D4F65((WriteContext_t4011B33FF5B289B23B81D9929C82A44F0A24DFCB *)L_9, L_10, /*hidden argument*/NULL);
	}

IL_0049:
	{
		bool L_11;
		L_11 = Annotation_get_HasEnd_mF1171BE52E3B2EFB80EFD5480675B2655897470E(__this, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0065;
		}
	}
	{
		WriteContext_t4011B33FF5B289B23B81D9929C82A44F0A24DFCB * L_12 = ___output0;
		WriteContext_WriteRawTag_m3E37F0B83B4E8B7C7E1CC6472D527AA22B11DA5A((WriteContext_t4011B33FF5B289B23B81D9929C82A44F0A24DFCB *)L_12, (uint8_t)((int32_t)32), /*hidden argument*/NULL);
		WriteContext_t4011B33FF5B289B23B81D9929C82A44F0A24DFCB * L_13 = ___output0;
		int32_t L_14;
		L_14 = Annotation_get_End_m1AB89595BE4271A333A470BDC5206C2814C050D9(__this, /*hidden argument*/NULL);
		WriteContext_WriteInt32_mCC3646C9CF9A13E2BEBC15A8E566669C913D4F65((WriteContext_t4011B33FF5B289B23B81D9929C82A44F0A24DFCB *)L_13, L_14, /*hidden argument*/NULL);
	}

IL_0065:
	{
		UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 * L_15 = __this->get__unknownFields_1();
		if (!L_15)
		{
			goto IL_0079;
		}
	}
	{
		UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 * L_16 = __this->get__unknownFields_1();
		WriteContext_t4011B33FF5B289B23B81D9929C82A44F0A24DFCB * L_17 = ___output0;
		NullCheck(L_16);
		UnknownFieldSet_WriteTo_mECF1EFA7AF68EBBEDDD046865B2C17131B969E1B(L_16, (WriteContext_t4011B33FF5B289B23B81D9929C82A44F0A24DFCB *)L_17, /*hidden argument*/NULL);
	}

IL_0079:
	{
		return;
	}
}
// System.Int32 Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation::CalculateSize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Annotation_CalculateSize_m1DE1ABE6707A9C1C6D415715E6EB90D7DDD98AB8 (Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CodedOutputStream_t2871B0D17F920BFB54B564E2295D9753721642E6_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&RepeatedField_1_CalculateSize_m67536E773582CAB8A68E5056E9A1D318FAE1942B_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		int32_t L_0 = V_0;
		RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419 * L_1 = __this->get_path__4();
		IL2CPP_RUNTIME_CLASS_INIT(Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_il2cpp_TypeInfo_var);
		FieldCodec_1_tAB7BF6BD67CE831FB7E8A437894272BD9C511681 * L_2 = ((Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_StaticFields*)il2cpp_codegen_static_fields_for(Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_il2cpp_TypeInfo_var))->get__repeated_path_codec_3();
		NullCheck(L_1);
		int32_t L_3;
		L_3 = RepeatedField_1_CalculateSize_m67536E773582CAB8A68E5056E9A1D318FAE1942B(L_1, L_2, /*hidden argument*/RepeatedField_1_CalculateSize_m67536E773582CAB8A68E5056E9A1D318FAE1942B_RuntimeMethod_var);
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_0, (int32_t)L_3));
		bool L_4;
		L_4 = Annotation_get_HasSourceFile_mD261BC3AC77DB72DB7E1BEE3A7A8955D0913C23E(__this, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_002d;
		}
	}
	{
		int32_t L_5 = V_0;
		String_t* L_6;
		L_6 = Annotation_get_SourceFile_m91B7098C2E39ECA1A8E13E40DB8A7A740C3C80F6(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CodedOutputStream_t2871B0D17F920BFB54B564E2295D9753721642E6_il2cpp_TypeInfo_var);
		int32_t L_7;
		L_7 = CodedOutputStream_ComputeStringSize_m3913248CC93D6067C32930171244FD185D818BDF(L_6, /*hidden argument*/NULL);
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_5, (int32_t)((int32_t)il2cpp_codegen_add((int32_t)1, (int32_t)L_7))));
	}

IL_002d:
	{
		bool L_8;
		L_8 = Annotation_get_HasBegin_m86E2171FE0BDCBB1B5A075E45C7A8DDDB931E97B(__this, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0045;
		}
	}
	{
		int32_t L_9 = V_0;
		int32_t L_10;
		L_10 = Annotation_get_Begin_m8891E38C06E537DCBAC9BEC72E83162AF4F6E9AC(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CodedOutputStream_t2871B0D17F920BFB54B564E2295D9753721642E6_il2cpp_TypeInfo_var);
		int32_t L_11;
		L_11 = CodedOutputStream_ComputeInt32Size_m03C7A226C41F660D0A853D576117A6386B9D0802(L_10, /*hidden argument*/NULL);
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_9, (int32_t)((int32_t)il2cpp_codegen_add((int32_t)1, (int32_t)L_11))));
	}

IL_0045:
	{
		bool L_12;
		L_12 = Annotation_get_HasEnd_mF1171BE52E3B2EFB80EFD5480675B2655897470E(__this, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_005d;
		}
	}
	{
		int32_t L_13 = V_0;
		int32_t L_14;
		L_14 = Annotation_get_End_m1AB89595BE4271A333A470BDC5206C2814C050D9(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CodedOutputStream_t2871B0D17F920BFB54B564E2295D9753721642E6_il2cpp_TypeInfo_var);
		int32_t L_15;
		L_15 = CodedOutputStream_ComputeInt32Size_m03C7A226C41F660D0A853D576117A6386B9D0802(L_14, /*hidden argument*/NULL);
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_13, (int32_t)((int32_t)il2cpp_codegen_add((int32_t)1, (int32_t)L_15))));
	}

IL_005d:
	{
		UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 * L_16 = __this->get__unknownFields_1();
		if (!L_16)
		{
			goto IL_0073;
		}
	}
	{
		int32_t L_17 = V_0;
		UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 * L_18 = __this->get__unknownFields_1();
		NullCheck(L_18);
		int32_t L_19;
		L_19 = UnknownFieldSet_CalculateSize_m0F48042AFDC7E6795E7A99D8B3BE0D5C61C96868(L_18, /*hidden argument*/NULL);
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_17, (int32_t)L_19));
	}

IL_0073:
	{
		int32_t L_20 = V_0;
		return L_20;
	}
}
// System.Void Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation::MergeFrom(Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Annotation_MergeFrom_m2468E39EA5A58BCFA6F9E35D01002292E3CC450F (Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405 * __this, Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&RepeatedField_1_Add_m65F8D4082B764C2A1CC6367692FB566C2A0D75F8_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405 * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0004;
		}
	}
	{
		return;
	}

IL_0004:
	{
		RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419 * L_1 = __this->get_path__4();
		Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405 * L_2 = ___other0;
		NullCheck(L_2);
		RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419 * L_3 = L_2->get_path__4();
		NullCheck(L_1);
		RepeatedField_1_Add_m65F8D4082B764C2A1CC6367692FB566C2A0D75F8(L_1, L_3, /*hidden argument*/RepeatedField_1_Add_m65F8D4082B764C2A1CC6367692FB566C2A0D75F8_RuntimeMethod_var);
		Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405 * L_4 = ___other0;
		NullCheck(L_4);
		bool L_5;
		L_5 = Annotation_get_HasSourceFile_mD261BC3AC77DB72DB7E1BEE3A7A8955D0913C23E(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0029;
		}
	}
	{
		Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405 * L_6 = ___other0;
		NullCheck(L_6);
		String_t* L_7;
		L_7 = Annotation_get_SourceFile_m91B7098C2E39ECA1A8E13E40DB8A7A740C3C80F6(L_6, /*hidden argument*/NULL);
		Annotation_set_SourceFile_mB2AED44E9EFB3CA7D5BF5CA03DE3A8F5CE51723E(__this, L_7, /*hidden argument*/NULL);
	}

IL_0029:
	{
		Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405 * L_8 = ___other0;
		NullCheck(L_8);
		bool L_9;
		L_9 = Annotation_get_HasBegin_m86E2171FE0BDCBB1B5A075E45C7A8DDDB931E97B(L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_003d;
		}
	}
	{
		Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405 * L_10 = ___other0;
		NullCheck(L_10);
		int32_t L_11;
		L_11 = Annotation_get_Begin_m8891E38C06E537DCBAC9BEC72E83162AF4F6E9AC(L_10, /*hidden argument*/NULL);
		Annotation_set_Begin_m5AA2016E643BF3BF22002C930EAACD7AE75ABDD5(__this, L_11, /*hidden argument*/NULL);
	}

IL_003d:
	{
		Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405 * L_12 = ___other0;
		NullCheck(L_12);
		bool L_13;
		L_13 = Annotation_get_HasEnd_mF1171BE52E3B2EFB80EFD5480675B2655897470E(L_12, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_0051;
		}
	}
	{
		Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405 * L_14 = ___other0;
		NullCheck(L_14);
		int32_t L_15;
		L_15 = Annotation_get_End_m1AB89595BE4271A333A470BDC5206C2814C050D9(L_14, /*hidden argument*/NULL);
		Annotation_set_End_m5BDF9D827DEC0FF6DDA13230D1C99C57817FA7C7(__this, L_15, /*hidden argument*/NULL);
	}

IL_0051:
	{
		UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 * L_16 = __this->get__unknownFields_1();
		Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405 * L_17 = ___other0;
		NullCheck(L_17);
		UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 * L_18 = L_17->get__unknownFields_1();
		UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 * L_19;
		L_19 = UnknownFieldSet_MergeFrom_m3175C4FF04375A5EF7DC4E3245C6606303344F85(L_16, L_18, /*hidden argument*/NULL);
		__this->set__unknownFields_1(L_19);
		return;
	}
}
// System.Void Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation::MergeFrom(Google.Protobuf.CodedInputStream)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Annotation_MergeFrom_m8CA8D3549BCBA484E4BA0ADCDE8200E20CCEDFBD (Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405 * __this, CodedInputStream_tC76F9515605395C11D7F9437E0E06C828232B0C8 * ___input0, const RuntimeMethod* method)
{
	{
		CodedInputStream_tC76F9515605395C11D7F9437E0E06C828232B0C8 * L_0 = ___input0;
		NullCheck(L_0);
		CodedInputStream_ReadRawMessage_m77E217968CD5654CC717E7DA57906C0219372943(L_0, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation::pb::Google.Protobuf.IBufferMessage.InternalMergeFrom(Google.Protobuf.ParseContext&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Annotation_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m314FDFF4E0741C746FA60D608C45F8C01E8A9117 (Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405 * __this, ParseContext_t288EAC5562A1D7E0FF8A06DBA58B8CF732ECE349 * ___input0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&RepeatedField_1_AddEntriesFrom_m5AFE58E765285935D3E71F09A425AD7BE2D56674_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		goto IL_0070;
	}

IL_0002:
	{
		uint32_t L_0 = V_0;
		if ((!(((uint32_t)L_0) <= ((uint32_t)((int32_t)10)))))
		{
			goto IL_0012;
		}
	}
	{
		uint32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)8)))
		{
			goto IL_0035;
		}
	}
	{
		uint32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)((int32_t)10))))
		{
			goto IL_0035;
		}
	}
	{
		goto IL_0021;
	}

IL_0012:
	{
		uint32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)((int32_t)18))))
		{
			goto IL_0048;
		}
	}
	{
		uint32_t L_4 = V_0;
		if ((((int32_t)L_4) == ((int32_t)((int32_t)24))))
		{
			goto IL_0056;
		}
	}
	{
		uint32_t L_5 = V_0;
		if ((((int32_t)L_5) == ((int32_t)((int32_t)32))))
		{
			goto IL_0064;
		}
	}

IL_0021:
	{
		UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 * L_6 = __this->get__unknownFields_1();
		ParseContext_t288EAC5562A1D7E0FF8A06DBA58B8CF732ECE349 * L_7 = ___input0;
		UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 * L_8;
		L_8 = UnknownFieldSet_MergeFieldFrom_m6D9E32ADBB0194941C65CD43522F53C3A2378283(L_6, (ParseContext_t288EAC5562A1D7E0FF8A06DBA58B8CF732ECE349 *)L_7, /*hidden argument*/NULL);
		__this->set__unknownFields_1(L_8);
		goto IL_0070;
	}

IL_0035:
	{
		RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419 * L_9 = __this->get_path__4();
		ParseContext_t288EAC5562A1D7E0FF8A06DBA58B8CF732ECE349 * L_10 = ___input0;
		IL2CPP_RUNTIME_CLASS_INIT(Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_il2cpp_TypeInfo_var);
		FieldCodec_1_tAB7BF6BD67CE831FB7E8A437894272BD9C511681 * L_11 = ((Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_StaticFields*)il2cpp_codegen_static_fields_for(Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_il2cpp_TypeInfo_var))->get__repeated_path_codec_3();
		NullCheck(L_9);
		RepeatedField_1_AddEntriesFrom_m5AFE58E765285935D3E71F09A425AD7BE2D56674(L_9, (ParseContext_t288EAC5562A1D7E0FF8A06DBA58B8CF732ECE349 *)L_10, L_11, /*hidden argument*/RepeatedField_1_AddEntriesFrom_m5AFE58E765285935D3E71F09A425AD7BE2D56674_RuntimeMethod_var);
		goto IL_0070;
	}

IL_0048:
	{
		ParseContext_t288EAC5562A1D7E0FF8A06DBA58B8CF732ECE349 * L_12 = ___input0;
		String_t* L_13;
		L_13 = ParseContext_ReadString_mA21E24240255C281D36669A209628AD6FDBB15AE_inline((ParseContext_t288EAC5562A1D7E0FF8A06DBA58B8CF732ECE349 *)L_12, /*hidden argument*/NULL);
		Annotation_set_SourceFile_mB2AED44E9EFB3CA7D5BF5CA03DE3A8F5CE51723E(__this, L_13, /*hidden argument*/NULL);
		goto IL_0070;
	}

IL_0056:
	{
		ParseContext_t288EAC5562A1D7E0FF8A06DBA58B8CF732ECE349 * L_14 = ___input0;
		int32_t L_15;
		L_15 = ParseContext_ReadInt32_m275087DD36E27495CC2EE76EEB08BC90EFAF94B9_inline((ParseContext_t288EAC5562A1D7E0FF8A06DBA58B8CF732ECE349 *)L_14, /*hidden argument*/NULL);
		Annotation_set_Begin_m5AA2016E643BF3BF22002C930EAACD7AE75ABDD5(__this, L_15, /*hidden argument*/NULL);
		goto IL_0070;
	}

IL_0064:
	{
		ParseContext_t288EAC5562A1D7E0FF8A06DBA58B8CF732ECE349 * L_16 = ___input0;
		int32_t L_17;
		L_17 = ParseContext_ReadInt32_m275087DD36E27495CC2EE76EEB08BC90EFAF94B9_inline((ParseContext_t288EAC5562A1D7E0FF8A06DBA58B8CF732ECE349 *)L_16, /*hidden argument*/NULL);
		Annotation_set_End_m5BDF9D827DEC0FF6DDA13230D1C99C57817FA7C7(__this, L_17, /*hidden argument*/NULL);
	}

IL_0070:
	{
		ParseContext_t288EAC5562A1D7E0FF8A06DBA58B8CF732ECE349 * L_18 = ___input0;
		uint32_t L_19;
		L_19 = ParseContext_ReadTag_mEE687A18078235454AEA63F1F61E9E3E9EB0260F_inline((ParseContext_t288EAC5562A1D7E0FF8A06DBA58B8CF732ECE349 *)L_18, /*hidden argument*/NULL);
		uint32_t L_20 = L_19;
		V_0 = L_20;
		if (L_20)
		{
			goto IL_0002;
		}
	}
	{
		return;
	}
}
// System.Void Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Annotation__cctor_m828EDEED29164F265B88F07BDC87ED3A6B0916AF (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_1__ctor_mE88B85D2A25CDAB279765DBE7C9E71B06D0C4C31_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_1_t8A740DFC3D657757DA5C263E40CD639547B59CB4_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MessageParser_1__ctor_m1927E72A8975A7E3D3A15FDFB5DCD64879E50B6E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MessageParser_1_t422AF617132E681AA126A8C774D49BC4101614D0_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_U3C_cctorU3Eb__55_0_m3FCF05CD6B82754BB25E87E1A3A8225796468906_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_t48164166658FE6E4AA4BC81699272A7660D5A345_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t48164166658FE6E4AA4BC81699272A7660D5A345_il2cpp_TypeInfo_var);
		U3CU3Ec_t48164166658FE6E4AA4BC81699272A7660D5A345 * L_0 = ((U3CU3Ec_t48164166658FE6E4AA4BC81699272A7660D5A345_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t48164166658FE6E4AA4BC81699272A7660D5A345_il2cpp_TypeInfo_var))->get_U3CU3E9_0();
		Func_1_t8A740DFC3D657757DA5C263E40CD639547B59CB4 * L_1 = (Func_1_t8A740DFC3D657757DA5C263E40CD639547B59CB4 *)il2cpp_codegen_object_new(Func_1_t8A740DFC3D657757DA5C263E40CD639547B59CB4_il2cpp_TypeInfo_var);
		Func_1__ctor_mE88B85D2A25CDAB279765DBE7C9E71B06D0C4C31(L_1, L_0, (intptr_t)((intptr_t)U3CU3Ec_U3C_cctorU3Eb__55_0_m3FCF05CD6B82754BB25E87E1A3A8225796468906_RuntimeMethod_var), /*hidden argument*/Func_1__ctor_mE88B85D2A25CDAB279765DBE7C9E71B06D0C4C31_RuntimeMethod_var);
		MessageParser_1_t422AF617132E681AA126A8C774D49BC4101614D0 * L_2 = (MessageParser_1_t422AF617132E681AA126A8C774D49BC4101614D0 *)il2cpp_codegen_object_new(MessageParser_1_t422AF617132E681AA126A8C774D49BC4101614D0_il2cpp_TypeInfo_var);
		MessageParser_1__ctor_m1927E72A8975A7E3D3A15FDFB5DCD64879E50B6E(L_2, L_1, /*hidden argument*/MessageParser_1__ctor_m1927E72A8975A7E3D3A15FDFB5DCD64879E50B6E_RuntimeMethod_var);
		((Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_StaticFields*)il2cpp_codegen_static_fields_for(Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_il2cpp_TypeInfo_var))->set__parser_0(L_2);
		FieldCodec_1_tAB7BF6BD67CE831FB7E8A437894272BD9C511681 * L_3;
		L_3 = FieldCodec_ForInt32_mA8FC008DA1A37C17DED262F93274DACAEAE60621(((int32_t)10), /*hidden argument*/NULL);
		((Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_StaticFields*)il2cpp_codegen_static_fields_for(Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_il2cpp_TypeInfo_var))->set__repeated_path_codec_3(L_3);
		((Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_StaticFields*)il2cpp_codegen_static_fields_for(Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_il2cpp_TypeInfo_var))->set_SourceFileDefaultValue_5(_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		((Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_StaticFields*)il2cpp_codegen_static_fields_for(Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_il2cpp_TypeInfo_var))->set_BeginDefaultValue_7(0);
		((Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_StaticFields*)il2cpp_codegen_static_fields_for(Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_il2cpp_TypeInfo_var))->set_EndDefaultValue_9(0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Google.Protobuf.JsonFormatter/OriginalEnumValueHelper/<>c::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__cctor_m23171665533BE5299B7466C18DEFEA08A225CED5 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_t5555F991758A0DD73D48BCE4AF9A1B14B7F05FA3_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CU3Ec_t5555F991758A0DD73D48BCE4AF9A1B14B7F05FA3 * L_0 = (U3CU3Ec_t5555F991758A0DD73D48BCE4AF9A1B14B7F05FA3 *)il2cpp_codegen_object_new(U3CU3Ec_t5555F991758A0DD73D48BCE4AF9A1B14B7F05FA3_il2cpp_TypeInfo_var);
		U3CU3Ec__ctor_m8A4E99CDBE7F880923AF3978955A596A6CFEAD21(L_0, /*hidden argument*/NULL);
		((U3CU3Ec_t5555F991758A0DD73D48BCE4AF9A1B14B7F05FA3_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t5555F991758A0DD73D48BCE4AF9A1B14B7F05FA3_il2cpp_TypeInfo_var))->set_U3CU3E9_0(L_0);
		return;
	}
}
// System.Void Google.Protobuf.JsonFormatter/OriginalEnumValueHelper/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m8A4E99CDBE7F880923AF3978955A596A6CFEAD21 (U3CU3Ec_t5555F991758A0DD73D48BCE4AF9A1B14B7F05FA3 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Google.Protobuf.JsonFormatter/OriginalEnumValueHelper/<>c::<GetNameMapping>b__2_0(System.Reflection.FieldInfo)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CU3Ec_U3CGetNameMappingU3Eb__2_0_mAB55EF229CE87D1E2A5F3EC117AFEE14CB3B0019 (U3CU3Ec_t5555F991758A0DD73D48BCE4AF9A1B14B7F05FA3 * __this, FieldInfo_t * ___f0, const RuntimeMethod* method)
{
	{
		FieldInfo_t * L_0 = ___f0;
		NullCheck(L_0);
		bool L_1;
		L_1 = FieldInfo_get_IsStatic_mE36F3A5B2DFF613C704AA56989D90D72760391EB(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean Google.Protobuf.JsonFormatter/OriginalEnumValueHelper/<>c::<GetNameMapping>b__2_1(System.Reflection.FieldInfo)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CU3Ec_U3CGetNameMappingU3Eb__2_1_m4363C5DBD92E971FA74128DFCEAE80C4FFDF40BE (U3CU3Ec_t5555F991758A0DD73D48BCE4AF9A1B14B7F05FA3 * __this, FieldInfo_t * ___f0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CustomAttributeExtensions_GetCustomAttributes_TisOriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664_mF22DB14B5869097D2C07559341A761D0ADAAF3EB_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerable_FirstOrDefault_TisOriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664_m20BF8C804DDE669AD4A21AB34682851EE714877E_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * G_B2_0 = NULL;
	OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * G_B1_0 = NULL;
	{
		FieldInfo_t * L_0 = ___f0;
		RuntimeObject* L_1;
		L_1 = CustomAttributeExtensions_GetCustomAttributes_TisOriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664_mF22DB14B5869097D2C07559341A761D0ADAAF3EB(L_0, /*hidden argument*/CustomAttributeExtensions_GetCustomAttributes_TisOriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664_mF22DB14B5869097D2C07559341A761D0ADAAF3EB_RuntimeMethod_var);
		OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * L_2;
		L_2 = Enumerable_FirstOrDefault_TisOriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664_m20BF8C804DDE669AD4A21AB34682851EE714877E(L_1, /*hidden argument*/Enumerable_FirstOrDefault_TisOriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664_m20BF8C804DDE669AD4A21AB34682851EE714877E_RuntimeMethod_var);
		OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * L_3 = L_2;
		G_B1_0 = L_3;
		if (L_3)
		{
			G_B2_0 = L_3;
			goto IL_0011;
		}
	}
	{
		return (bool)1;
	}

IL_0011:
	{
		NullCheck(G_B2_0);
		bool L_4;
		L_4 = OriginalNameAttribute_get_PreferredAlias_m8B1D004B47D36373572F40238135A599E23095B8_inline(G_B2_0, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Object Google.Protobuf.JsonFormatter/OriginalEnumValueHelper/<>c::<GetNameMapping>b__2_2(System.Reflection.FieldInfo)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CU3Ec_U3CGetNameMappingU3Eb__2_2_mC6E555E9DF03A3DD2D7841046524957CFD0F081D (U3CU3Ec_t5555F991758A0DD73D48BCE4AF9A1B14B7F05FA3 * __this, FieldInfo_t * ___f0, const RuntimeMethod* method)
{
	{
		FieldInfo_t * L_0 = ___f0;
		NullCheck(L_0);
		RuntimeObject * L_1;
		L_1 = VirtFuncInvoker1< RuntimeObject *, RuntimeObject * >::Invoke(19 /* System.Object System.Reflection.FieldInfo::GetValue(System.Object) */, L_0, NULL);
		return L_1;
	}
}
// System.String Google.Protobuf.JsonFormatter/OriginalEnumValueHelper/<>c::<GetNameMapping>b__2_3(System.Reflection.FieldInfo)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* U3CU3Ec_U3CGetNameMappingU3Eb__2_3_m974D038691FD95D34419B7B4D7EA34E65A8D05A9 (U3CU3Ec_t5555F991758A0DD73D48BCE4AF9A1B14B7F05FA3 * __this, FieldInfo_t * ___f0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CustomAttributeExtensions_GetCustomAttributes_TisOriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664_mF22DB14B5869097D2C07559341A761D0ADAAF3EB_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerable_FirstOrDefault_TisOriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664_m20BF8C804DDE669AD4A21AB34682851EE714877E_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * G_B2_0 = NULL;
	OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * G_B1_0 = NULL;
	String_t* G_B3_0 = NULL;
	String_t* G_B5_0 = NULL;
	String_t* G_B4_0 = NULL;
	{
		FieldInfo_t * L_0 = ___f0;
		RuntimeObject* L_1;
		L_1 = CustomAttributeExtensions_GetCustomAttributes_TisOriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664_mF22DB14B5869097D2C07559341A761D0ADAAF3EB(L_0, /*hidden argument*/CustomAttributeExtensions_GetCustomAttributes_TisOriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664_mF22DB14B5869097D2C07559341A761D0ADAAF3EB_RuntimeMethod_var);
		OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * L_2;
		L_2 = Enumerable_FirstOrDefault_TisOriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664_m20BF8C804DDE669AD4A21AB34682851EE714877E(L_1, /*hidden argument*/Enumerable_FirstOrDefault_TisOriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664_m20BF8C804DDE669AD4A21AB34682851EE714877E_RuntimeMethod_var);
		OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * L_3 = L_2;
		G_B1_0 = L_3;
		if (L_3)
		{
			G_B2_0 = L_3;
			goto IL_0012;
		}
	}
	{
		G_B3_0 = ((String_t*)(NULL));
		goto IL_0017;
	}

IL_0012:
	{
		NullCheck(G_B2_0);
		String_t* L_4;
		L_4 = OriginalNameAttribute_get_Name_m35E068ED514B51E172CF5FE5DD917801F045B819_inline(G_B2_0, /*hidden argument*/NULL);
		G_B3_0 = L_4;
	}

IL_0017:
	{
		String_t* L_5 = G_B3_0;
		G_B4_0 = L_5;
		if (L_5)
		{
			G_B5_0 = L_5;
			goto IL_0021;
		}
	}
	{
		FieldInfo_t * L_6 = ___f0;
		NullCheck(L_6);
		String_t* L_7;
		L_7 = VirtFuncInvoker0< String_t* >::Invoke(7 /* System.String System.Reflection.MemberInfo::get_Name() */, L_6);
		G_B5_0 = L_7;
	}

IL_0021:
	{
		return G_B5_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Google.Protobuf.MessageParser`1<Google.Protobuf.Reflection.SourceCodeInfo/Types/Location> Google.Protobuf.Reflection.SourceCodeInfo/Types/Location::get_Parser()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR MessageParser_1_t09C483F589C454D2B080958F963D017315003983 * Location_get_Parser_mB7AA6C4FB9B6FA6EB4B8C4CC5475D6AC028A10B1 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_il2cpp_TypeInfo_var);
		MessageParser_1_t09C483F589C454D2B080958F963D017315003983 * L_0 = ((Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_StaticFields*)il2cpp_codegen_static_fields_for(Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_il2cpp_TypeInfo_var))->get__parser_0();
		return L_0;
	}
}
// Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.Reflection.SourceCodeInfo/Types/Location::get_Descriptor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13 * Location_get_Descriptor_mAF9AF217303A7A050DD8AC21A40A039097EBCC08 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IList_1_tACE6E5172CFB9B21666313779F73E7389A285312_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SourceCodeInfo_tE9110A5CAA36A130E03CC9F3DE6E0EF7E7BF412B_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SourceCodeInfo_tE9110A5CAA36A130E03CC9F3DE6E0EF7E7BF412B_il2cpp_TypeInfo_var);
		MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13 * L_0;
		L_0 = SourceCodeInfo_get_Descriptor_mC3AEF456721098C0D729AFBFFF1365A7928CC547(/*hidden argument*/NULL);
		NullCheck(L_0);
		RuntimeObject* L_1;
		L_1 = MessageDescriptor_get_NestedTypes_m34A524E3C7D560C1537685CF6291AB60DA58FE7E_inline(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13 * L_2;
		L_2 = InterfaceFuncInvoker1< MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13 *, int32_t >::Invoke(0 /* !0 System.Collections.Generic.IList`1<Google.Protobuf.Reflection.MessageDescriptor>::get_Item(System.Int32) */, IList_1_tACE6E5172CFB9B21666313779F73E7389A285312_il2cpp_TypeInfo_var, L_1, 0);
		return L_2;
	}
}
// Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.Reflection.SourceCodeInfo/Types/Location::pb::Google.Protobuf.IMessage.get_Descriptor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13 * Location_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_mF87BC274A989F4DBC54D998D5B5AA598C5644508 (Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_il2cpp_TypeInfo_var);
		MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13 * L_0;
		L_0 = Location_get_Descriptor_mAF9AF217303A7A050DD8AC21A40A039097EBCC08(/*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void Google.Protobuf.Reflection.SourceCodeInfo/Types/Location::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Location__ctor_mFDC279769BEAE3382B8775935C8683621235F622 (Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&RepeatedField_1__ctor_m28E20B7724ACD650AE80B8E03BCB4A9412BFB63F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&RepeatedField_1__ctor_m397DAD9DD9B6E1993CE6E7CEF796BD2443BA0F44_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&RepeatedField_1_t03A3C968DD0FF896E3F108784B11A7CBD313CDE1_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419 * L_0 = (RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419 *)il2cpp_codegen_object_new(RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419_il2cpp_TypeInfo_var);
		RepeatedField_1__ctor_m28E20B7724ACD650AE80B8E03BCB4A9412BFB63F(L_0, /*hidden argument*/RepeatedField_1__ctor_m28E20B7724ACD650AE80B8E03BCB4A9412BFB63F_RuntimeMethod_var);
		__this->set_path__3(L_0);
		RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419 * L_1 = (RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419 *)il2cpp_codegen_object_new(RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419_il2cpp_TypeInfo_var);
		RepeatedField_1__ctor_m28E20B7724ACD650AE80B8E03BCB4A9412BFB63F(L_1, /*hidden argument*/RepeatedField_1__ctor_m28E20B7724ACD650AE80B8E03BCB4A9412BFB63F_RuntimeMethod_var);
		__this->set_span__5(L_1);
		RepeatedField_1_t03A3C968DD0FF896E3F108784B11A7CBD313CDE1 * L_2 = (RepeatedField_1_t03A3C968DD0FF896E3F108784B11A7CBD313CDE1 *)il2cpp_codegen_object_new(RepeatedField_1_t03A3C968DD0FF896E3F108784B11A7CBD313CDE1_il2cpp_TypeInfo_var);
		RepeatedField_1__ctor_m397DAD9DD9B6E1993CE6E7CEF796BD2443BA0F44(L_2, /*hidden argument*/RepeatedField_1__ctor_m397DAD9DD9B6E1993CE6E7CEF796BD2443BA0F44_RuntimeMethod_var);
		__this->set_leadingDetachedComments__11(L_2);
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Google.Protobuf.Reflection.SourceCodeInfo/Types/Location::.ctor(Google.Protobuf.Reflection.SourceCodeInfo/Types/Location)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Location__ctor_m28EFCE79F70879A58E3CB6F54CE731BD38BE41DD (Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4 * __this, Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&RepeatedField_1_Clone_mCCDDBCC8A031EB0A89878CA4C025F8B396DD9348_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&RepeatedField_1_Clone_mF6A2C055409968D973A30440A0F282A762BE6601_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Location__ctor_mFDC279769BEAE3382B8775935C8683621235F622(__this, /*hidden argument*/NULL);
		Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4 * L_0 = ___other0;
		NullCheck(L_0);
		RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419 * L_1 = L_0->get_path__3();
		NullCheck(L_1);
		RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419 * L_2;
		L_2 = RepeatedField_1_Clone_mCCDDBCC8A031EB0A89878CA4C025F8B396DD9348(L_1, /*hidden argument*/RepeatedField_1_Clone_mCCDDBCC8A031EB0A89878CA4C025F8B396DD9348_RuntimeMethod_var);
		__this->set_path__3(L_2);
		Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4 * L_3 = ___other0;
		NullCheck(L_3);
		RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419 * L_4 = L_3->get_span__5();
		NullCheck(L_4);
		RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419 * L_5;
		L_5 = RepeatedField_1_Clone_mCCDDBCC8A031EB0A89878CA4C025F8B396DD9348(L_4, /*hidden argument*/RepeatedField_1_Clone_mCCDDBCC8A031EB0A89878CA4C025F8B396DD9348_RuntimeMethod_var);
		__this->set_span__5(L_5);
		Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4 * L_6 = ___other0;
		NullCheck(L_6);
		String_t* L_7 = L_6->get_leadingComments__7();
		__this->set_leadingComments__7(L_7);
		Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4 * L_8 = ___other0;
		NullCheck(L_8);
		String_t* L_9 = L_8->get_trailingComments__9();
		__this->set_trailingComments__9(L_9);
		Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4 * L_10 = ___other0;
		NullCheck(L_10);
		RepeatedField_1_t03A3C968DD0FF896E3F108784B11A7CBD313CDE1 * L_11 = L_10->get_leadingDetachedComments__11();
		NullCheck(L_11);
		RepeatedField_1_t03A3C968DD0FF896E3F108784B11A7CBD313CDE1 * L_12;
		L_12 = RepeatedField_1_Clone_mF6A2C055409968D973A30440A0F282A762BE6601(L_11, /*hidden argument*/RepeatedField_1_Clone_mF6A2C055409968D973A30440A0F282A762BE6601_RuntimeMethod_var);
		__this->set_leadingDetachedComments__11(L_12);
		Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4 * L_13 = ___other0;
		NullCheck(L_13);
		UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 * L_14 = L_13->get__unknownFields_1();
		UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 * L_15;
		L_15 = UnknownFieldSet_Clone_mBCB3196178FBF9E17BF3C686C6C6DC214D0BBDEA(L_14, /*hidden argument*/NULL);
		__this->set__unknownFields_1(L_15);
		return;
	}
}
// Google.Protobuf.Reflection.SourceCodeInfo/Types/Location Google.Protobuf.Reflection.SourceCodeInfo/Types/Location::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4 * Location_Clone_m3C4C0598AE1D773542602ABBFBFC912ABB270BEE (Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4 * L_0 = (Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4 *)il2cpp_codegen_object_new(Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_il2cpp_TypeInfo_var);
		Location__ctor_m28EFCE79F70879A58E3CB6F54CE731BD38BE41DD(L_0, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// Google.Protobuf.Collections.RepeatedField`1<System.Int32> Google.Protobuf.Reflection.SourceCodeInfo/Types/Location::get_Path()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419 * Location_get_Path_m84C8DAADFB80B1112A8BAEB93E662D7CE667384A (Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4 * __this, const RuntimeMethod* method)
{
	{
		RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419 * L_0 = __this->get_path__3();
		return L_0;
	}
}
// Google.Protobuf.Collections.RepeatedField`1<System.Int32> Google.Protobuf.Reflection.SourceCodeInfo/Types/Location::get_Span()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419 * Location_get_Span_mFCF1B654E8BF0DB117024A4D1B6C6C1586C35D86 (Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4 * __this, const RuntimeMethod* method)
{
	{
		RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419 * L_0 = __this->get_span__5();
		return L_0;
	}
}
// System.String Google.Protobuf.Reflection.SourceCodeInfo/Types/Location::get_LeadingComments()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Location_get_LeadingComments_m7C54DD44CC201EEEA4654C52684874B58A20217C (Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	String_t* G_B2_0 = NULL;
	String_t* G_B1_0 = NULL;
	{
		String_t* L_0 = __this->get_leadingComments__7();
		String_t* L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_000f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_il2cpp_TypeInfo_var);
		String_t* L_2 = ((Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_StaticFields*)il2cpp_codegen_static_fields_for(Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_il2cpp_TypeInfo_var))->get_LeadingCommentsDefaultValue_6();
		G_B2_0 = L_2;
	}

IL_000f:
	{
		return G_B2_0;
	}
}
// System.Void Google.Protobuf.Reflection.SourceCodeInfo/Types/Location::set_LeadingComments(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Location_set_LeadingComments_m5C93169FF580ED160690B569AD1DB27A25CF1084 (Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ProtoPreconditions_CheckNotNull_TisString_t_m80AE3118869FAEF4DF83AF0D521B284CF725144E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral46F273EF641E07D271D91E0DC24A4392582671F8);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___value0;
		String_t* L_1;
		L_1 = ProtoPreconditions_CheckNotNull_TisString_t_m80AE3118869FAEF4DF83AF0D521B284CF725144E(L_0, _stringLiteral46F273EF641E07D271D91E0DC24A4392582671F8, /*hidden argument*/ProtoPreconditions_CheckNotNull_TisString_t_m80AE3118869FAEF4DF83AF0D521B284CF725144E_RuntimeMethod_var);
		__this->set_leadingComments__7(L_1);
		return;
	}
}
// System.Boolean Google.Protobuf.Reflection.SourceCodeInfo/Types/Location::get_HasLeadingComments()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Location_get_HasLeadingComments_m3C25DA28CE42357EA74945D022AD19BB7CEC0997 (Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_leadingComments__7();
		return (bool)((!(((RuntimeObject*)(String_t*)L_0) <= ((RuntimeObject*)(RuntimeObject *)NULL)))? 1 : 0);
	}
}
// System.String Google.Protobuf.Reflection.SourceCodeInfo/Types/Location::get_TrailingComments()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Location_get_TrailingComments_m46A2E3D945E49EC181F43757653BA3D43A1F3F86 (Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	String_t* G_B2_0 = NULL;
	String_t* G_B1_0 = NULL;
	{
		String_t* L_0 = __this->get_trailingComments__9();
		String_t* L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_000f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_il2cpp_TypeInfo_var);
		String_t* L_2 = ((Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_StaticFields*)il2cpp_codegen_static_fields_for(Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_il2cpp_TypeInfo_var))->get_TrailingCommentsDefaultValue_8();
		G_B2_0 = L_2;
	}

IL_000f:
	{
		return G_B2_0;
	}
}
// System.Void Google.Protobuf.Reflection.SourceCodeInfo/Types/Location::set_TrailingComments(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Location_set_TrailingComments_mD43CECC13E2083938944FEF61AEA9ADDD195E17A (Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ProtoPreconditions_CheckNotNull_TisString_t_m80AE3118869FAEF4DF83AF0D521B284CF725144E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral46F273EF641E07D271D91E0DC24A4392582671F8);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___value0;
		String_t* L_1;
		L_1 = ProtoPreconditions_CheckNotNull_TisString_t_m80AE3118869FAEF4DF83AF0D521B284CF725144E(L_0, _stringLiteral46F273EF641E07D271D91E0DC24A4392582671F8, /*hidden argument*/ProtoPreconditions_CheckNotNull_TisString_t_m80AE3118869FAEF4DF83AF0D521B284CF725144E_RuntimeMethod_var);
		__this->set_trailingComments__9(L_1);
		return;
	}
}
// System.Boolean Google.Protobuf.Reflection.SourceCodeInfo/Types/Location::get_HasTrailingComments()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Location_get_HasTrailingComments_mED1CB68639231AE0730E2FB6FB2C8875D89EF1F5 (Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_trailingComments__9();
		return (bool)((!(((RuntimeObject*)(String_t*)L_0) <= ((RuntimeObject*)(RuntimeObject *)NULL)))? 1 : 0);
	}
}
// Google.Protobuf.Collections.RepeatedField`1<System.String> Google.Protobuf.Reflection.SourceCodeInfo/Types/Location::get_LeadingDetachedComments()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RepeatedField_1_t03A3C968DD0FF896E3F108784B11A7CBD313CDE1 * Location_get_LeadingDetachedComments_m83F1C5A77721B6E95FC9FA1EB78A456F542DAD7E (Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4 * __this, const RuntimeMethod* method)
{
	{
		RepeatedField_1_t03A3C968DD0FF896E3F108784B11A7CBD313CDE1 * L_0 = __this->get_leadingDetachedComments__11();
		return L_0;
	}
}
// System.Boolean Google.Protobuf.Reflection.SourceCodeInfo/Types/Location::Equals(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Location_Equals_m85AD8ECE48C60DADB8768733AF1F6563AF4AC281 (Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4 * __this, RuntimeObject * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject * L_0 = ___other0;
		bool L_1;
		L_1 = Location_Equals_m8D982EACDA1888B6588CD7689C00D5EBB2985368(__this, ((Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4 *)IsInstSealed((RuntimeObject*)L_0, Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean Google.Protobuf.Reflection.SourceCodeInfo/Types/Location::Equals(Google.Protobuf.Reflection.SourceCodeInfo/Types/Location)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Location_Equals_m8D982EACDA1888B6588CD7689C00D5EBB2985368 (Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4 * __this, Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&RepeatedField_1_Equals_m2D0B705880C1B033423F8A185C0D0DFE9EEDAADA_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&RepeatedField_1_Equals_m4322AAE559CBB034117AE082A33552981014A7AF_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4 * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0005;
		}
	}
	{
		return (bool)0;
	}

IL_0005:
	{
		Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4 * L_1 = ___other0;
		if ((!(((RuntimeObject*)(Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4 *)L_1) == ((RuntimeObject*)(Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4 *)__this))))
		{
			goto IL_000b;
		}
	}
	{
		return (bool)1;
	}

IL_000b:
	{
		RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419 * L_2 = __this->get_path__3();
		Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4 * L_3 = ___other0;
		NullCheck(L_3);
		RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419 * L_4 = L_3->get_path__3();
		NullCheck(L_2);
		bool L_5;
		L_5 = RepeatedField_1_Equals_m2D0B705880C1B033423F8A185C0D0DFE9EEDAADA(L_2, L_4, /*hidden argument*/RepeatedField_1_Equals_m2D0B705880C1B033423F8A185C0D0DFE9EEDAADA_RuntimeMethod_var);
		if (L_5)
		{
			goto IL_0020;
		}
	}
	{
		return (bool)0;
	}

IL_0020:
	{
		RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419 * L_6 = __this->get_span__5();
		Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4 * L_7 = ___other0;
		NullCheck(L_7);
		RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419 * L_8 = L_7->get_span__5();
		NullCheck(L_6);
		bool L_9;
		L_9 = RepeatedField_1_Equals_m2D0B705880C1B033423F8A185C0D0DFE9EEDAADA(L_6, L_8, /*hidden argument*/RepeatedField_1_Equals_m2D0B705880C1B033423F8A185C0D0DFE9EEDAADA_RuntimeMethod_var);
		if (L_9)
		{
			goto IL_0035;
		}
	}
	{
		return (bool)0;
	}

IL_0035:
	{
		String_t* L_10;
		L_10 = Location_get_LeadingComments_m7C54DD44CC201EEEA4654C52684874B58A20217C(__this, /*hidden argument*/NULL);
		Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4 * L_11 = ___other0;
		NullCheck(L_11);
		String_t* L_12;
		L_12 = Location_get_LeadingComments_m7C54DD44CC201EEEA4654C52684874B58A20217C(L_11, /*hidden argument*/NULL);
		bool L_13;
		L_13 = String_op_Inequality_mDDA2DDED3E7EF042987EB7180EE3E88105F0AAE2(L_10, L_12, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_004a;
		}
	}
	{
		return (bool)0;
	}

IL_004a:
	{
		String_t* L_14;
		L_14 = Location_get_TrailingComments_m46A2E3D945E49EC181F43757653BA3D43A1F3F86(__this, /*hidden argument*/NULL);
		Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4 * L_15 = ___other0;
		NullCheck(L_15);
		String_t* L_16;
		L_16 = Location_get_TrailingComments_m46A2E3D945E49EC181F43757653BA3D43A1F3F86(L_15, /*hidden argument*/NULL);
		bool L_17;
		L_17 = String_op_Inequality_mDDA2DDED3E7EF042987EB7180EE3E88105F0AAE2(L_14, L_16, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_005f;
		}
	}
	{
		return (bool)0;
	}

IL_005f:
	{
		RepeatedField_1_t03A3C968DD0FF896E3F108784B11A7CBD313CDE1 * L_18 = __this->get_leadingDetachedComments__11();
		Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4 * L_19 = ___other0;
		NullCheck(L_19);
		RepeatedField_1_t03A3C968DD0FF896E3F108784B11A7CBD313CDE1 * L_20 = L_19->get_leadingDetachedComments__11();
		NullCheck(L_18);
		bool L_21;
		L_21 = RepeatedField_1_Equals_m4322AAE559CBB034117AE082A33552981014A7AF(L_18, L_20, /*hidden argument*/RepeatedField_1_Equals_m4322AAE559CBB034117AE082A33552981014A7AF_RuntimeMethod_var);
		if (L_21)
		{
			goto IL_0074;
		}
	}
	{
		return (bool)0;
	}

IL_0074:
	{
		UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 * L_22 = __this->get__unknownFields_1();
		Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4 * L_23 = ___other0;
		NullCheck(L_23);
		UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 * L_24 = L_23->get__unknownFields_1();
		bool L_25;
		L_25 = Object_Equals_mBE334AF263BDADA1D0F1DE7E14456A63E47F8053(L_22, L_24, /*hidden argument*/NULL);
		return L_25;
	}
}
// System.Int32 Google.Protobuf.Reflection.SourceCodeInfo/Types/Location::GetHashCode()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Location_GetHashCode_m6ACF33DBF8F3F18D5D7DD313D7729EA5839A174B (Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 1;
		int32_t L_0 = V_0;
		RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419 * L_1 = __this->get_path__3();
		NullCheck(L_1);
		int32_t L_2;
		L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, L_1);
		V_0 = ((int32_t)((int32_t)L_0^(int32_t)L_2));
		int32_t L_3 = V_0;
		RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419 * L_4 = __this->get_span__5();
		NullCheck(L_4);
		int32_t L_5;
		L_5 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, L_4);
		V_0 = ((int32_t)((int32_t)L_3^(int32_t)L_5));
		bool L_6;
		L_6 = Location_get_HasLeadingComments_m3C25DA28CE42357EA74945D022AD19BB7CEC0997(__this, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0034;
		}
	}
	{
		int32_t L_7 = V_0;
		String_t* L_8;
		L_8 = Location_get_LeadingComments_m7C54DD44CC201EEEA4654C52684874B58A20217C(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		int32_t L_9;
		L_9 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, L_8);
		V_0 = ((int32_t)((int32_t)L_7^(int32_t)L_9));
	}

IL_0034:
	{
		bool L_10;
		L_10 = Location_get_HasTrailingComments_mED1CB68639231AE0730E2FB6FB2C8875D89EF1F5(__this, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_004a;
		}
	}
	{
		int32_t L_11 = V_0;
		String_t* L_12;
		L_12 = Location_get_TrailingComments_m46A2E3D945E49EC181F43757653BA3D43A1F3F86(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		int32_t L_13;
		L_13 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, L_12);
		V_0 = ((int32_t)((int32_t)L_11^(int32_t)L_13));
	}

IL_004a:
	{
		int32_t L_14 = V_0;
		RepeatedField_1_t03A3C968DD0FF896E3F108784B11A7CBD313CDE1 * L_15 = __this->get_leadingDetachedComments__11();
		NullCheck(L_15);
		int32_t L_16;
		L_16 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, L_15);
		V_0 = ((int32_t)((int32_t)L_14^(int32_t)L_16));
		UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 * L_17 = __this->get__unknownFields_1();
		if (!L_17)
		{
			goto IL_006e;
		}
	}
	{
		int32_t L_18 = V_0;
		UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 * L_19 = __this->get__unknownFields_1();
		NullCheck(L_19);
		int32_t L_20;
		L_20 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, L_19);
		V_0 = ((int32_t)((int32_t)L_18^(int32_t)L_20));
	}

IL_006e:
	{
		int32_t L_21 = V_0;
		return L_21;
	}
}
// System.String Google.Protobuf.Reflection.SourceCodeInfo/Types/Location::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Location_ToString_m9B282141CD535FF8261C8195BF3CDDF528C40A4C (Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&JsonFormatter_t29EDD2B5D01B4FD88FD4DBF274554818F8ED11C7_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonFormatter_t29EDD2B5D01B4FD88FD4DBF274554818F8ED11C7_il2cpp_TypeInfo_var);
		String_t* L_0;
		L_0 = JsonFormatter_ToDiagnosticString_mB0CBCBDBF65F17FC52DEB9E0C49184E44F3F2EF1(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void Google.Protobuf.Reflection.SourceCodeInfo/Types/Location::WriteTo(Google.Protobuf.CodedOutputStream)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Location_WriteTo_m4210E1B1440977E2A413E0B8F75133982F73EC69 (Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4 * __this, CodedOutputStream_t2871B0D17F920BFB54B564E2295D9753721642E6 * ___output0, const RuntimeMethod* method)
{
	{
		CodedOutputStream_t2871B0D17F920BFB54B564E2295D9753721642E6 * L_0 = ___output0;
		NullCheck(L_0);
		CodedOutputStream_WriteRawMessage_m05CED72DF9A800E9BFB28956F21E7B4416E03D11(L_0, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Google.Protobuf.Reflection.SourceCodeInfo/Types/Location::pb::Google.Protobuf.IBufferMessage.InternalWriteTo(Google.Protobuf.WriteContext&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Location_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m504F51927BE7797E64ECDEC999C03BB04846EB33 (Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4 * __this, WriteContext_t4011B33FF5B289B23B81D9929C82A44F0A24DFCB * ___output0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&RepeatedField_1_WriteTo_m2D9D72EB2016DEE58A88C1C8873C2D512C638215_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&RepeatedField_1_WriteTo_mC5C250B204D43F5D814F1C19A1337853828264B8_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419 * L_0 = __this->get_path__3();
		WriteContext_t4011B33FF5B289B23B81D9929C82A44F0A24DFCB * L_1 = ___output0;
		IL2CPP_RUNTIME_CLASS_INIT(Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_il2cpp_TypeInfo_var);
		FieldCodec_1_tAB7BF6BD67CE831FB7E8A437894272BD9C511681 * L_2 = ((Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_StaticFields*)il2cpp_codegen_static_fields_for(Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_il2cpp_TypeInfo_var))->get__repeated_path_codec_2();
		NullCheck(L_0);
		RepeatedField_1_WriteTo_mC5C250B204D43F5D814F1C19A1337853828264B8(L_0, (WriteContext_t4011B33FF5B289B23B81D9929C82A44F0A24DFCB *)L_1, L_2, /*hidden argument*/RepeatedField_1_WriteTo_mC5C250B204D43F5D814F1C19A1337853828264B8_RuntimeMethod_var);
		RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419 * L_3 = __this->get_span__5();
		WriteContext_t4011B33FF5B289B23B81D9929C82A44F0A24DFCB * L_4 = ___output0;
		FieldCodec_1_tAB7BF6BD67CE831FB7E8A437894272BD9C511681 * L_5 = ((Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_StaticFields*)il2cpp_codegen_static_fields_for(Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_il2cpp_TypeInfo_var))->get__repeated_span_codec_4();
		NullCheck(L_3);
		RepeatedField_1_WriteTo_mC5C250B204D43F5D814F1C19A1337853828264B8(L_3, (WriteContext_t4011B33FF5B289B23B81D9929C82A44F0A24DFCB *)L_4, L_5, /*hidden argument*/RepeatedField_1_WriteTo_mC5C250B204D43F5D814F1C19A1337853828264B8_RuntimeMethod_var);
		bool L_6;
		L_6 = Location_get_HasLeadingComments_m3C25DA28CE42357EA74945D022AD19BB7CEC0997(__this, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_003e;
		}
	}
	{
		WriteContext_t4011B33FF5B289B23B81D9929C82A44F0A24DFCB * L_7 = ___output0;
		WriteContext_WriteRawTag_m3E37F0B83B4E8B7C7E1CC6472D527AA22B11DA5A((WriteContext_t4011B33FF5B289B23B81D9929C82A44F0A24DFCB *)L_7, (uint8_t)((int32_t)26), /*hidden argument*/NULL);
		WriteContext_t4011B33FF5B289B23B81D9929C82A44F0A24DFCB * L_8 = ___output0;
		String_t* L_9;
		L_9 = Location_get_LeadingComments_m7C54DD44CC201EEEA4654C52684874B58A20217C(__this, /*hidden argument*/NULL);
		WriteContext_WriteString_m1A0F46C5CF383B3F976BFBD24B546EC2E346C68C((WriteContext_t4011B33FF5B289B23B81D9929C82A44F0A24DFCB *)L_8, L_9, /*hidden argument*/NULL);
	}

IL_003e:
	{
		bool L_10;
		L_10 = Location_get_HasTrailingComments_mED1CB68639231AE0730E2FB6FB2C8875D89EF1F5(__this, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_005a;
		}
	}
	{
		WriteContext_t4011B33FF5B289B23B81D9929C82A44F0A24DFCB * L_11 = ___output0;
		WriteContext_WriteRawTag_m3E37F0B83B4E8B7C7E1CC6472D527AA22B11DA5A((WriteContext_t4011B33FF5B289B23B81D9929C82A44F0A24DFCB *)L_11, (uint8_t)((int32_t)34), /*hidden argument*/NULL);
		WriteContext_t4011B33FF5B289B23B81D9929C82A44F0A24DFCB * L_12 = ___output0;
		String_t* L_13;
		L_13 = Location_get_TrailingComments_m46A2E3D945E49EC181F43757653BA3D43A1F3F86(__this, /*hidden argument*/NULL);
		WriteContext_WriteString_m1A0F46C5CF383B3F976BFBD24B546EC2E346C68C((WriteContext_t4011B33FF5B289B23B81D9929C82A44F0A24DFCB *)L_12, L_13, /*hidden argument*/NULL);
	}

IL_005a:
	{
		RepeatedField_1_t03A3C968DD0FF896E3F108784B11A7CBD313CDE1 * L_14 = __this->get_leadingDetachedComments__11();
		WriteContext_t4011B33FF5B289B23B81D9929C82A44F0A24DFCB * L_15 = ___output0;
		IL2CPP_RUNTIME_CLASS_INIT(Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_il2cpp_TypeInfo_var);
		FieldCodec_1_t379D692C872A4BD7DE0539465056B8EF49D48FA0 * L_16 = ((Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_StaticFields*)il2cpp_codegen_static_fields_for(Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_il2cpp_TypeInfo_var))->get__repeated_leadingDetachedComments_codec_10();
		NullCheck(L_14);
		RepeatedField_1_WriteTo_m2D9D72EB2016DEE58A88C1C8873C2D512C638215(L_14, (WriteContext_t4011B33FF5B289B23B81D9929C82A44F0A24DFCB *)L_15, L_16, /*hidden argument*/RepeatedField_1_WriteTo_m2D9D72EB2016DEE58A88C1C8873C2D512C638215_RuntimeMethod_var);
		UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 * L_17 = __this->get__unknownFields_1();
		if (!L_17)
		{
			goto IL_007f;
		}
	}
	{
		UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 * L_18 = __this->get__unknownFields_1();
		WriteContext_t4011B33FF5B289B23B81D9929C82A44F0A24DFCB * L_19 = ___output0;
		NullCheck(L_18);
		UnknownFieldSet_WriteTo_mECF1EFA7AF68EBBEDDD046865B2C17131B969E1B(L_18, (WriteContext_t4011B33FF5B289B23B81D9929C82A44F0A24DFCB *)L_19, /*hidden argument*/NULL);
	}

IL_007f:
	{
		return;
	}
}
// System.Int32 Google.Protobuf.Reflection.SourceCodeInfo/Types/Location::CalculateSize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Location_CalculateSize_m4FE7C1E7A7BB0E885F9A9779A16EC3D562B6BA3F (Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CodedOutputStream_t2871B0D17F920BFB54B564E2295D9753721642E6_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&RepeatedField_1_CalculateSize_m67536E773582CAB8A68E5056E9A1D318FAE1942B_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&RepeatedField_1_CalculateSize_mBD445305760770A9C733116354E5B30CD5DC2EE3_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		int32_t L_0 = V_0;
		RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419 * L_1 = __this->get_path__3();
		IL2CPP_RUNTIME_CLASS_INIT(Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_il2cpp_TypeInfo_var);
		FieldCodec_1_tAB7BF6BD67CE831FB7E8A437894272BD9C511681 * L_2 = ((Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_StaticFields*)il2cpp_codegen_static_fields_for(Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_il2cpp_TypeInfo_var))->get__repeated_path_codec_2();
		NullCheck(L_1);
		int32_t L_3;
		L_3 = RepeatedField_1_CalculateSize_m67536E773582CAB8A68E5056E9A1D318FAE1942B(L_1, L_2, /*hidden argument*/RepeatedField_1_CalculateSize_m67536E773582CAB8A68E5056E9A1D318FAE1942B_RuntimeMethod_var);
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_0, (int32_t)L_3));
		int32_t L_4 = V_0;
		RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419 * L_5 = __this->get_span__5();
		FieldCodec_1_tAB7BF6BD67CE831FB7E8A437894272BD9C511681 * L_6 = ((Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_StaticFields*)il2cpp_codegen_static_fields_for(Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_il2cpp_TypeInfo_var))->get__repeated_span_codec_4();
		NullCheck(L_5);
		int32_t L_7;
		L_7 = RepeatedField_1_CalculateSize_m67536E773582CAB8A68E5056E9A1D318FAE1942B(L_5, L_6, /*hidden argument*/RepeatedField_1_CalculateSize_m67536E773582CAB8A68E5056E9A1D318FAE1942B_RuntimeMethod_var);
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)L_7));
		bool L_8;
		L_8 = Location_get_HasLeadingComments_m3C25DA28CE42357EA74945D022AD19BB7CEC0997(__this, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0040;
		}
	}
	{
		int32_t L_9 = V_0;
		String_t* L_10;
		L_10 = Location_get_LeadingComments_m7C54DD44CC201EEEA4654C52684874B58A20217C(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CodedOutputStream_t2871B0D17F920BFB54B564E2295D9753721642E6_il2cpp_TypeInfo_var);
		int32_t L_11;
		L_11 = CodedOutputStream_ComputeStringSize_m3913248CC93D6067C32930171244FD185D818BDF(L_10, /*hidden argument*/NULL);
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_9, (int32_t)((int32_t)il2cpp_codegen_add((int32_t)1, (int32_t)L_11))));
	}

IL_0040:
	{
		bool L_12;
		L_12 = Location_get_HasTrailingComments_mED1CB68639231AE0730E2FB6FB2C8875D89EF1F5(__this, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0058;
		}
	}
	{
		int32_t L_13 = V_0;
		String_t* L_14;
		L_14 = Location_get_TrailingComments_m46A2E3D945E49EC181F43757653BA3D43A1F3F86(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CodedOutputStream_t2871B0D17F920BFB54B564E2295D9753721642E6_il2cpp_TypeInfo_var);
		int32_t L_15;
		L_15 = CodedOutputStream_ComputeStringSize_m3913248CC93D6067C32930171244FD185D818BDF(L_14, /*hidden argument*/NULL);
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_13, (int32_t)((int32_t)il2cpp_codegen_add((int32_t)1, (int32_t)L_15))));
	}

IL_0058:
	{
		int32_t L_16 = V_0;
		RepeatedField_1_t03A3C968DD0FF896E3F108784B11A7CBD313CDE1 * L_17 = __this->get_leadingDetachedComments__11();
		IL2CPP_RUNTIME_CLASS_INIT(Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_il2cpp_TypeInfo_var);
		FieldCodec_1_t379D692C872A4BD7DE0539465056B8EF49D48FA0 * L_18 = ((Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_StaticFields*)il2cpp_codegen_static_fields_for(Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_il2cpp_TypeInfo_var))->get__repeated_leadingDetachedComments_codec_10();
		NullCheck(L_17);
		int32_t L_19;
		L_19 = RepeatedField_1_CalculateSize_mBD445305760770A9C733116354E5B30CD5DC2EE3(L_17, L_18, /*hidden argument*/RepeatedField_1_CalculateSize_mBD445305760770A9C733116354E5B30CD5DC2EE3_RuntimeMethod_var);
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_16, (int32_t)L_19));
		UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 * L_20 = __this->get__unknownFields_1();
		if (!L_20)
		{
			goto IL_0081;
		}
	}
	{
		int32_t L_21 = V_0;
		UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 * L_22 = __this->get__unknownFields_1();
		NullCheck(L_22);
		int32_t L_23;
		L_23 = UnknownFieldSet_CalculateSize_m0F48042AFDC7E6795E7A99D8B3BE0D5C61C96868(L_22, /*hidden argument*/NULL);
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_21, (int32_t)L_23));
	}

IL_0081:
	{
		int32_t L_24 = V_0;
		return L_24;
	}
}
// System.Void Google.Protobuf.Reflection.SourceCodeInfo/Types/Location::MergeFrom(Google.Protobuf.Reflection.SourceCodeInfo/Types/Location)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Location_MergeFrom_mFA64C2E2E2D47FD3294936AD3C696EAE3B0CD8F7 (Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4 * __this, Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&RepeatedField_1_Add_m65F8D4082B764C2A1CC6367692FB566C2A0D75F8_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&RepeatedField_1_Add_m9B3360A97708D5F88CB74E729387922F7CFFF0FE_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4 * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0004;
		}
	}
	{
		return;
	}

IL_0004:
	{
		RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419 * L_1 = __this->get_path__3();
		Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4 * L_2 = ___other0;
		NullCheck(L_2);
		RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419 * L_3 = L_2->get_path__3();
		NullCheck(L_1);
		RepeatedField_1_Add_m65F8D4082B764C2A1CC6367692FB566C2A0D75F8(L_1, L_3, /*hidden argument*/RepeatedField_1_Add_m65F8D4082B764C2A1CC6367692FB566C2A0D75F8_RuntimeMethod_var);
		RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419 * L_4 = __this->get_span__5();
		Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4 * L_5 = ___other0;
		NullCheck(L_5);
		RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419 * L_6 = L_5->get_span__5();
		NullCheck(L_4);
		RepeatedField_1_Add_m65F8D4082B764C2A1CC6367692FB566C2A0D75F8(L_4, L_6, /*hidden argument*/RepeatedField_1_Add_m65F8D4082B764C2A1CC6367692FB566C2A0D75F8_RuntimeMethod_var);
		Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4 * L_7 = ___other0;
		NullCheck(L_7);
		bool L_8;
		L_8 = Location_get_HasLeadingComments_m3C25DA28CE42357EA74945D022AD19BB7CEC0997(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_003a;
		}
	}
	{
		Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4 * L_9 = ___other0;
		NullCheck(L_9);
		String_t* L_10;
		L_10 = Location_get_LeadingComments_m7C54DD44CC201EEEA4654C52684874B58A20217C(L_9, /*hidden argument*/NULL);
		Location_set_LeadingComments_m5C93169FF580ED160690B569AD1DB27A25CF1084(__this, L_10, /*hidden argument*/NULL);
	}

IL_003a:
	{
		Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4 * L_11 = ___other0;
		NullCheck(L_11);
		bool L_12;
		L_12 = Location_get_HasTrailingComments_mED1CB68639231AE0730E2FB6FB2C8875D89EF1F5(L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_004e;
		}
	}
	{
		Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4 * L_13 = ___other0;
		NullCheck(L_13);
		String_t* L_14;
		L_14 = Location_get_TrailingComments_m46A2E3D945E49EC181F43757653BA3D43A1F3F86(L_13, /*hidden argument*/NULL);
		Location_set_TrailingComments_mD43CECC13E2083938944FEF61AEA9ADDD195E17A(__this, L_14, /*hidden argument*/NULL);
	}

IL_004e:
	{
		RepeatedField_1_t03A3C968DD0FF896E3F108784B11A7CBD313CDE1 * L_15 = __this->get_leadingDetachedComments__11();
		Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4 * L_16 = ___other0;
		NullCheck(L_16);
		RepeatedField_1_t03A3C968DD0FF896E3F108784B11A7CBD313CDE1 * L_17 = L_16->get_leadingDetachedComments__11();
		NullCheck(L_15);
		RepeatedField_1_Add_m9B3360A97708D5F88CB74E729387922F7CFFF0FE(L_15, L_17, /*hidden argument*/RepeatedField_1_Add_m9B3360A97708D5F88CB74E729387922F7CFFF0FE_RuntimeMethod_var);
		UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 * L_18 = __this->get__unknownFields_1();
		Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4 * L_19 = ___other0;
		NullCheck(L_19);
		UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 * L_20 = L_19->get__unknownFields_1();
		UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 * L_21;
		L_21 = UnknownFieldSet_MergeFrom_m3175C4FF04375A5EF7DC4E3245C6606303344F85(L_18, L_20, /*hidden argument*/NULL);
		__this->set__unknownFields_1(L_21);
		return;
	}
}
// System.Void Google.Protobuf.Reflection.SourceCodeInfo/Types/Location::MergeFrom(Google.Protobuf.CodedInputStream)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Location_MergeFrom_mD097D430111330C3EE05F9F06685B68DB4A7077D (Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4 * __this, CodedInputStream_tC76F9515605395C11D7F9437E0E06C828232B0C8 * ___input0, const RuntimeMethod* method)
{
	{
		CodedInputStream_tC76F9515605395C11D7F9437E0E06C828232B0C8 * L_0 = ___input0;
		NullCheck(L_0);
		CodedInputStream_ReadRawMessage_m77E217968CD5654CC717E7DA57906C0219372943(L_0, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Google.Protobuf.Reflection.SourceCodeInfo/Types/Location::pb::Google.Protobuf.IBufferMessage.InternalMergeFrom(Google.Protobuf.ParseContext&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Location_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m30960A0BA81FA14E2F5806B1821B17CEB6EC74A8 (Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4 * __this, ParseContext_t288EAC5562A1D7E0FF8A06DBA58B8CF732ECE349 * ___input0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&RepeatedField_1_AddEntriesFrom_m1CD228D26654811414016D368DDCDD8D3AFC85A4_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&RepeatedField_1_AddEntriesFrom_m5AFE58E765285935D3E71F09A425AD7BE2D56674_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		goto IL_009c;
	}

IL_0005:
	{
		uint32_t L_0 = V_0;
		if ((!(((uint32_t)L_0) <= ((uint32_t)((int32_t)16)))))
		{
			goto IL_001a;
		}
	}
	{
		uint32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)8)))
		{
			goto IL_0049;
		}
	}
	{
		uint32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)((int32_t)10))))
		{
			goto IL_0049;
		}
	}
	{
		uint32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)((int32_t)16))))
		{
			goto IL_005c;
		}
	}
	{
		goto IL_0035;
	}

IL_001a:
	{
		uint32_t L_4 = V_0;
		if ((!(((uint32_t)L_4) <= ((uint32_t)((int32_t)26)))))
		{
			goto IL_002b;
		}
	}
	{
		uint32_t L_5 = V_0;
		if ((((int32_t)L_5) == ((int32_t)((int32_t)18))))
		{
			goto IL_005c;
		}
	}
	{
		uint32_t L_6 = V_0;
		if ((((int32_t)L_6) == ((int32_t)((int32_t)26))))
		{
			goto IL_006f;
		}
	}
	{
		goto IL_0035;
	}

IL_002b:
	{
		uint32_t L_7 = V_0;
		if ((((int32_t)L_7) == ((int32_t)((int32_t)34))))
		{
			goto IL_007d;
		}
	}
	{
		uint32_t L_8 = V_0;
		if ((((int32_t)L_8) == ((int32_t)((int32_t)50))))
		{
			goto IL_008b;
		}
	}

IL_0035:
	{
		UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 * L_9 = __this->get__unknownFields_1();
		ParseContext_t288EAC5562A1D7E0FF8A06DBA58B8CF732ECE349 * L_10 = ___input0;
		UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 * L_11;
		L_11 = UnknownFieldSet_MergeFieldFrom_m6D9E32ADBB0194941C65CD43522F53C3A2378283(L_9, (ParseContext_t288EAC5562A1D7E0FF8A06DBA58B8CF732ECE349 *)L_10, /*hidden argument*/NULL);
		__this->set__unknownFields_1(L_11);
		goto IL_009c;
	}

IL_0049:
	{
		RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419 * L_12 = __this->get_path__3();
		ParseContext_t288EAC5562A1D7E0FF8A06DBA58B8CF732ECE349 * L_13 = ___input0;
		IL2CPP_RUNTIME_CLASS_INIT(Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_il2cpp_TypeInfo_var);
		FieldCodec_1_tAB7BF6BD67CE831FB7E8A437894272BD9C511681 * L_14 = ((Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_StaticFields*)il2cpp_codegen_static_fields_for(Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_il2cpp_TypeInfo_var))->get__repeated_path_codec_2();
		NullCheck(L_12);
		RepeatedField_1_AddEntriesFrom_m5AFE58E765285935D3E71F09A425AD7BE2D56674(L_12, (ParseContext_t288EAC5562A1D7E0FF8A06DBA58B8CF732ECE349 *)L_13, L_14, /*hidden argument*/RepeatedField_1_AddEntriesFrom_m5AFE58E765285935D3E71F09A425AD7BE2D56674_RuntimeMethod_var);
		goto IL_009c;
	}

IL_005c:
	{
		RepeatedField_1_t4EDE50F84D2750F6C2B21CE840B5F1D9A109B419 * L_15 = __this->get_span__5();
		ParseContext_t288EAC5562A1D7E0FF8A06DBA58B8CF732ECE349 * L_16 = ___input0;
		IL2CPP_RUNTIME_CLASS_INIT(Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_il2cpp_TypeInfo_var);
		FieldCodec_1_tAB7BF6BD67CE831FB7E8A437894272BD9C511681 * L_17 = ((Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_StaticFields*)il2cpp_codegen_static_fields_for(Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_il2cpp_TypeInfo_var))->get__repeated_span_codec_4();
		NullCheck(L_15);
		RepeatedField_1_AddEntriesFrom_m5AFE58E765285935D3E71F09A425AD7BE2D56674(L_15, (ParseContext_t288EAC5562A1D7E0FF8A06DBA58B8CF732ECE349 *)L_16, L_17, /*hidden argument*/RepeatedField_1_AddEntriesFrom_m5AFE58E765285935D3E71F09A425AD7BE2D56674_RuntimeMethod_var);
		goto IL_009c;
	}

IL_006f:
	{
		ParseContext_t288EAC5562A1D7E0FF8A06DBA58B8CF732ECE349 * L_18 = ___input0;
		String_t* L_19;
		L_19 = ParseContext_ReadString_mA21E24240255C281D36669A209628AD6FDBB15AE_inline((ParseContext_t288EAC5562A1D7E0FF8A06DBA58B8CF732ECE349 *)L_18, /*hidden argument*/NULL);
		Location_set_LeadingComments_m5C93169FF580ED160690B569AD1DB27A25CF1084(__this, L_19, /*hidden argument*/NULL);
		goto IL_009c;
	}

IL_007d:
	{
		ParseContext_t288EAC5562A1D7E0FF8A06DBA58B8CF732ECE349 * L_20 = ___input0;
		String_t* L_21;
		L_21 = ParseContext_ReadString_mA21E24240255C281D36669A209628AD6FDBB15AE_inline((ParseContext_t288EAC5562A1D7E0FF8A06DBA58B8CF732ECE349 *)L_20, /*hidden argument*/NULL);
		Location_set_TrailingComments_mD43CECC13E2083938944FEF61AEA9ADDD195E17A(__this, L_21, /*hidden argument*/NULL);
		goto IL_009c;
	}

IL_008b:
	{
		RepeatedField_1_t03A3C968DD0FF896E3F108784B11A7CBD313CDE1 * L_22 = __this->get_leadingDetachedComments__11();
		ParseContext_t288EAC5562A1D7E0FF8A06DBA58B8CF732ECE349 * L_23 = ___input0;
		IL2CPP_RUNTIME_CLASS_INIT(Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_il2cpp_TypeInfo_var);
		FieldCodec_1_t379D692C872A4BD7DE0539465056B8EF49D48FA0 * L_24 = ((Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_StaticFields*)il2cpp_codegen_static_fields_for(Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_il2cpp_TypeInfo_var))->get__repeated_leadingDetachedComments_codec_10();
		NullCheck(L_22);
		RepeatedField_1_AddEntriesFrom_m1CD228D26654811414016D368DDCDD8D3AFC85A4(L_22, (ParseContext_t288EAC5562A1D7E0FF8A06DBA58B8CF732ECE349 *)L_23, L_24, /*hidden argument*/RepeatedField_1_AddEntriesFrom_m1CD228D26654811414016D368DDCDD8D3AFC85A4_RuntimeMethod_var);
	}

IL_009c:
	{
		ParseContext_t288EAC5562A1D7E0FF8A06DBA58B8CF732ECE349 * L_25 = ___input0;
		uint32_t L_26;
		L_26 = ParseContext_ReadTag_mEE687A18078235454AEA63F1F61E9E3E9EB0260F_inline((ParseContext_t288EAC5562A1D7E0FF8A06DBA58B8CF732ECE349 *)L_25, /*hidden argument*/NULL);
		uint32_t L_27 = L_26;
		V_0 = L_27;
		if (L_27)
		{
			goto IL_0005;
		}
	}
	{
		return;
	}
}
// System.Void Google.Protobuf.Reflection.SourceCodeInfo/Types/Location::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Location__cctor_m4F2D645AE7099736A3AA036CED46C3C27A92FB8A (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_1__ctor_m2DA47B0B49E42E600E186F7701E4C322C935D1D4_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_1_t08ED00E63E70686FA636AFAACFA58BD631D35696_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MessageParser_1__ctor_m58870D9692E7115AE9C0CE22F33EF7950ED7BF19_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MessageParser_1_t09C483F589C454D2B080958F963D017315003983_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_U3C_cctorU3Eb__55_0_m4EB0DCECA4202B15C7EC20815CEFDEBD1E0AFB9B_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_tE6E47C051562B8F8FCE9347E1B99439CF27AB1AF_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_tE6E47C051562B8F8FCE9347E1B99439CF27AB1AF_il2cpp_TypeInfo_var);
		U3CU3Ec_tE6E47C051562B8F8FCE9347E1B99439CF27AB1AF * L_0 = ((U3CU3Ec_tE6E47C051562B8F8FCE9347E1B99439CF27AB1AF_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tE6E47C051562B8F8FCE9347E1B99439CF27AB1AF_il2cpp_TypeInfo_var))->get_U3CU3E9_0();
		Func_1_t08ED00E63E70686FA636AFAACFA58BD631D35696 * L_1 = (Func_1_t08ED00E63E70686FA636AFAACFA58BD631D35696 *)il2cpp_codegen_object_new(Func_1_t08ED00E63E70686FA636AFAACFA58BD631D35696_il2cpp_TypeInfo_var);
		Func_1__ctor_m2DA47B0B49E42E600E186F7701E4C322C935D1D4(L_1, L_0, (intptr_t)((intptr_t)U3CU3Ec_U3C_cctorU3Eb__55_0_m4EB0DCECA4202B15C7EC20815CEFDEBD1E0AFB9B_RuntimeMethod_var), /*hidden argument*/Func_1__ctor_m2DA47B0B49E42E600E186F7701E4C322C935D1D4_RuntimeMethod_var);
		MessageParser_1_t09C483F589C454D2B080958F963D017315003983 * L_2 = (MessageParser_1_t09C483F589C454D2B080958F963D017315003983 *)il2cpp_codegen_object_new(MessageParser_1_t09C483F589C454D2B080958F963D017315003983_il2cpp_TypeInfo_var);
		MessageParser_1__ctor_m58870D9692E7115AE9C0CE22F33EF7950ED7BF19(L_2, L_1, /*hidden argument*/MessageParser_1__ctor_m58870D9692E7115AE9C0CE22F33EF7950ED7BF19_RuntimeMethod_var);
		((Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_StaticFields*)il2cpp_codegen_static_fields_for(Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_il2cpp_TypeInfo_var))->set__parser_0(L_2);
		FieldCodec_1_tAB7BF6BD67CE831FB7E8A437894272BD9C511681 * L_3;
		L_3 = FieldCodec_ForInt32_mA8FC008DA1A37C17DED262F93274DACAEAE60621(((int32_t)10), /*hidden argument*/NULL);
		((Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_StaticFields*)il2cpp_codegen_static_fields_for(Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_il2cpp_TypeInfo_var))->set__repeated_path_codec_2(L_3);
		FieldCodec_1_tAB7BF6BD67CE831FB7E8A437894272BD9C511681 * L_4;
		L_4 = FieldCodec_ForInt32_mA8FC008DA1A37C17DED262F93274DACAEAE60621(((int32_t)18), /*hidden argument*/NULL);
		((Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_StaticFields*)il2cpp_codegen_static_fields_for(Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_il2cpp_TypeInfo_var))->set__repeated_span_codec_4(L_4);
		((Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_StaticFields*)il2cpp_codegen_static_fields_for(Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_il2cpp_TypeInfo_var))->set_LeadingCommentsDefaultValue_6(_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		((Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_StaticFields*)il2cpp_codegen_static_fields_for(Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_il2cpp_TypeInfo_var))->set_TrailingCommentsDefaultValue_8(_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		FieldCodec_1_t379D692C872A4BD7DE0539465056B8EF49D48FA0 * L_5;
		L_5 = FieldCodec_ForString_m686A669CFD9CF6A06AE0329BB75BFF8D20C17A32(((int32_t)50), /*hidden argument*/NULL);
		((Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_StaticFields*)il2cpp_codegen_static_fields_for(Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_il2cpp_TypeInfo_var))->set__repeated_leadingDetachedComments_codec_10(L_5);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Google.Protobuf.MessageParser`1<Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart> Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart::get_Parser()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR MessageParser_1_tE61087F0F9CB9EE7534691A864269E589F1453DF * NamePart_get_Parser_m52D44251BA5B107F48BA9D858E82CD1C2499F151 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22_il2cpp_TypeInfo_var);
		MessageParser_1_tE61087F0F9CB9EE7534691A864269E589F1453DF * L_0 = ((NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22_StaticFields*)il2cpp_codegen_static_fields_for(NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22_il2cpp_TypeInfo_var))->get__parser_0();
		return L_0;
	}
}
// Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart::get_Descriptor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13 * NamePart_get_Descriptor_mDAF09A4912482470FFDD7664FB4A245050AB4D99 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IList_1_tACE6E5172CFB9B21666313779F73E7389A285312_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728_il2cpp_TypeInfo_var);
		MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13 * L_0;
		L_0 = UninterpretedOption_get_Descriptor_m178F55E2F36773B937FB50177A2EA1EED60B24BA(/*hidden argument*/NULL);
		NullCheck(L_0);
		RuntimeObject* L_1;
		L_1 = MessageDescriptor_get_NestedTypes_m34A524E3C7D560C1537685CF6291AB60DA58FE7E_inline(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13 * L_2;
		L_2 = InterfaceFuncInvoker1< MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13 *, int32_t >::Invoke(0 /* !0 System.Collections.Generic.IList`1<Google.Protobuf.Reflection.MessageDescriptor>::get_Item(System.Int32) */, IList_1_tACE6E5172CFB9B21666313779F73E7389A285312_il2cpp_TypeInfo_var, L_1, 0);
		return L_2;
	}
}
// Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart::pb::Google.Protobuf.IMessage.get_Descriptor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13 * NamePart_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_mB4EBA66DA1D50FDC6D037B624317EAF1C1EC424F (NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22_il2cpp_TypeInfo_var);
		MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13 * L_0;
		L_0 = NamePart_get_Descriptor_mDAF09A4912482470FFDD7664FB4A245050AB4D99(/*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NamePart__ctor_mA9A9103300EE0850829D3BFE67B52EAD4512F085 (NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart::.ctor(Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NamePart__ctor_mFBC708208AD503A756034768ABF66A5E5E0AF4DD (NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22 * __this, NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22 * ___other0, const RuntimeMethod* method)
{
	{
		NamePart__ctor_mA9A9103300EE0850829D3BFE67B52EAD4512F085(__this, /*hidden argument*/NULL);
		NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22 * L_0 = ___other0;
		NullCheck(L_0);
		int32_t L_1 = L_0->get__hasBits0_2();
		__this->set__hasBits0_2(L_1);
		NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22 * L_2 = ___other0;
		NullCheck(L_2);
		String_t* L_3 = L_2->get_namePart__4();
		__this->set_namePart__4(L_3);
		NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22 * L_4 = ___other0;
		NullCheck(L_4);
		bool L_5 = L_4->get_isExtension__6();
		__this->set_isExtension__6(L_5);
		NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22 * L_6 = ___other0;
		NullCheck(L_6);
		UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 * L_7 = L_6->get__unknownFields_1();
		UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 * L_8;
		L_8 = UnknownFieldSet_Clone_mBCB3196178FBF9E17BF3C686C6C6DC214D0BBDEA(L_7, /*hidden argument*/NULL);
		__this->set__unknownFields_1(L_8);
		return;
	}
}
// Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22 * NamePart_Clone_m43B2E0BD86099C758B706C19DB038F56A2409C6F (NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22 * L_0 = (NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22 *)il2cpp_codegen_object_new(NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22_il2cpp_TypeInfo_var);
		NamePart__ctor_mFBC708208AD503A756034768ABF66A5E5E0AF4DD(L_0, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.String Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart::get_NamePart_()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* NamePart_get_NamePart__m745E8A77A6D196C10AEBD19CD97CD8FFFD6771F9 (NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	String_t* G_B2_0 = NULL;
	String_t* G_B1_0 = NULL;
	{
		String_t* L_0 = __this->get_namePart__4();
		String_t* L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_000f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22_il2cpp_TypeInfo_var);
		String_t* L_2 = ((NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22_StaticFields*)il2cpp_codegen_static_fields_for(NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22_il2cpp_TypeInfo_var))->get_NamePart_DefaultValue_3();
		G_B2_0 = L_2;
	}

IL_000f:
	{
		return G_B2_0;
	}
}
// System.Void Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart::set_NamePart_(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NamePart_set_NamePart__mB05B00F6AEB8BCC4C03176FB92DADB8C7A49B55E (NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ProtoPreconditions_CheckNotNull_TisString_t_m80AE3118869FAEF4DF83AF0D521B284CF725144E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral46F273EF641E07D271D91E0DC24A4392582671F8);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___value0;
		String_t* L_1;
		L_1 = ProtoPreconditions_CheckNotNull_TisString_t_m80AE3118869FAEF4DF83AF0D521B284CF725144E(L_0, _stringLiteral46F273EF641E07D271D91E0DC24A4392582671F8, /*hidden argument*/ProtoPreconditions_CheckNotNull_TisString_t_m80AE3118869FAEF4DF83AF0D521B284CF725144E_RuntimeMethod_var);
		__this->set_namePart__4(L_1);
		return;
	}
}
// System.Boolean Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart::get_HasNamePart_()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool NamePart_get_HasNamePart__mCEDB12E23C8A15CAD08E15A07D98285265637660 (NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_namePart__4();
		return (bool)((!(((RuntimeObject*)(String_t*)L_0) <= ((RuntimeObject*)(RuntimeObject *)NULL)))? 1 : 0);
	}
}
// System.Boolean Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart::get_IsExtension()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool NamePart_get_IsExtension_mF1F181A90E440604432916AA943CE44DA7BADDEA (NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get__hasBits0_2();
		if (!((int32_t)((int32_t)L_0&(int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		bool L_1 = __this->get_isExtension__6();
		return L_1;
	}

IL_0011:
	{
		IL2CPP_RUNTIME_CLASS_INIT(NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22_il2cpp_TypeInfo_var);
		bool L_2 = ((NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22_StaticFields*)il2cpp_codegen_static_fields_for(NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22_il2cpp_TypeInfo_var))->get_IsExtensionDefaultValue_5();
		return L_2;
	}
}
// System.Void Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart::set_IsExtension(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NamePart_set_IsExtension_m01E6A8E808AB54ACA78194DB40DC1D897F7713A2 (NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get__hasBits0_2();
		__this->set__hasBits0_2(((int32_t)((int32_t)L_0|(int32_t)1)));
		bool L_1 = ___value0;
		__this->set_isExtension__6(L_1);
		return;
	}
}
// System.Boolean Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart::get_HasIsExtension()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool NamePart_get_HasIsExtension_m5A187250EBDBA107E51D79C0BD502D42D5965B9D (NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get__hasBits0_2();
		return (bool)((!(((uint32_t)((int32_t)((int32_t)L_0&(int32_t)1))) <= ((uint32_t)0)))? 1 : 0);
	}
}
// System.Boolean Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart::Equals(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool NamePart_Equals_m9F4FC0E7DAF91E9141210A33B1AFE8F2127F68BB (NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22 * __this, RuntimeObject * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject * L_0 = ___other0;
		bool L_1;
		L_1 = NamePart_Equals_m4F68F7BFA6DC8252C7BF46F9B7B7A4561BC9B297(__this, ((NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22 *)IsInstSealed((RuntimeObject*)L_0, NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart::Equals(Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool NamePart_Equals_m4F68F7BFA6DC8252C7BF46F9B7B7A4561BC9B297 (NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22 * __this, NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22 * ___other0, const RuntimeMethod* method)
{
	{
		NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22 * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0005;
		}
	}
	{
		return (bool)0;
	}

IL_0005:
	{
		NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22 * L_1 = ___other0;
		if ((!(((RuntimeObject*)(NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22 *)L_1) == ((RuntimeObject*)(NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22 *)__this))))
		{
			goto IL_000b;
		}
	}
	{
		return (bool)1;
	}

IL_000b:
	{
		String_t* L_2;
		L_2 = NamePart_get_NamePart__m745E8A77A6D196C10AEBD19CD97CD8FFFD6771F9(__this, /*hidden argument*/NULL);
		NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22 * L_3 = ___other0;
		NullCheck(L_3);
		String_t* L_4;
		L_4 = NamePart_get_NamePart__m745E8A77A6D196C10AEBD19CD97CD8FFFD6771F9(L_3, /*hidden argument*/NULL);
		bool L_5;
		L_5 = String_op_Inequality_mDDA2DDED3E7EF042987EB7180EE3E88105F0AAE2(L_2, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0020;
		}
	}
	{
		return (bool)0;
	}

IL_0020:
	{
		bool L_6;
		L_6 = NamePart_get_IsExtension_mF1F181A90E440604432916AA943CE44DA7BADDEA(__this, /*hidden argument*/NULL);
		NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22 * L_7 = ___other0;
		NullCheck(L_7);
		bool L_8;
		L_8 = NamePart_get_IsExtension_mF1F181A90E440604432916AA943CE44DA7BADDEA(L_7, /*hidden argument*/NULL);
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0030;
		}
	}
	{
		return (bool)0;
	}

IL_0030:
	{
		UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 * L_9 = __this->get__unknownFields_1();
		NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22 * L_10 = ___other0;
		NullCheck(L_10);
		UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 * L_11 = L_10->get__unknownFields_1();
		bool L_12;
		L_12 = Object_Equals_mBE334AF263BDADA1D0F1DE7E14456A63E47F8053(L_9, L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
// System.Int32 Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart::GetHashCode()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t NamePart_GetHashCode_m84C037E032844843C3501CBBF1D5579160297F11 (NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	bool V_1 = false;
	{
		V_0 = 1;
		bool L_0;
		L_0 = NamePart_get_HasNamePart__mCEDB12E23C8A15CAD08E15A07D98285265637660(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		int32_t L_1 = V_0;
		String_t* L_2;
		L_2 = NamePart_get_NamePart__m745E8A77A6D196C10AEBD19CD97CD8FFFD6771F9(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		int32_t L_3;
		L_3 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, L_2);
		V_0 = ((int32_t)((int32_t)L_1^(int32_t)L_3));
	}

IL_0018:
	{
		bool L_4;
		L_4 = NamePart_get_HasIsExtension_m5A187250EBDBA107E51D79C0BD502D42D5965B9D(__this, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0031;
		}
	}
	{
		int32_t L_5 = V_0;
		bool L_6;
		L_6 = NamePart_get_IsExtension_mF1F181A90E440604432916AA943CE44DA7BADDEA(__this, /*hidden argument*/NULL);
		V_1 = L_6;
		int32_t L_7;
		L_7 = Boolean_GetHashCode_m03AF8B3CECAE9106C44A00E3B33E51CBFC45C411((bool*)(&V_1), /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)L_5^(int32_t)L_7));
	}

IL_0031:
	{
		UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 * L_8 = __this->get__unknownFields_1();
		if (!L_8)
		{
			goto IL_0047;
		}
	}
	{
		int32_t L_9 = V_0;
		UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 * L_10 = __this->get__unknownFields_1();
		NullCheck(L_10);
		int32_t L_11;
		L_11 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, L_10);
		V_0 = ((int32_t)((int32_t)L_9^(int32_t)L_11));
	}

IL_0047:
	{
		int32_t L_12 = V_0;
		return L_12;
	}
}
// System.String Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* NamePart_ToString_mF10088283D361B38968A6E57F78255654D3BB2D1 (NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&JsonFormatter_t29EDD2B5D01B4FD88FD4DBF274554818F8ED11C7_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonFormatter_t29EDD2B5D01B4FD88FD4DBF274554818F8ED11C7_il2cpp_TypeInfo_var);
		String_t* L_0;
		L_0 = JsonFormatter_ToDiagnosticString_mB0CBCBDBF65F17FC52DEB9E0C49184E44F3F2EF1(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart::WriteTo(Google.Protobuf.CodedOutputStream)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NamePart_WriteTo_m9F9C644A5C5530F04A945671E7BBA3FEF7061448 (NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22 * __this, CodedOutputStream_t2871B0D17F920BFB54B564E2295D9753721642E6 * ___output0, const RuntimeMethod* method)
{
	{
		CodedOutputStream_t2871B0D17F920BFB54B564E2295D9753721642E6 * L_0 = ___output0;
		NullCheck(L_0);
		CodedOutputStream_WriteRawMessage_m05CED72DF9A800E9BFB28956F21E7B4416E03D11(L_0, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart::pb::Google.Protobuf.IBufferMessage.InternalWriteTo(Google.Protobuf.WriteContext&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NamePart_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m0396A0FC112B2CFF1E5B1FD1C0AE400359E96A01 (NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22 * __this, WriteContext_t4011B33FF5B289B23B81D9929C82A44F0A24DFCB * ___output0, const RuntimeMethod* method)
{
	{
		bool L_0;
		L_0 = NamePart_get_HasNamePart__mCEDB12E23C8A15CAD08E15A07D98285265637660(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		WriteContext_t4011B33FF5B289B23B81D9929C82A44F0A24DFCB * L_1 = ___output0;
		WriteContext_WriteRawTag_m3E37F0B83B4E8B7C7E1CC6472D527AA22B11DA5A((WriteContext_t4011B33FF5B289B23B81D9929C82A44F0A24DFCB *)L_1, (uint8_t)((int32_t)10), /*hidden argument*/NULL);
		WriteContext_t4011B33FF5B289B23B81D9929C82A44F0A24DFCB * L_2 = ___output0;
		String_t* L_3;
		L_3 = NamePart_get_NamePart__m745E8A77A6D196C10AEBD19CD97CD8FFFD6771F9(__this, /*hidden argument*/NULL);
		WriteContext_WriteString_m1A0F46C5CF383B3F976BFBD24B546EC2E346C68C((WriteContext_t4011B33FF5B289B23B81D9929C82A44F0A24DFCB *)L_2, L_3, /*hidden argument*/NULL);
	}

IL_001c:
	{
		bool L_4;
		L_4 = NamePart_get_HasIsExtension_m5A187250EBDBA107E51D79C0BD502D42D5965B9D(__this, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0038;
		}
	}
	{
		WriteContext_t4011B33FF5B289B23B81D9929C82A44F0A24DFCB * L_5 = ___output0;
		WriteContext_WriteRawTag_m3E37F0B83B4E8B7C7E1CC6472D527AA22B11DA5A((WriteContext_t4011B33FF5B289B23B81D9929C82A44F0A24DFCB *)L_5, (uint8_t)((int32_t)16), /*hidden argument*/NULL);
		WriteContext_t4011B33FF5B289B23B81D9929C82A44F0A24DFCB * L_6 = ___output0;
		bool L_7;
		L_7 = NamePart_get_IsExtension_mF1F181A90E440604432916AA943CE44DA7BADDEA(__this, /*hidden argument*/NULL);
		WriteContext_WriteBool_m68B7D58774A48FFC3FF34D9A6F770E579EEFA7DF((WriteContext_t4011B33FF5B289B23B81D9929C82A44F0A24DFCB *)L_6, L_7, /*hidden argument*/NULL);
	}

IL_0038:
	{
		UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 * L_8 = __this->get__unknownFields_1();
		if (!L_8)
		{
			goto IL_004c;
		}
	}
	{
		UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 * L_9 = __this->get__unknownFields_1();
		WriteContext_t4011B33FF5B289B23B81D9929C82A44F0A24DFCB * L_10 = ___output0;
		NullCheck(L_9);
		UnknownFieldSet_WriteTo_mECF1EFA7AF68EBBEDDD046865B2C17131B969E1B(L_9, (WriteContext_t4011B33FF5B289B23B81D9929C82A44F0A24DFCB *)L_10, /*hidden argument*/NULL);
	}

IL_004c:
	{
		return;
	}
}
// System.Int32 Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart::CalculateSize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t NamePart_CalculateSize_mE593E657449DE890C6273E4C9DC3B90A948C8749 (NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CodedOutputStream_t2871B0D17F920BFB54B564E2295D9753721642E6_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		bool L_0;
		L_0 = NamePart_get_HasNamePart__mCEDB12E23C8A15CAD08E15A07D98285265637660(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		int32_t L_1 = V_0;
		String_t* L_2;
		L_2 = NamePart_get_NamePart__m745E8A77A6D196C10AEBD19CD97CD8FFFD6771F9(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CodedOutputStream_t2871B0D17F920BFB54B564E2295D9753721642E6_il2cpp_TypeInfo_var);
		int32_t L_3;
		L_3 = CodedOutputStream_ComputeStringSize_m3913248CC93D6067C32930171244FD185D818BDF(L_2, /*hidden argument*/NULL);
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_1, (int32_t)((int32_t)il2cpp_codegen_add((int32_t)1, (int32_t)L_3))));
	}

IL_001a:
	{
		bool L_4;
		L_4 = NamePart_get_HasIsExtension_m5A187250EBDBA107E51D79C0BD502D42D5965B9D(__this, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0026;
		}
	}
	{
		int32_t L_5 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_5, (int32_t)2));
	}

IL_0026:
	{
		UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 * L_6 = __this->get__unknownFields_1();
		if (!L_6)
		{
			goto IL_003c;
		}
	}
	{
		int32_t L_7 = V_0;
		UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 * L_8 = __this->get__unknownFields_1();
		NullCheck(L_8);
		int32_t L_9;
		L_9 = UnknownFieldSet_CalculateSize_m0F48042AFDC7E6795E7A99D8B3BE0D5C61C96868(L_8, /*hidden argument*/NULL);
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_7, (int32_t)L_9));
	}

IL_003c:
	{
		int32_t L_10 = V_0;
		return L_10;
	}
}
// System.Void Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart::MergeFrom(Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NamePart_MergeFrom_mB5380B100DD6F68EDEEF200E1BB000EFC4906DA7 (NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22 * __this, NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22 * ___other0, const RuntimeMethod* method)
{
	{
		NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22 * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0004;
		}
	}
	{
		return;
	}

IL_0004:
	{
		NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22 * L_1 = ___other0;
		NullCheck(L_1);
		bool L_2;
		L_2 = NamePart_get_HasNamePart__mCEDB12E23C8A15CAD08E15A07D98285265637660(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0018;
		}
	}
	{
		NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22 * L_3 = ___other0;
		NullCheck(L_3);
		String_t* L_4;
		L_4 = NamePart_get_NamePart__m745E8A77A6D196C10AEBD19CD97CD8FFFD6771F9(L_3, /*hidden argument*/NULL);
		NamePart_set_NamePart__mB05B00F6AEB8BCC4C03176FB92DADB8C7A49B55E(__this, L_4, /*hidden argument*/NULL);
	}

IL_0018:
	{
		NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22 * L_5 = ___other0;
		NullCheck(L_5);
		bool L_6;
		L_6 = NamePart_get_HasIsExtension_m5A187250EBDBA107E51D79C0BD502D42D5965B9D(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_002c;
		}
	}
	{
		NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22 * L_7 = ___other0;
		NullCheck(L_7);
		bool L_8;
		L_8 = NamePart_get_IsExtension_mF1F181A90E440604432916AA943CE44DA7BADDEA(L_7, /*hidden argument*/NULL);
		NamePart_set_IsExtension_m01E6A8E808AB54ACA78194DB40DC1D897F7713A2(__this, L_8, /*hidden argument*/NULL);
	}

IL_002c:
	{
		UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 * L_9 = __this->get__unknownFields_1();
		NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22 * L_10 = ___other0;
		NullCheck(L_10);
		UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 * L_11 = L_10->get__unknownFields_1();
		UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 * L_12;
		L_12 = UnknownFieldSet_MergeFrom_m3175C4FF04375A5EF7DC4E3245C6606303344F85(L_9, L_11, /*hidden argument*/NULL);
		__this->set__unknownFields_1(L_12);
		return;
	}
}
// System.Void Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart::MergeFrom(Google.Protobuf.CodedInputStream)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NamePart_MergeFrom_m4889B005A581C203986474994D59CAD2FC9F5F30 (NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22 * __this, CodedInputStream_tC76F9515605395C11D7F9437E0E06C828232B0C8 * ___input0, const RuntimeMethod* method)
{
	{
		CodedInputStream_tC76F9515605395C11D7F9437E0E06C828232B0C8 * L_0 = ___input0;
		NullCheck(L_0);
		CodedInputStream_ReadRawMessage_m77E217968CD5654CC717E7DA57906C0219372943(L_0, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart::pb::Google.Protobuf.IBufferMessage.InternalMergeFrom(Google.Protobuf.ParseContext&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NamePart_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m161542DC71B24D4416D0327851A5EFD2B8A093D2 (NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22 * __this, ParseContext_t288EAC5562A1D7E0FF8A06DBA58B8CF732ECE349 * ___input0, const RuntimeMethod* method)
{
	uint32_t V_0 = 0;
	{
		goto IL_003a;
	}

IL_0002:
	{
		uint32_t L_0 = V_0;
		if ((((int32_t)L_0) == ((int32_t)((int32_t)10))))
		{
			goto IL_0020;
		}
	}
	{
		uint32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)((int32_t)16))))
		{
			goto IL_002e;
		}
	}
	{
		UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 * L_2 = __this->get__unknownFields_1();
		ParseContext_t288EAC5562A1D7E0FF8A06DBA58B8CF732ECE349 * L_3 = ___input0;
		UnknownFieldSet_t41C131B6E3D3D67CA53110A0D82CAA9DBECEFF72 * L_4;
		L_4 = UnknownFieldSet_MergeFieldFrom_m6D9E32ADBB0194941C65CD43522F53C3A2378283(L_2, (ParseContext_t288EAC5562A1D7E0FF8A06DBA58B8CF732ECE349 *)L_3, /*hidden argument*/NULL);
		__this->set__unknownFields_1(L_4);
		goto IL_003a;
	}

IL_0020:
	{
		ParseContext_t288EAC5562A1D7E0FF8A06DBA58B8CF732ECE349 * L_5 = ___input0;
		String_t* L_6;
		L_6 = ParseContext_ReadString_mA21E24240255C281D36669A209628AD6FDBB15AE_inline((ParseContext_t288EAC5562A1D7E0FF8A06DBA58B8CF732ECE349 *)L_5, /*hidden argument*/NULL);
		NamePart_set_NamePart__mB05B00F6AEB8BCC4C03176FB92DADB8C7A49B55E(__this, L_6, /*hidden argument*/NULL);
		goto IL_003a;
	}

IL_002e:
	{
		ParseContext_t288EAC5562A1D7E0FF8A06DBA58B8CF732ECE349 * L_7 = ___input0;
		bool L_8;
		L_8 = ParseContext_ReadBool_m097C5083B8554A5F33B1799DD8BF30926A5942BE_inline((ParseContext_t288EAC5562A1D7E0FF8A06DBA58B8CF732ECE349 *)L_7, /*hidden argument*/NULL);
		NamePart_set_IsExtension_m01E6A8E808AB54ACA78194DB40DC1D897F7713A2(__this, L_8, /*hidden argument*/NULL);
	}

IL_003a:
	{
		ParseContext_t288EAC5562A1D7E0FF8A06DBA58B8CF732ECE349 * L_9 = ___input0;
		uint32_t L_10;
		L_10 = ParseContext_ReadTag_mEE687A18078235454AEA63F1F61E9E3E9EB0260F_inline((ParseContext_t288EAC5562A1D7E0FF8A06DBA58B8CF732ECE349 *)L_9, /*hidden argument*/NULL);
		uint32_t L_11 = L_10;
		V_0 = L_11;
		if (L_11)
		{
			goto IL_0002;
		}
	}
	{
		return;
	}
}
// System.Void Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NamePart__cctor_mAD02D8D937C525BAD836480091C7B3FEB91509C4 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_1__ctor_mFFBE264549A7AF17CBECB2E724E4B75F436FD857_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_1_t732F679FB575BFC105666600B1FC8E28B480BBAB_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MessageParser_1__ctor_m6B193EF7595551528130D40FD5AF408E99171248_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MessageParser_1_tE61087F0F9CB9EE7534691A864269E589F1453DF_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_U3C_cctorU3Eb__41_0_m613BA3E40E15DB658425AEB73890F58D554969E4_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_t70C29F3AE73CD37C0D77DCC96A139645BE59A453_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t70C29F3AE73CD37C0D77DCC96A139645BE59A453_il2cpp_TypeInfo_var);
		U3CU3Ec_t70C29F3AE73CD37C0D77DCC96A139645BE59A453 * L_0 = ((U3CU3Ec_t70C29F3AE73CD37C0D77DCC96A139645BE59A453_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t70C29F3AE73CD37C0D77DCC96A139645BE59A453_il2cpp_TypeInfo_var))->get_U3CU3E9_0();
		Func_1_t732F679FB575BFC105666600B1FC8E28B480BBAB * L_1 = (Func_1_t732F679FB575BFC105666600B1FC8E28B480BBAB *)il2cpp_codegen_object_new(Func_1_t732F679FB575BFC105666600B1FC8E28B480BBAB_il2cpp_TypeInfo_var);
		Func_1__ctor_mFFBE264549A7AF17CBECB2E724E4B75F436FD857(L_1, L_0, (intptr_t)((intptr_t)U3CU3Ec_U3C_cctorU3Eb__41_0_m613BA3E40E15DB658425AEB73890F58D554969E4_RuntimeMethod_var), /*hidden argument*/Func_1__ctor_mFFBE264549A7AF17CBECB2E724E4B75F436FD857_RuntimeMethod_var);
		MessageParser_1_tE61087F0F9CB9EE7534691A864269E589F1453DF * L_2 = (MessageParser_1_tE61087F0F9CB9EE7534691A864269E589F1453DF *)il2cpp_codegen_object_new(MessageParser_1_tE61087F0F9CB9EE7534691A864269E589F1453DF_il2cpp_TypeInfo_var);
		MessageParser_1__ctor_m6B193EF7595551528130D40FD5AF408E99171248(L_2, L_1, /*hidden argument*/MessageParser_1__ctor_m6B193EF7595551528130D40FD5AF408E99171248_RuntimeMethod_var);
		((NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22_StaticFields*)il2cpp_codegen_static_fields_for(NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22_il2cpp_TypeInfo_var))->set__parser_0(L_2);
		((NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22_StaticFields*)il2cpp_codegen_static_fields_for(NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22_il2cpp_TypeInfo_var))->set_NamePart_DefaultValue_3(_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		((NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22_StaticFields*)il2cpp_codegen_static_fields_for(NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22_il2cpp_TypeInfo_var))->set_IsExtensionDefaultValue_5((bool)0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Google.Protobuf.Reflection.DescriptorProto/Types/ExtensionRange/<>c::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__cctor_m39D6E6EC8A756DFEC357E0BF0076C0DFD801B3ED (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_tC3789B2A2639D39522B7FC88CE44B0FF78387D85_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CU3Ec_tC3789B2A2639D39522B7FC88CE44B0FF78387D85 * L_0 = (U3CU3Ec_tC3789B2A2639D39522B7FC88CE44B0FF78387D85 *)il2cpp_codegen_object_new(U3CU3Ec_tC3789B2A2639D39522B7FC88CE44B0FF78387D85_il2cpp_TypeInfo_var);
		U3CU3Ec__ctor_mAB207C6304C4309948492B33329C4030BBDA3EB3(L_0, /*hidden argument*/NULL);
		((U3CU3Ec_tC3789B2A2639D39522B7FC88CE44B0FF78387D85_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tC3789B2A2639D39522B7FC88CE44B0FF78387D85_il2cpp_TypeInfo_var))->set_U3CU3E9_0(L_0);
		return;
	}
}
// System.Void Google.Protobuf.Reflection.DescriptorProto/Types/ExtensionRange/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_mAB207C6304C4309948492B33329C4030BBDA3EB3 (U3CU3Ec_tC3789B2A2639D39522B7FC88CE44B0FF78387D85 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// Google.Protobuf.Reflection.DescriptorProto/Types/ExtensionRange Google.Protobuf.Reflection.DescriptorProto/Types/ExtensionRange/<>c::<.cctor>b__46_0()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ExtensionRange_t355AED379D3458AC80443047C2DF72AA44639D84 * U3CU3Ec_U3C_cctorU3Eb__46_0_m686DD5E765624AF43B355C35E0982F62E9EF4D69 (U3CU3Ec_tC3789B2A2639D39522B7FC88CE44B0FF78387D85 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ExtensionRange_t355AED379D3458AC80443047C2DF72AA44639D84_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		ExtensionRange_t355AED379D3458AC80443047C2DF72AA44639D84 * L_0 = (ExtensionRange_t355AED379D3458AC80443047C2DF72AA44639D84 *)il2cpp_codegen_object_new(ExtensionRange_t355AED379D3458AC80443047C2DF72AA44639D84_il2cpp_TypeInfo_var);
		ExtensionRange__ctor_mCE3CBBCDC18FA5EA4ADBF4AF4AF53D13327D323B(L_0, /*hidden argument*/NULL);
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Google.Protobuf.Reflection.DescriptorProto/Types/ReservedRange/<>c::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__cctor_m842920908C66FD0DBBD8BB949911FBBEE8646463 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_t14ED7AF2EE0452B68867D51843E5E3FBF40F7D2D_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CU3Ec_t14ED7AF2EE0452B68867D51843E5E3FBF40F7D2D * L_0 = (U3CU3Ec_t14ED7AF2EE0452B68867D51843E5E3FBF40F7D2D *)il2cpp_codegen_object_new(U3CU3Ec_t14ED7AF2EE0452B68867D51843E5E3FBF40F7D2D_il2cpp_TypeInfo_var);
		U3CU3Ec__ctor_m5826A097FE555448C5C789E783537C4451C2909A(L_0, /*hidden argument*/NULL);
		((U3CU3Ec_t14ED7AF2EE0452B68867D51843E5E3FBF40F7D2D_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t14ED7AF2EE0452B68867D51843E5E3FBF40F7D2D_il2cpp_TypeInfo_var))->set_U3CU3E9_0(L_0);
		return;
	}
}
// System.Void Google.Protobuf.Reflection.DescriptorProto/Types/ReservedRange/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m5826A097FE555448C5C789E783537C4451C2909A (U3CU3Ec_t14ED7AF2EE0452B68867D51843E5E3FBF40F7D2D * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// Google.Protobuf.Reflection.DescriptorProto/Types/ReservedRange Google.Protobuf.Reflection.DescriptorProto/Types/ReservedRange/<>c::<.cctor>b__41_0()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ReservedRange_tAED8F62AF040F5F133732196696893B313EA458B * U3CU3Ec_U3C_cctorU3Eb__41_0_m61BF31F913FE7F0E7A30777DA3CD3957025589E1 (U3CU3Ec_t14ED7AF2EE0452B68867D51843E5E3FBF40F7D2D * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ReservedRange_tAED8F62AF040F5F133732196696893B313EA458B_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		ReservedRange_tAED8F62AF040F5F133732196696893B313EA458B * L_0 = (ReservedRange_tAED8F62AF040F5F133732196696893B313EA458B *)il2cpp_codegen_object_new(ReservedRange_tAED8F62AF040F5F133732196696893B313EA458B_il2cpp_TypeInfo_var);
		ReservedRange__ctor_mB2F8257CCD2E526672CB4B552FD6799706F38CD5(L_0, /*hidden argument*/NULL);
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange/<>c::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__cctor_m4FA27EDEEFE8562E0CC992077B8C1D26C49865FA (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_t0FB8D11271B41B39C63CE8A142DE7F8F87630794_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CU3Ec_t0FB8D11271B41B39C63CE8A142DE7F8F87630794 * L_0 = (U3CU3Ec_t0FB8D11271B41B39C63CE8A142DE7F8F87630794 *)il2cpp_codegen_object_new(U3CU3Ec_t0FB8D11271B41B39C63CE8A142DE7F8F87630794_il2cpp_TypeInfo_var);
		U3CU3Ec__ctor_m9D85A171DC63EF24760F067CDB4D72BDE4E10F18(L_0, /*hidden argument*/NULL);
		((U3CU3Ec_t0FB8D11271B41B39C63CE8A142DE7F8F87630794_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t0FB8D11271B41B39C63CE8A142DE7F8F87630794_il2cpp_TypeInfo_var))->set_U3CU3E9_0(L_0);
		return;
	}
}
// System.Void Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m9D85A171DC63EF24760F067CDB4D72BDE4E10F18 (U3CU3Ec_t0FB8D11271B41B39C63CE8A142DE7F8F87630794 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange/<>c::<.cctor>b__41_0()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41 * U3CU3Ec_U3C_cctorU3Eb__41_0_m0FBA9F917C842E30E53B32B2B8E897821F32A8C3 (U3CU3Ec_t0FB8D11271B41B39C63CE8A142DE7F8F87630794 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41 * L_0 = (EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41 *)il2cpp_codegen_object_new(EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41_il2cpp_TypeInfo_var);
		EnumReservedRange__ctor_m02904F51FF5FE5B92BAA1E816D3A1B13534A6E0C(L_0, /*hidden argument*/NULL);
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation/<>c::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__cctor_m6812A7FE464B0BD846EBAB7F8B83B3E764B351DD (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_t48164166658FE6E4AA4BC81699272A7660D5A345_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CU3Ec_t48164166658FE6E4AA4BC81699272A7660D5A345 * L_0 = (U3CU3Ec_t48164166658FE6E4AA4BC81699272A7660D5A345 *)il2cpp_codegen_object_new(U3CU3Ec_t48164166658FE6E4AA4BC81699272A7660D5A345_il2cpp_TypeInfo_var);
		U3CU3Ec__ctor_mCC6C861EA46D5F38CE7B1D231D970DAFF8929007(L_0, /*hidden argument*/NULL);
		((U3CU3Ec_t48164166658FE6E4AA4BC81699272A7660D5A345_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t48164166658FE6E4AA4BC81699272A7660D5A345_il2cpp_TypeInfo_var))->set_U3CU3E9_0(L_0);
		return;
	}
}
// System.Void Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_mCC6C861EA46D5F38CE7B1D231D970DAFF8929007 (U3CU3Ec_t48164166658FE6E4AA4BC81699272A7660D5A345 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation/<>c::<.cctor>b__55_0()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405 * U3CU3Ec_U3C_cctorU3Eb__55_0_m3FCF05CD6B82754BB25E87E1A3A8225796468906 (U3CU3Ec_t48164166658FE6E4AA4BC81699272A7660D5A345 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405 * L_0 = (Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405 *)il2cpp_codegen_object_new(Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_il2cpp_TypeInfo_var);
		Annotation__ctor_m9ED6A93F900EBFD3D4BC4980C3D9B032B8F9B5D7(L_0, /*hidden argument*/NULL);
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Google.Protobuf.Reflection.SourceCodeInfo/Types/Location/<>c::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__cctor_m311FEE18EEA023FC4C54A69ED1F10E1073A1A258 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_tE6E47C051562B8F8FCE9347E1B99439CF27AB1AF_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CU3Ec_tE6E47C051562B8F8FCE9347E1B99439CF27AB1AF * L_0 = (U3CU3Ec_tE6E47C051562B8F8FCE9347E1B99439CF27AB1AF *)il2cpp_codegen_object_new(U3CU3Ec_tE6E47C051562B8F8FCE9347E1B99439CF27AB1AF_il2cpp_TypeInfo_var);
		U3CU3Ec__ctor_m0D7F18F9E242952A4718672889E7A6D4750E8C15(L_0, /*hidden argument*/NULL);
		((U3CU3Ec_tE6E47C051562B8F8FCE9347E1B99439CF27AB1AF_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tE6E47C051562B8F8FCE9347E1B99439CF27AB1AF_il2cpp_TypeInfo_var))->set_U3CU3E9_0(L_0);
		return;
	}
}
// System.Void Google.Protobuf.Reflection.SourceCodeInfo/Types/Location/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m0D7F18F9E242952A4718672889E7A6D4750E8C15 (U3CU3Ec_tE6E47C051562B8F8FCE9347E1B99439CF27AB1AF * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// Google.Protobuf.Reflection.SourceCodeInfo/Types/Location Google.Protobuf.Reflection.SourceCodeInfo/Types/Location/<>c::<.cctor>b__55_0()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4 * U3CU3Ec_U3C_cctorU3Eb__55_0_m4EB0DCECA4202B15C7EC20815CEFDEBD1E0AFB9B (U3CU3Ec_tE6E47C051562B8F8FCE9347E1B99439CF27AB1AF * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4 * L_0 = (Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4 *)il2cpp_codegen_object_new(Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_il2cpp_TypeInfo_var);
		Location__ctor_mFDC279769BEAE3382B8775935C8683621235F622(L_0, /*hidden argument*/NULL);
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart/<>c::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__cctor_mE8A52459A0655D42438ADBC5EF6EE84710B49777 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_t70C29F3AE73CD37C0D77DCC96A139645BE59A453_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CU3Ec_t70C29F3AE73CD37C0D77DCC96A139645BE59A453 * L_0 = (U3CU3Ec_t70C29F3AE73CD37C0D77DCC96A139645BE59A453 *)il2cpp_codegen_object_new(U3CU3Ec_t70C29F3AE73CD37C0D77DCC96A139645BE59A453_il2cpp_TypeInfo_var);
		U3CU3Ec__ctor_m14121C5533461FB8EFB0C4F01A4D5BD0AD91EA7F(L_0, /*hidden argument*/NULL);
		((U3CU3Ec_t70C29F3AE73CD37C0D77DCC96A139645BE59A453_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t70C29F3AE73CD37C0D77DCC96A139645BE59A453_il2cpp_TypeInfo_var))->set_U3CU3E9_0(L_0);
		return;
	}
}
// System.Void Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m14121C5533461FB8EFB0C4F01A4D5BD0AD91EA7F (U3CU3Ec_t70C29F3AE73CD37C0D77DCC96A139645BE59A453 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart/<>c::<.cctor>b__41_0()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22 * U3CU3Ec_U3C_cctorU3Eb__41_0_m613BA3E40E15DB658425AEB73890F58D554969E4 (U3CU3Ec_t70C29F3AE73CD37C0D77DCC96A139645BE59A453 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22 * L_0 = (NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22 *)il2cpp_codegen_object_new(NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22_il2cpp_TypeInfo_var);
		NamePart__ctor_mA9A9103300EE0850829D3BFE67B52EAD4512F085(L_0, /*hidden argument*/NULL);
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject* MessageDescriptor_get_NestedTypes_m34A524E3C7D560C1537685CF6291AB60DA58FE7E_inline (MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = __this->get_U3CNestedTypesU3Ek__BackingField_13();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t ParseContext_ReadInt32_m275087DD36E27495CC2EE76EEB08BC90EFAF94B9_inline (ParseContext_t288EAC5562A1D7E0FF8A06DBA58B8CF732ECE349 * __this, const RuntimeMethod* method)
{
	{
		ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 * L_0 = __this->get_address_of_buffer_0();
		ParserInternalState_t08A261A9F88B2B769A10BED876C9622FEAFD2596 * L_1 = __this->get_address_of_state_1();
		uint32_t L_2;
		L_2 = ParsingPrimitives_ParseRawVarint32_m603BEFB03EAD05D2C5B543DA459AE0E987BB92E7((ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 *)L_0, (ParserInternalState_t08A261A9F88B2B769A10BED876C9622FEAFD2596 *)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint32_t ParseContext_ReadTag_mEE687A18078235454AEA63F1F61E9E3E9EB0260F_inline (ParseContext_t288EAC5562A1D7E0FF8A06DBA58B8CF732ECE349 * __this, const RuntimeMethod* method)
{
	{
		ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 * L_0 = __this->get_address_of_buffer_0();
		ParserInternalState_t08A261A9F88B2B769A10BED876C9622FEAFD2596 * L_1 = __this->get_address_of_state_1();
		uint32_t L_2;
		L_2 = ParsingPrimitives_ParseTag_mF467A1C5FE6962837D3E138E0D2DD3F159EE6A1E((ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 *)L_0, (ParserInternalState_t08A261A9F88B2B769A10BED876C9622FEAFD2596 *)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* ParseContext_ReadString_mA21E24240255C281D36669A209628AD6FDBB15AE_inline (ParseContext_t288EAC5562A1D7E0FF8A06DBA58B8CF732ECE349 * __this, const RuntimeMethod* method)
{
	{
		ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 * L_0 = __this->get_address_of_buffer_0();
		ParserInternalState_t08A261A9F88B2B769A10BED876C9622FEAFD2596 * L_1 = __this->get_address_of_state_1();
		String_t* L_2;
		L_2 = ParsingPrimitives_ReadString_mF4065E9BBE004B97FBA2D159B6B175A1876AD495_inline((ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 *)L_0, (ParserInternalState_t08A261A9F88B2B769A10BED876C9622FEAFD2596 *)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool OriginalNameAttribute_get_PreferredAlias_m8B1D004B47D36373572F40238135A599E23095B8_inline (OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_U3CPreferredAliasU3Ek__BackingField_1();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* OriginalNameAttribute_get_Name_m35E068ED514B51E172CF5FE5DD917801F045B819_inline (OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CNameU3Ek__BackingField_0();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool ParseContext_ReadBool_m097C5083B8554A5F33B1799DD8BF30926A5942BE_inline (ParseContext_t288EAC5562A1D7E0FF8A06DBA58B8CF732ECE349 * __this, const RuntimeMethod* method)
{
	{
		ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 * L_0 = __this->get_address_of_buffer_0();
		ParserInternalState_t08A261A9F88B2B769A10BED876C9622FEAFD2596 * L_1 = __this->get_address_of_state_1();
		uint64_t L_2;
		L_2 = ParsingPrimitives_ParseRawVarint64_m991C711AC6C490742D2A18D54B518A73603B29CF((ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 *)L_0, (ParserInternalState_t08A261A9F88B2B769A10BED876C9622FEAFD2596 *)L_1, /*hidden argument*/NULL);
		return (bool)((!(((uint64_t)L_2) <= ((uint64_t)((int64_t)((int64_t)0)))))? 1 : 0);
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* ParsingPrimitives_ReadString_mF4065E9BBE004B97FBA2D159B6B175A1876AD495_inline (ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 * ___buffer0, ParserInternalState_t08A261A9F88B2B769A10BED876C9622FEAFD2596 * ___state1, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 * L_0 = ___buffer0;
		ParserInternalState_t08A261A9F88B2B769A10BED876C9622FEAFD2596 * L_1 = ___state1;
		int32_t L_2;
		L_2 = ParsingPrimitives_ParseLength_mC2FC8A77E6BCAB09D6149C6EDF2D080E76EC945F_inline((ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 *)L_0, (ParserInternalState_t08A261A9F88B2B769A10BED876C9622FEAFD2596 *)L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 * L_3 = ___buffer0;
		ParserInternalState_t08A261A9F88B2B769A10BED876C9622FEAFD2596 * L_4 = ___state1;
		int32_t L_5 = V_0;
		String_t* L_6;
		L_6 = ParsingPrimitives_ReadRawString_mFFD978AA6F9021997AA4154B07D4A5B47ABB345C((ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 *)L_3, (ParserInternalState_t08A261A9F88B2B769A10BED876C9622FEAFD2596 *)L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t ParsingPrimitives_ParseLength_mC2FC8A77E6BCAB09D6149C6EDF2D080E76EC945F_inline (ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 * ___buffer0, ParserInternalState_t08A261A9F88B2B769A10BED876C9622FEAFD2596 * ___state1, const RuntimeMethod* method)
{
	{
		ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 * L_0 = ___buffer0;
		ParserInternalState_t08A261A9F88B2B769A10BED876C9622FEAFD2596 * L_1 = ___state1;
		uint32_t L_2;
		L_2 = ParsingPrimitives_ParseRawVarint32_m603BEFB03EAD05D2C5B543DA459AE0E987BB92E7((ReadOnlySpan_1_t03DDF1A13DD7F8143C692DB7B68817A086932726 *)L_0, (ParserInternalState_t08A261A9F88B2B769A10BED876C9622FEAFD2596 *)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
