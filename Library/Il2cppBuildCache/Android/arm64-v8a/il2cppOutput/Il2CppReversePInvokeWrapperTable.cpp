﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <stdint.h>
#include <limits>



// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;



IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif

extern "C" int32_t DEFAULT_CALL ReversePInvokeWrapper_AssetBundleManager_GetResourceContents_mDFE89208C34422AEE3F365704F969ADFA843BA84(char* ___path0, intptr_t ___dst1);
extern "C" char* DEFAULT_CALL ReversePInvokeWrapper_AssetBundleManager_PathToResourceAsFile_mE4EA5782FC953FF16031FD0EB659BD758E0EDC7E(char* ___assetPath0);
extern "C" intptr_t DEFAULT_CALL ReversePInvokeWrapper_DemoGraph_PushInputInGlContext_m2000278CAE8C8F4281A45E55684BD7498AEE66E7();
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_ImageFrame_ReleasePixelData_mDF81706134475903E835D1F61193281D8FB96A78(intptr_t ___ptr0);
extern "C" intptr_t DEFAULT_CALL ReversePInvokeWrapper_InstantMotionTrackingGraph_MatrixCallback_mC846E6B8E080F5495EE9AB511DA6C35EFBC65C6D(intptr_t ___packetPtr0);
extern "C" int32_t DEFAULT_CALL ReversePInvokeWrapper_LocalAssetManager_GetResourceContents_m36D0D7964026D770978C9BECA4F18D510A46648A(char* ___path0, intptr_t ___dst1);
extern "C" char* DEFAULT_CALL ReversePInvokeWrapper_LocalAssetManager_PathToResourceAsFile_mD93EA6538E2E77CCD4ACB981201294DFFDF0C3A6(char* ___assetPath0);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_OSSpecificSynchronizationContext_InvocationEntry_m0045E44F7E960D6B4A864D5206D4116249C09BB0(intptr_t ___arg0);
extern "C" intptr_t DEFAULT_CALL ReversePInvokeWrapper_OfficialDemoGraph_BuildDestination_m8A2E396648876205DF379CDB3B2C198AB6A5E70D();
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_OfficialDemoGraph_OnReleaseDestinationTexture_mC9040C05CB2B2BC7C3D8126BCCFD6FF9B24B199D(uint64_t ___name0, intptr_t ___tokenPtr1);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_Protobuf_LogProtobufMessage_m1B59C21023D21E2AB5CA458EC71B9F43DA4E587D(int32_t ___level0, char* ___filename1, int32_t ___line2, char* ___message3);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SceneDirectors_GetCurrentContext_mCCE71B73479FC8A1F8D65066ED794FE54AF5401C(int32_t ___eventId0);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_TextureFramePool_OnTextureFrameRelease_m3F46346F6E43543DD9BE828528E788F0066D9B6D(uint64_t ___textureName0, intptr_t ___syncTokenPtr1);


IL2CPP_EXTERN_C const Il2CppMethodPointer g_ReversePInvokeWrapperPointers[];
const Il2CppMethodPointer g_ReversePInvokeWrapperPointers[13] = 
{
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_AssetBundleManager_GetResourceContents_mDFE89208C34422AEE3F365704F969ADFA843BA84),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_AssetBundleManager_PathToResourceAsFile_mE4EA5782FC953FF16031FD0EB659BD758E0EDC7E),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_DemoGraph_PushInputInGlContext_m2000278CAE8C8F4281A45E55684BD7498AEE66E7),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_ImageFrame_ReleasePixelData_mDF81706134475903E835D1F61193281D8FB96A78),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_InstantMotionTrackingGraph_MatrixCallback_mC846E6B8E080F5495EE9AB511DA6C35EFBC65C6D),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_LocalAssetManager_GetResourceContents_m36D0D7964026D770978C9BECA4F18D510A46648A),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_LocalAssetManager_PathToResourceAsFile_mD93EA6538E2E77CCD4ACB981201294DFFDF0C3A6),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_OSSpecificSynchronizationContext_InvocationEntry_m0045E44F7E960D6B4A864D5206D4116249C09BB0),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_OfficialDemoGraph_BuildDestination_m8A2E396648876205DF379CDB3B2C198AB6A5E70D),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_OfficialDemoGraph_OnReleaseDestinationTexture_mC9040C05CB2B2BC7C3D8126BCCFD6FF9B24B199D),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_Protobuf_LogProtobufMessage_m1B59C21023D21E2AB5CA458EC71B9F43DA4E587D),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_SceneDirectors_GetCurrentContext_mCCE71B73479FC8A1F8D65066ED794FE54AF5401C),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_TextureFramePool_OnTextureFrameRelease_m3F46346F6E43543DD9BE828528E788F0066D9B6D),
};
