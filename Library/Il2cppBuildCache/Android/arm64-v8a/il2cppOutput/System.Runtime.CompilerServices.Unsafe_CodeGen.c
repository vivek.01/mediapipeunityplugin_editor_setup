﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 T System.Runtime.CompilerServices.Unsafe::ReadUnaligned(System.Byte&)
// 0x00000002 System.Void System.Runtime.CompilerServices.Unsafe::WriteUnaligned(System.Byte&,T)
// 0x00000003 System.Int32 System.Runtime.CompilerServices.Unsafe::SizeOf()
// 0x00000004 System.Void System.Runtime.CompilerServices.Unsafe::CopyBlock(System.Byte&,System.Byte&,System.UInt32)
extern void Unsafe_CopyBlock_m620FFE28230187482E6A1F6C2951E86E9397D537 (void);
// 0x00000005 T System.Runtime.CompilerServices.Unsafe::As(System.Object)
// 0x00000006 T& System.Runtime.CompilerServices.Unsafe::AsRef(System.Void*)
// 0x00000007 TTo& System.Runtime.CompilerServices.Unsafe::As(TFrom&)
// 0x00000008 T& System.Runtime.CompilerServices.Unsafe::Add(T&,System.Int32)
// 0x00000009 T& System.Runtime.CompilerServices.Unsafe::Add(T&,System.IntPtr)
// 0x0000000A T& System.Runtime.CompilerServices.Unsafe::AddByteOffset(T&,System.IntPtr)
// 0x0000000B System.IntPtr System.Runtime.CompilerServices.Unsafe::ByteOffset(T&,T&)
// 0x0000000C System.Boolean System.Runtime.CompilerServices.Unsafe::AreSame(T&,T&)
// 0x0000000D System.Void System.Runtime.Versioning.NonVersionableAttribute::.ctor()
extern void NonVersionableAttribute__ctor_m42737892A413DAF4079C33A61205452777804D13 (void);
static Il2CppMethodPointer s_methodPointers[13] = 
{
	NULL,
	NULL,
	NULL,
	Unsafe_CopyBlock_m620FFE28230187482E6A1F6C2951E86E9397D537,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NonVersionableAttribute__ctor_m42737892A413DAF4079C33A61205452777804D13,
};
static const int32_t s_InvokerIndices[13] = 
{
	-1,
	-1,
	-1,
	1931,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	1450,
};
static const Il2CppTokenRangePair s_rgctxIndices[3] = 
{
	{ 0x06000003, { 0, 1 } },
	{ 0x06000008, { 1, 1 } },
	{ 0x06000009, { 2, 1 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[3] = 
{
	{ (Il2CppRGCTXDataType)2, 291 },
	{ (Il2CppRGCTXDataType)2, 296 },
	{ (Il2CppRGCTXDataType)2, 297 },
};
extern const CustomAttributesCacheGenerator g_System_Runtime_CompilerServices_Unsafe_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_System_Runtime_CompilerServices_Unsafe_CodeGenModule;
const Il2CppCodeGenModule g_System_Runtime_CompilerServices_Unsafe_CodeGenModule = 
{
	"System.Runtime.CompilerServices.Unsafe.dll",
	13,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	3,
	s_rgctxIndices,
	3,
	s_rgctxValues,
	NULL,
	g_System_Runtime_CompilerServices_Unsafe_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
