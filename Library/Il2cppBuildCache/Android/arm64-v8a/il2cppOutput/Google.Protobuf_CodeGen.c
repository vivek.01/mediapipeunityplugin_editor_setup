﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void Microsoft.CodeAnalysis.EmbeddedAttribute::.ctor()
extern void EmbeddedAttribute__ctor_mC4A60001FA556CF2C0A731104B5CEC786B0CB863 (void);
// 0x00000002 System.Void System.Runtime.CompilerServices.IsByRefLikeAttribute::.ctor()
extern void IsByRefLikeAttribute__ctor_m80997F85AA38401591B7574259E29881904ADC76 (void);
// 0x00000003 Google.Protobuf.ByteString Google.Protobuf.ByteString::AttachBytes(System.ReadOnlyMemory`1<System.Byte>)
extern void ByteString_AttachBytes_mDC1879404E2232856A143566E43072A86B37E561 (void);
// 0x00000004 Google.Protobuf.ByteString Google.Protobuf.ByteString::AttachBytes(System.Byte[])
extern void ByteString_AttachBytes_m0F32349B0FCFC5471F4A3A9EE7F854AF6B5B5D7B (void);
// 0x00000005 System.Void Google.Protobuf.ByteString::.ctor(System.ReadOnlyMemory`1<System.Byte>)
extern void ByteString__ctor_m187F59F42710CB6F5225669981E249B6FBC5CEA5 (void);
// 0x00000006 Google.Protobuf.ByteString Google.Protobuf.ByteString::get_Empty()
extern void ByteString_get_Empty_m557AC9DB3319BA318064975841E726A1F94581C6 (void);
// 0x00000007 System.Int32 Google.Protobuf.ByteString::get_Length()
extern void ByteString_get_Length_m7E5EF70AFE2C143AC75D11C41E5FFF131FBADB61 (void);
// 0x00000008 System.ReadOnlySpan`1<System.Byte> Google.Protobuf.ByteString::get_Span()
extern void ByteString_get_Span_m77B79420B0BBA1D1A0B36F0C3DE0E3B2EBDE32E4 (void);
// 0x00000009 System.String Google.Protobuf.ByteString::ToBase64()
extern void ByteString_ToBase64_mFF01EB56ED7E23C4BF01A8A0B008ABD1DC08D8CF (void);
// 0x0000000A Google.Protobuf.ByteString Google.Protobuf.ByteString::CopyFrom(System.Byte[])
extern void ByteString_CopyFrom_m5B311324FC5F998F3DB5B3E2B6AFAB16C497B8B0 (void);
// 0x0000000B System.Collections.Generic.IEnumerator`1<System.Byte> Google.Protobuf.ByteString::GetEnumerator()
extern void ByteString_GetEnumerator_m784D34E951A7A3FD048BA6ECD6E5AECF4E3C5CCF (void);
// 0x0000000C System.Collections.IEnumerator Google.Protobuf.ByteString::System.Collections.IEnumerable.GetEnumerator()
extern void ByteString_System_Collections_IEnumerable_GetEnumerator_m8F5EEDBC5634A38AF16663BD12846A7289D493E2 (void);
// 0x0000000D Google.Protobuf.CodedInputStream Google.Protobuf.ByteString::CreateCodedInput()
extern void ByteString_CreateCodedInput_m4254D8478FE34A7340DF4C6B6CAFD07E9BA4EBF5 (void);
// 0x0000000E System.Boolean Google.Protobuf.ByteString::op_Equality(Google.Protobuf.ByteString,Google.Protobuf.ByteString)
extern void ByteString_op_Equality_mC5A462A8865F4AFE094BE7A44612D48B087CF28B (void);
// 0x0000000F System.Boolean Google.Protobuf.ByteString::op_Inequality(Google.Protobuf.ByteString,Google.Protobuf.ByteString)
extern void ByteString_op_Inequality_mB58011343AD429C8AD0E05AFF0A651ADA18C9D8B (void);
// 0x00000010 System.Boolean Google.Protobuf.ByteString::Equals(System.Object)
extern void ByteString_Equals_m80505FF3A839E2DED23DE93456EA2ACFA3C4E361 (void);
// 0x00000011 System.Int32 Google.Protobuf.ByteString::GetHashCode()
extern void ByteString_GetHashCode_m1BB2CD9974C995B8AF0145DBC273C8E21167A289 (void);
// 0x00000012 System.Boolean Google.Protobuf.ByteString::Equals(Google.Protobuf.ByteString)
extern void ByteString_Equals_m9768F560492F61537B71FE0E5A85747948BD2E80 (void);
// 0x00000013 System.Void Google.Protobuf.ByteString::.cctor()
extern void ByteString__cctor_m7A33433DDDE5A01F2BE5CFBE1DEA39453F6BC62F (void);
// 0x00000014 System.Void Google.Protobuf.CodedInputStream::.ctor(System.Byte[])
extern void CodedInputStream__ctor_mE8E27A2076287B2B96592DFED394B71F859B63C7 (void);
// 0x00000015 System.Void Google.Protobuf.CodedInputStream::.ctor(System.IO.Stream,System.Byte[],System.Int32,System.Int32,System.Boolean)
extern void CodedInputStream__ctor_m96A6E9289D1BC42DBFDBC26C24E919AAC6971A1E (void);
// 0x00000016 System.Void Google.Protobuf.CodedInputStream::set_DiscardUnknownFields(System.Boolean)
extern void CodedInputStream_set_DiscardUnknownFields_m075C8E7040091C7606C4D77D7A17FF43EC47DE90 (void);
// 0x00000017 System.Void Google.Protobuf.CodedInputStream::set_ExtensionRegistry(Google.Protobuf.ExtensionRegistry)
extern void CodedInputStream_set_ExtensionRegistry_m7A74E3AFA8B0D9890FB2898FBA5520A5F8D88388 (void);
// 0x00000018 System.Byte[] Google.Protobuf.CodedInputStream::get_InternalBuffer()
extern void CodedInputStream_get_InternalBuffer_m8ED6DAC00B5378FD8AC7236ECED00D47B23A91B9 (void);
// 0x00000019 System.IO.Stream Google.Protobuf.CodedInputStream::get_InternalInputStream()
extern void CodedInputStream_get_InternalInputStream_m7C01D4DA80B4582DC88EC94E0E7B8F44A59FA224 (void);
// 0x0000001A Google.Protobuf.ParserInternalState& Google.Protobuf.CodedInputStream::get_InternalState()
extern void CodedInputStream_get_InternalState_m7BE1F1D3170D69A9B71FF50503E3D4F03CBD1D5A (void);
// 0x0000001B System.Void Google.Protobuf.CodedInputStream::Dispose()
extern void CodedInputStream_Dispose_m836C63C970DCD3F257DAA2E57C459D9C84E49120 (void);
// 0x0000001C System.Void Google.Protobuf.CodedInputStream::CheckReadEndOfStreamTag()
extern void CodedInputStream_CheckReadEndOfStreamTag_m3B7164CE80D09CFD73D100A4704EEAB7408E1D1D (void);
// 0x0000001D System.UInt32 Google.Protobuf.CodedInputStream::ReadTag()
extern void CodedInputStream_ReadTag_mB343941331F8E86DA3306D4D199E9CD0D82368B5 (void);
// 0x0000001E System.Single Google.Protobuf.CodedInputStream::ReadFloat()
extern void CodedInputStream_ReadFloat_mB03C9E0BB233097A2F735B708F9559360679FFB0 (void);
// 0x0000001F System.Int64 Google.Protobuf.CodedInputStream::ReadInt64()
extern void CodedInputStream_ReadInt64_m18E7F87499399FC30B303F0BB249CE79B8AF7845 (void);
// 0x00000020 System.Int32 Google.Protobuf.CodedInputStream::ReadInt32()
extern void CodedInputStream_ReadInt32_mC0FA7EDD8272884A635BB0BFAC8DFDB541D3FD6F (void);
// 0x00000021 System.Boolean Google.Protobuf.CodedInputStream::ReadBool()
extern void CodedInputStream_ReadBool_m621B922FD4F0BDF6FA6E3ACE90CCC284AAA1368A (void);
// 0x00000022 System.String Google.Protobuf.CodedInputStream::ReadString()
extern void CodedInputStream_ReadString_mDDD43FF62E2E89AC3E3679E7AC374765AB3E7827 (void);
// 0x00000023 System.Void Google.Protobuf.CodedInputStream::ReadMessage(Google.Protobuf.IMessage)
extern void CodedInputStream_ReadMessage_mA7ADE8031D91AEE188B9DB76768340C9FFEF1A46 (void);
// 0x00000024 System.Int32 Google.Protobuf.CodedInputStream::ReadEnum()
extern void CodedInputStream_ReadEnum_m7ABEC74A9C69CE648B9662E9B3464F0233C4387E (void);
// 0x00000025 System.UInt32 Google.Protobuf.CodedInputStream::ReadRawVarint32()
extern void CodedInputStream_ReadRawVarint32_m1E6B9987477C63A248CC7A50014BF6A8D0CB704D (void);
// 0x00000026 System.UInt64 Google.Protobuf.CodedInputStream::ReadRawVarint64()
extern void CodedInputStream_ReadRawVarint64_m5ED379B1F436F32BC308B6BAD560F9EE8D15B799 (void);
// 0x00000027 System.Void Google.Protobuf.CodedInputStream::ReadRawMessage(Google.Protobuf.IMessage)
extern void CodedInputStream_ReadRawMessage_m77E217968CD5654CC717E7DA57906C0219372943 (void);
// 0x00000028 System.Int32 Google.Protobuf.CodedOutputStream::ComputeUInt64Size(System.UInt64)
extern void CodedOutputStream_ComputeUInt64Size_mA7248484F80AE2EF1834D551EA35ADF1FC0A3180 (void);
// 0x00000029 System.Int32 Google.Protobuf.CodedOutputStream::ComputeInt64Size(System.Int64)
extern void CodedOutputStream_ComputeInt64Size_m5DEFA6C0EA352C5386383B9149DA6A02FDFE084D (void);
// 0x0000002A System.Int32 Google.Protobuf.CodedOutputStream::ComputeInt32Size(System.Int32)
extern void CodedOutputStream_ComputeInt32Size_m03C7A226C41F660D0A853D576117A6386B9D0802 (void);
// 0x0000002B System.Int32 Google.Protobuf.CodedOutputStream::ComputeFixed64Size(System.UInt64)
extern void CodedOutputStream_ComputeFixed64Size_m85712D2C1965813F1B33E40C4CD5B2F7246A06A1 (void);
// 0x0000002C System.Int32 Google.Protobuf.CodedOutputStream::ComputeFixed32Size(System.UInt32)
extern void CodedOutputStream_ComputeFixed32Size_m741ADFA703A6FC8573143E69FDE1E300BF7E67F5 (void);
// 0x0000002D System.Int32 Google.Protobuf.CodedOutputStream::ComputeStringSize(System.String)
extern void CodedOutputStream_ComputeStringSize_m3913248CC93D6067C32930171244FD185D818BDF (void);
// 0x0000002E System.Int32 Google.Protobuf.CodedOutputStream::ComputeMessageSize(Google.Protobuf.IMessage)
extern void CodedOutputStream_ComputeMessageSize_m9E53FB7A0F5C881075F630F771D962E2705525C3 (void);
// 0x0000002F System.Int32 Google.Protobuf.CodedOutputStream::ComputeBytesSize(Google.Protobuf.ByteString)
extern void CodedOutputStream_ComputeBytesSize_m353054D6ADADF9FF9FC4820BFEAAA56F65874FC6 (void);
// 0x00000030 System.Int32 Google.Protobuf.CodedOutputStream::ComputeUInt32Size(System.UInt32)
extern void CodedOutputStream_ComputeUInt32Size_m3C6EEC21F0AB57DB286D6F6FCA075D24A458476F (void);
// 0x00000031 System.Int32 Google.Protobuf.CodedOutputStream::ComputeEnumSize(System.Int32)
extern void CodedOutputStream_ComputeEnumSize_m6E6CA2CF9523B109061C60689E5B1B3AF60725C9 (void);
// 0x00000032 System.Int32 Google.Protobuf.CodedOutputStream::ComputeLengthSize(System.Int32)
extern void CodedOutputStream_ComputeLengthSize_m65C9D63AD1DD32EDBB3DBDF548D042702DA8C42D (void);
// 0x00000033 System.Int32 Google.Protobuf.CodedOutputStream::ComputeRawVarint32Size(System.UInt32)
extern void CodedOutputStream_ComputeRawVarint32Size_mFFED3477FE86FFDAAA384AE87B042DBB93C14796 (void);
// 0x00000034 System.Int32 Google.Protobuf.CodedOutputStream::ComputeRawVarint64Size(System.UInt64)
extern void CodedOutputStream_ComputeRawVarint64Size_mCEDC6338D482AA1533B45CC87F3418EF5E0B9113 (void);
// 0x00000035 System.Int32 Google.Protobuf.CodedOutputStream::ComputeTagSize(System.Int32)
extern void CodedOutputStream_ComputeTagSize_m3F3A5FB77424F92EAFF1A8C5C301BFA25CB98B20 (void);
// 0x00000036 System.Void Google.Protobuf.CodedOutputStream::.ctor(System.Byte[])
extern void CodedOutputStream__ctor_m9C304A1E8EF1207108AE808E3070AC471DD2993B (void);
// 0x00000037 System.Void Google.Protobuf.CodedOutputStream::.ctor(System.Byte[],System.Int32,System.Int32)
extern void CodedOutputStream__ctor_m04CE92D796E0E93EFB0BB245E7259104522A7DA8 (void);
// 0x00000038 System.Void Google.Protobuf.CodedOutputStream::WriteFloat(System.Single)
extern void CodedOutputStream_WriteFloat_m4836E03ACBC9A8BC134531C338C710CFDBF508DB (void);
// 0x00000039 System.Void Google.Protobuf.CodedOutputStream::WriteInt64(System.Int64)
extern void CodedOutputStream_WriteInt64_m2A02EC95E6A8674FC4BB4DC0916FE1D0B503E4A2 (void);
// 0x0000003A System.Void Google.Protobuf.CodedOutputStream::WriteInt32(System.Int32)
extern void CodedOutputStream_WriteInt32_mC6C462880D8E3DAA7A90BA4014EC98F8C5B6707F (void);
// 0x0000003B System.Void Google.Protobuf.CodedOutputStream::WriteBool(System.Boolean)
extern void CodedOutputStream_WriteBool_m857D13C9DD497AEE09EF2DD772989698E0D82882 (void);
// 0x0000003C System.Void Google.Protobuf.CodedOutputStream::WriteString(System.String)
extern void CodedOutputStream_WriteString_mD8116449C33FD9927EA22173717C811A3B72719E (void);
// 0x0000003D System.Void Google.Protobuf.CodedOutputStream::WriteMessage(Google.Protobuf.IMessage)
extern void CodedOutputStream_WriteMessage_mBD62FB39D6A6D0D51B2A1A05577F8F21D8E02F8D (void);
// 0x0000003E System.Void Google.Protobuf.CodedOutputStream::WriteRawMessage(Google.Protobuf.IMessage)
extern void CodedOutputStream_WriteRawMessage_m05CED72DF9A800E9BFB28956F21E7B4416E03D11 (void);
// 0x0000003F System.Void Google.Protobuf.CodedOutputStream::WriteEnum(System.Int32)
extern void CodedOutputStream_WriteEnum_m7D1360B9AC9ECF6E336179E786671AA9408C2FBD (void);
// 0x00000040 System.Void Google.Protobuf.CodedOutputStream::WriteRawTag(System.Byte)
extern void CodedOutputStream_WriteRawTag_m709724C278839993424703351DF6FB7BCD2C1675 (void);
// 0x00000041 System.Void Google.Protobuf.CodedOutputStream::WriteRawTag(System.Byte,System.Byte)
extern void CodedOutputStream_WriteRawTag_m4628DE31677CF587C0D6160CF295CC71EFF6AC12 (void);
// 0x00000042 System.Void Google.Protobuf.CodedOutputStream::Dispose()
extern void CodedOutputStream_Dispose_m03152AD8DEC550A0DB76D290F0E624BE73D96806 (void);
// 0x00000043 System.Void Google.Protobuf.CodedOutputStream::Flush()
extern void CodedOutputStream_Flush_m02373D0BABF1E63618932A10659FFD1571E41D4E (void);
// 0x00000044 System.Void Google.Protobuf.CodedOutputStream::CheckNoSpaceLeft()
extern void CodedOutputStream_CheckNoSpaceLeft_mEF97F3A84CA0D669EBECE7128818FD19CA04D7AD (void);
// 0x00000045 System.Byte[] Google.Protobuf.CodedOutputStream::get_InternalBuffer()
extern void CodedOutputStream_get_InternalBuffer_m945C519723571718B518C56653A9A7DA5F7BBECA (void);
// 0x00000046 System.IO.Stream Google.Protobuf.CodedOutputStream::get_InternalOutputStream()
extern void CodedOutputStream_get_InternalOutputStream_m85CB0EA8CA59EB077DE0E4CA8DB550CA4D1F88E3 (void);
// 0x00000047 Google.Protobuf.WriterInternalState& Google.Protobuf.CodedOutputStream::get_InternalState()
extern void CodedOutputStream_get_InternalState_m08406AFD75E90BC4C1F5927C63D3F29BF0B128F9 (void);
// 0x00000048 System.Void Google.Protobuf.CodedOutputStream::.cctor()
extern void CodedOutputStream__cctor_m5D1B2D651124EA8ED740645D67E8005208ED1684 (void);
// 0x00000049 System.Void Google.Protobuf.CodedOutputStream/OutOfSpaceException::.ctor()
extern void OutOfSpaceException__ctor_m971089A50DDCF3E93F0BC1E6BB03B36B530DF3C9 (void);
// 0x0000004A System.Type Google.Protobuf.Extension::get_TargetType()
// 0x0000004B Google.Protobuf.IExtensionValue Google.Protobuf.Extension::CreateValue()
// 0x0000004C System.Int32 Google.Protobuf.Extension::get_FieldNumber()
extern void Extension_get_FieldNumber_mA033A36AD6AC429AFD166C84A6BBB13ECD45B579 (void);
// 0x0000004D System.Boolean Google.Protobuf.Extension::get_IsRepeated()
// 0x0000004E TValue Google.Protobuf.Extension`2::get_DefaultValue()
// 0x0000004F System.Type Google.Protobuf.Extension`2::get_TargetType()
// 0x00000050 System.Boolean Google.Protobuf.Extension`2::get_IsRepeated()
// 0x00000051 Google.Protobuf.IExtensionValue Google.Protobuf.Extension`2::CreateValue()
// 0x00000052 System.Type Google.Protobuf.RepeatedExtension`2::get_TargetType()
// 0x00000053 System.Boolean Google.Protobuf.RepeatedExtension`2::get_IsRepeated()
// 0x00000054 Google.Protobuf.IExtensionValue Google.Protobuf.RepeatedExtension`2::CreateValue()
// 0x00000055 System.Void Google.Protobuf.ExtensionRegistry::.ctor()
extern void ExtensionRegistry__ctor_m467F6F03537721D3B00C7209E8D4A52B6B233A1C (void);
// 0x00000056 System.Void Google.Protobuf.ExtensionRegistry::.ctor(System.Collections.Generic.IDictionary`2<Google.Protobuf.ObjectIntPair`1<System.Type>,Google.Protobuf.Extension>)
extern void ExtensionRegistry__ctor_m34AA493FD4CFC60446427D7548DE508739B6C78B (void);
// 0x00000057 System.Int32 Google.Protobuf.ExtensionRegistry::get_Count()
extern void ExtensionRegistry_get_Count_m0FF84D632A1DD28FC6507D5679ECEC97D0548B92 (void);
// 0x00000058 System.Boolean Google.Protobuf.ExtensionRegistry::System.Collections.Generic.ICollection<Google.Protobuf.Extension>.get_IsReadOnly()
extern void ExtensionRegistry_System_Collections_Generic_ICollectionU3CGoogle_Protobuf_ExtensionU3E_get_IsReadOnly_mF8752C2176E029297ED18CC8CA9E9808E1A53AA5 (void);
// 0x00000059 System.Boolean Google.Protobuf.ExtensionRegistry::ContainsInputField(System.UInt32,System.Type,Google.Protobuf.Extension&)
extern void ExtensionRegistry_ContainsInputField_m4163B444D36A832DAD30F332A010356FAB62F899 (void);
// 0x0000005A System.Void Google.Protobuf.ExtensionRegistry::Add(Google.Protobuf.Extension)
extern void ExtensionRegistry_Add_m76E4A7F7DD2B9D7BBC3DA363DDB875E32AD7482F (void);
// 0x0000005B System.Void Google.Protobuf.ExtensionRegistry::AddRange(System.Collections.Generic.IEnumerable`1<Google.Protobuf.Extension>)
extern void ExtensionRegistry_AddRange_m5D36918B79A6B118519025CBBCF0ABD4B16F4D1D (void);
// 0x0000005C System.Void Google.Protobuf.ExtensionRegistry::Clear()
extern void ExtensionRegistry_Clear_m6C0223E3D9CF2B5BAFE197CD102FAF9915B65A0D (void);
// 0x0000005D System.Boolean Google.Protobuf.ExtensionRegistry::Contains(Google.Protobuf.Extension)
extern void ExtensionRegistry_Contains_mC285CBA8174D1D34E92381633141CC9235AD25F7 (void);
// 0x0000005E System.Void Google.Protobuf.ExtensionRegistry::System.Collections.Generic.ICollection<Google.Protobuf.Extension>.CopyTo(Google.Protobuf.Extension[],System.Int32)
extern void ExtensionRegistry_System_Collections_Generic_ICollectionU3CGoogle_Protobuf_ExtensionU3E_CopyTo_mFD6C19AD6A21DA6E4D506C77BBEF29655EA53DF8 (void);
// 0x0000005F System.Collections.Generic.IEnumerator`1<Google.Protobuf.Extension> Google.Protobuf.ExtensionRegistry::GetEnumerator()
extern void ExtensionRegistry_GetEnumerator_m8B43AE99907CFA07AA4F0FCFCB2F3D78A113BEA2 (void);
// 0x00000060 System.Boolean Google.Protobuf.ExtensionRegistry::Remove(Google.Protobuf.Extension)
extern void ExtensionRegistry_Remove_m769995CA8656F036CC43EF9E8F3031E27B1F8A34 (void);
// 0x00000061 System.Collections.IEnumerator Google.Protobuf.ExtensionRegistry::System.Collections.IEnumerable.GetEnumerator()
extern void ExtensionRegistry_System_Collections_IEnumerable_GetEnumerator_mD72DFA3D924882B12250828545AD7F9402357652 (void);
// 0x00000062 Google.Protobuf.ExtensionRegistry Google.Protobuf.ExtensionRegistry::Clone()
extern void ExtensionRegistry_Clone_mC9301792F1B16502D1A6AD13132D087A2A88D205 (void);
// 0x00000063 System.Boolean Google.Protobuf.ExtensionRegistry/ExtensionComparer::Equals(Google.Protobuf.Extension,Google.Protobuf.Extension)
extern void ExtensionComparer_Equals_mB396D42A10CD9EC4FD27A3EAA47C0BAF1B087F7F (void);
// 0x00000064 System.Int32 Google.Protobuf.ExtensionRegistry/ExtensionComparer::GetHashCode(Google.Protobuf.Extension)
extern void ExtensionComparer_GetHashCode_mCF8B9F41F0D06830A63F2CD551B1017653287C1A (void);
// 0x00000065 System.Void Google.Protobuf.ExtensionRegistry/ExtensionComparer::.ctor()
extern void ExtensionComparer__ctor_mB59BF9FCC245E9E6F78EEE4FE2BB61E0B2F6FDD1 (void);
// 0x00000066 System.Void Google.Protobuf.ExtensionRegistry/ExtensionComparer::.cctor()
extern void ExtensionComparer__cctor_mF316743F780039C34F959DDE162950A4A42116B2 (void);
// 0x00000067 System.Void Google.Protobuf.ExtensionRegistry/<>c::.cctor()
extern void U3CU3Ec__cctor_m1570AE1383064B8D35317069D4DBC927972CF392 (void);
// 0x00000068 System.Void Google.Protobuf.ExtensionRegistry/<>c::.ctor()
extern void U3CU3Ec__ctor_m848476C8C7E8D018C352DFA811B97D7C61C590D7 (void);
// 0x00000069 Google.Protobuf.ObjectIntPair`1<System.Type> Google.Protobuf.ExtensionRegistry/<>c::<.ctor>b__3_0(System.Collections.Generic.KeyValuePair`2<Google.Protobuf.ObjectIntPair`1<System.Type>,Google.Protobuf.Extension>)
extern void U3CU3Ec_U3C_ctorU3Eb__3_0_m1C441539DEBDF1ADEC32810641BEF979D9EA693F (void);
// 0x0000006A Google.Protobuf.Extension Google.Protobuf.ExtensionRegistry/<>c::<.ctor>b__3_1(System.Collections.Generic.KeyValuePair`2<Google.Protobuf.ObjectIntPair`1<System.Type>,Google.Protobuf.Extension>)
extern void U3CU3Ec_U3C_ctorU3Eb__3_1_mFDFE2F28B907535522F7D50FA7F7E4DA101268D5 (void);
// 0x0000006B System.Boolean Google.Protobuf.ExtensionSet::TryGetValue(Google.Protobuf.ExtensionSet`1<TTarget>&,Google.Protobuf.Extension,Google.Protobuf.IExtensionValue&)
// 0x0000006C TValue Google.Protobuf.ExtensionSet::Get(Google.Protobuf.ExtensionSet`1<TTarget>&,Google.Protobuf.Extension`2<TTarget,TValue>)
// 0x0000006D Google.Protobuf.Collections.RepeatedField`1<TValue> Google.Protobuf.ExtensionSet::Get(Google.Protobuf.ExtensionSet`1<TTarget>&,Google.Protobuf.RepeatedExtension`2<TTarget,TValue>)
// 0x0000006E Google.Protobuf.Collections.RepeatedField`1<TValue> Google.Protobuf.ExtensionSet::GetOrInitialize(Google.Protobuf.ExtensionSet`1<TTarget>&,Google.Protobuf.RepeatedExtension`2<TTarget,TValue>)
// 0x0000006F System.Void Google.Protobuf.ExtensionSet::Set(Google.Protobuf.ExtensionSet`1<TTarget>&,Google.Protobuf.Extension`2<TTarget,TValue>,TValue)
// 0x00000070 System.Boolean Google.Protobuf.ExtensionSet::Has(Google.Protobuf.ExtensionSet`1<TTarget>&,Google.Protobuf.Extension`2<TTarget,TValue>)
// 0x00000071 System.Void Google.Protobuf.ExtensionSet::Clear(Google.Protobuf.ExtensionSet`1<TTarget>&,Google.Protobuf.Extension`2<TTarget,TValue>)
// 0x00000072 System.Void Google.Protobuf.ExtensionSet::Clear(Google.Protobuf.ExtensionSet`1<TTarget>&,Google.Protobuf.RepeatedExtension`2<TTarget,TValue>)
// 0x00000073 System.Boolean Google.Protobuf.ExtensionSet::TryMergeFieldFrom(Google.Protobuf.ExtensionSet`1<TTarget>&,Google.Protobuf.CodedInputStream)
// 0x00000074 System.Boolean Google.Protobuf.ExtensionSet::TryMergeFieldFrom(Google.Protobuf.ExtensionSet`1<TTarget>&,Google.Protobuf.ParseContext&)
// 0x00000075 System.Void Google.Protobuf.ExtensionSet::MergeFrom(Google.Protobuf.ExtensionSet`1<TTarget>&,Google.Protobuf.ExtensionSet`1<TTarget>)
// 0x00000076 Google.Protobuf.ExtensionSet`1<TTarget> Google.Protobuf.ExtensionSet::Clone(Google.Protobuf.ExtensionSet`1<TTarget>)
// 0x00000077 System.Collections.Generic.Dictionary`2<System.Int32,Google.Protobuf.IExtensionValue> Google.Protobuf.ExtensionSet`1::get_ValuesByNumber()
// 0x00000078 System.Int32 Google.Protobuf.ExtensionSet`1::GetHashCode()
// 0x00000079 System.Boolean Google.Protobuf.ExtensionSet`1::Equals(System.Object)
// 0x0000007A System.Int32 Google.Protobuf.ExtensionSet`1::CalculateSize()
// 0x0000007B System.Void Google.Protobuf.ExtensionSet`1::WriteTo(Google.Protobuf.CodedOutputStream)
// 0x0000007C System.Void Google.Protobuf.ExtensionSet`1::WriteTo(Google.Protobuf.WriteContext&)
// 0x0000007D System.Void Google.Protobuf.ExtensionSet`1::.ctor()
// 0x0000007E System.Void Google.Protobuf.IExtensionValue::MergeFrom(Google.Protobuf.ParseContext&)
// 0x0000007F System.Void Google.Protobuf.IExtensionValue::MergeFrom(Google.Protobuf.IExtensionValue)
// 0x00000080 System.Void Google.Protobuf.IExtensionValue::WriteTo(Google.Protobuf.WriteContext&)
// 0x00000081 System.Int32 Google.Protobuf.IExtensionValue::CalculateSize()
// 0x00000082 System.Void Google.Protobuf.ExtensionValue`1::.ctor(Google.Protobuf.FieldCodec`1<T>)
// 0x00000083 System.Int32 Google.Protobuf.ExtensionValue`1::CalculateSize()
// 0x00000084 Google.Protobuf.IExtensionValue Google.Protobuf.ExtensionValue`1::Clone()
// 0x00000085 System.Boolean Google.Protobuf.ExtensionValue`1::Equals(Google.Protobuf.IExtensionValue)
// 0x00000086 System.Int32 Google.Protobuf.ExtensionValue`1::GetHashCode()
// 0x00000087 System.Void Google.Protobuf.ExtensionValue`1::MergeFrom(Google.Protobuf.ParseContext&)
// 0x00000088 System.Void Google.Protobuf.ExtensionValue`1::MergeFrom(Google.Protobuf.IExtensionValue)
// 0x00000089 System.Void Google.Protobuf.ExtensionValue`1::WriteTo(Google.Protobuf.WriteContext&)
// 0x0000008A T Google.Protobuf.ExtensionValue`1::GetValue()
// 0x0000008B System.Void Google.Protobuf.ExtensionValue`1::SetValue(T)
// 0x0000008C System.Void Google.Protobuf.RepeatedExtensionValue`1::.ctor(Google.Protobuf.FieldCodec`1<T>)
// 0x0000008D System.Int32 Google.Protobuf.RepeatedExtensionValue`1::CalculateSize()
// 0x0000008E Google.Protobuf.IExtensionValue Google.Protobuf.RepeatedExtensionValue`1::Clone()
// 0x0000008F System.Boolean Google.Protobuf.RepeatedExtensionValue`1::Equals(Google.Protobuf.IExtensionValue)
// 0x00000090 System.Int32 Google.Protobuf.RepeatedExtensionValue`1::GetHashCode()
// 0x00000091 System.Void Google.Protobuf.RepeatedExtensionValue`1::MergeFrom(Google.Protobuf.ParseContext&)
// 0x00000092 System.Void Google.Protobuf.RepeatedExtensionValue`1::MergeFrom(Google.Protobuf.IExtensionValue)
// 0x00000093 System.Void Google.Protobuf.RepeatedExtensionValue`1::WriteTo(Google.Protobuf.WriteContext&)
// 0x00000094 Google.Protobuf.Collections.RepeatedField`1<T> Google.Protobuf.RepeatedExtensionValue`1::GetValue()
// 0x00000095 Google.Protobuf.FieldCodec`1<System.String> Google.Protobuf.FieldCodec::ForString(System.UInt32)
extern void FieldCodec_ForString_m686A669CFD9CF6A06AE0329BB75BFF8D20C17A32 (void);
// 0x00000096 Google.Protobuf.FieldCodec`1<System.Int32> Google.Protobuf.FieldCodec::ForInt32(System.UInt32)
extern void FieldCodec_ForInt32_mA8FC008DA1A37C17DED262F93274DACAEAE60621 (void);
// 0x00000097 Google.Protobuf.FieldCodec`1<System.UInt32> Google.Protobuf.FieldCodec::ForUInt32(System.UInt32)
extern void FieldCodec_ForUInt32_mAE58216C2BB7BC5C71AB5323545035F56386DA3E (void);
// 0x00000098 Google.Protobuf.FieldCodec`1<System.Single> Google.Protobuf.FieldCodec::ForFloat(System.UInt32)
extern void FieldCodec_ForFloat_m137F48ED992040FF1BF81F4B59637342DD724B80 (void);
// 0x00000099 Google.Protobuf.FieldCodec`1<System.String> Google.Protobuf.FieldCodec::ForString(System.UInt32,System.String)
extern void FieldCodec_ForString_m8114E29C7FD5C4B23D6992A8B3276E34F9CC7571 (void);
// 0x0000009A Google.Protobuf.FieldCodec`1<System.Int32> Google.Protobuf.FieldCodec::ForInt32(System.UInt32,System.Int32)
extern void FieldCodec_ForInt32_m7155623F3AAC2B9859A93DE278A89F5C46C9CD27 (void);
// 0x0000009B Google.Protobuf.FieldCodec`1<System.UInt32> Google.Protobuf.FieldCodec::ForUInt32(System.UInt32,System.UInt32)
extern void FieldCodec_ForUInt32_m3DA357EA2E5517EA0C33145B948CFB33E1E0CB75 (void);
// 0x0000009C Google.Protobuf.FieldCodec`1<System.Single> Google.Protobuf.FieldCodec::ForFloat(System.UInt32,System.Single)
extern void FieldCodec_ForFloat_m7AB9D6D9E9EE313CB9398C4E553FC40B21E7B735 (void);
// 0x0000009D Google.Protobuf.FieldCodec`1<T> Google.Protobuf.FieldCodec::ForMessage(System.UInt32,Google.Protobuf.MessageParser`1<T>)
// 0x0000009E System.Void Google.Protobuf.FieldCodec/<>c::.cctor()
extern void U3CU3Ec__cctor_mC397E02616131EA53B2DAFC24E2835780A93E78B (void);
// 0x0000009F System.Void Google.Protobuf.FieldCodec/<>c::.ctor()
extern void U3CU3Ec__ctor_m19E1B9F158751668E1950E90EAD79637FEF05B41 (void);
// 0x000000A0 System.String Google.Protobuf.FieldCodec/<>c::<ForString>b__16_0(Google.Protobuf.ParseContext&)
extern void U3CU3Ec_U3CForStringU3Eb__16_0_m6FF30F39F8EF8E9894C4AB585144D034C41CC62F (void);
// 0x000000A1 System.Void Google.Protobuf.FieldCodec/<>c::<ForString>b__16_1(Google.Protobuf.WriteContext&,System.String)
extern void U3CU3Ec_U3CForStringU3Eb__16_1_m4C0D631118C6024CB9BC3BDC6B5157805C5C858D (void);
// 0x000000A2 System.Int32 Google.Protobuf.FieldCodec/<>c::<ForInt32>b__19_0(Google.Protobuf.ParseContext&)
extern void U3CU3Ec_U3CForInt32U3Eb__19_0_mB0615A186AA6F9BE7FC9B94329140448BDF9DDD5 (void);
// 0x000000A3 System.Void Google.Protobuf.FieldCodec/<>c::<ForInt32>b__19_1(Google.Protobuf.WriteContext&,System.Int32)
extern void U3CU3Ec_U3CForInt32U3Eb__19_1_m73BD232D28C8D24526A506D37AC43525AAA546A8 (void);
// 0x000000A4 System.UInt32 Google.Protobuf.FieldCodec/<>c::<ForUInt32>b__23_0(Google.Protobuf.ParseContext&)
extern void U3CU3Ec_U3CForUInt32U3Eb__23_0_m7493AAC26759C3EEA6814118D3A02B2A700DF57D (void);
// 0x000000A5 System.Void Google.Protobuf.FieldCodec/<>c::<ForUInt32>b__23_1(Google.Protobuf.WriteContext&,System.UInt32)
extern void U3CU3Ec_U3CForUInt32U3Eb__23_1_m10E0019D299AA0C40CE00B4311337F497B36EEBE (void);
// 0x000000A6 System.Single Google.Protobuf.FieldCodec/<>c::<ForFloat>b__29_0(Google.Protobuf.ParseContext&)
extern void U3CU3Ec_U3CForFloatU3Eb__29_0_m5E7B1CF62FEC7B6468FDBFE0C06FB8B9146A30BE (void);
// 0x000000A7 System.Void Google.Protobuf.FieldCodec/<>c::<ForFloat>b__29_1(Google.Protobuf.WriteContext&,System.Single)
extern void U3CU3Ec_U3CForFloatU3Eb__29_1_m6E83611C70058A29A0F4D0434049CF8DEE3E9890 (void);
// 0x000000A8 System.Void Google.Protobuf.FieldCodec/<>c__DisplayClass32_0`1::.ctor()
// 0x000000A9 T Google.Protobuf.FieldCodec/<>c__DisplayClass32_0`1::<ForMessage>b__0(Google.Protobuf.ParseContext&)
// 0x000000AA System.Void Google.Protobuf.FieldCodec/<>c__DisplayClass32_0`1::<ForMessage>b__2(Google.Protobuf.ParseContext&,T&)
// 0x000000AB System.Void Google.Protobuf.FieldCodec/<>c__32`1::.cctor()
// 0x000000AC System.Void Google.Protobuf.FieldCodec/<>c__32`1::.ctor()
// 0x000000AD System.Void Google.Protobuf.FieldCodec/<>c__32`1::<ForMessage>b__32_1(Google.Protobuf.WriteContext&,T)
// 0x000000AE System.Boolean Google.Protobuf.FieldCodec/<>c__32`1::<ForMessage>b__32_3(T&,T)
// 0x000000AF System.Int32 Google.Protobuf.FieldCodec/<>c__32`1::<ForMessage>b__32_4(T)
// 0x000000B0 System.Void Google.Protobuf.ValueReader`1::.ctor(System.Object,System.IntPtr)
// 0x000000B1 TValue Google.Protobuf.ValueReader`1::Invoke(Google.Protobuf.ParseContext&)
// 0x000000B2 System.IAsyncResult Google.Protobuf.ValueReader`1::BeginInvoke(Google.Protobuf.ParseContext&,System.AsyncCallback,System.Object)
// 0x000000B3 TValue Google.Protobuf.ValueReader`1::EndInvoke(Google.Protobuf.ParseContext&,System.IAsyncResult)
// 0x000000B4 System.Void Google.Protobuf.ValueWriter`1::.ctor(System.Object,System.IntPtr)
// 0x000000B5 System.Void Google.Protobuf.ValueWriter`1::Invoke(Google.Protobuf.WriteContext&,T)
// 0x000000B6 System.IAsyncResult Google.Protobuf.ValueWriter`1::BeginInvoke(Google.Protobuf.WriteContext&,T,System.AsyncCallback,System.Object)
// 0x000000B7 System.Void Google.Protobuf.ValueWriter`1::EndInvoke(Google.Protobuf.WriteContext&,System.IAsyncResult)
// 0x000000B8 System.Void Google.Protobuf.FieldCodec`1::.cctor()
// 0x000000B9 System.Boolean Google.Protobuf.FieldCodec`1::IsPackedRepeatedField(System.UInt32)
// 0x000000BA System.Boolean Google.Protobuf.FieldCodec`1::get_PackedRepeatedField()
// 0x000000BB Google.Protobuf.ValueWriter`1<T> Google.Protobuf.FieldCodec`1::get_ValueWriter()
// 0x000000BC System.Func`2<T,System.Int32> Google.Protobuf.FieldCodec`1::get_ValueSizeCalculator()
// 0x000000BD Google.Protobuf.ValueReader`1<T> Google.Protobuf.FieldCodec`1::get_ValueReader()
// 0x000000BE Google.Protobuf.FieldCodec`1/InputMerger<T> Google.Protobuf.FieldCodec`1::get_ValueMerger()
// 0x000000BF Google.Protobuf.FieldCodec`1/ValuesMerger<T> Google.Protobuf.FieldCodec`1::get_FieldMerger()
// 0x000000C0 System.Int32 Google.Protobuf.FieldCodec`1::get_FixedSize()
// 0x000000C1 System.UInt32 Google.Protobuf.FieldCodec`1::get_Tag()
// 0x000000C2 System.UInt32 Google.Protobuf.FieldCodec`1::get_EndTag()
// 0x000000C3 T Google.Protobuf.FieldCodec`1::get_DefaultValue()
// 0x000000C4 System.Void Google.Protobuf.FieldCodec`1::.ctor(Google.Protobuf.ValueReader`1<T>,Google.Protobuf.ValueWriter`1<T>,System.Int32,System.UInt32,T)
// 0x000000C5 System.Void Google.Protobuf.FieldCodec`1::.ctor(Google.Protobuf.ValueReader`1<T>,Google.Protobuf.ValueWriter`1<T>,System.Func`2<T,System.Int32>,System.UInt32,T)
// 0x000000C6 System.Void Google.Protobuf.FieldCodec`1::.ctor(Google.Protobuf.ValueReader`1<T>,Google.Protobuf.ValueWriter`1<T>,Google.Protobuf.FieldCodec`1/InputMerger<T>,Google.Protobuf.FieldCodec`1/ValuesMerger<T>,System.Func`2<T,System.Int32>,System.UInt32,System.UInt32)
// 0x000000C7 System.Void Google.Protobuf.FieldCodec`1::.ctor(Google.Protobuf.ValueReader`1<T>,Google.Protobuf.ValueWriter`1<T>,Google.Protobuf.FieldCodec`1/InputMerger<T>,Google.Protobuf.FieldCodec`1/ValuesMerger<T>,System.Func`2<T,System.Int32>,System.UInt32,System.UInt32,T)
// 0x000000C8 System.Void Google.Protobuf.FieldCodec`1::WriteTagAndValue(Google.Protobuf.WriteContext&,T)
// 0x000000C9 T Google.Protobuf.FieldCodec`1::Read(Google.Protobuf.CodedInputStream)
// 0x000000CA T Google.Protobuf.FieldCodec`1::Read(Google.Protobuf.ParseContext&)
// 0x000000CB System.Int32 Google.Protobuf.FieldCodec`1::CalculateSizeWithTag(T)
// 0x000000CC System.Int32 Google.Protobuf.FieldCodec`1::CalculateUnconditionalSizeWithTag(T)
// 0x000000CD System.Boolean Google.Protobuf.FieldCodec`1::IsDefault(T)
// 0x000000CE System.Void Google.Protobuf.FieldCodec`1/InputMerger::.ctor(System.Object,System.IntPtr)
// 0x000000CF System.Void Google.Protobuf.FieldCodec`1/InputMerger::Invoke(Google.Protobuf.ParseContext&,T&)
// 0x000000D0 System.IAsyncResult Google.Protobuf.FieldCodec`1/InputMerger::BeginInvoke(Google.Protobuf.ParseContext&,T&,System.AsyncCallback,System.Object)
// 0x000000D1 System.Void Google.Protobuf.FieldCodec`1/InputMerger::EndInvoke(Google.Protobuf.ParseContext&,T&,System.IAsyncResult)
// 0x000000D2 System.Void Google.Protobuf.FieldCodec`1/ValuesMerger::.ctor(System.Object,System.IntPtr)
// 0x000000D3 System.Boolean Google.Protobuf.FieldCodec`1/ValuesMerger::Invoke(T&,T)
// 0x000000D4 System.IAsyncResult Google.Protobuf.FieldCodec`1/ValuesMerger::BeginInvoke(T&,T,System.AsyncCallback,System.Object)
// 0x000000D5 System.Boolean Google.Protobuf.FieldCodec`1/ValuesMerger::EndInvoke(T&,System.IAsyncResult)
// 0x000000D6 System.Void Google.Protobuf.FieldCodec`1/<>c__DisplayClass38_0::.ctor()
// 0x000000D7 System.Int32 Google.Protobuf.FieldCodec`1/<>c__DisplayClass38_0::<.ctor>b__0(T)
// 0x000000D8 System.Void Google.Protobuf.FieldCodec`1/<>c__DisplayClass39_0::.ctor()
// 0x000000D9 System.Void Google.Protobuf.FieldCodec`1/<>c__DisplayClass39_0::<.ctor>b__0(Google.Protobuf.ParseContext&,T&)
// 0x000000DA System.Void Google.Protobuf.FieldCodec`1/<>c::.cctor()
// 0x000000DB System.Void Google.Protobuf.FieldCodec`1/<>c::.ctor()
// 0x000000DC System.Boolean Google.Protobuf.FieldCodec`1/<>c::<.ctor>b__39_1(T&,T)
// 0x000000DD System.Void Google.Protobuf.FrameworkPortability::.cctor()
extern void FrameworkPortability__cctor_m801FE62EEC02CB0B81953BCFA718130FD04FC44A (void);
// 0x000000DE System.Void Google.Protobuf.IBufferMessage::InternalMergeFrom(Google.Protobuf.ParseContext&)
// 0x000000DF System.Void Google.Protobuf.IBufferMessage::InternalWriteTo(Google.Protobuf.WriteContext&)
// 0x000000E0 System.String Google.Protobuf.ICustomDiagnosticMessage::ToDiagnosticString()
// 0x000000E1 T Google.Protobuf.IDeepCloneable`1::Clone()
// 0x000000E2 TValue Google.Protobuf.IExtendableMessage`1::GetExtension(Google.Protobuf.Extension`2<T,TValue>)
// 0x000000E3 Google.Protobuf.Collections.RepeatedField`1<TValue> Google.Protobuf.IExtendableMessage`1::GetExtension(Google.Protobuf.RepeatedExtension`2<T,TValue>)
// 0x000000E4 Google.Protobuf.Collections.RepeatedField`1<TValue> Google.Protobuf.IExtendableMessage`1::GetOrInitializeExtension(Google.Protobuf.RepeatedExtension`2<T,TValue>)
// 0x000000E5 System.Boolean Google.Protobuf.IExtendableMessage`1::HasExtension(Google.Protobuf.Extension`2<T,TValue>)
// 0x000000E6 System.Void Google.Protobuf.IExtendableMessage`1::ClearExtension(Google.Protobuf.Extension`2<T,TValue>)
// 0x000000E7 System.Void Google.Protobuf.IMessage::MergeFrom(Google.Protobuf.CodedInputStream)
// 0x000000E8 System.Void Google.Protobuf.IMessage::WriteTo(Google.Protobuf.CodedOutputStream)
// 0x000000E9 System.Int32 Google.Protobuf.IMessage::CalculateSize()
// 0x000000EA Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.IMessage::get_Descriptor()
// 0x000000EB System.Void Google.Protobuf.IMessage`1::MergeFrom(T)
// 0x000000EC System.Void Google.Protobuf.InvalidProtocolBufferException::.ctor(System.String)
extern void InvalidProtocolBufferException__ctor_m97213D88CEBD314FB2F77EFB4B9202A9BD801A9B (void);
// 0x000000ED Google.Protobuf.InvalidProtocolBufferException Google.Protobuf.InvalidProtocolBufferException::MoreDataAvailable()
extern void InvalidProtocolBufferException_MoreDataAvailable_mBCBB93C1A4BE4D08327A0179D7CABF90E23AEF5E (void);
// 0x000000EE Google.Protobuf.InvalidProtocolBufferException Google.Protobuf.InvalidProtocolBufferException::TruncatedMessage()
extern void InvalidProtocolBufferException_TruncatedMessage_m049CB9C77F1D2B7642E2BB6DF96A16040F4FA652 (void);
// 0x000000EF Google.Protobuf.InvalidProtocolBufferException Google.Protobuf.InvalidProtocolBufferException::NegativeSize()
extern void InvalidProtocolBufferException_NegativeSize_m2D86551E8150EC70DCF2DCDBF78687FB83457E44 (void);
// 0x000000F0 Google.Protobuf.InvalidProtocolBufferException Google.Protobuf.InvalidProtocolBufferException::MalformedVarint()
extern void InvalidProtocolBufferException_MalformedVarint_m3EBE0AF1E7098AABEA72E275B25470253EC6298E (void);
// 0x000000F1 Google.Protobuf.InvalidProtocolBufferException Google.Protobuf.InvalidProtocolBufferException::InvalidTag()
extern void InvalidProtocolBufferException_InvalidTag_m956A33D88AC6DFDA5BCBCA12E9B4522A91B54789 (void);
// 0x000000F2 Google.Protobuf.InvalidProtocolBufferException Google.Protobuf.InvalidProtocolBufferException::InvalidWireType()
extern void InvalidProtocolBufferException_InvalidWireType_m5282D58657D70BB87AA17FF8B67EC9859B2013B3 (void);
// 0x000000F3 Google.Protobuf.InvalidProtocolBufferException Google.Protobuf.InvalidProtocolBufferException::InvalidEndTag()
extern void InvalidProtocolBufferException_InvalidEndTag_mC822DDDBA12C5777CAF76A906A8AA96B9598FE09 (void);
// 0x000000F4 Google.Protobuf.InvalidProtocolBufferException Google.Protobuf.InvalidProtocolBufferException::RecursionLimitExceeded()
extern void InvalidProtocolBufferException_RecursionLimitExceeded_m5033217153F5E14D4AF5F8FB4D6586F38C83F7DA (void);
// 0x000000F5 Google.Protobuf.InvalidProtocolBufferException Google.Protobuf.InvalidProtocolBufferException::SizeLimitExceeded()
extern void InvalidProtocolBufferException_SizeLimitExceeded_mBA2A8064EFD92489926DA3226ED67A62878B6A5A (void);
// 0x000000F6 Google.Protobuf.JsonFormatter Google.Protobuf.JsonFormatter::get_Default()
extern void JsonFormatter_get_Default_m5A9C0948E8C0D01D6700160BED899AD2507140D3 (void);
// 0x000000F7 System.Void Google.Protobuf.JsonFormatter::.cctor()
extern void JsonFormatter__cctor_mC34CC91F731EFA758C29177A725364FCD1EAA437 (void);
// 0x000000F8 System.Boolean Google.Protobuf.JsonFormatter::get_DiagnosticOnly()
extern void JsonFormatter_get_DiagnosticOnly_mB4C251FB0989EAB4A33A2871E83BDC61DCFBE411 (void);
// 0x000000F9 System.Void Google.Protobuf.JsonFormatter::.ctor(Google.Protobuf.JsonFormatter/Settings)
extern void JsonFormatter__ctor_mDD448159E056960E96A0A194B4A9B2EE5D3F929C (void);
// 0x000000FA System.String Google.Protobuf.JsonFormatter::Format(Google.Protobuf.IMessage)
extern void JsonFormatter_Format_m69B067C28BCE856B5063696BE1557B3AAD3214C3 (void);
// 0x000000FB System.Void Google.Protobuf.JsonFormatter::Format(Google.Protobuf.IMessage,System.IO.TextWriter)
extern void JsonFormatter_Format_mAB3068DB9F98B576C1C767B3CE003828F889CC43 (void);
// 0x000000FC System.String Google.Protobuf.JsonFormatter::ToDiagnosticString(Google.Protobuf.IMessage)
extern void JsonFormatter_ToDiagnosticString_mB0CBCBDBF65F17FC52DEB9E0C49184E44F3F2EF1 (void);
// 0x000000FD System.Void Google.Protobuf.JsonFormatter::WriteMessage(System.IO.TextWriter,Google.Protobuf.IMessage)
extern void JsonFormatter_WriteMessage_mFCD4C794107AA4DC910B3AF0C5C9550C376195DC (void);
// 0x000000FE System.Boolean Google.Protobuf.JsonFormatter::WriteMessageFields(System.IO.TextWriter,Google.Protobuf.IMessage,System.Boolean)
extern void JsonFormatter_WriteMessageFields_mA6AA1382471AA43A1D3F634CFBDBAC77439AC1A7 (void);
// 0x000000FF System.Boolean Google.Protobuf.JsonFormatter::ShouldFormatFieldValue(Google.Protobuf.IMessage,Google.Protobuf.Reflection.FieldDescriptor,System.Object)
extern void JsonFormatter_ShouldFormatFieldValue_mC94B0485C44C1E8D14D3E46FA1B89FC338A3AD6B (void);
// 0x00000100 System.String Google.Protobuf.JsonFormatter::ToJsonName(System.String)
extern void JsonFormatter_ToJsonName_m3DF73844C3F14403A0B0945B8D47161E4EE95D43 (void);
// 0x00000101 System.Void Google.Protobuf.JsonFormatter::WriteNull(System.IO.TextWriter)
extern void JsonFormatter_WriteNull_m460643D32D3CB9A93245383AE62CCA09C78C2330 (void);
// 0x00000102 System.Boolean Google.Protobuf.JsonFormatter::IsDefaultValue(Google.Protobuf.Reflection.FieldDescriptor,System.Object)
extern void JsonFormatter_IsDefaultValue_mB2A8144C10EDB1F8B17027915EF39DF2F5642BC4 (void);
// 0x00000103 System.Void Google.Protobuf.JsonFormatter::WriteValue(System.IO.TextWriter,System.Object)
extern void JsonFormatter_WriteValue_m9312535823E296DD783D53133645231311E61D90 (void);
// 0x00000104 System.Void Google.Protobuf.JsonFormatter::WriteWellKnownTypeValue(System.IO.TextWriter,Google.Protobuf.Reflection.MessageDescriptor,System.Object)
extern void JsonFormatter_WriteWellKnownTypeValue_mCB7BB81E1D3D4FD7263576AC919816FF2DE09ED4 (void);
// 0x00000105 System.Void Google.Protobuf.JsonFormatter::WriteTimestamp(System.IO.TextWriter,Google.Protobuf.IMessage)
extern void JsonFormatter_WriteTimestamp_mBC5CFB76D835F00F9AD770422F96649A69C09D75 (void);
// 0x00000106 System.Void Google.Protobuf.JsonFormatter::WriteDuration(System.IO.TextWriter,Google.Protobuf.IMessage)
extern void JsonFormatter_WriteDuration_mF1E7087F5235E61DA7F42C914763CD2640E9A0A9 (void);
// 0x00000107 System.Void Google.Protobuf.JsonFormatter::WriteFieldMask(System.IO.TextWriter,Google.Protobuf.IMessage)
extern void JsonFormatter_WriteFieldMask_m8089EC7E6F371A3F16B76C0F7982B2F4556C66D7 (void);
// 0x00000108 System.Void Google.Protobuf.JsonFormatter::WriteAny(System.IO.TextWriter,Google.Protobuf.IMessage)
extern void JsonFormatter_WriteAny_m87C93F6B5F83D2A294F141BC10F9728006EA25FC (void);
// 0x00000109 System.Void Google.Protobuf.JsonFormatter::WriteDiagnosticOnlyAny(System.IO.TextWriter,Google.Protobuf.IMessage)
extern void JsonFormatter_WriteDiagnosticOnlyAny_m9D5B3C912D6DEFAD518D2B8E5BA1CFB09B8B0D52 (void);
// 0x0000010A System.Void Google.Protobuf.JsonFormatter::WriteStruct(System.IO.TextWriter,Google.Protobuf.IMessage)
extern void JsonFormatter_WriteStruct_m2EEFEE9C86D94A3BEA199AA236BCE1BAF936BF0B (void);
// 0x0000010B System.Void Google.Protobuf.JsonFormatter::WriteStructFieldValue(System.IO.TextWriter,Google.Protobuf.IMessage)
extern void JsonFormatter_WriteStructFieldValue_m85C319B5B236A40D7E069A5E42A0DFDAF7F1B136 (void);
// 0x0000010C System.Void Google.Protobuf.JsonFormatter::WriteList(System.IO.TextWriter,System.Collections.IList)
extern void JsonFormatter_WriteList_m01543BDA268D0BCB53EE9B1CC1B84A67C7FDF52D (void);
// 0x0000010D System.Void Google.Protobuf.JsonFormatter::WriteDictionary(System.IO.TextWriter,System.Collections.IDictionary)
extern void JsonFormatter_WriteDictionary_m05901C36EEDF7132D13097D539AA6A46B405D236 (void);
// 0x0000010E System.Void Google.Protobuf.JsonFormatter::WriteString(System.IO.TextWriter,System.String)
extern void JsonFormatter_WriteString_mD25155E3B003EF3A17D7F5D01971D38FACA89015 (void);
// 0x0000010F System.Void Google.Protobuf.JsonFormatter::HexEncodeUtf16CodeUnit(System.IO.TextWriter,System.Char)
extern void JsonFormatter_HexEncodeUtf16CodeUnit_mA2EEC68387011433F6B4E29F1598386C374E4AD3 (void);
// 0x00000110 Google.Protobuf.JsonFormatter/Settings Google.Protobuf.JsonFormatter/Settings::get_Default()
extern void Settings_get_Default_mF4FDE9D3D751BD702D8219E4EB5517E0F6FF9B74 (void);
// 0x00000111 System.Void Google.Protobuf.JsonFormatter/Settings::.cctor()
extern void Settings__cctor_mF4512A1C1C5FEA84200E9397456A62A96B181FD2 (void);
// 0x00000112 System.Boolean Google.Protobuf.JsonFormatter/Settings::get_FormatDefaultValues()
extern void Settings_get_FormatDefaultValues_m64C456D788C62CB3E0F0996AFF989CC6981239F3 (void);
// 0x00000113 Google.Protobuf.Reflection.TypeRegistry Google.Protobuf.JsonFormatter/Settings::get_TypeRegistry()
extern void Settings_get_TypeRegistry_m9BA673D58183796B0F0C9E2DBBE89F432A6D6D0E (void);
// 0x00000114 System.Boolean Google.Protobuf.JsonFormatter/Settings::get_FormatEnumsAsIntegers()
extern void Settings_get_FormatEnumsAsIntegers_m51CB1E83B7EA54E4D894263F570805407D05CFE1 (void);
// 0x00000115 System.Void Google.Protobuf.JsonFormatter/Settings::.ctor(System.Boolean)
extern void Settings__ctor_mA9EA3FE08831F0F5A44D585C0DB04A208F02AEFA (void);
// 0x00000116 System.Void Google.Protobuf.JsonFormatter/Settings::.ctor(System.Boolean,Google.Protobuf.Reflection.TypeRegistry)
extern void Settings__ctor_m929B7642DBF8FCFDA94B97233FC773B74DABD7B7 (void);
// 0x00000117 System.Void Google.Protobuf.JsonFormatter/Settings::.ctor(System.Boolean,Google.Protobuf.Reflection.TypeRegistry,System.Boolean)
extern void Settings__ctor_m778ABAE093451E7B1CA47C2A3B43D94C8A9D46BB (void);
// 0x00000118 System.String Google.Protobuf.JsonFormatter/OriginalEnumValueHelper::GetOriginalName(System.Object)
extern void OriginalEnumValueHelper_GetOriginalName_m36AA4B3C591A31126E42DD475946C02D9CB1296C (void);
// 0x00000119 System.Collections.Generic.Dictionary`2<System.Object,System.String> Google.Protobuf.JsonFormatter/OriginalEnumValueHelper::GetNameMapping(System.Type)
extern void OriginalEnumValueHelper_GetNameMapping_mC0C9C5FCD331F9997BF3F2A6D6349AB366C8D2F4 (void);
// 0x0000011A System.Void Google.Protobuf.JsonFormatter/OriginalEnumValueHelper::.cctor()
extern void OriginalEnumValueHelper__cctor_m08FFA8138A0441FC17D4D97DB5F209877A9F496E (void);
// 0x0000011B System.Void Google.Protobuf.JsonFormatter/OriginalEnumValueHelper/<>c::.cctor()
extern void U3CU3Ec__cctor_m23171665533BE5299B7466C18DEFEA08A225CED5 (void);
// 0x0000011C System.Void Google.Protobuf.JsonFormatter/OriginalEnumValueHelper/<>c::.ctor()
extern void U3CU3Ec__ctor_m8A4E99CDBE7F880923AF3978955A596A6CFEAD21 (void);
// 0x0000011D System.Boolean Google.Protobuf.JsonFormatter/OriginalEnumValueHelper/<>c::<GetNameMapping>b__2_0(System.Reflection.FieldInfo)
extern void U3CU3Ec_U3CGetNameMappingU3Eb__2_0_mAB55EF229CE87D1E2A5F3EC117AFEE14CB3B0019 (void);
// 0x0000011E System.Boolean Google.Protobuf.JsonFormatter/OriginalEnumValueHelper/<>c::<GetNameMapping>b__2_1(System.Reflection.FieldInfo)
extern void U3CU3Ec_U3CGetNameMappingU3Eb__2_1_m4363C5DBD92E971FA74128DFCEAE80C4FFDF40BE (void);
// 0x0000011F System.Object Google.Protobuf.JsonFormatter/OriginalEnumValueHelper/<>c::<GetNameMapping>b__2_2(System.Reflection.FieldInfo)
extern void U3CU3Ec_U3CGetNameMappingU3Eb__2_2_mC6E555E9DF03A3DD2D7841046524957CFD0F081D (void);
// 0x00000120 System.String Google.Protobuf.JsonFormatter/OriginalEnumValueHelper/<>c::<GetNameMapping>b__2_3(System.Reflection.FieldInfo)
extern void U3CU3Ec_U3CGetNameMappingU3Eb__2_3_m974D038691FD95D34419B7B4D7EA34E65A8D05A9 (void);
// 0x00000121 System.Byte[] Google.Protobuf.MessageExtensions::ToByteArray(Google.Protobuf.IMessage)
extern void MessageExtensions_ToByteArray_m1BBBB6D4DC2D766396375663F29F4427A33D8812 (void);
// 0x00000122 System.Void Google.Protobuf.MessageExtensions::MergeFrom(Google.Protobuf.IMessage,System.Byte[],System.Boolean,Google.Protobuf.ExtensionRegistry)
extern void MessageExtensions_MergeFrom_m1473F9DF3353F288CF4A04AA5CE23D014542C5F4 (void);
// 0x00000123 System.Void Google.Protobuf.MessageExtensions::MergeFrom(Google.Protobuf.IMessage,Google.Protobuf.ByteString,System.Boolean,Google.Protobuf.ExtensionRegistry)
extern void MessageExtensions_MergeFrom_mF7F80928C53902B587749A72D0569EFF68C10DA6 (void);
// 0x00000124 System.Boolean Google.Protobuf.MessageParser::get_DiscardUnknownFields()
extern void MessageParser_get_DiscardUnknownFields_m1B7BDCE4283878E28C0B4ACB3944EF71ED872B98 (void);
// 0x00000125 Google.Protobuf.ExtensionRegistry Google.Protobuf.MessageParser::get_Extensions()
extern void MessageParser_get_Extensions_m13D4911AAC2CFF3977A4EF7A60853D1234526737 (void);
// 0x00000126 System.Void Google.Protobuf.MessageParser::.ctor(System.Func`1<Google.Protobuf.IMessage>,System.Boolean,Google.Protobuf.ExtensionRegistry)
extern void MessageParser__ctor_mAF12C3D74ED237CE23AB15C17CE9D9DCF79C86AA (void);
// 0x00000127 Google.Protobuf.IMessage Google.Protobuf.MessageParser::ParseFrom(Google.Protobuf.ByteString)
extern void MessageParser_ParseFrom_mC4BC411F00F2ACB3A4CFD1C140BFAAE0467144B4 (void);
// 0x00000128 System.Void Google.Protobuf.MessageParser`1::.ctor(System.Func`1<T>)
// 0x00000129 System.Void Google.Protobuf.MessageParser`1::.ctor(System.Func`1<T>,System.Boolean,Google.Protobuf.ExtensionRegistry)
// 0x0000012A T Google.Protobuf.MessageParser`1::CreateTemplate()
// 0x0000012B T Google.Protobuf.MessageParser`1::ParseFrom(System.Byte[])
// 0x0000012C Google.Protobuf.MessageParser`1<T> Google.Protobuf.MessageParser`1::WithExtensionRegistry(Google.Protobuf.ExtensionRegistry)
// 0x0000012D System.Void Google.Protobuf.MessageParser`1/<>c__DisplayClass2_0::.ctor()
// 0x0000012E Google.Protobuf.IMessage Google.Protobuf.MessageParser`1/<>c__DisplayClass2_0::<.ctor>b__0()
// 0x0000012F System.Void Google.Protobuf.ObjectIntPair`1::.ctor(T,System.Int32)
// 0x00000130 System.Boolean Google.Protobuf.ObjectIntPair`1::Equals(Google.Protobuf.ObjectIntPair`1<T>)
// 0x00000131 System.Boolean Google.Protobuf.ObjectIntPair`1::Equals(System.Object)
// 0x00000132 System.Int32 Google.Protobuf.ObjectIntPair`1::GetHashCode()
// 0x00000133 System.Void Google.Protobuf.ParseContext::Initialize(System.ReadOnlySpan`1<System.Byte>&,Google.Protobuf.ParserInternalState&,Google.Protobuf.ParseContext&)
extern void ParseContext_Initialize_mBED2DA9409401F1EA9B8FC92DA667C0669BA41B0 (void);
// 0x00000134 System.Void Google.Protobuf.ParseContext::Initialize(Google.Protobuf.CodedInputStream,Google.Protobuf.ParseContext&)
extern void ParseContext_Initialize_mABC8723132FF5E7405A2E515A02306C184CB1B1C (void);
// 0x00000135 System.Void Google.Protobuf.ParseContext::Initialize(System.Buffers.ReadOnlySequence`1<System.Byte>,Google.Protobuf.ParseContext&)
extern void ParseContext_Initialize_m8EAA0BF3F3E350F19FC82575E0005B79CF3DFB2F (void);
// 0x00000136 System.Void Google.Protobuf.ParseContext::Initialize(System.Buffers.ReadOnlySequence`1<System.Byte>,System.Int32,Google.Protobuf.ParseContext&)
extern void ParseContext_Initialize_mF0462C0CAAB7FF2E2C6A75F5F8769FE5DDDD9E68 (void);
// 0x00000137 System.UInt32 Google.Protobuf.ParseContext::get_LastTag()
extern void ParseContext_get_LastTag_mD9E2763BC34A1111408D24771A9AC105C074DD53 (void);
// 0x00000138 System.Boolean Google.Protobuf.ParseContext::get_DiscardUnknownFields()
extern void ParseContext_get_DiscardUnknownFields_mB67E8578452722E0B978BF3D107532C93D3268CB (void);
// 0x00000139 Google.Protobuf.ExtensionRegistry Google.Protobuf.ParseContext::get_ExtensionRegistry()
extern void ParseContext_get_ExtensionRegistry_m4B2B015EBC8CA7328C07E164A6671545CCBD0F00 (void);
// 0x0000013A System.UInt32 Google.Protobuf.ParseContext::ReadTag()
extern void ParseContext_ReadTag_mEE687A18078235454AEA63F1F61E9E3E9EB0260F (void);
// 0x0000013B System.Double Google.Protobuf.ParseContext::ReadDouble()
extern void ParseContext_ReadDouble_m8C106CDD420E7FA1311F33B2EAD994E4FD6A99DB (void);
// 0x0000013C System.Single Google.Protobuf.ParseContext::ReadFloat()
extern void ParseContext_ReadFloat_m1B6730B5A824501C0163F2DE6693B2F3B5939BC8 (void);
// 0x0000013D System.UInt64 Google.Protobuf.ParseContext::ReadUInt64()
extern void ParseContext_ReadUInt64_m4E6FC396669BEAB285DFD47E650004F92AC16A48 (void);
// 0x0000013E System.Int64 Google.Protobuf.ParseContext::ReadInt64()
extern void ParseContext_ReadInt64_mEDF40EF10BA05D0959FAD12976873ACF68163D4D (void);
// 0x0000013F System.Int32 Google.Protobuf.ParseContext::ReadInt32()
extern void ParseContext_ReadInt32_m275087DD36E27495CC2EE76EEB08BC90EFAF94B9 (void);
// 0x00000140 System.UInt64 Google.Protobuf.ParseContext::ReadFixed64()
extern void ParseContext_ReadFixed64_m196FF86F1C53A073A179AB5CA93828CDBA3052ED (void);
// 0x00000141 System.UInt32 Google.Protobuf.ParseContext::ReadFixed32()
extern void ParseContext_ReadFixed32_m74BD7221FF3B1898D374C4E03EC036C576C677A2 (void);
// 0x00000142 System.Boolean Google.Protobuf.ParseContext::ReadBool()
extern void ParseContext_ReadBool_m097C5083B8554A5F33B1799DD8BF30926A5942BE (void);
// 0x00000143 System.String Google.Protobuf.ParseContext::ReadString()
extern void ParseContext_ReadString_mA21E24240255C281D36669A209628AD6FDBB15AE (void);
// 0x00000144 System.Void Google.Protobuf.ParseContext::ReadMessage(Google.Protobuf.IMessage)
extern void ParseContext_ReadMessage_mA72BE697566685D137AE783593B8AB4558090118 (void);
// 0x00000145 Google.Protobuf.ByteString Google.Protobuf.ParseContext::ReadBytes()
extern void ParseContext_ReadBytes_mB08BDD0C46819718BA3813A1A943264DEAC283C5 (void);
// 0x00000146 System.UInt32 Google.Protobuf.ParseContext::ReadUInt32()
extern void ParseContext_ReadUInt32_m961D49845C792A0B46CEA9E5B57FB50390FA5FDE (void);
// 0x00000147 System.Int32 Google.Protobuf.ParseContext::ReadEnum()
extern void ParseContext_ReadEnum_mB005F6D2C670173280B8EDB79CE4150B83F08374 (void);
// 0x00000148 System.Int32 Google.Protobuf.ParseContext::ReadLength()
extern void ParseContext_ReadLength_mABDC3E98A6A4A46C6102FF84C40C42A6C1F0EC87 (void);
// 0x00000149 System.Void Google.Protobuf.ParseContext::CopyStateTo(Google.Protobuf.CodedInputStream)
extern void ParseContext_CopyStateTo_mC016DFBBF2BAF69F8A740391DA9F3C25BB94E273 (void);
// 0x0000014A System.Void Google.Protobuf.ParseContext::LoadStateFrom(Google.Protobuf.CodedInputStream)
extern void ParseContext_LoadStateFrom_m74FA6FF020C2458A8787FC3E7643E624517F4CB4 (void);
// 0x0000014B Google.Protobuf.CodedInputStream Google.Protobuf.ParserInternalState::get_CodedInputStream()
extern void ParserInternalState_get_CodedInputStream_m245B3E90A4D6AC379449EA79C3D1A4E2526BD712 (void);
// 0x0000014C System.Boolean Google.Protobuf.ParserInternalState::get_DiscardUnknownFields()
extern void ParserInternalState_get_DiscardUnknownFields_mABFB91062F6B1A730ABD81691F138C516B0E3020 (void);
// 0x0000014D System.Void Google.Protobuf.ParserInternalState::set_DiscardUnknownFields(System.Boolean)
extern void ParserInternalState_set_DiscardUnknownFields_m6374B0D645A0352B3BF53F9B0EFEF62E45501C58 (void);
// 0x0000014E Google.Protobuf.ExtensionRegistry Google.Protobuf.ParserInternalState::get_ExtensionRegistry()
extern void ParserInternalState_get_ExtensionRegistry_m8ABCB61553B7732DBB4DB08481D898E5479347CE (void);
// 0x0000014F System.Void Google.Protobuf.ParserInternalState::set_ExtensionRegistry(Google.Protobuf.ExtensionRegistry)
extern void ParserInternalState_set_ExtensionRegistry_mEA5D5DD0A75713D26A2300501B940EB7A67C2B5C (void);
// 0x00000150 System.Int32 Google.Protobuf.ParsingPrimitives::ParseLength(System.ReadOnlySpan`1<System.Byte>&,Google.Protobuf.ParserInternalState&)
extern void ParsingPrimitives_ParseLength_mC2FC8A77E6BCAB09D6149C6EDF2D080E76EC945F (void);
// 0x00000151 System.UInt32 Google.Protobuf.ParsingPrimitives::ParseTag(System.ReadOnlySpan`1<System.Byte>&,Google.Protobuf.ParserInternalState&)
extern void ParsingPrimitives_ParseTag_mF467A1C5FE6962837D3E138E0D2DD3F159EE6A1E (void);
// 0x00000152 System.Boolean Google.Protobuf.ParsingPrimitives::MaybeConsumeTag(System.ReadOnlySpan`1<System.Byte>&,Google.Protobuf.ParserInternalState&,System.UInt32)
extern void ParsingPrimitives_MaybeConsumeTag_m1456930481A43092670E797F496880804BAA1910 (void);
// 0x00000153 System.UInt32 Google.Protobuf.ParsingPrimitives::PeekTag(System.ReadOnlySpan`1<System.Byte>&,Google.Protobuf.ParserInternalState&)
extern void ParsingPrimitives_PeekTag_m9FF6F1ABC59AA1A572BA0082DDD86EE2B0E2B8E1 (void);
// 0x00000154 System.UInt64 Google.Protobuf.ParsingPrimitives::ParseRawVarint64(System.ReadOnlySpan`1<System.Byte>&,Google.Protobuf.ParserInternalState&)
extern void ParsingPrimitives_ParseRawVarint64_m991C711AC6C490742D2A18D54B518A73603B29CF (void);
// 0x00000155 System.UInt64 Google.Protobuf.ParsingPrimitives::ParseRawVarint64SlowPath(System.ReadOnlySpan`1<System.Byte>&,Google.Protobuf.ParserInternalState&)
extern void ParsingPrimitives_ParseRawVarint64SlowPath_m59A67C82B9654A164A896C73076A54FF4E2799BE (void);
// 0x00000156 System.UInt32 Google.Protobuf.ParsingPrimitives::ParseRawVarint32(System.ReadOnlySpan`1<System.Byte>&,Google.Protobuf.ParserInternalState&)
extern void ParsingPrimitives_ParseRawVarint32_m603BEFB03EAD05D2C5B543DA459AE0E987BB92E7 (void);
// 0x00000157 System.UInt32 Google.Protobuf.ParsingPrimitives::ParseRawVarint32SlowPath(System.ReadOnlySpan`1<System.Byte>&,Google.Protobuf.ParserInternalState&)
extern void ParsingPrimitives_ParseRawVarint32SlowPath_m204EA9D003AE36F0706720209B029795B26156C7 (void);
// 0x00000158 System.UInt32 Google.Protobuf.ParsingPrimitives::ParseRawLittleEndian32(System.ReadOnlySpan`1<System.Byte>&,Google.Protobuf.ParserInternalState&)
extern void ParsingPrimitives_ParseRawLittleEndian32_m0B7F41846EEB39F5E8D6BCB2C05DD54205F98B64 (void);
// 0x00000159 System.UInt32 Google.Protobuf.ParsingPrimitives::ParseRawLittleEndian32SlowPath(System.ReadOnlySpan`1<System.Byte>&,Google.Protobuf.ParserInternalState&)
extern void ParsingPrimitives_ParseRawLittleEndian32SlowPath_mF929DE5CB57AC8A142965B9F5907760189973427 (void);
// 0x0000015A System.UInt64 Google.Protobuf.ParsingPrimitives::ParseRawLittleEndian64(System.ReadOnlySpan`1<System.Byte>&,Google.Protobuf.ParserInternalState&)
extern void ParsingPrimitives_ParseRawLittleEndian64_mD2A647B269340B3B4D1981C0A71DF21BA55FD92A (void);
// 0x0000015B System.UInt64 Google.Protobuf.ParsingPrimitives::ParseRawLittleEndian64SlowPath(System.ReadOnlySpan`1<System.Byte>&,Google.Protobuf.ParserInternalState&)
extern void ParsingPrimitives_ParseRawLittleEndian64SlowPath_m41E32918988DDFC97EAAF6CB926974A1D15D628A (void);
// 0x0000015C System.Double Google.Protobuf.ParsingPrimitives::ParseDouble(System.ReadOnlySpan`1<System.Byte>&,Google.Protobuf.ParserInternalState&)
extern void ParsingPrimitives_ParseDouble_m9D7A20CB6804A925788138CC60FC1006300AA3B2 (void);
// 0x0000015D System.Single Google.Protobuf.ParsingPrimitives::ParseFloat(System.ReadOnlySpan`1<System.Byte>&,Google.Protobuf.ParserInternalState&)
extern void ParsingPrimitives_ParseFloat_mC2FA1C7C9EAE7C021684295E4E09120C0BCCC710 (void);
// 0x0000015E System.Single Google.Protobuf.ParsingPrimitives::ParseFloatSlow(System.ReadOnlySpan`1<System.Byte>&,Google.Protobuf.ParserInternalState&)
extern void ParsingPrimitives_ParseFloatSlow_m512EC27CE5ABFDA8C010852A9A9F1024D6247D70 (void);
// 0x0000015F System.Byte[] Google.Protobuf.ParsingPrimitives::ReadRawBytes(System.ReadOnlySpan`1<System.Byte>&,Google.Protobuf.ParserInternalState&,System.Int32)
extern void ParsingPrimitives_ReadRawBytes_m2D3C496252E736CF124A44AEFF4CF51D95658B45 (void);
// 0x00000160 System.Byte[] Google.Protobuf.ParsingPrimitives::ReadRawBytesSlow(System.ReadOnlySpan`1<System.Byte>&,Google.Protobuf.ParserInternalState&,System.Int32)
extern void ParsingPrimitives_ReadRawBytesSlow_mBC9FF1D8D69D9C053ACF878CE12FE5AE1D9297AC (void);
// 0x00000161 System.Void Google.Protobuf.ParsingPrimitives::SkipRawBytes(System.ReadOnlySpan`1<System.Byte>&,Google.Protobuf.ParserInternalState&,System.Int32)
extern void ParsingPrimitives_SkipRawBytes_m43FF250C33B1B8942ECA81E472045DA849B44FC0 (void);
// 0x00000162 System.String Google.Protobuf.ParsingPrimitives::ReadString(System.ReadOnlySpan`1<System.Byte>&,Google.Protobuf.ParserInternalState&)
extern void ParsingPrimitives_ReadString_mF4065E9BBE004B97FBA2D159B6B175A1876AD495 (void);
// 0x00000163 Google.Protobuf.ByteString Google.Protobuf.ParsingPrimitives::ReadBytes(System.ReadOnlySpan`1<System.Byte>&,Google.Protobuf.ParserInternalState&)
extern void ParsingPrimitives_ReadBytes_mCED56422C2310BB900CF0B6A1F5E8F3F7F5AE214 (void);
// 0x00000164 System.String Google.Protobuf.ParsingPrimitives::ReadRawString(System.ReadOnlySpan`1<System.Byte>&,Google.Protobuf.ParserInternalState&,System.Int32)
extern void ParsingPrimitives_ReadRawString_mFFD978AA6F9021997AA4154B07D4A5B47ABB345C (void);
// 0x00000165 System.String Google.Protobuf.ParsingPrimitives::ReadStringSlow(System.ReadOnlySpan`1<System.Byte>&,Google.Protobuf.ParserInternalState&,System.Int32)
extern void ParsingPrimitives_ReadStringSlow_mA9EB2AE44F8A093E333B150063E6C936535E8CCB (void);
// 0x00000166 System.Void Google.Protobuf.ParsingPrimitives::ValidateCurrentLimit(System.ReadOnlySpan`1<System.Byte>&,Google.Protobuf.ParserInternalState&,System.Int32)
extern void ParsingPrimitives_ValidateCurrentLimit_m9EC4B92BAA5FE179C699A7941D03F634B2D1F5A9 (void);
// 0x00000167 System.Byte Google.Protobuf.ParsingPrimitives::ReadRawByte(System.ReadOnlySpan`1<System.Byte>&,Google.Protobuf.ParserInternalState&)
extern void ParsingPrimitives_ReadRawByte_m702A011D5ACEEA7CDE6EAFD06005D8989D281C55 (void);
// 0x00000168 System.Boolean Google.Protobuf.ParsingPrimitives::IsDataAvailable(Google.Protobuf.ParserInternalState&,System.Int32)
extern void ParsingPrimitives_IsDataAvailable_mC54DA4458BCF17D18A5DFBE63FFA362CE432E890 (void);
// 0x00000169 System.Boolean Google.Protobuf.ParsingPrimitives::IsDataAvailableInSource(Google.Protobuf.ParserInternalState&,System.Int32)
extern void ParsingPrimitives_IsDataAvailableInSource_m6AA9E21894C6603A481B5914B18258B20BFE2017 (void);
// 0x0000016A System.Void Google.Protobuf.ParsingPrimitives::ReadRawBytesIntoSpan(System.ReadOnlySpan`1<System.Byte>&,Google.Protobuf.ParserInternalState&,System.Int32,System.Span`1<System.Byte>)
extern void ParsingPrimitives_ReadRawBytesIntoSpan_m6120704BC8084ED27250B00C5ABB68B03E44A9EA (void);
// 0x0000016B System.Void Google.Protobuf.ParsingPrimitivesMessages::SkipLastField(System.ReadOnlySpan`1<System.Byte>&,Google.Protobuf.ParserInternalState&)
extern void ParsingPrimitivesMessages_SkipLastField_mEDE2B24A023BCD6071A30E2CCA444B11DBDA2449 (void);
// 0x0000016C System.Void Google.Protobuf.ParsingPrimitivesMessages::SkipGroup(System.ReadOnlySpan`1<System.Byte>&,Google.Protobuf.ParserInternalState&,System.UInt32)
extern void ParsingPrimitivesMessages_SkipGroup_m3CAD89D30DFE66A449C84CC9DAB7CE07B71919B0 (void);
// 0x0000016D System.Void Google.Protobuf.ParsingPrimitivesMessages::ReadMessage(Google.Protobuf.ParseContext&,Google.Protobuf.IMessage)
extern void ParsingPrimitivesMessages_ReadMessage_mF4AB6B44529A994E2FA7D71110BBE1B901D64810 (void);
// 0x0000016E System.Collections.Generic.KeyValuePair`2<TKey,TValue> Google.Protobuf.ParsingPrimitivesMessages::ReadMapEntry(Google.Protobuf.ParseContext&,Google.Protobuf.Collections.MapField`2/Codec<TKey,TValue>)
// 0x0000016F System.Void Google.Protobuf.ParsingPrimitivesMessages::ReadGroup(Google.Protobuf.ParseContext&,System.Int32,Google.Protobuf.UnknownFieldSet)
extern void ParsingPrimitivesMessages_ReadGroup_m0308867608D14663008DBCDD37F56B38A3AD4AA0 (void);
// 0x00000170 System.Void Google.Protobuf.ParsingPrimitivesMessages::ReadRawMessage(Google.Protobuf.ParseContext&,Google.Protobuf.IMessage)
extern void ParsingPrimitivesMessages_ReadRawMessage_m5A98B881D178F876BBE20108F4F024E352E0EDFE (void);
// 0x00000171 System.Void Google.Protobuf.ParsingPrimitivesMessages::CheckReadEndOfStreamTag(Google.Protobuf.ParserInternalState&)
extern void ParsingPrimitivesMessages_CheckReadEndOfStreamTag_m55AEBE9C00414E376BBBF968B0108225D3FEB981 (void);
// 0x00000172 System.Void Google.Protobuf.ParsingPrimitivesMessages::CheckLastTagWas(Google.Protobuf.ParserInternalState&,System.UInt32)
extern void ParsingPrimitivesMessages_CheckLastTagWas_m73D305A8B0749862580196E33B64624D3AB2BB6A (void);
// 0x00000173 System.Void Google.Protobuf.ParsingPrimitivesMessages::.cctor()
extern void ParsingPrimitivesMessages__cctor_m73B1494389EF8A80D92E9C979C08FCCF22C3C5A6 (void);
// 0x00000174 T Google.Protobuf.ProtoPreconditions::CheckNotNull(T,System.String)
// 0x00000175 T Google.Protobuf.ProtoPreconditions::CheckNotNullUnconstrained(T,System.String)
// 0x00000176 System.Void Google.Protobuf.SegmentedBufferHelper::Initialize(Google.Protobuf.CodedInputStream,Google.Protobuf.SegmentedBufferHelper&)
extern void SegmentedBufferHelper_Initialize_m63CA047207E9863358F76EBB38BE6A927F1DD991 (void);
// 0x00000177 System.Void Google.Protobuf.SegmentedBufferHelper::Initialize(System.Buffers.ReadOnlySequence`1<System.Byte>,Google.Protobuf.SegmentedBufferHelper&,System.ReadOnlySpan`1<System.Byte>&)
extern void SegmentedBufferHelper_Initialize_mCD7335441CB503495A0248F07E920189DC9E0A09 (void);
// 0x00000178 System.Boolean Google.Protobuf.SegmentedBufferHelper::RefillBuffer(System.ReadOnlySpan`1<System.Byte>&,Google.Protobuf.ParserInternalState&,System.Boolean)
extern void SegmentedBufferHelper_RefillBuffer_m75ABD1D414AA5D56ECB052EF58E151DA8468893F (void);
// 0x00000179 System.Nullable`1<System.Int32> Google.Protobuf.SegmentedBufferHelper::get_TotalLength()
extern void SegmentedBufferHelper_get_TotalLength_m1F9022D9A2F697A6F81FF34ABA719D983C7926E5 (void);
// 0x0000017A Google.Protobuf.CodedInputStream Google.Protobuf.SegmentedBufferHelper::get_CodedInputStream()
extern void SegmentedBufferHelper_get_CodedInputStream_m5FF2CFFD3C0EF5E732067FA923D9A1059C51E56E (void);
// 0x0000017B System.Int32 Google.Protobuf.SegmentedBufferHelper::PushLimit(Google.Protobuf.ParserInternalState&,System.Int32)
extern void SegmentedBufferHelper_PushLimit_mC9D151CF2449D1652EBD4857A9F986FF71AEC9F2 (void);
// 0x0000017C System.Void Google.Protobuf.SegmentedBufferHelper::PopLimit(Google.Protobuf.ParserInternalState&,System.Int32)
extern void SegmentedBufferHelper_PopLimit_m73669DD916AD6F101DBB5D6DD3ECA2E09EF7F0C8 (void);
// 0x0000017D System.Boolean Google.Protobuf.SegmentedBufferHelper::IsReachedLimit(Google.Protobuf.ParserInternalState&)
extern void SegmentedBufferHelper_IsReachedLimit_m64CAC04AA2F44A1C3063BBAA9EAE824628C436B8 (void);
// 0x0000017E System.Boolean Google.Protobuf.SegmentedBufferHelper::IsAtEnd(System.ReadOnlySpan`1<System.Byte>&,Google.Protobuf.ParserInternalState&)
extern void SegmentedBufferHelper_IsAtEnd_mD21ECBDA96B7672A23C47F6C4A4AD1B491E96379 (void);
// 0x0000017F System.Boolean Google.Protobuf.SegmentedBufferHelper::RefillFromReadOnlySequence(System.ReadOnlySpan`1<System.Byte>&,Google.Protobuf.ParserInternalState&,System.Boolean)
extern void SegmentedBufferHelper_RefillFromReadOnlySequence_m6205503EFBA7900C8DC05FB228F499EF57D76910 (void);
// 0x00000180 System.Boolean Google.Protobuf.SegmentedBufferHelper::RefillFromCodedInputStream(System.ReadOnlySpan`1<System.Byte>&,Google.Protobuf.ParserInternalState&,System.Boolean)
extern void SegmentedBufferHelper_RefillFromCodedInputStream_mD18F57A2D807F9653438B2843E58D82E6B58C04F (void);
// 0x00000181 System.Void Google.Protobuf.SegmentedBufferHelper::RecomputeBufferSizeAfterLimit(Google.Protobuf.ParserInternalState&)
extern void SegmentedBufferHelper_RecomputeBufferSizeAfterLimit_m0561CCE5954F686F6A6A95AD298DD75B4B86DB43 (void);
// 0x00000182 System.Void Google.Protobuf.SegmentedBufferHelper::CheckCurrentBufferIsEmpty(Google.Protobuf.ParserInternalState&)
extern void SegmentedBufferHelper_CheckCurrentBufferIsEmpty_mB9810DFFA170A059CF79A7CBBAE03DA3C0A69E5B (void);
// 0x00000183 System.Void Google.Protobuf.UnknownField::.ctor()
extern void UnknownField__ctor_m943C5046DD86CAEDEEDCE89DB2490E5D2840E68C (void);
// 0x00000184 System.Boolean Google.Protobuf.UnknownField::Equals(System.Object)
extern void UnknownField_Equals_m41D22CC0BBB261BEA6C753E55D97852732F0EBAF (void);
// 0x00000185 System.Int32 Google.Protobuf.UnknownField::GetHashCode()
extern void UnknownField_GetHashCode_m2545B6879093E416F5C13883EFB68520AEA057BC (void);
// 0x00000186 System.Void Google.Protobuf.UnknownField::WriteTo(System.Int32,Google.Protobuf.WriteContext&)
extern void UnknownField_WriteTo_m90C9A4AF939DA4BC8C3501B7021B5B67BD2EC864 (void);
// 0x00000187 System.Int32 Google.Protobuf.UnknownField::GetSerializedSize(System.Int32)
extern void UnknownField_GetSerializedSize_m7A2BC6C3337CA40AA2A9C3EAE126FC1100149B5D (void);
// 0x00000188 Google.Protobuf.UnknownField Google.Protobuf.UnknownField::MergeFrom(Google.Protobuf.UnknownField)
extern void UnknownField_MergeFrom_m0D5752F3FFEF0E25C9A4CBFF5BC6E1D1B3BD969B (void);
// 0x00000189 System.Collections.Generic.List`1<T> Google.Protobuf.UnknownField::AddAll(System.Collections.Generic.List`1<T>,System.Collections.Generic.IList`1<T>)
// 0x0000018A Google.Protobuf.UnknownField Google.Protobuf.UnknownField::AddVarint(System.UInt64)
extern void UnknownField_AddVarint_m4EEAABA5BD2B2EFBFEFF832409C93CF1A105B4CB (void);
// 0x0000018B Google.Protobuf.UnknownField Google.Protobuf.UnknownField::AddFixed32(System.UInt32)
extern void UnknownField_AddFixed32_mF341A2670F523D4AC2806E8A89A484E2E0B5F683 (void);
// 0x0000018C Google.Protobuf.UnknownField Google.Protobuf.UnknownField::AddFixed64(System.UInt64)
extern void UnknownField_AddFixed64_mE93303BDACFC97871E7D63C2663AF48CB9FF910F (void);
// 0x0000018D Google.Protobuf.UnknownField Google.Protobuf.UnknownField::AddLengthDelimited(Google.Protobuf.ByteString)
extern void UnknownField_AddLengthDelimited_mD1E59FF116E2BF450D56DEDCA74E4D09E2AA5B70 (void);
// 0x0000018E Google.Protobuf.UnknownField Google.Protobuf.UnknownField::AddGroup(Google.Protobuf.UnknownFieldSet)
extern void UnknownField_AddGroup_m2AC9A7F4F7D9074CCEFE54B53A8D8D55ABD40305 (void);
// 0x0000018F System.Collections.Generic.List`1<T> Google.Protobuf.UnknownField::Add(System.Collections.Generic.List`1<T>,T)
// 0x00000190 System.Void Google.Protobuf.UnknownFieldSet::.ctor()
extern void UnknownFieldSet__ctor_m4E14CBE1BB8E5B7C16412C33E43CC11A604DD6EB (void);
// 0x00000191 System.Boolean Google.Protobuf.UnknownFieldSet::HasField(System.Int32)
extern void UnknownFieldSet_HasField_mA80A58FCB11938C8403A3140642D0F203AD82CD4 (void);
// 0x00000192 System.Void Google.Protobuf.UnknownFieldSet::WriteTo(Google.Protobuf.CodedOutputStream)
extern void UnknownFieldSet_WriteTo_m9A480B637780AE4EC7FE1C788273D73E6C267795 (void);
// 0x00000193 System.Void Google.Protobuf.UnknownFieldSet::WriteTo(Google.Protobuf.WriteContext&)
extern void UnknownFieldSet_WriteTo_mECF1EFA7AF68EBBEDDD046865B2C17131B969E1B (void);
// 0x00000194 System.Int32 Google.Protobuf.UnknownFieldSet::CalculateSize()
extern void UnknownFieldSet_CalculateSize_m0F48042AFDC7E6795E7A99D8B3BE0D5C61C96868 (void);
// 0x00000195 System.Boolean Google.Protobuf.UnknownFieldSet::Equals(System.Object)
extern void UnknownFieldSet_Equals_m999BB91F13D3256C77F0C7A39E49793AF64E6595 (void);
// 0x00000196 System.Int32 Google.Protobuf.UnknownFieldSet::GetHashCode()
extern void UnknownFieldSet_GetHashCode_m6DAE03F0C0E9F6228751D8538C9734AE339ABD19 (void);
// 0x00000197 Google.Protobuf.UnknownField Google.Protobuf.UnknownFieldSet::GetOrAddField(System.Int32)
extern void UnknownFieldSet_GetOrAddField_m7B963B5E9DCB2579EAC0CC4E556FD83619441ECA (void);
// 0x00000198 Google.Protobuf.UnknownFieldSet Google.Protobuf.UnknownFieldSet::AddOrReplaceField(System.Int32,Google.Protobuf.UnknownField)
extern void UnknownFieldSet_AddOrReplaceField_m68E9F4093CD33361F77B5B5B0B7B9B2BE8FC26A3 (void);
// 0x00000199 System.Boolean Google.Protobuf.UnknownFieldSet::MergeFieldFrom(Google.Protobuf.ParseContext&)
extern void UnknownFieldSet_MergeFieldFrom_m561379A6714FBE8A08B68BA4FB6CD6EB159BADF1 (void);
// 0x0000019A System.Void Google.Protobuf.UnknownFieldSet::MergeGroupFrom(Google.Protobuf.ParseContext&)
extern void UnknownFieldSet_MergeGroupFrom_m5EFE89A8E12412A49792ED4CF1DB22FA77BBCE39 (void);
// 0x0000019B Google.Protobuf.UnknownFieldSet Google.Protobuf.UnknownFieldSet::MergeFieldFrom(Google.Protobuf.UnknownFieldSet,Google.Protobuf.CodedInputStream)
extern void UnknownFieldSet_MergeFieldFrom_m4655DE968A5275DA43F2968F08B58BDB8521B051 (void);
// 0x0000019C Google.Protobuf.UnknownFieldSet Google.Protobuf.UnknownFieldSet::MergeFieldFrom(Google.Protobuf.UnknownFieldSet,Google.Protobuf.ParseContext&)
extern void UnknownFieldSet_MergeFieldFrom_m6D9E32ADBB0194941C65CD43522F53C3A2378283 (void);
// 0x0000019D Google.Protobuf.UnknownFieldSet Google.Protobuf.UnknownFieldSet::MergeFrom(Google.Protobuf.UnknownFieldSet)
extern void UnknownFieldSet_MergeFrom_mB5736E4A0F2A45787555D2B0C98F0B531F9778AF (void);
// 0x0000019E Google.Protobuf.UnknownFieldSet Google.Protobuf.UnknownFieldSet::MergeFrom(Google.Protobuf.UnknownFieldSet,Google.Protobuf.UnknownFieldSet)
extern void UnknownFieldSet_MergeFrom_m3175C4FF04375A5EF7DC4E3245C6606303344F85 (void);
// 0x0000019F Google.Protobuf.UnknownFieldSet Google.Protobuf.UnknownFieldSet::MergeField(System.Int32,Google.Protobuf.UnknownField)
extern void UnknownFieldSet_MergeField_m2E0EDB53398818DF68D494A47321CD1085C57A65 (void);
// 0x000001A0 Google.Protobuf.UnknownFieldSet Google.Protobuf.UnknownFieldSet::Clone(Google.Protobuf.UnknownFieldSet)
extern void UnknownFieldSet_Clone_mBCB3196178FBF9E17BF3C686C6C6DC214D0BBDEA (void);
// 0x000001A1 Google.Protobuf.WireFormat/WireType Google.Protobuf.WireFormat::GetTagWireType(System.UInt32)
extern void WireFormat_GetTagWireType_m367E6B9AB0C0534F19A35A2574A55759A139510B (void);
// 0x000001A2 System.Int32 Google.Protobuf.WireFormat::GetTagFieldNumber(System.UInt32)
extern void WireFormat_GetTagFieldNumber_mDF369E25A14A2B12311F243DA9E68B25DC1DFB76 (void);
// 0x000001A3 System.UInt32 Google.Protobuf.WireFormat::MakeTag(System.Int32,Google.Protobuf.WireFormat/WireType)
extern void WireFormat_MakeTag_mA074D84171D39ACFB2BB7BB2DA85945DCE23B576 (void);
// 0x000001A4 Google.Protobuf.CodedOutputStream Google.Protobuf.WriteBufferHelper::get_CodedOutputStream()
extern void WriteBufferHelper_get_CodedOutputStream_m92805AD7A4248AFD24342FE0F3AF1ECA9E67CAB1 (void);
// 0x000001A5 System.Void Google.Protobuf.WriteBufferHelper::Initialize(Google.Protobuf.CodedOutputStream,Google.Protobuf.WriteBufferHelper&)
extern void WriteBufferHelper_Initialize_m5CDEEE39143C13C043A378606AD78D32A8F7E2F3 (void);
// 0x000001A6 System.Void Google.Protobuf.WriteBufferHelper::CheckNoSpaceLeft(Google.Protobuf.WriterInternalState&)
extern void WriteBufferHelper_CheckNoSpaceLeft_mE97B4E30DDB343E7E28F0301A49C9C9CA4DA1ECE (void);
// 0x000001A7 System.Int32 Google.Protobuf.WriteBufferHelper::GetSpaceLeft(Google.Protobuf.WriterInternalState&)
extern void WriteBufferHelper_GetSpaceLeft_m0A47B45340528CDB6E26BFA4776FB7A0507C7258 (void);
// 0x000001A8 System.Void Google.Protobuf.WriteBufferHelper::RefreshBuffer(System.Span`1<System.Byte>&,Google.Protobuf.WriterInternalState&)
extern void WriteBufferHelper_RefreshBuffer_mFB6576A94800AC1C6A210B3D45265C2CC3074AC1 (void);
// 0x000001A9 System.Void Google.Protobuf.WriteBufferHelper::Flush(System.Span`1<System.Byte>&,Google.Protobuf.WriterInternalState&)
extern void WriteBufferHelper_Flush_mBA2753A178AE3C8D13AE06D39910DEEC468C595A (void);
// 0x000001AA System.Void Google.Protobuf.WriteContext::Initialize(System.Span`1<System.Byte>&,Google.Protobuf.WriterInternalState&,Google.Protobuf.WriteContext&)
extern void WriteContext_Initialize_m76A276505C625A28A7FE936BF62FFA661235E53F (void);
// 0x000001AB System.Void Google.Protobuf.WriteContext::Initialize(Google.Protobuf.CodedOutputStream,Google.Protobuf.WriteContext&)
extern void WriteContext_Initialize_mBAAD3A83874DB7991E24AF783A983CC9204C89BD (void);
// 0x000001AC System.Void Google.Protobuf.WriteContext::WriteDouble(System.Double)
extern void WriteContext_WriteDouble_m36DE9708C2E9F55E08DF1593168026CD929B4114 (void);
// 0x000001AD System.Void Google.Protobuf.WriteContext::WriteFloat(System.Single)
extern void WriteContext_WriteFloat_m36D978A87046CB34CE1857EAE1079DD9B8972248 (void);
// 0x000001AE System.Void Google.Protobuf.WriteContext::WriteUInt64(System.UInt64)
extern void WriteContext_WriteUInt64_mE585A8A413E7581324D6B61B063DAB8CD88D4383 (void);
// 0x000001AF System.Void Google.Protobuf.WriteContext::WriteInt64(System.Int64)
extern void WriteContext_WriteInt64_m20616F89120F9551E46B5074B6C14823C97D0C4F (void);
// 0x000001B0 System.Void Google.Protobuf.WriteContext::WriteInt32(System.Int32)
extern void WriteContext_WriteInt32_mCC3646C9CF9A13E2BEBC15A8E566669C913D4F65 (void);
// 0x000001B1 System.Void Google.Protobuf.WriteContext::WriteFixed64(System.UInt64)
extern void WriteContext_WriteFixed64_mAE849DB91093D95337CB4164CA83C8E2B25C7716 (void);
// 0x000001B2 System.Void Google.Protobuf.WriteContext::WriteFixed32(System.UInt32)
extern void WriteContext_WriteFixed32_mECD70B0022C1DFBE81B61366495335BE6B297AF8 (void);
// 0x000001B3 System.Void Google.Protobuf.WriteContext::WriteBool(System.Boolean)
extern void WriteContext_WriteBool_m68B7D58774A48FFC3FF34D9A6F770E579EEFA7DF (void);
// 0x000001B4 System.Void Google.Protobuf.WriteContext::WriteString(System.String)
extern void WriteContext_WriteString_m1A0F46C5CF383B3F976BFBD24B546EC2E346C68C (void);
// 0x000001B5 System.Void Google.Protobuf.WriteContext::WriteMessage(Google.Protobuf.IMessage)
extern void WriteContext_WriteMessage_m191D2C3E865C097E18DA174B172FFFA9685C8B98 (void);
// 0x000001B6 System.Void Google.Protobuf.WriteContext::WriteBytes(Google.Protobuf.ByteString)
extern void WriteContext_WriteBytes_m6483E64A6655BB2E1499AD548C549B47FB5EB3E5 (void);
// 0x000001B7 System.Void Google.Protobuf.WriteContext::WriteUInt32(System.UInt32)
extern void WriteContext_WriteUInt32_mA6366289E56066C7C0AB06218534DCF1F5C75353 (void);
// 0x000001B8 System.Void Google.Protobuf.WriteContext::WriteEnum(System.Int32)
extern void WriteContext_WriteEnum_mE9C54A63A11FA8F28864359CE2236760B7C7AABC (void);
// 0x000001B9 System.Void Google.Protobuf.WriteContext::WriteLength(System.Int32)
extern void WriteContext_WriteLength_mEBAD2E61EA0B60DD8789138399357B2E42C7BE4F (void);
// 0x000001BA System.Void Google.Protobuf.WriteContext::WriteTag(System.Int32,Google.Protobuf.WireFormat/WireType)
extern void WriteContext_WriteTag_m610740BFC2CB1D051CF1DCC94FFE749C801584C9 (void);
// 0x000001BB System.Void Google.Protobuf.WriteContext::WriteTag(System.UInt32)
extern void WriteContext_WriteTag_m6041201C0BF3CBBCE75D5DD21474E69BC4CD80EF (void);
// 0x000001BC System.Void Google.Protobuf.WriteContext::WriteRawTag(System.Byte)
extern void WriteContext_WriteRawTag_m3E37F0B83B4E8B7C7E1CC6472D527AA22B11DA5A (void);
// 0x000001BD System.Void Google.Protobuf.WriteContext::WriteRawTag(System.Byte,System.Byte)
extern void WriteContext_WriteRawTag_m3889E0A7DCDBEE6B057EECBB614B022B4D4F991F (void);
// 0x000001BE System.Void Google.Protobuf.WriteContext::CopyStateTo(Google.Protobuf.CodedOutputStream)
extern void WriteContext_CopyStateTo_mB67D2A3DB1B06B506C65BEDC5CC7EC1FC3A3FB97 (void);
// 0x000001BF System.Void Google.Protobuf.WriteContext::LoadStateFrom(Google.Protobuf.CodedOutputStream)
extern void WriteContext_LoadStateFrom_mF406ADA99F6893BA926A0AD8950B8A8EF85F2D7A (void);
// 0x000001C0 Google.Protobuf.CodedOutputStream Google.Protobuf.WriterInternalState::get_CodedOutputStream()
extern void WriterInternalState_get_CodedOutputStream_m3601FE09E7D068C19BBEE6B3DEAB889130C983C3 (void);
// 0x000001C1 System.Void Google.Protobuf.WritingPrimitives::WriteDouble(System.Span`1<System.Byte>&,Google.Protobuf.WriterInternalState&,System.Double)
extern void WritingPrimitives_WriteDouble_mF3C3599EC5A2C8341CEDB1D9410EBAA66816BEA7 (void);
// 0x000001C2 System.Void Google.Protobuf.WritingPrimitives::WriteFloat(System.Span`1<System.Byte>&,Google.Protobuf.WriterInternalState&,System.Single)
extern void WritingPrimitives_WriteFloat_mEA7B691A96886F04E18865F4BDC0E584599C7E39 (void);
// 0x000001C3 System.Void Google.Protobuf.WritingPrimitives::WriteFloatSlowPath(System.Span`1<System.Byte>&,Google.Protobuf.WriterInternalState&,System.Single)
extern void WritingPrimitives_WriteFloatSlowPath_mA8E622A32E1170F3E8F595013DAB8AD141091709 (void);
// 0x000001C4 System.Void Google.Protobuf.WritingPrimitives::WriteUInt64(System.Span`1<System.Byte>&,Google.Protobuf.WriterInternalState&,System.UInt64)
extern void WritingPrimitives_WriteUInt64_m7179598033A8A18131EE39680B1F547B0C9E37F7 (void);
// 0x000001C5 System.Void Google.Protobuf.WritingPrimitives::WriteInt64(System.Span`1<System.Byte>&,Google.Protobuf.WriterInternalState&,System.Int64)
extern void WritingPrimitives_WriteInt64_mADE3CEBA230A0A86A0AC21D71ED8FE1674A96EB7 (void);
// 0x000001C6 System.Void Google.Protobuf.WritingPrimitives::WriteInt32(System.Span`1<System.Byte>&,Google.Protobuf.WriterInternalState&,System.Int32)
extern void WritingPrimitives_WriteInt32_m7DACD767626E22F6955E9371B451DECE85D647E1 (void);
// 0x000001C7 System.Void Google.Protobuf.WritingPrimitives::WriteFixed64(System.Span`1<System.Byte>&,Google.Protobuf.WriterInternalState&,System.UInt64)
extern void WritingPrimitives_WriteFixed64_m5BE77A057DF8A002691684B96C2B7A954CE85ECA (void);
// 0x000001C8 System.Void Google.Protobuf.WritingPrimitives::WriteFixed32(System.Span`1<System.Byte>&,Google.Protobuf.WriterInternalState&,System.UInt32)
extern void WritingPrimitives_WriteFixed32_m264BBE74CE53ADFA5280F3FEB396710C3D49EE93 (void);
// 0x000001C9 System.Void Google.Protobuf.WritingPrimitives::WriteBool(System.Span`1<System.Byte>&,Google.Protobuf.WriterInternalState&,System.Boolean)
extern void WritingPrimitives_WriteBool_mFC8A6B5C0260575DB6A581918C0ABD9F6EFC136A (void);
// 0x000001CA System.Void Google.Protobuf.WritingPrimitives::WriteString(System.Span`1<System.Byte>&,Google.Protobuf.WriterInternalState&,System.String)
extern void WritingPrimitives_WriteString_m9D7752CCBA30C9C42C8ECCBD03FD029A2240144A (void);
// 0x000001CB System.Int32 Google.Protobuf.WritingPrimitives::WriteStringToBuffer(System.Span`1<System.Byte>,Google.Protobuf.WriterInternalState&,System.String)
extern void WritingPrimitives_WriteStringToBuffer_m1B44348AEAF67F075423A8BA4D955A48ABF182C7 (void);
// 0x000001CC System.Void Google.Protobuf.WritingPrimitives::WriteBytes(System.Span`1<System.Byte>&,Google.Protobuf.WriterInternalState&,Google.Protobuf.ByteString)
extern void WritingPrimitives_WriteBytes_mC0B23B56CA938CAF6807BC7170860955410781F4 (void);
// 0x000001CD System.Void Google.Protobuf.WritingPrimitives::WriteUInt32(System.Span`1<System.Byte>&,Google.Protobuf.WriterInternalState&,System.UInt32)
extern void WritingPrimitives_WriteUInt32_m44AADF2F1DAE5ACFB8B982887AABC369DEBB7144 (void);
// 0x000001CE System.Void Google.Protobuf.WritingPrimitives::WriteEnum(System.Span`1<System.Byte>&,Google.Protobuf.WriterInternalState&,System.Int32)
extern void WritingPrimitives_WriteEnum_mE98FE36561BCAD8F86524064953A5661E3D03BED (void);
// 0x000001CF System.Void Google.Protobuf.WritingPrimitives::WriteLength(System.Span`1<System.Byte>&,Google.Protobuf.WriterInternalState&,System.Int32)
extern void WritingPrimitives_WriteLength_mFCCCDC6E637B15A5CF626E80D487D94E3FEAB5AE (void);
// 0x000001D0 System.Void Google.Protobuf.WritingPrimitives::WriteRawVarint32(System.Span`1<System.Byte>&,Google.Protobuf.WriterInternalState&,System.UInt32)
extern void WritingPrimitives_WriteRawVarint32_m80E7B1B2014AA3D0E50550EF7BC424B22426EAAB (void);
// 0x000001D1 System.Void Google.Protobuf.WritingPrimitives::WriteRawVarint64(System.Span`1<System.Byte>&,Google.Protobuf.WriterInternalState&,System.UInt64)
extern void WritingPrimitives_WriteRawVarint64_m209CE9059AE1427F11B53D9DC04ACFD3406D51DB (void);
// 0x000001D2 System.Void Google.Protobuf.WritingPrimitives::WriteRawLittleEndian32(System.Span`1<System.Byte>&,Google.Protobuf.WriterInternalState&,System.UInt32)
extern void WritingPrimitives_WriteRawLittleEndian32_mF720ADD7B8D2EFA7D76910B02F780C49B7BCB697 (void);
// 0x000001D3 System.Void Google.Protobuf.WritingPrimitives::WriteRawLittleEndian32SlowPath(System.Span`1<System.Byte>&,Google.Protobuf.WriterInternalState&,System.UInt32)
extern void WritingPrimitives_WriteRawLittleEndian32SlowPath_mC991B66C50349FE8EC5F0AF5D65191C877333247 (void);
// 0x000001D4 System.Void Google.Protobuf.WritingPrimitives::WriteRawLittleEndian64(System.Span`1<System.Byte>&,Google.Protobuf.WriterInternalState&,System.UInt64)
extern void WritingPrimitives_WriteRawLittleEndian64_m8CC508077D42C814112B1E80E1525711F2774AD3 (void);
// 0x000001D5 System.Void Google.Protobuf.WritingPrimitives::WriteRawLittleEndian64SlowPath(System.Span`1<System.Byte>&,Google.Protobuf.WriterInternalState&,System.UInt64)
extern void WritingPrimitives_WriteRawLittleEndian64SlowPath_mA4B4B0D9EFF6845D4BFAA835FF25DFB3CFDA9A29 (void);
// 0x000001D6 System.Void Google.Protobuf.WritingPrimitives::WriteRawByte(System.Span`1<System.Byte>&,Google.Protobuf.WriterInternalState&,System.Byte)
extern void WritingPrimitives_WriteRawByte_m09FE0FCC63987E7FC325D0785E5BAE55E11E6C27 (void);
// 0x000001D7 System.Void Google.Protobuf.WritingPrimitives::WriteRawBytes(System.Span`1<System.Byte>&,Google.Protobuf.WriterInternalState&,System.Byte[])
extern void WritingPrimitives_WriteRawBytes_m82F533CAB5C7A6EAA9238F1993BC6AF14E8E54BC (void);
// 0x000001D8 System.Void Google.Protobuf.WritingPrimitives::WriteRawBytes(System.Span`1<System.Byte>&,Google.Protobuf.WriterInternalState&,System.ReadOnlySpan`1<System.Byte>)
extern void WritingPrimitives_WriteRawBytes_m6BD7D272545E097DCA9B59C15526972EB1BE4E6A (void);
// 0x000001D9 System.Void Google.Protobuf.WritingPrimitives::WriteTag(System.Span`1<System.Byte>&,Google.Protobuf.WriterInternalState&,System.Int32,Google.Protobuf.WireFormat/WireType)
extern void WritingPrimitives_WriteTag_m20DA49695454AA1E65B7192B8DFA90C44F30152F (void);
// 0x000001DA System.Void Google.Protobuf.WritingPrimitives::WriteTag(System.Span`1<System.Byte>&,Google.Protobuf.WriterInternalState&,System.UInt32)
extern void WritingPrimitives_WriteTag_m4A875B305FF92E29D8BE78A347A0962ED5AE0AB1 (void);
// 0x000001DB System.Void Google.Protobuf.WritingPrimitives::WriteRawTag(System.Span`1<System.Byte>&,Google.Protobuf.WriterInternalState&,System.Byte)
extern void WritingPrimitives_WriteRawTag_mDEBC5CD4ACEEEB1E4ACA80622176C86F08DE7BF8 (void);
// 0x000001DC System.Void Google.Protobuf.WritingPrimitives::WriteRawTag(System.Span`1<System.Byte>&,Google.Protobuf.WriterInternalState&,System.Byte,System.Byte)
extern void WritingPrimitives_WriteRawTag_m6ECCDE65477C117CCCD301F38187D73D0DE7258C (void);
// 0x000001DD System.Void Google.Protobuf.WritingPrimitives::WriteRawTagSlowPath(System.Span`1<System.Byte>&,Google.Protobuf.WriterInternalState&,System.Byte,System.Byte)
extern void WritingPrimitives_WriteRawTagSlowPath_m52271A35E5027D4FCB8011C5DFC4062BA196E2D4 (void);
// 0x000001DE System.Void Google.Protobuf.WritingPrimitives::.cctor()
extern void WritingPrimitives__cctor_mC944CE893A1231A22BB210A15CFACBEDCFEAA651 (void);
// 0x000001DF System.Void Google.Protobuf.WritingPrimitivesMessages::WriteMessage(Google.Protobuf.WriteContext&,Google.Protobuf.IMessage)
extern void WritingPrimitivesMessages_WriteMessage_m9A9AA7223ABB3C3F21018D0F85F504736B07D85E (void);
// 0x000001E0 System.Void Google.Protobuf.WritingPrimitivesMessages::WriteRawMessage(Google.Protobuf.WriteContext&,Google.Protobuf.IMessage)
extern void WritingPrimitivesMessages_WriteRawMessage_m466A1428FF61276D3E990BA5B2EE64810303EB5E (void);
// 0x000001E1 Google.Protobuf.Reflection.FileDescriptor Google.Protobuf.WellKnownTypes.AnyReflection::get_Descriptor()
extern void AnyReflection_get_Descriptor_mF58E048143D4F90346366A71E0427335CDD98736 (void);
// 0x000001E2 System.Void Google.Protobuf.WellKnownTypes.AnyReflection::.cctor()
extern void AnyReflection__cctor_m28799E694101DB1E808027349EBDC740DCF1ABCF (void);
// 0x000001E3 Google.Protobuf.MessageParser`1<Google.Protobuf.WellKnownTypes.Any> Google.Protobuf.WellKnownTypes.Any::get_Parser()
extern void Any_get_Parser_mE797270D14D0703DA6E8BA1F9074B4DD42CDE9B0 (void);
// 0x000001E4 Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.WellKnownTypes.Any::get_Descriptor()
extern void Any_get_Descriptor_mB044C3483527D965D4203FA88CCDB8AB59FAF624 (void);
// 0x000001E5 Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.WellKnownTypes.Any::pb::Google.Protobuf.IMessage.get_Descriptor()
extern void Any_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m20D87828F043DEC39805FF4047C49A00DA10216F (void);
// 0x000001E6 System.Void Google.Protobuf.WellKnownTypes.Any::.ctor()
extern void Any__ctor_m1ECC62A42AB856A583601A266C7C8CF336408E90 (void);
// 0x000001E7 System.Void Google.Protobuf.WellKnownTypes.Any::.ctor(Google.Protobuf.WellKnownTypes.Any)
extern void Any__ctor_m2376F8C7176FFA131593622A480CE332EB4B1B13 (void);
// 0x000001E8 Google.Protobuf.WellKnownTypes.Any Google.Protobuf.WellKnownTypes.Any::Clone()
extern void Any_Clone_m0DED49DFACD7ED97904D1D4290A65A1C494FDA6F (void);
// 0x000001E9 System.String Google.Protobuf.WellKnownTypes.Any::get_TypeUrl()
extern void Any_get_TypeUrl_mC45F8AE94967D3FB99B564A4ADA96205BC80DF1E (void);
// 0x000001EA System.Void Google.Protobuf.WellKnownTypes.Any::set_TypeUrl(System.String)
extern void Any_set_TypeUrl_m8205CD3987662353B2D5C97ABE5B667789763FD4 (void);
// 0x000001EB Google.Protobuf.ByteString Google.Protobuf.WellKnownTypes.Any::get_Value()
extern void Any_get_Value_mDC67D93A2B4FD2412CA75536675294D04CAE5A4D (void);
// 0x000001EC System.Void Google.Protobuf.WellKnownTypes.Any::set_Value(Google.Protobuf.ByteString)
extern void Any_set_Value_m40B44205CB793BDC16154F620CFED4F55272E677 (void);
// 0x000001ED System.Boolean Google.Protobuf.WellKnownTypes.Any::Equals(System.Object)
extern void Any_Equals_mD55E6718944CD8AA56B9AFC8B9B584BE6E4BEF43 (void);
// 0x000001EE System.Boolean Google.Protobuf.WellKnownTypes.Any::Equals(Google.Protobuf.WellKnownTypes.Any)
extern void Any_Equals_m39E1D5E36B954E909B5F6C5D904430B5064AB976 (void);
// 0x000001EF System.Int32 Google.Protobuf.WellKnownTypes.Any::GetHashCode()
extern void Any_GetHashCode_m07B1193DDB01F335CE42B217AE9BD3A88C6848FB (void);
// 0x000001F0 System.String Google.Protobuf.WellKnownTypes.Any::ToString()
extern void Any_ToString_m0A75195213C8DF63A33CA3775B9684EEED9E9749 (void);
// 0x000001F1 System.Void Google.Protobuf.WellKnownTypes.Any::WriteTo(Google.Protobuf.CodedOutputStream)
extern void Any_WriteTo_m4F0C9449ECFD97252B6CC7090CACD8A532BD2BFB (void);
// 0x000001F2 System.Void Google.Protobuf.WellKnownTypes.Any::pb::Google.Protobuf.IBufferMessage.InternalWriteTo(Google.Protobuf.WriteContext&)
extern void Any_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m1A5C5A2DE6524DE3A5A86FB9E4AD5422AFA61562 (void);
// 0x000001F3 System.Int32 Google.Protobuf.WellKnownTypes.Any::CalculateSize()
extern void Any_CalculateSize_m0CC466C0EA20B4192A897E77F5AEDD4E22E6B9A4 (void);
// 0x000001F4 System.Void Google.Protobuf.WellKnownTypes.Any::MergeFrom(Google.Protobuf.WellKnownTypes.Any)
extern void Any_MergeFrom_mB3B78279933A5AA5BA4B45E7EB58A4F133BF7177 (void);
// 0x000001F5 System.Void Google.Protobuf.WellKnownTypes.Any::MergeFrom(Google.Protobuf.CodedInputStream)
extern void Any_MergeFrom_mBFB2D9E1EC1825E910098EDFEA6547ABB949BECD (void);
// 0x000001F6 System.Void Google.Protobuf.WellKnownTypes.Any::pb::Google.Protobuf.IBufferMessage.InternalMergeFrom(Google.Protobuf.ParseContext&)
extern void Any_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_mF51F0CE65F862C0DCAF37AD560D439D63DB570CB (void);
// 0x000001F7 System.String Google.Protobuf.WellKnownTypes.Any::GetTypeName(System.String)
extern void Any_GetTypeName_m981156327D46656C8E8819D94B7A496DD264B75B (void);
// 0x000001F8 System.Void Google.Protobuf.WellKnownTypes.Any::.cctor()
extern void Any__cctor_m990153EEC7FFADE95312E6AE3F5032DBDD7DD41C (void);
// 0x000001F9 System.Void Google.Protobuf.WellKnownTypes.Any/<>c::.cctor()
extern void U3CU3Ec__cctor_m355B62D4F25CCE2AC973FBE0B7EAE1BD827FE914 (void);
// 0x000001FA System.Void Google.Protobuf.WellKnownTypes.Any/<>c::.ctor()
extern void U3CU3Ec__ctor_m498B298BE961BF12E0B8C3F49D48537FB0B0E4B7 (void);
// 0x000001FB Google.Protobuf.WellKnownTypes.Any Google.Protobuf.WellKnownTypes.Any/<>c::<.cctor>b__40_0()
extern void U3CU3Ec_U3C_cctorU3Eb__40_0_m1427A4A9EAD998C31A791AF9996DD999A8382DF7 (void);
// 0x000001FC Google.Protobuf.Reflection.FileDescriptor Google.Protobuf.WellKnownTypes.DurationReflection::get_Descriptor()
extern void DurationReflection_get_Descriptor_mD740650A11CBA770313496B8D700061E0BE35E46 (void);
// 0x000001FD System.Void Google.Protobuf.WellKnownTypes.DurationReflection::.cctor()
extern void DurationReflection__cctor_m02F448AF98215704F1B577286E2A9114EA6F285D (void);
// 0x000001FE Google.Protobuf.MessageParser`1<Google.Protobuf.WellKnownTypes.Duration> Google.Protobuf.WellKnownTypes.Duration::get_Parser()
extern void Duration_get_Parser_m96C08CB52416A21B7C5B93F359A6EF4F3BC6AF7B (void);
// 0x000001FF Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.WellKnownTypes.Duration::get_Descriptor()
extern void Duration_get_Descriptor_m8B4F6425E8541BA5F1FACD8B8E0846BD3E5DA409 (void);
// 0x00000200 Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.WellKnownTypes.Duration::pb::Google.Protobuf.IMessage.get_Descriptor()
extern void Duration_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m12A6EE9725228581AA665137F9C37BC865C9820B (void);
// 0x00000201 System.Void Google.Protobuf.WellKnownTypes.Duration::.ctor()
extern void Duration__ctor_m6E8317977C3ABF38F7C0DF31CF9FD578DB563EEA (void);
// 0x00000202 System.Void Google.Protobuf.WellKnownTypes.Duration::.ctor(Google.Protobuf.WellKnownTypes.Duration)
extern void Duration__ctor_mE32B96FC0271D3892E7E9B684B384787EF78E56D (void);
// 0x00000203 Google.Protobuf.WellKnownTypes.Duration Google.Protobuf.WellKnownTypes.Duration::Clone()
extern void Duration_Clone_mF481B40FEAFC57DDFA82F9357B1E7BCB2A5645EE (void);
// 0x00000204 System.Int64 Google.Protobuf.WellKnownTypes.Duration::get_Seconds()
extern void Duration_get_Seconds_m1B8C2E4131812F6CE8BC4E4592274CEA4AFCFAE0 (void);
// 0x00000205 System.Void Google.Protobuf.WellKnownTypes.Duration::set_Seconds(System.Int64)
extern void Duration_set_Seconds_m4D56CEC9CFED641A03BFEE1EB411D4CCECB3819A (void);
// 0x00000206 System.Int32 Google.Protobuf.WellKnownTypes.Duration::get_Nanos()
extern void Duration_get_Nanos_m9AF2BD58D223A08F2179036D4E64C2421894744B (void);
// 0x00000207 System.Void Google.Protobuf.WellKnownTypes.Duration::set_Nanos(System.Int32)
extern void Duration_set_Nanos_m097E8310B0000815582B89BB0C445E14B81161E8 (void);
// 0x00000208 System.Boolean Google.Protobuf.WellKnownTypes.Duration::Equals(System.Object)
extern void Duration_Equals_mC8E45E2881848B04EF30A2A9F507A755840D8895 (void);
// 0x00000209 System.Boolean Google.Protobuf.WellKnownTypes.Duration::Equals(Google.Protobuf.WellKnownTypes.Duration)
extern void Duration_Equals_m1BE5DD19D22AF8643B4CE200DE7F7F79853D4292 (void);
// 0x0000020A System.Int32 Google.Protobuf.WellKnownTypes.Duration::GetHashCode()
extern void Duration_GetHashCode_m9177E77337F424B4F7B8895403B9E472EF1C98D8 (void);
// 0x0000020B System.String Google.Protobuf.WellKnownTypes.Duration::ToString()
extern void Duration_ToString_m9A4C19C321E83FD1E47AC98C2F8A2513B2580900 (void);
// 0x0000020C System.Void Google.Protobuf.WellKnownTypes.Duration::WriteTo(Google.Protobuf.CodedOutputStream)
extern void Duration_WriteTo_mBB57F86544E6C7F06853460506A3C7534FFDBC5D (void);
// 0x0000020D System.Void Google.Protobuf.WellKnownTypes.Duration::pb::Google.Protobuf.IBufferMessage.InternalWriteTo(Google.Protobuf.WriteContext&)
extern void Duration_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_mBA1C04DAEA64BD35140B8B97E305116436D657A3 (void);
// 0x0000020E System.Int32 Google.Protobuf.WellKnownTypes.Duration::CalculateSize()
extern void Duration_CalculateSize_m9E15FCF69AB0CE774A486CB3C0B5C9AA6417EABF (void);
// 0x0000020F System.Void Google.Protobuf.WellKnownTypes.Duration::MergeFrom(Google.Protobuf.WellKnownTypes.Duration)
extern void Duration_MergeFrom_m5815E64B100ABE517B1AF02E6EEEE2F9937C6EE9 (void);
// 0x00000210 System.Void Google.Protobuf.WellKnownTypes.Duration::MergeFrom(Google.Protobuf.CodedInputStream)
extern void Duration_MergeFrom_m4150356C75EC52495E8CF0B15F5A1832ACBE8F8A (void);
// 0x00000211 System.Void Google.Protobuf.WellKnownTypes.Duration::pb::Google.Protobuf.IBufferMessage.InternalMergeFrom(Google.Protobuf.ParseContext&)
extern void Duration_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m03941E2F76DE634FD45F0AD9FF80E8FDD8FC8B3C (void);
// 0x00000212 System.Boolean Google.Protobuf.WellKnownTypes.Duration::IsNormalized(System.Int64,System.Int32)
extern void Duration_IsNormalized_m279FFDCFEBAC7D93B9F1D5F0BDDAD859EC9534DE (void);
// 0x00000213 System.String Google.Protobuf.WellKnownTypes.Duration::ToJson(System.Int64,System.Int32,System.Boolean)
extern void Duration_ToJson_m4921C26255D3E10F2B76FBB66910931EAE20AF1B (void);
// 0x00000214 System.String Google.Protobuf.WellKnownTypes.Duration::ToDiagnosticString()
extern void Duration_ToDiagnosticString_m6AD61CDA4BE97EC1FE5EBB280E73952CC57D6653 (void);
// 0x00000215 System.Void Google.Protobuf.WellKnownTypes.Duration::AppendNanoseconds(System.Text.StringBuilder,System.Int32)
extern void Duration_AppendNanoseconds_mD3CEC9D0A01B513B37B78C2AE9A050258DF321EC (void);
// 0x00000216 System.Void Google.Protobuf.WellKnownTypes.Duration::.cctor()
extern void Duration__cctor_m0B409DD121AEF904DF5695FE6605B94974D23BB9 (void);
// 0x00000217 System.Void Google.Protobuf.WellKnownTypes.Duration/<>c::.cctor()
extern void U3CU3Ec__cctor_mE11DB05928D0D50178F0DB0C963950E9807D3E81 (void);
// 0x00000218 System.Void Google.Protobuf.WellKnownTypes.Duration/<>c::.ctor()
extern void U3CU3Ec__ctor_mF4755084B094EBF7BA7BF27974F8FF04CFF5E757 (void);
// 0x00000219 Google.Protobuf.WellKnownTypes.Duration Google.Protobuf.WellKnownTypes.Duration/<>c::<.cctor>b__48_0()
extern void U3CU3Ec_U3C_cctorU3Eb__48_0_m3CCA2718FE30E9EDE4182FF9A118D0FA3340C12A (void);
// 0x0000021A Google.Protobuf.Reflection.FileDescriptor Google.Protobuf.WellKnownTypes.FieldMaskReflection::get_Descriptor()
extern void FieldMaskReflection_get_Descriptor_m71513B57323612637E03F04DB3D3F972DF5342A3 (void);
// 0x0000021B System.Void Google.Protobuf.WellKnownTypes.FieldMaskReflection::.cctor()
extern void FieldMaskReflection__cctor_m19AFD86FFAEF23703D49F490A5C39D9CB9AEBA83 (void);
// 0x0000021C Google.Protobuf.MessageParser`1<Google.Protobuf.WellKnownTypes.FieldMask> Google.Protobuf.WellKnownTypes.FieldMask::get_Parser()
extern void FieldMask_get_Parser_m7D7346798414A3449234B1A840A06A3E3F682EC4 (void);
// 0x0000021D Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.WellKnownTypes.FieldMask::get_Descriptor()
extern void FieldMask_get_Descriptor_mEC2F8CEAD4DEC85FF8C020F7DAA36C267E75C63E (void);
// 0x0000021E Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.WellKnownTypes.FieldMask::pb::Google.Protobuf.IMessage.get_Descriptor()
extern void FieldMask_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m0FEDF588A4B0370EF55B76E856B263560DA23052 (void);
// 0x0000021F System.Void Google.Protobuf.WellKnownTypes.FieldMask::.ctor()
extern void FieldMask__ctor_m01D7985ADE2286627A933B2F86237E0C8483D3D5 (void);
// 0x00000220 System.Void Google.Protobuf.WellKnownTypes.FieldMask::.ctor(Google.Protobuf.WellKnownTypes.FieldMask)
extern void FieldMask__ctor_m77703B996B4EAD8BD22AA0AC7E4C9C768137D853 (void);
// 0x00000221 Google.Protobuf.WellKnownTypes.FieldMask Google.Protobuf.WellKnownTypes.FieldMask::Clone()
extern void FieldMask_Clone_m2EDD32CF5E0AC140742096AEFE1DAE3ED453CCA3 (void);
// 0x00000222 Google.Protobuf.Collections.RepeatedField`1<System.String> Google.Protobuf.WellKnownTypes.FieldMask::get_Paths()
extern void FieldMask_get_Paths_mF5E927D6A5853483850A5B1B0A60F6EA4C6419EF (void);
// 0x00000223 System.Boolean Google.Protobuf.WellKnownTypes.FieldMask::Equals(System.Object)
extern void FieldMask_Equals_m1B5C027B9B0A1A66D8B177DAAB4EDBD5FD1442C1 (void);
// 0x00000224 System.Boolean Google.Protobuf.WellKnownTypes.FieldMask::Equals(Google.Protobuf.WellKnownTypes.FieldMask)
extern void FieldMask_Equals_mC23F829C421F954D073A6C046B614DBB687E41AB (void);
// 0x00000225 System.Int32 Google.Protobuf.WellKnownTypes.FieldMask::GetHashCode()
extern void FieldMask_GetHashCode_mA97105104D19E8CF467DE10EAFC05DC28B86F2D2 (void);
// 0x00000226 System.String Google.Protobuf.WellKnownTypes.FieldMask::ToString()
extern void FieldMask_ToString_m89F5E655ABCBA6ACC9D9B86BE367D9919A837036 (void);
// 0x00000227 System.Void Google.Protobuf.WellKnownTypes.FieldMask::WriteTo(Google.Protobuf.CodedOutputStream)
extern void FieldMask_WriteTo_mE9F97A05551D2F6FBCDF475C259D1BEE4B9ECA43 (void);
// 0x00000228 System.Void Google.Protobuf.WellKnownTypes.FieldMask::pb::Google.Protobuf.IBufferMessage.InternalWriteTo(Google.Protobuf.WriteContext&)
extern void FieldMask_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m663519CE13FFC51F40560EE1C564F4C017133C59 (void);
// 0x00000229 System.Int32 Google.Protobuf.WellKnownTypes.FieldMask::CalculateSize()
extern void FieldMask_CalculateSize_m0A39301CC699EEA0A57D8D8E4846C58F734D8EA5 (void);
// 0x0000022A System.Void Google.Protobuf.WellKnownTypes.FieldMask::MergeFrom(Google.Protobuf.WellKnownTypes.FieldMask)
extern void FieldMask_MergeFrom_mEAF05299B243344C2E5FFF1D601F5EA18BAB9F00 (void);
// 0x0000022B System.Void Google.Protobuf.WellKnownTypes.FieldMask::MergeFrom(Google.Protobuf.CodedInputStream)
extern void FieldMask_MergeFrom_mBB29E5E3787C88D4F6813175DF0F3D7E5F8E1E0C (void);
// 0x0000022C System.Void Google.Protobuf.WellKnownTypes.FieldMask::pb::Google.Protobuf.IBufferMessage.InternalMergeFrom(Google.Protobuf.ParseContext&)
extern void FieldMask_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_mA91B89E0519001C72B2CF796A4969D2D87CE04DB (void);
// 0x0000022D System.String Google.Protobuf.WellKnownTypes.FieldMask::ToJson(System.Collections.Generic.IList`1<System.String>,System.Boolean)
extern void FieldMask_ToJson_m7B14EB25AF30592117FF785EDAD96AFECB0ABB5D (void);
// 0x0000022E System.String Google.Protobuf.WellKnownTypes.FieldMask::ToDiagnosticString()
extern void FieldMask_ToDiagnosticString_mF6F76E2063C7124C4A7C83A930C71E6EE09BAE27 (void);
// 0x0000022F System.Boolean Google.Protobuf.WellKnownTypes.FieldMask::IsPathValid(System.String)
extern void FieldMask_IsPathValid_m6E09EDDDA021553F4AFACCE1034F616DCDFE92EC (void);
// 0x00000230 System.Void Google.Protobuf.WellKnownTypes.FieldMask::.cctor()
extern void FieldMask__cctor_mB5A9DE16B215D884D58AF3EDA652FC5573BD2F99 (void);
// 0x00000231 System.Void Google.Protobuf.WellKnownTypes.FieldMask/<>c::.cctor()
extern void U3CU3Ec__cctor_mA4CF16058DFB4D2F9A57E170E1B3147351F91F3F (void);
// 0x00000232 System.Void Google.Protobuf.WellKnownTypes.FieldMask/<>c::.ctor()
extern void U3CU3Ec__ctor_mC3697FB080C4E30E48E773923788967EFA424DD5 (void);
// 0x00000233 System.Boolean Google.Protobuf.WellKnownTypes.FieldMask/<>c::<ToJson>b__29_0(System.String)
extern void U3CU3Ec_U3CToJsonU3Eb__29_0_m2073565A3A39EBE2AEDE128834375CCF6657C339 (void);
// 0x00000234 Google.Protobuf.WellKnownTypes.FieldMask Google.Protobuf.WellKnownTypes.FieldMask/<>c::<.cctor>b__47_0()
extern void U3CU3Ec_U3C_cctorU3Eb__47_0_mA4E7E39FB01ECA1205A724B224B541D316975F0F (void);
// 0x00000235 Google.Protobuf.Reflection.FileDescriptor Google.Protobuf.WellKnownTypes.SourceContextReflection::get_Descriptor()
extern void SourceContextReflection_get_Descriptor_m4009946C4A2FF4A97821E806EFB14F3B68EC82A4 (void);
// 0x00000236 System.Void Google.Protobuf.WellKnownTypes.SourceContextReflection::.cctor()
extern void SourceContextReflection__cctor_m7E875D9DD71E34023DDA5F3D2A6922F0E4B2C4E6 (void);
// 0x00000237 Google.Protobuf.MessageParser`1<Google.Protobuf.WellKnownTypes.SourceContext> Google.Protobuf.WellKnownTypes.SourceContext::get_Parser()
extern void SourceContext_get_Parser_m1574934F37EB77DFE5197AFB3579F5180374C54F (void);
// 0x00000238 Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.WellKnownTypes.SourceContext::get_Descriptor()
extern void SourceContext_get_Descriptor_mD96EC1A495DB470D7206A021731449612F0B7700 (void);
// 0x00000239 Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.WellKnownTypes.SourceContext::pb::Google.Protobuf.IMessage.get_Descriptor()
extern void SourceContext_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m533FF400CC76FC6D0972E7AB266DCF102BF07F99 (void);
// 0x0000023A System.Void Google.Protobuf.WellKnownTypes.SourceContext::.ctor()
extern void SourceContext__ctor_m7DC6A0CEA2DB342A90B3111F54EE7FE20035ED7A (void);
// 0x0000023B System.Void Google.Protobuf.WellKnownTypes.SourceContext::.ctor(Google.Protobuf.WellKnownTypes.SourceContext)
extern void SourceContext__ctor_m53B1A691B9A243427E519584FAA277F82940830C (void);
// 0x0000023C Google.Protobuf.WellKnownTypes.SourceContext Google.Protobuf.WellKnownTypes.SourceContext::Clone()
extern void SourceContext_Clone_mECE4CC09F0CFD276692AED613523558C5B35FF13 (void);
// 0x0000023D System.String Google.Protobuf.WellKnownTypes.SourceContext::get_FileName()
extern void SourceContext_get_FileName_m7955978D6CF0C518E8E4AB97CCF95E667224F9BF (void);
// 0x0000023E System.Void Google.Protobuf.WellKnownTypes.SourceContext::set_FileName(System.String)
extern void SourceContext_set_FileName_m5E18A613284370873EB32929CFDBBBC9A9FF250A (void);
// 0x0000023F System.Boolean Google.Protobuf.WellKnownTypes.SourceContext::Equals(System.Object)
extern void SourceContext_Equals_m2404E1B68FD460455E54DD5DA372BA7DD10A56A5 (void);
// 0x00000240 System.Boolean Google.Protobuf.WellKnownTypes.SourceContext::Equals(Google.Protobuf.WellKnownTypes.SourceContext)
extern void SourceContext_Equals_mDA27BD7606F7EA489A2C1AE54476ED5E0E357143 (void);
// 0x00000241 System.Int32 Google.Protobuf.WellKnownTypes.SourceContext::GetHashCode()
extern void SourceContext_GetHashCode_m3AF81E3DD69DB21BF3B9DB93411901F8F0C4C4D9 (void);
// 0x00000242 System.String Google.Protobuf.WellKnownTypes.SourceContext::ToString()
extern void SourceContext_ToString_mCC00619AD401EE2827C8F568E6798478F398F6F1 (void);
// 0x00000243 System.Void Google.Protobuf.WellKnownTypes.SourceContext::WriteTo(Google.Protobuf.CodedOutputStream)
extern void SourceContext_WriteTo_m29132C1DE0B5144E19D8889A192E79AEDDABA8ED (void);
// 0x00000244 System.Void Google.Protobuf.WellKnownTypes.SourceContext::pb::Google.Protobuf.IBufferMessage.InternalWriteTo(Google.Protobuf.WriteContext&)
extern void SourceContext_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m2C30589A2E794D1D6F5D64B6DC35C253E7261066 (void);
// 0x00000245 System.Int32 Google.Protobuf.WellKnownTypes.SourceContext::CalculateSize()
extern void SourceContext_CalculateSize_mC5AD31CF54346C37E07C001C6A2BD8DCEC68F1F6 (void);
// 0x00000246 System.Void Google.Protobuf.WellKnownTypes.SourceContext::MergeFrom(Google.Protobuf.WellKnownTypes.SourceContext)
extern void SourceContext_MergeFrom_m02826B16AA69141109E52E02757E9B1AF009ACD4 (void);
// 0x00000247 System.Void Google.Protobuf.WellKnownTypes.SourceContext::MergeFrom(Google.Protobuf.CodedInputStream)
extern void SourceContext_MergeFrom_m052D6EAFE9072262116999725700C7FF8FCB45C1 (void);
// 0x00000248 System.Void Google.Protobuf.WellKnownTypes.SourceContext::pb::Google.Protobuf.IBufferMessage.InternalMergeFrom(Google.Protobuf.ParseContext&)
extern void SourceContext_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_mADDDB228A85C7C3C457347DC2D13075031FD4271 (void);
// 0x00000249 System.Void Google.Protobuf.WellKnownTypes.SourceContext::.cctor()
extern void SourceContext__cctor_mD983F610D248578C99A11563DDDBE4D2940DE730 (void);
// 0x0000024A System.Void Google.Protobuf.WellKnownTypes.SourceContext/<>c::.cctor()
extern void U3CU3Ec__cctor_m3D6899B50C7A5F6C071FAE8BD11101C3D0D070A7 (void);
// 0x0000024B System.Void Google.Protobuf.WellKnownTypes.SourceContext/<>c::.ctor()
extern void U3CU3Ec__ctor_mDD9A1C66DBE28C23EC3A96BFE489549611D7C29E (void);
// 0x0000024C Google.Protobuf.WellKnownTypes.SourceContext Google.Protobuf.WellKnownTypes.SourceContext/<>c::<.cctor>b__27_0()
extern void U3CU3Ec_U3C_cctorU3Eb__27_0_m06320485F3F600B4D8A2B285584AB45C2A582B41 (void);
// 0x0000024D Google.Protobuf.Reflection.FileDescriptor Google.Protobuf.WellKnownTypes.StructReflection::get_Descriptor()
extern void StructReflection_get_Descriptor_m9748598A8A6401A3A22220952736699A6A66816D (void);
// 0x0000024E System.Void Google.Protobuf.WellKnownTypes.StructReflection::.cctor()
extern void StructReflection__cctor_m6F9759FF12CAB6290E72D0996F40CB7A3FAF93FB (void);
// 0x0000024F Google.Protobuf.MessageParser`1<Google.Protobuf.WellKnownTypes.Struct> Google.Protobuf.WellKnownTypes.Struct::get_Parser()
extern void Struct_get_Parser_m250728F6C5651FF2FA35C5FE72A4B83E25B2076B (void);
// 0x00000250 Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.WellKnownTypes.Struct::get_Descriptor()
extern void Struct_get_Descriptor_mAC89E1D144BC3DF0BD50971C23664642ACFD7225 (void);
// 0x00000251 Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.WellKnownTypes.Struct::pb::Google.Protobuf.IMessage.get_Descriptor()
extern void Struct_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_mAF8AA7343F0F82ADAFC28730687E6707C98252AD (void);
// 0x00000252 System.Void Google.Protobuf.WellKnownTypes.Struct::.ctor()
extern void Struct__ctor_m11D17B249FA390528B0E14F386ADA2577C6A871C (void);
// 0x00000253 System.Void Google.Protobuf.WellKnownTypes.Struct::.ctor(Google.Protobuf.WellKnownTypes.Struct)
extern void Struct__ctor_m608179184571F7E0C2F5879E37CF80A00B2B8D7A (void);
// 0x00000254 Google.Protobuf.WellKnownTypes.Struct Google.Protobuf.WellKnownTypes.Struct::Clone()
extern void Struct_Clone_m0D1BB90781C14E60D625538A9A19D696120475BE (void);
// 0x00000255 Google.Protobuf.Collections.MapField`2<System.String,Google.Protobuf.WellKnownTypes.Value> Google.Protobuf.WellKnownTypes.Struct::get_Fields()
extern void Struct_get_Fields_m8D843586CA5DDAE522D036C881C8E28AFCD591FD (void);
// 0x00000256 System.Boolean Google.Protobuf.WellKnownTypes.Struct::Equals(System.Object)
extern void Struct_Equals_m549285D67C027225573A1DB67C5301A0D6454BFA (void);
// 0x00000257 System.Boolean Google.Protobuf.WellKnownTypes.Struct::Equals(Google.Protobuf.WellKnownTypes.Struct)
extern void Struct_Equals_m2532A70D51E9BAB2D6D51B68E969EF10BDA2F728 (void);
// 0x00000258 System.Int32 Google.Protobuf.WellKnownTypes.Struct::GetHashCode()
extern void Struct_GetHashCode_mB35A1295AA470E34F0B3283C926A85E1DC10C61E (void);
// 0x00000259 System.String Google.Protobuf.WellKnownTypes.Struct::ToString()
extern void Struct_ToString_mB7E3408DD1CC7EA7EF64A6C0AAF69344623328B9 (void);
// 0x0000025A System.Void Google.Protobuf.WellKnownTypes.Struct::WriteTo(Google.Protobuf.CodedOutputStream)
extern void Struct_WriteTo_mB8C1FE19A7AD7FA903647D06FF0DCF1970A32D29 (void);
// 0x0000025B System.Void Google.Protobuf.WellKnownTypes.Struct::pb::Google.Protobuf.IBufferMessage.InternalWriteTo(Google.Protobuf.WriteContext&)
extern void Struct_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_mB4878B03C7F2037F3A6A0B017A57F95D785F0B7F (void);
// 0x0000025C System.Int32 Google.Protobuf.WellKnownTypes.Struct::CalculateSize()
extern void Struct_CalculateSize_m04764057A3243E48A65E84C497924FE3AE6BEDDC (void);
// 0x0000025D System.Void Google.Protobuf.WellKnownTypes.Struct::MergeFrom(Google.Protobuf.WellKnownTypes.Struct)
extern void Struct_MergeFrom_m6AA74D28AA5FDA2211BE8FB37B3E8709136F90B1 (void);
// 0x0000025E System.Void Google.Protobuf.WellKnownTypes.Struct::MergeFrom(Google.Protobuf.CodedInputStream)
extern void Struct_MergeFrom_m6DA60767E2F54627C88F498B3F3D2348F0908D17 (void);
// 0x0000025F System.Void Google.Protobuf.WellKnownTypes.Struct::pb::Google.Protobuf.IBufferMessage.InternalMergeFrom(Google.Protobuf.ParseContext&)
extern void Struct_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m48DE2FD28260E4F6EB713D4B2309D7ECC5A3DCAF (void);
// 0x00000260 System.Void Google.Protobuf.WellKnownTypes.Struct::.cctor()
extern void Struct__cctor_m0E60641DC488D27979DE826B0D5CB378FB70E69C (void);
// 0x00000261 System.Void Google.Protobuf.WellKnownTypes.Struct/<>c::.cctor()
extern void U3CU3Ec__cctor_mC1AD226A66AB425A66E1587BE428E136B50C8037 (void);
// 0x00000262 System.Void Google.Protobuf.WellKnownTypes.Struct/<>c::.ctor()
extern void U3CU3Ec__ctor_mB1D8AC612C944EB09A92F8DFEF18F8B4D614795E (void);
// 0x00000263 Google.Protobuf.WellKnownTypes.Struct Google.Protobuf.WellKnownTypes.Struct/<>c::<.cctor>b__27_0()
extern void U3CU3Ec_U3C_cctorU3Eb__27_0_m23EDA83D004C7EE7CDBBBF148315FB5ED05C3A99 (void);
// 0x00000264 Google.Protobuf.MessageParser`1<Google.Protobuf.WellKnownTypes.Value> Google.Protobuf.WellKnownTypes.Value::get_Parser()
extern void Value_get_Parser_m7B68E45F03000960D21749A4F65984B745CF0C8F (void);
// 0x00000265 Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.WellKnownTypes.Value::get_Descriptor()
extern void Value_get_Descriptor_m60C564DCB4C1B6CD4CE32F32BE60C3353080E5C3 (void);
// 0x00000266 Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.WellKnownTypes.Value::pb::Google.Protobuf.IMessage.get_Descriptor()
extern void Value_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m41249C534B0CE2FB46D2B174A3F694661FE5FB27 (void);
// 0x00000267 System.Void Google.Protobuf.WellKnownTypes.Value::.ctor()
extern void Value__ctor_mB36B9A9D61FC019A45BC871BA1478BB50D60E78B (void);
// 0x00000268 System.Void Google.Protobuf.WellKnownTypes.Value::.ctor(Google.Protobuf.WellKnownTypes.Value)
extern void Value__ctor_m44F71F1C4FF2ED09299528B51BDBA4E73788D40E (void);
// 0x00000269 Google.Protobuf.WellKnownTypes.Value Google.Protobuf.WellKnownTypes.Value::Clone()
extern void Value_Clone_mACDC4288492E8D9B24FE707A400B1445AD78477F (void);
// 0x0000026A Google.Protobuf.WellKnownTypes.NullValue Google.Protobuf.WellKnownTypes.Value::get_NullValue()
extern void Value_get_NullValue_m33417C21F46BA869D3CB5DA6E493E3E5E1E3D3C1 (void);
// 0x0000026B System.Void Google.Protobuf.WellKnownTypes.Value::set_NullValue(Google.Protobuf.WellKnownTypes.NullValue)
extern void Value_set_NullValue_mDF8668260C9D9D9D92DEBBA19243B502592D2C29 (void);
// 0x0000026C System.Double Google.Protobuf.WellKnownTypes.Value::get_NumberValue()
extern void Value_get_NumberValue_mDB3E2517F4811344E4D3DB5A76F7BEBC614A36DD (void);
// 0x0000026D System.Void Google.Protobuf.WellKnownTypes.Value::set_NumberValue(System.Double)
extern void Value_set_NumberValue_m53F97D99BE56BDFBBA550A9184E331887C8486DB (void);
// 0x0000026E System.String Google.Protobuf.WellKnownTypes.Value::get_StringValue()
extern void Value_get_StringValue_mFA3132FC715755E37C15FE78B3DD7D0CD57BF3A1 (void);
// 0x0000026F System.Void Google.Protobuf.WellKnownTypes.Value::set_StringValue(System.String)
extern void Value_set_StringValue_mD9494834E589A83005BBF23FE2CCAD73404792A9 (void);
// 0x00000270 System.Boolean Google.Protobuf.WellKnownTypes.Value::get_BoolValue()
extern void Value_get_BoolValue_mBCE7513492A5A9F3468F567A3D8D86354DEBB530 (void);
// 0x00000271 System.Void Google.Protobuf.WellKnownTypes.Value::set_BoolValue(System.Boolean)
extern void Value_set_BoolValue_m5C00C8E2B41399FB01CE052AE60F2BD412A0383C (void);
// 0x00000272 Google.Protobuf.WellKnownTypes.Struct Google.Protobuf.WellKnownTypes.Value::get_StructValue()
extern void Value_get_StructValue_m76244616B9DEDEDD4B418AB47BA65E0747C9A3FD (void);
// 0x00000273 System.Void Google.Protobuf.WellKnownTypes.Value::set_StructValue(Google.Protobuf.WellKnownTypes.Struct)
extern void Value_set_StructValue_mA58EBA2260E35A3750A339B58D5EFB04A4360A81 (void);
// 0x00000274 Google.Protobuf.WellKnownTypes.ListValue Google.Protobuf.WellKnownTypes.Value::get_ListValue()
extern void Value_get_ListValue_m7BE63C1AA6FA57D40493647A0B07A8B2E7D63F17 (void);
// 0x00000275 System.Void Google.Protobuf.WellKnownTypes.Value::set_ListValue(Google.Protobuf.WellKnownTypes.ListValue)
extern void Value_set_ListValue_mB3FDD1F950984E0E98B0D2597E1FED0BB4D357AD (void);
// 0x00000276 Google.Protobuf.WellKnownTypes.Value/KindOneofCase Google.Protobuf.WellKnownTypes.Value::get_KindCase()
extern void Value_get_KindCase_m1C58B2C15BF26E2CF7D9FF1F5AF4379AACAD2168 (void);
// 0x00000277 System.Boolean Google.Protobuf.WellKnownTypes.Value::Equals(System.Object)
extern void Value_Equals_mE007E6C81CFC39B3A5BA78873297DB9C2305C650 (void);
// 0x00000278 System.Boolean Google.Protobuf.WellKnownTypes.Value::Equals(Google.Protobuf.WellKnownTypes.Value)
extern void Value_Equals_m5F2BB40CD83A5988F958848F4410366EB7EC59B4 (void);
// 0x00000279 System.Int32 Google.Protobuf.WellKnownTypes.Value::GetHashCode()
extern void Value_GetHashCode_m7C544251FD75746E81866D5D36924010535124E6 (void);
// 0x0000027A System.String Google.Protobuf.WellKnownTypes.Value::ToString()
extern void Value_ToString_m0435FF1515A70A900374F24375010C41124BFFC1 (void);
// 0x0000027B System.Void Google.Protobuf.WellKnownTypes.Value::WriteTo(Google.Protobuf.CodedOutputStream)
extern void Value_WriteTo_m707E9B65D4E8B28CA233D2CC91203558071F8147 (void);
// 0x0000027C System.Void Google.Protobuf.WellKnownTypes.Value::pb::Google.Protobuf.IBufferMessage.InternalWriteTo(Google.Protobuf.WriteContext&)
extern void Value_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m8F8E38353725C50938560EC4F37B5F22C75BA7FC (void);
// 0x0000027D System.Int32 Google.Protobuf.WellKnownTypes.Value::CalculateSize()
extern void Value_CalculateSize_m0249098B9D7B5CB6074121B23B55E79FFB7FC9BA (void);
// 0x0000027E System.Void Google.Protobuf.WellKnownTypes.Value::MergeFrom(Google.Protobuf.WellKnownTypes.Value)
extern void Value_MergeFrom_mFFA75CAC017BBD75F8C0E116E299BDAC427EF1C5 (void);
// 0x0000027F System.Void Google.Protobuf.WellKnownTypes.Value::MergeFrom(Google.Protobuf.CodedInputStream)
extern void Value_MergeFrom_mDFF91157418DCFA0541BAC4E77C0D8E24CCDC62B (void);
// 0x00000280 System.Void Google.Protobuf.WellKnownTypes.Value::pb::Google.Protobuf.IBufferMessage.InternalMergeFrom(Google.Protobuf.ParseContext&)
extern void Value_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_mE15A6602E4EDEAB62505CC0E7AD868CBD7CDE78C (void);
// 0x00000281 System.Void Google.Protobuf.WellKnownTypes.Value::.cctor()
extern void Value__cctor_mB7C40B765A99BCB40610807AAA0F0FD626DCEA8F (void);
// 0x00000282 System.Void Google.Protobuf.WellKnownTypes.Value/<>c::.cctor()
extern void U3CU3Ec__cctor_m59B9CF586DF0FA99047815487D0C45215B9D2D4A (void);
// 0x00000283 System.Void Google.Protobuf.WellKnownTypes.Value/<>c::.ctor()
extern void U3CU3Ec__ctor_mE7FB7C6DB3D332EBFC708DFC047192998E989C77 (void);
// 0x00000284 Google.Protobuf.WellKnownTypes.Value Google.Protobuf.WellKnownTypes.Value/<>c::<.cctor>b__58_0()
extern void U3CU3Ec_U3C_cctorU3Eb__58_0_m6FD67B6209A03F37CF4376D60D84925808D9BC56 (void);
// 0x00000285 Google.Protobuf.MessageParser`1<Google.Protobuf.WellKnownTypes.ListValue> Google.Protobuf.WellKnownTypes.ListValue::get_Parser()
extern void ListValue_get_Parser_mF35481E4B4A79E9E57529C57D6CC78DE990229F2 (void);
// 0x00000286 Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.WellKnownTypes.ListValue::get_Descriptor()
extern void ListValue_get_Descriptor_mB366D6ABF5EFF98C61052A9690352D031A99D74B (void);
// 0x00000287 Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.WellKnownTypes.ListValue::pb::Google.Protobuf.IMessage.get_Descriptor()
extern void ListValue_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m9200CA4ED976F0B2E5AF6FC25C3FF5606F454789 (void);
// 0x00000288 System.Void Google.Protobuf.WellKnownTypes.ListValue::.ctor()
extern void ListValue__ctor_mB9FA676E88F0EA554534D3FFCBECA11AC459A22A (void);
// 0x00000289 System.Void Google.Protobuf.WellKnownTypes.ListValue::.ctor(Google.Protobuf.WellKnownTypes.ListValue)
extern void ListValue__ctor_m28B1D38DB2026931E319F255DEF52941857CE536 (void);
// 0x0000028A Google.Protobuf.WellKnownTypes.ListValue Google.Protobuf.WellKnownTypes.ListValue::Clone()
extern void ListValue_Clone_mAE7626FCDDF9C2C6BA65B98C47E483B8C84E800A (void);
// 0x0000028B System.Boolean Google.Protobuf.WellKnownTypes.ListValue::Equals(System.Object)
extern void ListValue_Equals_mF92FCAB22A5B02906FEFBA93776FDA4996C0B65B (void);
// 0x0000028C System.Boolean Google.Protobuf.WellKnownTypes.ListValue::Equals(Google.Protobuf.WellKnownTypes.ListValue)
extern void ListValue_Equals_mAFE9E0D31D3CFEE41502588A065E6B9ED7C0663F (void);
// 0x0000028D System.Int32 Google.Protobuf.WellKnownTypes.ListValue::GetHashCode()
extern void ListValue_GetHashCode_mBAB9D28DF3E73B510E816CFF5C20774383DDA53B (void);
// 0x0000028E System.String Google.Protobuf.WellKnownTypes.ListValue::ToString()
extern void ListValue_ToString_mE555FF95A4A538FD306A7DBD94DA019BCC737EDB (void);
// 0x0000028F System.Void Google.Protobuf.WellKnownTypes.ListValue::WriteTo(Google.Protobuf.CodedOutputStream)
extern void ListValue_WriteTo_m2200555A25434B19BE50039FFD72B23E104A4A11 (void);
// 0x00000290 System.Void Google.Protobuf.WellKnownTypes.ListValue::pb::Google.Protobuf.IBufferMessage.InternalWriteTo(Google.Protobuf.WriteContext&)
extern void ListValue_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_mC52AE62570621C7C4CA377BD233B78BCBC0178FE (void);
// 0x00000291 System.Int32 Google.Protobuf.WellKnownTypes.ListValue::CalculateSize()
extern void ListValue_CalculateSize_m997191D513733E472C8705995F87C8119B6FDADC (void);
// 0x00000292 System.Void Google.Protobuf.WellKnownTypes.ListValue::MergeFrom(Google.Protobuf.WellKnownTypes.ListValue)
extern void ListValue_MergeFrom_mFD20BFF52DA1BFB3F54CC90BE5674A118FC5177B (void);
// 0x00000293 System.Void Google.Protobuf.WellKnownTypes.ListValue::MergeFrom(Google.Protobuf.CodedInputStream)
extern void ListValue_MergeFrom_m384866A7A963FFBF044B0FC082ADD6A9AA6A4CE2 (void);
// 0x00000294 System.Void Google.Protobuf.WellKnownTypes.ListValue::pb::Google.Protobuf.IBufferMessage.InternalMergeFrom(Google.Protobuf.ParseContext&)
extern void ListValue_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m5E6C1BE581E812B32831F486A67887D377AE6EED (void);
// 0x00000295 System.Void Google.Protobuf.WellKnownTypes.ListValue::.cctor()
extern void ListValue__cctor_m0E41B102D22E607B1EA5C7CA55F6E3041002DE90 (void);
// 0x00000296 System.Void Google.Protobuf.WellKnownTypes.ListValue/<>c::.cctor()
extern void U3CU3Ec__cctor_mECD0F9D51B179FB8BC3BCB8F49238DDE6F0BAA53 (void);
// 0x00000297 System.Void Google.Protobuf.WellKnownTypes.ListValue/<>c::.ctor()
extern void U3CU3Ec__ctor_mBA74624BCF703F8B4693D839AD66466AD76A5CDE (void);
// 0x00000298 Google.Protobuf.WellKnownTypes.ListValue Google.Protobuf.WellKnownTypes.ListValue/<>c::<.cctor>b__27_0()
extern void U3CU3Ec_U3C_cctorU3Eb__27_0_m505FD8C4C78D9B6614DCD355F93C53E994627CFB (void);
// 0x00000299 Google.Protobuf.Reflection.FileDescriptor Google.Protobuf.WellKnownTypes.TimestampReflection::get_Descriptor()
extern void TimestampReflection_get_Descriptor_m20CC9712B17B7939EDE1079E357BC1DCA4915837 (void);
// 0x0000029A System.Void Google.Protobuf.WellKnownTypes.TimestampReflection::.cctor()
extern void TimestampReflection__cctor_m34CFC760B3D2D93C368E2391968195E98FBEACDF (void);
// 0x0000029B Google.Protobuf.MessageParser`1<Google.Protobuf.WellKnownTypes.Timestamp> Google.Protobuf.WellKnownTypes.Timestamp::get_Parser()
extern void Timestamp_get_Parser_mEB68C1AFD68769CCF297A05506CDA0D3FB5E3574 (void);
// 0x0000029C Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.WellKnownTypes.Timestamp::get_Descriptor()
extern void Timestamp_get_Descriptor_mC252E84951D63F4CB2F1A95863C073EC537EFF47 (void);
// 0x0000029D Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.WellKnownTypes.Timestamp::pb::Google.Protobuf.IMessage.get_Descriptor()
extern void Timestamp_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_mC49EAA8BC8FD246B54506B483EE810FD9441CE11 (void);
// 0x0000029E System.Void Google.Protobuf.WellKnownTypes.Timestamp::.ctor()
extern void Timestamp__ctor_m8F321B7F87FD2782A209D5251E2509121F91E084 (void);
// 0x0000029F System.Void Google.Protobuf.WellKnownTypes.Timestamp::.ctor(Google.Protobuf.WellKnownTypes.Timestamp)
extern void Timestamp__ctor_m2664E78CE259B3480E632DA374D7295D9511E644 (void);
// 0x000002A0 Google.Protobuf.WellKnownTypes.Timestamp Google.Protobuf.WellKnownTypes.Timestamp::Clone()
extern void Timestamp_Clone_mBF7E47B82DDCD226CD188726A48B380D7340F063 (void);
// 0x000002A1 System.Int64 Google.Protobuf.WellKnownTypes.Timestamp::get_Seconds()
extern void Timestamp_get_Seconds_m5DDC0421C4D27B8326BE43F633784C49EB9AD20B (void);
// 0x000002A2 System.Void Google.Protobuf.WellKnownTypes.Timestamp::set_Seconds(System.Int64)
extern void Timestamp_set_Seconds_mCD77AE40724D2522C2658BFB55DD79BCB159F6D2 (void);
// 0x000002A3 System.Int32 Google.Protobuf.WellKnownTypes.Timestamp::get_Nanos()
extern void Timestamp_get_Nanos_m9E3C3DC947E927EA42D3301BB019081E9A8C7306 (void);
// 0x000002A4 System.Void Google.Protobuf.WellKnownTypes.Timestamp::set_Nanos(System.Int32)
extern void Timestamp_set_Nanos_mB8E86307F7E57CC2A2EBBD2E8EC2766DE9F36D3D (void);
// 0x000002A5 System.Boolean Google.Protobuf.WellKnownTypes.Timestamp::Equals(System.Object)
extern void Timestamp_Equals_m43A5485ADA83370447C6ED05E83D93E9FD61F075 (void);
// 0x000002A6 System.Boolean Google.Protobuf.WellKnownTypes.Timestamp::Equals(Google.Protobuf.WellKnownTypes.Timestamp)
extern void Timestamp_Equals_m09CB0CAAD41410377A04A2A6B6F93E232A2FD558 (void);
// 0x000002A7 System.Int32 Google.Protobuf.WellKnownTypes.Timestamp::GetHashCode()
extern void Timestamp_GetHashCode_m4EF62C49B8D0D43BDCDF3AE4115D63442DCB6AF7 (void);
// 0x000002A8 System.String Google.Protobuf.WellKnownTypes.Timestamp::ToString()
extern void Timestamp_ToString_m54CE7B61A60BD4FBE9FA386E69B256FD9CAB0766 (void);
// 0x000002A9 System.Void Google.Protobuf.WellKnownTypes.Timestamp::WriteTo(Google.Protobuf.CodedOutputStream)
extern void Timestamp_WriteTo_m7A41B3EBAEDF1DB8EBA83393E27C1D47642EF1E6 (void);
// 0x000002AA System.Void Google.Protobuf.WellKnownTypes.Timestamp::pb::Google.Protobuf.IBufferMessage.InternalWriteTo(Google.Protobuf.WriteContext&)
extern void Timestamp_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m931F0E47EBF9936D807F0165935BE4D7B4DD6D33 (void);
// 0x000002AB System.Int32 Google.Protobuf.WellKnownTypes.Timestamp::CalculateSize()
extern void Timestamp_CalculateSize_mB8AA31386FAA6594A82EBFDA83023F01849EBA42 (void);
// 0x000002AC System.Void Google.Protobuf.WellKnownTypes.Timestamp::MergeFrom(Google.Protobuf.WellKnownTypes.Timestamp)
extern void Timestamp_MergeFrom_mB073280D4CC7A8E531F5BE75B1390589537790A4 (void);
// 0x000002AD System.Void Google.Protobuf.WellKnownTypes.Timestamp::MergeFrom(Google.Protobuf.CodedInputStream)
extern void Timestamp_MergeFrom_m47E1E135BC450678854DD2704A26607F69516BBF (void);
// 0x000002AE System.Void Google.Protobuf.WellKnownTypes.Timestamp::pb::Google.Protobuf.IBufferMessage.InternalMergeFrom(Google.Protobuf.ParseContext&)
extern void Timestamp_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m667DF5C1AC94559C80FE08DC714B1E2943D6C16D (void);
// 0x000002AF System.Boolean Google.Protobuf.WellKnownTypes.Timestamp::IsNormalized(System.Int64,System.Int32)
extern void Timestamp_IsNormalized_m4D15C2D1F00FDDAA057FA921F0C2C746E8FB3D0C (void);
// 0x000002B0 System.String Google.Protobuf.WellKnownTypes.Timestamp::ToJson(System.Int64,System.Int32,System.Boolean)
extern void Timestamp_ToJson_m2DB90513A2B4A7A8D917CA8AE9AE619A77B4D366 (void);
// 0x000002B1 System.Int32 Google.Protobuf.WellKnownTypes.Timestamp::CompareTo(Google.Protobuf.WellKnownTypes.Timestamp)
extern void Timestamp_CompareTo_m2C4306ADBFD6885786F776FE9115C6AE6A099223 (void);
// 0x000002B2 System.Boolean Google.Protobuf.WellKnownTypes.Timestamp::op_Equality(Google.Protobuf.WellKnownTypes.Timestamp,Google.Protobuf.WellKnownTypes.Timestamp)
extern void Timestamp_op_Equality_m919F45BB63A456B53460149D13F67F6F663E2767 (void);
// 0x000002B3 System.String Google.Protobuf.WellKnownTypes.Timestamp::ToDiagnosticString()
extern void Timestamp_ToDiagnosticString_mCF6DE1C0C81B5A880A9D2FCFEE9BA533253765B9 (void);
// 0x000002B4 System.Void Google.Protobuf.WellKnownTypes.Timestamp::.cctor()
extern void Timestamp__cctor_mE1E881B2E3510FE2BD42514770885507578A4689 (void);
// 0x000002B5 System.Void Google.Protobuf.WellKnownTypes.Timestamp/<>c::.cctor()
extern void U3CU3Ec__cctor_mC6E4A227DA90B6F08D288C8836C1EFF4ED6E3C3C (void);
// 0x000002B6 System.Void Google.Protobuf.WellKnownTypes.Timestamp/<>c::.ctor()
extern void U3CU3Ec__ctor_mA9A64A19A1BDD2B0BD230CE1FCB3BA203A732FE8 (void);
// 0x000002B7 Google.Protobuf.WellKnownTypes.Timestamp Google.Protobuf.WellKnownTypes.Timestamp/<>c::<.cctor>b__55_0()
extern void U3CU3Ec_U3C_cctorU3Eb__55_0_m6171D84FAC3F3F77F585EB62B3CEEE8F6E061AEC (void);
// 0x000002B8 Google.Protobuf.Reflection.FileDescriptor Google.Protobuf.WellKnownTypes.TypeReflection::get_Descriptor()
extern void TypeReflection_get_Descriptor_mDDBF6D9739EFCD93767D247825BAE30238FA760F (void);
// 0x000002B9 System.Void Google.Protobuf.WellKnownTypes.TypeReflection::.cctor()
extern void TypeReflection__cctor_mAC3FAA240EBDEAA68EF3BBD22A83FC2AB2ED6549 (void);
// 0x000002BA Google.Protobuf.MessageParser`1<Google.Protobuf.WellKnownTypes.Type> Google.Protobuf.WellKnownTypes.Type::get_Parser()
extern void Type_get_Parser_m8F54270ED12210981D9A59EDB8FECD375119340E (void);
// 0x000002BB Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.WellKnownTypes.Type::get_Descriptor()
extern void Type_get_Descriptor_m010BB83A3404B9825799F2A806CAE4F6F7ECC12A (void);
// 0x000002BC Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.WellKnownTypes.Type::pb::Google.Protobuf.IMessage.get_Descriptor()
extern void Type_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m67F408433B377CF6DDEFFAC42FF4C6D7E5936480 (void);
// 0x000002BD System.Void Google.Protobuf.WellKnownTypes.Type::.ctor()
extern void Type__ctor_m72B7DAE9051977572D45AFCF5B543AEB9E84A71F (void);
// 0x000002BE System.Void Google.Protobuf.WellKnownTypes.Type::.ctor(Google.Protobuf.WellKnownTypes.Type)
extern void Type__ctor_m7E6669C7E329257A31BA69B14587D6D7739C49DE (void);
// 0x000002BF Google.Protobuf.WellKnownTypes.Type Google.Protobuf.WellKnownTypes.Type::Clone()
extern void Type_Clone_m45297B0AFB39C2C32EDE75B17513877EAE97D1CC (void);
// 0x000002C0 System.String Google.Protobuf.WellKnownTypes.Type::get_Name()
extern void Type_get_Name_m1A7122FBF1A82ED55AE6925A636ED6A9030EC858 (void);
// 0x000002C1 System.Void Google.Protobuf.WellKnownTypes.Type::set_Name(System.String)
extern void Type_set_Name_m055734A2F94B05FA2084C169C5230EBD987092E5 (void);
// 0x000002C2 Google.Protobuf.WellKnownTypes.SourceContext Google.Protobuf.WellKnownTypes.Type::get_SourceContext()
extern void Type_get_SourceContext_m07B1EE8D911DC970ADBFBCCD9BDDF457CB5BB088 (void);
// 0x000002C3 System.Void Google.Protobuf.WellKnownTypes.Type::set_SourceContext(Google.Protobuf.WellKnownTypes.SourceContext)
extern void Type_set_SourceContext_m48BB05140D6C16D5A27BE26DE40F42C020796D57 (void);
// 0x000002C4 Google.Protobuf.WellKnownTypes.Syntax Google.Protobuf.WellKnownTypes.Type::get_Syntax()
extern void Type_get_Syntax_m3F1F54FABD4233B8D23876EAC71561C12DB84CD8 (void);
// 0x000002C5 System.Void Google.Protobuf.WellKnownTypes.Type::set_Syntax(Google.Protobuf.WellKnownTypes.Syntax)
extern void Type_set_Syntax_m86D67BE2C739303FF8A46891FB7692EC0D9DB068 (void);
// 0x000002C6 System.Boolean Google.Protobuf.WellKnownTypes.Type::Equals(System.Object)
extern void Type_Equals_mCC222586E6D1B96613944E3D66BEC72D362D9D63 (void);
// 0x000002C7 System.Boolean Google.Protobuf.WellKnownTypes.Type::Equals(Google.Protobuf.WellKnownTypes.Type)
extern void Type_Equals_mE4A16EB9E9DA3DD6BBCFD983D5E76A3B7EF7699B (void);
// 0x000002C8 System.Int32 Google.Protobuf.WellKnownTypes.Type::GetHashCode()
extern void Type_GetHashCode_m620763684AFC519635D83A5038D9874C8D840B43 (void);
// 0x000002C9 System.String Google.Protobuf.WellKnownTypes.Type::ToString()
extern void Type_ToString_m220D186C08F0D2510BEBAD9ADA9B2AFC9D9C7A2B (void);
// 0x000002CA System.Void Google.Protobuf.WellKnownTypes.Type::WriteTo(Google.Protobuf.CodedOutputStream)
extern void Type_WriteTo_mEAC9994F15ADD598116489C2F5DC26A213181E74 (void);
// 0x000002CB System.Void Google.Protobuf.WellKnownTypes.Type::pb::Google.Protobuf.IBufferMessage.InternalWriteTo(Google.Protobuf.WriteContext&)
extern void Type_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_mF4AB433048B17B8767AE79040EDF944BF398D8C4 (void);
// 0x000002CC System.Int32 Google.Protobuf.WellKnownTypes.Type::CalculateSize()
extern void Type_CalculateSize_m5AE90542655AB79BC6184ACB7A226E0D004371AA (void);
// 0x000002CD System.Void Google.Protobuf.WellKnownTypes.Type::MergeFrom(Google.Protobuf.WellKnownTypes.Type)
extern void Type_MergeFrom_m3128264DCA457EBEF76A421739ECF34A8244D3F3 (void);
// 0x000002CE System.Void Google.Protobuf.WellKnownTypes.Type::MergeFrom(Google.Protobuf.CodedInputStream)
extern void Type_MergeFrom_m71E0740B14181FDFFDA3FE9A92DE72D39471830D (void);
// 0x000002CF System.Void Google.Protobuf.WellKnownTypes.Type::pb::Google.Protobuf.IBufferMessage.InternalMergeFrom(Google.Protobuf.ParseContext&)
extern void Type_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_mEC3240A4B5DB410713B7697F2A15EA65B3C67EDB (void);
// 0x000002D0 System.Void Google.Protobuf.WellKnownTypes.Type::.cctor()
extern void Type__cctor_m9790E14F764F18462B2EEA0E074CB98EB7D962CE (void);
// 0x000002D1 System.Void Google.Protobuf.WellKnownTypes.Type/<>c::.cctor()
extern void U3CU3Ec__cctor_m29AE8A35B39754F4FC0EB5535A0AF62EF2A0CDF3 (void);
// 0x000002D2 System.Void Google.Protobuf.WellKnownTypes.Type/<>c::.ctor()
extern void U3CU3Ec__ctor_m2909DED14707E03A6BCAAFCC13683F51C6251537 (void);
// 0x000002D3 Google.Protobuf.WellKnownTypes.Type Google.Protobuf.WellKnownTypes.Type/<>c::<.cctor>b__52_0()
extern void U3CU3Ec_U3C_cctorU3Eb__52_0_mEB6B60B797080A26002C324ACB518D8345E8E0CB (void);
// 0x000002D4 Google.Protobuf.MessageParser`1<Google.Protobuf.WellKnownTypes.Field> Google.Protobuf.WellKnownTypes.Field::get_Parser()
extern void Field_get_Parser_m0878925481143310E7B3AA20B796B5A97E500CF2 (void);
// 0x000002D5 Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.WellKnownTypes.Field::get_Descriptor()
extern void Field_get_Descriptor_m531F22D378B22CF604660266902F8DF5C3D24C18 (void);
// 0x000002D6 Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.WellKnownTypes.Field::pb::Google.Protobuf.IMessage.get_Descriptor()
extern void Field_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m391F7C85EB83922F66C82433215878208DF9F2A9 (void);
// 0x000002D7 System.Void Google.Protobuf.WellKnownTypes.Field::.ctor()
extern void Field__ctor_m083271BB3DE4449286983AFE69B71C9DE2D68BE8 (void);
// 0x000002D8 System.Void Google.Protobuf.WellKnownTypes.Field::.ctor(Google.Protobuf.WellKnownTypes.Field)
extern void Field__ctor_mF445407AC817770B1DD11ED1FC30695C18F0B76B (void);
// 0x000002D9 Google.Protobuf.WellKnownTypes.Field Google.Protobuf.WellKnownTypes.Field::Clone()
extern void Field_Clone_m0CC33BAC52277DA85D019E632E4BEF45987DF7D7 (void);
// 0x000002DA Google.Protobuf.WellKnownTypes.Field/Types/Kind Google.Protobuf.WellKnownTypes.Field::get_Kind()
extern void Field_get_Kind_mD2F491FBD5BBD85968D52EBCB628569218D4BA60 (void);
// 0x000002DB System.Void Google.Protobuf.WellKnownTypes.Field::set_Kind(Google.Protobuf.WellKnownTypes.Field/Types/Kind)
extern void Field_set_Kind_m078446AC9EF3821A7F538B3BF32006B4A588DB69 (void);
// 0x000002DC Google.Protobuf.WellKnownTypes.Field/Types/Cardinality Google.Protobuf.WellKnownTypes.Field::get_Cardinality()
extern void Field_get_Cardinality_mF2A01FCE7EB6AD4890B2AD0DC697A43A86463DE2 (void);
// 0x000002DD System.Void Google.Protobuf.WellKnownTypes.Field::set_Cardinality(Google.Protobuf.WellKnownTypes.Field/Types/Cardinality)
extern void Field_set_Cardinality_m58FB9906ECC195083674F7FCD0A794339F42DFAF (void);
// 0x000002DE System.Int32 Google.Protobuf.WellKnownTypes.Field::get_Number()
extern void Field_get_Number_m5CA026EC04ACB1477820127E99B4352082BF716E (void);
// 0x000002DF System.Void Google.Protobuf.WellKnownTypes.Field::set_Number(System.Int32)
extern void Field_set_Number_mA3C7411AEA18DF70E16CAC6978C4DDA71B067D31 (void);
// 0x000002E0 System.String Google.Protobuf.WellKnownTypes.Field::get_Name()
extern void Field_get_Name_mAAD601EAA2275330987C83DFDAF0052AEC4CBEA1 (void);
// 0x000002E1 System.Void Google.Protobuf.WellKnownTypes.Field::set_Name(System.String)
extern void Field_set_Name_m33136B6931B53DF86103C46D793B6FCF9995FE0A (void);
// 0x000002E2 System.String Google.Protobuf.WellKnownTypes.Field::get_TypeUrl()
extern void Field_get_TypeUrl_m2528DB874C9BF6493DEB2660EF426461EB24B228 (void);
// 0x000002E3 System.Void Google.Protobuf.WellKnownTypes.Field::set_TypeUrl(System.String)
extern void Field_set_TypeUrl_mA737116E340BBD9BFAC30AE9B6F95055D9F56541 (void);
// 0x000002E4 System.Int32 Google.Protobuf.WellKnownTypes.Field::get_OneofIndex()
extern void Field_get_OneofIndex_m08967CEA183858F72EAFBCEF093F8253F0E67735 (void);
// 0x000002E5 System.Void Google.Protobuf.WellKnownTypes.Field::set_OneofIndex(System.Int32)
extern void Field_set_OneofIndex_mC794988564E6E6C198EDB5EE071792D7F39E79DA (void);
// 0x000002E6 System.Boolean Google.Protobuf.WellKnownTypes.Field::get_Packed()
extern void Field_get_Packed_m3FC4C3AF743AA04C3751A975B8753EE8BEB688C7 (void);
// 0x000002E7 System.Void Google.Protobuf.WellKnownTypes.Field::set_Packed(System.Boolean)
extern void Field_set_Packed_mC8EAB7B677B01394AB13FB37CC9C3FD1B4A7AC13 (void);
// 0x000002E8 System.String Google.Protobuf.WellKnownTypes.Field::get_JsonName()
extern void Field_get_JsonName_m44ACFC8356E38FB505DD382140938FDD14204562 (void);
// 0x000002E9 System.Void Google.Protobuf.WellKnownTypes.Field::set_JsonName(System.String)
extern void Field_set_JsonName_m2AC60C78FE3F665C34907371251489860497AB55 (void);
// 0x000002EA System.String Google.Protobuf.WellKnownTypes.Field::get_DefaultValue()
extern void Field_get_DefaultValue_mAE46BF6A0D3C4A39D237531D13D0B0AE33994EDE (void);
// 0x000002EB System.Void Google.Protobuf.WellKnownTypes.Field::set_DefaultValue(System.String)
extern void Field_set_DefaultValue_mA111AEFBED7BB95C17F50ED21F5F0CEFD66F2451 (void);
// 0x000002EC System.Boolean Google.Protobuf.WellKnownTypes.Field::Equals(System.Object)
extern void Field_Equals_m4954B931DEB49176652C38AC8B8047EB9CF2D083 (void);
// 0x000002ED System.Boolean Google.Protobuf.WellKnownTypes.Field::Equals(Google.Protobuf.WellKnownTypes.Field)
extern void Field_Equals_m98EF1C08EB6139CFAC12DDAF6587098D0965F8B8 (void);
// 0x000002EE System.Int32 Google.Protobuf.WellKnownTypes.Field::GetHashCode()
extern void Field_GetHashCode_m9920F826AB54CAC69212B120FA5196570B2EBF0F (void);
// 0x000002EF System.String Google.Protobuf.WellKnownTypes.Field::ToString()
extern void Field_ToString_mC417A2D84A15530EB338A880F26CAF5B899096D1 (void);
// 0x000002F0 System.Void Google.Protobuf.WellKnownTypes.Field::WriteTo(Google.Protobuf.CodedOutputStream)
extern void Field_WriteTo_mE97048ABCAD56F48771C6005B929D1D16822F5F9 (void);
// 0x000002F1 System.Void Google.Protobuf.WellKnownTypes.Field::pb::Google.Protobuf.IBufferMessage.InternalWriteTo(Google.Protobuf.WriteContext&)
extern void Field_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m1403C5B7A9323AAEEE2ECD2E2282CDADD1D3EBBA (void);
// 0x000002F2 System.Int32 Google.Protobuf.WellKnownTypes.Field::CalculateSize()
extern void Field_CalculateSize_m29958B0C393E23281EEBB33DE7F86329A78120EA (void);
// 0x000002F3 System.Void Google.Protobuf.WellKnownTypes.Field::MergeFrom(Google.Protobuf.WellKnownTypes.Field)
extern void Field_MergeFrom_m79C0F4297EB25A155F74B1D4FD88149786994F5A (void);
// 0x000002F4 System.Void Google.Protobuf.WellKnownTypes.Field::MergeFrom(Google.Protobuf.CodedInputStream)
extern void Field_MergeFrom_m9C1775ED4AE6363E980B965D39DE0651A2A30AA8 (void);
// 0x000002F5 System.Void Google.Protobuf.WellKnownTypes.Field::pb::Google.Protobuf.IBufferMessage.InternalMergeFrom(Google.Protobuf.ParseContext&)
extern void Field_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_mDCEE2693E502312C726C28AB4F137EAC8AB12640 (void);
// 0x000002F6 System.Void Google.Protobuf.WellKnownTypes.Field::.cctor()
extern void Field__cctor_mE41D2600BF86F2B33C8D4EE32BF1BE0531EC8C80 (void);
// 0x000002F7 System.Void Google.Protobuf.WellKnownTypes.Field/<>c::.cctor()
extern void U3CU3Ec__cctor_m242A2BE31487590FAB7F34251715E401DA720CB0 (void);
// 0x000002F8 System.Void Google.Protobuf.WellKnownTypes.Field/<>c::.ctor()
extern void U3CU3Ec__ctor_m2B6A49AD15FCA9833C2C58C9847F8B5BEDC4A6DF (void);
// 0x000002F9 Google.Protobuf.WellKnownTypes.Field Google.Protobuf.WellKnownTypes.Field/<>c::<.cctor>b__73_0()
extern void U3CU3Ec_U3C_cctorU3Eb__73_0_m9E0154A97E3AF3EC0FECB2A2355B3611E34D7C2D (void);
// 0x000002FA Google.Protobuf.MessageParser`1<Google.Protobuf.WellKnownTypes.Enum> Google.Protobuf.WellKnownTypes.Enum::get_Parser()
extern void Enum_get_Parser_m404249DEB9B89C3F7994A156561C602BBAA7EE39 (void);
// 0x000002FB Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.WellKnownTypes.Enum::get_Descriptor()
extern void Enum_get_Descriptor_m52EA724A27CEBC4D519A8045D6A60E8D58AF95F4 (void);
// 0x000002FC Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.WellKnownTypes.Enum::pb::Google.Protobuf.IMessage.get_Descriptor()
extern void Enum_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m201367CB1FBCA15E4572CEF2C01ED40BB772057D (void);
// 0x000002FD System.Void Google.Protobuf.WellKnownTypes.Enum::.ctor()
extern void Enum__ctor_m6E1C81B05018715B930662010FE77501F5394204 (void);
// 0x000002FE System.Void Google.Protobuf.WellKnownTypes.Enum::.ctor(Google.Protobuf.WellKnownTypes.Enum)
extern void Enum__ctor_m48DDA58AECBCB975A007B170E29CAB580FE5880F (void);
// 0x000002FF Google.Protobuf.WellKnownTypes.Enum Google.Protobuf.WellKnownTypes.Enum::Clone()
extern void Enum_Clone_mC8C340BFE917B4374A1D33C35CAC2FBFFB43D904 (void);
// 0x00000300 System.String Google.Protobuf.WellKnownTypes.Enum::get_Name()
extern void Enum_get_Name_m51F713652DB4E8F187FE773DB8583B0943968E33 (void);
// 0x00000301 System.Void Google.Protobuf.WellKnownTypes.Enum::set_Name(System.String)
extern void Enum_set_Name_m52AB363A89E9492ECC6AA4F717E8D0423FE43EB9 (void);
// 0x00000302 Google.Protobuf.WellKnownTypes.SourceContext Google.Protobuf.WellKnownTypes.Enum::get_SourceContext()
extern void Enum_get_SourceContext_mC4380A9389B7D17E7D814E376542931A6C39B92E (void);
// 0x00000303 System.Void Google.Protobuf.WellKnownTypes.Enum::set_SourceContext(Google.Protobuf.WellKnownTypes.SourceContext)
extern void Enum_set_SourceContext_m59B7D9BDEC030C7CE6C724788E800AA2187E0EB3 (void);
// 0x00000304 Google.Protobuf.WellKnownTypes.Syntax Google.Protobuf.WellKnownTypes.Enum::get_Syntax()
extern void Enum_get_Syntax_m892DFB901E063629BC13392D5629B2384D140D71 (void);
// 0x00000305 System.Void Google.Protobuf.WellKnownTypes.Enum::set_Syntax(Google.Protobuf.WellKnownTypes.Syntax)
extern void Enum_set_Syntax_m74BD39956DD611C4E90852AA2A5FD5E4C0B6ED2B (void);
// 0x00000306 System.Boolean Google.Protobuf.WellKnownTypes.Enum::Equals(System.Object)
extern void Enum_Equals_mC1F65CAC46C0ADE5A1D38C9FA48A3DED3D4DCE1C (void);
// 0x00000307 System.Boolean Google.Protobuf.WellKnownTypes.Enum::Equals(Google.Protobuf.WellKnownTypes.Enum)
extern void Enum_Equals_mFC4CEF2A5597BC4AABEC5B7CC79F18274CC8D7B6 (void);
// 0x00000308 System.Int32 Google.Protobuf.WellKnownTypes.Enum::GetHashCode()
extern void Enum_GetHashCode_m84A65EAEBAA7FDBE3579FB7E085A667AB6069380 (void);
// 0x00000309 System.String Google.Protobuf.WellKnownTypes.Enum::ToString()
extern void Enum_ToString_m8755890EAF8E3CBD38454AAEE50C219D06BC2AC1 (void);
// 0x0000030A System.Void Google.Protobuf.WellKnownTypes.Enum::WriteTo(Google.Protobuf.CodedOutputStream)
extern void Enum_WriteTo_m21A724F3E7465A22E8D2933F97B7AAEA97DDAAD1 (void);
// 0x0000030B System.Void Google.Protobuf.WellKnownTypes.Enum::pb::Google.Protobuf.IBufferMessage.InternalWriteTo(Google.Protobuf.WriteContext&)
extern void Enum_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m9E010AB642C27A55002C1798086F1126F5C28E91 (void);
// 0x0000030C System.Int32 Google.Protobuf.WellKnownTypes.Enum::CalculateSize()
extern void Enum_CalculateSize_m6AE4EEC93B797E6CF87A9C0F14F2DCE78D805A4A (void);
// 0x0000030D System.Void Google.Protobuf.WellKnownTypes.Enum::MergeFrom(Google.Protobuf.WellKnownTypes.Enum)
extern void Enum_MergeFrom_m02C3F847695806FAA2E6041572102F44B894C9D6 (void);
// 0x0000030E System.Void Google.Protobuf.WellKnownTypes.Enum::MergeFrom(Google.Protobuf.CodedInputStream)
extern void Enum_MergeFrom_mAC108FDF195D5796D71C464C308BEFE7F9BF0B51 (void);
// 0x0000030F System.Void Google.Protobuf.WellKnownTypes.Enum::pb::Google.Protobuf.IBufferMessage.InternalMergeFrom(Google.Protobuf.ParseContext&)
extern void Enum_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_mDFD4A7F6E813A69B629AFE079EFC31645ABC2881 (void);
// 0x00000310 System.Void Google.Protobuf.WellKnownTypes.Enum::.cctor()
extern void Enum__cctor_mA39ADC7A49D6F5DEC4B28EB6F8F6FFF862B0BE76 (void);
// 0x00000311 System.Void Google.Protobuf.WellKnownTypes.Enum/<>c::.cctor()
extern void U3CU3Ec__cctor_m25E0386AA478224EF99765AD3DEA945DEB12F2A6 (void);
// 0x00000312 System.Void Google.Protobuf.WellKnownTypes.Enum/<>c::.ctor()
extern void U3CU3Ec__ctor_mE1467AECF5DA674F2BBC40FFA42DD4CBC92C4DB5 (void);
// 0x00000313 Google.Protobuf.WellKnownTypes.Enum Google.Protobuf.WellKnownTypes.Enum/<>c::<.cctor>b__47_0()
extern void U3CU3Ec_U3C_cctorU3Eb__47_0_m1AB652809E5BEDE3E84F01DE4EE57976E8CB0886 (void);
// 0x00000314 Google.Protobuf.MessageParser`1<Google.Protobuf.WellKnownTypes.EnumValue> Google.Protobuf.WellKnownTypes.EnumValue::get_Parser()
extern void EnumValue_get_Parser_mCD86ABE02F0F9E530975BFB1DD8DF6DE0B2F066B (void);
// 0x00000315 Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.WellKnownTypes.EnumValue::get_Descriptor()
extern void EnumValue_get_Descriptor_m33825F769890F8E5B727EF64971606ADA8788EE6 (void);
// 0x00000316 Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.WellKnownTypes.EnumValue::pb::Google.Protobuf.IMessage.get_Descriptor()
extern void EnumValue_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m32E9B91C9B809E6EBBD26C5968A87371270B1A78 (void);
// 0x00000317 System.Void Google.Protobuf.WellKnownTypes.EnumValue::.ctor()
extern void EnumValue__ctor_m1F9880D30AB86CC9184A09C9244767AD02DBCDAB (void);
// 0x00000318 System.Void Google.Protobuf.WellKnownTypes.EnumValue::.ctor(Google.Protobuf.WellKnownTypes.EnumValue)
extern void EnumValue__ctor_mBEB46ABD1DA26AC98C6D2B3CC17B43F3FACE79CD (void);
// 0x00000319 Google.Protobuf.WellKnownTypes.EnumValue Google.Protobuf.WellKnownTypes.EnumValue::Clone()
extern void EnumValue_Clone_mAD4F136B74E13F29C57FD9BA516D5690D0D8A2E5 (void);
// 0x0000031A System.String Google.Protobuf.WellKnownTypes.EnumValue::get_Name()
extern void EnumValue_get_Name_m322894CCFF48C330198AE5F0F6A07B300FE90F91 (void);
// 0x0000031B System.Void Google.Protobuf.WellKnownTypes.EnumValue::set_Name(System.String)
extern void EnumValue_set_Name_m1F7DE39E39D66AD2E92C7789753BFBA22AAEA3A4 (void);
// 0x0000031C System.Int32 Google.Protobuf.WellKnownTypes.EnumValue::get_Number()
extern void EnumValue_get_Number_m8E0B1F149329F97C0F78F799E7390830389E7A44 (void);
// 0x0000031D System.Void Google.Protobuf.WellKnownTypes.EnumValue::set_Number(System.Int32)
extern void EnumValue_set_Number_m1B49E2FC906E4D2C639C38FAD0CC6AC06CB85F38 (void);
// 0x0000031E System.Boolean Google.Protobuf.WellKnownTypes.EnumValue::Equals(System.Object)
extern void EnumValue_Equals_mC7EAF672AF155B8A6E5E83A30AFA5C8C737549CD (void);
// 0x0000031F System.Boolean Google.Protobuf.WellKnownTypes.EnumValue::Equals(Google.Protobuf.WellKnownTypes.EnumValue)
extern void EnumValue_Equals_m300A77FE3BEA40D06F264421A4DF508A35FFFE42 (void);
// 0x00000320 System.Int32 Google.Protobuf.WellKnownTypes.EnumValue::GetHashCode()
extern void EnumValue_GetHashCode_m0A4617C5682F4BFD756D4E0027B99FB3C9916D0A (void);
// 0x00000321 System.String Google.Protobuf.WellKnownTypes.EnumValue::ToString()
extern void EnumValue_ToString_mE37319FEB422F63DAE1B920B3F47712CD57858D6 (void);
// 0x00000322 System.Void Google.Protobuf.WellKnownTypes.EnumValue::WriteTo(Google.Protobuf.CodedOutputStream)
extern void EnumValue_WriteTo_mAF6C20F5FC69A6D7FB9990941A81887DFC056FBA (void);
// 0x00000323 System.Void Google.Protobuf.WellKnownTypes.EnumValue::pb::Google.Protobuf.IBufferMessage.InternalWriteTo(Google.Protobuf.WriteContext&)
extern void EnumValue_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m2D246ECE495DC0034AACF00A7F7D7C749B605088 (void);
// 0x00000324 System.Int32 Google.Protobuf.WellKnownTypes.EnumValue::CalculateSize()
extern void EnumValue_CalculateSize_m4ED2A3BCDB5B45F73CEB8F384D1ABEEDA417A7CF (void);
// 0x00000325 System.Void Google.Protobuf.WellKnownTypes.EnumValue::MergeFrom(Google.Protobuf.WellKnownTypes.EnumValue)
extern void EnumValue_MergeFrom_m16FE96C76760F5E983CDC9DA7E9E6608E718AB2B (void);
// 0x00000326 System.Void Google.Protobuf.WellKnownTypes.EnumValue::MergeFrom(Google.Protobuf.CodedInputStream)
extern void EnumValue_MergeFrom_mE6B7795A557E42BECB42E02D36B871A5AF3F6F2A (void);
// 0x00000327 System.Void Google.Protobuf.WellKnownTypes.EnumValue::pb::Google.Protobuf.IBufferMessage.InternalMergeFrom(Google.Protobuf.ParseContext&)
extern void EnumValue_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m00F8E5F6718E02F5C8D361D25E5941380E4414C3 (void);
// 0x00000328 System.Void Google.Protobuf.WellKnownTypes.EnumValue::.cctor()
extern void EnumValue__cctor_m16C0CD9EA5974CFB8F74E3B4F9B3A71D4B3D5CD9 (void);
// 0x00000329 System.Void Google.Protobuf.WellKnownTypes.EnumValue/<>c::.cctor()
extern void U3CU3Ec__cctor_m2470DAE20AC02503A82DE4481D3EAD98146AEEFA (void);
// 0x0000032A System.Void Google.Protobuf.WellKnownTypes.EnumValue/<>c::.ctor()
extern void U3CU3Ec__ctor_m9D69464D4D9F014C698A0346C55E148268D7B059 (void);
// 0x0000032B Google.Protobuf.WellKnownTypes.EnumValue Google.Protobuf.WellKnownTypes.EnumValue/<>c::<.cctor>b__37_0()
extern void U3CU3Ec_U3C_cctorU3Eb__37_0_m3D2A2652020F03D1C589B2B278A809A5C0E9091E (void);
// 0x0000032C Google.Protobuf.MessageParser`1<Google.Protobuf.WellKnownTypes.Option> Google.Protobuf.WellKnownTypes.Option::get_Parser()
extern void Option_get_Parser_mB82574481FF68ABD73D74EBB04A07F24F3AF1223 (void);
// 0x0000032D Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.WellKnownTypes.Option::get_Descriptor()
extern void Option_get_Descriptor_m0D017A952FACE05A6055A69FE79571AB0276A1CC (void);
// 0x0000032E Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.WellKnownTypes.Option::pb::Google.Protobuf.IMessage.get_Descriptor()
extern void Option_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_mA95527E3FE55B328291D1B058B3EBFF40709958B (void);
// 0x0000032F System.Void Google.Protobuf.WellKnownTypes.Option::.ctor()
extern void Option__ctor_m61127A3EBF04C32BAECB3FDC43C06A4A0BC34D55 (void);
// 0x00000330 System.Void Google.Protobuf.WellKnownTypes.Option::.ctor(Google.Protobuf.WellKnownTypes.Option)
extern void Option__ctor_mE400E557A643D6FD4336467A7CA7F4EBE15B193B (void);
// 0x00000331 Google.Protobuf.WellKnownTypes.Option Google.Protobuf.WellKnownTypes.Option::Clone()
extern void Option_Clone_mE36C75B283328B159094B29AD7C378CF96029557 (void);
// 0x00000332 System.String Google.Protobuf.WellKnownTypes.Option::get_Name()
extern void Option_get_Name_mDE5C5D257BEB91DBB93C43AA341E7F84491B9542 (void);
// 0x00000333 System.Void Google.Protobuf.WellKnownTypes.Option::set_Name(System.String)
extern void Option_set_Name_m7961ED75B388392B16247BE9DD111C5A19B065CB (void);
// 0x00000334 Google.Protobuf.WellKnownTypes.Any Google.Protobuf.WellKnownTypes.Option::get_Value()
extern void Option_get_Value_m26415C9AE3093725D50E08EBF07C2FC3CA9C2DF7 (void);
// 0x00000335 System.Void Google.Protobuf.WellKnownTypes.Option::set_Value(Google.Protobuf.WellKnownTypes.Any)
extern void Option_set_Value_m6BF492B08C599377AA5046B039AE5F258DF06587 (void);
// 0x00000336 System.Boolean Google.Protobuf.WellKnownTypes.Option::Equals(System.Object)
extern void Option_Equals_m5EE432C5466D73AD284F80AC21D7B022855176E8 (void);
// 0x00000337 System.Boolean Google.Protobuf.WellKnownTypes.Option::Equals(Google.Protobuf.WellKnownTypes.Option)
extern void Option_Equals_m851A5A03B1AD4D16DFC04D52FA7548AF198392DD (void);
// 0x00000338 System.Int32 Google.Protobuf.WellKnownTypes.Option::GetHashCode()
extern void Option_GetHashCode_mAB0CB2FD7D75D98123988AECFD80B5C37B4A02D9 (void);
// 0x00000339 System.String Google.Protobuf.WellKnownTypes.Option::ToString()
extern void Option_ToString_m87855F8C2E044740FD0712D0F8CF3DBE3143BF82 (void);
// 0x0000033A System.Void Google.Protobuf.WellKnownTypes.Option::WriteTo(Google.Protobuf.CodedOutputStream)
extern void Option_WriteTo_m08FFCCEC210A886C2FC6AD2D163A500877A28531 (void);
// 0x0000033B System.Void Google.Protobuf.WellKnownTypes.Option::pb::Google.Protobuf.IBufferMessage.InternalWriteTo(Google.Protobuf.WriteContext&)
extern void Option_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_mB2C264C7D44F83BE2E6D5E68825776101ADDD61C (void);
// 0x0000033C System.Int32 Google.Protobuf.WellKnownTypes.Option::CalculateSize()
extern void Option_CalculateSize_mFF4098BC49D348AADE7E6B68D8976452F255D94D (void);
// 0x0000033D System.Void Google.Protobuf.WellKnownTypes.Option::MergeFrom(Google.Protobuf.WellKnownTypes.Option)
extern void Option_MergeFrom_mA6F300660905E229B30AF09C9207C6EF44E158B9 (void);
// 0x0000033E System.Void Google.Protobuf.WellKnownTypes.Option::MergeFrom(Google.Protobuf.CodedInputStream)
extern void Option_MergeFrom_m9EA5877DB4934007C01D74759B0E63925094987D (void);
// 0x0000033F System.Void Google.Protobuf.WellKnownTypes.Option::pb::Google.Protobuf.IBufferMessage.InternalMergeFrom(Google.Protobuf.ParseContext&)
extern void Option_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m1CA4CEF42E2A2A7ADD75A49F9CBE0A644C0D01D6 (void);
// 0x00000340 System.Void Google.Protobuf.WellKnownTypes.Option::.cctor()
extern void Option__cctor_mF9F2D97A5799BD227ACEDFD1A6E74A0252A731A6 (void);
// 0x00000341 System.Void Google.Protobuf.WellKnownTypes.Option/<>c::.cctor()
extern void U3CU3Ec__cctor_mEF2E772FB2F6BC6213D7BF8908B7B31291A9AA1D (void);
// 0x00000342 System.Void Google.Protobuf.WellKnownTypes.Option/<>c::.ctor()
extern void U3CU3Ec__ctor_m60DEA573EE6DC55B5D5D1C3EE605C513F0F55AD2 (void);
// 0x00000343 Google.Protobuf.WellKnownTypes.Option Google.Protobuf.WellKnownTypes.Option/<>c::<.cctor>b__32_0()
extern void U3CU3Ec_U3C_cctorU3Eb__32_0_m029CE28E5DF4064F4E57FF01560036B5824B3E91 (void);
// 0x00000344 Google.Protobuf.Reflection.FileDescriptor Google.Protobuf.Reflection.DescriptorReflection::get_Descriptor()
extern void DescriptorReflection_get_Descriptor_mD48E1E10378003D1F19B1E94D3D62672FDF05EFD (void);
// 0x00000345 System.Void Google.Protobuf.Reflection.DescriptorReflection::.cctor()
extern void DescriptorReflection__cctor_m0C008E17D8889B3551682DBB6471B9FA75F8DF36 (void);
// 0x00000346 Google.Protobuf.MessageParser`1<Google.Protobuf.Reflection.FileDescriptorSet> Google.Protobuf.Reflection.FileDescriptorSet::get_Parser()
extern void FileDescriptorSet_get_Parser_mF60C00B40B45ACD6CC5E25158BACEA45E709FC28 (void);
// 0x00000347 Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.Reflection.FileDescriptorSet::get_Descriptor()
extern void FileDescriptorSet_get_Descriptor_m8CFCFC9C20758CEFAFF0D8DA9B85FCF333D69C7C (void);
// 0x00000348 Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.Reflection.FileDescriptorSet::pb::Google.Protobuf.IMessage.get_Descriptor()
extern void FileDescriptorSet_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m3B6F227E8F1D163E6CDFD5CF25D5A83A072953A7 (void);
// 0x00000349 System.Void Google.Protobuf.Reflection.FileDescriptorSet::.ctor()
extern void FileDescriptorSet__ctor_mA7A07A046215E12A519D8592D84CC8E5D77DE8A4 (void);
// 0x0000034A System.Void Google.Protobuf.Reflection.FileDescriptorSet::.ctor(Google.Protobuf.Reflection.FileDescriptorSet)
extern void FileDescriptorSet__ctor_m109C9F09C6370D20B70E70630294D2C09790FB73 (void);
// 0x0000034B Google.Protobuf.Reflection.FileDescriptorSet Google.Protobuf.Reflection.FileDescriptorSet::Clone()
extern void FileDescriptorSet_Clone_m265C516E8B9C78BD5CF6FC647677B44EA22C19C1 (void);
// 0x0000034C System.Boolean Google.Protobuf.Reflection.FileDescriptorSet::Equals(System.Object)
extern void FileDescriptorSet_Equals_mF6380585A3C4BCBB875C05218072B5FC9E7BEDA4 (void);
// 0x0000034D System.Boolean Google.Protobuf.Reflection.FileDescriptorSet::Equals(Google.Protobuf.Reflection.FileDescriptorSet)
extern void FileDescriptorSet_Equals_m6FB81A95B00A2BA8007CD098A1E64BC53CB1E8DE (void);
// 0x0000034E System.Int32 Google.Protobuf.Reflection.FileDescriptorSet::GetHashCode()
extern void FileDescriptorSet_GetHashCode_m5E8932B0438992144D00152506F56564FC008603 (void);
// 0x0000034F System.String Google.Protobuf.Reflection.FileDescriptorSet::ToString()
extern void FileDescriptorSet_ToString_m55C6B1E4124A8667B7A472041A15DFA788312E7A (void);
// 0x00000350 System.Void Google.Protobuf.Reflection.FileDescriptorSet::WriteTo(Google.Protobuf.CodedOutputStream)
extern void FileDescriptorSet_WriteTo_m906D2C2B397CDAFF5EF8D444C80AEB8410EAB931 (void);
// 0x00000351 System.Void Google.Protobuf.Reflection.FileDescriptorSet::pb::Google.Protobuf.IBufferMessage.InternalWriteTo(Google.Protobuf.WriteContext&)
extern void FileDescriptorSet_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m07D693FF17AE52BE5EBFD65CC7132C30BD5F1838 (void);
// 0x00000352 System.Int32 Google.Protobuf.Reflection.FileDescriptorSet::CalculateSize()
extern void FileDescriptorSet_CalculateSize_m71F5ECD29922E5260C9F27F5E9158338697E1A6B (void);
// 0x00000353 System.Void Google.Protobuf.Reflection.FileDescriptorSet::MergeFrom(Google.Protobuf.Reflection.FileDescriptorSet)
extern void FileDescriptorSet_MergeFrom_m4AD41D3A6331B1B8AA293288747EE68F38E15C41 (void);
// 0x00000354 System.Void Google.Protobuf.Reflection.FileDescriptorSet::MergeFrom(Google.Protobuf.CodedInputStream)
extern void FileDescriptorSet_MergeFrom_m6A837636C5DF50AB53B23E53FE1FC765B6828B27 (void);
// 0x00000355 System.Void Google.Protobuf.Reflection.FileDescriptorSet::pb::Google.Protobuf.IBufferMessage.InternalMergeFrom(Google.Protobuf.ParseContext&)
extern void FileDescriptorSet_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m30A4D0A83029F411C07921760B1EF8F53B90D6B9 (void);
// 0x00000356 System.Void Google.Protobuf.Reflection.FileDescriptorSet::.cctor()
extern void FileDescriptorSet__cctor_m20E6F64A50D326CAA9499B7C76E02F52D6162E0E (void);
// 0x00000357 System.Void Google.Protobuf.Reflection.FileDescriptorSet/<>c::.cctor()
extern void U3CU3Ec__cctor_m50CDA641CBB1EA7607EEADEB9F5268C895A7E40A (void);
// 0x00000358 System.Void Google.Protobuf.Reflection.FileDescriptorSet/<>c::.ctor()
extern void U3CU3Ec__ctor_m5D33B777A5C55A3AC9CC92DEEA5A532F42A5CA49 (void);
// 0x00000359 Google.Protobuf.Reflection.FileDescriptorSet Google.Protobuf.Reflection.FileDescriptorSet/<>c::<.cctor>b__27_0()
extern void U3CU3Ec_U3C_cctorU3Eb__27_0_mE97991AD768C275DC264F25BDEB287DACA075A10 (void);
// 0x0000035A Google.Protobuf.MessageParser`1<Google.Protobuf.Reflection.FileDescriptorProto> Google.Protobuf.Reflection.FileDescriptorProto::get_Parser()
extern void FileDescriptorProto_get_Parser_mBC80D7C4313B651253041FAA51EA4DD08FA94377 (void);
// 0x0000035B Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.Reflection.FileDescriptorProto::get_Descriptor()
extern void FileDescriptorProto_get_Descriptor_mFEA4B1596AB9AF52BCD9884D22F1F3DC3DB72B74 (void);
// 0x0000035C Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.Reflection.FileDescriptorProto::pb::Google.Protobuf.IMessage.get_Descriptor()
extern void FileDescriptorProto_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_mA6906CC88D72C5AA2EFBD969A001AF2CA52AA4D2 (void);
// 0x0000035D System.Void Google.Protobuf.Reflection.FileDescriptorProto::.ctor()
extern void FileDescriptorProto__ctor_m4485617D3F5C6C7A0B6803FB7A6A9E5CD9289E68 (void);
// 0x0000035E System.Void Google.Protobuf.Reflection.FileDescriptorProto::.ctor(Google.Protobuf.Reflection.FileDescriptorProto)
extern void FileDescriptorProto__ctor_m0E39D73BEB59D91943EF2EA1E8B43205EE2AD0D8 (void);
// 0x0000035F Google.Protobuf.Reflection.FileDescriptorProto Google.Protobuf.Reflection.FileDescriptorProto::Clone()
extern void FileDescriptorProto_Clone_m8A232664925A576E17BAEEE2B7C78110936F0378 (void);
// 0x00000360 System.String Google.Protobuf.Reflection.FileDescriptorProto::get_Name()
extern void FileDescriptorProto_get_Name_mAF692632AC782B6383B93923C017CD3325A09A4B (void);
// 0x00000361 System.Void Google.Protobuf.Reflection.FileDescriptorProto::set_Name(System.String)
extern void FileDescriptorProto_set_Name_m24617CEDC7867F3D3587EA82B6D1477BE5EEEDBC (void);
// 0x00000362 System.Boolean Google.Protobuf.Reflection.FileDescriptorProto::get_HasName()
extern void FileDescriptorProto_get_HasName_mBFF45899FDF6E9190B27274BCD2681849D6B459B (void);
// 0x00000363 System.String Google.Protobuf.Reflection.FileDescriptorProto::get_Package()
extern void FileDescriptorProto_get_Package_m953FAE73F7EA46AE4C14D110088C6B4F72FB0F05 (void);
// 0x00000364 System.Void Google.Protobuf.Reflection.FileDescriptorProto::set_Package(System.String)
extern void FileDescriptorProto_set_Package_mF51F9BF238F9294C3BFA5B362F901BA3EB72700E (void);
// 0x00000365 System.Boolean Google.Protobuf.Reflection.FileDescriptorProto::get_HasPackage()
extern void FileDescriptorProto_get_HasPackage_m3CA96626EE3FDD289B24A875727808B9432B3CA6 (void);
// 0x00000366 Google.Protobuf.Collections.RepeatedField`1<System.String> Google.Protobuf.Reflection.FileDescriptorProto::get_Dependency()
extern void FileDescriptorProto_get_Dependency_mB2BD3360EC27F1D642EF3915EA9E3F29D57104D5 (void);
// 0x00000367 Google.Protobuf.Collections.RepeatedField`1<System.Int32> Google.Protobuf.Reflection.FileDescriptorProto::get_PublicDependency()
extern void FileDescriptorProto_get_PublicDependency_mE45FDE23237DDCE9B8EDCD13E2E66EADF35E241B (void);
// 0x00000368 Google.Protobuf.Collections.RepeatedField`1<Google.Protobuf.Reflection.DescriptorProto> Google.Protobuf.Reflection.FileDescriptorProto::get_MessageType()
extern void FileDescriptorProto_get_MessageType_m703D5E70CB6B4131CCB13F42E427FFDF7E80340E (void);
// 0x00000369 Google.Protobuf.Collections.RepeatedField`1<Google.Protobuf.Reflection.EnumDescriptorProto> Google.Protobuf.Reflection.FileDescriptorProto::get_EnumType()
extern void FileDescriptorProto_get_EnumType_m780C5B3655C4F6BA8F2ED5BD87B7DAC5C73229F7 (void);
// 0x0000036A Google.Protobuf.Collections.RepeatedField`1<Google.Protobuf.Reflection.ServiceDescriptorProto> Google.Protobuf.Reflection.FileDescriptorProto::get_Service()
extern void FileDescriptorProto_get_Service_m8D3746DB003BA0368F1EB92EDBE253AE6E86B9FB (void);
// 0x0000036B Google.Protobuf.Collections.RepeatedField`1<Google.Protobuf.Reflection.FieldDescriptorProto> Google.Protobuf.Reflection.FileDescriptorProto::get_Extension()
extern void FileDescriptorProto_get_Extension_mC531637642342EFEE8B96AFA17BA8C08D4344EE0 (void);
// 0x0000036C Google.Protobuf.Reflection.FileOptions Google.Protobuf.Reflection.FileDescriptorProto::get_Options()
extern void FileDescriptorProto_get_Options_m15C8BD6028E25FAC38BA2B8B3D19F1C585D3734B (void);
// 0x0000036D System.Void Google.Protobuf.Reflection.FileDescriptorProto::set_Options(Google.Protobuf.Reflection.FileOptions)
extern void FileDescriptorProto_set_Options_m21D58E2C3C8AE99895B49377450BC0F378454FDF (void);
// 0x0000036E Google.Protobuf.Reflection.SourceCodeInfo Google.Protobuf.Reflection.FileDescriptorProto::get_SourceCodeInfo()
extern void FileDescriptorProto_get_SourceCodeInfo_m663A9395D90E8CC850D03DC98BEC80693E3A1002 (void);
// 0x0000036F System.Void Google.Protobuf.Reflection.FileDescriptorProto::set_SourceCodeInfo(Google.Protobuf.Reflection.SourceCodeInfo)
extern void FileDescriptorProto_set_SourceCodeInfo_m012501768C09AE8C1865C916E32B32E3012BD764 (void);
// 0x00000370 System.String Google.Protobuf.Reflection.FileDescriptorProto::get_Syntax()
extern void FileDescriptorProto_get_Syntax_m51E3382B5FFB11D7749644173B6D77F5E4E2FA00 (void);
// 0x00000371 System.Void Google.Protobuf.Reflection.FileDescriptorProto::set_Syntax(System.String)
extern void FileDescriptorProto_set_Syntax_m1130A6525EA22989936E59B94713575A9FE56FAF (void);
// 0x00000372 System.Boolean Google.Protobuf.Reflection.FileDescriptorProto::get_HasSyntax()
extern void FileDescriptorProto_get_HasSyntax_m8D2B955DB661A0F2B1E2C199DD6F6DE39F207C55 (void);
// 0x00000373 System.Boolean Google.Protobuf.Reflection.FileDescriptorProto::Equals(System.Object)
extern void FileDescriptorProto_Equals_mB2CE0475BC597D2DD9156FE405B673152788F432 (void);
// 0x00000374 System.Boolean Google.Protobuf.Reflection.FileDescriptorProto::Equals(Google.Protobuf.Reflection.FileDescriptorProto)
extern void FileDescriptorProto_Equals_mC0682C79CEED2451C8CDFE709CF4F49307B2FB3A (void);
// 0x00000375 System.Int32 Google.Protobuf.Reflection.FileDescriptorProto::GetHashCode()
extern void FileDescriptorProto_GetHashCode_mE4A7E0861EF395F56F05D05B158256F22EE5670A (void);
// 0x00000376 System.String Google.Protobuf.Reflection.FileDescriptorProto::ToString()
extern void FileDescriptorProto_ToString_m570AF4B70A7741E75E2065FA39DCF185C8D94CF9 (void);
// 0x00000377 System.Void Google.Protobuf.Reflection.FileDescriptorProto::WriteTo(Google.Protobuf.CodedOutputStream)
extern void FileDescriptorProto_WriteTo_m29D8F0A32F233CEBB2F2FBCB590D4739642AA0C2 (void);
// 0x00000378 System.Void Google.Protobuf.Reflection.FileDescriptorProto::pb::Google.Protobuf.IBufferMessage.InternalWriteTo(Google.Protobuf.WriteContext&)
extern void FileDescriptorProto_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m00C04878C6E3CEEAD15F30CCF8564EB613B2AE9B (void);
// 0x00000379 System.Int32 Google.Protobuf.Reflection.FileDescriptorProto::CalculateSize()
extern void FileDescriptorProto_CalculateSize_m7F8750FEC9F67C17825224AF365936D26A5AD58F (void);
// 0x0000037A System.Void Google.Protobuf.Reflection.FileDescriptorProto::MergeFrom(Google.Protobuf.Reflection.FileDescriptorProto)
extern void FileDescriptorProto_MergeFrom_mC85444AE37C8A342C2E0AFCA376603A29A4FDA3D (void);
// 0x0000037B System.Void Google.Protobuf.Reflection.FileDescriptorProto::MergeFrom(Google.Protobuf.CodedInputStream)
extern void FileDescriptorProto_MergeFrom_mEE0DD8720B946B400CFEC3D6C2BC73470BCF592E (void);
// 0x0000037C System.Void Google.Protobuf.Reflection.FileDescriptorProto::pb::Google.Protobuf.IBufferMessage.InternalMergeFrom(Google.Protobuf.ParseContext&)
extern void FileDescriptorProto_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m2ED80E57E0627E94AE7DA4F37CAD6021096B16FC (void);
// 0x0000037D System.Void Google.Protobuf.Reflection.FileDescriptorProto::.cctor()
extern void FileDescriptorProto__cctor_m76C00D632E66A93BE727CEFF15D85564BA425D24 (void);
// 0x0000037E System.Void Google.Protobuf.Reflection.FileDescriptorProto/<>c::.cctor()
extern void U3CU3Ec__cctor_m16CACFB1855393A785FD18676663035469D6C0EC (void);
// 0x0000037F System.Void Google.Protobuf.Reflection.FileDescriptorProto/<>c::.ctor()
extern void U3CU3Ec__ctor_mA9A0202338D571F452DCCE13FE4979824A2F233F (void);
// 0x00000380 Google.Protobuf.Reflection.FileDescriptorProto Google.Protobuf.Reflection.FileDescriptorProto/<>c::<.cctor>b__94_0()
extern void U3CU3Ec_U3C_cctorU3Eb__94_0_m26998181CC83924986F5E0CD3E04EB487C270F31 (void);
// 0x00000381 Google.Protobuf.MessageParser`1<Google.Protobuf.Reflection.DescriptorProto> Google.Protobuf.Reflection.DescriptorProto::get_Parser()
extern void DescriptorProto_get_Parser_m276DCF460271094F8087B12382C7517DF98D3929 (void);
// 0x00000382 Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.Reflection.DescriptorProto::get_Descriptor()
extern void DescriptorProto_get_Descriptor_mB027BAEE5CD378616F1DEC129A2D5D4752231215 (void);
// 0x00000383 Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.Reflection.DescriptorProto::pb::Google.Protobuf.IMessage.get_Descriptor()
extern void DescriptorProto_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m5175F90F807BE57C52D5C3D3B6068418944561C3 (void);
// 0x00000384 System.Void Google.Protobuf.Reflection.DescriptorProto::.ctor()
extern void DescriptorProto__ctor_mBA850C613C3DB05A54F24C5E732A05CB9EEA4A4E (void);
// 0x00000385 System.Void Google.Protobuf.Reflection.DescriptorProto::.ctor(Google.Protobuf.Reflection.DescriptorProto)
extern void DescriptorProto__ctor_mCF435119FBBAA9A389619587C9435EC7EC24ED46 (void);
// 0x00000386 Google.Protobuf.Reflection.DescriptorProto Google.Protobuf.Reflection.DescriptorProto::Clone()
extern void DescriptorProto_Clone_m8D6A43AB22810E2ABA42EE59D221FFD6F7F7DD58 (void);
// 0x00000387 System.String Google.Protobuf.Reflection.DescriptorProto::get_Name()
extern void DescriptorProto_get_Name_m3E2C546CE4CE413FBAE61C936C6F93662D761817 (void);
// 0x00000388 System.Void Google.Protobuf.Reflection.DescriptorProto::set_Name(System.String)
extern void DescriptorProto_set_Name_mD3E8F34E0D994B21310A929C4C1CAAA90690A3D1 (void);
// 0x00000389 System.Boolean Google.Protobuf.Reflection.DescriptorProto::get_HasName()
extern void DescriptorProto_get_HasName_mBC33C3CD634AB93FB2D4A6467AE9F62BAFE36522 (void);
// 0x0000038A Google.Protobuf.Collections.RepeatedField`1<Google.Protobuf.Reflection.FieldDescriptorProto> Google.Protobuf.Reflection.DescriptorProto::get_Field()
extern void DescriptorProto_get_Field_mBBA791CF771781910F63E3C343CEC713DF32989C (void);
// 0x0000038B Google.Protobuf.Collections.RepeatedField`1<Google.Protobuf.Reflection.FieldDescriptorProto> Google.Protobuf.Reflection.DescriptorProto::get_Extension()
extern void DescriptorProto_get_Extension_m334A6CE24413CFBA60D5D086A33DAC63E917890F (void);
// 0x0000038C Google.Protobuf.Collections.RepeatedField`1<Google.Protobuf.Reflection.DescriptorProto> Google.Protobuf.Reflection.DescriptorProto::get_NestedType()
extern void DescriptorProto_get_NestedType_mCE87739C7356629259D2BA6BA630F4D2813B94D1 (void);
// 0x0000038D Google.Protobuf.Collections.RepeatedField`1<Google.Protobuf.Reflection.EnumDescriptorProto> Google.Protobuf.Reflection.DescriptorProto::get_EnumType()
extern void DescriptorProto_get_EnumType_m8D641DC9AD6B53E9AB11C57ABE67E4F45AA3F5C5 (void);
// 0x0000038E Google.Protobuf.Collections.RepeatedField`1<Google.Protobuf.Reflection.OneofDescriptorProto> Google.Protobuf.Reflection.DescriptorProto::get_OneofDecl()
extern void DescriptorProto_get_OneofDecl_mBDB00FCC8FF516265D2C82802DE1B85A30B357D7 (void);
// 0x0000038F Google.Protobuf.Reflection.MessageOptions Google.Protobuf.Reflection.DescriptorProto::get_Options()
extern void DescriptorProto_get_Options_mE52AE7E36455BB39D6448FB4866C8C3ECAB107A9 (void);
// 0x00000390 System.Void Google.Protobuf.Reflection.DescriptorProto::set_Options(Google.Protobuf.Reflection.MessageOptions)
extern void DescriptorProto_set_Options_m64D1E7AE01AAD3129C2A0020A5B813064B904624 (void);
// 0x00000391 System.Boolean Google.Protobuf.Reflection.DescriptorProto::Equals(System.Object)
extern void DescriptorProto_Equals_mFBBBAFCA37FEC00E0D4DDE7758E3F23405F72323 (void);
// 0x00000392 System.Boolean Google.Protobuf.Reflection.DescriptorProto::Equals(Google.Protobuf.Reflection.DescriptorProto)
extern void DescriptorProto_Equals_m2254305BFFB53FE27C4AA5B4E0FC5C1580C7230F (void);
// 0x00000393 System.Int32 Google.Protobuf.Reflection.DescriptorProto::GetHashCode()
extern void DescriptorProto_GetHashCode_m158B9AA4C433C9EEE8BAB3C7C702E46277B64AA5 (void);
// 0x00000394 System.String Google.Protobuf.Reflection.DescriptorProto::ToString()
extern void DescriptorProto_ToString_m7300A8A26C0B22F794F933459B56D6B86DF4854B (void);
// 0x00000395 System.Void Google.Protobuf.Reflection.DescriptorProto::WriteTo(Google.Protobuf.CodedOutputStream)
extern void DescriptorProto_WriteTo_mA8BED1F35DFFC420B71BC788C94C974AF366A735 (void);
// 0x00000396 System.Void Google.Protobuf.Reflection.DescriptorProto::pb::Google.Protobuf.IBufferMessage.InternalWriteTo(Google.Protobuf.WriteContext&)
extern void DescriptorProto_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m08457FFB232FE6F6902B30F8E14731C57D6FF845 (void);
// 0x00000397 System.Int32 Google.Protobuf.Reflection.DescriptorProto::CalculateSize()
extern void DescriptorProto_CalculateSize_mD15A93668659599FB42FF86E335DBCCBB5D828F6 (void);
// 0x00000398 System.Void Google.Protobuf.Reflection.DescriptorProto::MergeFrom(Google.Protobuf.Reflection.DescriptorProto)
extern void DescriptorProto_MergeFrom_m3309DEA6819B3F251E2F114761961232F3A555EF (void);
// 0x00000399 System.Void Google.Protobuf.Reflection.DescriptorProto::MergeFrom(Google.Protobuf.CodedInputStream)
extern void DescriptorProto_MergeFrom_m186FD354E99638C8B67744AE7ED9C2556DCC9E24 (void);
// 0x0000039A System.Void Google.Protobuf.Reflection.DescriptorProto::pb::Google.Protobuf.IBufferMessage.InternalMergeFrom(Google.Protobuf.ParseContext&)
extern void DescriptorProto_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_mB4A4E23A065E9FD8A6B8030ED9498F52A2181CD2 (void);
// 0x0000039B System.Void Google.Protobuf.Reflection.DescriptorProto::.cctor()
extern void DescriptorProto__cctor_mA9D22674E07BD74ED04412497E99D248ADDF38DF (void);
// 0x0000039C Google.Protobuf.MessageParser`1<Google.Protobuf.Reflection.DescriptorProto/Types/ExtensionRange> Google.Protobuf.Reflection.DescriptorProto/Types/ExtensionRange::get_Parser()
extern void ExtensionRange_get_Parser_m3ABE1E8A8C459846BBF066845CAC6666B1F437B6 (void);
// 0x0000039D Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.Reflection.DescriptorProto/Types/ExtensionRange::get_Descriptor()
extern void ExtensionRange_get_Descriptor_m388DA01C89021BCDAB9380E4B45EA660F872F105 (void);
// 0x0000039E Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.Reflection.DescriptorProto/Types/ExtensionRange::pb::Google.Protobuf.IMessage.get_Descriptor()
extern void ExtensionRange_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m9F7D657538A26F3E6CDC05BCA8DBF1317D857291 (void);
// 0x0000039F System.Void Google.Protobuf.Reflection.DescriptorProto/Types/ExtensionRange::.ctor()
extern void ExtensionRange__ctor_mCE3CBBCDC18FA5EA4ADBF4AF4AF53D13327D323B (void);
// 0x000003A0 System.Void Google.Protobuf.Reflection.DescriptorProto/Types/ExtensionRange::.ctor(Google.Protobuf.Reflection.DescriptorProto/Types/ExtensionRange)
extern void ExtensionRange__ctor_m86DE2DE4CC81E1797CDC04A3068161C7AE6137D5 (void);
// 0x000003A1 Google.Protobuf.Reflection.DescriptorProto/Types/ExtensionRange Google.Protobuf.Reflection.DescriptorProto/Types/ExtensionRange::Clone()
extern void ExtensionRange_Clone_mFFA2D7061BFDF6189B1DAAD7388E9C545D8495C9 (void);
// 0x000003A2 System.Int32 Google.Protobuf.Reflection.DescriptorProto/Types/ExtensionRange::get_Start()
extern void ExtensionRange_get_Start_mA3D0997486C50725BE58C1CA5673D7FF55A5C146 (void);
// 0x000003A3 System.Void Google.Protobuf.Reflection.DescriptorProto/Types/ExtensionRange::set_Start(System.Int32)
extern void ExtensionRange_set_Start_m9365B396D1E93D9383FB218E8C1647580CAF8F8A (void);
// 0x000003A4 System.Boolean Google.Protobuf.Reflection.DescriptorProto/Types/ExtensionRange::get_HasStart()
extern void ExtensionRange_get_HasStart_mF06F274021CF0AAA2A089D408DB9C7A42CE55470 (void);
// 0x000003A5 System.Int32 Google.Protobuf.Reflection.DescriptorProto/Types/ExtensionRange::get_End()
extern void ExtensionRange_get_End_mE003D2FA043CC97339771738FFD41E7A0666EE1A (void);
// 0x000003A6 System.Void Google.Protobuf.Reflection.DescriptorProto/Types/ExtensionRange::set_End(System.Int32)
extern void ExtensionRange_set_End_m426BFCDB49FFED418C737A05B892921DB94A7C0F (void);
// 0x000003A7 System.Boolean Google.Protobuf.Reflection.DescriptorProto/Types/ExtensionRange::get_HasEnd()
extern void ExtensionRange_get_HasEnd_m1AF0817908CE18ABC02A12F0A969A8AED4FB353D (void);
// 0x000003A8 Google.Protobuf.Reflection.ExtensionRangeOptions Google.Protobuf.Reflection.DescriptorProto/Types/ExtensionRange::get_Options()
extern void ExtensionRange_get_Options_mB23DF8C7A3E0D7C3F1B7A68035C2B420FA26BD57 (void);
// 0x000003A9 System.Void Google.Protobuf.Reflection.DescriptorProto/Types/ExtensionRange::set_Options(Google.Protobuf.Reflection.ExtensionRangeOptions)
extern void ExtensionRange_set_Options_mCAD0B5561EED579E40AAE5AB14FD08224AB36539 (void);
// 0x000003AA System.Boolean Google.Protobuf.Reflection.DescriptorProto/Types/ExtensionRange::Equals(System.Object)
extern void ExtensionRange_Equals_mC16F46A587D2727AB7527B37898019C9EC0436FC (void);
// 0x000003AB System.Boolean Google.Protobuf.Reflection.DescriptorProto/Types/ExtensionRange::Equals(Google.Protobuf.Reflection.DescriptorProto/Types/ExtensionRange)
extern void ExtensionRange_Equals_mBCE0F486ED3163380B949A8F4654DC2359A0EA61 (void);
// 0x000003AC System.Int32 Google.Protobuf.Reflection.DescriptorProto/Types/ExtensionRange::GetHashCode()
extern void ExtensionRange_GetHashCode_m1705F6C558349578176C5BA9C2CD918B6A6689E8 (void);
// 0x000003AD System.String Google.Protobuf.Reflection.DescriptorProto/Types/ExtensionRange::ToString()
extern void ExtensionRange_ToString_m92C8CC6CEE16909BD1FA3C1323DC5A214AF7FAC4 (void);
// 0x000003AE System.Void Google.Protobuf.Reflection.DescriptorProto/Types/ExtensionRange::WriteTo(Google.Protobuf.CodedOutputStream)
extern void ExtensionRange_WriteTo_mCC7686D9A689AC98ED8D425B3131531FFD927C77 (void);
// 0x000003AF System.Void Google.Protobuf.Reflection.DescriptorProto/Types/ExtensionRange::pb::Google.Protobuf.IBufferMessage.InternalWriteTo(Google.Protobuf.WriteContext&)
extern void ExtensionRange_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m78E864903CC6C7966483D1C1DADD8FE35236BD02 (void);
// 0x000003B0 System.Int32 Google.Protobuf.Reflection.DescriptorProto/Types/ExtensionRange::CalculateSize()
extern void ExtensionRange_CalculateSize_mBE8901808FC96A0B6647FEE680B06880F0FA84E7 (void);
// 0x000003B1 System.Void Google.Protobuf.Reflection.DescriptorProto/Types/ExtensionRange::MergeFrom(Google.Protobuf.Reflection.DescriptorProto/Types/ExtensionRange)
extern void ExtensionRange_MergeFrom_m314E38B926BE9F9F1CD9E34681625888E95EB00E (void);
// 0x000003B2 System.Void Google.Protobuf.Reflection.DescriptorProto/Types/ExtensionRange::MergeFrom(Google.Protobuf.CodedInputStream)
extern void ExtensionRange_MergeFrom_m99ECB1EF2D098752D7635C356C1CFA50784EB3EA (void);
// 0x000003B3 System.Void Google.Protobuf.Reflection.DescriptorProto/Types/ExtensionRange::pb::Google.Protobuf.IBufferMessage.InternalMergeFrom(Google.Protobuf.ParseContext&)
extern void ExtensionRange_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m24A97A8F83256C686E2E8AE441EF5A58C03244EE (void);
// 0x000003B4 System.Void Google.Protobuf.Reflection.DescriptorProto/Types/ExtensionRange::.cctor()
extern void ExtensionRange__cctor_mD514B6273C218021DF7CECEADC129B307BA766E5 (void);
// 0x000003B5 System.Void Google.Protobuf.Reflection.DescriptorProto/Types/ExtensionRange/<>c::.cctor()
extern void U3CU3Ec__cctor_m39D6E6EC8A756DFEC357E0BF0076C0DFD801B3ED (void);
// 0x000003B6 System.Void Google.Protobuf.Reflection.DescriptorProto/Types/ExtensionRange/<>c::.ctor()
extern void U3CU3Ec__ctor_mAB207C6304C4309948492B33329C4030BBDA3EB3 (void);
// 0x000003B7 Google.Protobuf.Reflection.DescriptorProto/Types/ExtensionRange Google.Protobuf.Reflection.DescriptorProto/Types/ExtensionRange/<>c::<.cctor>b__46_0()
extern void U3CU3Ec_U3C_cctorU3Eb__46_0_m686DD5E765624AF43B355C35E0982F62E9EF4D69 (void);
// 0x000003B8 Google.Protobuf.MessageParser`1<Google.Protobuf.Reflection.DescriptorProto/Types/ReservedRange> Google.Protobuf.Reflection.DescriptorProto/Types/ReservedRange::get_Parser()
extern void ReservedRange_get_Parser_m40DA6F39B9D2674289F247EF71B3B96A85AFF5FE (void);
// 0x000003B9 Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.Reflection.DescriptorProto/Types/ReservedRange::get_Descriptor()
extern void ReservedRange_get_Descriptor_m823BCA62EE04D3B7C9822171D2330F06FC8C485C (void);
// 0x000003BA Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.Reflection.DescriptorProto/Types/ReservedRange::pb::Google.Protobuf.IMessage.get_Descriptor()
extern void ReservedRange_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_mF06225797A3EAD80A0EB516FAD8664418A9AA8A7 (void);
// 0x000003BB System.Void Google.Protobuf.Reflection.DescriptorProto/Types/ReservedRange::.ctor()
extern void ReservedRange__ctor_mB2F8257CCD2E526672CB4B552FD6799706F38CD5 (void);
// 0x000003BC System.Void Google.Protobuf.Reflection.DescriptorProto/Types/ReservedRange::.ctor(Google.Protobuf.Reflection.DescriptorProto/Types/ReservedRange)
extern void ReservedRange__ctor_m73F9F5F5A8EE837C56568DD545DA961703779FAD (void);
// 0x000003BD Google.Protobuf.Reflection.DescriptorProto/Types/ReservedRange Google.Protobuf.Reflection.DescriptorProto/Types/ReservedRange::Clone()
extern void ReservedRange_Clone_m30F1DBAF17F68D2418492E3502CC6D8EA1CBDCBB (void);
// 0x000003BE System.Int32 Google.Protobuf.Reflection.DescriptorProto/Types/ReservedRange::get_Start()
extern void ReservedRange_get_Start_mB2B194971EBAA1B830056532A863FA5ADCC959DA (void);
// 0x000003BF System.Void Google.Protobuf.Reflection.DescriptorProto/Types/ReservedRange::set_Start(System.Int32)
extern void ReservedRange_set_Start_m6F47455AAF98ED71ECC7B3F962F888F605779301 (void);
// 0x000003C0 System.Boolean Google.Protobuf.Reflection.DescriptorProto/Types/ReservedRange::get_HasStart()
extern void ReservedRange_get_HasStart_m06D63475716042512F267FC11EBD1B6091CB9863 (void);
// 0x000003C1 System.Int32 Google.Protobuf.Reflection.DescriptorProto/Types/ReservedRange::get_End()
extern void ReservedRange_get_End_mE43F5C280D211E7F703010B8F2DE636F6A41A613 (void);
// 0x000003C2 System.Void Google.Protobuf.Reflection.DescriptorProto/Types/ReservedRange::set_End(System.Int32)
extern void ReservedRange_set_End_m73F555A1EA731DC79EDFECF80708ABD6EC439180 (void);
// 0x000003C3 System.Boolean Google.Protobuf.Reflection.DescriptorProto/Types/ReservedRange::get_HasEnd()
extern void ReservedRange_get_HasEnd_mF494E69EE1C08B5151162C24A2567A73F71503DA (void);
// 0x000003C4 System.Boolean Google.Protobuf.Reflection.DescriptorProto/Types/ReservedRange::Equals(System.Object)
extern void ReservedRange_Equals_m1C20E897B3FFD56BAB9E3248E741DFD243C3EC67 (void);
// 0x000003C5 System.Boolean Google.Protobuf.Reflection.DescriptorProto/Types/ReservedRange::Equals(Google.Protobuf.Reflection.DescriptorProto/Types/ReservedRange)
extern void ReservedRange_Equals_mE5912F6171510806B9EF02DE03CF8DD3F35B9E4C (void);
// 0x000003C6 System.Int32 Google.Protobuf.Reflection.DescriptorProto/Types/ReservedRange::GetHashCode()
extern void ReservedRange_GetHashCode_mA843DDE13FDC934E5BF90F4E87226E6895C7DF3A (void);
// 0x000003C7 System.String Google.Protobuf.Reflection.DescriptorProto/Types/ReservedRange::ToString()
extern void ReservedRange_ToString_m5B37BC81177B03CCEE38CEF2CC602923C1AE18E9 (void);
// 0x000003C8 System.Void Google.Protobuf.Reflection.DescriptorProto/Types/ReservedRange::WriteTo(Google.Protobuf.CodedOutputStream)
extern void ReservedRange_WriteTo_mAE8C9FDDD04DFAA10E55D3CB8E65948E461FBCB7 (void);
// 0x000003C9 System.Void Google.Protobuf.Reflection.DescriptorProto/Types/ReservedRange::pb::Google.Protobuf.IBufferMessage.InternalWriteTo(Google.Protobuf.WriteContext&)
extern void ReservedRange_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m85F3FBF2DF0058D3A982337FD4736926519B8D07 (void);
// 0x000003CA System.Int32 Google.Protobuf.Reflection.DescriptorProto/Types/ReservedRange::CalculateSize()
extern void ReservedRange_CalculateSize_m76825970AE352D4A345CA58060D3BA6FF4AD90C5 (void);
// 0x000003CB System.Void Google.Protobuf.Reflection.DescriptorProto/Types/ReservedRange::MergeFrom(Google.Protobuf.Reflection.DescriptorProto/Types/ReservedRange)
extern void ReservedRange_MergeFrom_mE827CEF25B811678E61BB8E50240CBE127E51442 (void);
// 0x000003CC System.Void Google.Protobuf.Reflection.DescriptorProto/Types/ReservedRange::MergeFrom(Google.Protobuf.CodedInputStream)
extern void ReservedRange_MergeFrom_m79DF028674F84059F761E6CAA9967E750D41E0C6 (void);
// 0x000003CD System.Void Google.Protobuf.Reflection.DescriptorProto/Types/ReservedRange::pb::Google.Protobuf.IBufferMessage.InternalMergeFrom(Google.Protobuf.ParseContext&)
extern void ReservedRange_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m53FFCAA3778D05AEEA1A4930A32451CB6F529B68 (void);
// 0x000003CE System.Void Google.Protobuf.Reflection.DescriptorProto/Types/ReservedRange::.cctor()
extern void ReservedRange__cctor_m10A1727C90A4836F89C853BFEDB8BC36A8F456A6 (void);
// 0x000003CF System.Void Google.Protobuf.Reflection.DescriptorProto/Types/ReservedRange/<>c::.cctor()
extern void U3CU3Ec__cctor_m842920908C66FD0DBBD8BB949911FBBEE8646463 (void);
// 0x000003D0 System.Void Google.Protobuf.Reflection.DescriptorProto/Types/ReservedRange/<>c::.ctor()
extern void U3CU3Ec__ctor_m5826A097FE555448C5C789E783537C4451C2909A (void);
// 0x000003D1 Google.Protobuf.Reflection.DescriptorProto/Types/ReservedRange Google.Protobuf.Reflection.DescriptorProto/Types/ReservedRange/<>c::<.cctor>b__41_0()
extern void U3CU3Ec_U3C_cctorU3Eb__41_0_m61BF31F913FE7F0E7A30777DA3CD3957025589E1 (void);
// 0x000003D2 System.Void Google.Protobuf.Reflection.DescriptorProto/<>c::.cctor()
extern void U3CU3Ec__cctor_m8D1AE7EC1E3202B9983A6DB4A59541D21C7A307B (void);
// 0x000003D3 System.Void Google.Protobuf.Reflection.DescriptorProto/<>c::.ctor()
extern void U3CU3Ec__ctor_mCD77D5ADC50872DDA7563F46E2160CC31477126E (void);
// 0x000003D4 Google.Protobuf.Reflection.DescriptorProto Google.Protobuf.Reflection.DescriptorProto/<>c::<.cctor>b__77_0()
extern void U3CU3Ec_U3C_cctorU3Eb__77_0_m79A3FABE6A2FDED40D62FF3DEE18242F7B6B336D (void);
// 0x000003D5 Google.Protobuf.MessageParser`1<Google.Protobuf.Reflection.ExtensionRangeOptions> Google.Protobuf.Reflection.ExtensionRangeOptions::get_Parser()
extern void ExtensionRangeOptions_get_Parser_m85440C70A07EB46D6F38725186FA3EEA3DF1DBF5 (void);
// 0x000003D6 Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.Reflection.ExtensionRangeOptions::get_Descriptor()
extern void ExtensionRangeOptions_get_Descriptor_m4F76243964B5BE49E73DCC80C35993D84C40A7D8 (void);
// 0x000003D7 Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.Reflection.ExtensionRangeOptions::pb::Google.Protobuf.IMessage.get_Descriptor()
extern void ExtensionRangeOptions_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_mB61A67B362B2405EE1D0FEA2F76462DCF0B6F2A5 (void);
// 0x000003D8 System.Void Google.Protobuf.Reflection.ExtensionRangeOptions::.ctor()
extern void ExtensionRangeOptions__ctor_m03DB6B7CDC07388AD8EAC496F9F8FE6150421544 (void);
// 0x000003D9 System.Void Google.Protobuf.Reflection.ExtensionRangeOptions::.ctor(Google.Protobuf.Reflection.ExtensionRangeOptions)
extern void ExtensionRangeOptions__ctor_m0268E7412A855333005EB632BC2D81177F7CF0D2 (void);
// 0x000003DA Google.Protobuf.Reflection.ExtensionRangeOptions Google.Protobuf.Reflection.ExtensionRangeOptions::Clone()
extern void ExtensionRangeOptions_Clone_mC718522EFD92E71DA33FA9E9C28DB3DDF2C4DA7E (void);
// 0x000003DB System.Boolean Google.Protobuf.Reflection.ExtensionRangeOptions::Equals(System.Object)
extern void ExtensionRangeOptions_Equals_mC28F05C7BAD43AC052787ED90AE10C25DE6F9E5F (void);
// 0x000003DC System.Boolean Google.Protobuf.Reflection.ExtensionRangeOptions::Equals(Google.Protobuf.Reflection.ExtensionRangeOptions)
extern void ExtensionRangeOptions_Equals_m2A50B09E41F199B9161902DC11CEE01EDB02AD93 (void);
// 0x000003DD System.Int32 Google.Protobuf.Reflection.ExtensionRangeOptions::GetHashCode()
extern void ExtensionRangeOptions_GetHashCode_m1FA993AAD13E3E515CB8E03384BBC859A807F719 (void);
// 0x000003DE System.String Google.Protobuf.Reflection.ExtensionRangeOptions::ToString()
extern void ExtensionRangeOptions_ToString_mCDAFBAF9D027FD33891829389B30DEEEDA1DA694 (void);
// 0x000003DF System.Void Google.Protobuf.Reflection.ExtensionRangeOptions::WriteTo(Google.Protobuf.CodedOutputStream)
extern void ExtensionRangeOptions_WriteTo_mE36C0650E68E929057CDB2BEFBD837EB32726F93 (void);
// 0x000003E0 System.Void Google.Protobuf.Reflection.ExtensionRangeOptions::pb::Google.Protobuf.IBufferMessage.InternalWriteTo(Google.Protobuf.WriteContext&)
extern void ExtensionRangeOptions_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m8AAAA629B772292D614BE8DF07E56142B2ACDADE (void);
// 0x000003E1 System.Int32 Google.Protobuf.Reflection.ExtensionRangeOptions::CalculateSize()
extern void ExtensionRangeOptions_CalculateSize_mE5FABC172FABF85E1427D3110E45BDD0E64E464C (void);
// 0x000003E2 System.Void Google.Protobuf.Reflection.ExtensionRangeOptions::MergeFrom(Google.Protobuf.Reflection.ExtensionRangeOptions)
extern void ExtensionRangeOptions_MergeFrom_m676DA699130B7B80F30ACA1338B9E369B44940B1 (void);
// 0x000003E3 System.Void Google.Protobuf.Reflection.ExtensionRangeOptions::MergeFrom(Google.Protobuf.CodedInputStream)
extern void ExtensionRangeOptions_MergeFrom_m2E9222BFE09BE7AC39995BB9DF8377E8C5C02486 (void);
// 0x000003E4 System.Void Google.Protobuf.Reflection.ExtensionRangeOptions::pb::Google.Protobuf.IBufferMessage.InternalMergeFrom(Google.Protobuf.ParseContext&)
extern void ExtensionRangeOptions_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m67DB4C9A5F1DC35C7852751B65AE26736ED00D11 (void);
// 0x000003E5 TValue Google.Protobuf.Reflection.ExtensionRangeOptions::GetExtension(Google.Protobuf.Extension`2<Google.Protobuf.Reflection.ExtensionRangeOptions,TValue>)
// 0x000003E6 Google.Protobuf.Collections.RepeatedField`1<TValue> Google.Protobuf.Reflection.ExtensionRangeOptions::GetExtension(Google.Protobuf.RepeatedExtension`2<Google.Protobuf.Reflection.ExtensionRangeOptions,TValue>)
// 0x000003E7 Google.Protobuf.Collections.RepeatedField`1<TValue> Google.Protobuf.Reflection.ExtensionRangeOptions::GetOrInitializeExtension(Google.Protobuf.RepeatedExtension`2<Google.Protobuf.Reflection.ExtensionRangeOptions,TValue>)
// 0x000003E8 System.Boolean Google.Protobuf.Reflection.ExtensionRangeOptions::HasExtension(Google.Protobuf.Extension`2<Google.Protobuf.Reflection.ExtensionRangeOptions,TValue>)
// 0x000003E9 System.Void Google.Protobuf.Reflection.ExtensionRangeOptions::ClearExtension(Google.Protobuf.Extension`2<Google.Protobuf.Reflection.ExtensionRangeOptions,TValue>)
// 0x000003EA System.Void Google.Protobuf.Reflection.ExtensionRangeOptions::.cctor()
extern void ExtensionRangeOptions__cctor_mFA24396C1DEF8ECDC28E86DBFB1C632D983C4F45 (void);
// 0x000003EB System.Void Google.Protobuf.Reflection.ExtensionRangeOptions/<>c::.cctor()
extern void U3CU3Ec__cctor_m965149C8F86F84E9D3197F694E9968244A1153B0 (void);
// 0x000003EC System.Void Google.Protobuf.Reflection.ExtensionRangeOptions/<>c::.ctor()
extern void U3CU3Ec__ctor_m77513FE6A1DA0E18E451A30C160D4CA9975F0B6D (void);
// 0x000003ED Google.Protobuf.Reflection.ExtensionRangeOptions Google.Protobuf.Reflection.ExtensionRangeOptions/<>c::<.cctor>b__37_0()
extern void U3CU3Ec_U3C_cctorU3Eb__37_0_mDB82B61FE0D537AD51A6B817F5629708C04D9324 (void);
// 0x000003EE Google.Protobuf.MessageParser`1<Google.Protobuf.Reflection.FieldDescriptorProto> Google.Protobuf.Reflection.FieldDescriptorProto::get_Parser()
extern void FieldDescriptorProto_get_Parser_mB9782B37EA5D993DA11EAC9F10D5A49B516F7B0F (void);
// 0x000003EF Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.Reflection.FieldDescriptorProto::get_Descriptor()
extern void FieldDescriptorProto_get_Descriptor_m9A6BB07AB7DE3A05D6B336CB8CB4F6FFEE25ADB1 (void);
// 0x000003F0 Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.Reflection.FieldDescriptorProto::pb::Google.Protobuf.IMessage.get_Descriptor()
extern void FieldDescriptorProto_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m8F889757EED4DE719D6CD2C961086C28FF9BCE01 (void);
// 0x000003F1 System.Void Google.Protobuf.Reflection.FieldDescriptorProto::.ctor()
extern void FieldDescriptorProto__ctor_mDB51787020766E9E2EAB543B0184EAFA0982FD22 (void);
// 0x000003F2 System.Void Google.Protobuf.Reflection.FieldDescriptorProto::.ctor(Google.Protobuf.Reflection.FieldDescriptorProto)
extern void FieldDescriptorProto__ctor_mF5B66C7A74743444259BC8300F6D960A429A61AA (void);
// 0x000003F3 Google.Protobuf.Reflection.FieldDescriptorProto Google.Protobuf.Reflection.FieldDescriptorProto::Clone()
extern void FieldDescriptorProto_Clone_m3B18B348D372D0C457D0F49FD218045E8D8A89B3 (void);
// 0x000003F4 System.String Google.Protobuf.Reflection.FieldDescriptorProto::get_Name()
extern void FieldDescriptorProto_get_Name_m5F250A971D59E241BF2880A56CB1F9745B16BDD2 (void);
// 0x000003F5 System.Void Google.Protobuf.Reflection.FieldDescriptorProto::set_Name(System.String)
extern void FieldDescriptorProto_set_Name_m5C9270A955025D66573AE8215E7926F1BE4B3F4E (void);
// 0x000003F6 System.Boolean Google.Protobuf.Reflection.FieldDescriptorProto::get_HasName()
extern void FieldDescriptorProto_get_HasName_m0C19A6BB41B5781F03DE0293C808C48AC24497BF (void);
// 0x000003F7 System.Int32 Google.Protobuf.Reflection.FieldDescriptorProto::get_Number()
extern void FieldDescriptorProto_get_Number_m21A6E332B603CE4603DE65C8DF13AF06DE04CE15 (void);
// 0x000003F8 System.Void Google.Protobuf.Reflection.FieldDescriptorProto::set_Number(System.Int32)
extern void FieldDescriptorProto_set_Number_mB34288A1D2DC39CFF8DB4B385ED9E0732DBEE4B0 (void);
// 0x000003F9 System.Boolean Google.Protobuf.Reflection.FieldDescriptorProto::get_HasNumber()
extern void FieldDescriptorProto_get_HasNumber_m5B490D0FF92C919DC31393CF001F9E33A651E765 (void);
// 0x000003FA Google.Protobuf.Reflection.FieldDescriptorProto/Types/Label Google.Protobuf.Reflection.FieldDescriptorProto::get_Label()
extern void FieldDescriptorProto_get_Label_mFCD8775062E789C07AAF8DCECA6DA4DCEB7AE628 (void);
// 0x000003FB System.Void Google.Protobuf.Reflection.FieldDescriptorProto::set_Label(Google.Protobuf.Reflection.FieldDescriptorProto/Types/Label)
extern void FieldDescriptorProto_set_Label_m557B8E68A108A0C73C4086A9AD5DBD461AFF6C99 (void);
// 0x000003FC System.Boolean Google.Protobuf.Reflection.FieldDescriptorProto::get_HasLabel()
extern void FieldDescriptorProto_get_HasLabel_mE7F9CB633C16FCBB83919A67579168F04D5F7023 (void);
// 0x000003FD Google.Protobuf.Reflection.FieldDescriptorProto/Types/Type Google.Protobuf.Reflection.FieldDescriptorProto::get_Type()
extern void FieldDescriptorProto_get_Type_m0504EED6A9D14275AC75A370351D61C7D52A8040 (void);
// 0x000003FE System.Void Google.Protobuf.Reflection.FieldDescriptorProto::set_Type(Google.Protobuf.Reflection.FieldDescriptorProto/Types/Type)
extern void FieldDescriptorProto_set_Type_m30BEE0C21CCB3126E6B50762F497BB97369D9782 (void);
// 0x000003FF System.Boolean Google.Protobuf.Reflection.FieldDescriptorProto::get_HasType()
extern void FieldDescriptorProto_get_HasType_m51916144B1C5D1E46D8B48BE61276FD4C76B6FF2 (void);
// 0x00000400 System.String Google.Protobuf.Reflection.FieldDescriptorProto::get_TypeName()
extern void FieldDescriptorProto_get_TypeName_mB4F1FA66AEE10FE9E458109DA595B976110EBD23 (void);
// 0x00000401 System.Void Google.Protobuf.Reflection.FieldDescriptorProto::set_TypeName(System.String)
extern void FieldDescriptorProto_set_TypeName_mA13C37BF4F941B77BA0366C5EC41CDA8E8D811BC (void);
// 0x00000402 System.Boolean Google.Protobuf.Reflection.FieldDescriptorProto::get_HasTypeName()
extern void FieldDescriptorProto_get_HasTypeName_m67246CBDAA461FBEBEF934813707FAFA63D71C76 (void);
// 0x00000403 System.String Google.Protobuf.Reflection.FieldDescriptorProto::get_Extendee()
extern void FieldDescriptorProto_get_Extendee_m5CA86ED516F65391E65B7B9F62DC18D832516EF0 (void);
// 0x00000404 System.Void Google.Protobuf.Reflection.FieldDescriptorProto::set_Extendee(System.String)
extern void FieldDescriptorProto_set_Extendee_m6A7039C9B3FEF8B31BC1D22A046EC6ACCC14FA1C (void);
// 0x00000405 System.Boolean Google.Protobuf.Reflection.FieldDescriptorProto::get_HasExtendee()
extern void FieldDescriptorProto_get_HasExtendee_m002937A41A74030F48AECCF56AD16783E62469FC (void);
// 0x00000406 System.String Google.Protobuf.Reflection.FieldDescriptorProto::get_DefaultValue()
extern void FieldDescriptorProto_get_DefaultValue_mEB7AA3385EA5B984CA4BC5A81AD8FD2E2838072E (void);
// 0x00000407 System.Void Google.Protobuf.Reflection.FieldDescriptorProto::set_DefaultValue(System.String)
extern void FieldDescriptorProto_set_DefaultValue_m30453870C914082727395E4F483B9C27FACDF83F (void);
// 0x00000408 System.Boolean Google.Protobuf.Reflection.FieldDescriptorProto::get_HasDefaultValue()
extern void FieldDescriptorProto_get_HasDefaultValue_m43BB2F47AAEF3682FD933DFCE9E676303443C3E3 (void);
// 0x00000409 System.Int32 Google.Protobuf.Reflection.FieldDescriptorProto::get_OneofIndex()
extern void FieldDescriptorProto_get_OneofIndex_m2F72D788585178F84508EA65E31798F47E591C06 (void);
// 0x0000040A System.Void Google.Protobuf.Reflection.FieldDescriptorProto::set_OneofIndex(System.Int32)
extern void FieldDescriptorProto_set_OneofIndex_mB59F7FD960A44D5713C2C7C533DCB2E29598176E (void);
// 0x0000040B System.Boolean Google.Protobuf.Reflection.FieldDescriptorProto::get_HasOneofIndex()
extern void FieldDescriptorProto_get_HasOneofIndex_m402CEDFAE1C19BC01B93ECE144F012A6F4F8D10F (void);
// 0x0000040C System.String Google.Protobuf.Reflection.FieldDescriptorProto::get_JsonName()
extern void FieldDescriptorProto_get_JsonName_m88C694CE526673F798DFDD9428D8E461FAC219DD (void);
// 0x0000040D System.Void Google.Protobuf.Reflection.FieldDescriptorProto::set_JsonName(System.String)
extern void FieldDescriptorProto_set_JsonName_m648841B04831164E456885A2AD7C4E1A907271C4 (void);
// 0x0000040E System.Boolean Google.Protobuf.Reflection.FieldDescriptorProto::get_HasJsonName()
extern void FieldDescriptorProto_get_HasJsonName_mC66BBCBF80BC5DF9F925E353513CAE31D9B543A8 (void);
// 0x0000040F Google.Protobuf.Reflection.FieldOptions Google.Protobuf.Reflection.FieldDescriptorProto::get_Options()
extern void FieldDescriptorProto_get_Options_m024A5FCA8A0E7863232F739DE7EADDC097E83406 (void);
// 0x00000410 System.Void Google.Protobuf.Reflection.FieldDescriptorProto::set_Options(Google.Protobuf.Reflection.FieldOptions)
extern void FieldDescriptorProto_set_Options_m95B65464743B9F0939EB24BBEE6C968C71C4F778 (void);
// 0x00000411 System.Boolean Google.Protobuf.Reflection.FieldDescriptorProto::get_Proto3Optional()
extern void FieldDescriptorProto_get_Proto3Optional_m03E3CCAB1139E8E1FF6A7101498E5099FDDE5C17 (void);
// 0x00000412 System.Void Google.Protobuf.Reflection.FieldDescriptorProto::set_Proto3Optional(System.Boolean)
extern void FieldDescriptorProto_set_Proto3Optional_m4681472D59480132B5EA2040610D889A6CA34811 (void);
// 0x00000413 System.Boolean Google.Protobuf.Reflection.FieldDescriptorProto::get_HasProto3Optional()
extern void FieldDescriptorProto_get_HasProto3Optional_m66D148A23694B79771081395760DB902F52CC67D (void);
// 0x00000414 System.Boolean Google.Protobuf.Reflection.FieldDescriptorProto::Equals(System.Object)
extern void FieldDescriptorProto_Equals_m8280930EB8D7F7C3BFE6BA9FA223C0411C56F47C (void);
// 0x00000415 System.Boolean Google.Protobuf.Reflection.FieldDescriptorProto::Equals(Google.Protobuf.Reflection.FieldDescriptorProto)
extern void FieldDescriptorProto_Equals_m3527F0C058D9FC4E1466AFD93B448721A1F18763 (void);
// 0x00000416 System.Int32 Google.Protobuf.Reflection.FieldDescriptorProto::GetHashCode()
extern void FieldDescriptorProto_GetHashCode_mB99143077FA079BD863F94EB1D18717B09C01CC8 (void);
// 0x00000417 System.String Google.Protobuf.Reflection.FieldDescriptorProto::ToString()
extern void FieldDescriptorProto_ToString_m11F796FE089C2875CDB38AAA3422E19996433A7E (void);
// 0x00000418 System.Void Google.Protobuf.Reflection.FieldDescriptorProto::WriteTo(Google.Protobuf.CodedOutputStream)
extern void FieldDescriptorProto_WriteTo_mFA22B0175F6D838CEA09CA1F7B6DFE0E76622CC7 (void);
// 0x00000419 System.Void Google.Protobuf.Reflection.FieldDescriptorProto::pb::Google.Protobuf.IBufferMessage.InternalWriteTo(Google.Protobuf.WriteContext&)
extern void FieldDescriptorProto_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m5898B3409384DC7633B83CDAC353FFD5D1D67784 (void);
// 0x0000041A System.Int32 Google.Protobuf.Reflection.FieldDescriptorProto::CalculateSize()
extern void FieldDescriptorProto_CalculateSize_m955D78E94DE36494938994453F571457E48436D2 (void);
// 0x0000041B System.Void Google.Protobuf.Reflection.FieldDescriptorProto::MergeFrom(Google.Protobuf.Reflection.FieldDescriptorProto)
extern void FieldDescriptorProto_MergeFrom_m0DCDC8CB908E61CF0D7136B40C38940D9F21F986 (void);
// 0x0000041C System.Void Google.Protobuf.Reflection.FieldDescriptorProto::MergeFrom(Google.Protobuf.CodedInputStream)
extern void FieldDescriptorProto_MergeFrom_m70DA833065B73BA3CE80FE249DFDAA745A4981EC (void);
// 0x0000041D System.Void Google.Protobuf.Reflection.FieldDescriptorProto::pb::Google.Protobuf.IBufferMessage.InternalMergeFrom(Google.Protobuf.ParseContext&)
extern void FieldDescriptorProto_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m986E810C00664AFB4CF43B946B88EE7A1DC7B66F (void);
// 0x0000041E System.Void Google.Protobuf.Reflection.FieldDescriptorProto::.cctor()
extern void FieldDescriptorProto__cctor_m759F49EAC9FA24827EB7D731D55D718F4DD10C76 (void);
// 0x0000041F System.Void Google.Protobuf.Reflection.FieldDescriptorProto/<>c::.cctor()
extern void U3CU3Ec__cctor_m9922F139143B79B66907D5606AA1A59C5C20E9F7 (void);
// 0x00000420 System.Void Google.Protobuf.Reflection.FieldDescriptorProto/<>c::.ctor()
extern void U3CU3Ec__ctor_m072AEA01AA790344E658362FBD0DEAD2855BF199 (void);
// 0x00000421 Google.Protobuf.Reflection.FieldDescriptorProto Google.Protobuf.Reflection.FieldDescriptorProto/<>c::<.cctor>b__119_0()
extern void U3CU3Ec_U3C_cctorU3Eb__119_0_m7D9173AA040B54F58FFE68F3FC7A1534375B457D (void);
// 0x00000422 Google.Protobuf.MessageParser`1<Google.Protobuf.Reflection.OneofDescriptorProto> Google.Protobuf.Reflection.OneofDescriptorProto::get_Parser()
extern void OneofDescriptorProto_get_Parser_m38645932E3246CFFC2302EE1172459DD75F4C40E (void);
// 0x00000423 Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.Reflection.OneofDescriptorProto::get_Descriptor()
extern void OneofDescriptorProto_get_Descriptor_m0F8C4CF9CDFA95DADBE71668532A80BF2280EE5C (void);
// 0x00000424 Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.Reflection.OneofDescriptorProto::pb::Google.Protobuf.IMessage.get_Descriptor()
extern void OneofDescriptorProto_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m8A36A9B5974436DB17EE53737775E9102339983B (void);
// 0x00000425 System.Void Google.Protobuf.Reflection.OneofDescriptorProto::.ctor()
extern void OneofDescriptorProto__ctor_mD2CE062E1C095E49ABDEFDC74B17229A45493047 (void);
// 0x00000426 System.Void Google.Protobuf.Reflection.OneofDescriptorProto::.ctor(Google.Protobuf.Reflection.OneofDescriptorProto)
extern void OneofDescriptorProto__ctor_m14C97250B9E90E60CA6CA6064C9250354632C3AC (void);
// 0x00000427 Google.Protobuf.Reflection.OneofDescriptorProto Google.Protobuf.Reflection.OneofDescriptorProto::Clone()
extern void OneofDescriptorProto_Clone_m7C45A01EF4DAE4EDB8A17F181864989B593E3D11 (void);
// 0x00000428 System.String Google.Protobuf.Reflection.OneofDescriptorProto::get_Name()
extern void OneofDescriptorProto_get_Name_m48DE233B82B48DD4D9E7066E0785FA2F846CEC20 (void);
// 0x00000429 System.Void Google.Protobuf.Reflection.OneofDescriptorProto::set_Name(System.String)
extern void OneofDescriptorProto_set_Name_m1F624AEBFC415E1D977216D1CA23C22865C374BF (void);
// 0x0000042A System.Boolean Google.Protobuf.Reflection.OneofDescriptorProto::get_HasName()
extern void OneofDescriptorProto_get_HasName_m3EE3ED61FCF21A67137EC7AE8BE7B1190B827308 (void);
// 0x0000042B Google.Protobuf.Reflection.OneofOptions Google.Protobuf.Reflection.OneofDescriptorProto::get_Options()
extern void OneofDescriptorProto_get_Options_mAC4CABC75D32418D2430F721324B86232B0E9D3C (void);
// 0x0000042C System.Void Google.Protobuf.Reflection.OneofDescriptorProto::set_Options(Google.Protobuf.Reflection.OneofOptions)
extern void OneofDescriptorProto_set_Options_mA0DB684E887321ED76D23FCF58BFA5E680F61B07 (void);
// 0x0000042D System.Boolean Google.Protobuf.Reflection.OneofDescriptorProto::Equals(System.Object)
extern void OneofDescriptorProto_Equals_m63D5B71E1166BAFAF2BE805067EC147D8D75298D (void);
// 0x0000042E System.Boolean Google.Protobuf.Reflection.OneofDescriptorProto::Equals(Google.Protobuf.Reflection.OneofDescriptorProto)
extern void OneofDescriptorProto_Equals_mFB6BA3CE309BF9605D4E93E1CDBE45ECAF125EA3 (void);
// 0x0000042F System.Int32 Google.Protobuf.Reflection.OneofDescriptorProto::GetHashCode()
extern void OneofDescriptorProto_GetHashCode_m44452D2990660A273BDB0CBA23FC660618F20E1A (void);
// 0x00000430 System.String Google.Protobuf.Reflection.OneofDescriptorProto::ToString()
extern void OneofDescriptorProto_ToString_mE05FEA563B6444CC0150CAB7800E63589CFF3683 (void);
// 0x00000431 System.Void Google.Protobuf.Reflection.OneofDescriptorProto::WriteTo(Google.Protobuf.CodedOutputStream)
extern void OneofDescriptorProto_WriteTo_m1122F29DE940E7FDE249B8B49D0A4CE85FA88DF1 (void);
// 0x00000432 System.Void Google.Protobuf.Reflection.OneofDescriptorProto::pb::Google.Protobuf.IBufferMessage.InternalWriteTo(Google.Protobuf.WriteContext&)
extern void OneofDescriptorProto_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_mC33BBE456CCB3E50F512370278FCB60B20882B58 (void);
// 0x00000433 System.Int32 Google.Protobuf.Reflection.OneofDescriptorProto::CalculateSize()
extern void OneofDescriptorProto_CalculateSize_mF6EDD0758C04B3A0C663D458BAE874DD78BAE845 (void);
// 0x00000434 System.Void Google.Protobuf.Reflection.OneofDescriptorProto::MergeFrom(Google.Protobuf.Reflection.OneofDescriptorProto)
extern void OneofDescriptorProto_MergeFrom_m9929A6BFE74508B75D3C84F099E0473C67499803 (void);
// 0x00000435 System.Void Google.Protobuf.Reflection.OneofDescriptorProto::MergeFrom(Google.Protobuf.CodedInputStream)
extern void OneofDescriptorProto_MergeFrom_m7E5A8F79E11BF5CC5DC28F66E56916BE9C185511 (void);
// 0x00000436 System.Void Google.Protobuf.Reflection.OneofDescriptorProto::pb::Google.Protobuf.IBufferMessage.InternalMergeFrom(Google.Protobuf.ParseContext&)
extern void OneofDescriptorProto_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_mDF43CF284022D2519858B4865D13C83C21F249A4 (void);
// 0x00000437 System.Void Google.Protobuf.Reflection.OneofDescriptorProto::.cctor()
extern void OneofDescriptorProto__cctor_mA36895FBF6949FC809866F5FF0118000B3DF050C (void);
// 0x00000438 System.Void Google.Protobuf.Reflection.OneofDescriptorProto/<>c::.cctor()
extern void U3CU3Ec__cctor_m242B6FF0015724B2B43732AB2E615DAAB731EA19 (void);
// 0x00000439 System.Void Google.Protobuf.Reflection.OneofDescriptorProto/<>c::.ctor()
extern void U3CU3Ec__ctor_m5AD40BE630C1F0562A7B91052FA00678AFF64073 (void);
// 0x0000043A Google.Protobuf.Reflection.OneofDescriptorProto Google.Protobuf.Reflection.OneofDescriptorProto/<>c::<.cctor>b__36_0()
extern void U3CU3Ec_U3C_cctorU3Eb__36_0_m9B3284D41CFACF2A16341508FE1BF105891BEA82 (void);
// 0x0000043B Google.Protobuf.MessageParser`1<Google.Protobuf.Reflection.EnumDescriptorProto> Google.Protobuf.Reflection.EnumDescriptorProto::get_Parser()
extern void EnumDescriptorProto_get_Parser_m5FE24F404BAE27BD341F2424625E3F2201CF6EB0 (void);
// 0x0000043C Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.Reflection.EnumDescriptorProto::get_Descriptor()
extern void EnumDescriptorProto_get_Descriptor_mB89737A900858354FD200A724032DF8194CF571C (void);
// 0x0000043D Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.Reflection.EnumDescriptorProto::pb::Google.Protobuf.IMessage.get_Descriptor()
extern void EnumDescriptorProto_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m0BC205A88C56A3251A74F252C5D080B69215021C (void);
// 0x0000043E System.Void Google.Protobuf.Reflection.EnumDescriptorProto::.ctor()
extern void EnumDescriptorProto__ctor_mB3A28E8576A228315F116223DA06685ABD39550A (void);
// 0x0000043F System.Void Google.Protobuf.Reflection.EnumDescriptorProto::.ctor(Google.Protobuf.Reflection.EnumDescriptorProto)
extern void EnumDescriptorProto__ctor_m147F600199E44F16A8C1AA7F6991895708CF4282 (void);
// 0x00000440 Google.Protobuf.Reflection.EnumDescriptorProto Google.Protobuf.Reflection.EnumDescriptorProto::Clone()
extern void EnumDescriptorProto_Clone_mB7008C506A35ACCFE0E5D36AAD55F49F98F6E85A (void);
// 0x00000441 System.String Google.Protobuf.Reflection.EnumDescriptorProto::get_Name()
extern void EnumDescriptorProto_get_Name_m239FF7D5EB664E2BE0E535D490D9999276C6E9C7 (void);
// 0x00000442 System.Void Google.Protobuf.Reflection.EnumDescriptorProto::set_Name(System.String)
extern void EnumDescriptorProto_set_Name_m86B3EB26198B6E6AC4B74DA6FAE449D2710C4E3F (void);
// 0x00000443 System.Boolean Google.Protobuf.Reflection.EnumDescriptorProto::get_HasName()
extern void EnumDescriptorProto_get_HasName_m3C3EB6849643E5D01261B7B8A141641CECC48CF3 (void);
// 0x00000444 Google.Protobuf.Collections.RepeatedField`1<Google.Protobuf.Reflection.EnumValueDescriptorProto> Google.Protobuf.Reflection.EnumDescriptorProto::get_Value()
extern void EnumDescriptorProto_get_Value_mB8CCEB385142A84668F5C1325A5C5ECCFF7E842A (void);
// 0x00000445 Google.Protobuf.Reflection.EnumOptions Google.Protobuf.Reflection.EnumDescriptorProto::get_Options()
extern void EnumDescriptorProto_get_Options_m54DBDC48FC10B41D6455B5ED6708DA40E2D9929A (void);
// 0x00000446 System.Void Google.Protobuf.Reflection.EnumDescriptorProto::set_Options(Google.Protobuf.Reflection.EnumOptions)
extern void EnumDescriptorProto_set_Options_m817E21BD5AD5FE2050946B7F42F4D1010F661F2E (void);
// 0x00000447 System.Boolean Google.Protobuf.Reflection.EnumDescriptorProto::Equals(System.Object)
extern void EnumDescriptorProto_Equals_m6711ABED6B0C4EC1496AAE697AA452084CB3546B (void);
// 0x00000448 System.Boolean Google.Protobuf.Reflection.EnumDescriptorProto::Equals(Google.Protobuf.Reflection.EnumDescriptorProto)
extern void EnumDescriptorProto_Equals_m22E80DDF1828BF67B70CDC86776D054CB05F6F23 (void);
// 0x00000449 System.Int32 Google.Protobuf.Reflection.EnumDescriptorProto::GetHashCode()
extern void EnumDescriptorProto_GetHashCode_m7EACA529EA0BFE654F625A89C93F046781EE0F9F (void);
// 0x0000044A System.String Google.Protobuf.Reflection.EnumDescriptorProto::ToString()
extern void EnumDescriptorProto_ToString_m4FB571E200CAA7F881910FBEB583E511661A8340 (void);
// 0x0000044B System.Void Google.Protobuf.Reflection.EnumDescriptorProto::WriteTo(Google.Protobuf.CodedOutputStream)
extern void EnumDescriptorProto_WriteTo_m9D12A0C8024AA0DA17AE32C6782D2930703EEAE4 (void);
// 0x0000044C System.Void Google.Protobuf.Reflection.EnumDescriptorProto::pb::Google.Protobuf.IBufferMessage.InternalWriteTo(Google.Protobuf.WriteContext&)
extern void EnumDescriptorProto_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m14AEC404CD6EFADDE5EBA504F48275D2AA0D66F7 (void);
// 0x0000044D System.Int32 Google.Protobuf.Reflection.EnumDescriptorProto::CalculateSize()
extern void EnumDescriptorProto_CalculateSize_m9F366B59267B5766D2E4B89E64EE02E653AAF2E0 (void);
// 0x0000044E System.Void Google.Protobuf.Reflection.EnumDescriptorProto::MergeFrom(Google.Protobuf.Reflection.EnumDescriptorProto)
extern void EnumDescriptorProto_MergeFrom_mE277EA218CDEAB5A29709DA28ED6FA1B6E7A1CCC (void);
// 0x0000044F System.Void Google.Protobuf.Reflection.EnumDescriptorProto::MergeFrom(Google.Protobuf.CodedInputStream)
extern void EnumDescriptorProto_MergeFrom_mD7331F6048B5F3A9CBD9BCE98EC931224970178E (void);
// 0x00000450 System.Void Google.Protobuf.Reflection.EnumDescriptorProto::pb::Google.Protobuf.IBufferMessage.InternalMergeFrom(Google.Protobuf.ParseContext&)
extern void EnumDescriptorProto_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_mEEA70B246D0C85DA555958273DC1F8F2F977F7CF (void);
// 0x00000451 System.Void Google.Protobuf.Reflection.EnumDescriptorProto::.cctor()
extern void EnumDescriptorProto__cctor_mBBEB7BC4DCDC9A22FB4641CEE3690F4273CFDD46 (void);
// 0x00000452 Google.Protobuf.MessageParser`1<Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange> Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange::get_Parser()
extern void EnumReservedRange_get_Parser_mCA427AC75416A92344F6D66F4CCC3578CFE40DAA (void);
// 0x00000453 Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange::get_Descriptor()
extern void EnumReservedRange_get_Descriptor_mD31EA935A954A7AAF19EB17AED0FEE0644E39056 (void);
// 0x00000454 Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange::pb::Google.Protobuf.IMessage.get_Descriptor()
extern void EnumReservedRange_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_mB1D21B0C42DD2B880258B2F6190BC9EE0D8A98AD (void);
// 0x00000455 System.Void Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange::.ctor()
extern void EnumReservedRange__ctor_m02904F51FF5FE5B92BAA1E816D3A1B13534A6E0C (void);
// 0x00000456 System.Void Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange::.ctor(Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange)
extern void EnumReservedRange__ctor_m4F7BB9BB1007668742F085BDA2A82E889F6D1B6A (void);
// 0x00000457 Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange::Clone()
extern void EnumReservedRange_Clone_m0478CEBDD1CC260098613EC9608FA8DA619521A4 (void);
// 0x00000458 System.Int32 Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange::get_Start()
extern void EnumReservedRange_get_Start_m425FFA90089C7FCDD1C01FA5C950E5477F17DCC5 (void);
// 0x00000459 System.Void Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange::set_Start(System.Int32)
extern void EnumReservedRange_set_Start_m69F0A6CB6629086763121A8CD5E295415242BE82 (void);
// 0x0000045A System.Boolean Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange::get_HasStart()
extern void EnumReservedRange_get_HasStart_m394D0FDEAAE8B2C54B2B7ACE2DAFD1F354B2AFA8 (void);
// 0x0000045B System.Int32 Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange::get_End()
extern void EnumReservedRange_get_End_m012A5FA46B5F0D66FBCEAA9B17BCE31CB105E0D9 (void);
// 0x0000045C System.Void Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange::set_End(System.Int32)
extern void EnumReservedRange_set_End_mF993946771523BFE0D542CD2C3ACB19ADE9C1CC3 (void);
// 0x0000045D System.Boolean Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange::get_HasEnd()
extern void EnumReservedRange_get_HasEnd_m743B6947E1F9A8CD3CB43B5CC0056313942F1AE1 (void);
// 0x0000045E System.Boolean Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange::Equals(System.Object)
extern void EnumReservedRange_Equals_m3728912F8E0AD0EA5A74FD69323CF9F73465DA04 (void);
// 0x0000045F System.Boolean Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange::Equals(Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange)
extern void EnumReservedRange_Equals_m1FB9B43A21E9D232DFE20485F50C4673D84948E1 (void);
// 0x00000460 System.Int32 Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange::GetHashCode()
extern void EnumReservedRange_GetHashCode_m6415E55F96EE5B92BBE2965CD1FE5D37C2604231 (void);
// 0x00000461 System.String Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange::ToString()
extern void EnumReservedRange_ToString_mA9995D4DB4A84520B09678054B780C0714C18E32 (void);
// 0x00000462 System.Void Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange::WriteTo(Google.Protobuf.CodedOutputStream)
extern void EnumReservedRange_WriteTo_m7D1FE13C055716582D6BC6B24B10AE1654DA1316 (void);
// 0x00000463 System.Void Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange::pb::Google.Protobuf.IBufferMessage.InternalWriteTo(Google.Protobuf.WriteContext&)
extern void EnumReservedRange_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m40335111DC4E167612FEF3662BE9124300D3480A (void);
// 0x00000464 System.Int32 Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange::CalculateSize()
extern void EnumReservedRange_CalculateSize_m0A62C551FDA64908B0C58397CD1428C5C71BF2F5 (void);
// 0x00000465 System.Void Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange::MergeFrom(Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange)
extern void EnumReservedRange_MergeFrom_m2FC3A667F2AF4C85C2C7D3AF0E4E471D92019CD2 (void);
// 0x00000466 System.Void Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange::MergeFrom(Google.Protobuf.CodedInputStream)
extern void EnumReservedRange_MergeFrom_mCD0205C8ED2CEB21A1C6F16D967019975292C0A0 (void);
// 0x00000467 System.Void Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange::pb::Google.Protobuf.IBufferMessage.InternalMergeFrom(Google.Protobuf.ParseContext&)
extern void EnumReservedRange_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_mDCF2D669D389A95648C8864C743EBF87D6CAC659 (void);
// 0x00000468 System.Void Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange::.cctor()
extern void EnumReservedRange__cctor_m97E50FAA53B7507FE0785E95741F3780277FD731 (void);
// 0x00000469 System.Void Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange/<>c::.cctor()
extern void U3CU3Ec__cctor_m4FA27EDEEFE8562E0CC992077B8C1D26C49865FA (void);
// 0x0000046A System.Void Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange/<>c::.ctor()
extern void U3CU3Ec__ctor_m9D85A171DC63EF24760F067CDB4D72BDE4E10F18 (void);
// 0x0000046B Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange Google.Protobuf.Reflection.EnumDescriptorProto/Types/EnumReservedRange/<>c::<.cctor>b__41_0()
extern void U3CU3Ec_U3C_cctorU3Eb__41_0_m0FBA9F917C842E30E53B32B2B8E897821F32A8C3 (void);
// 0x0000046C System.Void Google.Protobuf.Reflection.EnumDescriptorProto/<>c::.cctor()
extern void U3CU3Ec__cctor_mF24FCE934E414464CBEED3D1CD4FE729C6329AE2 (void);
// 0x0000046D System.Void Google.Protobuf.Reflection.EnumDescriptorProto/<>c::.ctor()
extern void U3CU3Ec__ctor_m1CCAEB2DF4233470E1D07108642ADA2076F28191 (void);
// 0x0000046E Google.Protobuf.Reflection.EnumDescriptorProto Google.Protobuf.Reflection.EnumDescriptorProto/<>c::<.cctor>b__52_0()
extern void U3CU3Ec_U3C_cctorU3Eb__52_0_m2EF60F0DEB12598A39B13FE3EFF834AD9A2057BD (void);
// 0x0000046F Google.Protobuf.MessageParser`1<Google.Protobuf.Reflection.EnumValueDescriptorProto> Google.Protobuf.Reflection.EnumValueDescriptorProto::get_Parser()
extern void EnumValueDescriptorProto_get_Parser_m736581710BC0F2AF5F05E5D5E6BC467A063C433E (void);
// 0x00000470 Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.Reflection.EnumValueDescriptorProto::get_Descriptor()
extern void EnumValueDescriptorProto_get_Descriptor_m146373C8AA663CEDF40BC9946A83EE3F670300D9 (void);
// 0x00000471 Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.Reflection.EnumValueDescriptorProto::pb::Google.Protobuf.IMessage.get_Descriptor()
extern void EnumValueDescriptorProto_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_mCFE0C5CFECCD5C3E838C28DAFBB70451CC9FDB6B (void);
// 0x00000472 System.Void Google.Protobuf.Reflection.EnumValueDescriptorProto::.ctor()
extern void EnumValueDescriptorProto__ctor_mCD6381E84158229867FFB9D4C43817B859E9A183 (void);
// 0x00000473 System.Void Google.Protobuf.Reflection.EnumValueDescriptorProto::.ctor(Google.Protobuf.Reflection.EnumValueDescriptorProto)
extern void EnumValueDescriptorProto__ctor_m0E2CA6496FBAE77FB5E1F05802586BD4BE3A3D3D (void);
// 0x00000474 Google.Protobuf.Reflection.EnumValueDescriptorProto Google.Protobuf.Reflection.EnumValueDescriptorProto::Clone()
extern void EnumValueDescriptorProto_Clone_m1611AE2A72549FFE60B8119A55D815BA696E862D (void);
// 0x00000475 System.String Google.Protobuf.Reflection.EnumValueDescriptorProto::get_Name()
extern void EnumValueDescriptorProto_get_Name_m4D3B8EBCC09CB1F0B22D0350125C38EEB087B34B (void);
// 0x00000476 System.Void Google.Protobuf.Reflection.EnumValueDescriptorProto::set_Name(System.String)
extern void EnumValueDescriptorProto_set_Name_mFB204B06E511114E6E238BB93C90771FB6C254A4 (void);
// 0x00000477 System.Boolean Google.Protobuf.Reflection.EnumValueDescriptorProto::get_HasName()
extern void EnumValueDescriptorProto_get_HasName_mA03E99F8576641D0A97E44CAEF6082E2A85E28C6 (void);
// 0x00000478 System.Int32 Google.Protobuf.Reflection.EnumValueDescriptorProto::get_Number()
extern void EnumValueDescriptorProto_get_Number_m2658C0963EEB574D8E6BF71C97475DBDBC93B2DE (void);
// 0x00000479 System.Void Google.Protobuf.Reflection.EnumValueDescriptorProto::set_Number(System.Int32)
extern void EnumValueDescriptorProto_set_Number_mF3E1231A22B71F47DFD2BD8D29FE562EB13054E7 (void);
// 0x0000047A System.Boolean Google.Protobuf.Reflection.EnumValueDescriptorProto::get_HasNumber()
extern void EnumValueDescriptorProto_get_HasNumber_mE73E6803CF10AF23F69DC74FF664557B2F0AFF80 (void);
// 0x0000047B Google.Protobuf.Reflection.EnumValueOptions Google.Protobuf.Reflection.EnumValueDescriptorProto::get_Options()
extern void EnumValueDescriptorProto_get_Options_mEB9A81DBD16E5E0E884D41FBD603AC0912439FA5 (void);
// 0x0000047C System.Void Google.Protobuf.Reflection.EnumValueDescriptorProto::set_Options(Google.Protobuf.Reflection.EnumValueOptions)
extern void EnumValueDescriptorProto_set_Options_m2D4E0DF7E53ED5FB52F2FEE5C1EADA4827B66B7F (void);
// 0x0000047D System.Boolean Google.Protobuf.Reflection.EnumValueDescriptorProto::Equals(System.Object)
extern void EnumValueDescriptorProto_Equals_mAEBB3D490CDECFB762189684B4B64227E2759C07 (void);
// 0x0000047E System.Boolean Google.Protobuf.Reflection.EnumValueDescriptorProto::Equals(Google.Protobuf.Reflection.EnumValueDescriptorProto)
extern void EnumValueDescriptorProto_Equals_m4676E76A2633C7C43EA50E9A532A5CFBAA32C1BF (void);
// 0x0000047F System.Int32 Google.Protobuf.Reflection.EnumValueDescriptorProto::GetHashCode()
extern void EnumValueDescriptorProto_GetHashCode_mB2267753F8624960C3BE6FEF07DE4F221E82E7D0 (void);
// 0x00000480 System.String Google.Protobuf.Reflection.EnumValueDescriptorProto::ToString()
extern void EnumValueDescriptorProto_ToString_m2A605BE66BAB0ADDB88DF7F6D39D38E4AE30E399 (void);
// 0x00000481 System.Void Google.Protobuf.Reflection.EnumValueDescriptorProto::WriteTo(Google.Protobuf.CodedOutputStream)
extern void EnumValueDescriptorProto_WriteTo_m628970DB0F15CF7E6A2C600B877D999E92B0AF30 (void);
// 0x00000482 System.Void Google.Protobuf.Reflection.EnumValueDescriptorProto::pb::Google.Protobuf.IBufferMessage.InternalWriteTo(Google.Protobuf.WriteContext&)
extern void EnumValueDescriptorProto_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m742B37B6C10FF627FF03157232ABA74C1918CEA7 (void);
// 0x00000483 System.Int32 Google.Protobuf.Reflection.EnumValueDescriptorProto::CalculateSize()
extern void EnumValueDescriptorProto_CalculateSize_m1F7A660E62D7576C260035086428BF3452D89C36 (void);
// 0x00000484 System.Void Google.Protobuf.Reflection.EnumValueDescriptorProto::MergeFrom(Google.Protobuf.Reflection.EnumValueDescriptorProto)
extern void EnumValueDescriptorProto_MergeFrom_mC16859FC588043BE69253243292C173544C0BC14 (void);
// 0x00000485 System.Void Google.Protobuf.Reflection.EnumValueDescriptorProto::MergeFrom(Google.Protobuf.CodedInputStream)
extern void EnumValueDescriptorProto_MergeFrom_m415B9988016FE0741689B94605032A8CD4033362 (void);
// 0x00000486 System.Void Google.Protobuf.Reflection.EnumValueDescriptorProto::pb::Google.Protobuf.IBufferMessage.InternalMergeFrom(Google.Protobuf.ParseContext&)
extern void EnumValueDescriptorProto_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m586D380CAEE945088D1FDBB1C08489F63B823E5D (void);
// 0x00000487 System.Void Google.Protobuf.Reflection.EnumValueDescriptorProto::.cctor()
extern void EnumValueDescriptorProto__cctor_m38CBBCC0C5F4CECD79755881C364BADB96F73ADD (void);
// 0x00000488 System.Void Google.Protobuf.Reflection.EnumValueDescriptorProto/<>c::.cctor()
extern void U3CU3Ec__cctor_mBFE3CF17B8E60367972AC1782280497A1E4DD08F (void);
// 0x00000489 System.Void Google.Protobuf.Reflection.EnumValueDescriptorProto/<>c::.ctor()
extern void U3CU3Ec__ctor_m742974F2783A1F0967EDADBE5B0BDD981BEE9997 (void);
// 0x0000048A Google.Protobuf.Reflection.EnumValueDescriptorProto Google.Protobuf.Reflection.EnumValueDescriptorProto/<>c::<.cctor>b__46_0()
extern void U3CU3Ec_U3C_cctorU3Eb__46_0_mB3936AABD2C18F0C5B2CBD1B38489783D4690BB6 (void);
// 0x0000048B Google.Protobuf.MessageParser`1<Google.Protobuf.Reflection.ServiceDescriptorProto> Google.Protobuf.Reflection.ServiceDescriptorProto::get_Parser()
extern void ServiceDescriptorProto_get_Parser_mC303B987ABAB6F64E367D474BA45F890394438A0 (void);
// 0x0000048C Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.Reflection.ServiceDescriptorProto::get_Descriptor()
extern void ServiceDescriptorProto_get_Descriptor_mA67C62EEBA7D28C74F621B11784459C60E940606 (void);
// 0x0000048D Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.Reflection.ServiceDescriptorProto::pb::Google.Protobuf.IMessage.get_Descriptor()
extern void ServiceDescriptorProto_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m72780D4209A552D65E0B2EDC9AF2F74396F945BA (void);
// 0x0000048E System.Void Google.Protobuf.Reflection.ServiceDescriptorProto::.ctor()
extern void ServiceDescriptorProto__ctor_m2ABEEBBB877E11DC2EB775785478B01367CEFDC5 (void);
// 0x0000048F System.Void Google.Protobuf.Reflection.ServiceDescriptorProto::.ctor(Google.Protobuf.Reflection.ServiceDescriptorProto)
extern void ServiceDescriptorProto__ctor_mF76948802201FEE2ACD8D67A55B5C75EBFF5FDF1 (void);
// 0x00000490 Google.Protobuf.Reflection.ServiceDescriptorProto Google.Protobuf.Reflection.ServiceDescriptorProto::Clone()
extern void ServiceDescriptorProto_Clone_mE601AB9C3641148DB0AC9C3B6BD5F3281DC57109 (void);
// 0x00000491 System.String Google.Protobuf.Reflection.ServiceDescriptorProto::get_Name()
extern void ServiceDescriptorProto_get_Name_m2CA082476B1455CAA4307E364D0806D815297114 (void);
// 0x00000492 System.Void Google.Protobuf.Reflection.ServiceDescriptorProto::set_Name(System.String)
extern void ServiceDescriptorProto_set_Name_m6F0DC197D53F8ADC16E042DD3FAFA07659E22D42 (void);
// 0x00000493 System.Boolean Google.Protobuf.Reflection.ServiceDescriptorProto::get_HasName()
extern void ServiceDescriptorProto_get_HasName_m348F6D83EF5F66D7B494D3C15604109B29341484 (void);
// 0x00000494 Google.Protobuf.Collections.RepeatedField`1<Google.Protobuf.Reflection.MethodDescriptorProto> Google.Protobuf.Reflection.ServiceDescriptorProto::get_Method()
extern void ServiceDescriptorProto_get_Method_m95D93BB86E2947C96B1B9D01D068A695307053E4 (void);
// 0x00000495 Google.Protobuf.Reflection.ServiceOptions Google.Protobuf.Reflection.ServiceDescriptorProto::get_Options()
extern void ServiceDescriptorProto_get_Options_m59C55E0142DCA61D9BF7F0E6BBB356F8872BAE8C (void);
// 0x00000496 System.Void Google.Protobuf.Reflection.ServiceDescriptorProto::set_Options(Google.Protobuf.Reflection.ServiceOptions)
extern void ServiceDescriptorProto_set_Options_m620243EB5D6716CEE0E1C61A6B5808830C1EB302 (void);
// 0x00000497 System.Boolean Google.Protobuf.Reflection.ServiceDescriptorProto::Equals(System.Object)
extern void ServiceDescriptorProto_Equals_m5C75FE57DE6DE100164657FB1129840A9B9489AB (void);
// 0x00000498 System.Boolean Google.Protobuf.Reflection.ServiceDescriptorProto::Equals(Google.Protobuf.Reflection.ServiceDescriptorProto)
extern void ServiceDescriptorProto_Equals_mFBE6BA3EC312B0C6F693C79A2D723347CD013C55 (void);
// 0x00000499 System.Int32 Google.Protobuf.Reflection.ServiceDescriptorProto::GetHashCode()
extern void ServiceDescriptorProto_GetHashCode_m7BA75A38679BF8C53EA10F293060F72BFDC46396 (void);
// 0x0000049A System.String Google.Protobuf.Reflection.ServiceDescriptorProto::ToString()
extern void ServiceDescriptorProto_ToString_m97875482BD469952825E67CBE1ACB629241E775A (void);
// 0x0000049B System.Void Google.Protobuf.Reflection.ServiceDescriptorProto::WriteTo(Google.Protobuf.CodedOutputStream)
extern void ServiceDescriptorProto_WriteTo_m4D6E98E4F13566154B1EBFB0309D2DF3A4B71A38 (void);
// 0x0000049C System.Void Google.Protobuf.Reflection.ServiceDescriptorProto::pb::Google.Protobuf.IBufferMessage.InternalWriteTo(Google.Protobuf.WriteContext&)
extern void ServiceDescriptorProto_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m4E48FC87B5F4637449255AF403AAF0971BD9FF91 (void);
// 0x0000049D System.Int32 Google.Protobuf.Reflection.ServiceDescriptorProto::CalculateSize()
extern void ServiceDescriptorProto_CalculateSize_mFD1610268B60E21FDA5CA531B00065C1E4A6AB8E (void);
// 0x0000049E System.Void Google.Protobuf.Reflection.ServiceDescriptorProto::MergeFrom(Google.Protobuf.Reflection.ServiceDescriptorProto)
extern void ServiceDescriptorProto_MergeFrom_m6581ECAC1AF5ECDFFCDEC9A551D9F71BCEC0005B (void);
// 0x0000049F System.Void Google.Protobuf.Reflection.ServiceDescriptorProto::MergeFrom(Google.Protobuf.CodedInputStream)
extern void ServiceDescriptorProto_MergeFrom_mA53EA15C18F1199B95F5F1DF3AD52A81F6E5AB7E (void);
// 0x000004A0 System.Void Google.Protobuf.Reflection.ServiceDescriptorProto::pb::Google.Protobuf.IBufferMessage.InternalMergeFrom(Google.Protobuf.ParseContext&)
extern void ServiceDescriptorProto_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m70DB24C4CDA1363FF025EA7F7CBE3449C1A4C49C (void);
// 0x000004A1 System.Void Google.Protobuf.Reflection.ServiceDescriptorProto::.cctor()
extern void ServiceDescriptorProto__cctor_m5D33924606924480FEC2470E35B57960B0DA560C (void);
// 0x000004A2 System.Void Google.Protobuf.Reflection.ServiceDescriptorProto/<>c::.cctor()
extern void U3CU3Ec__cctor_m8AA62DE218C9E50A4C74E5558316F4481A8462A6 (void);
// 0x000004A3 System.Void Google.Protobuf.Reflection.ServiceDescriptorProto/<>c::.ctor()
extern void U3CU3Ec__ctor_m500563BC829DEC9ED764001F0FAE0B0022EA1311 (void);
// 0x000004A4 Google.Protobuf.Reflection.ServiceDescriptorProto Google.Protobuf.Reflection.ServiceDescriptorProto/<>c::<.cctor>b__41_0()
extern void U3CU3Ec_U3C_cctorU3Eb__41_0_mB6AC9111A732B674814B82F419F830E4881F83B4 (void);
// 0x000004A5 Google.Protobuf.MessageParser`1<Google.Protobuf.Reflection.MethodDescriptorProto> Google.Protobuf.Reflection.MethodDescriptorProto::get_Parser()
extern void MethodDescriptorProto_get_Parser_m33F87B49E86519177E43BAC6C8B3AC22C81BA120 (void);
// 0x000004A6 Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.Reflection.MethodDescriptorProto::get_Descriptor()
extern void MethodDescriptorProto_get_Descriptor_m9C3F7BFEED0E7AAB045476A0DE1C80FDBA812E0B (void);
// 0x000004A7 Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.Reflection.MethodDescriptorProto::pb::Google.Protobuf.IMessage.get_Descriptor()
extern void MethodDescriptorProto_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_mBDBFD0BA05C9C3B6D89862D779A5EDC34A0BA98C (void);
// 0x000004A8 System.Void Google.Protobuf.Reflection.MethodDescriptorProto::.ctor()
extern void MethodDescriptorProto__ctor_m3181F7006234380068049FEDB5E800625B8193C8 (void);
// 0x000004A9 System.Void Google.Protobuf.Reflection.MethodDescriptorProto::.ctor(Google.Protobuf.Reflection.MethodDescriptorProto)
extern void MethodDescriptorProto__ctor_mEF5E8090FBE2C5FB52278A283B26A67284FBD781 (void);
// 0x000004AA Google.Protobuf.Reflection.MethodDescriptorProto Google.Protobuf.Reflection.MethodDescriptorProto::Clone()
extern void MethodDescriptorProto_Clone_mA90669FE9E8DE792CEC9323E367F334926F95A21 (void);
// 0x000004AB System.String Google.Protobuf.Reflection.MethodDescriptorProto::get_Name()
extern void MethodDescriptorProto_get_Name_m2BFA476C2A5B5DA54D12CC4C923453098821B361 (void);
// 0x000004AC System.Void Google.Protobuf.Reflection.MethodDescriptorProto::set_Name(System.String)
extern void MethodDescriptorProto_set_Name_mE535380BD9B436D4001611F5F17EE62BB5756ABA (void);
// 0x000004AD System.Boolean Google.Protobuf.Reflection.MethodDescriptorProto::get_HasName()
extern void MethodDescriptorProto_get_HasName_m87477C570158C989CFE5916FA1998FFE8435BA5E (void);
// 0x000004AE System.String Google.Protobuf.Reflection.MethodDescriptorProto::get_InputType()
extern void MethodDescriptorProto_get_InputType_m624EC07E1B8E70B8223DB68B7172551912E61184 (void);
// 0x000004AF System.Void Google.Protobuf.Reflection.MethodDescriptorProto::set_InputType(System.String)
extern void MethodDescriptorProto_set_InputType_mCCE83CEDF67EDFE5B94902EC8B14ABA22B6076DD (void);
// 0x000004B0 System.Boolean Google.Protobuf.Reflection.MethodDescriptorProto::get_HasInputType()
extern void MethodDescriptorProto_get_HasInputType_mD4C609F5B5EFE2E14E0E831341E9ECB040D90336 (void);
// 0x000004B1 System.String Google.Protobuf.Reflection.MethodDescriptorProto::get_OutputType()
extern void MethodDescriptorProto_get_OutputType_m0EAA2C37BEC6E3719FFD4D56D37C10BC2AE4E004 (void);
// 0x000004B2 System.Void Google.Protobuf.Reflection.MethodDescriptorProto::set_OutputType(System.String)
extern void MethodDescriptorProto_set_OutputType_m621B8EBDBD403F20BF4DC5CC38B1189FFE7D7E30 (void);
// 0x000004B3 System.Boolean Google.Protobuf.Reflection.MethodDescriptorProto::get_HasOutputType()
extern void MethodDescriptorProto_get_HasOutputType_mF6C592B099C3A481A06FB8ADE2F2F4C2503D8B3A (void);
// 0x000004B4 Google.Protobuf.Reflection.MethodOptions Google.Protobuf.Reflection.MethodDescriptorProto::get_Options()
extern void MethodDescriptorProto_get_Options_m0524F118FC671D80EA9BE19F9B2E4B7E2F264DC4 (void);
// 0x000004B5 System.Void Google.Protobuf.Reflection.MethodDescriptorProto::set_Options(Google.Protobuf.Reflection.MethodOptions)
extern void MethodDescriptorProto_set_Options_mA972CB7C00D36504F42C0A7EA333696B665DF185 (void);
// 0x000004B6 System.Boolean Google.Protobuf.Reflection.MethodDescriptorProto::get_ClientStreaming()
extern void MethodDescriptorProto_get_ClientStreaming_mDD3445121C38886F66D3AD2817BB1C73F87D8BA3 (void);
// 0x000004B7 System.Void Google.Protobuf.Reflection.MethodDescriptorProto::set_ClientStreaming(System.Boolean)
extern void MethodDescriptorProto_set_ClientStreaming_m1DF1AEA16E674C69657697F16EDFB7E84CE271AC (void);
// 0x000004B8 System.Boolean Google.Protobuf.Reflection.MethodDescriptorProto::get_HasClientStreaming()
extern void MethodDescriptorProto_get_HasClientStreaming_m24B36114B891184073831BDB2FC41DD8666A7248 (void);
// 0x000004B9 System.Boolean Google.Protobuf.Reflection.MethodDescriptorProto::get_ServerStreaming()
extern void MethodDescriptorProto_get_ServerStreaming_m41152F7FB02BA7B27E28CA920118107536EB1FD6 (void);
// 0x000004BA System.Void Google.Protobuf.Reflection.MethodDescriptorProto::set_ServerStreaming(System.Boolean)
extern void MethodDescriptorProto_set_ServerStreaming_m97B11280B7BF0E4CC0333240EE49A41CAEC5029A (void);
// 0x000004BB System.Boolean Google.Protobuf.Reflection.MethodDescriptorProto::get_HasServerStreaming()
extern void MethodDescriptorProto_get_HasServerStreaming_m3929AD6103E1FAA56B3EC8EF7F9E95761AEED20F (void);
// 0x000004BC System.Boolean Google.Protobuf.Reflection.MethodDescriptorProto::Equals(System.Object)
extern void MethodDescriptorProto_Equals_mE404C60BAAC311AF6BC9E81457AE4901A2EC69F1 (void);
// 0x000004BD System.Boolean Google.Protobuf.Reflection.MethodDescriptorProto::Equals(Google.Protobuf.Reflection.MethodDescriptorProto)
extern void MethodDescriptorProto_Equals_m15C523245227F5007A8438DA67437535DADCA8A0 (void);
// 0x000004BE System.Int32 Google.Protobuf.Reflection.MethodDescriptorProto::GetHashCode()
extern void MethodDescriptorProto_GetHashCode_mBE5452AA6B4265114D0C57FDC8BC4496C01211CB (void);
// 0x000004BF System.String Google.Protobuf.Reflection.MethodDescriptorProto::ToString()
extern void MethodDescriptorProto_ToString_mF5CA9EB9E35C30D1141628CDA8D36F3183524337 (void);
// 0x000004C0 System.Void Google.Protobuf.Reflection.MethodDescriptorProto::WriteTo(Google.Protobuf.CodedOutputStream)
extern void MethodDescriptorProto_WriteTo_m81FE666F3F825DFE70F6A79A3E8EC18E54F3CBBA (void);
// 0x000004C1 System.Void Google.Protobuf.Reflection.MethodDescriptorProto::pb::Google.Protobuf.IBufferMessage.InternalWriteTo(Google.Protobuf.WriteContext&)
extern void MethodDescriptorProto_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m3D279F4D543CF3AEDDA8A726D1DDE892CF58D8D2 (void);
// 0x000004C2 System.Int32 Google.Protobuf.Reflection.MethodDescriptorProto::CalculateSize()
extern void MethodDescriptorProto_CalculateSize_mD02B74F49B50CFCB0A8504ACE320C81D1815D622 (void);
// 0x000004C3 System.Void Google.Protobuf.Reflection.MethodDescriptorProto::MergeFrom(Google.Protobuf.Reflection.MethodDescriptorProto)
extern void MethodDescriptorProto_MergeFrom_m2B10CD2A53C04E575D652B444AF50326300A36C6 (void);
// 0x000004C4 System.Void Google.Protobuf.Reflection.MethodDescriptorProto::MergeFrom(Google.Protobuf.CodedInputStream)
extern void MethodDescriptorProto_MergeFrom_mB331EAAF45C06DE2158A063B329836049B00650A (void);
// 0x000004C5 System.Void Google.Protobuf.Reflection.MethodDescriptorProto::pb::Google.Protobuf.IBufferMessage.InternalMergeFrom(Google.Protobuf.ParseContext&)
extern void MethodDescriptorProto_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_mEEC631A9308E9E0931247AD1FC67A84AD79F7E88 (void);
// 0x000004C6 System.Void Google.Protobuf.Reflection.MethodDescriptorProto::.cctor()
extern void MethodDescriptorProto__cctor_m83F215670109EA13720E3F066D4C28E7632DCF10 (void);
// 0x000004C7 System.Void Google.Protobuf.Reflection.MethodDescriptorProto/<>c::.cctor()
extern void U3CU3Ec__cctor_mE42352A3A7C05E9A32494055E42FCCC08F0CEA33 (void);
// 0x000004C8 System.Void Google.Protobuf.Reflection.MethodDescriptorProto/<>c::.ctor()
extern void U3CU3Ec__ctor_m45798C39C0D5980147ACA9522D5212C9A9B817AB (void);
// 0x000004C9 Google.Protobuf.Reflection.MethodDescriptorProto Google.Protobuf.Reflection.MethodDescriptorProto/<>c::<.cctor>b__73_0()
extern void U3CU3Ec_U3C_cctorU3Eb__73_0_m6A24E6A87D6014A86D7AF2BDCA311405278FC48C (void);
// 0x000004CA Google.Protobuf.MessageParser`1<Google.Protobuf.Reflection.FileOptions> Google.Protobuf.Reflection.FileOptions::get_Parser()
extern void FileOptions_get_Parser_m587B87D900DF3D95A6AE812D799D31545B4C2ED1 (void);
// 0x000004CB Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.Reflection.FileOptions::get_Descriptor()
extern void FileOptions_get_Descriptor_m713F7027607716CA4C97E854D9069EBB2C36FCD9 (void);
// 0x000004CC Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.Reflection.FileOptions::pb::Google.Protobuf.IMessage.get_Descriptor()
extern void FileOptions_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m2AFDE55473E18F7F871B408A286EFB5C5D29D99F (void);
// 0x000004CD System.Void Google.Protobuf.Reflection.FileOptions::.ctor()
extern void FileOptions__ctor_m53E8E18F45026B68293C1D6E27ADD3C809A81E75 (void);
// 0x000004CE System.Void Google.Protobuf.Reflection.FileOptions::.ctor(Google.Protobuf.Reflection.FileOptions)
extern void FileOptions__ctor_mE14316BC374AEFD5371F0306BB87D7635BE51612 (void);
// 0x000004CF Google.Protobuf.Reflection.FileOptions Google.Protobuf.Reflection.FileOptions::Clone()
extern void FileOptions_Clone_m4AE4FD9F2AA9ACE2E4FEA61EF8CE2996995E6D79 (void);
// 0x000004D0 System.String Google.Protobuf.Reflection.FileOptions::get_JavaPackage()
extern void FileOptions_get_JavaPackage_mD28C975B137594B1A06CD71877B582E00047EAFF (void);
// 0x000004D1 System.Void Google.Protobuf.Reflection.FileOptions::set_JavaPackage(System.String)
extern void FileOptions_set_JavaPackage_m4C2D8A8713B941E8024E0A4D9D69216F96D22AA4 (void);
// 0x000004D2 System.Boolean Google.Protobuf.Reflection.FileOptions::get_HasJavaPackage()
extern void FileOptions_get_HasJavaPackage_m0CF95ACB5F2EEB4452E36CC18B3E9E2754996C13 (void);
// 0x000004D3 System.String Google.Protobuf.Reflection.FileOptions::get_JavaOuterClassname()
extern void FileOptions_get_JavaOuterClassname_mC228C2687E34392FC2AD06D987BF11887BA90365 (void);
// 0x000004D4 System.Void Google.Protobuf.Reflection.FileOptions::set_JavaOuterClassname(System.String)
extern void FileOptions_set_JavaOuterClassname_mACBCB1B833F08F938F8DC77F699C8BDC13943565 (void);
// 0x000004D5 System.Boolean Google.Protobuf.Reflection.FileOptions::get_HasJavaOuterClassname()
extern void FileOptions_get_HasJavaOuterClassname_mCCC09DA98F0EFE916F6F5B021F7E435F64841689 (void);
// 0x000004D6 System.Boolean Google.Protobuf.Reflection.FileOptions::get_JavaMultipleFiles()
extern void FileOptions_get_JavaMultipleFiles_m54C438931C320DCE01E37ACD53B3EC00A996A130 (void);
// 0x000004D7 System.Void Google.Protobuf.Reflection.FileOptions::set_JavaMultipleFiles(System.Boolean)
extern void FileOptions_set_JavaMultipleFiles_mAB89868C1A5BDE795E2EB58FBFBADB44ED4077E5 (void);
// 0x000004D8 System.Boolean Google.Protobuf.Reflection.FileOptions::get_HasJavaMultipleFiles()
extern void FileOptions_get_HasJavaMultipleFiles_mC4CBB93CF269855063DB6D402B183971C1EE39F7 (void);
// 0x000004D9 System.Boolean Google.Protobuf.Reflection.FileOptions::get_JavaGenerateEqualsAndHash()
extern void FileOptions_get_JavaGenerateEqualsAndHash_m1A05435B25FDC559CF64163E210308D8993FEF6F (void);
// 0x000004DA System.Void Google.Protobuf.Reflection.FileOptions::set_JavaGenerateEqualsAndHash(System.Boolean)
extern void FileOptions_set_JavaGenerateEqualsAndHash_m054D64AD6F01F060D1298F1A0EA2806D07B8C3AA (void);
// 0x000004DB System.Boolean Google.Protobuf.Reflection.FileOptions::get_HasJavaGenerateEqualsAndHash()
extern void FileOptions_get_HasJavaGenerateEqualsAndHash_mB6A19D104A2EA0440A049969861450B3BBD126A3 (void);
// 0x000004DC System.Boolean Google.Protobuf.Reflection.FileOptions::get_JavaStringCheckUtf8()
extern void FileOptions_get_JavaStringCheckUtf8_mF7300DBDC375B34FE98BC5FDABDCF3CB03D9086A (void);
// 0x000004DD System.Void Google.Protobuf.Reflection.FileOptions::set_JavaStringCheckUtf8(System.Boolean)
extern void FileOptions_set_JavaStringCheckUtf8_mF3D2C16F0B9CA1A659EC4B28ED19110E5E5400FA (void);
// 0x000004DE System.Boolean Google.Protobuf.Reflection.FileOptions::get_HasJavaStringCheckUtf8()
extern void FileOptions_get_HasJavaStringCheckUtf8_m1F7D4ED06293C20BD49A26D12EAF6BDC3DF1B818 (void);
// 0x000004DF Google.Protobuf.Reflection.FileOptions/Types/OptimizeMode Google.Protobuf.Reflection.FileOptions::get_OptimizeFor()
extern void FileOptions_get_OptimizeFor_m1080B2062A0FDACE602AABC7B1ED8702DD3997D0 (void);
// 0x000004E0 System.Void Google.Protobuf.Reflection.FileOptions::set_OptimizeFor(Google.Protobuf.Reflection.FileOptions/Types/OptimizeMode)
extern void FileOptions_set_OptimizeFor_mC9FFBF890C9174B432DDC78E43A71BCA6C5AC301 (void);
// 0x000004E1 System.Boolean Google.Protobuf.Reflection.FileOptions::get_HasOptimizeFor()
extern void FileOptions_get_HasOptimizeFor_m2BABE8FEF9437D386D8FD7183BC3F99638827263 (void);
// 0x000004E2 System.String Google.Protobuf.Reflection.FileOptions::get_GoPackage()
extern void FileOptions_get_GoPackage_m1CEB4D63D857A8B07A43F669FFCE5A873E131006 (void);
// 0x000004E3 System.Void Google.Protobuf.Reflection.FileOptions::set_GoPackage(System.String)
extern void FileOptions_set_GoPackage_m6AA83587E1434ED366B11E31822E99D084ECF00D (void);
// 0x000004E4 System.Boolean Google.Protobuf.Reflection.FileOptions::get_HasGoPackage()
extern void FileOptions_get_HasGoPackage_m0E38B6F1288817361D48841ECA8D915BFB3CAD86 (void);
// 0x000004E5 System.Boolean Google.Protobuf.Reflection.FileOptions::get_CcGenericServices()
extern void FileOptions_get_CcGenericServices_mB3F1D5A2E0D7C5A184943EA1C7964A2FAC7D4A80 (void);
// 0x000004E6 System.Void Google.Protobuf.Reflection.FileOptions::set_CcGenericServices(System.Boolean)
extern void FileOptions_set_CcGenericServices_m5C83601CC00B6B5DD7C229291C49F57CB0FF38EE (void);
// 0x000004E7 System.Boolean Google.Protobuf.Reflection.FileOptions::get_HasCcGenericServices()
extern void FileOptions_get_HasCcGenericServices_m2DA36E1DEAE50843DBB9E587DB839732D5B82FCC (void);
// 0x000004E8 System.Boolean Google.Protobuf.Reflection.FileOptions::get_JavaGenericServices()
extern void FileOptions_get_JavaGenericServices_m90F59166A912AAF683D36B6BC2B6680F355DC466 (void);
// 0x000004E9 System.Void Google.Protobuf.Reflection.FileOptions::set_JavaGenericServices(System.Boolean)
extern void FileOptions_set_JavaGenericServices_m774C5C12C2BDA83FA632B50C362773A738D46DDD (void);
// 0x000004EA System.Boolean Google.Protobuf.Reflection.FileOptions::get_HasJavaGenericServices()
extern void FileOptions_get_HasJavaGenericServices_m383D1E2AB00C08219D174DC2E685A7E4CBCE0008 (void);
// 0x000004EB System.Boolean Google.Protobuf.Reflection.FileOptions::get_PyGenericServices()
extern void FileOptions_get_PyGenericServices_mE14DF5C60BDB3E4B42281019939D4582D291364A (void);
// 0x000004EC System.Void Google.Protobuf.Reflection.FileOptions::set_PyGenericServices(System.Boolean)
extern void FileOptions_set_PyGenericServices_m88EC0EEF66167798DD20331211D07DC628E61B71 (void);
// 0x000004ED System.Boolean Google.Protobuf.Reflection.FileOptions::get_HasPyGenericServices()
extern void FileOptions_get_HasPyGenericServices_m60552C76BCAEFD77B73EE64AD3BC594FB9ECDD91 (void);
// 0x000004EE System.Boolean Google.Protobuf.Reflection.FileOptions::get_PhpGenericServices()
extern void FileOptions_get_PhpGenericServices_mDCD5D0BF4C976F4A719C05E10009D699D9141CFF (void);
// 0x000004EF System.Void Google.Protobuf.Reflection.FileOptions::set_PhpGenericServices(System.Boolean)
extern void FileOptions_set_PhpGenericServices_m1802BF8CF6B99D94E4C932FDD08C9EC09F016451 (void);
// 0x000004F0 System.Boolean Google.Protobuf.Reflection.FileOptions::get_HasPhpGenericServices()
extern void FileOptions_get_HasPhpGenericServices_m83B42571440FD10507A557B11D64146DAAD723B2 (void);
// 0x000004F1 System.Boolean Google.Protobuf.Reflection.FileOptions::get_Deprecated()
extern void FileOptions_get_Deprecated_mD3EFBEBFE2D806322D2F1F7C97BDFB23872FB959 (void);
// 0x000004F2 System.Void Google.Protobuf.Reflection.FileOptions::set_Deprecated(System.Boolean)
extern void FileOptions_set_Deprecated_m13C24D771BE054745D062E8AFAEB153661B70A00 (void);
// 0x000004F3 System.Boolean Google.Protobuf.Reflection.FileOptions::get_HasDeprecated()
extern void FileOptions_get_HasDeprecated_m65BAFFA6D7DA1CCDF1E3153F2B4DD272583B42DA (void);
// 0x000004F4 System.Boolean Google.Protobuf.Reflection.FileOptions::get_CcEnableArenas()
extern void FileOptions_get_CcEnableArenas_mF64263429431E46458295FA2EFABB841BCF8BD4C (void);
// 0x000004F5 System.Void Google.Protobuf.Reflection.FileOptions::set_CcEnableArenas(System.Boolean)
extern void FileOptions_set_CcEnableArenas_m603FEF01FBD68F43ECB94700C21771C02A183935 (void);
// 0x000004F6 System.Boolean Google.Protobuf.Reflection.FileOptions::get_HasCcEnableArenas()
extern void FileOptions_get_HasCcEnableArenas_m1DE12B00BAF66E1C3D92A69069CDFBC4CAEF230D (void);
// 0x000004F7 System.String Google.Protobuf.Reflection.FileOptions::get_ObjcClassPrefix()
extern void FileOptions_get_ObjcClassPrefix_m4E99A739C7303DA7010B0E94AFEDB3B17B83DD22 (void);
// 0x000004F8 System.Void Google.Protobuf.Reflection.FileOptions::set_ObjcClassPrefix(System.String)
extern void FileOptions_set_ObjcClassPrefix_mC135A42CF25258F6FBCA1CD05C8B27F7D05D0473 (void);
// 0x000004F9 System.Boolean Google.Protobuf.Reflection.FileOptions::get_HasObjcClassPrefix()
extern void FileOptions_get_HasObjcClassPrefix_m8E411663B3789519CAC6ECAEC05ADBAD24E55C70 (void);
// 0x000004FA System.String Google.Protobuf.Reflection.FileOptions::get_CsharpNamespace()
extern void FileOptions_get_CsharpNamespace_mA06DB44F8B42378563FF4DD3CC4503826EA7FE29 (void);
// 0x000004FB System.Void Google.Protobuf.Reflection.FileOptions::set_CsharpNamespace(System.String)
extern void FileOptions_set_CsharpNamespace_m9BA16A0178140239C7796FC95AB184B2074F9D9A (void);
// 0x000004FC System.Boolean Google.Protobuf.Reflection.FileOptions::get_HasCsharpNamespace()
extern void FileOptions_get_HasCsharpNamespace_mC522872193C94FAA36CE4F4DA9826507FB9EB04E (void);
// 0x000004FD System.String Google.Protobuf.Reflection.FileOptions::get_SwiftPrefix()
extern void FileOptions_get_SwiftPrefix_m03C817944B8FFE3789DAC9410DB7D997ED39B7C4 (void);
// 0x000004FE System.Void Google.Protobuf.Reflection.FileOptions::set_SwiftPrefix(System.String)
extern void FileOptions_set_SwiftPrefix_mA39A1D730B7CC4910F544B629FDB598C6ED69204 (void);
// 0x000004FF System.Boolean Google.Protobuf.Reflection.FileOptions::get_HasSwiftPrefix()
extern void FileOptions_get_HasSwiftPrefix_m021DC903B6160DC2BA4141DB2CEFA1D8EE97C823 (void);
// 0x00000500 System.String Google.Protobuf.Reflection.FileOptions::get_PhpClassPrefix()
extern void FileOptions_get_PhpClassPrefix_m13FD38C91D59EB43F935E6E01CBDBBA2FBBDB909 (void);
// 0x00000501 System.Void Google.Protobuf.Reflection.FileOptions::set_PhpClassPrefix(System.String)
extern void FileOptions_set_PhpClassPrefix_m5AFABD6BD1438D4D77A077B04130F85B9426877B (void);
// 0x00000502 System.Boolean Google.Protobuf.Reflection.FileOptions::get_HasPhpClassPrefix()
extern void FileOptions_get_HasPhpClassPrefix_m28176CDCD14A142E578E858BF09477D49B9B77CC (void);
// 0x00000503 System.String Google.Protobuf.Reflection.FileOptions::get_PhpNamespace()
extern void FileOptions_get_PhpNamespace_m776A55870E0A2F92A6F65739C496E9E7428CF29D (void);
// 0x00000504 System.Void Google.Protobuf.Reflection.FileOptions::set_PhpNamespace(System.String)
extern void FileOptions_set_PhpNamespace_m9F9AE2BE7414A7D099DAD4A9E2CE7965CDAC12C8 (void);
// 0x00000505 System.Boolean Google.Protobuf.Reflection.FileOptions::get_HasPhpNamespace()
extern void FileOptions_get_HasPhpNamespace_m3256AF1C6ED7E314A9154E6D82C73DEE05342D8D (void);
// 0x00000506 System.String Google.Protobuf.Reflection.FileOptions::get_PhpMetadataNamespace()
extern void FileOptions_get_PhpMetadataNamespace_m0547A034716AB0083517A8C382C46167B7BAAD27 (void);
// 0x00000507 System.Void Google.Protobuf.Reflection.FileOptions::set_PhpMetadataNamespace(System.String)
extern void FileOptions_set_PhpMetadataNamespace_m0223FB7107C6DEC58CC783B23EC580AA856F5A60 (void);
// 0x00000508 System.Boolean Google.Protobuf.Reflection.FileOptions::get_HasPhpMetadataNamespace()
extern void FileOptions_get_HasPhpMetadataNamespace_mFB8CB7D1EAFD71823C1A8E38FF78663FD4DAA81C (void);
// 0x00000509 System.String Google.Protobuf.Reflection.FileOptions::get_RubyPackage()
extern void FileOptions_get_RubyPackage_mCCBFE231F243FC7919302D387A8854D15B97F856 (void);
// 0x0000050A System.Void Google.Protobuf.Reflection.FileOptions::set_RubyPackage(System.String)
extern void FileOptions_set_RubyPackage_m23FA32D15B20D7D20CA0D40A723D88D9C19A81BC (void);
// 0x0000050B System.Boolean Google.Protobuf.Reflection.FileOptions::get_HasRubyPackage()
extern void FileOptions_get_HasRubyPackage_m1FA658801E944386F74C089FCC78A6639A41A320 (void);
// 0x0000050C System.Boolean Google.Protobuf.Reflection.FileOptions::Equals(System.Object)
extern void FileOptions_Equals_m269B71EC6513379C07694356E74F494902C8BCCA (void);
// 0x0000050D System.Boolean Google.Protobuf.Reflection.FileOptions::Equals(Google.Protobuf.Reflection.FileOptions)
extern void FileOptions_Equals_m9A68AC5269CD93866D6B3AE6103426CA0B62C75F (void);
// 0x0000050E System.Int32 Google.Protobuf.Reflection.FileOptions::GetHashCode()
extern void FileOptions_GetHashCode_mC6302B4BBD76B6B1B81E42621DC3B414AFBB2093 (void);
// 0x0000050F System.String Google.Protobuf.Reflection.FileOptions::ToString()
extern void FileOptions_ToString_m1F624CC78700C658ED7BCFCFC64F1932A9DE8591 (void);
// 0x00000510 System.Void Google.Protobuf.Reflection.FileOptions::WriteTo(Google.Protobuf.CodedOutputStream)
extern void FileOptions_WriteTo_m46AC0C3DC74648185755CF83BB96E3097A430512 (void);
// 0x00000511 System.Void Google.Protobuf.Reflection.FileOptions::pb::Google.Protobuf.IBufferMessage.InternalWriteTo(Google.Protobuf.WriteContext&)
extern void FileOptions_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_mE7D6ECEA3BB13F301585BA7374B00F84DFE61CC7 (void);
// 0x00000512 System.Int32 Google.Protobuf.Reflection.FileOptions::CalculateSize()
extern void FileOptions_CalculateSize_m1CBAF20ABA4BCFD4FAC533D9EDF18F7C65AF1105 (void);
// 0x00000513 System.Void Google.Protobuf.Reflection.FileOptions::MergeFrom(Google.Protobuf.Reflection.FileOptions)
extern void FileOptions_MergeFrom_m602802909D9E87D110BEA1BB3D6ECDA93F9030E6 (void);
// 0x00000514 System.Void Google.Protobuf.Reflection.FileOptions::MergeFrom(Google.Protobuf.CodedInputStream)
extern void FileOptions_MergeFrom_mD0E3ECF95A4D41396D5A6CD667943E22F2D163AC (void);
// 0x00000515 System.Void Google.Protobuf.Reflection.FileOptions::pb::Google.Protobuf.IBufferMessage.InternalMergeFrom(Google.Protobuf.ParseContext&)
extern void FileOptions_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m9A84CB596D5C261B6CD9973D22690134C98313FC (void);
// 0x00000516 TValue Google.Protobuf.Reflection.FileOptions::GetExtension(Google.Protobuf.Extension`2<Google.Protobuf.Reflection.FileOptions,TValue>)
// 0x00000517 Google.Protobuf.Collections.RepeatedField`1<TValue> Google.Protobuf.Reflection.FileOptions::GetExtension(Google.Protobuf.RepeatedExtension`2<Google.Protobuf.Reflection.FileOptions,TValue>)
// 0x00000518 Google.Protobuf.Collections.RepeatedField`1<TValue> Google.Protobuf.Reflection.FileOptions::GetOrInitializeExtension(Google.Protobuf.RepeatedExtension`2<Google.Protobuf.Reflection.FileOptions,TValue>)
// 0x00000519 System.Boolean Google.Protobuf.Reflection.FileOptions::HasExtension(Google.Protobuf.Extension`2<Google.Protobuf.Reflection.FileOptions,TValue>)
// 0x0000051A System.Void Google.Protobuf.Reflection.FileOptions::ClearExtension(Google.Protobuf.Extension`2<Google.Protobuf.Reflection.FileOptions,TValue>)
// 0x0000051B System.Void Google.Protobuf.Reflection.FileOptions::.cctor()
extern void FileOptions__cctor_mBE8948D9ED64B5359AEB8F87679730C37BC18C9A (void);
// 0x0000051C System.Void Google.Protobuf.Reflection.FileOptions/<>c::.cctor()
extern void U3CU3Ec__cctor_m5BDFC78A6F18D3D73E06A7CCC26BF02ABE01B154 (void);
// 0x0000051D System.Void Google.Protobuf.Reflection.FileOptions/<>c::.ctor()
extern void U3CU3Ec__ctor_m222F030FEBC8373501D9E4877AE838AF87A4E667 (void);
// 0x0000051E Google.Protobuf.Reflection.FileOptions Google.Protobuf.Reflection.FileOptions/<>c::<.cctor>b__219_0()
extern void U3CU3Ec_U3C_cctorU3Eb__219_0_m0D4607E83EE7D3F07DC37BB7C205D7AC3C6988A8 (void);
// 0x0000051F Google.Protobuf.MessageParser`1<Google.Protobuf.Reflection.MessageOptions> Google.Protobuf.Reflection.MessageOptions::get_Parser()
extern void MessageOptions_get_Parser_m53EF9155B5F709F4CBFA333CFE441C6948C7C918 (void);
// 0x00000520 Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.Reflection.MessageOptions::get_Descriptor()
extern void MessageOptions_get_Descriptor_mD7174A123DEA36462F8D277A6F92BF72D2DA5E67 (void);
// 0x00000521 Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.Reflection.MessageOptions::pb::Google.Protobuf.IMessage.get_Descriptor()
extern void MessageOptions_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m50EE2D14CC4936D1C6557AF7B4E6AF42016AD648 (void);
// 0x00000522 System.Void Google.Protobuf.Reflection.MessageOptions::.ctor()
extern void MessageOptions__ctor_m04D30C43D35878336875570DC6A5E3E6BC862DC8 (void);
// 0x00000523 System.Void Google.Protobuf.Reflection.MessageOptions::.ctor(Google.Protobuf.Reflection.MessageOptions)
extern void MessageOptions__ctor_m624EB105BF4DBD4102C96FBB4E33A6298249DADD (void);
// 0x00000524 Google.Protobuf.Reflection.MessageOptions Google.Protobuf.Reflection.MessageOptions::Clone()
extern void MessageOptions_Clone_m145FE6784EC0872E03A2A8F80ED305D98CA4B885 (void);
// 0x00000525 System.Boolean Google.Protobuf.Reflection.MessageOptions::get_MessageSetWireFormat()
extern void MessageOptions_get_MessageSetWireFormat_m63B35209206B97197CD6FD8307DE4144B17964F3 (void);
// 0x00000526 System.Void Google.Protobuf.Reflection.MessageOptions::set_MessageSetWireFormat(System.Boolean)
extern void MessageOptions_set_MessageSetWireFormat_m8BDF032C4FA1BE4B05801F8B8D03B39A0666582F (void);
// 0x00000527 System.Boolean Google.Protobuf.Reflection.MessageOptions::get_HasMessageSetWireFormat()
extern void MessageOptions_get_HasMessageSetWireFormat_m70C2E97A02C79B905B2BC5DF99A6C42A346AAAA4 (void);
// 0x00000528 System.Boolean Google.Protobuf.Reflection.MessageOptions::get_NoStandardDescriptorAccessor()
extern void MessageOptions_get_NoStandardDescriptorAccessor_m4C9C735876CF812FAC0C4C7D519C8C83F9E85E24 (void);
// 0x00000529 System.Void Google.Protobuf.Reflection.MessageOptions::set_NoStandardDescriptorAccessor(System.Boolean)
extern void MessageOptions_set_NoStandardDescriptorAccessor_mE1FAD81D27D01BF43C2D329D1BFFC32A7948126E (void);
// 0x0000052A System.Boolean Google.Protobuf.Reflection.MessageOptions::get_HasNoStandardDescriptorAccessor()
extern void MessageOptions_get_HasNoStandardDescriptorAccessor_m12E85FAC2F4B78894501B280E396820F6FDBB9C7 (void);
// 0x0000052B System.Boolean Google.Protobuf.Reflection.MessageOptions::get_Deprecated()
extern void MessageOptions_get_Deprecated_mC1F396571D1C88388162E16C17B4FE00894068B6 (void);
// 0x0000052C System.Void Google.Protobuf.Reflection.MessageOptions::set_Deprecated(System.Boolean)
extern void MessageOptions_set_Deprecated_m0958446C63A923B48CA62EB2762C81FB7EC73667 (void);
// 0x0000052D System.Boolean Google.Protobuf.Reflection.MessageOptions::get_HasDeprecated()
extern void MessageOptions_get_HasDeprecated_m79382B8AA4977F21658B9555ED6945B3C31C0E16 (void);
// 0x0000052E System.Boolean Google.Protobuf.Reflection.MessageOptions::get_MapEntry()
extern void MessageOptions_get_MapEntry_m1C138926AEAAF672FEB8EEEFE10802BF21872A4F (void);
// 0x0000052F System.Void Google.Protobuf.Reflection.MessageOptions::set_MapEntry(System.Boolean)
extern void MessageOptions_set_MapEntry_mC82D11ECA27A8EF638F709C2E0D73D0627605344 (void);
// 0x00000530 System.Boolean Google.Protobuf.Reflection.MessageOptions::get_HasMapEntry()
extern void MessageOptions_get_HasMapEntry_mB5DADF5A359A828AFCFEC3B0ADBBE04E2CD2B55F (void);
// 0x00000531 System.Boolean Google.Protobuf.Reflection.MessageOptions::Equals(System.Object)
extern void MessageOptions_Equals_m1B9DA96523A1EBF257B4C05943A355ABDEF8D4A3 (void);
// 0x00000532 System.Boolean Google.Protobuf.Reflection.MessageOptions::Equals(Google.Protobuf.Reflection.MessageOptions)
extern void MessageOptions_Equals_m140F555A06D1A7D663A75E01F530CE9504F73BCB (void);
// 0x00000533 System.Int32 Google.Protobuf.Reflection.MessageOptions::GetHashCode()
extern void MessageOptions_GetHashCode_m5FB4F16D0E258B4EA5B190B1FC9BA4B92CE54A0D (void);
// 0x00000534 System.String Google.Protobuf.Reflection.MessageOptions::ToString()
extern void MessageOptions_ToString_mD783887EDD978433A31E4DDCDF8A1F73C927BE1D (void);
// 0x00000535 System.Void Google.Protobuf.Reflection.MessageOptions::WriteTo(Google.Protobuf.CodedOutputStream)
extern void MessageOptions_WriteTo_mB3320A7754F94A90E6A9DC30D4FE1D3E6D933849 (void);
// 0x00000536 System.Void Google.Protobuf.Reflection.MessageOptions::pb::Google.Protobuf.IBufferMessage.InternalWriteTo(Google.Protobuf.WriteContext&)
extern void MessageOptions_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m47F229E21C08BCE7F2BE882F5811ACBBBF789A13 (void);
// 0x00000537 System.Int32 Google.Protobuf.Reflection.MessageOptions::CalculateSize()
extern void MessageOptions_CalculateSize_mC3FBEC0AC2FF4F8C44D2E5F8387DF31F177936A3 (void);
// 0x00000538 System.Void Google.Protobuf.Reflection.MessageOptions::MergeFrom(Google.Protobuf.Reflection.MessageOptions)
extern void MessageOptions_MergeFrom_m79A658F6D709B1A47BF52BDFE737C37609FCC755 (void);
// 0x00000539 System.Void Google.Protobuf.Reflection.MessageOptions::MergeFrom(Google.Protobuf.CodedInputStream)
extern void MessageOptions_MergeFrom_m3DF6E1BE37AE3869439B0A6FA4DA0BE58835014F (void);
// 0x0000053A System.Void Google.Protobuf.Reflection.MessageOptions::pb::Google.Protobuf.IBufferMessage.InternalMergeFrom(Google.Protobuf.ParseContext&)
extern void MessageOptions_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m3009CFC09F4F10050F4E663DD16E6151A0A3F556 (void);
// 0x0000053B TValue Google.Protobuf.Reflection.MessageOptions::GetExtension(Google.Protobuf.Extension`2<Google.Protobuf.Reflection.MessageOptions,TValue>)
// 0x0000053C Google.Protobuf.Collections.RepeatedField`1<TValue> Google.Protobuf.Reflection.MessageOptions::GetExtension(Google.Protobuf.RepeatedExtension`2<Google.Protobuf.Reflection.MessageOptions,TValue>)
// 0x0000053D Google.Protobuf.Collections.RepeatedField`1<TValue> Google.Protobuf.Reflection.MessageOptions::GetOrInitializeExtension(Google.Protobuf.RepeatedExtension`2<Google.Protobuf.Reflection.MessageOptions,TValue>)
// 0x0000053E System.Boolean Google.Protobuf.Reflection.MessageOptions::HasExtension(Google.Protobuf.Extension`2<Google.Protobuf.Reflection.MessageOptions,TValue>)
// 0x0000053F System.Void Google.Protobuf.Reflection.MessageOptions::ClearExtension(Google.Protobuf.Extension`2<Google.Protobuf.Reflection.MessageOptions,TValue>)
// 0x00000540 System.Void Google.Protobuf.Reflection.MessageOptions::.cctor()
extern void MessageOptions__cctor_m462805F90ECB353428AF419F3978C00E6A6F2DD4 (void);
// 0x00000541 System.Void Google.Protobuf.Reflection.MessageOptions/<>c::.cctor()
extern void U3CU3Ec__cctor_m3FD417E3735579E27791E66F8C96C2EBAFD45AC0 (void);
// 0x00000542 System.Void Google.Protobuf.Reflection.MessageOptions/<>c::.ctor()
extern void U3CU3Ec__ctor_mABC8A3FD52E725A6409DAE462412C577E8BC2D0C (void);
// 0x00000543 Google.Protobuf.Reflection.MessageOptions Google.Protobuf.Reflection.MessageOptions/<>c::<.cctor>b__74_0()
extern void U3CU3Ec_U3C_cctorU3Eb__74_0_m01F1354E67601AD2A56A657600F0962C27829E24 (void);
// 0x00000544 Google.Protobuf.MessageParser`1<Google.Protobuf.Reflection.FieldOptions> Google.Protobuf.Reflection.FieldOptions::get_Parser()
extern void FieldOptions_get_Parser_mBD73A7B75AC3AE5A81923A81B1DB409BE54A588C (void);
// 0x00000545 Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.Reflection.FieldOptions::get_Descriptor()
extern void FieldOptions_get_Descriptor_mC37D8081402EFCB32AC32E58B6A1D6FC199EA794 (void);
// 0x00000546 Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.Reflection.FieldOptions::pb::Google.Protobuf.IMessage.get_Descriptor()
extern void FieldOptions_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m7DFFB15C6EB7F4DFF583A2B9C08152C03B82B91A (void);
// 0x00000547 System.Void Google.Protobuf.Reflection.FieldOptions::.ctor()
extern void FieldOptions__ctor_mAEDAAFD3E1E1476831562F5E223B4AF2F806D42E (void);
// 0x00000548 System.Void Google.Protobuf.Reflection.FieldOptions::.ctor(Google.Protobuf.Reflection.FieldOptions)
extern void FieldOptions__ctor_mEBACF18F7CD68BE7590F93E5181DEB9E8D8FB13C (void);
// 0x00000549 Google.Protobuf.Reflection.FieldOptions Google.Protobuf.Reflection.FieldOptions::Clone()
extern void FieldOptions_Clone_m2B5511B42C346DF381B638EBDD946CF86BA411F3 (void);
// 0x0000054A Google.Protobuf.Reflection.FieldOptions/Types/CType Google.Protobuf.Reflection.FieldOptions::get_Ctype()
extern void FieldOptions_get_Ctype_mA6843F20FFA5EB265B019001BFDF8E0E02D3CC8A (void);
// 0x0000054B System.Void Google.Protobuf.Reflection.FieldOptions::set_Ctype(Google.Protobuf.Reflection.FieldOptions/Types/CType)
extern void FieldOptions_set_Ctype_m6BC337EFC7D873F75ABC51876448674862E2D66D (void);
// 0x0000054C System.Boolean Google.Protobuf.Reflection.FieldOptions::get_HasCtype()
extern void FieldOptions_get_HasCtype_mADF13326B6F7D0AC6DFC3D8341088E44B85B14E3 (void);
// 0x0000054D System.Boolean Google.Protobuf.Reflection.FieldOptions::get_Packed()
extern void FieldOptions_get_Packed_m4AE4E26FD463DBE5F8133A2A91320E5C494BA9DC (void);
// 0x0000054E System.Void Google.Protobuf.Reflection.FieldOptions::set_Packed(System.Boolean)
extern void FieldOptions_set_Packed_m5DB5B5076854BAD5F123E60C13172B3C3E8BE3D4 (void);
// 0x0000054F System.Boolean Google.Protobuf.Reflection.FieldOptions::get_HasPacked()
extern void FieldOptions_get_HasPacked_m44A791BEEB8477823C08D9217756E10A27906DD3 (void);
// 0x00000550 Google.Protobuf.Reflection.FieldOptions/Types/JSType Google.Protobuf.Reflection.FieldOptions::get_Jstype()
extern void FieldOptions_get_Jstype_m9D6F072E771C39153BF21434200CE11B1A2A4377 (void);
// 0x00000551 System.Void Google.Protobuf.Reflection.FieldOptions::set_Jstype(Google.Protobuf.Reflection.FieldOptions/Types/JSType)
extern void FieldOptions_set_Jstype_mCE489B662A9B15437E7BC7A29D88FE5A2D1F1F75 (void);
// 0x00000552 System.Boolean Google.Protobuf.Reflection.FieldOptions::get_HasJstype()
extern void FieldOptions_get_HasJstype_m606CBC541A4D02C27396A9B7BAE2C82CC7D69A0C (void);
// 0x00000553 System.Boolean Google.Protobuf.Reflection.FieldOptions::get_Lazy()
extern void FieldOptions_get_Lazy_m57A6A0502DC600EE64748624A79C46F3369DCA31 (void);
// 0x00000554 System.Void Google.Protobuf.Reflection.FieldOptions::set_Lazy(System.Boolean)
extern void FieldOptions_set_Lazy_mEE802290A41C3F0805530D8700FDFE08E0D69FA3 (void);
// 0x00000555 System.Boolean Google.Protobuf.Reflection.FieldOptions::get_HasLazy()
extern void FieldOptions_get_HasLazy_m1CF72D570ED6CC8B184508095DE59865357F83F0 (void);
// 0x00000556 System.Boolean Google.Protobuf.Reflection.FieldOptions::get_Deprecated()
extern void FieldOptions_get_Deprecated_mF0E44E4DF76BD6A70934E29858F576F60CAECE8E (void);
// 0x00000557 System.Void Google.Protobuf.Reflection.FieldOptions::set_Deprecated(System.Boolean)
extern void FieldOptions_set_Deprecated_m8AEECA8FC9BD18F19AA1D664AD8A3425ADFD71A7 (void);
// 0x00000558 System.Boolean Google.Protobuf.Reflection.FieldOptions::get_HasDeprecated()
extern void FieldOptions_get_HasDeprecated_mB31BECA44B454D97A914531CAADC91757942A9C2 (void);
// 0x00000559 System.Boolean Google.Protobuf.Reflection.FieldOptions::get_Weak()
extern void FieldOptions_get_Weak_m7A3A4A8898C2A6F32C9F5B81A149F41661769440 (void);
// 0x0000055A System.Void Google.Protobuf.Reflection.FieldOptions::set_Weak(System.Boolean)
extern void FieldOptions_set_Weak_m78E446C9BC7216A1AA5BB97716EBC9E6BD2E4DDC (void);
// 0x0000055B System.Boolean Google.Protobuf.Reflection.FieldOptions::get_HasWeak()
extern void FieldOptions_get_HasWeak_m68798BAE7FC4C483D909B594AB866FB4B8D4E3CE (void);
// 0x0000055C System.Boolean Google.Protobuf.Reflection.FieldOptions::Equals(System.Object)
extern void FieldOptions_Equals_m35E10FF061C593610F69C8A497CBBB5345B641DA (void);
// 0x0000055D System.Boolean Google.Protobuf.Reflection.FieldOptions::Equals(Google.Protobuf.Reflection.FieldOptions)
extern void FieldOptions_Equals_mC82884DE34A33E7799B4B537DB3E5327FFF93DE8 (void);
// 0x0000055E System.Int32 Google.Protobuf.Reflection.FieldOptions::GetHashCode()
extern void FieldOptions_GetHashCode_m2C8BB8DAB5A6D69F940B9714BAAF1632D551814B (void);
// 0x0000055F System.String Google.Protobuf.Reflection.FieldOptions::ToString()
extern void FieldOptions_ToString_m9E9A624E4C63E7B5FB16D55551A2B3E1BCE409F5 (void);
// 0x00000560 System.Void Google.Protobuf.Reflection.FieldOptions::WriteTo(Google.Protobuf.CodedOutputStream)
extern void FieldOptions_WriteTo_mB619B555A1CF0FABD74BAB7646DAAEC68B206B4C (void);
// 0x00000561 System.Void Google.Protobuf.Reflection.FieldOptions::pb::Google.Protobuf.IBufferMessage.InternalWriteTo(Google.Protobuf.WriteContext&)
extern void FieldOptions_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_mCE93A239A7677AE5873AAE5CB3E6F3979A60247D (void);
// 0x00000562 System.Int32 Google.Protobuf.Reflection.FieldOptions::CalculateSize()
extern void FieldOptions_CalculateSize_m28F032BDF8603529AFE3FF729BF891A8B035A4F2 (void);
// 0x00000563 System.Void Google.Protobuf.Reflection.FieldOptions::MergeFrom(Google.Protobuf.Reflection.FieldOptions)
extern void FieldOptions_MergeFrom_m78199E61A3B42CB84FC665C80A5C16C79E81E330 (void);
// 0x00000564 System.Void Google.Protobuf.Reflection.FieldOptions::MergeFrom(Google.Protobuf.CodedInputStream)
extern void FieldOptions_MergeFrom_mC9159B562BFFE7A8E521072DCCD5F4D9E42043B4 (void);
// 0x00000565 System.Void Google.Protobuf.Reflection.FieldOptions::pb::Google.Protobuf.IBufferMessage.InternalMergeFrom(Google.Protobuf.ParseContext&)
extern void FieldOptions_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m19D5BFCA155031073581BDEA524C8D1A712D5393 (void);
// 0x00000566 TValue Google.Protobuf.Reflection.FieldOptions::GetExtension(Google.Protobuf.Extension`2<Google.Protobuf.Reflection.FieldOptions,TValue>)
// 0x00000567 Google.Protobuf.Collections.RepeatedField`1<TValue> Google.Protobuf.Reflection.FieldOptions::GetExtension(Google.Protobuf.RepeatedExtension`2<Google.Protobuf.Reflection.FieldOptions,TValue>)
// 0x00000568 Google.Protobuf.Collections.RepeatedField`1<TValue> Google.Protobuf.Reflection.FieldOptions::GetOrInitializeExtension(Google.Protobuf.RepeatedExtension`2<Google.Protobuf.Reflection.FieldOptions,TValue>)
// 0x00000569 System.Boolean Google.Protobuf.Reflection.FieldOptions::HasExtension(Google.Protobuf.Extension`2<Google.Protobuf.Reflection.FieldOptions,TValue>)
// 0x0000056A System.Void Google.Protobuf.Reflection.FieldOptions::ClearExtension(Google.Protobuf.Extension`2<Google.Protobuf.Reflection.FieldOptions,TValue>)
// 0x0000056B System.Void Google.Protobuf.Reflection.FieldOptions::.cctor()
extern void FieldOptions__cctor_m97F67E7667AC8FF8F4A94D63B04CDC1B0C773D7E (void);
// 0x0000056C System.Void Google.Protobuf.Reflection.FieldOptions/<>c::.cctor()
extern void U3CU3Ec__cctor_mCD4BC8531FD960927751A5B7AA227FA1214B561F (void);
// 0x0000056D System.Void Google.Protobuf.Reflection.FieldOptions/<>c::.ctor()
extern void U3CU3Ec__ctor_mA983EF284773415CD295C5EF5F558E82661C71B5 (void);
// 0x0000056E Google.Protobuf.Reflection.FieldOptions Google.Protobuf.Reflection.FieldOptions/<>c::<.cctor>b__93_0()
extern void U3CU3Ec_U3C_cctorU3Eb__93_0_mA390BA56593D29C80E46DEF99F9821A284EA1198 (void);
// 0x0000056F Google.Protobuf.MessageParser`1<Google.Protobuf.Reflection.OneofOptions> Google.Protobuf.Reflection.OneofOptions::get_Parser()
extern void OneofOptions_get_Parser_m7B0678394C34F9A7B292A180247CB002D4B0BCF2 (void);
// 0x00000570 Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.Reflection.OneofOptions::get_Descriptor()
extern void OneofOptions_get_Descriptor_m8B5E60C8A1C89E3E748C9A976BC8F7AFA3290E67 (void);
// 0x00000571 Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.Reflection.OneofOptions::pb::Google.Protobuf.IMessage.get_Descriptor()
extern void OneofOptions_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_mCC08AF835625C824BE55AF335D89D0DA3F309090 (void);
// 0x00000572 System.Void Google.Protobuf.Reflection.OneofOptions::.ctor()
extern void OneofOptions__ctor_mCB3DEA19D4B2D302F3960474C65B68869AE65A6C (void);
// 0x00000573 System.Void Google.Protobuf.Reflection.OneofOptions::.ctor(Google.Protobuf.Reflection.OneofOptions)
extern void OneofOptions__ctor_m87704EF2A87D953EEE82B1634AB6326C00D36690 (void);
// 0x00000574 Google.Protobuf.Reflection.OneofOptions Google.Protobuf.Reflection.OneofOptions::Clone()
extern void OneofOptions_Clone_m861C126BB5E08AD41DFCC26AF082E5E53435DB55 (void);
// 0x00000575 System.Boolean Google.Protobuf.Reflection.OneofOptions::Equals(System.Object)
extern void OneofOptions_Equals_mF33E711DDF4A91CE5F2BB106B53D2AC4688D058B (void);
// 0x00000576 System.Boolean Google.Protobuf.Reflection.OneofOptions::Equals(Google.Protobuf.Reflection.OneofOptions)
extern void OneofOptions_Equals_mD4943C6868C3350846C89F186BABEF1E024925B6 (void);
// 0x00000577 System.Int32 Google.Protobuf.Reflection.OneofOptions::GetHashCode()
extern void OneofOptions_GetHashCode_m9F0EBEE5FCA5AC438B7DDCF82E19623AE486A957 (void);
// 0x00000578 System.String Google.Protobuf.Reflection.OneofOptions::ToString()
extern void OneofOptions_ToString_m472557171AD5BB71572A06F3169E632E1322C621 (void);
// 0x00000579 System.Void Google.Protobuf.Reflection.OneofOptions::WriteTo(Google.Protobuf.CodedOutputStream)
extern void OneofOptions_WriteTo_mA65BE0E9F2AE867975411486CE00F66754F593DB (void);
// 0x0000057A System.Void Google.Protobuf.Reflection.OneofOptions::pb::Google.Protobuf.IBufferMessage.InternalWriteTo(Google.Protobuf.WriteContext&)
extern void OneofOptions_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m63942D15544A3D5AC7BEB303FE4FA711E3364C69 (void);
// 0x0000057B System.Int32 Google.Protobuf.Reflection.OneofOptions::CalculateSize()
extern void OneofOptions_CalculateSize_m2018ADE928533BABBA3CCDEA40E9FC13978CC4F0 (void);
// 0x0000057C System.Void Google.Protobuf.Reflection.OneofOptions::MergeFrom(Google.Protobuf.Reflection.OneofOptions)
extern void OneofOptions_MergeFrom_mBCB2053FE63A76031534E6E4CD2DBD96E73118D2 (void);
// 0x0000057D System.Void Google.Protobuf.Reflection.OneofOptions::MergeFrom(Google.Protobuf.CodedInputStream)
extern void OneofOptions_MergeFrom_mD36D6943FAFB5C64CC30EA67CF7DFAD976C846EA (void);
// 0x0000057E System.Void Google.Protobuf.Reflection.OneofOptions::pb::Google.Protobuf.IBufferMessage.InternalMergeFrom(Google.Protobuf.ParseContext&)
extern void OneofOptions_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_mDF39C909A8356CFEBE50ADE3B724A1C7B306E84D (void);
// 0x0000057F TValue Google.Protobuf.Reflection.OneofOptions::GetExtension(Google.Protobuf.Extension`2<Google.Protobuf.Reflection.OneofOptions,TValue>)
// 0x00000580 Google.Protobuf.Collections.RepeatedField`1<TValue> Google.Protobuf.Reflection.OneofOptions::GetExtension(Google.Protobuf.RepeatedExtension`2<Google.Protobuf.Reflection.OneofOptions,TValue>)
// 0x00000581 Google.Protobuf.Collections.RepeatedField`1<TValue> Google.Protobuf.Reflection.OneofOptions::GetOrInitializeExtension(Google.Protobuf.RepeatedExtension`2<Google.Protobuf.Reflection.OneofOptions,TValue>)
// 0x00000582 System.Boolean Google.Protobuf.Reflection.OneofOptions::HasExtension(Google.Protobuf.Extension`2<Google.Protobuf.Reflection.OneofOptions,TValue>)
// 0x00000583 System.Void Google.Protobuf.Reflection.OneofOptions::ClearExtension(Google.Protobuf.Extension`2<Google.Protobuf.Reflection.OneofOptions,TValue>)
// 0x00000584 System.Void Google.Protobuf.Reflection.OneofOptions::.cctor()
extern void OneofOptions__cctor_mA657EF433F7836DC9101B67054175F0B03627DA2 (void);
// 0x00000585 System.Void Google.Protobuf.Reflection.OneofOptions/<>c::.cctor()
extern void U3CU3Ec__cctor_mF2353099F097FAFF0923EBA64043DA97C1EE522F (void);
// 0x00000586 System.Void Google.Protobuf.Reflection.OneofOptions/<>c::.ctor()
extern void U3CU3Ec__ctor_m1699C6B6E16AB84D0B4240F3A2AA1839CF079492 (void);
// 0x00000587 Google.Protobuf.Reflection.OneofOptions Google.Protobuf.Reflection.OneofOptions/<>c::<.cctor>b__37_0()
extern void U3CU3Ec_U3C_cctorU3Eb__37_0_m33EF6B2A034ECB2874C9EEB9C96479B5B99ED9C1 (void);
// 0x00000588 Google.Protobuf.MessageParser`1<Google.Protobuf.Reflection.EnumOptions> Google.Protobuf.Reflection.EnumOptions::get_Parser()
extern void EnumOptions_get_Parser_m74C36E689CB3185F069FBBCE1852FB446B4FEFD1 (void);
// 0x00000589 Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.Reflection.EnumOptions::get_Descriptor()
extern void EnumOptions_get_Descriptor_m055E04545550B61018A8CBFEBEF747141610DF4A (void);
// 0x0000058A Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.Reflection.EnumOptions::pb::Google.Protobuf.IMessage.get_Descriptor()
extern void EnumOptions_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m2B62FA8955EC17A18D516DD20AA183ADDFECA920 (void);
// 0x0000058B System.Void Google.Protobuf.Reflection.EnumOptions::.ctor()
extern void EnumOptions__ctor_m5385E9F25EF8615050334EEC3708BA5725590EA6 (void);
// 0x0000058C System.Void Google.Protobuf.Reflection.EnumOptions::.ctor(Google.Protobuf.Reflection.EnumOptions)
extern void EnumOptions__ctor_m6CC675C9336E6028155824F6AC523E84B9B72900 (void);
// 0x0000058D Google.Protobuf.Reflection.EnumOptions Google.Protobuf.Reflection.EnumOptions::Clone()
extern void EnumOptions_Clone_m607D859024E1D3E98EADC2D87FE1A6004633A364 (void);
// 0x0000058E System.Boolean Google.Protobuf.Reflection.EnumOptions::get_AllowAlias()
extern void EnumOptions_get_AllowAlias_m8451F8AB3791D2D2D98A58909C89C0B27FB554E9 (void);
// 0x0000058F System.Void Google.Protobuf.Reflection.EnumOptions::set_AllowAlias(System.Boolean)
extern void EnumOptions_set_AllowAlias_mBF2086F3EDBD93C60C083563B09B70899D937388 (void);
// 0x00000590 System.Boolean Google.Protobuf.Reflection.EnumOptions::get_HasAllowAlias()
extern void EnumOptions_get_HasAllowAlias_m516455BF0D491A603D3690B64A17AA780750759E (void);
// 0x00000591 System.Boolean Google.Protobuf.Reflection.EnumOptions::get_Deprecated()
extern void EnumOptions_get_Deprecated_m14967E7D273D984C34E8418D1FF0CC1F3B317032 (void);
// 0x00000592 System.Void Google.Protobuf.Reflection.EnumOptions::set_Deprecated(System.Boolean)
extern void EnumOptions_set_Deprecated_mC25625421D337CD31FD954015B842180C4ED5BCB (void);
// 0x00000593 System.Boolean Google.Protobuf.Reflection.EnumOptions::get_HasDeprecated()
extern void EnumOptions_get_HasDeprecated_mD5B251C2DAAD2B688DFC8E45C8029DB3918F89C5 (void);
// 0x00000594 System.Boolean Google.Protobuf.Reflection.EnumOptions::Equals(System.Object)
extern void EnumOptions_Equals_m7AE883DD0D520647C6A466B4B7731AA4C3ECC696 (void);
// 0x00000595 System.Boolean Google.Protobuf.Reflection.EnumOptions::Equals(Google.Protobuf.Reflection.EnumOptions)
extern void EnumOptions_Equals_mA6AEAD2E23DF4EA72CB302A48CA00FC4D7844CB0 (void);
// 0x00000596 System.Int32 Google.Protobuf.Reflection.EnumOptions::GetHashCode()
extern void EnumOptions_GetHashCode_mC319C69B06D57266DF45D6E68DBAD4A8A24CB638 (void);
// 0x00000597 System.String Google.Protobuf.Reflection.EnumOptions::ToString()
extern void EnumOptions_ToString_m5D36D78A38E16C3589A17C93AF80ABC42F5304F7 (void);
// 0x00000598 System.Void Google.Protobuf.Reflection.EnumOptions::WriteTo(Google.Protobuf.CodedOutputStream)
extern void EnumOptions_WriteTo_m5689DE4D2F562F444B47E032A670D6F495472D74 (void);
// 0x00000599 System.Void Google.Protobuf.Reflection.EnumOptions::pb::Google.Protobuf.IBufferMessage.InternalWriteTo(Google.Protobuf.WriteContext&)
extern void EnumOptions_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m014FF1827E80308CE342F0F91A38C87AD1E6FFAA (void);
// 0x0000059A System.Int32 Google.Protobuf.Reflection.EnumOptions::CalculateSize()
extern void EnumOptions_CalculateSize_mE57649652E7F429D04DDCB6BA25843D83D858366 (void);
// 0x0000059B System.Void Google.Protobuf.Reflection.EnumOptions::MergeFrom(Google.Protobuf.Reflection.EnumOptions)
extern void EnumOptions_MergeFrom_m8783323089C9BE38786C2F133AE42096DF6374AF (void);
// 0x0000059C System.Void Google.Protobuf.Reflection.EnumOptions::MergeFrom(Google.Protobuf.CodedInputStream)
extern void EnumOptions_MergeFrom_mE5A3CD277AF799DB94EF81CFEBC4C84E13947F53 (void);
// 0x0000059D System.Void Google.Protobuf.Reflection.EnumOptions::pb::Google.Protobuf.IBufferMessage.InternalMergeFrom(Google.Protobuf.ParseContext&)
extern void EnumOptions_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m7703AE4472D5FDEB96708EFA3BD302633F5211F4 (void);
// 0x0000059E TValue Google.Protobuf.Reflection.EnumOptions::GetExtension(Google.Protobuf.Extension`2<Google.Protobuf.Reflection.EnumOptions,TValue>)
// 0x0000059F Google.Protobuf.Collections.RepeatedField`1<TValue> Google.Protobuf.Reflection.EnumOptions::GetExtension(Google.Protobuf.RepeatedExtension`2<Google.Protobuf.Reflection.EnumOptions,TValue>)
// 0x000005A0 Google.Protobuf.Collections.RepeatedField`1<TValue> Google.Protobuf.Reflection.EnumOptions::GetOrInitializeExtension(Google.Protobuf.RepeatedExtension`2<Google.Protobuf.Reflection.EnumOptions,TValue>)
// 0x000005A1 System.Boolean Google.Protobuf.Reflection.EnumOptions::HasExtension(Google.Protobuf.Extension`2<Google.Protobuf.Reflection.EnumOptions,TValue>)
// 0x000005A2 System.Void Google.Protobuf.Reflection.EnumOptions::ClearExtension(Google.Protobuf.Extension`2<Google.Protobuf.Reflection.EnumOptions,TValue>)
// 0x000005A3 System.Void Google.Protobuf.Reflection.EnumOptions::.cctor()
extern void EnumOptions__cctor_m724F897999CB7C4D707CF4B441621C453C1C6279 (void);
// 0x000005A4 System.Void Google.Protobuf.Reflection.EnumOptions/<>c::.cctor()
extern void U3CU3Ec__cctor_mF63C23521E8F29F07A3F3DC0CB8257E25C472AFF (void);
// 0x000005A5 System.Void Google.Protobuf.Reflection.EnumOptions/<>c::.ctor()
extern void U3CU3Ec__ctor_mFA35CCDA24AD68084C39622FB3B4B0EAB2F193CF (void);
// 0x000005A6 Google.Protobuf.Reflection.EnumOptions Google.Protobuf.Reflection.EnumOptions/<>c::<.cctor>b__56_0()
extern void U3CU3Ec_U3C_cctorU3Eb__56_0_m6C29F69BEC4A1E886865DFDE2E165CA490780A78 (void);
// 0x000005A7 Google.Protobuf.MessageParser`1<Google.Protobuf.Reflection.EnumValueOptions> Google.Protobuf.Reflection.EnumValueOptions::get_Parser()
extern void EnumValueOptions_get_Parser_m1DDDB5F095116785BF9C5F1110F5C66F29061810 (void);
// 0x000005A8 Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.Reflection.EnumValueOptions::get_Descriptor()
extern void EnumValueOptions_get_Descriptor_mD42AFD1FAC628908E9715418CD1BB395939EAD54 (void);
// 0x000005A9 Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.Reflection.EnumValueOptions::pb::Google.Protobuf.IMessage.get_Descriptor()
extern void EnumValueOptions_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_mB0348AB4813E4A5349811036C55D1E4D4EF59C12 (void);
// 0x000005AA System.Void Google.Protobuf.Reflection.EnumValueOptions::.ctor()
extern void EnumValueOptions__ctor_mEDC4489E058024D0EE74CD1882B267796D0E6F05 (void);
// 0x000005AB System.Void Google.Protobuf.Reflection.EnumValueOptions::.ctor(Google.Protobuf.Reflection.EnumValueOptions)
extern void EnumValueOptions__ctor_m1E94702D09BCE26AC8F7CBFDD1D52C1AEFBB183E (void);
// 0x000005AC Google.Protobuf.Reflection.EnumValueOptions Google.Protobuf.Reflection.EnumValueOptions::Clone()
extern void EnumValueOptions_Clone_m6C604B56C82AC36BE5F555CD73F471B01B831730 (void);
// 0x000005AD System.Boolean Google.Protobuf.Reflection.EnumValueOptions::get_Deprecated()
extern void EnumValueOptions_get_Deprecated_mBB62B4E8F3B20B75EC4C85AC0B34446C7D94E6E6 (void);
// 0x000005AE System.Void Google.Protobuf.Reflection.EnumValueOptions::set_Deprecated(System.Boolean)
extern void EnumValueOptions_set_Deprecated_mCB6C503A0C948031B3DC7D7DC7DD12F6F6D8B0F4 (void);
// 0x000005AF System.Boolean Google.Protobuf.Reflection.EnumValueOptions::get_HasDeprecated()
extern void EnumValueOptions_get_HasDeprecated_m842E47F888CC39366FEDF5F807ECE1BABCF0D5AC (void);
// 0x000005B0 System.Boolean Google.Protobuf.Reflection.EnumValueOptions::Equals(System.Object)
extern void EnumValueOptions_Equals_mBE4B57FD48FF5410BEB1956196C76DA4B5B451CD (void);
// 0x000005B1 System.Boolean Google.Protobuf.Reflection.EnumValueOptions::Equals(Google.Protobuf.Reflection.EnumValueOptions)
extern void EnumValueOptions_Equals_m4275B004B5AB046B0CCC1C05C4C9C88C672D4B27 (void);
// 0x000005B2 System.Int32 Google.Protobuf.Reflection.EnumValueOptions::GetHashCode()
extern void EnumValueOptions_GetHashCode_m3CDA5B21FFC6387A8313F2E0230D63C5CC4D9699 (void);
// 0x000005B3 System.String Google.Protobuf.Reflection.EnumValueOptions::ToString()
extern void EnumValueOptions_ToString_m689014E160C318BA3C63EFD328D3B0F7DCA8908E (void);
// 0x000005B4 System.Void Google.Protobuf.Reflection.EnumValueOptions::WriteTo(Google.Protobuf.CodedOutputStream)
extern void EnumValueOptions_WriteTo_m2C38B59E54CC93366529373A55DCBBF1805B2FBC (void);
// 0x000005B5 System.Void Google.Protobuf.Reflection.EnumValueOptions::pb::Google.Protobuf.IBufferMessage.InternalWriteTo(Google.Protobuf.WriteContext&)
extern void EnumValueOptions_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m1E4497803CFE23240560E2EFDD0C4ED503A98338 (void);
// 0x000005B6 System.Int32 Google.Protobuf.Reflection.EnumValueOptions::CalculateSize()
extern void EnumValueOptions_CalculateSize_m5CEBE3C36B5DFD8EB4139E0BC860D680C1A1EB19 (void);
// 0x000005B7 System.Void Google.Protobuf.Reflection.EnumValueOptions::MergeFrom(Google.Protobuf.Reflection.EnumValueOptions)
extern void EnumValueOptions_MergeFrom_m5ED39DC9CFB8446A5595EFED2D18B56BFE2F761E (void);
// 0x000005B8 System.Void Google.Protobuf.Reflection.EnumValueOptions::MergeFrom(Google.Protobuf.CodedInputStream)
extern void EnumValueOptions_MergeFrom_m9CB27E7FFBCF59A93EFB81F345EEEE0018780465 (void);
// 0x000005B9 System.Void Google.Protobuf.Reflection.EnumValueOptions::pb::Google.Protobuf.IBufferMessage.InternalMergeFrom(Google.Protobuf.ParseContext&)
extern void EnumValueOptions_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m9A7A72C5D302BD0113B44F30F4D93A74E69CB7CD (void);
// 0x000005BA TValue Google.Protobuf.Reflection.EnumValueOptions::GetExtension(Google.Protobuf.Extension`2<Google.Protobuf.Reflection.EnumValueOptions,TValue>)
// 0x000005BB Google.Protobuf.Collections.RepeatedField`1<TValue> Google.Protobuf.Reflection.EnumValueOptions::GetExtension(Google.Protobuf.RepeatedExtension`2<Google.Protobuf.Reflection.EnumValueOptions,TValue>)
// 0x000005BC Google.Protobuf.Collections.RepeatedField`1<TValue> Google.Protobuf.Reflection.EnumValueOptions::GetOrInitializeExtension(Google.Protobuf.RepeatedExtension`2<Google.Protobuf.Reflection.EnumValueOptions,TValue>)
// 0x000005BD System.Boolean Google.Protobuf.Reflection.EnumValueOptions::HasExtension(Google.Protobuf.Extension`2<Google.Protobuf.Reflection.EnumValueOptions,TValue>)
// 0x000005BE System.Void Google.Protobuf.Reflection.EnumValueOptions::ClearExtension(Google.Protobuf.Extension`2<Google.Protobuf.Reflection.EnumValueOptions,TValue>)
// 0x000005BF System.Void Google.Protobuf.Reflection.EnumValueOptions::.cctor()
extern void EnumValueOptions__cctor_mE8B758BA5661472577315BF5F23766C4D042438B (void);
// 0x000005C0 System.Void Google.Protobuf.Reflection.EnumValueOptions/<>c::.cctor()
extern void U3CU3Ec__cctor_m339291422CD5DC70C5FE9F48EB6614CC17EC0E86 (void);
// 0x000005C1 System.Void Google.Protobuf.Reflection.EnumValueOptions/<>c::.ctor()
extern void U3CU3Ec__ctor_m8BE7481CF1C66F1276CBFC6396B323BD610D674F (void);
// 0x000005C2 Google.Protobuf.Reflection.EnumValueOptions Google.Protobuf.Reflection.EnumValueOptions/<>c::<.cctor>b__47_0()
extern void U3CU3Ec_U3C_cctorU3Eb__47_0_m15773815C34E5654CC3E45E9FD03CDBB768B3BAC (void);
// 0x000005C3 Google.Protobuf.MessageParser`1<Google.Protobuf.Reflection.ServiceOptions> Google.Protobuf.Reflection.ServiceOptions::get_Parser()
extern void ServiceOptions_get_Parser_mE5EE0F3C480BFC70D4520E59B786CE266CD7990E (void);
// 0x000005C4 Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.Reflection.ServiceOptions::get_Descriptor()
extern void ServiceOptions_get_Descriptor_mA87B56F95F37999176DAFF804F73FCC24CEE3590 (void);
// 0x000005C5 Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.Reflection.ServiceOptions::pb::Google.Protobuf.IMessage.get_Descriptor()
extern void ServiceOptions_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m5CBA61A832DD81EA936FCEF5994429B4B938ECC9 (void);
// 0x000005C6 System.Void Google.Protobuf.Reflection.ServiceOptions::.ctor()
extern void ServiceOptions__ctor_mF53F4F57784851D29B2274A2B2C0B53A044FC5F2 (void);
// 0x000005C7 System.Void Google.Protobuf.Reflection.ServiceOptions::.ctor(Google.Protobuf.Reflection.ServiceOptions)
extern void ServiceOptions__ctor_mB0C390F674A6FD4B7E940F7DF26EE01E8B72486C (void);
// 0x000005C8 Google.Protobuf.Reflection.ServiceOptions Google.Protobuf.Reflection.ServiceOptions::Clone()
extern void ServiceOptions_Clone_m548C4ABD6A7B28FF5D8339A2878E617F8FF0FC0F (void);
// 0x000005C9 System.Boolean Google.Protobuf.Reflection.ServiceOptions::get_Deprecated()
extern void ServiceOptions_get_Deprecated_m693CCD13364B54FEA9019FC75FF94D361457D435 (void);
// 0x000005CA System.Void Google.Protobuf.Reflection.ServiceOptions::set_Deprecated(System.Boolean)
extern void ServiceOptions_set_Deprecated_mB85FD593FB12B41F625CBA2424FF36A008CCB047 (void);
// 0x000005CB System.Boolean Google.Protobuf.Reflection.ServiceOptions::get_HasDeprecated()
extern void ServiceOptions_get_HasDeprecated_m93C02BA34BCA0DF3289167D556788AECB2388C8E (void);
// 0x000005CC System.Boolean Google.Protobuf.Reflection.ServiceOptions::Equals(System.Object)
extern void ServiceOptions_Equals_m8F72F53030D77F97C862B939A18F0B803318DE2F (void);
// 0x000005CD System.Boolean Google.Protobuf.Reflection.ServiceOptions::Equals(Google.Protobuf.Reflection.ServiceOptions)
extern void ServiceOptions_Equals_m8103227D46B27ED1BC216DB305C3973DC4F2DF76 (void);
// 0x000005CE System.Int32 Google.Protobuf.Reflection.ServiceOptions::GetHashCode()
extern void ServiceOptions_GetHashCode_m6843CA5920027100EC678C9E12D958ABBB0B7A2F (void);
// 0x000005CF System.String Google.Protobuf.Reflection.ServiceOptions::ToString()
extern void ServiceOptions_ToString_m9C3A29E7B7D222E10DD900C0AF541A804B3130F8 (void);
// 0x000005D0 System.Void Google.Protobuf.Reflection.ServiceOptions::WriteTo(Google.Protobuf.CodedOutputStream)
extern void ServiceOptions_WriteTo_mC3CCEB2117A0EFC47E65EBCFF2D95500CAB4F504 (void);
// 0x000005D1 System.Void Google.Protobuf.Reflection.ServiceOptions::pb::Google.Protobuf.IBufferMessage.InternalWriteTo(Google.Protobuf.WriteContext&)
extern void ServiceOptions_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m595286491DBE361A862243B1A0F5AA2C64F48739 (void);
// 0x000005D2 System.Int32 Google.Protobuf.Reflection.ServiceOptions::CalculateSize()
extern void ServiceOptions_CalculateSize_mF38C58E3DD52815641DE6B3734035AD147E28E37 (void);
// 0x000005D3 System.Void Google.Protobuf.Reflection.ServiceOptions::MergeFrom(Google.Protobuf.Reflection.ServiceOptions)
extern void ServiceOptions_MergeFrom_m0F4BD8AC991185BC2C06F1541C340B56202A6AE4 (void);
// 0x000005D4 System.Void Google.Protobuf.Reflection.ServiceOptions::MergeFrom(Google.Protobuf.CodedInputStream)
extern void ServiceOptions_MergeFrom_m91ACECFAAC88BE4A3B9AF6E32A85D5ACD847507D (void);
// 0x000005D5 System.Void Google.Protobuf.Reflection.ServiceOptions::pb::Google.Protobuf.IBufferMessage.InternalMergeFrom(Google.Protobuf.ParseContext&)
extern void ServiceOptions_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_mB1A0155B8F628B062613DE44B49A8F6B680587AB (void);
// 0x000005D6 TValue Google.Protobuf.Reflection.ServiceOptions::GetExtension(Google.Protobuf.Extension`2<Google.Protobuf.Reflection.ServiceOptions,TValue>)
// 0x000005D7 Google.Protobuf.Collections.RepeatedField`1<TValue> Google.Protobuf.Reflection.ServiceOptions::GetExtension(Google.Protobuf.RepeatedExtension`2<Google.Protobuf.Reflection.ServiceOptions,TValue>)
// 0x000005D8 Google.Protobuf.Collections.RepeatedField`1<TValue> Google.Protobuf.Reflection.ServiceOptions::GetOrInitializeExtension(Google.Protobuf.RepeatedExtension`2<Google.Protobuf.Reflection.ServiceOptions,TValue>)
// 0x000005D9 System.Boolean Google.Protobuf.Reflection.ServiceOptions::HasExtension(Google.Protobuf.Extension`2<Google.Protobuf.Reflection.ServiceOptions,TValue>)
// 0x000005DA System.Void Google.Protobuf.Reflection.ServiceOptions::ClearExtension(Google.Protobuf.Extension`2<Google.Protobuf.Reflection.ServiceOptions,TValue>)
// 0x000005DB System.Void Google.Protobuf.Reflection.ServiceOptions::.cctor()
extern void ServiceOptions__cctor_mA12CD486E7D01A575C79ED676B4717E794D32D6D (void);
// 0x000005DC System.Void Google.Protobuf.Reflection.ServiceOptions/<>c::.cctor()
extern void U3CU3Ec__cctor_mE6820DD8647C46595D786FED96262C40BB604219 (void);
// 0x000005DD System.Void Google.Protobuf.Reflection.ServiceOptions/<>c::.ctor()
extern void U3CU3Ec__ctor_m12F13970B895A83AE6585DEE9E0EF609A6B41FD4 (void);
// 0x000005DE Google.Protobuf.Reflection.ServiceOptions Google.Protobuf.Reflection.ServiceOptions/<>c::<.cctor>b__47_0()
extern void U3CU3Ec_U3C_cctorU3Eb__47_0_m2C9FFFFE70006856529D67653183A12D3822540C (void);
// 0x000005DF Google.Protobuf.MessageParser`1<Google.Protobuf.Reflection.MethodOptions> Google.Protobuf.Reflection.MethodOptions::get_Parser()
extern void MethodOptions_get_Parser_m47FFE6E96180F8B34F52620D8477E9347A14B112 (void);
// 0x000005E0 Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.Reflection.MethodOptions::get_Descriptor()
extern void MethodOptions_get_Descriptor_m0051862801E9C1FDB008D4CF25CB5FBCEDB743FD (void);
// 0x000005E1 Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.Reflection.MethodOptions::pb::Google.Protobuf.IMessage.get_Descriptor()
extern void MethodOptions_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m2D1F1AA832041E1D63B55590B512598237C7F075 (void);
// 0x000005E2 System.Void Google.Protobuf.Reflection.MethodOptions::.ctor()
extern void MethodOptions__ctor_m8E6C41386BE75CAF13EBCA4773DE2C9455DCAACB (void);
// 0x000005E3 System.Void Google.Protobuf.Reflection.MethodOptions::.ctor(Google.Protobuf.Reflection.MethodOptions)
extern void MethodOptions__ctor_m5D84C74E7A241DC3FEB9E244C786A3CA5A9EA6C1 (void);
// 0x000005E4 Google.Protobuf.Reflection.MethodOptions Google.Protobuf.Reflection.MethodOptions::Clone()
extern void MethodOptions_Clone_m4F7F9963D8DD66C5CBCDD6DE8D9044C573755C7B (void);
// 0x000005E5 System.Boolean Google.Protobuf.Reflection.MethodOptions::get_Deprecated()
extern void MethodOptions_get_Deprecated_m453B70C6DB6041AD7F9CEE21E15E3E0648E4A58F (void);
// 0x000005E6 System.Void Google.Protobuf.Reflection.MethodOptions::set_Deprecated(System.Boolean)
extern void MethodOptions_set_Deprecated_mEF21B0E81788197583497701AE63836A899719DB (void);
// 0x000005E7 System.Boolean Google.Protobuf.Reflection.MethodOptions::get_HasDeprecated()
extern void MethodOptions_get_HasDeprecated_mCE0E857CD48CAEFC2FB4D9DFDACD8F25C32877A2 (void);
// 0x000005E8 Google.Protobuf.Reflection.MethodOptions/Types/IdempotencyLevel Google.Protobuf.Reflection.MethodOptions::get_IdempotencyLevel()
extern void MethodOptions_get_IdempotencyLevel_m49E2B7AD7EC9962B4905DEED3F0D7ACBD3632D9A (void);
// 0x000005E9 System.Void Google.Protobuf.Reflection.MethodOptions::set_IdempotencyLevel(Google.Protobuf.Reflection.MethodOptions/Types/IdempotencyLevel)
extern void MethodOptions_set_IdempotencyLevel_mBE4B4DB5A9D6C2BCD5B3F3BE761A412F0FFEEA83 (void);
// 0x000005EA System.Boolean Google.Protobuf.Reflection.MethodOptions::get_HasIdempotencyLevel()
extern void MethodOptions_get_HasIdempotencyLevel_mB77C16E4DF253114A663A813E37C3FCD27857040 (void);
// 0x000005EB System.Boolean Google.Protobuf.Reflection.MethodOptions::Equals(System.Object)
extern void MethodOptions_Equals_mC19CF9BE571A1064D708C490AFBB4DF8508597E2 (void);
// 0x000005EC System.Boolean Google.Protobuf.Reflection.MethodOptions::Equals(Google.Protobuf.Reflection.MethodOptions)
extern void MethodOptions_Equals_mB3CF6D193D1EB5531625EA2135546118AF0F4DA7 (void);
// 0x000005ED System.Int32 Google.Protobuf.Reflection.MethodOptions::GetHashCode()
extern void MethodOptions_GetHashCode_m419CD1A58A8AFCDB6D673259EB2CD03A31400414 (void);
// 0x000005EE System.String Google.Protobuf.Reflection.MethodOptions::ToString()
extern void MethodOptions_ToString_m582EC2A358621D0F1398769E1AF5733E93B2C166 (void);
// 0x000005EF System.Void Google.Protobuf.Reflection.MethodOptions::WriteTo(Google.Protobuf.CodedOutputStream)
extern void MethodOptions_WriteTo_m8DC61AF4DBB7A265BD61718BFB2BA3E149F4CED0 (void);
// 0x000005F0 System.Void Google.Protobuf.Reflection.MethodOptions::pb::Google.Protobuf.IBufferMessage.InternalWriteTo(Google.Protobuf.WriteContext&)
extern void MethodOptions_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m308FFA3BF40A93AD6A3BFD1CE927BD5745F4BA4B (void);
// 0x000005F1 System.Int32 Google.Protobuf.Reflection.MethodOptions::CalculateSize()
extern void MethodOptions_CalculateSize_m7AE8BB6308CC41A549C3409B7DD10E7459F38347 (void);
// 0x000005F2 System.Void Google.Protobuf.Reflection.MethodOptions::MergeFrom(Google.Protobuf.Reflection.MethodOptions)
extern void MethodOptions_MergeFrom_mDBD557C9249B039843EEA039CFBD3A5604D9FCD5 (void);
// 0x000005F3 System.Void Google.Protobuf.Reflection.MethodOptions::MergeFrom(Google.Protobuf.CodedInputStream)
extern void MethodOptions_MergeFrom_m55F1821842F961B9E76D746DEB8F09AA2F93032D (void);
// 0x000005F4 System.Void Google.Protobuf.Reflection.MethodOptions::pb::Google.Protobuf.IBufferMessage.InternalMergeFrom(Google.Protobuf.ParseContext&)
extern void MethodOptions_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_mD8A99A524FB7D62431721187321E3FCA9124E0F1 (void);
// 0x000005F5 TValue Google.Protobuf.Reflection.MethodOptions::GetExtension(Google.Protobuf.Extension`2<Google.Protobuf.Reflection.MethodOptions,TValue>)
// 0x000005F6 Google.Protobuf.Collections.RepeatedField`1<TValue> Google.Protobuf.Reflection.MethodOptions::GetExtension(Google.Protobuf.RepeatedExtension`2<Google.Protobuf.Reflection.MethodOptions,TValue>)
// 0x000005F7 Google.Protobuf.Collections.RepeatedField`1<TValue> Google.Protobuf.Reflection.MethodOptions::GetOrInitializeExtension(Google.Protobuf.RepeatedExtension`2<Google.Protobuf.Reflection.MethodOptions,TValue>)
// 0x000005F8 System.Boolean Google.Protobuf.Reflection.MethodOptions::HasExtension(Google.Protobuf.Extension`2<Google.Protobuf.Reflection.MethodOptions,TValue>)
// 0x000005F9 System.Void Google.Protobuf.Reflection.MethodOptions::ClearExtension(Google.Protobuf.Extension`2<Google.Protobuf.Reflection.MethodOptions,TValue>)
// 0x000005FA System.Void Google.Protobuf.Reflection.MethodOptions::.cctor()
extern void MethodOptions__cctor_m77AC2A0D5A7117AFEF6A250A907395621B97250B (void);
// 0x000005FB System.Void Google.Protobuf.Reflection.MethodOptions/<>c::.cctor()
extern void U3CU3Ec__cctor_mC398777769724D087F9978E51197681D21637BE7 (void);
// 0x000005FC System.Void Google.Protobuf.Reflection.MethodOptions/<>c::.ctor()
extern void U3CU3Ec__ctor_m829CD2280A26CF144B536B9AD5A2D5BB7849A6D8 (void);
// 0x000005FD Google.Protobuf.Reflection.MethodOptions Google.Protobuf.Reflection.MethodOptions/<>c::<.cctor>b__57_0()
extern void U3CU3Ec_U3C_cctorU3Eb__57_0_m0D85273A171DEA4DF6CCB946F5B9C7DED906FF32 (void);
// 0x000005FE Google.Protobuf.MessageParser`1<Google.Protobuf.Reflection.UninterpretedOption> Google.Protobuf.Reflection.UninterpretedOption::get_Parser()
extern void UninterpretedOption_get_Parser_m8A25A0D8CD91A59AF3F96095A8C7FBF704082F87 (void);
// 0x000005FF Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.Reflection.UninterpretedOption::get_Descriptor()
extern void UninterpretedOption_get_Descriptor_m178F55E2F36773B937FB50177A2EA1EED60B24BA (void);
// 0x00000600 Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.Reflection.UninterpretedOption::pb::Google.Protobuf.IMessage.get_Descriptor()
extern void UninterpretedOption_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m84FA1CFC2A958FC52931B105C448510604E328A1 (void);
// 0x00000601 System.Void Google.Protobuf.Reflection.UninterpretedOption::.ctor()
extern void UninterpretedOption__ctor_m01FCE0DA392AF8E60B04329799B2FD66C2A4676D (void);
// 0x00000602 System.Void Google.Protobuf.Reflection.UninterpretedOption::.ctor(Google.Protobuf.Reflection.UninterpretedOption)
extern void UninterpretedOption__ctor_m11693D3C42B33BE6E844F5F6F2810E399BF413EC (void);
// 0x00000603 Google.Protobuf.Reflection.UninterpretedOption Google.Protobuf.Reflection.UninterpretedOption::Clone()
extern void UninterpretedOption_Clone_m0B1D69F3BE93968ACEAECA8835FEB1B5542ECE6A (void);
// 0x00000604 System.String Google.Protobuf.Reflection.UninterpretedOption::get_IdentifierValue()
extern void UninterpretedOption_get_IdentifierValue_mD5B29DF167934C31919F2C72448397D93EE1C660 (void);
// 0x00000605 System.Void Google.Protobuf.Reflection.UninterpretedOption::set_IdentifierValue(System.String)
extern void UninterpretedOption_set_IdentifierValue_mE9A55DB611D4CEDAA41B826B967A968F7D11DE1B (void);
// 0x00000606 System.Boolean Google.Protobuf.Reflection.UninterpretedOption::get_HasIdentifierValue()
extern void UninterpretedOption_get_HasIdentifierValue_m15054E0A8CE07585E05E389B861D6A64C2A9826C (void);
// 0x00000607 System.UInt64 Google.Protobuf.Reflection.UninterpretedOption::get_PositiveIntValue()
extern void UninterpretedOption_get_PositiveIntValue_m3A8C7B091C726FAC6F32BEC182C3D7AAF779F138 (void);
// 0x00000608 System.Void Google.Protobuf.Reflection.UninterpretedOption::set_PositiveIntValue(System.UInt64)
extern void UninterpretedOption_set_PositiveIntValue_m14051B48D3FF3D6319209F0BAAEE164C37BBA7F6 (void);
// 0x00000609 System.Boolean Google.Protobuf.Reflection.UninterpretedOption::get_HasPositiveIntValue()
extern void UninterpretedOption_get_HasPositiveIntValue_mCFCEFB7BF8094469609A8206C715A4FACE91E1E9 (void);
// 0x0000060A System.Int64 Google.Protobuf.Reflection.UninterpretedOption::get_NegativeIntValue()
extern void UninterpretedOption_get_NegativeIntValue_mDF907ADC55BA0D7EAA07D1EF512DE2680794AE0C (void);
// 0x0000060B System.Void Google.Protobuf.Reflection.UninterpretedOption::set_NegativeIntValue(System.Int64)
extern void UninterpretedOption_set_NegativeIntValue_m9879F18843C9ECC5D16B683870A87926E0AB78BE (void);
// 0x0000060C System.Boolean Google.Protobuf.Reflection.UninterpretedOption::get_HasNegativeIntValue()
extern void UninterpretedOption_get_HasNegativeIntValue_mFAAE4B914DEC9FBB3D057FF01A0A51F091D7E73A (void);
// 0x0000060D System.Double Google.Protobuf.Reflection.UninterpretedOption::get_DoubleValue()
extern void UninterpretedOption_get_DoubleValue_mBCA6B09141A76B1C9F58DEC365C1239A6854DB64 (void);
// 0x0000060E System.Void Google.Protobuf.Reflection.UninterpretedOption::set_DoubleValue(System.Double)
extern void UninterpretedOption_set_DoubleValue_mD24E441551CA03B2182ED25B8F20588A3ADF3451 (void);
// 0x0000060F System.Boolean Google.Protobuf.Reflection.UninterpretedOption::get_HasDoubleValue()
extern void UninterpretedOption_get_HasDoubleValue_m01812D763A3048ACFC310A84F0471C3D60DDF27A (void);
// 0x00000610 Google.Protobuf.ByteString Google.Protobuf.Reflection.UninterpretedOption::get_StringValue()
extern void UninterpretedOption_get_StringValue_m1A6CF457CDA8BD51996A33AB200794F054D33F4D (void);
// 0x00000611 System.Void Google.Protobuf.Reflection.UninterpretedOption::set_StringValue(Google.Protobuf.ByteString)
extern void UninterpretedOption_set_StringValue_m80C4D5B8F3CAD7B1DF1414C15C64698D403D9EEE (void);
// 0x00000612 System.Boolean Google.Protobuf.Reflection.UninterpretedOption::get_HasStringValue()
extern void UninterpretedOption_get_HasStringValue_m170914175399C1398923F8138756A3BC76238264 (void);
// 0x00000613 System.String Google.Protobuf.Reflection.UninterpretedOption::get_AggregateValue()
extern void UninterpretedOption_get_AggregateValue_mE551DA9DE1AD64384C5FA59D30D0D76A09255B16 (void);
// 0x00000614 System.Void Google.Protobuf.Reflection.UninterpretedOption::set_AggregateValue(System.String)
extern void UninterpretedOption_set_AggregateValue_m5F918A7FA8B1D9328AED89B89AAFB8785058F290 (void);
// 0x00000615 System.Boolean Google.Protobuf.Reflection.UninterpretedOption::get_HasAggregateValue()
extern void UninterpretedOption_get_HasAggregateValue_m8394E61DBE7BE1C1813823D1C63A073B525A6AA0 (void);
// 0x00000616 System.Boolean Google.Protobuf.Reflection.UninterpretedOption::Equals(System.Object)
extern void UninterpretedOption_Equals_m480D8D7A06EC8E37BDDAE05B1A4A3709F6FC0850 (void);
// 0x00000617 System.Boolean Google.Protobuf.Reflection.UninterpretedOption::Equals(Google.Protobuf.Reflection.UninterpretedOption)
extern void UninterpretedOption_Equals_m695AA83BD422C1433861AB2DFEA337174A603910 (void);
// 0x00000618 System.Int32 Google.Protobuf.Reflection.UninterpretedOption::GetHashCode()
extern void UninterpretedOption_GetHashCode_m9E5BF7D1BE4AA11718BA1841A1A0F0A010FC1ECC (void);
// 0x00000619 System.String Google.Protobuf.Reflection.UninterpretedOption::ToString()
extern void UninterpretedOption_ToString_m5C88AF1EC3E062A8E98637D0316F62C9874A5E5F (void);
// 0x0000061A System.Void Google.Protobuf.Reflection.UninterpretedOption::WriteTo(Google.Protobuf.CodedOutputStream)
extern void UninterpretedOption_WriteTo_mE3E2D1AEAF4379F1E60A80DB89EDD27EE8285255 (void);
// 0x0000061B System.Void Google.Protobuf.Reflection.UninterpretedOption::pb::Google.Protobuf.IBufferMessage.InternalWriteTo(Google.Protobuf.WriteContext&)
extern void UninterpretedOption_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m790A1EEC06943590A53B5FE56456EFA59EB44034 (void);
// 0x0000061C System.Int32 Google.Protobuf.Reflection.UninterpretedOption::CalculateSize()
extern void UninterpretedOption_CalculateSize_m0073EE08E92667DDF1A0764667B6890C0765E0D1 (void);
// 0x0000061D System.Void Google.Protobuf.Reflection.UninterpretedOption::MergeFrom(Google.Protobuf.Reflection.UninterpretedOption)
extern void UninterpretedOption_MergeFrom_mCDAB1F9A576C61CDC4FAA103105C9C3BED5DFCE9 (void);
// 0x0000061E System.Void Google.Protobuf.Reflection.UninterpretedOption::MergeFrom(Google.Protobuf.CodedInputStream)
extern void UninterpretedOption_MergeFrom_m0464D25AE497BE73F7067D4A56C1AFB748FBB16B (void);
// 0x0000061F System.Void Google.Protobuf.Reflection.UninterpretedOption::pb::Google.Protobuf.IBufferMessage.InternalMergeFrom(Google.Protobuf.ParseContext&)
extern void UninterpretedOption_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_mD2F9D9A892C3A6A090D637C759815A6BFA835FE3 (void);
// 0x00000620 System.Void Google.Protobuf.Reflection.UninterpretedOption::.cctor()
extern void UninterpretedOption__cctor_mA6AA5DBFF01E41142A5CD7641F12FBAA4E9E37A2 (void);
// 0x00000621 Google.Protobuf.MessageParser`1<Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart> Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart::get_Parser()
extern void NamePart_get_Parser_m52D44251BA5B107F48BA9D858E82CD1C2499F151 (void);
// 0x00000622 Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart::get_Descriptor()
extern void NamePart_get_Descriptor_mDAF09A4912482470FFDD7664FB4A245050AB4D99 (void);
// 0x00000623 Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart::pb::Google.Protobuf.IMessage.get_Descriptor()
extern void NamePart_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_mB4EBA66DA1D50FDC6D037B624317EAF1C1EC424F (void);
// 0x00000624 System.Void Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart::.ctor()
extern void NamePart__ctor_mA9A9103300EE0850829D3BFE67B52EAD4512F085 (void);
// 0x00000625 System.Void Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart::.ctor(Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart)
extern void NamePart__ctor_mFBC708208AD503A756034768ABF66A5E5E0AF4DD (void);
// 0x00000626 Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart::Clone()
extern void NamePart_Clone_m43B2E0BD86099C758B706C19DB038F56A2409C6F (void);
// 0x00000627 System.String Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart::get_NamePart_()
extern void NamePart_get_NamePart__m745E8A77A6D196C10AEBD19CD97CD8FFFD6771F9 (void);
// 0x00000628 System.Void Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart::set_NamePart_(System.String)
extern void NamePart_set_NamePart__mB05B00F6AEB8BCC4C03176FB92DADB8C7A49B55E (void);
// 0x00000629 System.Boolean Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart::get_HasNamePart_()
extern void NamePart_get_HasNamePart__mCEDB12E23C8A15CAD08E15A07D98285265637660 (void);
// 0x0000062A System.Boolean Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart::get_IsExtension()
extern void NamePart_get_IsExtension_mF1F181A90E440604432916AA943CE44DA7BADDEA (void);
// 0x0000062B System.Void Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart::set_IsExtension(System.Boolean)
extern void NamePart_set_IsExtension_m01E6A8E808AB54ACA78194DB40DC1D897F7713A2 (void);
// 0x0000062C System.Boolean Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart::get_HasIsExtension()
extern void NamePart_get_HasIsExtension_m5A187250EBDBA107E51D79C0BD502D42D5965B9D (void);
// 0x0000062D System.Boolean Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart::Equals(System.Object)
extern void NamePart_Equals_m9F4FC0E7DAF91E9141210A33B1AFE8F2127F68BB (void);
// 0x0000062E System.Boolean Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart::Equals(Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart)
extern void NamePart_Equals_m4F68F7BFA6DC8252C7BF46F9B7B7A4561BC9B297 (void);
// 0x0000062F System.Int32 Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart::GetHashCode()
extern void NamePart_GetHashCode_m84C037E032844843C3501CBBF1D5579160297F11 (void);
// 0x00000630 System.String Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart::ToString()
extern void NamePart_ToString_mF10088283D361B38968A6E57F78255654D3BB2D1 (void);
// 0x00000631 System.Void Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart::WriteTo(Google.Protobuf.CodedOutputStream)
extern void NamePart_WriteTo_m9F9C644A5C5530F04A945671E7BBA3FEF7061448 (void);
// 0x00000632 System.Void Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart::pb::Google.Protobuf.IBufferMessage.InternalWriteTo(Google.Protobuf.WriteContext&)
extern void NamePart_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m0396A0FC112B2CFF1E5B1FD1C0AE400359E96A01 (void);
// 0x00000633 System.Int32 Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart::CalculateSize()
extern void NamePart_CalculateSize_mE593E657449DE890C6273E4C9DC3B90A948C8749 (void);
// 0x00000634 System.Void Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart::MergeFrom(Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart)
extern void NamePart_MergeFrom_mB5380B100DD6F68EDEEF200E1BB000EFC4906DA7 (void);
// 0x00000635 System.Void Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart::MergeFrom(Google.Protobuf.CodedInputStream)
extern void NamePart_MergeFrom_m4889B005A581C203986474994D59CAD2FC9F5F30 (void);
// 0x00000636 System.Void Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart::pb::Google.Protobuf.IBufferMessage.InternalMergeFrom(Google.Protobuf.ParseContext&)
extern void NamePart_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m161542DC71B24D4416D0327851A5EFD2B8A093D2 (void);
// 0x00000637 System.Void Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart::.cctor()
extern void NamePart__cctor_mAD02D8D937C525BAD836480091C7B3FEB91509C4 (void);
// 0x00000638 System.Void Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart/<>c::.cctor()
extern void U3CU3Ec__cctor_mE8A52459A0655D42438ADBC5EF6EE84710B49777 (void);
// 0x00000639 System.Void Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart/<>c::.ctor()
extern void U3CU3Ec__ctor_m14121C5533461FB8EFB0C4F01A4D5BD0AD91EA7F (void);
// 0x0000063A Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart Google.Protobuf.Reflection.UninterpretedOption/Types/NamePart/<>c::<.cctor>b__41_0()
extern void U3CU3Ec_U3C_cctorU3Eb__41_0_m613BA3E40E15DB658425AEB73890F58D554969E4 (void);
// 0x0000063B System.Void Google.Protobuf.Reflection.UninterpretedOption/<>c::.cctor()
extern void U3CU3Ec__cctor_mBA81C0F546421AF123D609A914E9903942045AA4 (void);
// 0x0000063C System.Void Google.Protobuf.Reflection.UninterpretedOption/<>c::.ctor()
extern void U3CU3Ec__ctor_mF1AB9241F810C896EBA8E9BE5CC2AFD3A40D9229 (void);
// 0x0000063D Google.Protobuf.Reflection.UninterpretedOption Google.Protobuf.Reflection.UninterpretedOption/<>c::<.cctor>b__83_0()
extern void U3CU3Ec_U3C_cctorU3Eb__83_0_m8B7EDDAF6E8803E0E6DEDFF72F6632B1415B7ACE (void);
// 0x0000063E Google.Protobuf.MessageParser`1<Google.Protobuf.Reflection.SourceCodeInfo> Google.Protobuf.Reflection.SourceCodeInfo::get_Parser()
extern void SourceCodeInfo_get_Parser_m8E4CF0002B4769C5F797021AB4ADFC77A656E8D6 (void);
// 0x0000063F Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.Reflection.SourceCodeInfo::get_Descriptor()
extern void SourceCodeInfo_get_Descriptor_mC3AEF456721098C0D729AFBFFF1365A7928CC547 (void);
// 0x00000640 Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.Reflection.SourceCodeInfo::pb::Google.Protobuf.IMessage.get_Descriptor()
extern void SourceCodeInfo_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m3D927BDA5AA3529EFC6BF2C344AD0826C5C46AF2 (void);
// 0x00000641 System.Void Google.Protobuf.Reflection.SourceCodeInfo::.ctor()
extern void SourceCodeInfo__ctor_m0A1DC09D0F3B17BC20F44D8201C5D0DB841145E9 (void);
// 0x00000642 System.Void Google.Protobuf.Reflection.SourceCodeInfo::.ctor(Google.Protobuf.Reflection.SourceCodeInfo)
extern void SourceCodeInfo__ctor_m12C51AC307B6497E9CF7C2887ADB23E01B33392A (void);
// 0x00000643 Google.Protobuf.Reflection.SourceCodeInfo Google.Protobuf.Reflection.SourceCodeInfo::Clone()
extern void SourceCodeInfo_Clone_m879DC87218D95C6E0907ABB23C9F355F21C158BA (void);
// 0x00000644 Google.Protobuf.Collections.RepeatedField`1<Google.Protobuf.Reflection.SourceCodeInfo/Types/Location> Google.Protobuf.Reflection.SourceCodeInfo::get_Location()
extern void SourceCodeInfo_get_Location_m16B0802C3859FF4A0EAE4B0F211B4B60054FBAE1 (void);
// 0x00000645 System.Boolean Google.Protobuf.Reflection.SourceCodeInfo::Equals(System.Object)
extern void SourceCodeInfo_Equals_mAC74617ADE26118FC282D20867FF972CF4B90786 (void);
// 0x00000646 System.Boolean Google.Protobuf.Reflection.SourceCodeInfo::Equals(Google.Protobuf.Reflection.SourceCodeInfo)
extern void SourceCodeInfo_Equals_mA2B30829479F4760E5B91E5B65D41B02889BED41 (void);
// 0x00000647 System.Int32 Google.Protobuf.Reflection.SourceCodeInfo::GetHashCode()
extern void SourceCodeInfo_GetHashCode_mCD706831C8044FE179D8117D2526273B88B073A3 (void);
// 0x00000648 System.String Google.Protobuf.Reflection.SourceCodeInfo::ToString()
extern void SourceCodeInfo_ToString_mE4FC459AEF9A122B2893BE043EFA5EF455BA1CAD (void);
// 0x00000649 System.Void Google.Protobuf.Reflection.SourceCodeInfo::WriteTo(Google.Protobuf.CodedOutputStream)
extern void SourceCodeInfo_WriteTo_m11D19F228C061C27C584002104687141B0D514AE (void);
// 0x0000064A System.Void Google.Protobuf.Reflection.SourceCodeInfo::pb::Google.Protobuf.IBufferMessage.InternalWriteTo(Google.Protobuf.WriteContext&)
extern void SourceCodeInfo_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m601DD6F66EB4F4AF24126704725F9979A31EE648 (void);
// 0x0000064B System.Int32 Google.Protobuf.Reflection.SourceCodeInfo::CalculateSize()
extern void SourceCodeInfo_CalculateSize_m5530408BED9841C7EDC134DC3B2CB88F2691B4CA (void);
// 0x0000064C System.Void Google.Protobuf.Reflection.SourceCodeInfo::MergeFrom(Google.Protobuf.Reflection.SourceCodeInfo)
extern void SourceCodeInfo_MergeFrom_mA3BFA5056081A4CFA0CE5DFF332A6815646EEF0A (void);
// 0x0000064D System.Void Google.Protobuf.Reflection.SourceCodeInfo::MergeFrom(Google.Protobuf.CodedInputStream)
extern void SourceCodeInfo_MergeFrom_mEC664CB41897C3A5E540B205E64E6658866D29FC (void);
// 0x0000064E System.Void Google.Protobuf.Reflection.SourceCodeInfo::pb::Google.Protobuf.IBufferMessage.InternalMergeFrom(Google.Protobuf.ParseContext&)
extern void SourceCodeInfo_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m1E724139176220624BC89C54831F1E7A68FA3815 (void);
// 0x0000064F System.Void Google.Protobuf.Reflection.SourceCodeInfo::.cctor()
extern void SourceCodeInfo__cctor_mD3379D252A4597A98F0AC9B5C9F369E499A0C30C (void);
// 0x00000650 Google.Protobuf.MessageParser`1<Google.Protobuf.Reflection.SourceCodeInfo/Types/Location> Google.Protobuf.Reflection.SourceCodeInfo/Types/Location::get_Parser()
extern void Location_get_Parser_mB7AA6C4FB9B6FA6EB4B8C4CC5475D6AC028A10B1 (void);
// 0x00000651 Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.Reflection.SourceCodeInfo/Types/Location::get_Descriptor()
extern void Location_get_Descriptor_mAF9AF217303A7A050DD8AC21A40A039097EBCC08 (void);
// 0x00000652 Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.Reflection.SourceCodeInfo/Types/Location::pb::Google.Protobuf.IMessage.get_Descriptor()
extern void Location_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_mF87BC274A989F4DBC54D998D5B5AA598C5644508 (void);
// 0x00000653 System.Void Google.Protobuf.Reflection.SourceCodeInfo/Types/Location::.ctor()
extern void Location__ctor_mFDC279769BEAE3382B8775935C8683621235F622 (void);
// 0x00000654 System.Void Google.Protobuf.Reflection.SourceCodeInfo/Types/Location::.ctor(Google.Protobuf.Reflection.SourceCodeInfo/Types/Location)
extern void Location__ctor_m28EFCE79F70879A58E3CB6F54CE731BD38BE41DD (void);
// 0x00000655 Google.Protobuf.Reflection.SourceCodeInfo/Types/Location Google.Protobuf.Reflection.SourceCodeInfo/Types/Location::Clone()
extern void Location_Clone_m3C4C0598AE1D773542602ABBFBFC912ABB270BEE (void);
// 0x00000656 Google.Protobuf.Collections.RepeatedField`1<System.Int32> Google.Protobuf.Reflection.SourceCodeInfo/Types/Location::get_Path()
extern void Location_get_Path_m84C8DAADFB80B1112A8BAEB93E662D7CE667384A (void);
// 0x00000657 Google.Protobuf.Collections.RepeatedField`1<System.Int32> Google.Protobuf.Reflection.SourceCodeInfo/Types/Location::get_Span()
extern void Location_get_Span_mFCF1B654E8BF0DB117024A4D1B6C6C1586C35D86 (void);
// 0x00000658 System.String Google.Protobuf.Reflection.SourceCodeInfo/Types/Location::get_LeadingComments()
extern void Location_get_LeadingComments_m7C54DD44CC201EEEA4654C52684874B58A20217C (void);
// 0x00000659 System.Void Google.Protobuf.Reflection.SourceCodeInfo/Types/Location::set_LeadingComments(System.String)
extern void Location_set_LeadingComments_m5C93169FF580ED160690B569AD1DB27A25CF1084 (void);
// 0x0000065A System.Boolean Google.Protobuf.Reflection.SourceCodeInfo/Types/Location::get_HasLeadingComments()
extern void Location_get_HasLeadingComments_m3C25DA28CE42357EA74945D022AD19BB7CEC0997 (void);
// 0x0000065B System.String Google.Protobuf.Reflection.SourceCodeInfo/Types/Location::get_TrailingComments()
extern void Location_get_TrailingComments_m46A2E3D945E49EC181F43757653BA3D43A1F3F86 (void);
// 0x0000065C System.Void Google.Protobuf.Reflection.SourceCodeInfo/Types/Location::set_TrailingComments(System.String)
extern void Location_set_TrailingComments_mD43CECC13E2083938944FEF61AEA9ADDD195E17A (void);
// 0x0000065D System.Boolean Google.Protobuf.Reflection.SourceCodeInfo/Types/Location::get_HasTrailingComments()
extern void Location_get_HasTrailingComments_mED1CB68639231AE0730E2FB6FB2C8875D89EF1F5 (void);
// 0x0000065E Google.Protobuf.Collections.RepeatedField`1<System.String> Google.Protobuf.Reflection.SourceCodeInfo/Types/Location::get_LeadingDetachedComments()
extern void Location_get_LeadingDetachedComments_m83F1C5A77721B6E95FC9FA1EB78A456F542DAD7E (void);
// 0x0000065F System.Boolean Google.Protobuf.Reflection.SourceCodeInfo/Types/Location::Equals(System.Object)
extern void Location_Equals_m85AD8ECE48C60DADB8768733AF1F6563AF4AC281 (void);
// 0x00000660 System.Boolean Google.Protobuf.Reflection.SourceCodeInfo/Types/Location::Equals(Google.Protobuf.Reflection.SourceCodeInfo/Types/Location)
extern void Location_Equals_m8D982EACDA1888B6588CD7689C00D5EBB2985368 (void);
// 0x00000661 System.Int32 Google.Protobuf.Reflection.SourceCodeInfo/Types/Location::GetHashCode()
extern void Location_GetHashCode_m6ACF33DBF8F3F18D5D7DD313D7729EA5839A174B (void);
// 0x00000662 System.String Google.Protobuf.Reflection.SourceCodeInfo/Types/Location::ToString()
extern void Location_ToString_m9B282141CD535FF8261C8195BF3CDDF528C40A4C (void);
// 0x00000663 System.Void Google.Protobuf.Reflection.SourceCodeInfo/Types/Location::WriteTo(Google.Protobuf.CodedOutputStream)
extern void Location_WriteTo_m4210E1B1440977E2A413E0B8F75133982F73EC69 (void);
// 0x00000664 System.Void Google.Protobuf.Reflection.SourceCodeInfo/Types/Location::pb::Google.Protobuf.IBufferMessage.InternalWriteTo(Google.Protobuf.WriteContext&)
extern void Location_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m504F51927BE7797E64ECDEC999C03BB04846EB33 (void);
// 0x00000665 System.Int32 Google.Protobuf.Reflection.SourceCodeInfo/Types/Location::CalculateSize()
extern void Location_CalculateSize_m4FE7C1E7A7BB0E885F9A9779A16EC3D562B6BA3F (void);
// 0x00000666 System.Void Google.Protobuf.Reflection.SourceCodeInfo/Types/Location::MergeFrom(Google.Protobuf.Reflection.SourceCodeInfo/Types/Location)
extern void Location_MergeFrom_mFA64C2E2E2D47FD3294936AD3C696EAE3B0CD8F7 (void);
// 0x00000667 System.Void Google.Protobuf.Reflection.SourceCodeInfo/Types/Location::MergeFrom(Google.Protobuf.CodedInputStream)
extern void Location_MergeFrom_mD097D430111330C3EE05F9F06685B68DB4A7077D (void);
// 0x00000668 System.Void Google.Protobuf.Reflection.SourceCodeInfo/Types/Location::pb::Google.Protobuf.IBufferMessage.InternalMergeFrom(Google.Protobuf.ParseContext&)
extern void Location_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m30960A0BA81FA14E2F5806B1821B17CEB6EC74A8 (void);
// 0x00000669 System.Void Google.Protobuf.Reflection.SourceCodeInfo/Types/Location::.cctor()
extern void Location__cctor_m4F2D645AE7099736A3AA036CED46C3C27A92FB8A (void);
// 0x0000066A System.Void Google.Protobuf.Reflection.SourceCodeInfo/Types/Location/<>c::.cctor()
extern void U3CU3Ec__cctor_m311FEE18EEA023FC4C54A69ED1F10E1073A1A258 (void);
// 0x0000066B System.Void Google.Protobuf.Reflection.SourceCodeInfo/Types/Location/<>c::.ctor()
extern void U3CU3Ec__ctor_m0D7F18F9E242952A4718672889E7A6D4750E8C15 (void);
// 0x0000066C Google.Protobuf.Reflection.SourceCodeInfo/Types/Location Google.Protobuf.Reflection.SourceCodeInfo/Types/Location/<>c::<.cctor>b__55_0()
extern void U3CU3Ec_U3C_cctorU3Eb__55_0_m4EB0DCECA4202B15C7EC20815CEFDEBD1E0AFB9B (void);
// 0x0000066D System.Void Google.Protobuf.Reflection.SourceCodeInfo/<>c::.cctor()
extern void U3CU3Ec__cctor_m4DD420C5F69AC09CA65E4F8C2CDE5983F0877BC8 (void);
// 0x0000066E System.Void Google.Protobuf.Reflection.SourceCodeInfo/<>c::.ctor()
extern void U3CU3Ec__ctor_mF19514F90700678D960AF81872A0077A86CC8772 (void);
// 0x0000066F Google.Protobuf.Reflection.SourceCodeInfo Google.Protobuf.Reflection.SourceCodeInfo/<>c::<.cctor>b__28_0()
extern void U3CU3Ec_U3C_cctorU3Eb__28_0_m907FAD098B56E37E4A833213A6B36ABB4CE4FB74 (void);
// 0x00000670 Google.Protobuf.MessageParser`1<Google.Protobuf.Reflection.GeneratedCodeInfo> Google.Protobuf.Reflection.GeneratedCodeInfo::get_Parser()
extern void GeneratedCodeInfo_get_Parser_m901B7DDFAAA3AF8FDC72519AD01DD576EA0FD055 (void);
// 0x00000671 Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.Reflection.GeneratedCodeInfo::get_Descriptor()
extern void GeneratedCodeInfo_get_Descriptor_m2A40BE0B9007A08DDF69F22C9D6B16B364E08361 (void);
// 0x00000672 Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.Reflection.GeneratedCodeInfo::pb::Google.Protobuf.IMessage.get_Descriptor()
extern void GeneratedCodeInfo_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_mF88EDF6DA1CDE6AF97B451B5917A98DF5D8F0B6B (void);
// 0x00000673 System.Void Google.Protobuf.Reflection.GeneratedCodeInfo::.ctor()
extern void GeneratedCodeInfo__ctor_mDE0BB1D90115580D0C21C264412FD0C6BDC79827 (void);
// 0x00000674 System.Void Google.Protobuf.Reflection.GeneratedCodeInfo::.ctor(Google.Protobuf.Reflection.GeneratedCodeInfo)
extern void GeneratedCodeInfo__ctor_m5588A1B1D68A18DA88CA5DCB4533BE39BA987B0B (void);
// 0x00000675 Google.Protobuf.Reflection.GeneratedCodeInfo Google.Protobuf.Reflection.GeneratedCodeInfo::Clone()
extern void GeneratedCodeInfo_Clone_mB2E07DB9A3075EAAF762DFDCE21F99B40521F491 (void);
// 0x00000676 System.Boolean Google.Protobuf.Reflection.GeneratedCodeInfo::Equals(System.Object)
extern void GeneratedCodeInfo_Equals_m881F3A7864546DCD98F1AC8E5C753DA3B1AEC0A1 (void);
// 0x00000677 System.Boolean Google.Protobuf.Reflection.GeneratedCodeInfo::Equals(Google.Protobuf.Reflection.GeneratedCodeInfo)
extern void GeneratedCodeInfo_Equals_mA65F1EB2D6AE57EAB2AB0B72A3F4892252760755 (void);
// 0x00000678 System.Int32 Google.Protobuf.Reflection.GeneratedCodeInfo::GetHashCode()
extern void GeneratedCodeInfo_GetHashCode_mB9F0A2A53CF8BEB45CBAFE12C8CFCB5819E31C3D (void);
// 0x00000679 System.String Google.Protobuf.Reflection.GeneratedCodeInfo::ToString()
extern void GeneratedCodeInfo_ToString_m325B0EB4FF2412BCDDAD96524195745F0517B9D9 (void);
// 0x0000067A System.Void Google.Protobuf.Reflection.GeneratedCodeInfo::WriteTo(Google.Protobuf.CodedOutputStream)
extern void GeneratedCodeInfo_WriteTo_m66B19D7405A8F6561D49E8A49165FE62712685DF (void);
// 0x0000067B System.Void Google.Protobuf.Reflection.GeneratedCodeInfo::pb::Google.Protobuf.IBufferMessage.InternalWriteTo(Google.Protobuf.WriteContext&)
extern void GeneratedCodeInfo_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_mE9F47B77861B199F58CF5AB795C22FA47A0E3FD4 (void);
// 0x0000067C System.Int32 Google.Protobuf.Reflection.GeneratedCodeInfo::CalculateSize()
extern void GeneratedCodeInfo_CalculateSize_mB7F3C903908BB158E829E98EF242D7BE2070A981 (void);
// 0x0000067D System.Void Google.Protobuf.Reflection.GeneratedCodeInfo::MergeFrom(Google.Protobuf.Reflection.GeneratedCodeInfo)
extern void GeneratedCodeInfo_MergeFrom_m2AF45EF1C2F717A5957EF6F07C088851083D83D2 (void);
// 0x0000067E System.Void Google.Protobuf.Reflection.GeneratedCodeInfo::MergeFrom(Google.Protobuf.CodedInputStream)
extern void GeneratedCodeInfo_MergeFrom_m87520A239A84EFAEA34062FD27265F10E6EE7BCE (void);
// 0x0000067F System.Void Google.Protobuf.Reflection.GeneratedCodeInfo::pb::Google.Protobuf.IBufferMessage.InternalMergeFrom(Google.Protobuf.ParseContext&)
extern void GeneratedCodeInfo_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m68CF657C364AE67E08C90AA4854CA28F745E6AEF (void);
// 0x00000680 System.Void Google.Protobuf.Reflection.GeneratedCodeInfo::.cctor()
extern void GeneratedCodeInfo__cctor_m92C373068D04210DD09EA61906F904DFBDD968D9 (void);
// 0x00000681 Google.Protobuf.MessageParser`1<Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation> Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation::get_Parser()
extern void Annotation_get_Parser_m8134CF19EF949C69B6DDD6103A008A1CF7A51BC7 (void);
// 0x00000682 Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation::get_Descriptor()
extern void Annotation_get_Descriptor_m03B3B365C873BFD9BECDEB8D977CF8C3CA4D161A (void);
// 0x00000683 Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation::pb::Google.Protobuf.IMessage.get_Descriptor()
extern void Annotation_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m838535C2735EA0FFFB83AB7E49836FBF2E1F51E5 (void);
// 0x00000684 System.Void Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation::.ctor()
extern void Annotation__ctor_m9ED6A93F900EBFD3D4BC4980C3D9B032B8F9B5D7 (void);
// 0x00000685 System.Void Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation::.ctor(Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation)
extern void Annotation__ctor_m61D9641ED59D1CDA766721C1A40198C8BF8F6E45 (void);
// 0x00000686 Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation::Clone()
extern void Annotation_Clone_mB86AE67CF1DB8FC7CF33ADDCEF2FEEB74BCDFE5D (void);
// 0x00000687 System.String Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation::get_SourceFile()
extern void Annotation_get_SourceFile_m91B7098C2E39ECA1A8E13E40DB8A7A740C3C80F6 (void);
// 0x00000688 System.Void Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation::set_SourceFile(System.String)
extern void Annotation_set_SourceFile_mB2AED44E9EFB3CA7D5BF5CA03DE3A8F5CE51723E (void);
// 0x00000689 System.Boolean Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation::get_HasSourceFile()
extern void Annotation_get_HasSourceFile_mD261BC3AC77DB72DB7E1BEE3A7A8955D0913C23E (void);
// 0x0000068A System.Int32 Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation::get_Begin()
extern void Annotation_get_Begin_m8891E38C06E537DCBAC9BEC72E83162AF4F6E9AC (void);
// 0x0000068B System.Void Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation::set_Begin(System.Int32)
extern void Annotation_set_Begin_m5AA2016E643BF3BF22002C930EAACD7AE75ABDD5 (void);
// 0x0000068C System.Boolean Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation::get_HasBegin()
extern void Annotation_get_HasBegin_m86E2171FE0BDCBB1B5A075E45C7A8DDDB931E97B (void);
// 0x0000068D System.Int32 Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation::get_End()
extern void Annotation_get_End_m1AB89595BE4271A333A470BDC5206C2814C050D9 (void);
// 0x0000068E System.Void Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation::set_End(System.Int32)
extern void Annotation_set_End_m5BDF9D827DEC0FF6DDA13230D1C99C57817FA7C7 (void);
// 0x0000068F System.Boolean Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation::get_HasEnd()
extern void Annotation_get_HasEnd_mF1171BE52E3B2EFB80EFD5480675B2655897470E (void);
// 0x00000690 System.Boolean Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation::Equals(System.Object)
extern void Annotation_Equals_m5C1A51E20393172CE798E42CAD95C3DD71E511C8 (void);
// 0x00000691 System.Boolean Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation::Equals(Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation)
extern void Annotation_Equals_mD66C9282185C31DF563EED063B1019BE13C2811E (void);
// 0x00000692 System.Int32 Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation::GetHashCode()
extern void Annotation_GetHashCode_m6F1BE21B2168F0D61B4E600E21DDC81A22132BF2 (void);
// 0x00000693 System.String Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation::ToString()
extern void Annotation_ToString_m2E543D1B02E6207CB9009CEA5A987A6439466064 (void);
// 0x00000694 System.Void Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation::WriteTo(Google.Protobuf.CodedOutputStream)
extern void Annotation_WriteTo_m108B8AEABCE4F89A6E90997BAC8AC9371A53565D (void);
// 0x00000695 System.Void Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation::pb::Google.Protobuf.IBufferMessage.InternalWriteTo(Google.Protobuf.WriteContext&)
extern void Annotation_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m2D0F50AAD5686905CCAA2959D0C6E112FDA37E85 (void);
// 0x00000696 System.Int32 Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation::CalculateSize()
extern void Annotation_CalculateSize_m1DE1ABE6707A9C1C6D415715E6EB90D7DDD98AB8 (void);
// 0x00000697 System.Void Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation::MergeFrom(Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation)
extern void Annotation_MergeFrom_m2468E39EA5A58BCFA6F9E35D01002292E3CC450F (void);
// 0x00000698 System.Void Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation::MergeFrom(Google.Protobuf.CodedInputStream)
extern void Annotation_MergeFrom_m8CA8D3549BCBA484E4BA0ADCDE8200E20CCEDFBD (void);
// 0x00000699 System.Void Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation::pb::Google.Protobuf.IBufferMessage.InternalMergeFrom(Google.Protobuf.ParseContext&)
extern void Annotation_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m314FDFF4E0741C746FA60D608C45F8C01E8A9117 (void);
// 0x0000069A System.Void Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation::.cctor()
extern void Annotation__cctor_m828EDEED29164F265B88F07BDC87ED3A6B0916AF (void);
// 0x0000069B System.Void Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation/<>c::.cctor()
extern void U3CU3Ec__cctor_m6812A7FE464B0BD846EBAB7F8B83B3E764B351DD (void);
// 0x0000069C System.Void Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation/<>c::.ctor()
extern void U3CU3Ec__ctor_mCC6C861EA46D5F38CE7B1D231D970DAFF8929007 (void);
// 0x0000069D Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation Google.Protobuf.Reflection.GeneratedCodeInfo/Types/Annotation/<>c::<.cctor>b__55_0()
extern void U3CU3Ec_U3C_cctorU3Eb__55_0_m3FCF05CD6B82754BB25E87E1A3A8225796468906 (void);
// 0x0000069E System.Void Google.Protobuf.Reflection.GeneratedCodeInfo/<>c::.cctor()
extern void U3CU3Ec__cctor_mE6C011350A2644975F83F6EDD92390F4D65EABF0 (void);
// 0x0000069F System.Void Google.Protobuf.Reflection.GeneratedCodeInfo/<>c::.ctor()
extern void U3CU3Ec__ctor_mCC8B95876E8186437C0512E3622F829A8CDEC6BC (void);
// 0x000006A0 Google.Protobuf.Reflection.GeneratedCodeInfo Google.Protobuf.Reflection.GeneratedCodeInfo/<>c::<.cctor>b__28_0()
extern void U3CU3Ec_U3C_cctorU3Eb__28_0_mE7837BAF9CA3B043285CE7FB0009A563519DE96F (void);
// 0x000006A1 System.Void Google.Protobuf.Reflection.DescriptorBase::.ctor(Google.Protobuf.Reflection.FileDescriptor,System.String,System.Int32)
extern void DescriptorBase__ctor_m3CBAAC5484603E366E74D8555F06DAFD3CE18624 (void);
// 0x000006A2 System.String Google.Protobuf.Reflection.DescriptorBase::get_Name()
// 0x000006A3 System.String Google.Protobuf.Reflection.DescriptorBase::get_FullName()
extern void DescriptorBase_get_FullName_m915949C2A2E317ADAAA3141CB30DE432F1D6047D (void);
// 0x000006A4 Google.Protobuf.Reflection.FileDescriptor Google.Protobuf.Reflection.DescriptorBase::get_File()
extern void DescriptorBase_get_File_mFBA850CF476DBD50BF1AB469555EAD69FFD827A4 (void);
// 0x000006A5 System.Collections.Generic.IReadOnlyList`1<Google.Protobuf.Reflection.DescriptorBase> Google.Protobuf.Reflection.DescriptorBase::GetNestedDescriptorListForField(System.Int32)
extern void DescriptorBase_GetNestedDescriptorListForField_m8CF298873F30256140DAB157A67A3B12F607E3A7 (void);
// 0x000006A6 System.Int32 Google.Protobuf.Reflection.DescriptorDeclaration::get_StartLine()
extern void DescriptorDeclaration_get_StartLine_mECF4D235763DA0659A3E0E32D5ED6F286FAA6920 (void);
// 0x000006A7 System.Void Google.Protobuf.Reflection.DescriptorDeclaration::.ctor(Google.Protobuf.Reflection.IDescriptor,Google.Protobuf.Reflection.SourceCodeInfo/Types/Location)
extern void DescriptorDeclaration__ctor_m10B83BB65DA7F729601AD1D939AC0CFD5AC4F593 (void);
// 0x000006A8 Google.Protobuf.Reflection.DescriptorDeclaration Google.Protobuf.Reflection.DescriptorDeclaration::FromProto(Google.Protobuf.Reflection.IDescriptor,Google.Protobuf.Reflection.SourceCodeInfo/Types/Location)
extern void DescriptorDeclaration_FromProto_m99719C1FCF22A196785EBF1080E07B56AE12771E (void);
// 0x000006A9 System.Void Google.Protobuf.Reflection.DescriptorPool::.ctor(System.Collections.Generic.IEnumerable`1<Google.Protobuf.Reflection.FileDescriptor>)
extern void DescriptorPool__ctor_m3446B1F10FAE222DCC0C07601F7643870492D4E4 (void);
// 0x000006AA System.Void Google.Protobuf.Reflection.DescriptorPool::ImportPublicDependencies(Google.Protobuf.Reflection.FileDescriptor)
extern void DescriptorPool_ImportPublicDependencies_m3F1100ADD0AFF8C71E175D47D8BBDA8DDE0CB862 (void);
// 0x000006AB T Google.Protobuf.Reflection.DescriptorPool::FindSymbol(System.String)
// 0x000006AC System.Void Google.Protobuf.Reflection.DescriptorPool::AddPackage(System.String,Google.Protobuf.Reflection.FileDescriptor)
extern void DescriptorPool_AddPackage_mC40972982B553A005B1CEE0FA8B6445C1C6E927E (void);
// 0x000006AD System.Void Google.Protobuf.Reflection.DescriptorPool::AddSymbol(Google.Protobuf.Reflection.IDescriptor)
extern void DescriptorPool_AddSymbol_mC6C924B467AB9351A61B0E6ACFD993F6280AF69D (void);
// 0x000006AE System.Void Google.Protobuf.Reflection.DescriptorPool::ValidateSymbolName(Google.Protobuf.Reflection.IDescriptor)
extern void DescriptorPool_ValidateSymbolName_mA33C25FFD11FE6FCE83FEEF25295663C375766C9 (void);
// 0x000006AF Google.Protobuf.Reflection.FieldDescriptor Google.Protobuf.Reflection.DescriptorPool::FindFieldByNumber(Google.Protobuf.Reflection.MessageDescriptor,System.Int32)
extern void DescriptorPool_FindFieldByNumber_mC43A5674F9B8820C8AAED2559A7CF18D9F4B65E4 (void);
// 0x000006B0 System.Void Google.Protobuf.Reflection.DescriptorPool::AddFieldByNumber(Google.Protobuf.Reflection.FieldDescriptor)
extern void DescriptorPool_AddFieldByNumber_m1517BFCF4BE7FB64ED7D2DD35698035103279EBA (void);
// 0x000006B1 System.Void Google.Protobuf.Reflection.DescriptorPool::AddEnumValueByNumber(Google.Protobuf.Reflection.EnumValueDescriptor)
extern void DescriptorPool_AddEnumValueByNumber_mC17A3CEF22AE41D774AE5FB05525FFF69BAF3D97 (void);
// 0x000006B2 Google.Protobuf.Reflection.IDescriptor Google.Protobuf.Reflection.DescriptorPool::LookupSymbol(System.String,Google.Protobuf.Reflection.IDescriptor)
extern void DescriptorPool_LookupSymbol_m88CAFD3C6CA1FBDE5EADDEC6B49A0AA035612132 (void);
// 0x000006B3 System.Void Google.Protobuf.Reflection.DescriptorPool::.cctor()
extern void DescriptorPool__cctor_m90DDEA8CF7C71FF98B0AA65E5C7521A96A3E956A (void);
// 0x000006B4 System.Collections.Generic.IList`1<TOutput> Google.Protobuf.Reflection.DescriptorUtil::ConvertAndMakeReadOnly(System.Collections.Generic.IList`1<TInput>,Google.Protobuf.Reflection.DescriptorUtil/IndexedConverter`2<TInput,TOutput>)
// 0x000006B5 System.Void Google.Protobuf.Reflection.DescriptorUtil/IndexedConverter`2::.ctor(System.Object,System.IntPtr)
// 0x000006B6 TOutput Google.Protobuf.Reflection.DescriptorUtil/IndexedConverter`2::Invoke(TInput,System.Int32)
// 0x000006B7 System.IAsyncResult Google.Protobuf.Reflection.DescriptorUtil/IndexedConverter`2::BeginInvoke(TInput,System.Int32,System.AsyncCallback,System.Object)
// 0x000006B8 TOutput Google.Protobuf.Reflection.DescriptorUtil/IndexedConverter`2::EndInvoke(System.IAsyncResult)
// 0x000006B9 System.Void Google.Protobuf.Reflection.DescriptorValidationException::.ctor(Google.Protobuf.Reflection.IDescriptor,System.String)
extern void DescriptorValidationException__ctor_mEE5B42C9FE1D522DE79FF92C402AE6B1304D250B (void);
// 0x000006BA System.Void Google.Protobuf.Reflection.EnumDescriptor::.ctor(Google.Protobuf.Reflection.EnumDescriptorProto,Google.Protobuf.Reflection.FileDescriptor,Google.Protobuf.Reflection.MessageDescriptor,System.Int32,System.Type)
extern void EnumDescriptor__ctor_mDA2A55D7BF57FD4C483811A71ECE81E8F610076C (void);
// 0x000006BB System.String Google.Protobuf.Reflection.EnumDescriptor::get_Name()
extern void EnumDescriptor_get_Name_m30805FE69B2F54A40BFE040B98CACFC8B86BAAD1 (void);
// 0x000006BC System.Collections.Generic.IReadOnlyList`1<Google.Protobuf.Reflection.DescriptorBase> Google.Protobuf.Reflection.EnumDescriptor::GetNestedDescriptorListForField(System.Int32)
extern void EnumDescriptor_GetNestedDescriptorListForField_m2350A2452DA44A3A1CE99B7D3B17638BEC742A47 (void);
// 0x000006BD System.Collections.Generic.IList`1<Google.Protobuf.Reflection.EnumValueDescriptor> Google.Protobuf.Reflection.EnumDescriptor::get_Values()
extern void EnumDescriptor_get_Values_mC452E4F3B573CFA9BCFB0066C2C2E591C56EF7CA (void);
// 0x000006BE System.Void Google.Protobuf.Reflection.EnumDescriptor/<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_m3CD1BD5E0682052BC2736E123DA1E0CD643F5F19 (void);
// 0x000006BF Google.Protobuf.Reflection.EnumValueDescriptor Google.Protobuf.Reflection.EnumDescriptor/<>c__DisplayClass4_0::<.ctor>b__0(Google.Protobuf.Reflection.EnumValueDescriptorProto,System.Int32)
extern void U3CU3Ec__DisplayClass4_0_U3C_ctorU3Eb__0_mB714443299145966BFEE6F1D77782F527C0BD297 (void);
// 0x000006C0 System.Void Google.Protobuf.Reflection.EnumValueDescriptor::.ctor(Google.Protobuf.Reflection.EnumValueDescriptorProto,Google.Protobuf.Reflection.FileDescriptor,Google.Protobuf.Reflection.EnumDescriptor,System.Int32)
extern void EnumValueDescriptor__ctor_mEA61C9FBBDF476EDD95613E82D81F79A3408E13E (void);
// 0x000006C1 Google.Protobuf.Reflection.EnumValueDescriptorProto Google.Protobuf.Reflection.EnumValueDescriptor::get_Proto()
extern void EnumValueDescriptor_get_Proto_mDCAD4DA8B13A5C4E600D9851320D498AAF568EE3 (void);
// 0x000006C2 System.String Google.Protobuf.Reflection.EnumValueDescriptor::get_Name()
extern void EnumValueDescriptor_get_Name_m2443B3332402EE679943A717757DE5D5FE4CCEE1 (void);
// 0x000006C3 System.Int32 Google.Protobuf.Reflection.EnumValueDescriptor::get_Number()
extern void EnumValueDescriptor_get_Number_m8D2442A9BF95C527E5AD80513FB64DE556CEC87B (void);
// 0x000006C4 Google.Protobuf.Reflection.EnumDescriptor Google.Protobuf.Reflection.EnumValueDescriptor::get_EnumDescriptor()
extern void EnumValueDescriptor_get_EnumDescriptor_mC63A873A0076016527E5702DD577842216DCFCDC (void);
// 0x000006C5 System.Void Google.Protobuf.Reflection.ExtensionAccessor::.ctor(Google.Protobuf.Reflection.FieldDescriptor)
extern void ExtensionAccessor__ctor_m4ACA7FC1F7AAEF568FF3CFF3479691E5E10A14D4 (void);
// 0x000006C6 Google.Protobuf.Reflection.FieldDescriptor Google.Protobuf.Reflection.ExtensionAccessor::get_Descriptor()
extern void ExtensionAccessor_get_Descriptor_m4D3230A77360F27ED2DDBCD1A52FBB7644A3BB9D (void);
// 0x000006C7 System.Void Google.Protobuf.Reflection.ExtensionAccessor::Clear(Google.Protobuf.IMessage)
extern void ExtensionAccessor_Clear_m29286BD928CB332BC464F5DB5483D2226B4D3DE5 (void);
// 0x000006C8 System.Boolean Google.Protobuf.Reflection.ExtensionAccessor::HasValue(Google.Protobuf.IMessage)
extern void ExtensionAccessor_HasValue_mE22212F94D6D425DAA7ECB3F9A34D3EAB5866C09 (void);
// 0x000006C9 System.Object Google.Protobuf.Reflection.ExtensionAccessor::GetValue(Google.Protobuf.IMessage)
extern void ExtensionAccessor_GetValue_m1EF5981EE49DE9FA7FEDCEAE261B42C046593B67 (void);
// 0x000006CA System.Void Google.Protobuf.Reflection.ExtensionCollection::.ctor(Google.Protobuf.Reflection.FileDescriptor,Google.Protobuf.Extension[])
extern void ExtensionCollection__ctor_mBC1AC9F2DDFF484254B83482F2AC84AF58E09C70 (void);
// 0x000006CB System.Void Google.Protobuf.Reflection.ExtensionCollection::.ctor(Google.Protobuf.Reflection.MessageDescriptor,Google.Protobuf.Extension[])
extern void ExtensionCollection__ctor_m56E5E750DFBCAF06299847ECF6DD0D583D500F3E (void);
// 0x000006CC System.Collections.Generic.IList`1<Google.Protobuf.Reflection.FieldDescriptor> Google.Protobuf.Reflection.ExtensionCollection::get_UnorderedExtensions()
extern void ExtensionCollection_get_UnorderedExtensions_mE70760039915D512775C9C2193AE020D011B0FCD (void);
// 0x000006CD System.Void Google.Protobuf.Reflection.ExtensionCollection::CrossLink()
extern void ExtensionCollection_CrossLink_m37C7BCF7C6F0FBF1C9C542D491D6062DA1F9BD73 (void);
// 0x000006CE System.Void Google.Protobuf.Reflection.ExtensionCollection/<>c__DisplayClass2_0::.ctor()
extern void U3CU3Ec__DisplayClass2_0__ctor_m2ED777838168A9CC6F1BB1EC2AC8D02C681B068E (void);
// 0x000006CF Google.Protobuf.Reflection.FieldDescriptor Google.Protobuf.Reflection.ExtensionCollection/<>c__DisplayClass2_0::<.ctor>b__0(Google.Protobuf.Reflection.FieldDescriptorProto,System.Int32)
extern void U3CU3Ec__DisplayClass2_0_U3C_ctorU3Eb__0_m558AD2CC97BBFEC6B85A7F348CCAA432FBE03A4A (void);
// 0x000006D0 System.Void Google.Protobuf.Reflection.ExtensionCollection/<>c__DisplayClass3_0::.ctor()
extern void U3CU3Ec__DisplayClass3_0__ctor_mB106836DF16F7428328204C09587D10A733F8A66 (void);
// 0x000006D1 Google.Protobuf.Reflection.FieldDescriptor Google.Protobuf.Reflection.ExtensionCollection/<>c__DisplayClass3_0::<.ctor>b__0(Google.Protobuf.Reflection.FieldDescriptorProto,System.Int32)
extern void U3CU3Ec__DisplayClass3_0_U3C_ctorU3Eb__0_mA1A32BE5DEE2B7A1A971607E15393B67ECE985A1 (void);
// 0x000006D2 System.Void Google.Protobuf.Reflection.ExtensionCollection/<>c::.cctor()
extern void U3CU3Ec__cctor_m63DAC6F6D5C73A9410138963A8F2A0F5B0D96789 (void);
// 0x000006D3 System.Void Google.Protobuf.Reflection.ExtensionCollection/<>c::.ctor()
extern void U3CU3Ec__ctor_m1A360C8649F78B39FADAB4E32F8B8DCE6ED4C553 (void);
// 0x000006D4 Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.Reflection.ExtensionCollection/<>c::<CrossLink>b__9_0(System.Collections.Generic.KeyValuePair`2<Google.Protobuf.Reflection.MessageDescriptor,System.Collections.Generic.IList`1<Google.Protobuf.Reflection.FieldDescriptor>>)
extern void U3CU3Ec_U3CCrossLinkU3Eb__9_0_mA69041194D5AFD2AEB3994919282DD2C61DBEF4A (void);
// 0x000006D5 System.Collections.Generic.IList`1<Google.Protobuf.Reflection.FieldDescriptor> Google.Protobuf.Reflection.ExtensionCollection/<>c::<CrossLink>b__9_1(System.Collections.Generic.KeyValuePair`2<Google.Protobuf.Reflection.MessageDescriptor,System.Collections.Generic.IList`1<Google.Protobuf.Reflection.FieldDescriptor>>)
extern void U3CU3Ec_U3CCrossLinkU3Eb__9_1_m007A16D2D19D320F04BFBB718E4D97541B2C2FCD (void);
// 0x000006D6 Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.Reflection.ExtensionCollection/<>c::<CrossLink>b__9_2(System.Collections.Generic.KeyValuePair`2<Google.Protobuf.Reflection.MessageDescriptor,System.Collections.Generic.IList`1<Google.Protobuf.Reflection.FieldDescriptor>>)
extern void U3CU3Ec_U3CCrossLinkU3Eb__9_2_mB17CB78CECF7D04F38F880AC236D87C18D11B86A (void);
// 0x000006D7 System.Collections.Generic.IList`1<Google.Protobuf.Reflection.FieldDescriptor> Google.Protobuf.Reflection.ExtensionCollection/<>c::<CrossLink>b__9_3(System.Collections.Generic.KeyValuePair`2<Google.Protobuf.Reflection.MessageDescriptor,System.Collections.Generic.IList`1<Google.Protobuf.Reflection.FieldDescriptor>>)
extern void U3CU3Ec_U3CCrossLinkU3Eb__9_3_m71224EA1CE8E870B632CC0125BCD2F957BCEC53F (void);
// 0x000006D8 System.Int32 Google.Protobuf.Reflection.ExtensionCollection/<>c::<CrossLink>b__9_4(Google.Protobuf.Reflection.FieldDescriptor)
extern void U3CU3Ec_U3CCrossLinkU3Eb__9_4_m4CC0AD06B114348090A7EDEE490784E01BA71C9B (void);
// 0x000006D9 System.Void Google.Protobuf.Reflection.FieldAccessorBase::.ctor(System.Reflection.PropertyInfo,Google.Protobuf.Reflection.FieldDescriptor)
extern void FieldAccessorBase__ctor_mDA90EDBE6ABD75ACD7CE4865E1AC0AF671166C7D (void);
// 0x000006DA Google.Protobuf.Reflection.FieldDescriptor Google.Protobuf.Reflection.FieldAccessorBase::get_Descriptor()
extern void FieldAccessorBase_get_Descriptor_mB23889A8B4C924C6371B1BE7E93FF6AE85873ABE (void);
// 0x000006DB System.Object Google.Protobuf.Reflection.FieldAccessorBase::GetValue(Google.Protobuf.IMessage)
extern void FieldAccessorBase_GetValue_mFCDE1F63C6914FFBAAA9250C92099283CA476D04 (void);
// 0x000006DC System.Boolean Google.Protobuf.Reflection.FieldAccessorBase::HasValue(Google.Protobuf.IMessage)
// 0x000006DD System.Void Google.Protobuf.Reflection.FieldAccessorBase::Clear(Google.Protobuf.IMessage)
// 0x000006DE System.Void Google.Protobuf.Reflection.FieldAccessorBase::SetValue(Google.Protobuf.IMessage,System.Object)
// 0x000006DF Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.Reflection.FieldDescriptor::get_ContainingType()
extern void FieldDescriptor_get_ContainingType_mBEBF37A2458327C9419A884CB13132958B0009CA (void);
// 0x000006E0 Google.Protobuf.Reflection.OneofDescriptor Google.Protobuf.Reflection.FieldDescriptor::get_ContainingOneof()
extern void FieldDescriptor_get_ContainingOneof_m792FE02B65FBBEFDCFA68FCE08CE1B789C935BE9 (void);
// 0x000006E1 Google.Protobuf.Reflection.OneofDescriptor Google.Protobuf.Reflection.FieldDescriptor::get_RealContainingOneof()
extern void FieldDescriptor_get_RealContainingOneof_m3226A16C8578309F0FA967FBAC9EB77AC9E5BC38 (void);
// 0x000006E2 System.String Google.Protobuf.Reflection.FieldDescriptor::get_JsonName()
extern void FieldDescriptor_get_JsonName_m37E80D133D994BDE854220D920790A1E1BBDAF50 (void);
// 0x000006E3 System.Boolean Google.Protobuf.Reflection.FieldDescriptor::get_HasPresence()
extern void FieldDescriptor_get_HasPresence_m8E6C4D1B98C4A2101F1CC09904BDCC27B6F94D04 (void);
// 0x000006E4 Google.Protobuf.Reflection.FieldDescriptorProto Google.Protobuf.Reflection.FieldDescriptor::get_Proto()
extern void FieldDescriptor_get_Proto_m193D4347BAF5B48C4CDACF7FEBEB0EB7ECB1A5CB (void);
// 0x000006E5 Google.Protobuf.Extension Google.Protobuf.Reflection.FieldDescriptor::get_Extension()
extern void FieldDescriptor_get_Extension_m47CB4AEB81E5372FE16AF15451EE3DB8F96F95BA (void);
// 0x000006E6 System.Void Google.Protobuf.Reflection.FieldDescriptor::.ctor(Google.Protobuf.Reflection.FieldDescriptorProto,Google.Protobuf.Reflection.FileDescriptor,Google.Protobuf.Reflection.MessageDescriptor,System.Int32,System.String,Google.Protobuf.Extension)
extern void FieldDescriptor__ctor_mD92149FF28235D1FC63C5A5A2B2FC0406E5AE75C (void);
// 0x000006E7 System.String Google.Protobuf.Reflection.FieldDescriptor::get_Name()
extern void FieldDescriptor_get_Name_mB196272140AC4DEA270A676EFB4D21CC845A3780 (void);
// 0x000006E8 Google.Protobuf.Reflection.IFieldAccessor Google.Protobuf.Reflection.FieldDescriptor::get_Accessor()
extern void FieldDescriptor_get_Accessor_m718155654529CB79855E2BFFC48E50B52EB5391F (void);
// 0x000006E9 Google.Protobuf.Reflection.FieldType Google.Protobuf.Reflection.FieldDescriptor::GetFieldTypeFromProtoType(Google.Protobuf.Reflection.FieldDescriptorProto/Types/Type)
extern void FieldDescriptor_GetFieldTypeFromProtoType_mB9A10AADB44459B7297AB37B159F2F756C733DD4 (void);
// 0x000006EA System.Boolean Google.Protobuf.Reflection.FieldDescriptor::get_IsRepeated()
extern void FieldDescriptor_get_IsRepeated_m7F3022FC108ECC88C011541D4F9FD9C3E3055402 (void);
// 0x000006EB System.Boolean Google.Protobuf.Reflection.FieldDescriptor::get_IsMap()
extern void FieldDescriptor_get_IsMap_m3B9F4C45E70C951A309829EADB9511F613DFFB17 (void);
// 0x000006EC Google.Protobuf.Reflection.FieldType Google.Protobuf.Reflection.FieldDescriptor::get_FieldType()
extern void FieldDescriptor_get_FieldType_m8CDD564F9C32B9A063FD9F3445E07E4BAEB71BB3 (void);
// 0x000006ED System.Int32 Google.Protobuf.Reflection.FieldDescriptor::get_FieldNumber()
extern void FieldDescriptor_get_FieldNumber_mFF0472DC4B8038BA9084A1BF8B0891389E17C19A (void);
// 0x000006EE System.Int32 Google.Protobuf.Reflection.FieldDescriptor::CompareTo(Google.Protobuf.Reflection.FieldDescriptor)
extern void FieldDescriptor_CompareTo_mA0749274411F2F1B494CE5076DA3F139080FCD22 (void);
// 0x000006EF Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.Reflection.FieldDescriptor::get_ExtendeeType()
extern void FieldDescriptor_get_ExtendeeType_mFADEA70D2F9458933EABF6DA40185DE074B4B175 (void);
// 0x000006F0 System.Void Google.Protobuf.Reflection.FieldDescriptor::CrossLink()
extern void FieldDescriptor_CrossLink_m484E2D06235B6620B9048EF0A0B7CABCC4483E0D (void);
// 0x000006F1 Google.Protobuf.Reflection.IFieldAccessor Google.Protobuf.Reflection.FieldDescriptor::CreateAccessor()
extern void FieldDescriptor_CreateAccessor_m6C2A60C7A38E1C04C77A45D8BF0D472BE9D3C92F (void);
// 0x000006F2 System.Void Google.Protobuf.Reflection.FileDescriptor::.cctor()
extern void FileDescriptor__cctor_m1E81B80D00FA0C1EC79C602D09D33FE091C32FDB (void);
// 0x000006F3 System.Void Google.Protobuf.Reflection.FileDescriptor::.ctor(Google.Protobuf.ByteString,Google.Protobuf.Reflection.FileDescriptorProto,System.Collections.Generic.IEnumerable`1<Google.Protobuf.Reflection.FileDescriptor>,Google.Protobuf.Reflection.DescriptorPool,System.Boolean,Google.Protobuf.Reflection.GeneratedClrTypeInfo)
extern void FileDescriptor__ctor_m61879DEE1769B5A93F930CB2B582B5E8B8C7B51E (void);
// 0x000006F4 System.Collections.Generic.Dictionary`2<Google.Protobuf.Reflection.IDescriptor,Google.Protobuf.Reflection.DescriptorDeclaration> Google.Protobuf.Reflection.FileDescriptor::CreateDeclarationMap()
extern void FileDescriptor_CreateDeclarationMap_mE75594435C5FC246AEE0CEFA165AD8B7884DC10C (void);
// 0x000006F5 Google.Protobuf.Reflection.IDescriptor Google.Protobuf.Reflection.FileDescriptor::FindDescriptorForPath(System.Collections.Generic.IList`1<System.Int32>)
extern void FileDescriptor_FindDescriptorForPath_mAF95CCD6F8978D3C6D86EB058902E6E748400F3E (void);
// 0x000006F6 Google.Protobuf.Reflection.DescriptorBase Google.Protobuf.Reflection.FileDescriptor::GetDescriptorFromList(System.Collections.Generic.IReadOnlyList`1<Google.Protobuf.Reflection.DescriptorBase>,System.Int32)
extern void FileDescriptor_GetDescriptorFromList_mB2F7045F8B685140BC31D9CC1D50D9680E8FD86C (void);
// 0x000006F7 System.Collections.Generic.IReadOnlyList`1<Google.Protobuf.Reflection.DescriptorBase> Google.Protobuf.Reflection.FileDescriptor::GetNestedDescriptorListForField(System.Int32)
extern void FileDescriptor_GetNestedDescriptorListForField_m1A075CC14E96C6BE62BC9A6179A6F169F9D27777 (void);
// 0x000006F8 System.String Google.Protobuf.Reflection.FileDescriptor::ComputeFullName(Google.Protobuf.Reflection.MessageDescriptor,System.String)
extern void FileDescriptor_ComputeFullName_m1E33F379B1DF41015BAEF029AA5350FDEC2F0BF9 (void);
// 0x000006F9 System.Collections.Generic.IList`1<Google.Protobuf.Reflection.FileDescriptor> Google.Protobuf.Reflection.FileDescriptor::DeterminePublicDependencies(Google.Protobuf.Reflection.FileDescriptor,Google.Protobuf.Reflection.FileDescriptorProto,System.Collections.Generic.IEnumerable`1<Google.Protobuf.Reflection.FileDescriptor>,System.Boolean)
extern void FileDescriptor_DeterminePublicDependencies_mE391E9B57841EDA9C511E5E127FFDDF42B1E56AE (void);
// 0x000006FA Google.Protobuf.Reflection.FileDescriptorProto Google.Protobuf.Reflection.FileDescriptor::get_Proto()
extern void FileDescriptor_get_Proto_m3861FEA6DC13D62F54CC04BD11AAAAA1CC9D235B (void);
// 0x000006FB Google.Protobuf.Reflection.Syntax Google.Protobuf.Reflection.FileDescriptor::get_Syntax()
extern void FileDescriptor_get_Syntax_mC6D5E49E01F937101A4C15FDA84933E5E3AAD38C (void);
// 0x000006FC System.String Google.Protobuf.Reflection.FileDescriptor::get_Name()
extern void FileDescriptor_get_Name_m359361EF303E319D93FD2EE80C56C13BFCE88463 (void);
// 0x000006FD System.String Google.Protobuf.Reflection.FileDescriptor::get_Package()
extern void FileDescriptor_get_Package_mED949BF62398400887F1E3610052C3DB35D1CF35 (void);
// 0x000006FE System.Collections.Generic.IList`1<Google.Protobuf.Reflection.MessageDescriptor> Google.Protobuf.Reflection.FileDescriptor::get_MessageTypes()
extern void FileDescriptor_get_MessageTypes_m55CD9B2EB841D0F8FF161F0DB15A2D67266C536C (void);
// 0x000006FF System.Collections.Generic.IList`1<Google.Protobuf.Reflection.EnumDescriptor> Google.Protobuf.Reflection.FileDescriptor::get_EnumTypes()
extern void FileDescriptor_get_EnumTypes_m52F61381D43042CA3A0B4B0FF30BB33ED4BE8D44 (void);
// 0x00000700 System.Collections.Generic.IList`1<Google.Protobuf.Reflection.ServiceDescriptor> Google.Protobuf.Reflection.FileDescriptor::get_Services()
extern void FileDescriptor_get_Services_m111C66F60C4E1EB7FCE260D433F35058563F9E39 (void);
// 0x00000701 Google.Protobuf.Reflection.ExtensionCollection Google.Protobuf.Reflection.FileDescriptor::get_Extensions()
extern void FileDescriptor_get_Extensions_m3308E404E50DC055A4AF9EDAB142BA227CA7F4B2 (void);
// 0x00000702 System.Collections.Generic.IList`1<Google.Protobuf.Reflection.FileDescriptor> Google.Protobuf.Reflection.FileDescriptor::get_Dependencies()
extern void FileDescriptor_get_Dependencies_m9E9EB5F41B5489AE519540C7027903300E4F5955 (void);
// 0x00000703 System.Collections.Generic.IList`1<Google.Protobuf.Reflection.FileDescriptor> Google.Protobuf.Reflection.FileDescriptor::get_PublicDependencies()
extern void FileDescriptor_get_PublicDependencies_mAC7D2C8B65EA6260FD1E00427C865B073C26C20C (void);
// 0x00000704 System.String Google.Protobuf.Reflection.FileDescriptor::Google.Protobuf.Reflection.IDescriptor.get_FullName()
extern void FileDescriptor_Google_Protobuf_Reflection_IDescriptor_get_FullName_mD6B5C71C93A7AC4808B8F36F5205AC4343FB0285 (void);
// 0x00000705 Google.Protobuf.Reflection.FileDescriptor Google.Protobuf.Reflection.FileDescriptor::Google.Protobuf.Reflection.IDescriptor.get_File()
extern void FileDescriptor_Google_Protobuf_Reflection_IDescriptor_get_File_m7A7CCEEF3A0850D5E2197D05E6B7BD7FC271D76E (void);
// 0x00000706 Google.Protobuf.Reflection.DescriptorPool Google.Protobuf.Reflection.FileDescriptor::get_DescriptorPool()
extern void FileDescriptor_get_DescriptorPool_m39E958D13A60EBE7CC79BD0456664D61900040A3 (void);
// 0x00000707 Google.Protobuf.Reflection.FileDescriptor Google.Protobuf.Reflection.FileDescriptor::BuildFrom(Google.Protobuf.ByteString,Google.Protobuf.Reflection.FileDescriptorProto,Google.Protobuf.Reflection.FileDescriptor[],System.Boolean,Google.Protobuf.Reflection.GeneratedClrTypeInfo)
extern void FileDescriptor_BuildFrom_mCD34070738CA05D87DD536AB4107D914BCED5742 (void);
// 0x00000708 System.Void Google.Protobuf.Reflection.FileDescriptor::CrossLink()
extern void FileDescriptor_CrossLink_m8ECB5727793304E7F249AE2CE766443038C5C62A (void);
// 0x00000709 Google.Protobuf.Reflection.FileDescriptor Google.Protobuf.Reflection.FileDescriptor::FromGeneratedCode(System.Byte[],Google.Protobuf.Reflection.FileDescriptor[],Google.Protobuf.Reflection.GeneratedClrTypeInfo)
extern void FileDescriptor_FromGeneratedCode_m8D03D78677A8EEED4BFD41D18722EC65306A1E0C (void);
// 0x0000070A System.Collections.Generic.IEnumerable`1<Google.Protobuf.Extension> Google.Protobuf.Reflection.FileDescriptor::GetAllExtensions(Google.Protobuf.Reflection.FileDescriptor[],Google.Protobuf.Reflection.GeneratedClrTypeInfo)
extern void FileDescriptor_GetAllExtensions_mC4A06792ECB89A4169D169E0DB3230EE354D1525 (void);
// 0x0000070B System.Collections.Generic.IEnumerable`1<Google.Protobuf.Extension> Google.Protobuf.Reflection.FileDescriptor::GetAllGeneratedExtensions(Google.Protobuf.Reflection.GeneratedClrTypeInfo)
extern void FileDescriptor_GetAllGeneratedExtensions_mB3D508503C0A808F853E95B8E61BC6952AE7C1FB (void);
// 0x0000070C System.Collections.Generic.IEnumerable`1<Google.Protobuf.Extension> Google.Protobuf.Reflection.FileDescriptor::GetAllDependedExtensions(Google.Protobuf.Reflection.FileDescriptor)
extern void FileDescriptor_GetAllDependedExtensions_m2AF9D46708DFB5D083FB77F8833ADD6E49AAC53D (void);
// 0x0000070D System.Collections.Generic.IEnumerable`1<Google.Protobuf.Extension> Google.Protobuf.Reflection.FileDescriptor::GetAllDependedExtensionsFromMessage(Google.Protobuf.Reflection.MessageDescriptor)
extern void FileDescriptor_GetAllDependedExtensionsFromMessage_mAF3C1E48FE0C8B9A01DCE72D1F0027A816D11E83 (void);
// 0x0000070E System.String Google.Protobuf.Reflection.FileDescriptor::ToString()
extern void FileDescriptor_ToString_m298D74F239FB4E9968E5A4D143710A208FC19FC2 (void);
// 0x0000070F System.Void Google.Protobuf.Reflection.FileDescriptor::ForceReflectionInitialization()
// 0x00000710 System.Void Google.Protobuf.Reflection.FileDescriptor/<>c__DisplayClass2_0::.ctor()
extern void U3CU3Ec__DisplayClass2_0__ctor_m1BBBFEF67CF19FC3D913628B8267031E50EE7BD1 (void);
// 0x00000711 Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.Reflection.FileDescriptor/<>c__DisplayClass2_0::<.ctor>b__0(Google.Protobuf.Reflection.DescriptorProto,System.Int32)
extern void U3CU3Ec__DisplayClass2_0_U3C_ctorU3Eb__0_mF855F9915F2FDA29420683333DFCB44960768C73 (void);
// 0x00000712 Google.Protobuf.Reflection.EnumDescriptor Google.Protobuf.Reflection.FileDescriptor/<>c__DisplayClass2_0::<.ctor>b__1(Google.Protobuf.Reflection.EnumDescriptorProto,System.Int32)
extern void U3CU3Ec__DisplayClass2_0_U3C_ctorU3Eb__1_mAF485315B60EA9EC6EA2889CDC446CFEAE199D2C (void);
// 0x00000713 Google.Protobuf.Reflection.ServiceDescriptor Google.Protobuf.Reflection.FileDescriptor/<>c__DisplayClass2_0::<.ctor>b__2(Google.Protobuf.Reflection.ServiceDescriptorProto,System.Int32)
extern void U3CU3Ec__DisplayClass2_0_U3C_ctorU3Eb__2_m741146A6C9B950182E1D5DF31CF8438B179F334F (void);
// 0x00000714 System.Void Google.Protobuf.Reflection.FileDescriptor/<>c::.cctor()
extern void U3CU3Ec__cctor_mB9D86B4B471E23B201A8BA720BEB46C09DB837FA (void);
// 0x00000715 System.Void Google.Protobuf.Reflection.FileDescriptor/<>c::.ctor()
extern void U3CU3Ec__ctor_m514F00EF3119B63948DF8181712FACBC1781C4FF (void);
// 0x00000716 System.String Google.Protobuf.Reflection.FileDescriptor/<>c::<DeterminePublicDependencies>b__9_0(Google.Protobuf.Reflection.FileDescriptor)
extern void U3CU3Ec_U3CDeterminePublicDependenciesU3Eb__9_0_m2F98535BA4236CD9DDDF38E84AD5B067271D7C72 (void);
// 0x00000717 System.Boolean Google.Protobuf.Reflection.FileDescriptor/<>c::<GetAllGeneratedExtensions>b__53_0(Google.Protobuf.Reflection.GeneratedClrTypeInfo)
extern void U3CU3Ec_U3CGetAllGeneratedExtensionsU3Eb__53_0_m48DE89AAF572557D4B64DD307D20EC96AB9EF794 (void);
// 0x00000718 Google.Protobuf.Extension Google.Protobuf.Reflection.FileDescriptor/<>c::<GetAllDependedExtensions>b__54_0(Google.Protobuf.Reflection.FieldDescriptor)
extern void U3CU3Ec_U3CGetAllDependedExtensionsU3Eb__54_0_mDDE483E7D148A30C453B855CABB4C6FFF1D0432B (void);
// 0x00000719 System.Boolean Google.Protobuf.Reflection.FileDescriptor/<>c::<GetAllDependedExtensions>b__54_1(Google.Protobuf.Extension)
extern void U3CU3Ec_U3CGetAllDependedExtensionsU3Eb__54_1_m1895951D33B2B511E79771BC2BC40BE852896663 (void);
// 0x0000071A Google.Protobuf.Extension Google.Protobuf.Reflection.FileDescriptor/<>c::<GetAllDependedExtensionsFromMessage>b__55_0(Google.Protobuf.Reflection.FieldDescriptor)
extern void U3CU3Ec_U3CGetAllDependedExtensionsFromMessageU3Eb__55_0_m65C957D175FDD3779E86AC7E48FDBF9EBD29A899 (void);
// 0x0000071B System.Boolean Google.Protobuf.Reflection.FileDescriptor/<>c::<GetAllDependedExtensionsFromMessage>b__55_1(Google.Protobuf.Extension)
extern void U3CU3Ec_U3CGetAllDependedExtensionsFromMessageU3Eb__55_1_m1757252CFE8BD09C5192725451A9C30ED1CC1C85 (void);
// 0x0000071C System.Type Google.Protobuf.Reflection.GeneratedClrTypeInfo::get_ClrType()
extern void GeneratedClrTypeInfo_get_ClrType_mBC268EA87D91A720FC864656F282267E52E5C415 (void);
// 0x0000071D System.Void Google.Protobuf.Reflection.GeneratedClrTypeInfo::set_ClrType(System.Type)
extern void GeneratedClrTypeInfo_set_ClrType_m47923E24303E58B15765BA976560FF54060AA463 (void);
// 0x0000071E Google.Protobuf.MessageParser Google.Protobuf.Reflection.GeneratedClrTypeInfo::get_Parser()
extern void GeneratedClrTypeInfo_get_Parser_mE306A707C2E98793AE20F8F22D73682256A6825A (void);
// 0x0000071F System.String[] Google.Protobuf.Reflection.GeneratedClrTypeInfo::get_PropertyNames()
extern void GeneratedClrTypeInfo_get_PropertyNames_m9AC3AF88BE6E4401E8CBE61D2A27205D494C7537 (void);
// 0x00000720 Google.Protobuf.Extension[] Google.Protobuf.Reflection.GeneratedClrTypeInfo::get_Extensions()
extern void GeneratedClrTypeInfo_get_Extensions_mBC35C072C48C25BC9E3E10930E7ABC899E7D0BD9 (void);
// 0x00000721 System.String[] Google.Protobuf.Reflection.GeneratedClrTypeInfo::get_OneofNames()
extern void GeneratedClrTypeInfo_get_OneofNames_m8225DF12A9536F188BB8F84100E0026AD38398D3 (void);
// 0x00000722 Google.Protobuf.Reflection.GeneratedClrTypeInfo[] Google.Protobuf.Reflection.GeneratedClrTypeInfo::get_NestedTypes()
extern void GeneratedClrTypeInfo_get_NestedTypes_mC8CDA4ACD8F8B83388C64EFB5C63E8841E8B7767 (void);
// 0x00000723 System.Type[] Google.Protobuf.Reflection.GeneratedClrTypeInfo::get_NestedEnums()
extern void GeneratedClrTypeInfo_get_NestedEnums_m124D8277A92DC1AD640C130ABE855B6C9B55EB82 (void);
// 0x00000724 System.Void Google.Protobuf.Reflection.GeneratedClrTypeInfo::.ctor(System.Type,Google.Protobuf.MessageParser,System.String[],System.String[],System.Type[],Google.Protobuf.Extension[],Google.Protobuf.Reflection.GeneratedClrTypeInfo[])
extern void GeneratedClrTypeInfo__ctor_m87AFD6307EE56C2709AE4416F880230CE5ABB33D (void);
// 0x00000725 System.Void Google.Protobuf.Reflection.GeneratedClrTypeInfo::.ctor(System.Type[],Google.Protobuf.Extension[],Google.Protobuf.Reflection.GeneratedClrTypeInfo[])
extern void GeneratedClrTypeInfo__ctor_m8348FF9744C6AE72A6F4BE44BF26E1F64FE9CA0D (void);
// 0x00000726 System.Void Google.Protobuf.Reflection.GeneratedClrTypeInfo::.cctor()
extern void GeneratedClrTypeInfo__cctor_m7FBF9CA32832A453B8E2AEA077F3C68B9116B559 (void);
// 0x00000727 System.String Google.Protobuf.Reflection.IDescriptor::get_Name()
// 0x00000728 System.String Google.Protobuf.Reflection.IDescriptor::get_FullName()
// 0x00000729 Google.Protobuf.Reflection.FileDescriptor Google.Protobuf.Reflection.IDescriptor::get_File()
// 0x0000072A Google.Protobuf.Reflection.FieldDescriptor Google.Protobuf.Reflection.IFieldAccessor::get_Descriptor()
// 0x0000072B System.Void Google.Protobuf.Reflection.IFieldAccessor::Clear(Google.Protobuf.IMessage)
// 0x0000072C System.Object Google.Protobuf.Reflection.IFieldAccessor::GetValue(Google.Protobuf.IMessage)
// 0x0000072D System.Boolean Google.Protobuf.Reflection.IFieldAccessor::HasValue(Google.Protobuf.IMessage)
// 0x0000072E System.Void Google.Protobuf.Reflection.MapFieldAccessor::.ctor(System.Reflection.PropertyInfo,Google.Protobuf.Reflection.FieldDescriptor)
extern void MapFieldAccessor__ctor_m61136C9F4623BC5A3317ABCC64189464D185B668 (void);
// 0x0000072F System.Void Google.Protobuf.Reflection.MapFieldAccessor::Clear(Google.Protobuf.IMessage)
extern void MapFieldAccessor_Clear_m52A48E029A7C026DB49C871F5A2034B20D933971 (void);
// 0x00000730 System.Boolean Google.Protobuf.Reflection.MapFieldAccessor::HasValue(Google.Protobuf.IMessage)
extern void MapFieldAccessor_HasValue_m50488B3129E6ED46577A49DA005FCA89E9356FE7 (void);
// 0x00000731 System.Void Google.Protobuf.Reflection.MapFieldAccessor::SetValue(Google.Protobuf.IMessage,System.Object)
extern void MapFieldAccessor_SetValue_m8F5FF224C709F04189A2A702C0490D548E50CC91 (void);
// 0x00000732 System.Void Google.Protobuf.Reflection.MessageDescriptor::.ctor(Google.Protobuf.Reflection.DescriptorProto,Google.Protobuf.Reflection.FileDescriptor,Google.Protobuf.Reflection.MessageDescriptor,System.Int32,Google.Protobuf.Reflection.GeneratedClrTypeInfo)
extern void MessageDescriptor__ctor_m697D82B75EF84C2BFB0B3496E4969F100DE9EAA2 (void);
// 0x00000733 System.Collections.ObjectModel.ReadOnlyDictionary`2<System.String,Google.Protobuf.Reflection.FieldDescriptor> Google.Protobuf.Reflection.MessageDescriptor::CreateJsonFieldMap(System.Collections.Generic.IList`1<Google.Protobuf.Reflection.FieldDescriptor>)
extern void MessageDescriptor_CreateJsonFieldMap_m3BCA8184AD8B50F81DDA4258D37A4803D7016ED4 (void);
// 0x00000734 System.String Google.Protobuf.Reflection.MessageDescriptor::get_Name()
extern void MessageDescriptor_get_Name_mF83C7F30F48B7153D53ECEFDB5907712AFD7CDA8 (void);
// 0x00000735 System.Collections.Generic.IReadOnlyList`1<Google.Protobuf.Reflection.DescriptorBase> Google.Protobuf.Reflection.MessageDescriptor::GetNestedDescriptorListForField(System.Int32)
extern void MessageDescriptor_GetNestedDescriptorListForField_mE4C88416F59B1501F97D889CD095B65B043A0BE9 (void);
// 0x00000736 Google.Protobuf.Reflection.DescriptorProto Google.Protobuf.Reflection.MessageDescriptor::get_Proto()
extern void MessageDescriptor_get_Proto_mC19705FDFDE1B7189CE45B38771704217266D87C (void);
// 0x00000737 System.Type Google.Protobuf.Reflection.MessageDescriptor::get_ClrType()
extern void MessageDescriptor_get_ClrType_m67EDD051E54EF81C60E7C10479F1EAC25F222FFB (void);
// 0x00000738 Google.Protobuf.MessageParser Google.Protobuf.Reflection.MessageDescriptor::get_Parser()
extern void MessageDescriptor_get_Parser_mBA1FD49A96FD0E2AC88955EA6BCF536555E9B68C (void);
// 0x00000739 System.Boolean Google.Protobuf.Reflection.MessageDescriptor::get_IsWellKnownType()
extern void MessageDescriptor_get_IsWellKnownType_m098E92A49A8B04698C1147852A8FEE73837DB432 (void);
// 0x0000073A System.Boolean Google.Protobuf.Reflection.MessageDescriptor::get_IsWrapperType()
extern void MessageDescriptor_get_IsWrapperType_m4F0204430E2A7A742569380A4C0D7647337020B1 (void);
// 0x0000073B Google.Protobuf.Reflection.MessageDescriptor/FieldCollection Google.Protobuf.Reflection.MessageDescriptor::get_Fields()
extern void MessageDescriptor_get_Fields_m7EC5DF1D21CA98B0E798ED2171C276DEDAC39352 (void);
// 0x0000073C Google.Protobuf.Reflection.ExtensionCollection Google.Protobuf.Reflection.MessageDescriptor::get_Extensions()
extern void MessageDescriptor_get_Extensions_mEE45D3BA082A29D28E2390D85156DD1E9F579155 (void);
// 0x0000073D System.Collections.Generic.IList`1<Google.Protobuf.Reflection.MessageDescriptor> Google.Protobuf.Reflection.MessageDescriptor::get_NestedTypes()
extern void MessageDescriptor_get_NestedTypes_m34A524E3C7D560C1537685CF6291AB60DA58FE7E (void);
// 0x0000073E System.Collections.Generic.IList`1<Google.Protobuf.Reflection.EnumDescriptor> Google.Protobuf.Reflection.MessageDescriptor::get_EnumTypes()
extern void MessageDescriptor_get_EnumTypes_m663896B42139FAB5706A6408FA931F95A60C3F39 (void);
// 0x0000073F System.Collections.Generic.IList`1<Google.Protobuf.Reflection.OneofDescriptor> Google.Protobuf.Reflection.MessageDescriptor::get_Oneofs()
extern void MessageDescriptor_get_Oneofs_mC26EB7828846AC0A04AB6F892C4D22E79CD1E364 (void);
// 0x00000740 Google.Protobuf.Reflection.FieldDescriptor Google.Protobuf.Reflection.MessageDescriptor::FindFieldByNumber(System.Int32)
extern void MessageDescriptor_FindFieldByNumber_m1E924845EDE30B6290C3D286A5321A48A7162CA7 (void);
// 0x00000741 System.Void Google.Protobuf.Reflection.MessageDescriptor::CrossLink()
extern void MessageDescriptor_CrossLink_m730D7BEC4896228D0EE1C8342BBFC2C39C5C3CCE (void);
// 0x00000742 System.Void Google.Protobuf.Reflection.MessageDescriptor::.cctor()
extern void MessageDescriptor__cctor_mA6B7A9F65008C855243566808C3A798EB574F67A (void);
// 0x00000743 System.Void Google.Protobuf.Reflection.MessageDescriptor/FieldCollection::.ctor(Google.Protobuf.Reflection.MessageDescriptor)
extern void FieldCollection__ctor_m635BF532C669D474F0DBCEE3C341D1EBB85A8575 (void);
// 0x00000744 System.Collections.Generic.IList`1<Google.Protobuf.Reflection.FieldDescriptor> Google.Protobuf.Reflection.MessageDescriptor/FieldCollection::InDeclarationOrder()
extern void FieldCollection_InDeclarationOrder_m8973915B3DDF9F261F5DD019B1F96365D9D1E2A8 (void);
// 0x00000745 System.Collections.Generic.IList`1<Google.Protobuf.Reflection.FieldDescriptor> Google.Protobuf.Reflection.MessageDescriptor/FieldCollection::InFieldNumberOrder()
extern void FieldCollection_InFieldNumberOrder_m524EBA77C464F653AAA2966BE3074D779452EF9F (void);
// 0x00000746 Google.Protobuf.Reflection.FieldDescriptor Google.Protobuf.Reflection.MessageDescriptor/FieldCollection::get_Item(System.Int32)
extern void FieldCollection_get_Item_m72E66500B2AE74CF46AF26C874E489EC7C90DC5D (void);
// 0x00000747 System.Void Google.Protobuf.Reflection.MessageDescriptor/<>c__DisplayClass5_0::.ctor()
extern void U3CU3Ec__DisplayClass5_0__ctor_mD5B8E9D6A516E2B443B7E3706D42B8F49DB59C9E (void);
// 0x00000748 Google.Protobuf.Reflection.OneofDescriptor Google.Protobuf.Reflection.MessageDescriptor/<>c__DisplayClass5_0::<.ctor>b__0(Google.Protobuf.Reflection.OneofDescriptorProto,System.Int32)
extern void U3CU3Ec__DisplayClass5_0_U3C_ctorU3Eb__0_m7F7296467B42A2D888BF6E956523A2CCAA0DB7E9 (void);
// 0x00000749 Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.Reflection.MessageDescriptor/<>c__DisplayClass5_0::<.ctor>b__1(Google.Protobuf.Reflection.DescriptorProto,System.Int32)
extern void U3CU3Ec__DisplayClass5_0_U3C_ctorU3Eb__1_mDCD04AA0B2B183BFE77262DE788E2E41A9E7B976 (void);
// 0x0000074A Google.Protobuf.Reflection.EnumDescriptor Google.Protobuf.Reflection.MessageDescriptor/<>c__DisplayClass5_0::<.ctor>b__2(Google.Protobuf.Reflection.EnumDescriptorProto,System.Int32)
extern void U3CU3Ec__DisplayClass5_0_U3C_ctorU3Eb__2_mC76375B9A8C8ACD417F8D3D12B0938EF31D3356B (void);
// 0x0000074B Google.Protobuf.Reflection.FieldDescriptor Google.Protobuf.Reflection.MessageDescriptor/<>c__DisplayClass5_0::<.ctor>b__3(Google.Protobuf.Reflection.FieldDescriptorProto,System.Int32)
extern void U3CU3Ec__DisplayClass5_0_U3C_ctorU3Eb__3_m8C77FD6177F520BAF3853864945C7605A3E204FD (void);
// 0x0000074C System.Void Google.Protobuf.Reflection.MessageDescriptor/<>c::.cctor()
extern void U3CU3Ec__cctor_m7927D07E530D7A18F9955C58168BE142DB3D17C8 (void);
// 0x0000074D System.Void Google.Protobuf.Reflection.MessageDescriptor/<>c::.ctor()
extern void U3CU3Ec__ctor_m656123E4D759619026DC9C0A1C31EB10D5D92771 (void);
// 0x0000074E System.Int32 Google.Protobuf.Reflection.MessageDescriptor/<>c::<.ctor>b__5_4(Google.Protobuf.Reflection.FieldDescriptor)
extern void U3CU3Ec_U3C_ctorU3Eb__5_4_mE110F0F6FFC9418FA99CD3FEB029514ABD0A7CEB (void);
// 0x0000074F System.Void Google.Protobuf.Reflection.MethodDescriptor::.ctor(Google.Protobuf.Reflection.MethodDescriptorProto,Google.Protobuf.Reflection.FileDescriptor,Google.Protobuf.Reflection.ServiceDescriptor,System.Int32)
extern void MethodDescriptor__ctor_m132CEA4CAD2205D2A73DA0AFDB62ECB48D3DD721 (void);
// 0x00000750 Google.Protobuf.Reflection.MethodDescriptorProto Google.Protobuf.Reflection.MethodDescriptor::get_Proto()
extern void MethodDescriptor_get_Proto_m85B3E65F7AB108C8BF212BA6B373614DFEF5B7EA (void);
// 0x00000751 System.String Google.Protobuf.Reflection.MethodDescriptor::get_Name()
extern void MethodDescriptor_get_Name_m51107131036CE66EB661A0575C507E2F75A916BF (void);
// 0x00000752 System.Void Google.Protobuf.Reflection.MethodDescriptor::CrossLink()
extern void MethodDescriptor_CrossLink_m1C07F242D0EEC94C7754FB8A8D0BCB15D70D7173 (void);
// 0x00000753 System.Void Google.Protobuf.Reflection.OneofAccessor::.ctor(Google.Protobuf.Reflection.OneofDescriptor,System.Func`2<Google.Protobuf.IMessage,System.Int32>,System.Action`1<Google.Protobuf.IMessage>)
extern void OneofAccessor__ctor_m04638176ACFC67393E75EE9B90815DF94B1BCB4F (void);
// 0x00000754 Google.Protobuf.Reflection.OneofAccessor Google.Protobuf.Reflection.OneofAccessor::ForRegularOneof(Google.Protobuf.Reflection.OneofDescriptor,System.Reflection.PropertyInfo,System.Reflection.MethodInfo)
extern void OneofAccessor_ForRegularOneof_m077D8097F0102527660A43A5150DDF02786998D0 (void);
// 0x00000755 Google.Protobuf.Reflection.OneofAccessor Google.Protobuf.Reflection.OneofAccessor::ForSyntheticOneof(Google.Protobuf.Reflection.OneofDescriptor)
extern void OneofAccessor_ForSyntheticOneof_m97A860E2AB9B35D8ADAFBE180D755833D2482650 (void);
// 0x00000756 Google.Protobuf.Reflection.OneofDescriptor Google.Protobuf.Reflection.OneofAccessor::get_Descriptor()
extern void OneofAccessor_get_Descriptor_mBDAF0F18B7C8EBBC4C2CF19A267C0AE3F09785C5 (void);
// 0x00000757 System.Void Google.Protobuf.Reflection.OneofAccessor::Clear(Google.Protobuf.IMessage)
extern void OneofAccessor_Clear_mB06FD88CAB211F03CCA2F11D5449C1BD8B4385D1 (void);
// 0x00000758 Google.Protobuf.Reflection.FieldDescriptor Google.Protobuf.Reflection.OneofAccessor::GetCaseFieldDescriptor(Google.Protobuf.IMessage)
extern void OneofAccessor_GetCaseFieldDescriptor_mD29158042A855E4F866DF7F40864DCDDB499A6AB (void);
// 0x00000759 System.Void Google.Protobuf.Reflection.OneofAccessor/<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_m54A4FD9D7E8331F3560233E3633FD4C55502C4E3 (void);
// 0x0000075A System.Int32 Google.Protobuf.Reflection.OneofAccessor/<>c__DisplayClass4_0::<ForSyntheticOneof>b__0(Google.Protobuf.IMessage)
extern void U3CU3Ec__DisplayClass4_0_U3CForSyntheticOneofU3Eb__0_m8464C36B95B2F3EEB07E1C9BAAD27CE57C81D22B (void);
// 0x0000075B System.Void Google.Protobuf.Reflection.OneofAccessor/<>c__DisplayClass4_0::<ForSyntheticOneof>b__1(Google.Protobuf.IMessage)
extern void U3CU3Ec__DisplayClass4_0_U3CForSyntheticOneofU3Eb__1_mE617B86AD9732907F68C010E8EF97189B415E9DF (void);
// 0x0000075C System.Void Google.Protobuf.Reflection.OneofDescriptor::.ctor(Google.Protobuf.Reflection.OneofDescriptorProto,Google.Protobuf.Reflection.FileDescriptor,Google.Protobuf.Reflection.MessageDescriptor,System.Int32,System.String)
extern void OneofDescriptor__ctor_mE372C893E379410EDD55F3F7938EFCEB9033E18C (void);
// 0x0000075D System.String Google.Protobuf.Reflection.OneofDescriptor::get_Name()
extern void OneofDescriptor_get_Name_m43F733262646E17BC195D6E953894198F6F6ABFF (void);
// 0x0000075E Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.Reflection.OneofDescriptor::get_ContainingType()
extern void OneofDescriptor_get_ContainingType_m40130FC0260EE9E246C5181B36F9334DE4D16D58 (void);
// 0x0000075F System.Collections.Generic.IList`1<Google.Protobuf.Reflection.FieldDescriptor> Google.Protobuf.Reflection.OneofDescriptor::get_Fields()
extern void OneofDescriptor_get_Fields_mC0948EC448A32E6CFA1B80878E4D859689BCB2E2 (void);
// 0x00000760 System.Boolean Google.Protobuf.Reflection.OneofDescriptor::get_IsSynthetic()
extern void OneofDescriptor_get_IsSynthetic_m22DC2931DF30D51ABB96C336D1F8A89F37898D17 (void);
// 0x00000761 Google.Protobuf.Reflection.OneofAccessor Google.Protobuf.Reflection.OneofDescriptor::get_Accessor()
extern void OneofDescriptor_get_Accessor_m733D8CEEB45918E4CB9DA558BA04BE5B4040C308 (void);
// 0x00000762 System.Void Google.Protobuf.Reflection.OneofDescriptor::CrossLink()
extern void OneofDescriptor_CrossLink_m13EB3F795A61006620EEB3BC83886F6C41F094D8 (void);
// 0x00000763 Google.Protobuf.Reflection.OneofAccessor Google.Protobuf.Reflection.OneofDescriptor::CreateAccessor(System.String)
extern void OneofDescriptor_CreateAccessor_mEE00EA091CED277D753F95233CC66CC178DAC50A (void);
// 0x00000764 System.Void Google.Protobuf.Reflection.OneofDescriptor/<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_mC4C62E7CBD32174E07B91502F2E90039AA2C2458 (void);
// 0x00000765 System.Boolean Google.Protobuf.Reflection.OneofDescriptor/<>c__DisplayClass4_0::<.ctor>b__0(Google.Protobuf.Reflection.FieldDescriptorProto)
extern void U3CU3Ec__DisplayClass4_0_U3C_ctorU3Eb__0_mE51D4E8DD73DCEE3E47EDF2C7532EFFD88E5E1E4 (void);
// 0x00000766 System.String Google.Protobuf.Reflection.OriginalNameAttribute::get_Name()
extern void OriginalNameAttribute_get_Name_m35E068ED514B51E172CF5FE5DD917801F045B819 (void);
// 0x00000767 System.Void Google.Protobuf.Reflection.OriginalNameAttribute::set_Name(System.String)
extern void OriginalNameAttribute_set_Name_mC0A333EF33378D9143B3077B4E86270FE05636DA (void);
// 0x00000768 System.Boolean Google.Protobuf.Reflection.OriginalNameAttribute::get_PreferredAlias()
extern void OriginalNameAttribute_get_PreferredAlias_m8B1D004B47D36373572F40238135A599E23095B8 (void);
// 0x00000769 System.Void Google.Protobuf.Reflection.OriginalNameAttribute::set_PreferredAlias(System.Boolean)
extern void OriginalNameAttribute_set_PreferredAlias_m5717AA7D6B145D079B78DE15B31B88C8D6B4C258 (void);
// 0x0000076A System.Void Google.Protobuf.Reflection.OriginalNameAttribute::.ctor(System.String)
extern void OriginalNameAttribute__ctor_mB0C6C38C49B9261D2DC423D692E80B8EBC657BF9 (void);
// 0x0000076B System.Void Google.Protobuf.Reflection.PackageDescriptor::.ctor(System.String,System.String,Google.Protobuf.Reflection.FileDescriptor)
extern void PackageDescriptor__ctor_m894073A12D24F6326EEAAD2B410485ADBAA0E6E6 (void);
// 0x0000076C System.String Google.Protobuf.Reflection.PackageDescriptor::get_Name()
extern void PackageDescriptor_get_Name_m0F6A6231BEC61A25B1974D9B34DAD9E96FE966EA (void);
// 0x0000076D System.String Google.Protobuf.Reflection.PackageDescriptor::get_FullName()
extern void PackageDescriptor_get_FullName_m567C159C73D1ACFC2026E89DDB985443136D2A28 (void);
// 0x0000076E Google.Protobuf.Reflection.FileDescriptor Google.Protobuf.Reflection.PackageDescriptor::get_File()
extern void PackageDescriptor_get_File_m29E90BB5391086BE17A857FCC9FE9B795185FD12 (void);
// 0x0000076F System.Void Google.Protobuf.Reflection.ReflectionUtil::.cctor()
extern void ReflectionUtil__cctor_m0BEBC8EADA0FDC0B5BFD115604E20C6A235A00A2 (void);
// 0x00000770 System.Void Google.Protobuf.Reflection.ReflectionUtil::ForceInitialize()
// 0x00000771 System.Func`2<Google.Protobuf.IMessage,System.Object> Google.Protobuf.Reflection.ReflectionUtil::CreateFuncIMessageObject(System.Reflection.MethodInfo)
extern void ReflectionUtil_CreateFuncIMessageObject_m7EB7E65C8F968EFC76315D476F6CABE4DED75C36 (void);
// 0x00000772 System.Func`2<Google.Protobuf.IMessage,System.Int32> Google.Protobuf.Reflection.ReflectionUtil::CreateFuncIMessageInt32(System.Reflection.MethodInfo)
extern void ReflectionUtil_CreateFuncIMessageInt32_mEB9452DEEB7DE855185B7D73D741A997FE77CE83 (void);
// 0x00000773 System.Action`2<Google.Protobuf.IMessage,System.Object> Google.Protobuf.Reflection.ReflectionUtil::CreateActionIMessageObject(System.Reflection.MethodInfo)
extern void ReflectionUtil_CreateActionIMessageObject_mB18243688BDE03EDD030D7237CA2062DE1826F4B (void);
// 0x00000774 System.Action`1<Google.Protobuf.IMessage> Google.Protobuf.Reflection.ReflectionUtil::CreateActionIMessage(System.Reflection.MethodInfo)
extern void ReflectionUtil_CreateActionIMessage_mA298FA5EF2B7C1182C43A6411C245705A4D74341 (void);
// 0x00000775 System.Func`2<Google.Protobuf.IMessage,System.Boolean> Google.Protobuf.Reflection.ReflectionUtil::CreateFuncIMessageBool(System.Reflection.MethodInfo)
extern void ReflectionUtil_CreateFuncIMessageBool_m4A9ED81E058059190B75C43C95DC86F52317C2FB (void);
// 0x00000776 Google.Protobuf.Reflection.ReflectionUtil/IExtensionReflectionHelper Google.Protobuf.Reflection.ReflectionUtil::CreateExtensionHelper(Google.Protobuf.Extension)
extern void ReflectionUtil_CreateExtensionHelper_m326120533AE4AE52669353D223874610B37A53C4 (void);
// 0x00000777 Google.Protobuf.Reflection.ReflectionUtil/IReflectionHelper Google.Protobuf.Reflection.ReflectionUtil::GetReflectionHelper(System.Type,System.Type)
extern void ReflectionUtil_GetReflectionHelper_m92A3653AB63EEB04F467485B59F7E4DCA1E6C6DF (void);
// 0x00000778 System.Boolean Google.Protobuf.Reflection.ReflectionUtil::get_CanConvertEnumFuncToInt32Func()
extern void ReflectionUtil_get_CanConvertEnumFuncToInt32Func_m14B4A020233464D727830248EED426EBDE5D45EF (void);
// 0x00000779 System.Boolean Google.Protobuf.Reflection.ReflectionUtil::CheckCanConvertEnumFuncToInt32Func()
extern void ReflectionUtil_CheckCanConvertEnumFuncToInt32Func_mE6C18ECB39982549C3500A8DD65603E20177B7CF (void);
// 0x0000077A Google.Protobuf.Reflection.ReflectionUtil/SampleEnum Google.Protobuf.Reflection.ReflectionUtil::SampleEnumMethod()
extern void ReflectionUtil_SampleEnumMethod_m3A940A9C50EEAB34B229B0A4E3BE3A3375E40A1B (void);
// 0x0000077B System.Func`2<Google.Protobuf.IMessage,System.Int32> Google.Protobuf.Reflection.ReflectionUtil/IReflectionHelper::CreateFuncIMessageInt32(System.Reflection.MethodInfo)
// 0x0000077C System.Action`1<Google.Protobuf.IMessage> Google.Protobuf.Reflection.ReflectionUtil/IReflectionHelper::CreateActionIMessage(System.Reflection.MethodInfo)
// 0x0000077D System.Func`2<Google.Protobuf.IMessage,System.Object> Google.Protobuf.Reflection.ReflectionUtil/IReflectionHelper::CreateFuncIMessageObject(System.Reflection.MethodInfo)
// 0x0000077E System.Action`2<Google.Protobuf.IMessage,System.Object> Google.Protobuf.Reflection.ReflectionUtil/IReflectionHelper::CreateActionIMessageObject(System.Reflection.MethodInfo)
// 0x0000077F System.Func`2<Google.Protobuf.IMessage,System.Boolean> Google.Protobuf.Reflection.ReflectionUtil/IReflectionHelper::CreateFuncIMessageBool(System.Reflection.MethodInfo)
// 0x00000780 System.Object Google.Protobuf.Reflection.ReflectionUtil/IExtensionReflectionHelper::GetExtension(Google.Protobuf.IMessage)
// 0x00000781 System.Boolean Google.Protobuf.Reflection.ReflectionUtil/IExtensionReflectionHelper::HasExtension(Google.Protobuf.IMessage)
// 0x00000782 System.Void Google.Protobuf.Reflection.ReflectionUtil/IExtensionReflectionHelper::ClearExtension(Google.Protobuf.IMessage)
// 0x00000783 System.Func`2<Google.Protobuf.IMessage,System.Int32> Google.Protobuf.Reflection.ReflectionUtil/ReflectionHelper`2::CreateFuncIMessageInt32(System.Reflection.MethodInfo)
// 0x00000784 System.Action`1<Google.Protobuf.IMessage> Google.Protobuf.Reflection.ReflectionUtil/ReflectionHelper`2::CreateActionIMessage(System.Reflection.MethodInfo)
// 0x00000785 System.Func`2<Google.Protobuf.IMessage,System.Object> Google.Protobuf.Reflection.ReflectionUtil/ReflectionHelper`2::CreateFuncIMessageObject(System.Reflection.MethodInfo)
// 0x00000786 System.Action`2<Google.Protobuf.IMessage,System.Object> Google.Protobuf.Reflection.ReflectionUtil/ReflectionHelper`2::CreateActionIMessageObject(System.Reflection.MethodInfo)
// 0x00000787 System.Func`2<Google.Protobuf.IMessage,System.Boolean> Google.Protobuf.Reflection.ReflectionUtil/ReflectionHelper`2::CreateFuncIMessageBool(System.Reflection.MethodInfo)
// 0x00000788 System.Void Google.Protobuf.Reflection.ReflectionUtil/ReflectionHelper`2::.ctor()
// 0x00000789 System.Void Google.Protobuf.Reflection.ReflectionUtil/ReflectionHelper`2/<>c__DisplayClass0_0::.ctor()
// 0x0000078A System.Int32 Google.Protobuf.Reflection.ReflectionUtil/ReflectionHelper`2/<>c__DisplayClass0_0::<CreateFuncIMessageInt32>b__0(Google.Protobuf.IMessage)
// 0x0000078B System.Void Google.Protobuf.Reflection.ReflectionUtil/ReflectionHelper`2/<>c__DisplayClass0_1::.ctor()
// 0x0000078C System.Int32 Google.Protobuf.Reflection.ReflectionUtil/ReflectionHelper`2/<>c__DisplayClass0_1::<CreateFuncIMessageInt32>b__1(Google.Protobuf.IMessage)
// 0x0000078D System.Void Google.Protobuf.Reflection.ReflectionUtil/ReflectionHelper`2/<>c__DisplayClass1_0::.ctor()
// 0x0000078E System.Void Google.Protobuf.Reflection.ReflectionUtil/ReflectionHelper`2/<>c__DisplayClass1_0::<CreateActionIMessage>b__0(Google.Protobuf.IMessage)
// 0x0000078F System.Void Google.Protobuf.Reflection.ReflectionUtil/ReflectionHelper`2/<>c__DisplayClass2_0::.ctor()
// 0x00000790 System.Object Google.Protobuf.Reflection.ReflectionUtil/ReflectionHelper`2/<>c__DisplayClass2_0::<CreateFuncIMessageObject>b__0(Google.Protobuf.IMessage)
// 0x00000791 System.Void Google.Protobuf.Reflection.ReflectionUtil/ReflectionHelper`2/<>c__DisplayClass3_0::.ctor()
// 0x00000792 System.Void Google.Protobuf.Reflection.ReflectionUtil/ReflectionHelper`2/<>c__DisplayClass3_0::<CreateActionIMessageObject>b__0(Google.Protobuf.IMessage,System.Object)
// 0x00000793 System.Void Google.Protobuf.Reflection.ReflectionUtil/ReflectionHelper`2/<>c__DisplayClass4_0::.ctor()
// 0x00000794 System.Boolean Google.Protobuf.Reflection.ReflectionUtil/ReflectionHelper`2/<>c__DisplayClass4_0::<CreateFuncIMessageBool>b__0(Google.Protobuf.IMessage)
// 0x00000795 System.Void Google.Protobuf.Reflection.ReflectionUtil/ExtensionReflectionHelper`2::.ctor(Google.Protobuf.Extension)
// 0x00000796 System.Object Google.Protobuf.Reflection.ReflectionUtil/ExtensionReflectionHelper`2::GetExtension(Google.Protobuf.IMessage)
// 0x00000797 System.Boolean Google.Protobuf.Reflection.ReflectionUtil/ExtensionReflectionHelper`2::HasExtension(Google.Protobuf.IMessage)
// 0x00000798 System.Void Google.Protobuf.Reflection.ReflectionUtil/ExtensionReflectionHelper`2::ClearExtension(Google.Protobuf.IMessage)
// 0x00000799 System.Void Google.Protobuf.Reflection.RepeatedFieldAccessor::.ctor(System.Reflection.PropertyInfo,Google.Protobuf.Reflection.FieldDescriptor)
extern void RepeatedFieldAccessor__ctor_m3811007EB321C48D609E209F0356C5EED92EE31D (void);
// 0x0000079A System.Void Google.Protobuf.Reflection.RepeatedFieldAccessor::Clear(Google.Protobuf.IMessage)
extern void RepeatedFieldAccessor_Clear_m4B471923A3DD82F2F63F0C2B265B4D3DD481E5B1 (void);
// 0x0000079B System.Boolean Google.Protobuf.Reflection.RepeatedFieldAccessor::HasValue(Google.Protobuf.IMessage)
extern void RepeatedFieldAccessor_HasValue_m51C2E466833B840D288A62390BF36EA7CB83F51A (void);
// 0x0000079C System.Void Google.Protobuf.Reflection.RepeatedFieldAccessor::SetValue(Google.Protobuf.IMessage,System.Object)
extern void RepeatedFieldAccessor_SetValue_mB77B2AEBEBB96A4FFB4A2840FFAD6E97F0EE8C11 (void);
// 0x0000079D System.Void Google.Protobuf.Reflection.ServiceDescriptor::.ctor(Google.Protobuf.Reflection.ServiceDescriptorProto,Google.Protobuf.Reflection.FileDescriptor,System.Int32)
extern void ServiceDescriptor__ctor_m1E35DA06921850E3F74D47AC2A2EAB94A5C0A49B (void);
// 0x0000079E System.String Google.Protobuf.Reflection.ServiceDescriptor::get_Name()
extern void ServiceDescriptor_get_Name_mA5DB087BFBB418946B706E72C018817BFA1A21A3 (void);
// 0x0000079F System.Collections.Generic.IReadOnlyList`1<Google.Protobuf.Reflection.DescriptorBase> Google.Protobuf.Reflection.ServiceDescriptor::GetNestedDescriptorListForField(System.Int32)
extern void ServiceDescriptor_GetNestedDescriptorListForField_m090316EBEC0E64AFF243EFAB68991305AA1A6557 (void);
// 0x000007A0 System.Void Google.Protobuf.Reflection.ServiceDescriptor::CrossLink()
extern void ServiceDescriptor_CrossLink_m09200D1B7D2C5D5D7CBCC95F7F4A7A340B552BAC (void);
// 0x000007A1 System.Void Google.Protobuf.Reflection.ServiceDescriptor/<>c__DisplayClass2_0::.ctor()
extern void U3CU3Ec__DisplayClass2_0__ctor_mA57CD15AFF22F93543EB4483302868520307C699 (void);
// 0x000007A2 Google.Protobuf.Reflection.MethodDescriptor Google.Protobuf.Reflection.ServiceDescriptor/<>c__DisplayClass2_0::<.ctor>b__0(Google.Protobuf.Reflection.MethodDescriptorProto,System.Int32)
extern void U3CU3Ec__DisplayClass2_0_U3C_ctorU3Eb__0_m8B1C23CBA2E35B1EBF430937E0688DA3344E0523 (void);
// 0x000007A3 System.Void Google.Protobuf.Reflection.SingleFieldAccessor::.ctor(System.Reflection.PropertyInfo,Google.Protobuf.Reflection.FieldDescriptor)
extern void SingleFieldAccessor__ctor_m6EF8CEFCEF1A3AE8291868B6AA8D65C1487C0029 (void);
// 0x000007A4 System.Void Google.Protobuf.Reflection.SingleFieldAccessor::Clear(Google.Protobuf.IMessage)
extern void SingleFieldAccessor_Clear_mE6BB9FF9FBB353A7935FCC1027FE3C8BD0FA61AB (void);
// 0x000007A5 System.Boolean Google.Protobuf.Reflection.SingleFieldAccessor::HasValue(Google.Protobuf.IMessage)
extern void SingleFieldAccessor_HasValue_m3140954CBB9A3FC88D46ED762AB4870AC8A51C05 (void);
// 0x000007A6 System.Void Google.Protobuf.Reflection.SingleFieldAccessor::SetValue(Google.Protobuf.IMessage,System.Object)
extern void SingleFieldAccessor_SetValue_m1D4ABBA8146D08B8122B1C60616B93651D763455 (void);
// 0x000007A7 System.Void Google.Protobuf.Reflection.SingleFieldAccessor/<>c__DisplayClass3_0::.ctor()
extern void U3CU3Ec__DisplayClass3_0__ctor_mDC1F9AC11E1EC64AFE7BC876EA7437E050D0485A (void);
// 0x000007A8 System.Boolean Google.Protobuf.Reflection.SingleFieldAccessor/<>c__DisplayClass3_0::<.ctor>b__0(Google.Protobuf.IMessage)
extern void U3CU3Ec__DisplayClass3_0_U3C_ctorU3Eb__0_m4D68499F01F2D65C8F3BC4979D1FEA22C5951F70 (void);
// 0x000007A9 System.Void Google.Protobuf.Reflection.SingleFieldAccessor/<>c__DisplayClass3_0::<.ctor>b__1(Google.Protobuf.IMessage)
extern void U3CU3Ec__DisplayClass3_0_U3C_ctorU3Eb__1_m012D25B971005EF2116E2694C55B4F456E202517 (void);
// 0x000007AA System.Void Google.Protobuf.Reflection.SingleFieldAccessor/<>c__DisplayClass3_1::.ctor()
extern void U3CU3Ec__DisplayClass3_1__ctor_m4A4866967E04FE04C8939565057C67AF9240DF90 (void);
// 0x000007AB System.Boolean Google.Protobuf.Reflection.SingleFieldAccessor/<>c__DisplayClass3_1::<.ctor>b__2(Google.Protobuf.IMessage)
extern void U3CU3Ec__DisplayClass3_1_U3C_ctorU3Eb__2_m9BE17FECA2DF3B7BD94067194F4F625C729441A1 (void);
// 0x000007AC System.Void Google.Protobuf.Reflection.SingleFieldAccessor/<>c__DisplayClass3_1::<.ctor>b__3(Google.Protobuf.IMessage)
extern void U3CU3Ec__DisplayClass3_1_U3C_ctorU3Eb__3_m8428E2627162B464F374C62DA9929B4441D56C30 (void);
// 0x000007AD System.Void Google.Protobuf.Reflection.SingleFieldAccessor/<>c__DisplayClass3_2::.ctor()
extern void U3CU3Ec__DisplayClass3_2__ctor_m223CDF862764DE5D298ECEC01BB24B61AC86A3F8 (void);
// 0x000007AE System.Void Google.Protobuf.Reflection.SingleFieldAccessor/<>c__DisplayClass3_2::<.ctor>b__5(Google.Protobuf.IMessage)
extern void U3CU3Ec__DisplayClass3_2_U3C_ctorU3Eb__5_m5DC9C0EA88D7E40F6E6B5A05C5DD0D56BA9A0B0B (void);
// 0x000007AF System.Void Google.Protobuf.Reflection.SingleFieldAccessor/<>c::.cctor()
extern void U3CU3Ec__cctor_m180CF70E3704C30ADA4200909331204FF0AB4FF1 (void);
// 0x000007B0 System.Void Google.Protobuf.Reflection.SingleFieldAccessor/<>c::.ctor()
extern void U3CU3Ec__ctor_m7041048761CB9F36498D056F57D33017D3F10A01 (void);
// 0x000007B1 System.Boolean Google.Protobuf.Reflection.SingleFieldAccessor/<>c::<.ctor>b__3_4(Google.Protobuf.IMessage)
extern void U3CU3Ec_U3C_ctorU3Eb__3_4_mB4EA619F55628AC055F7D2A4372316C6438523C1 (void);
// 0x000007B2 Google.Protobuf.Reflection.TypeRegistry Google.Protobuf.Reflection.TypeRegistry::get_Empty()
extern void TypeRegistry_get_Empty_mE5DF84BF4F8096E4E4DEC88EB1B60542256F54C9 (void);
// 0x000007B3 System.Void Google.Protobuf.Reflection.TypeRegistry::.ctor(System.Collections.Generic.Dictionary`2<System.String,Google.Protobuf.Reflection.MessageDescriptor>)
extern void TypeRegistry__ctor_m015B123424761E68E7F35E8C70AD3B1702B532EB (void);
// 0x000007B4 Google.Protobuf.Reflection.MessageDescriptor Google.Protobuf.Reflection.TypeRegistry::Find(System.String)
extern void TypeRegistry_Find_m4DBDC19FDD721BEAF8C9BC5223871AF618A067E0 (void);
// 0x000007B5 System.Void Google.Protobuf.Reflection.TypeRegistry::.cctor()
extern void TypeRegistry__cctor_mECDF4DAF9F427B14CBD2F7E37E07CB4243B0A3AF (void);
// 0x000007B6 System.Boolean Google.Protobuf.Collections.Lists::Equals(System.Collections.Generic.List`1<T>,System.Collections.Generic.List`1<T>)
// 0x000007B7 System.Int32 Google.Protobuf.Collections.Lists::GetHashCode(System.Collections.Generic.List`1<T>)
// 0x000007B8 Google.Protobuf.Collections.MapField`2<TKey,TValue> Google.Protobuf.Collections.MapField`2::Clone()
// 0x000007B9 System.Void Google.Protobuf.Collections.MapField`2::Add(TKey,TValue)
// 0x000007BA System.Boolean Google.Protobuf.Collections.MapField`2::ContainsKey(TKey)
// 0x000007BB System.Boolean Google.Protobuf.Collections.MapField`2::ContainsValue(TValue)
// 0x000007BC System.Boolean Google.Protobuf.Collections.MapField`2::Remove(TKey)
// 0x000007BD System.Boolean Google.Protobuf.Collections.MapField`2::TryGetValue(TKey,TValue&)
// 0x000007BE TValue Google.Protobuf.Collections.MapField`2::get_Item(TKey)
// 0x000007BF System.Void Google.Protobuf.Collections.MapField`2::set_Item(TKey,TValue)
// 0x000007C0 System.Collections.Generic.ICollection`1<TValue> Google.Protobuf.Collections.MapField`2::get_Values()
// 0x000007C1 System.Void Google.Protobuf.Collections.MapField`2::Add(System.Collections.Generic.IDictionary`2<TKey,TValue>)
// 0x000007C2 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> Google.Protobuf.Collections.MapField`2::GetEnumerator()
// 0x000007C3 System.Collections.IEnumerator Google.Protobuf.Collections.MapField`2::System.Collections.IEnumerable.GetEnumerator()
// 0x000007C4 System.Void Google.Protobuf.Collections.MapField`2::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
// 0x000007C5 System.Void Google.Protobuf.Collections.MapField`2::Clear()
// 0x000007C6 System.Boolean Google.Protobuf.Collections.MapField`2::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
// 0x000007C7 System.Void Google.Protobuf.Collections.MapField`2::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
// 0x000007C8 System.Boolean Google.Protobuf.Collections.MapField`2::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
// 0x000007C9 System.Int32 Google.Protobuf.Collections.MapField`2::get_Count()
// 0x000007CA System.Boolean Google.Protobuf.Collections.MapField`2::get_IsReadOnly()
// 0x000007CB System.Boolean Google.Protobuf.Collections.MapField`2::Equals(System.Object)
// 0x000007CC System.Int32 Google.Protobuf.Collections.MapField`2::GetHashCode()
// 0x000007CD System.Boolean Google.Protobuf.Collections.MapField`2::Equals(Google.Protobuf.Collections.MapField`2<TKey,TValue>)
// 0x000007CE System.Void Google.Protobuf.Collections.MapField`2::AddEntriesFrom(Google.Protobuf.ParseContext&,Google.Protobuf.Collections.MapField`2/Codec<TKey,TValue>)
// 0x000007CF System.Void Google.Protobuf.Collections.MapField`2::WriteTo(Google.Protobuf.WriteContext&,Google.Protobuf.Collections.MapField`2/Codec<TKey,TValue>)
// 0x000007D0 System.Int32 Google.Protobuf.Collections.MapField`2::CalculateSize(Google.Protobuf.Collections.MapField`2/Codec<TKey,TValue>)
// 0x000007D1 System.Int32 Google.Protobuf.Collections.MapField`2::CalculateEntrySize(Google.Protobuf.Collections.MapField`2/Codec<TKey,TValue>,System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
// 0x000007D2 System.String Google.Protobuf.Collections.MapField`2::ToString()
// 0x000007D3 System.Boolean Google.Protobuf.Collections.MapField`2::System.Collections.IDictionary.Contains(System.Object)
// 0x000007D4 System.Collections.IDictionaryEnumerator Google.Protobuf.Collections.MapField`2::System.Collections.IDictionary.GetEnumerator()
// 0x000007D5 System.Void Google.Protobuf.Collections.MapField`2::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
// 0x000007D6 System.Object Google.Protobuf.Collections.MapField`2::System.Collections.ICollection.get_SyncRoot()
// 0x000007D7 System.Object Google.Protobuf.Collections.MapField`2::System.Collections.IDictionary.get_Item(System.Object)
// 0x000007D8 System.Void Google.Protobuf.Collections.MapField`2::System.Collections.IDictionary.set_Item(System.Object,System.Object)
// 0x000007D9 System.Void Google.Protobuf.Collections.MapField`2::.ctor()
// 0x000007DA System.Void Google.Protobuf.Collections.MapField`2::.cctor()
// 0x000007DB System.Void Google.Protobuf.Collections.MapField`2/DictionaryEnumerator::.ctor(System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>>)
// 0x000007DC System.Boolean Google.Protobuf.Collections.MapField`2/DictionaryEnumerator::MoveNext()
// 0x000007DD System.Void Google.Protobuf.Collections.MapField`2/DictionaryEnumerator::Reset()
// 0x000007DE System.Object Google.Protobuf.Collections.MapField`2/DictionaryEnumerator::get_Current()
// 0x000007DF System.Collections.DictionaryEntry Google.Protobuf.Collections.MapField`2/DictionaryEnumerator::get_Entry()
// 0x000007E0 System.Object Google.Protobuf.Collections.MapField`2/DictionaryEnumerator::get_Key()
// 0x000007E1 System.Object Google.Protobuf.Collections.MapField`2/DictionaryEnumerator::get_Value()
// 0x000007E2 System.Void Google.Protobuf.Collections.MapField`2/Codec::.ctor(Google.Protobuf.FieldCodec`1<TKey>,Google.Protobuf.FieldCodec`1<TValue>,System.UInt32)
// 0x000007E3 Google.Protobuf.FieldCodec`1<TKey> Google.Protobuf.Collections.MapField`2/Codec::get_KeyCodec()
// 0x000007E4 Google.Protobuf.FieldCodec`1<TValue> Google.Protobuf.Collections.MapField`2/Codec::get_ValueCodec()
// 0x000007E5 System.UInt32 Google.Protobuf.Collections.MapField`2/Codec::get_MapTag()
// 0x000007E6 System.Void Google.Protobuf.Collections.MapField`2/MapView`1::.ctor(Google.Protobuf.Collections.MapField`2<TKey,TValue>,System.Func`2<System.Collections.Generic.KeyValuePair`2<TKey,TValue>,T>,System.Func`2<T,System.Boolean>)
// 0x000007E7 System.Int32 Google.Protobuf.Collections.MapField`2/MapView`1::get_Count()
// 0x000007E8 System.Boolean Google.Protobuf.Collections.MapField`2/MapView`1::get_IsReadOnly()
// 0x000007E9 System.Object Google.Protobuf.Collections.MapField`2/MapView`1::get_SyncRoot()
// 0x000007EA System.Void Google.Protobuf.Collections.MapField`2/MapView`1::Add(T)
// 0x000007EB System.Void Google.Protobuf.Collections.MapField`2/MapView`1::Clear()
// 0x000007EC System.Boolean Google.Protobuf.Collections.MapField`2/MapView`1::Contains(T)
// 0x000007ED System.Void Google.Protobuf.Collections.MapField`2/MapView`1::CopyTo(T[],System.Int32)
// 0x000007EE System.Collections.Generic.IEnumerator`1<T> Google.Protobuf.Collections.MapField`2/MapView`1::GetEnumerator()
// 0x000007EF System.Boolean Google.Protobuf.Collections.MapField`2/MapView`1::Remove(T)
// 0x000007F0 System.Collections.IEnumerator Google.Protobuf.Collections.MapField`2/MapView`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000007F1 System.Void Google.Protobuf.Collections.MapField`2/MapView`1::CopyTo(System.Array,System.Int32)
// 0x000007F2 System.Void Google.Protobuf.Collections.MapField`2/<>c__DisplayClass7_0::.ctor()
// 0x000007F3 System.Boolean Google.Protobuf.Collections.MapField`2/<>c__DisplayClass7_0::<ContainsValue>b__0(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
// 0x000007F4 System.Void Google.Protobuf.Collections.MapField`2/<>c::.cctor()
// 0x000007F5 System.Void Google.Protobuf.Collections.MapField`2/<>c::.ctor()
// 0x000007F6 TValue Google.Protobuf.Collections.MapField`2/<>c::<get_Values>b__16_0(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
// 0x000007F7 System.Collections.DictionaryEntry Google.Protobuf.Collections.MapField`2/<>c::<System.Collections.ICollection.CopyTo>b__43_0(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
// 0x000007F8 System.Collections.Generic.EqualityComparer`1<T> Google.Protobuf.Collections.ProtobufEqualityComparers::GetEqualityComparer()
// 0x000007F9 System.Collections.Generic.EqualityComparer`1<System.Double> Google.Protobuf.Collections.ProtobufEqualityComparers::get_BitwiseDoubleEqualityComparer()
extern void ProtobufEqualityComparers_get_BitwiseDoubleEqualityComparer_mE44F7862345571F0CA657DC541C39FE262543714 (void);
// 0x000007FA System.Collections.Generic.EqualityComparer`1<System.Single> Google.Protobuf.Collections.ProtobufEqualityComparers::get_BitwiseSingleEqualityComparer()
extern void ProtobufEqualityComparers_get_BitwiseSingleEqualityComparer_m5E6A6EB33216A3E9E829ED8FBA161E0B7BBCCBDB (void);
// 0x000007FB System.Collections.Generic.EqualityComparer`1<System.Nullable`1<System.Double>> Google.Protobuf.Collections.ProtobufEqualityComparers::get_BitwiseNullableDoubleEqualityComparer()
extern void ProtobufEqualityComparers_get_BitwiseNullableDoubleEqualityComparer_m137AF49F6F51AC6745E79BB1EDDA611DCDCEE632 (void);
// 0x000007FC System.Collections.Generic.EqualityComparer`1<System.Nullable`1<System.Single>> Google.Protobuf.Collections.ProtobufEqualityComparers::get_BitwiseNullableSingleEqualityComparer()
extern void ProtobufEqualityComparers_get_BitwiseNullableSingleEqualityComparer_m903E7CC373C92BA08C7D8DC4B5569774DFB5A090 (void);
// 0x000007FD System.Void Google.Protobuf.Collections.ProtobufEqualityComparers::.cctor()
extern void ProtobufEqualityComparers__cctor_m223E93E99644A8615959C1427CF098ED377FC4D6 (void);
// 0x000007FE System.Boolean Google.Protobuf.Collections.ProtobufEqualityComparers/BitwiseDoubleEqualityComparerImpl::Equals(System.Double,System.Double)
extern void BitwiseDoubleEqualityComparerImpl_Equals_m3D1DE5B018777F8054F4498447E193DE57E1377A (void);
// 0x000007FF System.Int32 Google.Protobuf.Collections.ProtobufEqualityComparers/BitwiseDoubleEqualityComparerImpl::GetHashCode(System.Double)
extern void BitwiseDoubleEqualityComparerImpl_GetHashCode_m17D9A5AD333C5C4259F628C9AA83BB0B0518D869 (void);
// 0x00000800 System.Void Google.Protobuf.Collections.ProtobufEqualityComparers/BitwiseDoubleEqualityComparerImpl::.ctor()
extern void BitwiseDoubleEqualityComparerImpl__ctor_m42230DFBD6329922D97BA6121A5869C56873161E (void);
// 0x00000801 System.Boolean Google.Protobuf.Collections.ProtobufEqualityComparers/BitwiseSingleEqualityComparerImpl::Equals(System.Single,System.Single)
extern void BitwiseSingleEqualityComparerImpl_Equals_m8AB7DD8696608B73490722B7D6905D89F057A70A (void);
// 0x00000802 System.Int32 Google.Protobuf.Collections.ProtobufEqualityComparers/BitwiseSingleEqualityComparerImpl::GetHashCode(System.Single)
extern void BitwiseSingleEqualityComparerImpl_GetHashCode_mEA1C8E4E164520A05A12B8EF07FB96F01FAC6D5D (void);
// 0x00000803 System.Void Google.Protobuf.Collections.ProtobufEqualityComparers/BitwiseSingleEqualityComparerImpl::.ctor()
extern void BitwiseSingleEqualityComparerImpl__ctor_m7480C419D32175FA06D087D0858E36AC01CE2F26 (void);
// 0x00000804 System.Boolean Google.Protobuf.Collections.ProtobufEqualityComparers/BitwiseNullableDoubleEqualityComparerImpl::Equals(System.Nullable`1<System.Double>,System.Nullable`1<System.Double>)
extern void BitwiseNullableDoubleEqualityComparerImpl_Equals_m49F8CDD266A123D145A38883D55CB4101752A4C6 (void);
// 0x00000805 System.Int32 Google.Protobuf.Collections.ProtobufEqualityComparers/BitwiseNullableDoubleEqualityComparerImpl::GetHashCode(System.Nullable`1<System.Double>)
extern void BitwiseNullableDoubleEqualityComparerImpl_GetHashCode_m17468884ABAA2B04E0C699D1A0E566CAC371AF61 (void);
// 0x00000806 System.Void Google.Protobuf.Collections.ProtobufEqualityComparers/BitwiseNullableDoubleEqualityComparerImpl::.ctor()
extern void BitwiseNullableDoubleEqualityComparerImpl__ctor_m3F13ED9C4964530540A1B7AB182C63423C1C8123 (void);
// 0x00000807 System.Boolean Google.Protobuf.Collections.ProtobufEqualityComparers/BitwiseNullableSingleEqualityComparerImpl::Equals(System.Nullable`1<System.Single>,System.Nullable`1<System.Single>)
extern void BitwiseNullableSingleEqualityComparerImpl_Equals_m71EF7BDCCE81CC09EC352D957239E077848BD442 (void);
// 0x00000808 System.Int32 Google.Protobuf.Collections.ProtobufEqualityComparers/BitwiseNullableSingleEqualityComparerImpl::GetHashCode(System.Nullable`1<System.Single>)
extern void BitwiseNullableSingleEqualityComparerImpl_GetHashCode_mADDC36D6849BE45D0FF7E8E9EFB28F3EBF8530C9 (void);
// 0x00000809 System.Void Google.Protobuf.Collections.ProtobufEqualityComparers/BitwiseNullableSingleEqualityComparerImpl::.ctor()
extern void BitwiseNullableSingleEqualityComparerImpl__ctor_m7C52BA8E085815257246D2FC6537F17AE8CC3C5B (void);
// 0x0000080A Google.Protobuf.Collections.RepeatedField`1<T> Google.Protobuf.Collections.RepeatedField`1::Clone()
// 0x0000080B System.Void Google.Protobuf.Collections.RepeatedField`1::AddEntriesFrom(Google.Protobuf.CodedInputStream,Google.Protobuf.FieldCodec`1<T>)
// 0x0000080C System.Void Google.Protobuf.Collections.RepeatedField`1::AddEntriesFrom(Google.Protobuf.ParseContext&,Google.Protobuf.FieldCodec`1<T>)
// 0x0000080D System.Int32 Google.Protobuf.Collections.RepeatedField`1::CalculateSize(Google.Protobuf.FieldCodec`1<T>)
// 0x0000080E System.Int32 Google.Protobuf.Collections.RepeatedField`1::CalculatePackedDataSize(Google.Protobuf.FieldCodec`1<T>)
// 0x0000080F System.Void Google.Protobuf.Collections.RepeatedField`1::WriteTo(Google.Protobuf.CodedOutputStream,Google.Protobuf.FieldCodec`1<T>)
// 0x00000810 System.Void Google.Protobuf.Collections.RepeatedField`1::WriteTo(Google.Protobuf.WriteContext&,Google.Protobuf.FieldCodec`1<T>)
// 0x00000811 System.Void Google.Protobuf.Collections.RepeatedField`1::EnsureSize(System.Int32)
// 0x00000812 System.Void Google.Protobuf.Collections.RepeatedField`1::SetSize(System.Int32)
// 0x00000813 System.Void Google.Protobuf.Collections.RepeatedField`1::Add(T)
// 0x00000814 System.Void Google.Protobuf.Collections.RepeatedField`1::Clear()
// 0x00000815 System.Boolean Google.Protobuf.Collections.RepeatedField`1::Contains(T)
// 0x00000816 System.Void Google.Protobuf.Collections.RepeatedField`1::CopyTo(T[],System.Int32)
// 0x00000817 System.Boolean Google.Protobuf.Collections.RepeatedField`1::Remove(T)
// 0x00000818 System.Int32 Google.Protobuf.Collections.RepeatedField`1::get_Count()
// 0x00000819 System.Boolean Google.Protobuf.Collections.RepeatedField`1::get_IsReadOnly()
// 0x0000081A System.Void Google.Protobuf.Collections.RepeatedField`1::AddRange(System.Collections.Generic.IEnumerable`1<T>)
// 0x0000081B System.Void Google.Protobuf.Collections.RepeatedField`1::Add(System.Collections.Generic.IEnumerable`1<T>)
// 0x0000081C System.Collections.Generic.IEnumerator`1<T> Google.Protobuf.Collections.RepeatedField`1::GetEnumerator()
// 0x0000081D System.Boolean Google.Protobuf.Collections.RepeatedField`1::Equals(System.Object)
// 0x0000081E System.Collections.IEnumerator Google.Protobuf.Collections.RepeatedField`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000081F System.Int32 Google.Protobuf.Collections.RepeatedField`1::GetHashCode()
// 0x00000820 System.Boolean Google.Protobuf.Collections.RepeatedField`1::Equals(Google.Protobuf.Collections.RepeatedField`1<T>)
// 0x00000821 System.Int32 Google.Protobuf.Collections.RepeatedField`1::IndexOf(T)
// 0x00000822 System.Void Google.Protobuf.Collections.RepeatedField`1::Insert(System.Int32,T)
// 0x00000823 System.Void Google.Protobuf.Collections.RepeatedField`1::RemoveAt(System.Int32)
// 0x00000824 System.String Google.Protobuf.Collections.RepeatedField`1::ToString()
// 0x00000825 T Google.Protobuf.Collections.RepeatedField`1::get_Item(System.Int32)
// 0x00000826 System.Void Google.Protobuf.Collections.RepeatedField`1::set_Item(System.Int32,T)
// 0x00000827 System.Void Google.Protobuf.Collections.RepeatedField`1::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
// 0x00000828 System.Object Google.Protobuf.Collections.RepeatedField`1::System.Collections.ICollection.get_SyncRoot()
// 0x00000829 System.Object Google.Protobuf.Collections.RepeatedField`1::System.Collections.IList.get_Item(System.Int32)
// 0x0000082A System.Void Google.Protobuf.Collections.RepeatedField`1::System.Collections.IList.set_Item(System.Int32,System.Object)
// 0x0000082B System.Int32 Google.Protobuf.Collections.RepeatedField`1::System.Collections.IList.Add(System.Object)
// 0x0000082C System.Boolean Google.Protobuf.Collections.RepeatedField`1::System.Collections.IList.Contains(System.Object)
// 0x0000082D System.Int32 Google.Protobuf.Collections.RepeatedField`1::System.Collections.IList.IndexOf(System.Object)
// 0x0000082E System.Void Google.Protobuf.Collections.RepeatedField`1::System.Collections.IList.Insert(System.Int32,System.Object)
// 0x0000082F System.Void Google.Protobuf.Collections.RepeatedField`1::System.Collections.IList.Remove(System.Object)
// 0x00000830 System.Void Google.Protobuf.Collections.RepeatedField`1::.ctor()
// 0x00000831 System.Void Google.Protobuf.Collections.RepeatedField`1::.cctor()
// 0x00000832 System.Void Google.Protobuf.Collections.RepeatedField`1/<GetEnumerator>d__28::.ctor(System.Int32)
// 0x00000833 System.Void Google.Protobuf.Collections.RepeatedField`1/<GetEnumerator>d__28::System.IDisposable.Dispose()
// 0x00000834 System.Boolean Google.Protobuf.Collections.RepeatedField`1/<GetEnumerator>d__28::MoveNext()
// 0x00000835 T Google.Protobuf.Collections.RepeatedField`1/<GetEnumerator>d__28::System.Collections.Generic.IEnumerator<T>.get_Current()
// 0x00000836 System.Void Google.Protobuf.Collections.RepeatedField`1/<GetEnumerator>d__28::System.Collections.IEnumerator.Reset()
// 0x00000837 System.Object Google.Protobuf.Collections.RepeatedField`1/<GetEnumerator>d__28::System.Collections.IEnumerator.get_Current()
static Il2CppMethodPointer s_methodPointers[2103] = 
{
	EmbeddedAttribute__ctor_mC4A60001FA556CF2C0A731104B5CEC786B0CB863,
	IsByRefLikeAttribute__ctor_m80997F85AA38401591B7574259E29881904ADC76,
	ByteString_AttachBytes_mDC1879404E2232856A143566E43072A86B37E561,
	ByteString_AttachBytes_m0F32349B0FCFC5471F4A3A9EE7F854AF6B5B5D7B,
	ByteString__ctor_m187F59F42710CB6F5225669981E249B6FBC5CEA5,
	ByteString_get_Empty_m557AC9DB3319BA318064975841E726A1F94581C6,
	ByteString_get_Length_m7E5EF70AFE2C143AC75D11C41E5FFF131FBADB61,
	ByteString_get_Span_m77B79420B0BBA1D1A0B36F0C3DE0E3B2EBDE32E4,
	ByteString_ToBase64_mFF01EB56ED7E23C4BF01A8A0B008ABD1DC08D8CF,
	ByteString_CopyFrom_m5B311324FC5F998F3DB5B3E2B6AFAB16C497B8B0,
	ByteString_GetEnumerator_m784D34E951A7A3FD048BA6ECD6E5AECF4E3C5CCF,
	ByteString_System_Collections_IEnumerable_GetEnumerator_m8F5EEDBC5634A38AF16663BD12846A7289D493E2,
	ByteString_CreateCodedInput_m4254D8478FE34A7340DF4C6B6CAFD07E9BA4EBF5,
	ByteString_op_Equality_mC5A462A8865F4AFE094BE7A44612D48B087CF28B,
	ByteString_op_Inequality_mB58011343AD429C8AD0E05AFF0A651ADA18C9D8B,
	ByteString_Equals_m80505FF3A839E2DED23DE93456EA2ACFA3C4E361,
	ByteString_GetHashCode_m1BB2CD9974C995B8AF0145DBC273C8E21167A289,
	ByteString_Equals_m9768F560492F61537B71FE0E5A85747948BD2E80,
	ByteString__cctor_m7A33433DDDE5A01F2BE5CFBE1DEA39453F6BC62F,
	CodedInputStream__ctor_mE8E27A2076287B2B96592DFED394B71F859B63C7,
	CodedInputStream__ctor_m96A6E9289D1BC42DBFDBC26C24E919AAC6971A1E,
	CodedInputStream_set_DiscardUnknownFields_m075C8E7040091C7606C4D77D7A17FF43EC47DE90,
	CodedInputStream_set_ExtensionRegistry_m7A74E3AFA8B0D9890FB2898FBA5520A5F8D88388,
	CodedInputStream_get_InternalBuffer_m8ED6DAC00B5378FD8AC7236ECED00D47B23A91B9,
	CodedInputStream_get_InternalInputStream_m7C01D4DA80B4582DC88EC94E0E7B8F44A59FA224,
	CodedInputStream_get_InternalState_m7BE1F1D3170D69A9B71FF50503E3D4F03CBD1D5A,
	CodedInputStream_Dispose_m836C63C970DCD3F257DAA2E57C459D9C84E49120,
	CodedInputStream_CheckReadEndOfStreamTag_m3B7164CE80D09CFD73D100A4704EEAB7408E1D1D,
	CodedInputStream_ReadTag_mB343941331F8E86DA3306D4D199E9CD0D82368B5,
	CodedInputStream_ReadFloat_mB03C9E0BB233097A2F735B708F9559360679FFB0,
	CodedInputStream_ReadInt64_m18E7F87499399FC30B303F0BB249CE79B8AF7845,
	CodedInputStream_ReadInt32_mC0FA7EDD8272884A635BB0BFAC8DFDB541D3FD6F,
	CodedInputStream_ReadBool_m621B922FD4F0BDF6FA6E3ACE90CCC284AAA1368A,
	CodedInputStream_ReadString_mDDD43FF62E2E89AC3E3679E7AC374765AB3E7827,
	CodedInputStream_ReadMessage_mA7ADE8031D91AEE188B9DB76768340C9FFEF1A46,
	CodedInputStream_ReadEnum_m7ABEC74A9C69CE648B9662E9B3464F0233C4387E,
	CodedInputStream_ReadRawVarint32_m1E6B9987477C63A248CC7A50014BF6A8D0CB704D,
	CodedInputStream_ReadRawVarint64_m5ED379B1F436F32BC308B6BAD560F9EE8D15B799,
	CodedInputStream_ReadRawMessage_m77E217968CD5654CC717E7DA57906C0219372943,
	CodedOutputStream_ComputeUInt64Size_mA7248484F80AE2EF1834D551EA35ADF1FC0A3180,
	CodedOutputStream_ComputeInt64Size_m5DEFA6C0EA352C5386383B9149DA6A02FDFE084D,
	CodedOutputStream_ComputeInt32Size_m03C7A226C41F660D0A853D576117A6386B9D0802,
	CodedOutputStream_ComputeFixed64Size_m85712D2C1965813F1B33E40C4CD5B2F7246A06A1,
	CodedOutputStream_ComputeFixed32Size_m741ADFA703A6FC8573143E69FDE1E300BF7E67F5,
	CodedOutputStream_ComputeStringSize_m3913248CC93D6067C32930171244FD185D818BDF,
	CodedOutputStream_ComputeMessageSize_m9E53FB7A0F5C881075F630F771D962E2705525C3,
	CodedOutputStream_ComputeBytesSize_m353054D6ADADF9FF9FC4820BFEAAA56F65874FC6,
	CodedOutputStream_ComputeUInt32Size_m3C6EEC21F0AB57DB286D6F6FCA075D24A458476F,
	CodedOutputStream_ComputeEnumSize_m6E6CA2CF9523B109061C60689E5B1B3AF60725C9,
	CodedOutputStream_ComputeLengthSize_m65C9D63AD1DD32EDBB3DBDF548D042702DA8C42D,
	CodedOutputStream_ComputeRawVarint32Size_mFFED3477FE86FFDAAA384AE87B042DBB93C14796,
	CodedOutputStream_ComputeRawVarint64Size_mCEDC6338D482AA1533B45CC87F3418EF5E0B9113,
	CodedOutputStream_ComputeTagSize_m3F3A5FB77424F92EAFF1A8C5C301BFA25CB98B20,
	CodedOutputStream__ctor_m9C304A1E8EF1207108AE808E3070AC471DD2993B,
	CodedOutputStream__ctor_m04CE92D796E0E93EFB0BB245E7259104522A7DA8,
	CodedOutputStream_WriteFloat_m4836E03ACBC9A8BC134531C338C710CFDBF508DB,
	CodedOutputStream_WriteInt64_m2A02EC95E6A8674FC4BB4DC0916FE1D0B503E4A2,
	CodedOutputStream_WriteInt32_mC6C462880D8E3DAA7A90BA4014EC98F8C5B6707F,
	CodedOutputStream_WriteBool_m857D13C9DD497AEE09EF2DD772989698E0D82882,
	CodedOutputStream_WriteString_mD8116449C33FD9927EA22173717C811A3B72719E,
	CodedOutputStream_WriteMessage_mBD62FB39D6A6D0D51B2A1A05577F8F21D8E02F8D,
	CodedOutputStream_WriteRawMessage_m05CED72DF9A800E9BFB28956F21E7B4416E03D11,
	CodedOutputStream_WriteEnum_m7D1360B9AC9ECF6E336179E786671AA9408C2FBD,
	CodedOutputStream_WriteRawTag_m709724C278839993424703351DF6FB7BCD2C1675,
	CodedOutputStream_WriteRawTag_m4628DE31677CF587C0D6160CF295CC71EFF6AC12,
	CodedOutputStream_Dispose_m03152AD8DEC550A0DB76D290F0E624BE73D96806,
	CodedOutputStream_Flush_m02373D0BABF1E63618932A10659FFD1571E41D4E,
	CodedOutputStream_CheckNoSpaceLeft_mEF97F3A84CA0D669EBECE7128818FD19CA04D7AD,
	CodedOutputStream_get_InternalBuffer_m945C519723571718B518C56653A9A7DA5F7BBECA,
	CodedOutputStream_get_InternalOutputStream_m85CB0EA8CA59EB077DE0E4CA8DB550CA4D1F88E3,
	CodedOutputStream_get_InternalState_m08406AFD75E90BC4C1F5927C63D3F29BF0B128F9,
	CodedOutputStream__cctor_m5D1B2D651124EA8ED740645D67E8005208ED1684,
	OutOfSpaceException__ctor_m971089A50DDCF3E93F0BC1E6BB03B36B530DF3C9,
	NULL,
	NULL,
	Extension_get_FieldNumber_mA033A36AD6AC429AFD166C84A6BBB13ECD45B579,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ExtensionRegistry__ctor_m467F6F03537721D3B00C7209E8D4A52B6B233A1C,
	ExtensionRegistry__ctor_m34AA493FD4CFC60446427D7548DE508739B6C78B,
	ExtensionRegistry_get_Count_m0FF84D632A1DD28FC6507D5679ECEC97D0548B92,
	ExtensionRegistry_System_Collections_Generic_ICollectionU3CGoogle_Protobuf_ExtensionU3E_get_IsReadOnly_mF8752C2176E029297ED18CC8CA9E9808E1A53AA5,
	ExtensionRegistry_ContainsInputField_m4163B444D36A832DAD30F332A010356FAB62F899,
	ExtensionRegistry_Add_m76E4A7F7DD2B9D7BBC3DA363DDB875E32AD7482F,
	ExtensionRegistry_AddRange_m5D36918B79A6B118519025CBBCF0ABD4B16F4D1D,
	ExtensionRegistry_Clear_m6C0223E3D9CF2B5BAFE197CD102FAF9915B65A0D,
	ExtensionRegistry_Contains_mC285CBA8174D1D34E92381633141CC9235AD25F7,
	ExtensionRegistry_System_Collections_Generic_ICollectionU3CGoogle_Protobuf_ExtensionU3E_CopyTo_mFD6C19AD6A21DA6E4D506C77BBEF29655EA53DF8,
	ExtensionRegistry_GetEnumerator_m8B43AE99907CFA07AA4F0FCFCB2F3D78A113BEA2,
	ExtensionRegistry_Remove_m769995CA8656F036CC43EF9E8F3031E27B1F8A34,
	ExtensionRegistry_System_Collections_IEnumerable_GetEnumerator_mD72DFA3D924882B12250828545AD7F9402357652,
	ExtensionRegistry_Clone_mC9301792F1B16502D1A6AD13132D087A2A88D205,
	ExtensionComparer_Equals_mB396D42A10CD9EC4FD27A3EAA47C0BAF1B087F7F,
	ExtensionComparer_GetHashCode_mCF8B9F41F0D06830A63F2CD551B1017653287C1A,
	ExtensionComparer__ctor_mB59BF9FCC245E9E6F78EEE4FE2BB61E0B2F6FDD1,
	ExtensionComparer__cctor_mF316743F780039C34F959DDE162950A4A42116B2,
	U3CU3Ec__cctor_m1570AE1383064B8D35317069D4DBC927972CF392,
	U3CU3Ec__ctor_m848476C8C7E8D018C352DFA811B97D7C61C590D7,
	U3CU3Ec_U3C_ctorU3Eb__3_0_m1C441539DEBDF1ADEC32810641BEF979D9EA693F,
	U3CU3Ec_U3C_ctorU3Eb__3_1_mFDFE2F28B907535522F7D50FA7F7E4DA101268D5,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	FieldCodec_ForString_m686A669CFD9CF6A06AE0329BB75BFF8D20C17A32,
	FieldCodec_ForInt32_mA8FC008DA1A37C17DED262F93274DACAEAE60621,
	FieldCodec_ForUInt32_mAE58216C2BB7BC5C71AB5323545035F56386DA3E,
	FieldCodec_ForFloat_m137F48ED992040FF1BF81F4B59637342DD724B80,
	FieldCodec_ForString_m8114E29C7FD5C4B23D6992A8B3276E34F9CC7571,
	FieldCodec_ForInt32_m7155623F3AAC2B9859A93DE278A89F5C46C9CD27,
	FieldCodec_ForUInt32_m3DA357EA2E5517EA0C33145B948CFB33E1E0CB75,
	FieldCodec_ForFloat_m7AB9D6D9E9EE313CB9398C4E553FC40B21E7B735,
	NULL,
	U3CU3Ec__cctor_mC397E02616131EA53B2DAFC24E2835780A93E78B,
	U3CU3Ec__ctor_m19E1B9F158751668E1950E90EAD79637FEF05B41,
	U3CU3Ec_U3CForStringU3Eb__16_0_m6FF30F39F8EF8E9894C4AB585144D034C41CC62F,
	U3CU3Ec_U3CForStringU3Eb__16_1_m4C0D631118C6024CB9BC3BDC6B5157805C5C858D,
	U3CU3Ec_U3CForInt32U3Eb__19_0_mB0615A186AA6F9BE7FC9B94329140448BDF9DDD5,
	U3CU3Ec_U3CForInt32U3Eb__19_1_m73BD232D28C8D24526A506D37AC43525AAA546A8,
	U3CU3Ec_U3CForUInt32U3Eb__23_0_m7493AAC26759C3EEA6814118D3A02B2A700DF57D,
	U3CU3Ec_U3CForUInt32U3Eb__23_1_m10E0019D299AA0C40CE00B4311337F497B36EEBE,
	U3CU3Ec_U3CForFloatU3Eb__29_0_m5E7B1CF62FEC7B6468FDBFE0C06FB8B9146A30BE,
	U3CU3Ec_U3CForFloatU3Eb__29_1_m6E83611C70058A29A0F4D0434049CF8DEE3E9890,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	FrameworkPortability__cctor_m801FE62EEC02CB0B81953BCFA718130FD04FC44A,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	InvalidProtocolBufferException__ctor_m97213D88CEBD314FB2F77EFB4B9202A9BD801A9B,
	InvalidProtocolBufferException_MoreDataAvailable_mBCBB93C1A4BE4D08327A0179D7CABF90E23AEF5E,
	InvalidProtocolBufferException_TruncatedMessage_m049CB9C77F1D2B7642E2BB6DF96A16040F4FA652,
	InvalidProtocolBufferException_NegativeSize_m2D86551E8150EC70DCF2DCDBF78687FB83457E44,
	InvalidProtocolBufferException_MalformedVarint_m3EBE0AF1E7098AABEA72E275B25470253EC6298E,
	InvalidProtocolBufferException_InvalidTag_m956A33D88AC6DFDA5BCBCA12E9B4522A91B54789,
	InvalidProtocolBufferException_InvalidWireType_m5282D58657D70BB87AA17FF8B67EC9859B2013B3,
	InvalidProtocolBufferException_InvalidEndTag_mC822DDDBA12C5777CAF76A906A8AA96B9598FE09,
	InvalidProtocolBufferException_RecursionLimitExceeded_m5033217153F5E14D4AF5F8FB4D6586F38C83F7DA,
	InvalidProtocolBufferException_SizeLimitExceeded_mBA2A8064EFD92489926DA3226ED67A62878B6A5A,
	JsonFormatter_get_Default_m5A9C0948E8C0D01D6700160BED899AD2507140D3,
	JsonFormatter__cctor_mC34CC91F731EFA758C29177A725364FCD1EAA437,
	JsonFormatter_get_DiagnosticOnly_mB4C251FB0989EAB4A33A2871E83BDC61DCFBE411,
	JsonFormatter__ctor_mDD448159E056960E96A0A194B4A9B2EE5D3F929C,
	JsonFormatter_Format_m69B067C28BCE856B5063696BE1557B3AAD3214C3,
	JsonFormatter_Format_mAB3068DB9F98B576C1C767B3CE003828F889CC43,
	JsonFormatter_ToDiagnosticString_mB0CBCBDBF65F17FC52DEB9E0C49184E44F3F2EF1,
	JsonFormatter_WriteMessage_mFCD4C794107AA4DC910B3AF0C5C9550C376195DC,
	JsonFormatter_WriteMessageFields_mA6AA1382471AA43A1D3F634CFBDBAC77439AC1A7,
	JsonFormatter_ShouldFormatFieldValue_mC94B0485C44C1E8D14D3E46FA1B89FC338A3AD6B,
	JsonFormatter_ToJsonName_m3DF73844C3F14403A0B0945B8D47161E4EE95D43,
	JsonFormatter_WriteNull_m460643D32D3CB9A93245383AE62CCA09C78C2330,
	JsonFormatter_IsDefaultValue_mB2A8144C10EDB1F8B17027915EF39DF2F5642BC4,
	JsonFormatter_WriteValue_m9312535823E296DD783D53133645231311E61D90,
	JsonFormatter_WriteWellKnownTypeValue_mCB7BB81E1D3D4FD7263576AC919816FF2DE09ED4,
	JsonFormatter_WriteTimestamp_mBC5CFB76D835F00F9AD770422F96649A69C09D75,
	JsonFormatter_WriteDuration_mF1E7087F5235E61DA7F42C914763CD2640E9A0A9,
	JsonFormatter_WriteFieldMask_m8089EC7E6F371A3F16B76C0F7982B2F4556C66D7,
	JsonFormatter_WriteAny_m87C93F6B5F83D2A294F141BC10F9728006EA25FC,
	JsonFormatter_WriteDiagnosticOnlyAny_m9D5B3C912D6DEFAD518D2B8E5BA1CFB09B8B0D52,
	JsonFormatter_WriteStruct_m2EEFEE9C86D94A3BEA199AA236BCE1BAF936BF0B,
	JsonFormatter_WriteStructFieldValue_m85C319B5B236A40D7E069A5E42A0DFDAF7F1B136,
	JsonFormatter_WriteList_m01543BDA268D0BCB53EE9B1CC1B84A67C7FDF52D,
	JsonFormatter_WriteDictionary_m05901C36EEDF7132D13097D539AA6A46B405D236,
	JsonFormatter_WriteString_mD25155E3B003EF3A17D7F5D01971D38FACA89015,
	JsonFormatter_HexEncodeUtf16CodeUnit_mA2EEC68387011433F6B4E29F1598386C374E4AD3,
	Settings_get_Default_mF4FDE9D3D751BD702D8219E4EB5517E0F6FF9B74,
	Settings__cctor_mF4512A1C1C5FEA84200E9397456A62A96B181FD2,
	Settings_get_FormatDefaultValues_m64C456D788C62CB3E0F0996AFF989CC6981239F3,
	Settings_get_TypeRegistry_m9BA673D58183796B0F0C9E2DBBE89F432A6D6D0E,
	Settings_get_FormatEnumsAsIntegers_m51CB1E83B7EA54E4D894263F570805407D05CFE1,
	Settings__ctor_mA9EA3FE08831F0F5A44D585C0DB04A208F02AEFA,
	Settings__ctor_m929B7642DBF8FCFDA94B97233FC773B74DABD7B7,
	Settings__ctor_m778ABAE093451E7B1CA47C2A3B43D94C8A9D46BB,
	OriginalEnumValueHelper_GetOriginalName_m36AA4B3C591A31126E42DD475946C02D9CB1296C,
	OriginalEnumValueHelper_GetNameMapping_mC0C9C5FCD331F9997BF3F2A6D6349AB366C8D2F4,
	OriginalEnumValueHelper__cctor_m08FFA8138A0441FC17D4D97DB5F209877A9F496E,
	U3CU3Ec__cctor_m23171665533BE5299B7466C18DEFEA08A225CED5,
	U3CU3Ec__ctor_m8A4E99CDBE7F880923AF3978955A596A6CFEAD21,
	U3CU3Ec_U3CGetNameMappingU3Eb__2_0_mAB55EF229CE87D1E2A5F3EC117AFEE14CB3B0019,
	U3CU3Ec_U3CGetNameMappingU3Eb__2_1_m4363C5DBD92E971FA74128DFCEAE80C4FFDF40BE,
	U3CU3Ec_U3CGetNameMappingU3Eb__2_2_mC6E555E9DF03A3DD2D7841046524957CFD0F081D,
	U3CU3Ec_U3CGetNameMappingU3Eb__2_3_m974D038691FD95D34419B7B4D7EA34E65A8D05A9,
	MessageExtensions_ToByteArray_m1BBBB6D4DC2D766396375663F29F4427A33D8812,
	MessageExtensions_MergeFrom_m1473F9DF3353F288CF4A04AA5CE23D014542C5F4,
	MessageExtensions_MergeFrom_mF7F80928C53902B587749A72D0569EFF68C10DA6,
	MessageParser_get_DiscardUnknownFields_m1B7BDCE4283878E28C0B4ACB3944EF71ED872B98,
	MessageParser_get_Extensions_m13D4911AAC2CFF3977A4EF7A60853D1234526737,
	MessageParser__ctor_mAF12C3D74ED237CE23AB15C17CE9D9DCF79C86AA,
	MessageParser_ParseFrom_mC4BC411F00F2ACB3A4CFD1C140BFAAE0467144B4,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ParseContext_Initialize_mBED2DA9409401F1EA9B8FC92DA667C0669BA41B0,
	ParseContext_Initialize_mABC8723132FF5E7405A2E515A02306C184CB1B1C,
	ParseContext_Initialize_m8EAA0BF3F3E350F19FC82575E0005B79CF3DFB2F,
	ParseContext_Initialize_mF0462C0CAAB7FF2E2C6A75F5F8769FE5DDDD9E68,
	ParseContext_get_LastTag_mD9E2763BC34A1111408D24771A9AC105C074DD53,
	ParseContext_get_DiscardUnknownFields_mB67E8578452722E0B978BF3D107532C93D3268CB,
	ParseContext_get_ExtensionRegistry_m4B2B015EBC8CA7328C07E164A6671545CCBD0F00,
	ParseContext_ReadTag_mEE687A18078235454AEA63F1F61E9E3E9EB0260F,
	ParseContext_ReadDouble_m8C106CDD420E7FA1311F33B2EAD994E4FD6A99DB,
	ParseContext_ReadFloat_m1B6730B5A824501C0163F2DE6693B2F3B5939BC8,
	ParseContext_ReadUInt64_m4E6FC396669BEAB285DFD47E650004F92AC16A48,
	ParseContext_ReadInt64_mEDF40EF10BA05D0959FAD12976873ACF68163D4D,
	ParseContext_ReadInt32_m275087DD36E27495CC2EE76EEB08BC90EFAF94B9,
	ParseContext_ReadFixed64_m196FF86F1C53A073A179AB5CA93828CDBA3052ED,
	ParseContext_ReadFixed32_m74BD7221FF3B1898D374C4E03EC036C576C677A2,
	ParseContext_ReadBool_m097C5083B8554A5F33B1799DD8BF30926A5942BE,
	ParseContext_ReadString_mA21E24240255C281D36669A209628AD6FDBB15AE,
	ParseContext_ReadMessage_mA72BE697566685D137AE783593B8AB4558090118,
	ParseContext_ReadBytes_mB08BDD0C46819718BA3813A1A943264DEAC283C5,
	ParseContext_ReadUInt32_m961D49845C792A0B46CEA9E5B57FB50390FA5FDE,
	ParseContext_ReadEnum_mB005F6D2C670173280B8EDB79CE4150B83F08374,
	ParseContext_ReadLength_mABDC3E98A6A4A46C6102FF84C40C42A6C1F0EC87,
	ParseContext_CopyStateTo_mC016DFBBF2BAF69F8A740391DA9F3C25BB94E273,
	ParseContext_LoadStateFrom_m74FA6FF020C2458A8787FC3E7643E624517F4CB4,
	ParserInternalState_get_CodedInputStream_m245B3E90A4D6AC379449EA79C3D1A4E2526BD712,
	ParserInternalState_get_DiscardUnknownFields_mABFB91062F6B1A730ABD81691F138C516B0E3020,
	ParserInternalState_set_DiscardUnknownFields_m6374B0D645A0352B3BF53F9B0EFEF62E45501C58,
	ParserInternalState_get_ExtensionRegistry_m8ABCB61553B7732DBB4DB08481D898E5479347CE,
	ParserInternalState_set_ExtensionRegistry_mEA5D5DD0A75713D26A2300501B940EB7A67C2B5C,
	ParsingPrimitives_ParseLength_mC2FC8A77E6BCAB09D6149C6EDF2D080E76EC945F,
	ParsingPrimitives_ParseTag_mF467A1C5FE6962837D3E138E0D2DD3F159EE6A1E,
	ParsingPrimitives_MaybeConsumeTag_m1456930481A43092670E797F496880804BAA1910,
	ParsingPrimitives_PeekTag_m9FF6F1ABC59AA1A572BA0082DDD86EE2B0E2B8E1,
	ParsingPrimitives_ParseRawVarint64_m991C711AC6C490742D2A18D54B518A73603B29CF,
	ParsingPrimitives_ParseRawVarint64SlowPath_m59A67C82B9654A164A896C73076A54FF4E2799BE,
	ParsingPrimitives_ParseRawVarint32_m603BEFB03EAD05D2C5B543DA459AE0E987BB92E7,
	ParsingPrimitives_ParseRawVarint32SlowPath_m204EA9D003AE36F0706720209B029795B26156C7,
	ParsingPrimitives_ParseRawLittleEndian32_m0B7F41846EEB39F5E8D6BCB2C05DD54205F98B64,
	ParsingPrimitives_ParseRawLittleEndian32SlowPath_mF929DE5CB57AC8A142965B9F5907760189973427,
	ParsingPrimitives_ParseRawLittleEndian64_mD2A647B269340B3B4D1981C0A71DF21BA55FD92A,
	ParsingPrimitives_ParseRawLittleEndian64SlowPath_m41E32918988DDFC97EAAF6CB926974A1D15D628A,
	ParsingPrimitives_ParseDouble_m9D7A20CB6804A925788138CC60FC1006300AA3B2,
	ParsingPrimitives_ParseFloat_mC2FA1C7C9EAE7C021684295E4E09120C0BCCC710,
	ParsingPrimitives_ParseFloatSlow_m512EC27CE5ABFDA8C010852A9A9F1024D6247D70,
	ParsingPrimitives_ReadRawBytes_m2D3C496252E736CF124A44AEFF4CF51D95658B45,
	ParsingPrimitives_ReadRawBytesSlow_mBC9FF1D8D69D9C053ACF878CE12FE5AE1D9297AC,
	ParsingPrimitives_SkipRawBytes_m43FF250C33B1B8942ECA81E472045DA849B44FC0,
	ParsingPrimitives_ReadString_mF4065E9BBE004B97FBA2D159B6B175A1876AD495,
	ParsingPrimitives_ReadBytes_mCED56422C2310BB900CF0B6A1F5E8F3F7F5AE214,
	ParsingPrimitives_ReadRawString_mFFD978AA6F9021997AA4154B07D4A5B47ABB345C,
	ParsingPrimitives_ReadStringSlow_mA9EB2AE44F8A093E333B150063E6C936535E8CCB,
	ParsingPrimitives_ValidateCurrentLimit_m9EC4B92BAA5FE179C699A7941D03F634B2D1F5A9,
	ParsingPrimitives_ReadRawByte_m702A011D5ACEEA7CDE6EAFD06005D8989D281C55,
	ParsingPrimitives_IsDataAvailable_mC54DA4458BCF17D18A5DFBE63FFA362CE432E890,
	ParsingPrimitives_IsDataAvailableInSource_m6AA9E21894C6603A481B5914B18258B20BFE2017,
	ParsingPrimitives_ReadRawBytesIntoSpan_m6120704BC8084ED27250B00C5ABB68B03E44A9EA,
	ParsingPrimitivesMessages_SkipLastField_mEDE2B24A023BCD6071A30E2CCA444B11DBDA2449,
	ParsingPrimitivesMessages_SkipGroup_m3CAD89D30DFE66A449C84CC9DAB7CE07B71919B0,
	ParsingPrimitivesMessages_ReadMessage_mF4AB6B44529A994E2FA7D71110BBE1B901D64810,
	NULL,
	ParsingPrimitivesMessages_ReadGroup_m0308867608D14663008DBCDD37F56B38A3AD4AA0,
	ParsingPrimitivesMessages_ReadRawMessage_m5A98B881D178F876BBE20108F4F024E352E0EDFE,
	ParsingPrimitivesMessages_CheckReadEndOfStreamTag_m55AEBE9C00414E376BBBF968B0108225D3FEB981,
	ParsingPrimitivesMessages_CheckLastTagWas_m73D305A8B0749862580196E33B64624D3AB2BB6A,
	ParsingPrimitivesMessages__cctor_m73B1494389EF8A80D92E9C979C08FCCF22C3C5A6,
	NULL,
	NULL,
	SegmentedBufferHelper_Initialize_m63CA047207E9863358F76EBB38BE6A927F1DD991,
	SegmentedBufferHelper_Initialize_mCD7335441CB503495A0248F07E920189DC9E0A09,
	SegmentedBufferHelper_RefillBuffer_m75ABD1D414AA5D56ECB052EF58E151DA8468893F,
	SegmentedBufferHelper_get_TotalLength_m1F9022D9A2F697A6F81FF34ABA719D983C7926E5,
	SegmentedBufferHelper_get_CodedInputStream_m5FF2CFFD3C0EF5E732067FA923D9A1059C51E56E,
	SegmentedBufferHelper_PushLimit_mC9D151CF2449D1652EBD4857A9F986FF71AEC9F2,
	SegmentedBufferHelper_PopLimit_m73669DD916AD6F101DBB5D6DD3ECA2E09EF7F0C8,
	SegmentedBufferHelper_IsReachedLimit_m64CAC04AA2F44A1C3063BBAA9EAE824628C436B8,
	SegmentedBufferHelper_IsAtEnd_mD21ECBDA96B7672A23C47F6C4A4AD1B491E96379,
	SegmentedBufferHelper_RefillFromReadOnlySequence_m6205503EFBA7900C8DC05FB228F499EF57D76910,
	SegmentedBufferHelper_RefillFromCodedInputStream_mD18F57A2D807F9653438B2843E58D82E6B58C04F,
	SegmentedBufferHelper_RecomputeBufferSizeAfterLimit_m0561CCE5954F686F6A6A95AD298DD75B4B86DB43,
	SegmentedBufferHelper_CheckCurrentBufferIsEmpty_mB9810DFFA170A059CF79A7CBBAE03DA3C0A69E5B,
	UnknownField__ctor_m943C5046DD86CAEDEEDCE89DB2490E5D2840E68C,
	UnknownField_Equals_m41D22CC0BBB261BEA6C753E55D97852732F0EBAF,
	UnknownField_GetHashCode_m2545B6879093E416F5C13883EFB68520AEA057BC,
	UnknownField_WriteTo_m90C9A4AF939DA4BC8C3501B7021B5B67BD2EC864,
	UnknownField_GetSerializedSize_m7A2BC6C3337CA40AA2A9C3EAE126FC1100149B5D,
	UnknownField_MergeFrom_m0D5752F3FFEF0E25C9A4CBFF5BC6E1D1B3BD969B,
	NULL,
	UnknownField_AddVarint_m4EEAABA5BD2B2EFBFEFF832409C93CF1A105B4CB,
	UnknownField_AddFixed32_mF341A2670F523D4AC2806E8A89A484E2E0B5F683,
	UnknownField_AddFixed64_mE93303BDACFC97871E7D63C2663AF48CB9FF910F,
	UnknownField_AddLengthDelimited_mD1E59FF116E2BF450D56DEDCA74E4D09E2AA5B70,
	UnknownField_AddGroup_m2AC9A7F4F7D9074CCEFE54B53A8D8D55ABD40305,
	NULL,
	UnknownFieldSet__ctor_m4E14CBE1BB8E5B7C16412C33E43CC11A604DD6EB,
	UnknownFieldSet_HasField_mA80A58FCB11938C8403A3140642D0F203AD82CD4,
	UnknownFieldSet_WriteTo_m9A480B637780AE4EC7FE1C788273D73E6C267795,
	UnknownFieldSet_WriteTo_mECF1EFA7AF68EBBEDDD046865B2C17131B969E1B,
	UnknownFieldSet_CalculateSize_m0F48042AFDC7E6795E7A99D8B3BE0D5C61C96868,
	UnknownFieldSet_Equals_m999BB91F13D3256C77F0C7A39E49793AF64E6595,
	UnknownFieldSet_GetHashCode_m6DAE03F0C0E9F6228751D8538C9734AE339ABD19,
	UnknownFieldSet_GetOrAddField_m7B963B5E9DCB2579EAC0CC4E556FD83619441ECA,
	UnknownFieldSet_AddOrReplaceField_m68E9F4093CD33361F77B5B5B0B7B9B2BE8FC26A3,
	UnknownFieldSet_MergeFieldFrom_m561379A6714FBE8A08B68BA4FB6CD6EB159BADF1,
	UnknownFieldSet_MergeGroupFrom_m5EFE89A8E12412A49792ED4CF1DB22FA77BBCE39,
	UnknownFieldSet_MergeFieldFrom_m4655DE968A5275DA43F2968F08B58BDB8521B051,
	UnknownFieldSet_MergeFieldFrom_m6D9E32ADBB0194941C65CD43522F53C3A2378283,
	UnknownFieldSet_MergeFrom_mB5736E4A0F2A45787555D2B0C98F0B531F9778AF,
	UnknownFieldSet_MergeFrom_m3175C4FF04375A5EF7DC4E3245C6606303344F85,
	UnknownFieldSet_MergeField_m2E0EDB53398818DF68D494A47321CD1085C57A65,
	UnknownFieldSet_Clone_mBCB3196178FBF9E17BF3C686C6C6DC214D0BBDEA,
	WireFormat_GetTagWireType_m367E6B9AB0C0534F19A35A2574A55759A139510B,
	WireFormat_GetTagFieldNumber_mDF369E25A14A2B12311F243DA9E68B25DC1DFB76,
	WireFormat_MakeTag_mA074D84171D39ACFB2BB7BB2DA85945DCE23B576,
	WriteBufferHelper_get_CodedOutputStream_m92805AD7A4248AFD24342FE0F3AF1ECA9E67CAB1,
	WriteBufferHelper_Initialize_m5CDEEE39143C13C043A378606AD78D32A8F7E2F3,
	WriteBufferHelper_CheckNoSpaceLeft_mE97B4E30DDB343E7E28F0301A49C9C9CA4DA1ECE,
	WriteBufferHelper_GetSpaceLeft_m0A47B45340528CDB6E26BFA4776FB7A0507C7258,
	WriteBufferHelper_RefreshBuffer_mFB6576A94800AC1C6A210B3D45265C2CC3074AC1,
	WriteBufferHelper_Flush_mBA2753A178AE3C8D13AE06D39910DEEC468C595A,
	WriteContext_Initialize_m76A276505C625A28A7FE936BF62FFA661235E53F,
	WriteContext_Initialize_mBAAD3A83874DB7991E24AF783A983CC9204C89BD,
	WriteContext_WriteDouble_m36DE9708C2E9F55E08DF1593168026CD929B4114,
	WriteContext_WriteFloat_m36D978A87046CB34CE1857EAE1079DD9B8972248,
	WriteContext_WriteUInt64_mE585A8A413E7581324D6B61B063DAB8CD88D4383,
	WriteContext_WriteInt64_m20616F89120F9551E46B5074B6C14823C97D0C4F,
	WriteContext_WriteInt32_mCC3646C9CF9A13E2BEBC15A8E566669C913D4F65,
	WriteContext_WriteFixed64_mAE849DB91093D95337CB4164CA83C8E2B25C7716,
	WriteContext_WriteFixed32_mECD70B0022C1DFBE81B61366495335BE6B297AF8,
	WriteContext_WriteBool_m68B7D58774A48FFC3FF34D9A6F770E579EEFA7DF,
	WriteContext_WriteString_m1A0F46C5CF383B3F976BFBD24B546EC2E346C68C,
	WriteContext_WriteMessage_m191D2C3E865C097E18DA174B172FFFA9685C8B98,
	WriteContext_WriteBytes_m6483E64A6655BB2E1499AD548C549B47FB5EB3E5,
	WriteContext_WriteUInt32_mA6366289E56066C7C0AB06218534DCF1F5C75353,
	WriteContext_WriteEnum_mE9C54A63A11FA8F28864359CE2236760B7C7AABC,
	WriteContext_WriteLength_mEBAD2E61EA0B60DD8789138399357B2E42C7BE4F,
	WriteContext_WriteTag_m610740BFC2CB1D051CF1DCC94FFE749C801584C9,
	WriteContext_WriteTag_m6041201C0BF3CBBCE75D5DD21474E69BC4CD80EF,
	WriteContext_WriteRawTag_m3E37F0B83B4E8B7C7E1CC6472D527AA22B11DA5A,
	WriteContext_WriteRawTag_m3889E0A7DCDBEE6B057EECBB614B022B4D4F991F,
	WriteContext_CopyStateTo_mB67D2A3DB1B06B506C65BEDC5CC7EC1FC3A3FB97,
	WriteContext_LoadStateFrom_mF406ADA99F6893BA926A0AD8950B8A8EF85F2D7A,
	WriterInternalState_get_CodedOutputStream_m3601FE09E7D068C19BBEE6B3DEAB889130C983C3,
	WritingPrimitives_WriteDouble_mF3C3599EC5A2C8341CEDB1D9410EBAA66816BEA7,
	WritingPrimitives_WriteFloat_mEA7B691A96886F04E18865F4BDC0E584599C7E39,
	WritingPrimitives_WriteFloatSlowPath_mA8E622A32E1170F3E8F595013DAB8AD141091709,
	WritingPrimitives_WriteUInt64_m7179598033A8A18131EE39680B1F547B0C9E37F7,
	WritingPrimitives_WriteInt64_mADE3CEBA230A0A86A0AC21D71ED8FE1674A96EB7,
	WritingPrimitives_WriteInt32_m7DACD767626E22F6955E9371B451DECE85D647E1,
	WritingPrimitives_WriteFixed64_m5BE77A057DF8A002691684B96C2B7A954CE85ECA,
	WritingPrimitives_WriteFixed32_m264BBE74CE53ADFA5280F3FEB396710C3D49EE93,
	WritingPrimitives_WriteBool_mFC8A6B5C0260575DB6A581918C0ABD9F6EFC136A,
	WritingPrimitives_WriteString_m9D7752CCBA30C9C42C8ECCBD03FD029A2240144A,
	WritingPrimitives_WriteStringToBuffer_m1B44348AEAF67F075423A8BA4D955A48ABF182C7,
	WritingPrimitives_WriteBytes_mC0B23B56CA938CAF6807BC7170860955410781F4,
	WritingPrimitives_WriteUInt32_m44AADF2F1DAE5ACFB8B982887AABC369DEBB7144,
	WritingPrimitives_WriteEnum_mE98FE36561BCAD8F86524064953A5661E3D03BED,
	WritingPrimitives_WriteLength_mFCCCDC6E637B15A5CF626E80D487D94E3FEAB5AE,
	WritingPrimitives_WriteRawVarint32_m80E7B1B2014AA3D0E50550EF7BC424B22426EAAB,
	WritingPrimitives_WriteRawVarint64_m209CE9059AE1427F11B53D9DC04ACFD3406D51DB,
	WritingPrimitives_WriteRawLittleEndian32_mF720ADD7B8D2EFA7D76910B02F780C49B7BCB697,
	WritingPrimitives_WriteRawLittleEndian32SlowPath_mC991B66C50349FE8EC5F0AF5D65191C877333247,
	WritingPrimitives_WriteRawLittleEndian64_m8CC508077D42C814112B1E80E1525711F2774AD3,
	WritingPrimitives_WriteRawLittleEndian64SlowPath_mA4B4B0D9EFF6845D4BFAA835FF25DFB3CFDA9A29,
	WritingPrimitives_WriteRawByte_m09FE0FCC63987E7FC325D0785E5BAE55E11E6C27,
	WritingPrimitives_WriteRawBytes_m82F533CAB5C7A6EAA9238F1993BC6AF14E8E54BC,
	WritingPrimitives_WriteRawBytes_m6BD7D272545E097DCA9B59C15526972EB1BE4E6A,
	WritingPrimitives_WriteTag_m20DA49695454AA1E65B7192B8DFA90C44F30152F,
	WritingPrimitives_WriteTag_m4A875B305FF92E29D8BE78A347A0962ED5AE0AB1,
	WritingPrimitives_WriteRawTag_mDEBC5CD4ACEEEB1E4ACA80622176C86F08DE7BF8,
	WritingPrimitives_WriteRawTag_m6ECCDE65477C117CCCD301F38187D73D0DE7258C,
	WritingPrimitives_WriteRawTagSlowPath_m52271A35E5027D4FCB8011C5DFC4062BA196E2D4,
	WritingPrimitives__cctor_mC944CE893A1231A22BB210A15CFACBEDCFEAA651,
	WritingPrimitivesMessages_WriteMessage_m9A9AA7223ABB3C3F21018D0F85F504736B07D85E,
	WritingPrimitivesMessages_WriteRawMessage_m466A1428FF61276D3E990BA5B2EE64810303EB5E,
	AnyReflection_get_Descriptor_mF58E048143D4F90346366A71E0427335CDD98736,
	AnyReflection__cctor_m28799E694101DB1E808027349EBDC740DCF1ABCF,
	Any_get_Parser_mE797270D14D0703DA6E8BA1F9074B4DD42CDE9B0,
	Any_get_Descriptor_mB044C3483527D965D4203FA88CCDB8AB59FAF624,
	Any_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m20D87828F043DEC39805FF4047C49A00DA10216F,
	Any__ctor_m1ECC62A42AB856A583601A266C7C8CF336408E90,
	Any__ctor_m2376F8C7176FFA131593622A480CE332EB4B1B13,
	Any_Clone_m0DED49DFACD7ED97904D1D4290A65A1C494FDA6F,
	Any_get_TypeUrl_mC45F8AE94967D3FB99B564A4ADA96205BC80DF1E,
	Any_set_TypeUrl_m8205CD3987662353B2D5C97ABE5B667789763FD4,
	Any_get_Value_mDC67D93A2B4FD2412CA75536675294D04CAE5A4D,
	Any_set_Value_m40B44205CB793BDC16154F620CFED4F55272E677,
	Any_Equals_mD55E6718944CD8AA56B9AFC8B9B584BE6E4BEF43,
	Any_Equals_m39E1D5E36B954E909B5F6C5D904430B5064AB976,
	Any_GetHashCode_m07B1193DDB01F335CE42B217AE9BD3A88C6848FB,
	Any_ToString_m0A75195213C8DF63A33CA3775B9684EEED9E9749,
	Any_WriteTo_m4F0C9449ECFD97252B6CC7090CACD8A532BD2BFB,
	Any_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m1A5C5A2DE6524DE3A5A86FB9E4AD5422AFA61562,
	Any_CalculateSize_m0CC466C0EA20B4192A897E77F5AEDD4E22E6B9A4,
	Any_MergeFrom_mB3B78279933A5AA5BA4B45E7EB58A4F133BF7177,
	Any_MergeFrom_mBFB2D9E1EC1825E910098EDFEA6547ABB949BECD,
	Any_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_mF51F0CE65F862C0DCAF37AD560D439D63DB570CB,
	Any_GetTypeName_m981156327D46656C8E8819D94B7A496DD264B75B,
	Any__cctor_m990153EEC7FFADE95312E6AE3F5032DBDD7DD41C,
	U3CU3Ec__cctor_m355B62D4F25CCE2AC973FBE0B7EAE1BD827FE914,
	U3CU3Ec__ctor_m498B298BE961BF12E0B8C3F49D48537FB0B0E4B7,
	U3CU3Ec_U3C_cctorU3Eb__40_0_m1427A4A9EAD998C31A791AF9996DD999A8382DF7,
	DurationReflection_get_Descriptor_mD740650A11CBA770313496B8D700061E0BE35E46,
	DurationReflection__cctor_m02F448AF98215704F1B577286E2A9114EA6F285D,
	Duration_get_Parser_m96C08CB52416A21B7C5B93F359A6EF4F3BC6AF7B,
	Duration_get_Descriptor_m8B4F6425E8541BA5F1FACD8B8E0846BD3E5DA409,
	Duration_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m12A6EE9725228581AA665137F9C37BC865C9820B,
	Duration__ctor_m6E8317977C3ABF38F7C0DF31CF9FD578DB563EEA,
	Duration__ctor_mE32B96FC0271D3892E7E9B684B384787EF78E56D,
	Duration_Clone_mF481B40FEAFC57DDFA82F9357B1E7BCB2A5645EE,
	Duration_get_Seconds_m1B8C2E4131812F6CE8BC4E4592274CEA4AFCFAE0,
	Duration_set_Seconds_m4D56CEC9CFED641A03BFEE1EB411D4CCECB3819A,
	Duration_get_Nanos_m9AF2BD58D223A08F2179036D4E64C2421894744B,
	Duration_set_Nanos_m097E8310B0000815582B89BB0C445E14B81161E8,
	Duration_Equals_mC8E45E2881848B04EF30A2A9F507A755840D8895,
	Duration_Equals_m1BE5DD19D22AF8643B4CE200DE7F7F79853D4292,
	Duration_GetHashCode_m9177E77337F424B4F7B8895403B9E472EF1C98D8,
	Duration_ToString_m9A4C19C321E83FD1E47AC98C2F8A2513B2580900,
	Duration_WriteTo_mBB57F86544E6C7F06853460506A3C7534FFDBC5D,
	Duration_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_mBA1C04DAEA64BD35140B8B97E305116436D657A3,
	Duration_CalculateSize_m9E15FCF69AB0CE774A486CB3C0B5C9AA6417EABF,
	Duration_MergeFrom_m5815E64B100ABE517B1AF02E6EEEE2F9937C6EE9,
	Duration_MergeFrom_m4150356C75EC52495E8CF0B15F5A1832ACBE8F8A,
	Duration_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m03941E2F76DE634FD45F0AD9FF80E8FDD8FC8B3C,
	Duration_IsNormalized_m279FFDCFEBAC7D93B9F1D5F0BDDAD859EC9534DE,
	Duration_ToJson_m4921C26255D3E10F2B76FBB66910931EAE20AF1B,
	Duration_ToDiagnosticString_m6AD61CDA4BE97EC1FE5EBB280E73952CC57D6653,
	Duration_AppendNanoseconds_mD3CEC9D0A01B513B37B78C2AE9A050258DF321EC,
	Duration__cctor_m0B409DD121AEF904DF5695FE6605B94974D23BB9,
	U3CU3Ec__cctor_mE11DB05928D0D50178F0DB0C963950E9807D3E81,
	U3CU3Ec__ctor_mF4755084B094EBF7BA7BF27974F8FF04CFF5E757,
	U3CU3Ec_U3C_cctorU3Eb__48_0_m3CCA2718FE30E9EDE4182FF9A118D0FA3340C12A,
	FieldMaskReflection_get_Descriptor_m71513B57323612637E03F04DB3D3F972DF5342A3,
	FieldMaskReflection__cctor_m19AFD86FFAEF23703D49F490A5C39D9CB9AEBA83,
	FieldMask_get_Parser_m7D7346798414A3449234B1A840A06A3E3F682EC4,
	FieldMask_get_Descriptor_mEC2F8CEAD4DEC85FF8C020F7DAA36C267E75C63E,
	FieldMask_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m0FEDF588A4B0370EF55B76E856B263560DA23052,
	FieldMask__ctor_m01D7985ADE2286627A933B2F86237E0C8483D3D5,
	FieldMask__ctor_m77703B996B4EAD8BD22AA0AC7E4C9C768137D853,
	FieldMask_Clone_m2EDD32CF5E0AC140742096AEFE1DAE3ED453CCA3,
	FieldMask_get_Paths_mF5E927D6A5853483850A5B1B0A60F6EA4C6419EF,
	FieldMask_Equals_m1B5C027B9B0A1A66D8B177DAAB4EDBD5FD1442C1,
	FieldMask_Equals_mC23F829C421F954D073A6C046B614DBB687E41AB,
	FieldMask_GetHashCode_mA97105104D19E8CF467DE10EAFC05DC28B86F2D2,
	FieldMask_ToString_m89F5E655ABCBA6ACC9D9B86BE367D9919A837036,
	FieldMask_WriteTo_mE9F97A05551D2F6FBCDF475C259D1BEE4B9ECA43,
	FieldMask_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m663519CE13FFC51F40560EE1C564F4C017133C59,
	FieldMask_CalculateSize_m0A39301CC699EEA0A57D8D8E4846C58F734D8EA5,
	FieldMask_MergeFrom_mEAF05299B243344C2E5FFF1D601F5EA18BAB9F00,
	FieldMask_MergeFrom_mBB29E5E3787C88D4F6813175DF0F3D7E5F8E1E0C,
	FieldMask_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_mA91B89E0519001C72B2CF796A4969D2D87CE04DB,
	FieldMask_ToJson_m7B14EB25AF30592117FF785EDAD96AFECB0ABB5D,
	FieldMask_ToDiagnosticString_mF6F76E2063C7124C4A7C83A930C71E6EE09BAE27,
	FieldMask_IsPathValid_m6E09EDDDA021553F4AFACCE1034F616DCDFE92EC,
	FieldMask__cctor_mB5A9DE16B215D884D58AF3EDA652FC5573BD2F99,
	U3CU3Ec__cctor_mA4CF16058DFB4D2F9A57E170E1B3147351F91F3F,
	U3CU3Ec__ctor_mC3697FB080C4E30E48E773923788967EFA424DD5,
	U3CU3Ec_U3CToJsonU3Eb__29_0_m2073565A3A39EBE2AEDE128834375CCF6657C339,
	U3CU3Ec_U3C_cctorU3Eb__47_0_mA4E7E39FB01ECA1205A724B224B541D316975F0F,
	SourceContextReflection_get_Descriptor_m4009946C4A2FF4A97821E806EFB14F3B68EC82A4,
	SourceContextReflection__cctor_m7E875D9DD71E34023DDA5F3D2A6922F0E4B2C4E6,
	SourceContext_get_Parser_m1574934F37EB77DFE5197AFB3579F5180374C54F,
	SourceContext_get_Descriptor_mD96EC1A495DB470D7206A021731449612F0B7700,
	SourceContext_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m533FF400CC76FC6D0972E7AB266DCF102BF07F99,
	SourceContext__ctor_m7DC6A0CEA2DB342A90B3111F54EE7FE20035ED7A,
	SourceContext__ctor_m53B1A691B9A243427E519584FAA277F82940830C,
	SourceContext_Clone_mECE4CC09F0CFD276692AED613523558C5B35FF13,
	SourceContext_get_FileName_m7955978D6CF0C518E8E4AB97CCF95E667224F9BF,
	SourceContext_set_FileName_m5E18A613284370873EB32929CFDBBBC9A9FF250A,
	SourceContext_Equals_m2404E1B68FD460455E54DD5DA372BA7DD10A56A5,
	SourceContext_Equals_mDA27BD7606F7EA489A2C1AE54476ED5E0E357143,
	SourceContext_GetHashCode_m3AF81E3DD69DB21BF3B9DB93411901F8F0C4C4D9,
	SourceContext_ToString_mCC00619AD401EE2827C8F568E6798478F398F6F1,
	SourceContext_WriteTo_m29132C1DE0B5144E19D8889A192E79AEDDABA8ED,
	SourceContext_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m2C30589A2E794D1D6F5D64B6DC35C253E7261066,
	SourceContext_CalculateSize_mC5AD31CF54346C37E07C001C6A2BD8DCEC68F1F6,
	SourceContext_MergeFrom_m02826B16AA69141109E52E02757E9B1AF009ACD4,
	SourceContext_MergeFrom_m052D6EAFE9072262116999725700C7FF8FCB45C1,
	SourceContext_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_mADDDB228A85C7C3C457347DC2D13075031FD4271,
	SourceContext__cctor_mD983F610D248578C99A11563DDDBE4D2940DE730,
	U3CU3Ec__cctor_m3D6899B50C7A5F6C071FAE8BD11101C3D0D070A7,
	U3CU3Ec__ctor_mDD9A1C66DBE28C23EC3A96BFE489549611D7C29E,
	U3CU3Ec_U3C_cctorU3Eb__27_0_m06320485F3F600B4D8A2B285584AB45C2A582B41,
	StructReflection_get_Descriptor_m9748598A8A6401A3A22220952736699A6A66816D,
	StructReflection__cctor_m6F9759FF12CAB6290E72D0996F40CB7A3FAF93FB,
	Struct_get_Parser_m250728F6C5651FF2FA35C5FE72A4B83E25B2076B,
	Struct_get_Descriptor_mAC89E1D144BC3DF0BD50971C23664642ACFD7225,
	Struct_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_mAF8AA7343F0F82ADAFC28730687E6707C98252AD,
	Struct__ctor_m11D17B249FA390528B0E14F386ADA2577C6A871C,
	Struct__ctor_m608179184571F7E0C2F5879E37CF80A00B2B8D7A,
	Struct_Clone_m0D1BB90781C14E60D625538A9A19D696120475BE,
	Struct_get_Fields_m8D843586CA5DDAE522D036C881C8E28AFCD591FD,
	Struct_Equals_m549285D67C027225573A1DB67C5301A0D6454BFA,
	Struct_Equals_m2532A70D51E9BAB2D6D51B68E969EF10BDA2F728,
	Struct_GetHashCode_mB35A1295AA470E34F0B3283C926A85E1DC10C61E,
	Struct_ToString_mB7E3408DD1CC7EA7EF64A6C0AAF69344623328B9,
	Struct_WriteTo_mB8C1FE19A7AD7FA903647D06FF0DCF1970A32D29,
	Struct_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_mB4878B03C7F2037F3A6A0B017A57F95D785F0B7F,
	Struct_CalculateSize_m04764057A3243E48A65E84C497924FE3AE6BEDDC,
	Struct_MergeFrom_m6AA74D28AA5FDA2211BE8FB37B3E8709136F90B1,
	Struct_MergeFrom_m6DA60767E2F54627C88F498B3F3D2348F0908D17,
	Struct_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m48DE2FD28260E4F6EB713D4B2309D7ECC5A3DCAF,
	Struct__cctor_m0E60641DC488D27979DE826B0D5CB378FB70E69C,
	U3CU3Ec__cctor_mC1AD226A66AB425A66E1587BE428E136B50C8037,
	U3CU3Ec__ctor_mB1D8AC612C944EB09A92F8DFEF18F8B4D614795E,
	U3CU3Ec_U3C_cctorU3Eb__27_0_m23EDA83D004C7EE7CDBBBF148315FB5ED05C3A99,
	Value_get_Parser_m7B68E45F03000960D21749A4F65984B745CF0C8F,
	Value_get_Descriptor_m60C564DCB4C1B6CD4CE32F32BE60C3353080E5C3,
	Value_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m41249C534B0CE2FB46D2B174A3F694661FE5FB27,
	Value__ctor_mB36B9A9D61FC019A45BC871BA1478BB50D60E78B,
	Value__ctor_m44F71F1C4FF2ED09299528B51BDBA4E73788D40E,
	Value_Clone_mACDC4288492E8D9B24FE707A400B1445AD78477F,
	Value_get_NullValue_m33417C21F46BA869D3CB5DA6E493E3E5E1E3D3C1,
	Value_set_NullValue_mDF8668260C9D9D9D92DEBBA19243B502592D2C29,
	Value_get_NumberValue_mDB3E2517F4811344E4D3DB5A76F7BEBC614A36DD,
	Value_set_NumberValue_m53F97D99BE56BDFBBA550A9184E331887C8486DB,
	Value_get_StringValue_mFA3132FC715755E37C15FE78B3DD7D0CD57BF3A1,
	Value_set_StringValue_mD9494834E589A83005BBF23FE2CCAD73404792A9,
	Value_get_BoolValue_mBCE7513492A5A9F3468F567A3D8D86354DEBB530,
	Value_set_BoolValue_m5C00C8E2B41399FB01CE052AE60F2BD412A0383C,
	Value_get_StructValue_m76244616B9DEDEDD4B418AB47BA65E0747C9A3FD,
	Value_set_StructValue_mA58EBA2260E35A3750A339B58D5EFB04A4360A81,
	Value_get_ListValue_m7BE63C1AA6FA57D40493647A0B07A8B2E7D63F17,
	Value_set_ListValue_mB3FDD1F950984E0E98B0D2597E1FED0BB4D357AD,
	Value_get_KindCase_m1C58B2C15BF26E2CF7D9FF1F5AF4379AACAD2168,
	Value_Equals_mE007E6C81CFC39B3A5BA78873297DB9C2305C650,
	Value_Equals_m5F2BB40CD83A5988F958848F4410366EB7EC59B4,
	Value_GetHashCode_m7C544251FD75746E81866D5D36924010535124E6,
	Value_ToString_m0435FF1515A70A900374F24375010C41124BFFC1,
	Value_WriteTo_m707E9B65D4E8B28CA233D2CC91203558071F8147,
	Value_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m8F8E38353725C50938560EC4F37B5F22C75BA7FC,
	Value_CalculateSize_m0249098B9D7B5CB6074121B23B55E79FFB7FC9BA,
	Value_MergeFrom_mFFA75CAC017BBD75F8C0E116E299BDAC427EF1C5,
	Value_MergeFrom_mDFF91157418DCFA0541BAC4E77C0D8E24CCDC62B,
	Value_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_mE15A6602E4EDEAB62505CC0E7AD868CBD7CDE78C,
	Value__cctor_mB7C40B765A99BCB40610807AAA0F0FD626DCEA8F,
	U3CU3Ec__cctor_m59B9CF586DF0FA99047815487D0C45215B9D2D4A,
	U3CU3Ec__ctor_mE7FB7C6DB3D332EBFC708DFC047192998E989C77,
	U3CU3Ec_U3C_cctorU3Eb__58_0_m6FD67B6209A03F37CF4376D60D84925808D9BC56,
	ListValue_get_Parser_mF35481E4B4A79E9E57529C57D6CC78DE990229F2,
	ListValue_get_Descriptor_mB366D6ABF5EFF98C61052A9690352D031A99D74B,
	ListValue_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m9200CA4ED976F0B2E5AF6FC25C3FF5606F454789,
	ListValue__ctor_mB9FA676E88F0EA554534D3FFCBECA11AC459A22A,
	ListValue__ctor_m28B1D38DB2026931E319F255DEF52941857CE536,
	ListValue_Clone_mAE7626FCDDF9C2C6BA65B98C47E483B8C84E800A,
	ListValue_Equals_mF92FCAB22A5B02906FEFBA93776FDA4996C0B65B,
	ListValue_Equals_mAFE9E0D31D3CFEE41502588A065E6B9ED7C0663F,
	ListValue_GetHashCode_mBAB9D28DF3E73B510E816CFF5C20774383DDA53B,
	ListValue_ToString_mE555FF95A4A538FD306A7DBD94DA019BCC737EDB,
	ListValue_WriteTo_m2200555A25434B19BE50039FFD72B23E104A4A11,
	ListValue_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_mC52AE62570621C7C4CA377BD233B78BCBC0178FE,
	ListValue_CalculateSize_m997191D513733E472C8705995F87C8119B6FDADC,
	ListValue_MergeFrom_mFD20BFF52DA1BFB3F54CC90BE5674A118FC5177B,
	ListValue_MergeFrom_m384866A7A963FFBF044B0FC082ADD6A9AA6A4CE2,
	ListValue_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m5E6C1BE581E812B32831F486A67887D377AE6EED,
	ListValue__cctor_m0E41B102D22E607B1EA5C7CA55F6E3041002DE90,
	U3CU3Ec__cctor_mECD0F9D51B179FB8BC3BCB8F49238DDE6F0BAA53,
	U3CU3Ec__ctor_mBA74624BCF703F8B4693D839AD66466AD76A5CDE,
	U3CU3Ec_U3C_cctorU3Eb__27_0_m505FD8C4C78D9B6614DCD355F93C53E994627CFB,
	TimestampReflection_get_Descriptor_m20CC9712B17B7939EDE1079E357BC1DCA4915837,
	TimestampReflection__cctor_m34CFC760B3D2D93C368E2391968195E98FBEACDF,
	Timestamp_get_Parser_mEB68C1AFD68769CCF297A05506CDA0D3FB5E3574,
	Timestamp_get_Descriptor_mC252E84951D63F4CB2F1A95863C073EC537EFF47,
	Timestamp_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_mC49EAA8BC8FD246B54506B483EE810FD9441CE11,
	Timestamp__ctor_m8F321B7F87FD2782A209D5251E2509121F91E084,
	Timestamp__ctor_m2664E78CE259B3480E632DA374D7295D9511E644,
	Timestamp_Clone_mBF7E47B82DDCD226CD188726A48B380D7340F063,
	Timestamp_get_Seconds_m5DDC0421C4D27B8326BE43F633784C49EB9AD20B,
	Timestamp_set_Seconds_mCD77AE40724D2522C2658BFB55DD79BCB159F6D2,
	Timestamp_get_Nanos_m9E3C3DC947E927EA42D3301BB019081E9A8C7306,
	Timestamp_set_Nanos_mB8E86307F7E57CC2A2EBBD2E8EC2766DE9F36D3D,
	Timestamp_Equals_m43A5485ADA83370447C6ED05E83D93E9FD61F075,
	Timestamp_Equals_m09CB0CAAD41410377A04A2A6B6F93E232A2FD558,
	Timestamp_GetHashCode_m4EF62C49B8D0D43BDCDF3AE4115D63442DCB6AF7,
	Timestamp_ToString_m54CE7B61A60BD4FBE9FA386E69B256FD9CAB0766,
	Timestamp_WriteTo_m7A41B3EBAEDF1DB8EBA83393E27C1D47642EF1E6,
	Timestamp_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m931F0E47EBF9936D807F0165935BE4D7B4DD6D33,
	Timestamp_CalculateSize_mB8AA31386FAA6594A82EBFDA83023F01849EBA42,
	Timestamp_MergeFrom_mB073280D4CC7A8E531F5BE75B1390589537790A4,
	Timestamp_MergeFrom_m47E1E135BC450678854DD2704A26607F69516BBF,
	Timestamp_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m667DF5C1AC94559C80FE08DC714B1E2943D6C16D,
	Timestamp_IsNormalized_m4D15C2D1F00FDDAA057FA921F0C2C746E8FB3D0C,
	Timestamp_ToJson_m2DB90513A2B4A7A8D917CA8AE9AE619A77B4D366,
	Timestamp_CompareTo_m2C4306ADBFD6885786F776FE9115C6AE6A099223,
	Timestamp_op_Equality_m919F45BB63A456B53460149D13F67F6F663E2767,
	Timestamp_ToDiagnosticString_mCF6DE1C0C81B5A880A9D2FCFEE9BA533253765B9,
	Timestamp__cctor_mE1E881B2E3510FE2BD42514770885507578A4689,
	U3CU3Ec__cctor_mC6E4A227DA90B6F08D288C8836C1EFF4ED6E3C3C,
	U3CU3Ec__ctor_mA9A64A19A1BDD2B0BD230CE1FCB3BA203A732FE8,
	U3CU3Ec_U3C_cctorU3Eb__55_0_m6171D84FAC3F3F77F585EB62B3CEEE8F6E061AEC,
	TypeReflection_get_Descriptor_mDDBF6D9739EFCD93767D247825BAE30238FA760F,
	TypeReflection__cctor_mAC3FAA240EBDEAA68EF3BBD22A83FC2AB2ED6549,
	Type_get_Parser_m8F54270ED12210981D9A59EDB8FECD375119340E,
	Type_get_Descriptor_m010BB83A3404B9825799F2A806CAE4F6F7ECC12A,
	Type_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m67F408433B377CF6DDEFFAC42FF4C6D7E5936480,
	Type__ctor_m72B7DAE9051977572D45AFCF5B543AEB9E84A71F,
	Type__ctor_m7E6669C7E329257A31BA69B14587D6D7739C49DE,
	Type_Clone_m45297B0AFB39C2C32EDE75B17513877EAE97D1CC,
	Type_get_Name_m1A7122FBF1A82ED55AE6925A636ED6A9030EC858,
	Type_set_Name_m055734A2F94B05FA2084C169C5230EBD987092E5,
	Type_get_SourceContext_m07B1EE8D911DC970ADBFBCCD9BDDF457CB5BB088,
	Type_set_SourceContext_m48BB05140D6C16D5A27BE26DE40F42C020796D57,
	Type_get_Syntax_m3F1F54FABD4233B8D23876EAC71561C12DB84CD8,
	Type_set_Syntax_m86D67BE2C739303FF8A46891FB7692EC0D9DB068,
	Type_Equals_mCC222586E6D1B96613944E3D66BEC72D362D9D63,
	Type_Equals_mE4A16EB9E9DA3DD6BBCFD983D5E76A3B7EF7699B,
	Type_GetHashCode_m620763684AFC519635D83A5038D9874C8D840B43,
	Type_ToString_m220D186C08F0D2510BEBAD9ADA9B2AFC9D9C7A2B,
	Type_WriteTo_mEAC9994F15ADD598116489C2F5DC26A213181E74,
	Type_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_mF4AB433048B17B8767AE79040EDF944BF398D8C4,
	Type_CalculateSize_m5AE90542655AB79BC6184ACB7A226E0D004371AA,
	Type_MergeFrom_m3128264DCA457EBEF76A421739ECF34A8244D3F3,
	Type_MergeFrom_m71E0740B14181FDFFDA3FE9A92DE72D39471830D,
	Type_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_mEC3240A4B5DB410713B7697F2A15EA65B3C67EDB,
	Type__cctor_m9790E14F764F18462B2EEA0E074CB98EB7D962CE,
	U3CU3Ec__cctor_m29AE8A35B39754F4FC0EB5535A0AF62EF2A0CDF3,
	U3CU3Ec__ctor_m2909DED14707E03A6BCAAFCC13683F51C6251537,
	U3CU3Ec_U3C_cctorU3Eb__52_0_mEB6B60B797080A26002C324ACB518D8345E8E0CB,
	Field_get_Parser_m0878925481143310E7B3AA20B796B5A97E500CF2,
	Field_get_Descriptor_m531F22D378B22CF604660266902F8DF5C3D24C18,
	Field_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m391F7C85EB83922F66C82433215878208DF9F2A9,
	Field__ctor_m083271BB3DE4449286983AFE69B71C9DE2D68BE8,
	Field__ctor_mF445407AC817770B1DD11ED1FC30695C18F0B76B,
	Field_Clone_m0CC33BAC52277DA85D019E632E4BEF45987DF7D7,
	Field_get_Kind_mD2F491FBD5BBD85968D52EBCB628569218D4BA60,
	Field_set_Kind_m078446AC9EF3821A7F538B3BF32006B4A588DB69,
	Field_get_Cardinality_mF2A01FCE7EB6AD4890B2AD0DC697A43A86463DE2,
	Field_set_Cardinality_m58FB9906ECC195083674F7FCD0A794339F42DFAF,
	Field_get_Number_m5CA026EC04ACB1477820127E99B4352082BF716E,
	Field_set_Number_mA3C7411AEA18DF70E16CAC6978C4DDA71B067D31,
	Field_get_Name_mAAD601EAA2275330987C83DFDAF0052AEC4CBEA1,
	Field_set_Name_m33136B6931B53DF86103C46D793B6FCF9995FE0A,
	Field_get_TypeUrl_m2528DB874C9BF6493DEB2660EF426461EB24B228,
	Field_set_TypeUrl_mA737116E340BBD9BFAC30AE9B6F95055D9F56541,
	Field_get_OneofIndex_m08967CEA183858F72EAFBCEF093F8253F0E67735,
	Field_set_OneofIndex_mC794988564E6E6C198EDB5EE071792D7F39E79DA,
	Field_get_Packed_m3FC4C3AF743AA04C3751A975B8753EE8BEB688C7,
	Field_set_Packed_mC8EAB7B677B01394AB13FB37CC9C3FD1B4A7AC13,
	Field_get_JsonName_m44ACFC8356E38FB505DD382140938FDD14204562,
	Field_set_JsonName_m2AC60C78FE3F665C34907371251489860497AB55,
	Field_get_DefaultValue_mAE46BF6A0D3C4A39D237531D13D0B0AE33994EDE,
	Field_set_DefaultValue_mA111AEFBED7BB95C17F50ED21F5F0CEFD66F2451,
	Field_Equals_m4954B931DEB49176652C38AC8B8047EB9CF2D083,
	Field_Equals_m98EF1C08EB6139CFAC12DDAF6587098D0965F8B8,
	Field_GetHashCode_m9920F826AB54CAC69212B120FA5196570B2EBF0F,
	Field_ToString_mC417A2D84A15530EB338A880F26CAF5B899096D1,
	Field_WriteTo_mE97048ABCAD56F48771C6005B929D1D16822F5F9,
	Field_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m1403C5B7A9323AAEEE2ECD2E2282CDADD1D3EBBA,
	Field_CalculateSize_m29958B0C393E23281EEBB33DE7F86329A78120EA,
	Field_MergeFrom_m79C0F4297EB25A155F74B1D4FD88149786994F5A,
	Field_MergeFrom_m9C1775ED4AE6363E980B965D39DE0651A2A30AA8,
	Field_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_mDCEE2693E502312C726C28AB4F137EAC8AB12640,
	Field__cctor_mE41D2600BF86F2B33C8D4EE32BF1BE0531EC8C80,
	U3CU3Ec__cctor_m242A2BE31487590FAB7F34251715E401DA720CB0,
	U3CU3Ec__ctor_m2B6A49AD15FCA9833C2C58C9847F8B5BEDC4A6DF,
	U3CU3Ec_U3C_cctorU3Eb__73_0_m9E0154A97E3AF3EC0FECB2A2355B3611E34D7C2D,
	Enum_get_Parser_m404249DEB9B89C3F7994A156561C602BBAA7EE39,
	Enum_get_Descriptor_m52EA724A27CEBC4D519A8045D6A60E8D58AF95F4,
	Enum_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m201367CB1FBCA15E4572CEF2C01ED40BB772057D,
	Enum__ctor_m6E1C81B05018715B930662010FE77501F5394204,
	Enum__ctor_m48DDA58AECBCB975A007B170E29CAB580FE5880F,
	Enum_Clone_mC8C340BFE917B4374A1D33C35CAC2FBFFB43D904,
	Enum_get_Name_m51F713652DB4E8F187FE773DB8583B0943968E33,
	Enum_set_Name_m52AB363A89E9492ECC6AA4F717E8D0423FE43EB9,
	Enum_get_SourceContext_mC4380A9389B7D17E7D814E376542931A6C39B92E,
	Enum_set_SourceContext_m59B7D9BDEC030C7CE6C724788E800AA2187E0EB3,
	Enum_get_Syntax_m892DFB901E063629BC13392D5629B2384D140D71,
	Enum_set_Syntax_m74BD39956DD611C4E90852AA2A5FD5E4C0B6ED2B,
	Enum_Equals_mC1F65CAC46C0ADE5A1D38C9FA48A3DED3D4DCE1C,
	Enum_Equals_mFC4CEF2A5597BC4AABEC5B7CC79F18274CC8D7B6,
	Enum_GetHashCode_m84A65EAEBAA7FDBE3579FB7E085A667AB6069380,
	Enum_ToString_m8755890EAF8E3CBD38454AAEE50C219D06BC2AC1,
	Enum_WriteTo_m21A724F3E7465A22E8D2933F97B7AAEA97DDAAD1,
	Enum_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m9E010AB642C27A55002C1798086F1126F5C28E91,
	Enum_CalculateSize_m6AE4EEC93B797E6CF87A9C0F14F2DCE78D805A4A,
	Enum_MergeFrom_m02C3F847695806FAA2E6041572102F44B894C9D6,
	Enum_MergeFrom_mAC108FDF195D5796D71C464C308BEFE7F9BF0B51,
	Enum_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_mDFD4A7F6E813A69B629AFE079EFC31645ABC2881,
	Enum__cctor_mA39ADC7A49D6F5DEC4B28EB6F8F6FFF862B0BE76,
	U3CU3Ec__cctor_m25E0386AA478224EF99765AD3DEA945DEB12F2A6,
	U3CU3Ec__ctor_mE1467AECF5DA674F2BBC40FFA42DD4CBC92C4DB5,
	U3CU3Ec_U3C_cctorU3Eb__47_0_m1AB652809E5BEDE3E84F01DE4EE57976E8CB0886,
	EnumValue_get_Parser_mCD86ABE02F0F9E530975BFB1DD8DF6DE0B2F066B,
	EnumValue_get_Descriptor_m33825F769890F8E5B727EF64971606ADA8788EE6,
	EnumValue_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m32E9B91C9B809E6EBBD26C5968A87371270B1A78,
	EnumValue__ctor_m1F9880D30AB86CC9184A09C9244767AD02DBCDAB,
	EnumValue__ctor_mBEB46ABD1DA26AC98C6D2B3CC17B43F3FACE79CD,
	EnumValue_Clone_mAD4F136B74E13F29C57FD9BA516D5690D0D8A2E5,
	EnumValue_get_Name_m322894CCFF48C330198AE5F0F6A07B300FE90F91,
	EnumValue_set_Name_m1F7DE39E39D66AD2E92C7789753BFBA22AAEA3A4,
	EnumValue_get_Number_m8E0B1F149329F97C0F78F799E7390830389E7A44,
	EnumValue_set_Number_m1B49E2FC906E4D2C639C38FAD0CC6AC06CB85F38,
	EnumValue_Equals_mC7EAF672AF155B8A6E5E83A30AFA5C8C737549CD,
	EnumValue_Equals_m300A77FE3BEA40D06F264421A4DF508A35FFFE42,
	EnumValue_GetHashCode_m0A4617C5682F4BFD756D4E0027B99FB3C9916D0A,
	EnumValue_ToString_mE37319FEB422F63DAE1B920B3F47712CD57858D6,
	EnumValue_WriteTo_mAF6C20F5FC69A6D7FB9990941A81887DFC056FBA,
	EnumValue_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m2D246ECE495DC0034AACF00A7F7D7C749B605088,
	EnumValue_CalculateSize_m4ED2A3BCDB5B45F73CEB8F384D1ABEEDA417A7CF,
	EnumValue_MergeFrom_m16FE96C76760F5E983CDC9DA7E9E6608E718AB2B,
	EnumValue_MergeFrom_mE6B7795A557E42BECB42E02D36B871A5AF3F6F2A,
	EnumValue_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m00F8E5F6718E02F5C8D361D25E5941380E4414C3,
	EnumValue__cctor_m16C0CD9EA5974CFB8F74E3B4F9B3A71D4B3D5CD9,
	U3CU3Ec__cctor_m2470DAE20AC02503A82DE4481D3EAD98146AEEFA,
	U3CU3Ec__ctor_m9D69464D4D9F014C698A0346C55E148268D7B059,
	U3CU3Ec_U3C_cctorU3Eb__37_0_m3D2A2652020F03D1C589B2B278A809A5C0E9091E,
	Option_get_Parser_mB82574481FF68ABD73D74EBB04A07F24F3AF1223,
	Option_get_Descriptor_m0D017A952FACE05A6055A69FE79571AB0276A1CC,
	Option_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_mA95527E3FE55B328291D1B058B3EBFF40709958B,
	Option__ctor_m61127A3EBF04C32BAECB3FDC43C06A4A0BC34D55,
	Option__ctor_mE400E557A643D6FD4336467A7CA7F4EBE15B193B,
	Option_Clone_mE36C75B283328B159094B29AD7C378CF96029557,
	Option_get_Name_mDE5C5D257BEB91DBB93C43AA341E7F84491B9542,
	Option_set_Name_m7961ED75B388392B16247BE9DD111C5A19B065CB,
	Option_get_Value_m26415C9AE3093725D50E08EBF07C2FC3CA9C2DF7,
	Option_set_Value_m6BF492B08C599377AA5046B039AE5F258DF06587,
	Option_Equals_m5EE432C5466D73AD284F80AC21D7B022855176E8,
	Option_Equals_m851A5A03B1AD4D16DFC04D52FA7548AF198392DD,
	Option_GetHashCode_mAB0CB2FD7D75D98123988AECFD80B5C37B4A02D9,
	Option_ToString_m87855F8C2E044740FD0712D0F8CF3DBE3143BF82,
	Option_WriteTo_m08FFCCEC210A886C2FC6AD2D163A500877A28531,
	Option_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_mB2C264C7D44F83BE2E6D5E68825776101ADDD61C,
	Option_CalculateSize_mFF4098BC49D348AADE7E6B68D8976452F255D94D,
	Option_MergeFrom_mA6F300660905E229B30AF09C9207C6EF44E158B9,
	Option_MergeFrom_m9EA5877DB4934007C01D74759B0E63925094987D,
	Option_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m1CA4CEF42E2A2A7ADD75A49F9CBE0A644C0D01D6,
	Option__cctor_mF9F2D97A5799BD227ACEDFD1A6E74A0252A731A6,
	U3CU3Ec__cctor_mEF2E772FB2F6BC6213D7BF8908B7B31291A9AA1D,
	U3CU3Ec__ctor_m60DEA573EE6DC55B5D5D1C3EE605C513F0F55AD2,
	U3CU3Ec_U3C_cctorU3Eb__32_0_m029CE28E5DF4064F4E57FF01560036B5824B3E91,
	DescriptorReflection_get_Descriptor_mD48E1E10378003D1F19B1E94D3D62672FDF05EFD,
	DescriptorReflection__cctor_m0C008E17D8889B3551682DBB6471B9FA75F8DF36,
	FileDescriptorSet_get_Parser_mF60C00B40B45ACD6CC5E25158BACEA45E709FC28,
	FileDescriptorSet_get_Descriptor_m8CFCFC9C20758CEFAFF0D8DA9B85FCF333D69C7C,
	FileDescriptorSet_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m3B6F227E8F1D163E6CDFD5CF25D5A83A072953A7,
	FileDescriptorSet__ctor_mA7A07A046215E12A519D8592D84CC8E5D77DE8A4,
	FileDescriptorSet__ctor_m109C9F09C6370D20B70E70630294D2C09790FB73,
	FileDescriptorSet_Clone_m265C516E8B9C78BD5CF6FC647677B44EA22C19C1,
	FileDescriptorSet_Equals_mF6380585A3C4BCBB875C05218072B5FC9E7BEDA4,
	FileDescriptorSet_Equals_m6FB81A95B00A2BA8007CD098A1E64BC53CB1E8DE,
	FileDescriptorSet_GetHashCode_m5E8932B0438992144D00152506F56564FC008603,
	FileDescriptorSet_ToString_m55C6B1E4124A8667B7A472041A15DFA788312E7A,
	FileDescriptorSet_WriteTo_m906D2C2B397CDAFF5EF8D444C80AEB8410EAB931,
	FileDescriptorSet_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m07D693FF17AE52BE5EBFD65CC7132C30BD5F1838,
	FileDescriptorSet_CalculateSize_m71F5ECD29922E5260C9F27F5E9158338697E1A6B,
	FileDescriptorSet_MergeFrom_m4AD41D3A6331B1B8AA293288747EE68F38E15C41,
	FileDescriptorSet_MergeFrom_m6A837636C5DF50AB53B23E53FE1FC765B6828B27,
	FileDescriptorSet_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m30A4D0A83029F411C07921760B1EF8F53B90D6B9,
	FileDescriptorSet__cctor_m20E6F64A50D326CAA9499B7C76E02F52D6162E0E,
	U3CU3Ec__cctor_m50CDA641CBB1EA7607EEADEB9F5268C895A7E40A,
	U3CU3Ec__ctor_m5D33B777A5C55A3AC9CC92DEEA5A532F42A5CA49,
	U3CU3Ec_U3C_cctorU3Eb__27_0_mE97991AD768C275DC264F25BDEB287DACA075A10,
	FileDescriptorProto_get_Parser_mBC80D7C4313B651253041FAA51EA4DD08FA94377,
	FileDescriptorProto_get_Descriptor_mFEA4B1596AB9AF52BCD9884D22F1F3DC3DB72B74,
	FileDescriptorProto_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_mA6906CC88D72C5AA2EFBD969A001AF2CA52AA4D2,
	FileDescriptorProto__ctor_m4485617D3F5C6C7A0B6803FB7A6A9E5CD9289E68,
	FileDescriptorProto__ctor_m0E39D73BEB59D91943EF2EA1E8B43205EE2AD0D8,
	FileDescriptorProto_Clone_m8A232664925A576E17BAEEE2B7C78110936F0378,
	FileDescriptorProto_get_Name_mAF692632AC782B6383B93923C017CD3325A09A4B,
	FileDescriptorProto_set_Name_m24617CEDC7867F3D3587EA82B6D1477BE5EEEDBC,
	FileDescriptorProto_get_HasName_mBFF45899FDF6E9190B27274BCD2681849D6B459B,
	FileDescriptorProto_get_Package_m953FAE73F7EA46AE4C14D110088C6B4F72FB0F05,
	FileDescriptorProto_set_Package_mF51F9BF238F9294C3BFA5B362F901BA3EB72700E,
	FileDescriptorProto_get_HasPackage_m3CA96626EE3FDD289B24A875727808B9432B3CA6,
	FileDescriptorProto_get_Dependency_mB2BD3360EC27F1D642EF3915EA9E3F29D57104D5,
	FileDescriptorProto_get_PublicDependency_mE45FDE23237DDCE9B8EDCD13E2E66EADF35E241B,
	FileDescriptorProto_get_MessageType_m703D5E70CB6B4131CCB13F42E427FFDF7E80340E,
	FileDescriptorProto_get_EnumType_m780C5B3655C4F6BA8F2ED5BD87B7DAC5C73229F7,
	FileDescriptorProto_get_Service_m8D3746DB003BA0368F1EB92EDBE253AE6E86B9FB,
	FileDescriptorProto_get_Extension_mC531637642342EFEE8B96AFA17BA8C08D4344EE0,
	FileDescriptorProto_get_Options_m15C8BD6028E25FAC38BA2B8B3D19F1C585D3734B,
	FileDescriptorProto_set_Options_m21D58E2C3C8AE99895B49377450BC0F378454FDF,
	FileDescriptorProto_get_SourceCodeInfo_m663A9395D90E8CC850D03DC98BEC80693E3A1002,
	FileDescriptorProto_set_SourceCodeInfo_m012501768C09AE8C1865C916E32B32E3012BD764,
	FileDescriptorProto_get_Syntax_m51E3382B5FFB11D7749644173B6D77F5E4E2FA00,
	FileDescriptorProto_set_Syntax_m1130A6525EA22989936E59B94713575A9FE56FAF,
	FileDescriptorProto_get_HasSyntax_m8D2B955DB661A0F2B1E2C199DD6F6DE39F207C55,
	FileDescriptorProto_Equals_mB2CE0475BC597D2DD9156FE405B673152788F432,
	FileDescriptorProto_Equals_mC0682C79CEED2451C8CDFE709CF4F49307B2FB3A,
	FileDescriptorProto_GetHashCode_mE4A7E0861EF395F56F05D05B158256F22EE5670A,
	FileDescriptorProto_ToString_m570AF4B70A7741E75E2065FA39DCF185C8D94CF9,
	FileDescriptorProto_WriteTo_m29D8F0A32F233CEBB2F2FBCB590D4739642AA0C2,
	FileDescriptorProto_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m00C04878C6E3CEEAD15F30CCF8564EB613B2AE9B,
	FileDescriptorProto_CalculateSize_m7F8750FEC9F67C17825224AF365936D26A5AD58F,
	FileDescriptorProto_MergeFrom_mC85444AE37C8A342C2E0AFCA376603A29A4FDA3D,
	FileDescriptorProto_MergeFrom_mEE0DD8720B946B400CFEC3D6C2BC73470BCF592E,
	FileDescriptorProto_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m2ED80E57E0627E94AE7DA4F37CAD6021096B16FC,
	FileDescriptorProto__cctor_m76C00D632E66A93BE727CEFF15D85564BA425D24,
	U3CU3Ec__cctor_m16CACFB1855393A785FD18676663035469D6C0EC,
	U3CU3Ec__ctor_mA9A0202338D571F452DCCE13FE4979824A2F233F,
	U3CU3Ec_U3C_cctorU3Eb__94_0_m26998181CC83924986F5E0CD3E04EB487C270F31,
	DescriptorProto_get_Parser_m276DCF460271094F8087B12382C7517DF98D3929,
	DescriptorProto_get_Descriptor_mB027BAEE5CD378616F1DEC129A2D5D4752231215,
	DescriptorProto_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m5175F90F807BE57C52D5C3D3B6068418944561C3,
	DescriptorProto__ctor_mBA850C613C3DB05A54F24C5E732A05CB9EEA4A4E,
	DescriptorProto__ctor_mCF435119FBBAA9A389619587C9435EC7EC24ED46,
	DescriptorProto_Clone_m8D6A43AB22810E2ABA42EE59D221FFD6F7F7DD58,
	DescriptorProto_get_Name_m3E2C546CE4CE413FBAE61C936C6F93662D761817,
	DescriptorProto_set_Name_mD3E8F34E0D994B21310A929C4C1CAAA90690A3D1,
	DescriptorProto_get_HasName_mBC33C3CD634AB93FB2D4A6467AE9F62BAFE36522,
	DescriptorProto_get_Field_mBBA791CF771781910F63E3C343CEC713DF32989C,
	DescriptorProto_get_Extension_m334A6CE24413CFBA60D5D086A33DAC63E917890F,
	DescriptorProto_get_NestedType_mCE87739C7356629259D2BA6BA630F4D2813B94D1,
	DescriptorProto_get_EnumType_m8D641DC9AD6B53E9AB11C57ABE67E4F45AA3F5C5,
	DescriptorProto_get_OneofDecl_mBDB00FCC8FF516265D2C82802DE1B85A30B357D7,
	DescriptorProto_get_Options_mE52AE7E36455BB39D6448FB4866C8C3ECAB107A9,
	DescriptorProto_set_Options_m64D1E7AE01AAD3129C2A0020A5B813064B904624,
	DescriptorProto_Equals_mFBBBAFCA37FEC00E0D4DDE7758E3F23405F72323,
	DescriptorProto_Equals_m2254305BFFB53FE27C4AA5B4E0FC5C1580C7230F,
	DescriptorProto_GetHashCode_m158B9AA4C433C9EEE8BAB3C7C702E46277B64AA5,
	DescriptorProto_ToString_m7300A8A26C0B22F794F933459B56D6B86DF4854B,
	DescriptorProto_WriteTo_mA8BED1F35DFFC420B71BC788C94C974AF366A735,
	DescriptorProto_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m08457FFB232FE6F6902B30F8E14731C57D6FF845,
	DescriptorProto_CalculateSize_mD15A93668659599FB42FF86E335DBCCBB5D828F6,
	DescriptorProto_MergeFrom_m3309DEA6819B3F251E2F114761961232F3A555EF,
	DescriptorProto_MergeFrom_m186FD354E99638C8B67744AE7ED9C2556DCC9E24,
	DescriptorProto_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_mB4A4E23A065E9FD8A6B8030ED9498F52A2181CD2,
	DescriptorProto__cctor_mA9D22674E07BD74ED04412497E99D248ADDF38DF,
	ExtensionRange_get_Parser_m3ABE1E8A8C459846BBF066845CAC6666B1F437B6,
	ExtensionRange_get_Descriptor_m388DA01C89021BCDAB9380E4B45EA660F872F105,
	ExtensionRange_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m9F7D657538A26F3E6CDC05BCA8DBF1317D857291,
	ExtensionRange__ctor_mCE3CBBCDC18FA5EA4ADBF4AF4AF53D13327D323B,
	ExtensionRange__ctor_m86DE2DE4CC81E1797CDC04A3068161C7AE6137D5,
	ExtensionRange_Clone_mFFA2D7061BFDF6189B1DAAD7388E9C545D8495C9,
	ExtensionRange_get_Start_mA3D0997486C50725BE58C1CA5673D7FF55A5C146,
	ExtensionRange_set_Start_m9365B396D1E93D9383FB218E8C1647580CAF8F8A,
	ExtensionRange_get_HasStart_mF06F274021CF0AAA2A089D408DB9C7A42CE55470,
	ExtensionRange_get_End_mE003D2FA043CC97339771738FFD41E7A0666EE1A,
	ExtensionRange_set_End_m426BFCDB49FFED418C737A05B892921DB94A7C0F,
	ExtensionRange_get_HasEnd_m1AF0817908CE18ABC02A12F0A969A8AED4FB353D,
	ExtensionRange_get_Options_mB23DF8C7A3E0D7C3F1B7A68035C2B420FA26BD57,
	ExtensionRange_set_Options_mCAD0B5561EED579E40AAE5AB14FD08224AB36539,
	ExtensionRange_Equals_mC16F46A587D2727AB7527B37898019C9EC0436FC,
	ExtensionRange_Equals_mBCE0F486ED3163380B949A8F4654DC2359A0EA61,
	ExtensionRange_GetHashCode_m1705F6C558349578176C5BA9C2CD918B6A6689E8,
	ExtensionRange_ToString_m92C8CC6CEE16909BD1FA3C1323DC5A214AF7FAC4,
	ExtensionRange_WriteTo_mCC7686D9A689AC98ED8D425B3131531FFD927C77,
	ExtensionRange_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m78E864903CC6C7966483D1C1DADD8FE35236BD02,
	ExtensionRange_CalculateSize_mBE8901808FC96A0B6647FEE680B06880F0FA84E7,
	ExtensionRange_MergeFrom_m314E38B926BE9F9F1CD9E34681625888E95EB00E,
	ExtensionRange_MergeFrom_m99ECB1EF2D098752D7635C356C1CFA50784EB3EA,
	ExtensionRange_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m24A97A8F83256C686E2E8AE441EF5A58C03244EE,
	ExtensionRange__cctor_mD514B6273C218021DF7CECEADC129B307BA766E5,
	U3CU3Ec__cctor_m39D6E6EC8A756DFEC357E0BF0076C0DFD801B3ED,
	U3CU3Ec__ctor_mAB207C6304C4309948492B33329C4030BBDA3EB3,
	U3CU3Ec_U3C_cctorU3Eb__46_0_m686DD5E765624AF43B355C35E0982F62E9EF4D69,
	ReservedRange_get_Parser_m40DA6F39B9D2674289F247EF71B3B96A85AFF5FE,
	ReservedRange_get_Descriptor_m823BCA62EE04D3B7C9822171D2330F06FC8C485C,
	ReservedRange_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_mF06225797A3EAD80A0EB516FAD8664418A9AA8A7,
	ReservedRange__ctor_mB2F8257CCD2E526672CB4B552FD6799706F38CD5,
	ReservedRange__ctor_m73F9F5F5A8EE837C56568DD545DA961703779FAD,
	ReservedRange_Clone_m30F1DBAF17F68D2418492E3502CC6D8EA1CBDCBB,
	ReservedRange_get_Start_mB2B194971EBAA1B830056532A863FA5ADCC959DA,
	ReservedRange_set_Start_m6F47455AAF98ED71ECC7B3F962F888F605779301,
	ReservedRange_get_HasStart_m06D63475716042512F267FC11EBD1B6091CB9863,
	ReservedRange_get_End_mE43F5C280D211E7F703010B8F2DE636F6A41A613,
	ReservedRange_set_End_m73F555A1EA731DC79EDFECF80708ABD6EC439180,
	ReservedRange_get_HasEnd_mF494E69EE1C08B5151162C24A2567A73F71503DA,
	ReservedRange_Equals_m1C20E897B3FFD56BAB9E3248E741DFD243C3EC67,
	ReservedRange_Equals_mE5912F6171510806B9EF02DE03CF8DD3F35B9E4C,
	ReservedRange_GetHashCode_mA843DDE13FDC934E5BF90F4E87226E6895C7DF3A,
	ReservedRange_ToString_m5B37BC81177B03CCEE38CEF2CC602923C1AE18E9,
	ReservedRange_WriteTo_mAE8C9FDDD04DFAA10E55D3CB8E65948E461FBCB7,
	ReservedRange_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m85F3FBF2DF0058D3A982337FD4736926519B8D07,
	ReservedRange_CalculateSize_m76825970AE352D4A345CA58060D3BA6FF4AD90C5,
	ReservedRange_MergeFrom_mE827CEF25B811678E61BB8E50240CBE127E51442,
	ReservedRange_MergeFrom_m79DF028674F84059F761E6CAA9967E750D41E0C6,
	ReservedRange_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m53FFCAA3778D05AEEA1A4930A32451CB6F529B68,
	ReservedRange__cctor_m10A1727C90A4836F89C853BFEDB8BC36A8F456A6,
	U3CU3Ec__cctor_m842920908C66FD0DBBD8BB949911FBBEE8646463,
	U3CU3Ec__ctor_m5826A097FE555448C5C789E783537C4451C2909A,
	U3CU3Ec_U3C_cctorU3Eb__41_0_m61BF31F913FE7F0E7A30777DA3CD3957025589E1,
	U3CU3Ec__cctor_m8D1AE7EC1E3202B9983A6DB4A59541D21C7A307B,
	U3CU3Ec__ctor_mCD77D5ADC50872DDA7563F46E2160CC31477126E,
	U3CU3Ec_U3C_cctorU3Eb__77_0_m79A3FABE6A2FDED40D62FF3DEE18242F7B6B336D,
	ExtensionRangeOptions_get_Parser_m85440C70A07EB46D6F38725186FA3EEA3DF1DBF5,
	ExtensionRangeOptions_get_Descriptor_m4F76243964B5BE49E73DCC80C35993D84C40A7D8,
	ExtensionRangeOptions_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_mB61A67B362B2405EE1D0FEA2F76462DCF0B6F2A5,
	ExtensionRangeOptions__ctor_m03DB6B7CDC07388AD8EAC496F9F8FE6150421544,
	ExtensionRangeOptions__ctor_m0268E7412A855333005EB632BC2D81177F7CF0D2,
	ExtensionRangeOptions_Clone_mC718522EFD92E71DA33FA9E9C28DB3DDF2C4DA7E,
	ExtensionRangeOptions_Equals_mC28F05C7BAD43AC052787ED90AE10C25DE6F9E5F,
	ExtensionRangeOptions_Equals_m2A50B09E41F199B9161902DC11CEE01EDB02AD93,
	ExtensionRangeOptions_GetHashCode_m1FA993AAD13E3E515CB8E03384BBC859A807F719,
	ExtensionRangeOptions_ToString_mCDAFBAF9D027FD33891829389B30DEEEDA1DA694,
	ExtensionRangeOptions_WriteTo_mE36C0650E68E929057CDB2BEFBD837EB32726F93,
	ExtensionRangeOptions_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m8AAAA629B772292D614BE8DF07E56142B2ACDADE,
	ExtensionRangeOptions_CalculateSize_mE5FABC172FABF85E1427D3110E45BDD0E64E464C,
	ExtensionRangeOptions_MergeFrom_m676DA699130B7B80F30ACA1338B9E369B44940B1,
	ExtensionRangeOptions_MergeFrom_m2E9222BFE09BE7AC39995BB9DF8377E8C5C02486,
	ExtensionRangeOptions_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m67DB4C9A5F1DC35C7852751B65AE26736ED00D11,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ExtensionRangeOptions__cctor_mFA24396C1DEF8ECDC28E86DBFB1C632D983C4F45,
	U3CU3Ec__cctor_m965149C8F86F84E9D3197F694E9968244A1153B0,
	U3CU3Ec__ctor_m77513FE6A1DA0E18E451A30C160D4CA9975F0B6D,
	U3CU3Ec_U3C_cctorU3Eb__37_0_mDB82B61FE0D537AD51A6B817F5629708C04D9324,
	FieldDescriptorProto_get_Parser_mB9782B37EA5D993DA11EAC9F10D5A49B516F7B0F,
	FieldDescriptorProto_get_Descriptor_m9A6BB07AB7DE3A05D6B336CB8CB4F6FFEE25ADB1,
	FieldDescriptorProto_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m8F889757EED4DE719D6CD2C961086C28FF9BCE01,
	FieldDescriptorProto__ctor_mDB51787020766E9E2EAB543B0184EAFA0982FD22,
	FieldDescriptorProto__ctor_mF5B66C7A74743444259BC8300F6D960A429A61AA,
	FieldDescriptorProto_Clone_m3B18B348D372D0C457D0F49FD218045E8D8A89B3,
	FieldDescriptorProto_get_Name_m5F250A971D59E241BF2880A56CB1F9745B16BDD2,
	FieldDescriptorProto_set_Name_m5C9270A955025D66573AE8215E7926F1BE4B3F4E,
	FieldDescriptorProto_get_HasName_m0C19A6BB41B5781F03DE0293C808C48AC24497BF,
	FieldDescriptorProto_get_Number_m21A6E332B603CE4603DE65C8DF13AF06DE04CE15,
	FieldDescriptorProto_set_Number_mB34288A1D2DC39CFF8DB4B385ED9E0732DBEE4B0,
	FieldDescriptorProto_get_HasNumber_m5B490D0FF92C919DC31393CF001F9E33A651E765,
	FieldDescriptorProto_get_Label_mFCD8775062E789C07AAF8DCECA6DA4DCEB7AE628,
	FieldDescriptorProto_set_Label_m557B8E68A108A0C73C4086A9AD5DBD461AFF6C99,
	FieldDescriptorProto_get_HasLabel_mE7F9CB633C16FCBB83919A67579168F04D5F7023,
	FieldDescriptorProto_get_Type_m0504EED6A9D14275AC75A370351D61C7D52A8040,
	FieldDescriptorProto_set_Type_m30BEE0C21CCB3126E6B50762F497BB97369D9782,
	FieldDescriptorProto_get_HasType_m51916144B1C5D1E46D8B48BE61276FD4C76B6FF2,
	FieldDescriptorProto_get_TypeName_mB4F1FA66AEE10FE9E458109DA595B976110EBD23,
	FieldDescriptorProto_set_TypeName_mA13C37BF4F941B77BA0366C5EC41CDA8E8D811BC,
	FieldDescriptorProto_get_HasTypeName_m67246CBDAA461FBEBEF934813707FAFA63D71C76,
	FieldDescriptorProto_get_Extendee_m5CA86ED516F65391E65B7B9F62DC18D832516EF0,
	FieldDescriptorProto_set_Extendee_m6A7039C9B3FEF8B31BC1D22A046EC6ACCC14FA1C,
	FieldDescriptorProto_get_HasExtendee_m002937A41A74030F48AECCF56AD16783E62469FC,
	FieldDescriptorProto_get_DefaultValue_mEB7AA3385EA5B984CA4BC5A81AD8FD2E2838072E,
	FieldDescriptorProto_set_DefaultValue_m30453870C914082727395E4F483B9C27FACDF83F,
	FieldDescriptorProto_get_HasDefaultValue_m43BB2F47AAEF3682FD933DFCE9E676303443C3E3,
	FieldDescriptorProto_get_OneofIndex_m2F72D788585178F84508EA65E31798F47E591C06,
	FieldDescriptorProto_set_OneofIndex_mB59F7FD960A44D5713C2C7C533DCB2E29598176E,
	FieldDescriptorProto_get_HasOneofIndex_m402CEDFAE1C19BC01B93ECE144F012A6F4F8D10F,
	FieldDescriptorProto_get_JsonName_m88C694CE526673F798DFDD9428D8E461FAC219DD,
	FieldDescriptorProto_set_JsonName_m648841B04831164E456885A2AD7C4E1A907271C4,
	FieldDescriptorProto_get_HasJsonName_mC66BBCBF80BC5DF9F925E353513CAE31D9B543A8,
	FieldDescriptorProto_get_Options_m024A5FCA8A0E7863232F739DE7EADDC097E83406,
	FieldDescriptorProto_set_Options_m95B65464743B9F0939EB24BBEE6C968C71C4F778,
	FieldDescriptorProto_get_Proto3Optional_m03E3CCAB1139E8E1FF6A7101498E5099FDDE5C17,
	FieldDescriptorProto_set_Proto3Optional_m4681472D59480132B5EA2040610D889A6CA34811,
	FieldDescriptorProto_get_HasProto3Optional_m66D148A23694B79771081395760DB902F52CC67D,
	FieldDescriptorProto_Equals_m8280930EB8D7F7C3BFE6BA9FA223C0411C56F47C,
	FieldDescriptorProto_Equals_m3527F0C058D9FC4E1466AFD93B448721A1F18763,
	FieldDescriptorProto_GetHashCode_mB99143077FA079BD863F94EB1D18717B09C01CC8,
	FieldDescriptorProto_ToString_m11F796FE089C2875CDB38AAA3422E19996433A7E,
	FieldDescriptorProto_WriteTo_mFA22B0175F6D838CEA09CA1F7B6DFE0E76622CC7,
	FieldDescriptorProto_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m5898B3409384DC7633B83CDAC353FFD5D1D67784,
	FieldDescriptorProto_CalculateSize_m955D78E94DE36494938994453F571457E48436D2,
	FieldDescriptorProto_MergeFrom_m0DCDC8CB908E61CF0D7136B40C38940D9F21F986,
	FieldDescriptorProto_MergeFrom_m70DA833065B73BA3CE80FE249DFDAA745A4981EC,
	FieldDescriptorProto_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m986E810C00664AFB4CF43B946B88EE7A1DC7B66F,
	FieldDescriptorProto__cctor_m759F49EAC9FA24827EB7D731D55D718F4DD10C76,
	U3CU3Ec__cctor_m9922F139143B79B66907D5606AA1A59C5C20E9F7,
	U3CU3Ec__ctor_m072AEA01AA790344E658362FBD0DEAD2855BF199,
	U3CU3Ec_U3C_cctorU3Eb__119_0_m7D9173AA040B54F58FFE68F3FC7A1534375B457D,
	OneofDescriptorProto_get_Parser_m38645932E3246CFFC2302EE1172459DD75F4C40E,
	OneofDescriptorProto_get_Descriptor_m0F8C4CF9CDFA95DADBE71668532A80BF2280EE5C,
	OneofDescriptorProto_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m8A36A9B5974436DB17EE53737775E9102339983B,
	OneofDescriptorProto__ctor_mD2CE062E1C095E49ABDEFDC74B17229A45493047,
	OneofDescriptorProto__ctor_m14C97250B9E90E60CA6CA6064C9250354632C3AC,
	OneofDescriptorProto_Clone_m7C45A01EF4DAE4EDB8A17F181864989B593E3D11,
	OneofDescriptorProto_get_Name_m48DE233B82B48DD4D9E7066E0785FA2F846CEC20,
	OneofDescriptorProto_set_Name_m1F624AEBFC415E1D977216D1CA23C22865C374BF,
	OneofDescriptorProto_get_HasName_m3EE3ED61FCF21A67137EC7AE8BE7B1190B827308,
	OneofDescriptorProto_get_Options_mAC4CABC75D32418D2430F721324B86232B0E9D3C,
	OneofDescriptorProto_set_Options_mA0DB684E887321ED76D23FCF58BFA5E680F61B07,
	OneofDescriptorProto_Equals_m63D5B71E1166BAFAF2BE805067EC147D8D75298D,
	OneofDescriptorProto_Equals_mFB6BA3CE309BF9605D4E93E1CDBE45ECAF125EA3,
	OneofDescriptorProto_GetHashCode_m44452D2990660A273BDB0CBA23FC660618F20E1A,
	OneofDescriptorProto_ToString_mE05FEA563B6444CC0150CAB7800E63589CFF3683,
	OneofDescriptorProto_WriteTo_m1122F29DE940E7FDE249B8B49D0A4CE85FA88DF1,
	OneofDescriptorProto_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_mC33BBE456CCB3E50F512370278FCB60B20882B58,
	OneofDescriptorProto_CalculateSize_mF6EDD0758C04B3A0C663D458BAE874DD78BAE845,
	OneofDescriptorProto_MergeFrom_m9929A6BFE74508B75D3C84F099E0473C67499803,
	OneofDescriptorProto_MergeFrom_m7E5A8F79E11BF5CC5DC28F66E56916BE9C185511,
	OneofDescriptorProto_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_mDF43CF284022D2519858B4865D13C83C21F249A4,
	OneofDescriptorProto__cctor_mA36895FBF6949FC809866F5FF0118000B3DF050C,
	U3CU3Ec__cctor_m242B6FF0015724B2B43732AB2E615DAAB731EA19,
	U3CU3Ec__ctor_m5AD40BE630C1F0562A7B91052FA00678AFF64073,
	U3CU3Ec_U3C_cctorU3Eb__36_0_m9B3284D41CFACF2A16341508FE1BF105891BEA82,
	EnumDescriptorProto_get_Parser_m5FE24F404BAE27BD341F2424625E3F2201CF6EB0,
	EnumDescriptorProto_get_Descriptor_mB89737A900858354FD200A724032DF8194CF571C,
	EnumDescriptorProto_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m0BC205A88C56A3251A74F252C5D080B69215021C,
	EnumDescriptorProto__ctor_mB3A28E8576A228315F116223DA06685ABD39550A,
	EnumDescriptorProto__ctor_m147F600199E44F16A8C1AA7F6991895708CF4282,
	EnumDescriptorProto_Clone_mB7008C506A35ACCFE0E5D36AAD55F49F98F6E85A,
	EnumDescriptorProto_get_Name_m239FF7D5EB664E2BE0E535D490D9999276C6E9C7,
	EnumDescriptorProto_set_Name_m86B3EB26198B6E6AC4B74DA6FAE449D2710C4E3F,
	EnumDescriptorProto_get_HasName_m3C3EB6849643E5D01261B7B8A141641CECC48CF3,
	EnumDescriptorProto_get_Value_mB8CCEB385142A84668F5C1325A5C5ECCFF7E842A,
	EnumDescriptorProto_get_Options_m54DBDC48FC10B41D6455B5ED6708DA40E2D9929A,
	EnumDescriptorProto_set_Options_m817E21BD5AD5FE2050946B7F42F4D1010F661F2E,
	EnumDescriptorProto_Equals_m6711ABED6B0C4EC1496AAE697AA452084CB3546B,
	EnumDescriptorProto_Equals_m22E80DDF1828BF67B70CDC86776D054CB05F6F23,
	EnumDescriptorProto_GetHashCode_m7EACA529EA0BFE654F625A89C93F046781EE0F9F,
	EnumDescriptorProto_ToString_m4FB571E200CAA7F881910FBEB583E511661A8340,
	EnumDescriptorProto_WriteTo_m9D12A0C8024AA0DA17AE32C6782D2930703EEAE4,
	EnumDescriptorProto_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m14AEC404CD6EFADDE5EBA504F48275D2AA0D66F7,
	EnumDescriptorProto_CalculateSize_m9F366B59267B5766D2E4B89E64EE02E653AAF2E0,
	EnumDescriptorProto_MergeFrom_mE277EA218CDEAB5A29709DA28ED6FA1B6E7A1CCC,
	EnumDescriptorProto_MergeFrom_mD7331F6048B5F3A9CBD9BCE98EC931224970178E,
	EnumDescriptorProto_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_mEEA70B246D0C85DA555958273DC1F8F2F977F7CF,
	EnumDescriptorProto__cctor_mBBEB7BC4DCDC9A22FB4641CEE3690F4273CFDD46,
	EnumReservedRange_get_Parser_mCA427AC75416A92344F6D66F4CCC3578CFE40DAA,
	EnumReservedRange_get_Descriptor_mD31EA935A954A7AAF19EB17AED0FEE0644E39056,
	EnumReservedRange_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_mB1D21B0C42DD2B880258B2F6190BC9EE0D8A98AD,
	EnumReservedRange__ctor_m02904F51FF5FE5B92BAA1E816D3A1B13534A6E0C,
	EnumReservedRange__ctor_m4F7BB9BB1007668742F085BDA2A82E889F6D1B6A,
	EnumReservedRange_Clone_m0478CEBDD1CC260098613EC9608FA8DA619521A4,
	EnumReservedRange_get_Start_m425FFA90089C7FCDD1C01FA5C950E5477F17DCC5,
	EnumReservedRange_set_Start_m69F0A6CB6629086763121A8CD5E295415242BE82,
	EnumReservedRange_get_HasStart_m394D0FDEAAE8B2C54B2B7ACE2DAFD1F354B2AFA8,
	EnumReservedRange_get_End_m012A5FA46B5F0D66FBCEAA9B17BCE31CB105E0D9,
	EnumReservedRange_set_End_mF993946771523BFE0D542CD2C3ACB19ADE9C1CC3,
	EnumReservedRange_get_HasEnd_m743B6947E1F9A8CD3CB43B5CC0056313942F1AE1,
	EnumReservedRange_Equals_m3728912F8E0AD0EA5A74FD69323CF9F73465DA04,
	EnumReservedRange_Equals_m1FB9B43A21E9D232DFE20485F50C4673D84948E1,
	EnumReservedRange_GetHashCode_m6415E55F96EE5B92BBE2965CD1FE5D37C2604231,
	EnumReservedRange_ToString_mA9995D4DB4A84520B09678054B780C0714C18E32,
	EnumReservedRange_WriteTo_m7D1FE13C055716582D6BC6B24B10AE1654DA1316,
	EnumReservedRange_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m40335111DC4E167612FEF3662BE9124300D3480A,
	EnumReservedRange_CalculateSize_m0A62C551FDA64908B0C58397CD1428C5C71BF2F5,
	EnumReservedRange_MergeFrom_m2FC3A667F2AF4C85C2C7D3AF0E4E471D92019CD2,
	EnumReservedRange_MergeFrom_mCD0205C8ED2CEB21A1C6F16D967019975292C0A0,
	EnumReservedRange_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_mDCF2D669D389A95648C8864C743EBF87D6CAC659,
	EnumReservedRange__cctor_m97E50FAA53B7507FE0785E95741F3780277FD731,
	U3CU3Ec__cctor_m4FA27EDEEFE8562E0CC992077B8C1D26C49865FA,
	U3CU3Ec__ctor_m9D85A171DC63EF24760F067CDB4D72BDE4E10F18,
	U3CU3Ec_U3C_cctorU3Eb__41_0_m0FBA9F917C842E30E53B32B2B8E897821F32A8C3,
	U3CU3Ec__cctor_mF24FCE934E414464CBEED3D1CD4FE729C6329AE2,
	U3CU3Ec__ctor_m1CCAEB2DF4233470E1D07108642ADA2076F28191,
	U3CU3Ec_U3C_cctorU3Eb__52_0_m2EF60F0DEB12598A39B13FE3EFF834AD9A2057BD,
	EnumValueDescriptorProto_get_Parser_m736581710BC0F2AF5F05E5D5E6BC467A063C433E,
	EnumValueDescriptorProto_get_Descriptor_m146373C8AA663CEDF40BC9946A83EE3F670300D9,
	EnumValueDescriptorProto_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_mCFE0C5CFECCD5C3E838C28DAFBB70451CC9FDB6B,
	EnumValueDescriptorProto__ctor_mCD6381E84158229867FFB9D4C43817B859E9A183,
	EnumValueDescriptorProto__ctor_m0E2CA6496FBAE77FB5E1F05802586BD4BE3A3D3D,
	EnumValueDescriptorProto_Clone_m1611AE2A72549FFE60B8119A55D815BA696E862D,
	EnumValueDescriptorProto_get_Name_m4D3B8EBCC09CB1F0B22D0350125C38EEB087B34B,
	EnumValueDescriptorProto_set_Name_mFB204B06E511114E6E238BB93C90771FB6C254A4,
	EnumValueDescriptorProto_get_HasName_mA03E99F8576641D0A97E44CAEF6082E2A85E28C6,
	EnumValueDescriptorProto_get_Number_m2658C0963EEB574D8E6BF71C97475DBDBC93B2DE,
	EnumValueDescriptorProto_set_Number_mF3E1231A22B71F47DFD2BD8D29FE562EB13054E7,
	EnumValueDescriptorProto_get_HasNumber_mE73E6803CF10AF23F69DC74FF664557B2F0AFF80,
	EnumValueDescriptorProto_get_Options_mEB9A81DBD16E5E0E884D41FBD603AC0912439FA5,
	EnumValueDescriptorProto_set_Options_m2D4E0DF7E53ED5FB52F2FEE5C1EADA4827B66B7F,
	EnumValueDescriptorProto_Equals_mAEBB3D490CDECFB762189684B4B64227E2759C07,
	EnumValueDescriptorProto_Equals_m4676E76A2633C7C43EA50E9A532A5CFBAA32C1BF,
	EnumValueDescriptorProto_GetHashCode_mB2267753F8624960C3BE6FEF07DE4F221E82E7D0,
	EnumValueDescriptorProto_ToString_m2A605BE66BAB0ADDB88DF7F6D39D38E4AE30E399,
	EnumValueDescriptorProto_WriteTo_m628970DB0F15CF7E6A2C600B877D999E92B0AF30,
	EnumValueDescriptorProto_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m742B37B6C10FF627FF03157232ABA74C1918CEA7,
	EnumValueDescriptorProto_CalculateSize_m1F7A660E62D7576C260035086428BF3452D89C36,
	EnumValueDescriptorProto_MergeFrom_mC16859FC588043BE69253243292C173544C0BC14,
	EnumValueDescriptorProto_MergeFrom_m415B9988016FE0741689B94605032A8CD4033362,
	EnumValueDescriptorProto_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m586D380CAEE945088D1FDBB1C08489F63B823E5D,
	EnumValueDescriptorProto__cctor_m38CBBCC0C5F4CECD79755881C364BADB96F73ADD,
	U3CU3Ec__cctor_mBFE3CF17B8E60367972AC1782280497A1E4DD08F,
	U3CU3Ec__ctor_m742974F2783A1F0967EDADBE5B0BDD981BEE9997,
	U3CU3Ec_U3C_cctorU3Eb__46_0_mB3936AABD2C18F0C5B2CBD1B38489783D4690BB6,
	ServiceDescriptorProto_get_Parser_mC303B987ABAB6F64E367D474BA45F890394438A0,
	ServiceDescriptorProto_get_Descriptor_mA67C62EEBA7D28C74F621B11784459C60E940606,
	ServiceDescriptorProto_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m72780D4209A552D65E0B2EDC9AF2F74396F945BA,
	ServiceDescriptorProto__ctor_m2ABEEBBB877E11DC2EB775785478B01367CEFDC5,
	ServiceDescriptorProto__ctor_mF76948802201FEE2ACD8D67A55B5C75EBFF5FDF1,
	ServiceDescriptorProto_Clone_mE601AB9C3641148DB0AC9C3B6BD5F3281DC57109,
	ServiceDescriptorProto_get_Name_m2CA082476B1455CAA4307E364D0806D815297114,
	ServiceDescriptorProto_set_Name_m6F0DC197D53F8ADC16E042DD3FAFA07659E22D42,
	ServiceDescriptorProto_get_HasName_m348F6D83EF5F66D7B494D3C15604109B29341484,
	ServiceDescriptorProto_get_Method_m95D93BB86E2947C96B1B9D01D068A695307053E4,
	ServiceDescriptorProto_get_Options_m59C55E0142DCA61D9BF7F0E6BBB356F8872BAE8C,
	ServiceDescriptorProto_set_Options_m620243EB5D6716CEE0E1C61A6B5808830C1EB302,
	ServiceDescriptorProto_Equals_m5C75FE57DE6DE100164657FB1129840A9B9489AB,
	ServiceDescriptorProto_Equals_mFBE6BA3EC312B0C6F693C79A2D723347CD013C55,
	ServiceDescriptorProto_GetHashCode_m7BA75A38679BF8C53EA10F293060F72BFDC46396,
	ServiceDescriptorProto_ToString_m97875482BD469952825E67CBE1ACB629241E775A,
	ServiceDescriptorProto_WriteTo_m4D6E98E4F13566154B1EBFB0309D2DF3A4B71A38,
	ServiceDescriptorProto_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m4E48FC87B5F4637449255AF403AAF0971BD9FF91,
	ServiceDescriptorProto_CalculateSize_mFD1610268B60E21FDA5CA531B00065C1E4A6AB8E,
	ServiceDescriptorProto_MergeFrom_m6581ECAC1AF5ECDFFCDEC9A551D9F71BCEC0005B,
	ServiceDescriptorProto_MergeFrom_mA53EA15C18F1199B95F5F1DF3AD52A81F6E5AB7E,
	ServiceDescriptorProto_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m70DB24C4CDA1363FF025EA7F7CBE3449C1A4C49C,
	ServiceDescriptorProto__cctor_m5D33924606924480FEC2470E35B57960B0DA560C,
	U3CU3Ec__cctor_m8AA62DE218C9E50A4C74E5558316F4481A8462A6,
	U3CU3Ec__ctor_m500563BC829DEC9ED764001F0FAE0B0022EA1311,
	U3CU3Ec_U3C_cctorU3Eb__41_0_mB6AC9111A732B674814B82F419F830E4881F83B4,
	MethodDescriptorProto_get_Parser_m33F87B49E86519177E43BAC6C8B3AC22C81BA120,
	MethodDescriptorProto_get_Descriptor_m9C3F7BFEED0E7AAB045476A0DE1C80FDBA812E0B,
	MethodDescriptorProto_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_mBDBFD0BA05C9C3B6D89862D779A5EDC34A0BA98C,
	MethodDescriptorProto__ctor_m3181F7006234380068049FEDB5E800625B8193C8,
	MethodDescriptorProto__ctor_mEF5E8090FBE2C5FB52278A283B26A67284FBD781,
	MethodDescriptorProto_Clone_mA90669FE9E8DE792CEC9323E367F334926F95A21,
	MethodDescriptorProto_get_Name_m2BFA476C2A5B5DA54D12CC4C923453098821B361,
	MethodDescriptorProto_set_Name_mE535380BD9B436D4001611F5F17EE62BB5756ABA,
	MethodDescriptorProto_get_HasName_m87477C570158C989CFE5916FA1998FFE8435BA5E,
	MethodDescriptorProto_get_InputType_m624EC07E1B8E70B8223DB68B7172551912E61184,
	MethodDescriptorProto_set_InputType_mCCE83CEDF67EDFE5B94902EC8B14ABA22B6076DD,
	MethodDescriptorProto_get_HasInputType_mD4C609F5B5EFE2E14E0E831341E9ECB040D90336,
	MethodDescriptorProto_get_OutputType_m0EAA2C37BEC6E3719FFD4D56D37C10BC2AE4E004,
	MethodDescriptorProto_set_OutputType_m621B8EBDBD403F20BF4DC5CC38B1189FFE7D7E30,
	MethodDescriptorProto_get_HasOutputType_mF6C592B099C3A481A06FB8ADE2F2F4C2503D8B3A,
	MethodDescriptorProto_get_Options_m0524F118FC671D80EA9BE19F9B2E4B7E2F264DC4,
	MethodDescriptorProto_set_Options_mA972CB7C00D36504F42C0A7EA333696B665DF185,
	MethodDescriptorProto_get_ClientStreaming_mDD3445121C38886F66D3AD2817BB1C73F87D8BA3,
	MethodDescriptorProto_set_ClientStreaming_m1DF1AEA16E674C69657697F16EDFB7E84CE271AC,
	MethodDescriptorProto_get_HasClientStreaming_m24B36114B891184073831BDB2FC41DD8666A7248,
	MethodDescriptorProto_get_ServerStreaming_m41152F7FB02BA7B27E28CA920118107536EB1FD6,
	MethodDescriptorProto_set_ServerStreaming_m97B11280B7BF0E4CC0333240EE49A41CAEC5029A,
	MethodDescriptorProto_get_HasServerStreaming_m3929AD6103E1FAA56B3EC8EF7F9E95761AEED20F,
	MethodDescriptorProto_Equals_mE404C60BAAC311AF6BC9E81457AE4901A2EC69F1,
	MethodDescriptorProto_Equals_m15C523245227F5007A8438DA67437535DADCA8A0,
	MethodDescriptorProto_GetHashCode_mBE5452AA6B4265114D0C57FDC8BC4496C01211CB,
	MethodDescriptorProto_ToString_mF5CA9EB9E35C30D1141628CDA8D36F3183524337,
	MethodDescriptorProto_WriteTo_m81FE666F3F825DFE70F6A79A3E8EC18E54F3CBBA,
	MethodDescriptorProto_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m3D279F4D543CF3AEDDA8A726D1DDE892CF58D8D2,
	MethodDescriptorProto_CalculateSize_mD02B74F49B50CFCB0A8504ACE320C81D1815D622,
	MethodDescriptorProto_MergeFrom_m2B10CD2A53C04E575D652B444AF50326300A36C6,
	MethodDescriptorProto_MergeFrom_mB331EAAF45C06DE2158A063B329836049B00650A,
	MethodDescriptorProto_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_mEEC631A9308E9E0931247AD1FC67A84AD79F7E88,
	MethodDescriptorProto__cctor_m83F215670109EA13720E3F066D4C28E7632DCF10,
	U3CU3Ec__cctor_mE42352A3A7C05E9A32494055E42FCCC08F0CEA33,
	U3CU3Ec__ctor_m45798C39C0D5980147ACA9522D5212C9A9B817AB,
	U3CU3Ec_U3C_cctorU3Eb__73_0_m6A24E6A87D6014A86D7AF2BDCA311405278FC48C,
	FileOptions_get_Parser_m587B87D900DF3D95A6AE812D799D31545B4C2ED1,
	FileOptions_get_Descriptor_m713F7027607716CA4C97E854D9069EBB2C36FCD9,
	FileOptions_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m2AFDE55473E18F7F871B408A286EFB5C5D29D99F,
	FileOptions__ctor_m53E8E18F45026B68293C1D6E27ADD3C809A81E75,
	FileOptions__ctor_mE14316BC374AEFD5371F0306BB87D7635BE51612,
	FileOptions_Clone_m4AE4FD9F2AA9ACE2E4FEA61EF8CE2996995E6D79,
	FileOptions_get_JavaPackage_mD28C975B137594B1A06CD71877B582E00047EAFF,
	FileOptions_set_JavaPackage_m4C2D8A8713B941E8024E0A4D9D69216F96D22AA4,
	FileOptions_get_HasJavaPackage_m0CF95ACB5F2EEB4452E36CC18B3E9E2754996C13,
	FileOptions_get_JavaOuterClassname_mC228C2687E34392FC2AD06D987BF11887BA90365,
	FileOptions_set_JavaOuterClassname_mACBCB1B833F08F938F8DC77F699C8BDC13943565,
	FileOptions_get_HasJavaOuterClassname_mCCC09DA98F0EFE916F6F5B021F7E435F64841689,
	FileOptions_get_JavaMultipleFiles_m54C438931C320DCE01E37ACD53B3EC00A996A130,
	FileOptions_set_JavaMultipleFiles_mAB89868C1A5BDE795E2EB58FBFBADB44ED4077E5,
	FileOptions_get_HasJavaMultipleFiles_mC4CBB93CF269855063DB6D402B183971C1EE39F7,
	FileOptions_get_JavaGenerateEqualsAndHash_m1A05435B25FDC559CF64163E210308D8993FEF6F,
	FileOptions_set_JavaGenerateEqualsAndHash_m054D64AD6F01F060D1298F1A0EA2806D07B8C3AA,
	FileOptions_get_HasJavaGenerateEqualsAndHash_mB6A19D104A2EA0440A049969861450B3BBD126A3,
	FileOptions_get_JavaStringCheckUtf8_mF7300DBDC375B34FE98BC5FDABDCF3CB03D9086A,
	FileOptions_set_JavaStringCheckUtf8_mF3D2C16F0B9CA1A659EC4B28ED19110E5E5400FA,
	FileOptions_get_HasJavaStringCheckUtf8_m1F7D4ED06293C20BD49A26D12EAF6BDC3DF1B818,
	FileOptions_get_OptimizeFor_m1080B2062A0FDACE602AABC7B1ED8702DD3997D0,
	FileOptions_set_OptimizeFor_mC9FFBF890C9174B432DDC78E43A71BCA6C5AC301,
	FileOptions_get_HasOptimizeFor_m2BABE8FEF9437D386D8FD7183BC3F99638827263,
	FileOptions_get_GoPackage_m1CEB4D63D857A8B07A43F669FFCE5A873E131006,
	FileOptions_set_GoPackage_m6AA83587E1434ED366B11E31822E99D084ECF00D,
	FileOptions_get_HasGoPackage_m0E38B6F1288817361D48841ECA8D915BFB3CAD86,
	FileOptions_get_CcGenericServices_mB3F1D5A2E0D7C5A184943EA1C7964A2FAC7D4A80,
	FileOptions_set_CcGenericServices_m5C83601CC00B6B5DD7C229291C49F57CB0FF38EE,
	FileOptions_get_HasCcGenericServices_m2DA36E1DEAE50843DBB9E587DB839732D5B82FCC,
	FileOptions_get_JavaGenericServices_m90F59166A912AAF683D36B6BC2B6680F355DC466,
	FileOptions_set_JavaGenericServices_m774C5C12C2BDA83FA632B50C362773A738D46DDD,
	FileOptions_get_HasJavaGenericServices_m383D1E2AB00C08219D174DC2E685A7E4CBCE0008,
	FileOptions_get_PyGenericServices_mE14DF5C60BDB3E4B42281019939D4582D291364A,
	FileOptions_set_PyGenericServices_m88EC0EEF66167798DD20331211D07DC628E61B71,
	FileOptions_get_HasPyGenericServices_m60552C76BCAEFD77B73EE64AD3BC594FB9ECDD91,
	FileOptions_get_PhpGenericServices_mDCD5D0BF4C976F4A719C05E10009D699D9141CFF,
	FileOptions_set_PhpGenericServices_m1802BF8CF6B99D94E4C932FDD08C9EC09F016451,
	FileOptions_get_HasPhpGenericServices_m83B42571440FD10507A557B11D64146DAAD723B2,
	FileOptions_get_Deprecated_mD3EFBEBFE2D806322D2F1F7C97BDFB23872FB959,
	FileOptions_set_Deprecated_m13C24D771BE054745D062E8AFAEB153661B70A00,
	FileOptions_get_HasDeprecated_m65BAFFA6D7DA1CCDF1E3153F2B4DD272583B42DA,
	FileOptions_get_CcEnableArenas_mF64263429431E46458295FA2EFABB841BCF8BD4C,
	FileOptions_set_CcEnableArenas_m603FEF01FBD68F43ECB94700C21771C02A183935,
	FileOptions_get_HasCcEnableArenas_m1DE12B00BAF66E1C3D92A69069CDFBC4CAEF230D,
	FileOptions_get_ObjcClassPrefix_m4E99A739C7303DA7010B0E94AFEDB3B17B83DD22,
	FileOptions_set_ObjcClassPrefix_mC135A42CF25258F6FBCA1CD05C8B27F7D05D0473,
	FileOptions_get_HasObjcClassPrefix_m8E411663B3789519CAC6ECAEC05ADBAD24E55C70,
	FileOptions_get_CsharpNamespace_mA06DB44F8B42378563FF4DD3CC4503826EA7FE29,
	FileOptions_set_CsharpNamespace_m9BA16A0178140239C7796FC95AB184B2074F9D9A,
	FileOptions_get_HasCsharpNamespace_mC522872193C94FAA36CE4F4DA9826507FB9EB04E,
	FileOptions_get_SwiftPrefix_m03C817944B8FFE3789DAC9410DB7D997ED39B7C4,
	FileOptions_set_SwiftPrefix_mA39A1D730B7CC4910F544B629FDB598C6ED69204,
	FileOptions_get_HasSwiftPrefix_m021DC903B6160DC2BA4141DB2CEFA1D8EE97C823,
	FileOptions_get_PhpClassPrefix_m13FD38C91D59EB43F935E6E01CBDBBA2FBBDB909,
	FileOptions_set_PhpClassPrefix_m5AFABD6BD1438D4D77A077B04130F85B9426877B,
	FileOptions_get_HasPhpClassPrefix_m28176CDCD14A142E578E858BF09477D49B9B77CC,
	FileOptions_get_PhpNamespace_m776A55870E0A2F92A6F65739C496E9E7428CF29D,
	FileOptions_set_PhpNamespace_m9F9AE2BE7414A7D099DAD4A9E2CE7965CDAC12C8,
	FileOptions_get_HasPhpNamespace_m3256AF1C6ED7E314A9154E6D82C73DEE05342D8D,
	FileOptions_get_PhpMetadataNamespace_m0547A034716AB0083517A8C382C46167B7BAAD27,
	FileOptions_set_PhpMetadataNamespace_m0223FB7107C6DEC58CC783B23EC580AA856F5A60,
	FileOptions_get_HasPhpMetadataNamespace_mFB8CB7D1EAFD71823C1A8E38FF78663FD4DAA81C,
	FileOptions_get_RubyPackage_mCCBFE231F243FC7919302D387A8854D15B97F856,
	FileOptions_set_RubyPackage_m23FA32D15B20D7D20CA0D40A723D88D9C19A81BC,
	FileOptions_get_HasRubyPackage_m1FA658801E944386F74C089FCC78A6639A41A320,
	FileOptions_Equals_m269B71EC6513379C07694356E74F494902C8BCCA,
	FileOptions_Equals_m9A68AC5269CD93866D6B3AE6103426CA0B62C75F,
	FileOptions_GetHashCode_mC6302B4BBD76B6B1B81E42621DC3B414AFBB2093,
	FileOptions_ToString_m1F624CC78700C658ED7BCFCFC64F1932A9DE8591,
	FileOptions_WriteTo_m46AC0C3DC74648185755CF83BB96E3097A430512,
	FileOptions_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_mE7D6ECEA3BB13F301585BA7374B00F84DFE61CC7,
	FileOptions_CalculateSize_m1CBAF20ABA4BCFD4FAC533D9EDF18F7C65AF1105,
	FileOptions_MergeFrom_m602802909D9E87D110BEA1BB3D6ECDA93F9030E6,
	FileOptions_MergeFrom_mD0E3ECF95A4D41396D5A6CD667943E22F2D163AC,
	FileOptions_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m9A84CB596D5C261B6CD9973D22690134C98313FC,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	FileOptions__cctor_mBE8948D9ED64B5359AEB8F87679730C37BC18C9A,
	U3CU3Ec__cctor_m5BDFC78A6F18D3D73E06A7CCC26BF02ABE01B154,
	U3CU3Ec__ctor_m222F030FEBC8373501D9E4877AE838AF87A4E667,
	U3CU3Ec_U3C_cctorU3Eb__219_0_m0D4607E83EE7D3F07DC37BB7C205D7AC3C6988A8,
	MessageOptions_get_Parser_m53EF9155B5F709F4CBFA333CFE441C6948C7C918,
	MessageOptions_get_Descriptor_mD7174A123DEA36462F8D277A6F92BF72D2DA5E67,
	MessageOptions_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m50EE2D14CC4936D1C6557AF7B4E6AF42016AD648,
	MessageOptions__ctor_m04D30C43D35878336875570DC6A5E3E6BC862DC8,
	MessageOptions__ctor_m624EB105BF4DBD4102C96FBB4E33A6298249DADD,
	MessageOptions_Clone_m145FE6784EC0872E03A2A8F80ED305D98CA4B885,
	MessageOptions_get_MessageSetWireFormat_m63B35209206B97197CD6FD8307DE4144B17964F3,
	MessageOptions_set_MessageSetWireFormat_m8BDF032C4FA1BE4B05801F8B8D03B39A0666582F,
	MessageOptions_get_HasMessageSetWireFormat_m70C2E97A02C79B905B2BC5DF99A6C42A346AAAA4,
	MessageOptions_get_NoStandardDescriptorAccessor_m4C9C735876CF812FAC0C4C7D519C8C83F9E85E24,
	MessageOptions_set_NoStandardDescriptorAccessor_mE1FAD81D27D01BF43C2D329D1BFFC32A7948126E,
	MessageOptions_get_HasNoStandardDescriptorAccessor_m12E85FAC2F4B78894501B280E396820F6FDBB9C7,
	MessageOptions_get_Deprecated_mC1F396571D1C88388162E16C17B4FE00894068B6,
	MessageOptions_set_Deprecated_m0958446C63A923B48CA62EB2762C81FB7EC73667,
	MessageOptions_get_HasDeprecated_m79382B8AA4977F21658B9555ED6945B3C31C0E16,
	MessageOptions_get_MapEntry_m1C138926AEAAF672FEB8EEEFE10802BF21872A4F,
	MessageOptions_set_MapEntry_mC82D11ECA27A8EF638F709C2E0D73D0627605344,
	MessageOptions_get_HasMapEntry_mB5DADF5A359A828AFCFEC3B0ADBBE04E2CD2B55F,
	MessageOptions_Equals_m1B9DA96523A1EBF257B4C05943A355ABDEF8D4A3,
	MessageOptions_Equals_m140F555A06D1A7D663A75E01F530CE9504F73BCB,
	MessageOptions_GetHashCode_m5FB4F16D0E258B4EA5B190B1FC9BA4B92CE54A0D,
	MessageOptions_ToString_mD783887EDD978433A31E4DDCDF8A1F73C927BE1D,
	MessageOptions_WriteTo_mB3320A7754F94A90E6A9DC30D4FE1D3E6D933849,
	MessageOptions_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m47F229E21C08BCE7F2BE882F5811ACBBBF789A13,
	MessageOptions_CalculateSize_mC3FBEC0AC2FF4F8C44D2E5F8387DF31F177936A3,
	MessageOptions_MergeFrom_m79A658F6D709B1A47BF52BDFE737C37609FCC755,
	MessageOptions_MergeFrom_m3DF6E1BE37AE3869439B0A6FA4DA0BE58835014F,
	MessageOptions_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m3009CFC09F4F10050F4E663DD16E6151A0A3F556,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	MessageOptions__cctor_m462805F90ECB353428AF419F3978C00E6A6F2DD4,
	U3CU3Ec__cctor_m3FD417E3735579E27791E66F8C96C2EBAFD45AC0,
	U3CU3Ec__ctor_mABC8A3FD52E725A6409DAE462412C577E8BC2D0C,
	U3CU3Ec_U3C_cctorU3Eb__74_0_m01F1354E67601AD2A56A657600F0962C27829E24,
	FieldOptions_get_Parser_mBD73A7B75AC3AE5A81923A81B1DB409BE54A588C,
	FieldOptions_get_Descriptor_mC37D8081402EFCB32AC32E58B6A1D6FC199EA794,
	FieldOptions_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m7DFFB15C6EB7F4DFF583A2B9C08152C03B82B91A,
	FieldOptions__ctor_mAEDAAFD3E1E1476831562F5E223B4AF2F806D42E,
	FieldOptions__ctor_mEBACF18F7CD68BE7590F93E5181DEB9E8D8FB13C,
	FieldOptions_Clone_m2B5511B42C346DF381B638EBDD946CF86BA411F3,
	FieldOptions_get_Ctype_mA6843F20FFA5EB265B019001BFDF8E0E02D3CC8A,
	FieldOptions_set_Ctype_m6BC337EFC7D873F75ABC51876448674862E2D66D,
	FieldOptions_get_HasCtype_mADF13326B6F7D0AC6DFC3D8341088E44B85B14E3,
	FieldOptions_get_Packed_m4AE4E26FD463DBE5F8133A2A91320E5C494BA9DC,
	FieldOptions_set_Packed_m5DB5B5076854BAD5F123E60C13172B3C3E8BE3D4,
	FieldOptions_get_HasPacked_m44A791BEEB8477823C08D9217756E10A27906DD3,
	FieldOptions_get_Jstype_m9D6F072E771C39153BF21434200CE11B1A2A4377,
	FieldOptions_set_Jstype_mCE489B662A9B15437E7BC7A29D88FE5A2D1F1F75,
	FieldOptions_get_HasJstype_m606CBC541A4D02C27396A9B7BAE2C82CC7D69A0C,
	FieldOptions_get_Lazy_m57A6A0502DC600EE64748624A79C46F3369DCA31,
	FieldOptions_set_Lazy_mEE802290A41C3F0805530D8700FDFE08E0D69FA3,
	FieldOptions_get_HasLazy_m1CF72D570ED6CC8B184508095DE59865357F83F0,
	FieldOptions_get_Deprecated_mF0E44E4DF76BD6A70934E29858F576F60CAECE8E,
	FieldOptions_set_Deprecated_m8AEECA8FC9BD18F19AA1D664AD8A3425ADFD71A7,
	FieldOptions_get_HasDeprecated_mB31BECA44B454D97A914531CAADC91757942A9C2,
	FieldOptions_get_Weak_m7A3A4A8898C2A6F32C9F5B81A149F41661769440,
	FieldOptions_set_Weak_m78E446C9BC7216A1AA5BB97716EBC9E6BD2E4DDC,
	FieldOptions_get_HasWeak_m68798BAE7FC4C483D909B594AB866FB4B8D4E3CE,
	FieldOptions_Equals_m35E10FF061C593610F69C8A497CBBB5345B641DA,
	FieldOptions_Equals_mC82884DE34A33E7799B4B537DB3E5327FFF93DE8,
	FieldOptions_GetHashCode_m2C8BB8DAB5A6D69F940B9714BAAF1632D551814B,
	FieldOptions_ToString_m9E9A624E4C63E7B5FB16D55551A2B3E1BCE409F5,
	FieldOptions_WriteTo_mB619B555A1CF0FABD74BAB7646DAAEC68B206B4C,
	FieldOptions_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_mCE93A239A7677AE5873AAE5CB3E6F3979A60247D,
	FieldOptions_CalculateSize_m28F032BDF8603529AFE3FF729BF891A8B035A4F2,
	FieldOptions_MergeFrom_m78199E61A3B42CB84FC665C80A5C16C79E81E330,
	FieldOptions_MergeFrom_mC9159B562BFFE7A8E521072DCCD5F4D9E42043B4,
	FieldOptions_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m19D5BFCA155031073581BDEA524C8D1A712D5393,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	FieldOptions__cctor_m97F67E7667AC8FF8F4A94D63B04CDC1B0C773D7E,
	U3CU3Ec__cctor_mCD4BC8531FD960927751A5B7AA227FA1214B561F,
	U3CU3Ec__ctor_mA983EF284773415CD295C5EF5F558E82661C71B5,
	U3CU3Ec_U3C_cctorU3Eb__93_0_mA390BA56593D29C80E46DEF99F9821A284EA1198,
	OneofOptions_get_Parser_m7B0678394C34F9A7B292A180247CB002D4B0BCF2,
	OneofOptions_get_Descriptor_m8B5E60C8A1C89E3E748C9A976BC8F7AFA3290E67,
	OneofOptions_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_mCC08AF835625C824BE55AF335D89D0DA3F309090,
	OneofOptions__ctor_mCB3DEA19D4B2D302F3960474C65B68869AE65A6C,
	OneofOptions__ctor_m87704EF2A87D953EEE82B1634AB6326C00D36690,
	OneofOptions_Clone_m861C126BB5E08AD41DFCC26AF082E5E53435DB55,
	OneofOptions_Equals_mF33E711DDF4A91CE5F2BB106B53D2AC4688D058B,
	OneofOptions_Equals_mD4943C6868C3350846C89F186BABEF1E024925B6,
	OneofOptions_GetHashCode_m9F0EBEE5FCA5AC438B7DDCF82E19623AE486A957,
	OneofOptions_ToString_m472557171AD5BB71572A06F3169E632E1322C621,
	OneofOptions_WriteTo_mA65BE0E9F2AE867975411486CE00F66754F593DB,
	OneofOptions_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m63942D15544A3D5AC7BEB303FE4FA711E3364C69,
	OneofOptions_CalculateSize_m2018ADE928533BABBA3CCDEA40E9FC13978CC4F0,
	OneofOptions_MergeFrom_mBCB2053FE63A76031534E6E4CD2DBD96E73118D2,
	OneofOptions_MergeFrom_mD36D6943FAFB5C64CC30EA67CF7DFAD976C846EA,
	OneofOptions_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_mDF39C909A8356CFEBE50ADE3B724A1C7B306E84D,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	OneofOptions__cctor_mA657EF433F7836DC9101B67054175F0B03627DA2,
	U3CU3Ec__cctor_mF2353099F097FAFF0923EBA64043DA97C1EE522F,
	U3CU3Ec__ctor_m1699C6B6E16AB84D0B4240F3A2AA1839CF079492,
	U3CU3Ec_U3C_cctorU3Eb__37_0_m33EF6B2A034ECB2874C9EEB9C96479B5B99ED9C1,
	EnumOptions_get_Parser_m74C36E689CB3185F069FBBCE1852FB446B4FEFD1,
	EnumOptions_get_Descriptor_m055E04545550B61018A8CBFEBEF747141610DF4A,
	EnumOptions_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m2B62FA8955EC17A18D516DD20AA183ADDFECA920,
	EnumOptions__ctor_m5385E9F25EF8615050334EEC3708BA5725590EA6,
	EnumOptions__ctor_m6CC675C9336E6028155824F6AC523E84B9B72900,
	EnumOptions_Clone_m607D859024E1D3E98EADC2D87FE1A6004633A364,
	EnumOptions_get_AllowAlias_m8451F8AB3791D2D2D98A58909C89C0B27FB554E9,
	EnumOptions_set_AllowAlias_mBF2086F3EDBD93C60C083563B09B70899D937388,
	EnumOptions_get_HasAllowAlias_m516455BF0D491A603D3690B64A17AA780750759E,
	EnumOptions_get_Deprecated_m14967E7D273D984C34E8418D1FF0CC1F3B317032,
	EnumOptions_set_Deprecated_mC25625421D337CD31FD954015B842180C4ED5BCB,
	EnumOptions_get_HasDeprecated_mD5B251C2DAAD2B688DFC8E45C8029DB3918F89C5,
	EnumOptions_Equals_m7AE883DD0D520647C6A466B4B7731AA4C3ECC696,
	EnumOptions_Equals_mA6AEAD2E23DF4EA72CB302A48CA00FC4D7844CB0,
	EnumOptions_GetHashCode_mC319C69B06D57266DF45D6E68DBAD4A8A24CB638,
	EnumOptions_ToString_m5D36D78A38E16C3589A17C93AF80ABC42F5304F7,
	EnumOptions_WriteTo_m5689DE4D2F562F444B47E032A670D6F495472D74,
	EnumOptions_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m014FF1827E80308CE342F0F91A38C87AD1E6FFAA,
	EnumOptions_CalculateSize_mE57649652E7F429D04DDCB6BA25843D83D858366,
	EnumOptions_MergeFrom_m8783323089C9BE38786C2F133AE42096DF6374AF,
	EnumOptions_MergeFrom_mE5A3CD277AF799DB94EF81CFEBC4C84E13947F53,
	EnumOptions_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m7703AE4472D5FDEB96708EFA3BD302633F5211F4,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	EnumOptions__cctor_m724F897999CB7C4D707CF4B441621C453C1C6279,
	U3CU3Ec__cctor_mF63C23521E8F29F07A3F3DC0CB8257E25C472AFF,
	U3CU3Ec__ctor_mFA35CCDA24AD68084C39622FB3B4B0EAB2F193CF,
	U3CU3Ec_U3C_cctorU3Eb__56_0_m6C29F69BEC4A1E886865DFDE2E165CA490780A78,
	EnumValueOptions_get_Parser_m1DDDB5F095116785BF9C5F1110F5C66F29061810,
	EnumValueOptions_get_Descriptor_mD42AFD1FAC628908E9715418CD1BB395939EAD54,
	EnumValueOptions_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_mB0348AB4813E4A5349811036C55D1E4D4EF59C12,
	EnumValueOptions__ctor_mEDC4489E058024D0EE74CD1882B267796D0E6F05,
	EnumValueOptions__ctor_m1E94702D09BCE26AC8F7CBFDD1D52C1AEFBB183E,
	EnumValueOptions_Clone_m6C604B56C82AC36BE5F555CD73F471B01B831730,
	EnumValueOptions_get_Deprecated_mBB62B4E8F3B20B75EC4C85AC0B34446C7D94E6E6,
	EnumValueOptions_set_Deprecated_mCB6C503A0C948031B3DC7D7DC7DD12F6F6D8B0F4,
	EnumValueOptions_get_HasDeprecated_m842E47F888CC39366FEDF5F807ECE1BABCF0D5AC,
	EnumValueOptions_Equals_mBE4B57FD48FF5410BEB1956196C76DA4B5B451CD,
	EnumValueOptions_Equals_m4275B004B5AB046B0CCC1C05C4C9C88C672D4B27,
	EnumValueOptions_GetHashCode_m3CDA5B21FFC6387A8313F2E0230D63C5CC4D9699,
	EnumValueOptions_ToString_m689014E160C318BA3C63EFD328D3B0F7DCA8908E,
	EnumValueOptions_WriteTo_m2C38B59E54CC93366529373A55DCBBF1805B2FBC,
	EnumValueOptions_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m1E4497803CFE23240560E2EFDD0C4ED503A98338,
	EnumValueOptions_CalculateSize_m5CEBE3C36B5DFD8EB4139E0BC860D680C1A1EB19,
	EnumValueOptions_MergeFrom_m5ED39DC9CFB8446A5595EFED2D18B56BFE2F761E,
	EnumValueOptions_MergeFrom_m9CB27E7FFBCF59A93EFB81F345EEEE0018780465,
	EnumValueOptions_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m9A7A72C5D302BD0113B44F30F4D93A74E69CB7CD,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	EnumValueOptions__cctor_mE8B758BA5661472577315BF5F23766C4D042438B,
	U3CU3Ec__cctor_m339291422CD5DC70C5FE9F48EB6614CC17EC0E86,
	U3CU3Ec__ctor_m8BE7481CF1C66F1276CBFC6396B323BD610D674F,
	U3CU3Ec_U3C_cctorU3Eb__47_0_m15773815C34E5654CC3E45E9FD03CDBB768B3BAC,
	ServiceOptions_get_Parser_mE5EE0F3C480BFC70D4520E59B786CE266CD7990E,
	ServiceOptions_get_Descriptor_mA87B56F95F37999176DAFF804F73FCC24CEE3590,
	ServiceOptions_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m5CBA61A832DD81EA936FCEF5994429B4B938ECC9,
	ServiceOptions__ctor_mF53F4F57784851D29B2274A2B2C0B53A044FC5F2,
	ServiceOptions__ctor_mB0C390F674A6FD4B7E940F7DF26EE01E8B72486C,
	ServiceOptions_Clone_m548C4ABD6A7B28FF5D8339A2878E617F8FF0FC0F,
	ServiceOptions_get_Deprecated_m693CCD13364B54FEA9019FC75FF94D361457D435,
	ServiceOptions_set_Deprecated_mB85FD593FB12B41F625CBA2424FF36A008CCB047,
	ServiceOptions_get_HasDeprecated_m93C02BA34BCA0DF3289167D556788AECB2388C8E,
	ServiceOptions_Equals_m8F72F53030D77F97C862B939A18F0B803318DE2F,
	ServiceOptions_Equals_m8103227D46B27ED1BC216DB305C3973DC4F2DF76,
	ServiceOptions_GetHashCode_m6843CA5920027100EC678C9E12D958ABBB0B7A2F,
	ServiceOptions_ToString_m9C3A29E7B7D222E10DD900C0AF541A804B3130F8,
	ServiceOptions_WriteTo_mC3CCEB2117A0EFC47E65EBCFF2D95500CAB4F504,
	ServiceOptions_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m595286491DBE361A862243B1A0F5AA2C64F48739,
	ServiceOptions_CalculateSize_mF38C58E3DD52815641DE6B3734035AD147E28E37,
	ServiceOptions_MergeFrom_m0F4BD8AC991185BC2C06F1541C340B56202A6AE4,
	ServiceOptions_MergeFrom_m91ACECFAAC88BE4A3B9AF6E32A85D5ACD847507D,
	ServiceOptions_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_mB1A0155B8F628B062613DE44B49A8F6B680587AB,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ServiceOptions__cctor_mA12CD486E7D01A575C79ED676B4717E794D32D6D,
	U3CU3Ec__cctor_mE6820DD8647C46595D786FED96262C40BB604219,
	U3CU3Ec__ctor_m12F13970B895A83AE6585DEE9E0EF609A6B41FD4,
	U3CU3Ec_U3C_cctorU3Eb__47_0_m2C9FFFFE70006856529D67653183A12D3822540C,
	MethodOptions_get_Parser_m47FFE6E96180F8B34F52620D8477E9347A14B112,
	MethodOptions_get_Descriptor_m0051862801E9C1FDB008D4CF25CB5FBCEDB743FD,
	MethodOptions_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m2D1F1AA832041E1D63B55590B512598237C7F075,
	MethodOptions__ctor_m8E6C41386BE75CAF13EBCA4773DE2C9455DCAACB,
	MethodOptions__ctor_m5D84C74E7A241DC3FEB9E244C786A3CA5A9EA6C1,
	MethodOptions_Clone_m4F7F9963D8DD66C5CBCDD6DE8D9044C573755C7B,
	MethodOptions_get_Deprecated_m453B70C6DB6041AD7F9CEE21E15E3E0648E4A58F,
	MethodOptions_set_Deprecated_mEF21B0E81788197583497701AE63836A899719DB,
	MethodOptions_get_HasDeprecated_mCE0E857CD48CAEFC2FB4D9DFDACD8F25C32877A2,
	MethodOptions_get_IdempotencyLevel_m49E2B7AD7EC9962B4905DEED3F0D7ACBD3632D9A,
	MethodOptions_set_IdempotencyLevel_mBE4B4DB5A9D6C2BCD5B3F3BE761A412F0FFEEA83,
	MethodOptions_get_HasIdempotencyLevel_mB77C16E4DF253114A663A813E37C3FCD27857040,
	MethodOptions_Equals_mC19CF9BE571A1064D708C490AFBB4DF8508597E2,
	MethodOptions_Equals_mB3CF6D193D1EB5531625EA2135546118AF0F4DA7,
	MethodOptions_GetHashCode_m419CD1A58A8AFCDB6D673259EB2CD03A31400414,
	MethodOptions_ToString_m582EC2A358621D0F1398769E1AF5733E93B2C166,
	MethodOptions_WriteTo_m8DC61AF4DBB7A265BD61718BFB2BA3E149F4CED0,
	MethodOptions_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m308FFA3BF40A93AD6A3BFD1CE927BD5745F4BA4B,
	MethodOptions_CalculateSize_m7AE8BB6308CC41A549C3409B7DD10E7459F38347,
	MethodOptions_MergeFrom_mDBD557C9249B039843EEA039CFBD3A5604D9FCD5,
	MethodOptions_MergeFrom_m55F1821842F961B9E76D746DEB8F09AA2F93032D,
	MethodOptions_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_mD8A99A524FB7D62431721187321E3FCA9124E0F1,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	MethodOptions__cctor_m77AC2A0D5A7117AFEF6A250A907395621B97250B,
	U3CU3Ec__cctor_mC398777769724D087F9978E51197681D21637BE7,
	U3CU3Ec__ctor_m829CD2280A26CF144B536B9AD5A2D5BB7849A6D8,
	U3CU3Ec_U3C_cctorU3Eb__57_0_m0D85273A171DEA4DF6CCB946F5B9C7DED906FF32,
	UninterpretedOption_get_Parser_m8A25A0D8CD91A59AF3F96095A8C7FBF704082F87,
	UninterpretedOption_get_Descriptor_m178F55E2F36773B937FB50177A2EA1EED60B24BA,
	UninterpretedOption_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m84FA1CFC2A958FC52931B105C448510604E328A1,
	UninterpretedOption__ctor_m01FCE0DA392AF8E60B04329799B2FD66C2A4676D,
	UninterpretedOption__ctor_m11693D3C42B33BE6E844F5F6F2810E399BF413EC,
	UninterpretedOption_Clone_m0B1D69F3BE93968ACEAECA8835FEB1B5542ECE6A,
	UninterpretedOption_get_IdentifierValue_mD5B29DF167934C31919F2C72448397D93EE1C660,
	UninterpretedOption_set_IdentifierValue_mE9A55DB611D4CEDAA41B826B967A968F7D11DE1B,
	UninterpretedOption_get_HasIdentifierValue_m15054E0A8CE07585E05E389B861D6A64C2A9826C,
	UninterpretedOption_get_PositiveIntValue_m3A8C7B091C726FAC6F32BEC182C3D7AAF779F138,
	UninterpretedOption_set_PositiveIntValue_m14051B48D3FF3D6319209F0BAAEE164C37BBA7F6,
	UninterpretedOption_get_HasPositiveIntValue_mCFCEFB7BF8094469609A8206C715A4FACE91E1E9,
	UninterpretedOption_get_NegativeIntValue_mDF907ADC55BA0D7EAA07D1EF512DE2680794AE0C,
	UninterpretedOption_set_NegativeIntValue_m9879F18843C9ECC5D16B683870A87926E0AB78BE,
	UninterpretedOption_get_HasNegativeIntValue_mFAAE4B914DEC9FBB3D057FF01A0A51F091D7E73A,
	UninterpretedOption_get_DoubleValue_mBCA6B09141A76B1C9F58DEC365C1239A6854DB64,
	UninterpretedOption_set_DoubleValue_mD24E441551CA03B2182ED25B8F20588A3ADF3451,
	UninterpretedOption_get_HasDoubleValue_m01812D763A3048ACFC310A84F0471C3D60DDF27A,
	UninterpretedOption_get_StringValue_m1A6CF457CDA8BD51996A33AB200794F054D33F4D,
	UninterpretedOption_set_StringValue_m80C4D5B8F3CAD7B1DF1414C15C64698D403D9EEE,
	UninterpretedOption_get_HasStringValue_m170914175399C1398923F8138756A3BC76238264,
	UninterpretedOption_get_AggregateValue_mE551DA9DE1AD64384C5FA59D30D0D76A09255B16,
	UninterpretedOption_set_AggregateValue_m5F918A7FA8B1D9328AED89B89AAFB8785058F290,
	UninterpretedOption_get_HasAggregateValue_m8394E61DBE7BE1C1813823D1C63A073B525A6AA0,
	UninterpretedOption_Equals_m480D8D7A06EC8E37BDDAE05B1A4A3709F6FC0850,
	UninterpretedOption_Equals_m695AA83BD422C1433861AB2DFEA337174A603910,
	UninterpretedOption_GetHashCode_m9E5BF7D1BE4AA11718BA1841A1A0F0A010FC1ECC,
	UninterpretedOption_ToString_m5C88AF1EC3E062A8E98637D0316F62C9874A5E5F,
	UninterpretedOption_WriteTo_mE3E2D1AEAF4379F1E60A80DB89EDD27EE8285255,
	UninterpretedOption_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m790A1EEC06943590A53B5FE56456EFA59EB44034,
	UninterpretedOption_CalculateSize_m0073EE08E92667DDF1A0764667B6890C0765E0D1,
	UninterpretedOption_MergeFrom_mCDAB1F9A576C61CDC4FAA103105C9C3BED5DFCE9,
	UninterpretedOption_MergeFrom_m0464D25AE497BE73F7067D4A56C1AFB748FBB16B,
	UninterpretedOption_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_mD2F9D9A892C3A6A090D637C759815A6BFA835FE3,
	UninterpretedOption__cctor_mA6AA5DBFF01E41142A5CD7641F12FBAA4E9E37A2,
	NamePart_get_Parser_m52D44251BA5B107F48BA9D858E82CD1C2499F151,
	NamePart_get_Descriptor_mDAF09A4912482470FFDD7664FB4A245050AB4D99,
	NamePart_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_mB4EBA66DA1D50FDC6D037B624317EAF1C1EC424F,
	NamePart__ctor_mA9A9103300EE0850829D3BFE67B52EAD4512F085,
	NamePart__ctor_mFBC708208AD503A756034768ABF66A5E5E0AF4DD,
	NamePart_Clone_m43B2E0BD86099C758B706C19DB038F56A2409C6F,
	NamePart_get_NamePart__m745E8A77A6D196C10AEBD19CD97CD8FFFD6771F9,
	NamePart_set_NamePart__mB05B00F6AEB8BCC4C03176FB92DADB8C7A49B55E,
	NamePart_get_HasNamePart__mCEDB12E23C8A15CAD08E15A07D98285265637660,
	NamePart_get_IsExtension_mF1F181A90E440604432916AA943CE44DA7BADDEA,
	NamePart_set_IsExtension_m01E6A8E808AB54ACA78194DB40DC1D897F7713A2,
	NamePart_get_HasIsExtension_m5A187250EBDBA107E51D79C0BD502D42D5965B9D,
	NamePart_Equals_m9F4FC0E7DAF91E9141210A33B1AFE8F2127F68BB,
	NamePart_Equals_m4F68F7BFA6DC8252C7BF46F9B7B7A4561BC9B297,
	NamePart_GetHashCode_m84C037E032844843C3501CBBF1D5579160297F11,
	NamePart_ToString_mF10088283D361B38968A6E57F78255654D3BB2D1,
	NamePart_WriteTo_m9F9C644A5C5530F04A945671E7BBA3FEF7061448,
	NamePart_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m0396A0FC112B2CFF1E5B1FD1C0AE400359E96A01,
	NamePart_CalculateSize_mE593E657449DE890C6273E4C9DC3B90A948C8749,
	NamePart_MergeFrom_mB5380B100DD6F68EDEEF200E1BB000EFC4906DA7,
	NamePart_MergeFrom_m4889B005A581C203986474994D59CAD2FC9F5F30,
	NamePart_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m161542DC71B24D4416D0327851A5EFD2B8A093D2,
	NamePart__cctor_mAD02D8D937C525BAD836480091C7B3FEB91509C4,
	U3CU3Ec__cctor_mE8A52459A0655D42438ADBC5EF6EE84710B49777,
	U3CU3Ec__ctor_m14121C5533461FB8EFB0C4F01A4D5BD0AD91EA7F,
	U3CU3Ec_U3C_cctorU3Eb__41_0_m613BA3E40E15DB658425AEB73890F58D554969E4,
	U3CU3Ec__cctor_mBA81C0F546421AF123D609A914E9903942045AA4,
	U3CU3Ec__ctor_mF1AB9241F810C896EBA8E9BE5CC2AFD3A40D9229,
	U3CU3Ec_U3C_cctorU3Eb__83_0_m8B7EDDAF6E8803E0E6DEDFF72F6632B1415B7ACE,
	SourceCodeInfo_get_Parser_m8E4CF0002B4769C5F797021AB4ADFC77A656E8D6,
	SourceCodeInfo_get_Descriptor_mC3AEF456721098C0D729AFBFFF1365A7928CC547,
	SourceCodeInfo_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m3D927BDA5AA3529EFC6BF2C344AD0826C5C46AF2,
	SourceCodeInfo__ctor_m0A1DC09D0F3B17BC20F44D8201C5D0DB841145E9,
	SourceCodeInfo__ctor_m12C51AC307B6497E9CF7C2887ADB23E01B33392A,
	SourceCodeInfo_Clone_m879DC87218D95C6E0907ABB23C9F355F21C158BA,
	SourceCodeInfo_get_Location_m16B0802C3859FF4A0EAE4B0F211B4B60054FBAE1,
	SourceCodeInfo_Equals_mAC74617ADE26118FC282D20867FF972CF4B90786,
	SourceCodeInfo_Equals_mA2B30829479F4760E5B91E5B65D41B02889BED41,
	SourceCodeInfo_GetHashCode_mCD706831C8044FE179D8117D2526273B88B073A3,
	SourceCodeInfo_ToString_mE4FC459AEF9A122B2893BE043EFA5EF455BA1CAD,
	SourceCodeInfo_WriteTo_m11D19F228C061C27C584002104687141B0D514AE,
	SourceCodeInfo_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m601DD6F66EB4F4AF24126704725F9979A31EE648,
	SourceCodeInfo_CalculateSize_m5530408BED9841C7EDC134DC3B2CB88F2691B4CA,
	SourceCodeInfo_MergeFrom_mA3BFA5056081A4CFA0CE5DFF332A6815646EEF0A,
	SourceCodeInfo_MergeFrom_mEC664CB41897C3A5E540B205E64E6658866D29FC,
	SourceCodeInfo_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m1E724139176220624BC89C54831F1E7A68FA3815,
	SourceCodeInfo__cctor_mD3379D252A4597A98F0AC9B5C9F369E499A0C30C,
	Location_get_Parser_mB7AA6C4FB9B6FA6EB4B8C4CC5475D6AC028A10B1,
	Location_get_Descriptor_mAF9AF217303A7A050DD8AC21A40A039097EBCC08,
	Location_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_mF87BC274A989F4DBC54D998D5B5AA598C5644508,
	Location__ctor_mFDC279769BEAE3382B8775935C8683621235F622,
	Location__ctor_m28EFCE79F70879A58E3CB6F54CE731BD38BE41DD,
	Location_Clone_m3C4C0598AE1D773542602ABBFBFC912ABB270BEE,
	Location_get_Path_m84C8DAADFB80B1112A8BAEB93E662D7CE667384A,
	Location_get_Span_mFCF1B654E8BF0DB117024A4D1B6C6C1586C35D86,
	Location_get_LeadingComments_m7C54DD44CC201EEEA4654C52684874B58A20217C,
	Location_set_LeadingComments_m5C93169FF580ED160690B569AD1DB27A25CF1084,
	Location_get_HasLeadingComments_m3C25DA28CE42357EA74945D022AD19BB7CEC0997,
	Location_get_TrailingComments_m46A2E3D945E49EC181F43757653BA3D43A1F3F86,
	Location_set_TrailingComments_mD43CECC13E2083938944FEF61AEA9ADDD195E17A,
	Location_get_HasTrailingComments_mED1CB68639231AE0730E2FB6FB2C8875D89EF1F5,
	Location_get_LeadingDetachedComments_m83F1C5A77721B6E95FC9FA1EB78A456F542DAD7E,
	Location_Equals_m85AD8ECE48C60DADB8768733AF1F6563AF4AC281,
	Location_Equals_m8D982EACDA1888B6588CD7689C00D5EBB2985368,
	Location_GetHashCode_m6ACF33DBF8F3F18D5D7DD313D7729EA5839A174B,
	Location_ToString_m9B282141CD535FF8261C8195BF3CDDF528C40A4C,
	Location_WriteTo_m4210E1B1440977E2A413E0B8F75133982F73EC69,
	Location_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m504F51927BE7797E64ECDEC999C03BB04846EB33,
	Location_CalculateSize_m4FE7C1E7A7BB0E885F9A9779A16EC3D562B6BA3F,
	Location_MergeFrom_mFA64C2E2E2D47FD3294936AD3C696EAE3B0CD8F7,
	Location_MergeFrom_mD097D430111330C3EE05F9F06685B68DB4A7077D,
	Location_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m30960A0BA81FA14E2F5806B1821B17CEB6EC74A8,
	Location__cctor_m4F2D645AE7099736A3AA036CED46C3C27A92FB8A,
	U3CU3Ec__cctor_m311FEE18EEA023FC4C54A69ED1F10E1073A1A258,
	U3CU3Ec__ctor_m0D7F18F9E242952A4718672889E7A6D4750E8C15,
	U3CU3Ec_U3C_cctorU3Eb__55_0_m4EB0DCECA4202B15C7EC20815CEFDEBD1E0AFB9B,
	U3CU3Ec__cctor_m4DD420C5F69AC09CA65E4F8C2CDE5983F0877BC8,
	U3CU3Ec__ctor_mF19514F90700678D960AF81872A0077A86CC8772,
	U3CU3Ec_U3C_cctorU3Eb__28_0_m907FAD098B56E37E4A833213A6B36ABB4CE4FB74,
	GeneratedCodeInfo_get_Parser_m901B7DDFAAA3AF8FDC72519AD01DD576EA0FD055,
	GeneratedCodeInfo_get_Descriptor_m2A40BE0B9007A08DDF69F22C9D6B16B364E08361,
	GeneratedCodeInfo_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_mF88EDF6DA1CDE6AF97B451B5917A98DF5D8F0B6B,
	GeneratedCodeInfo__ctor_mDE0BB1D90115580D0C21C264412FD0C6BDC79827,
	GeneratedCodeInfo__ctor_m5588A1B1D68A18DA88CA5DCB4533BE39BA987B0B,
	GeneratedCodeInfo_Clone_mB2E07DB9A3075EAAF762DFDCE21F99B40521F491,
	GeneratedCodeInfo_Equals_m881F3A7864546DCD98F1AC8E5C753DA3B1AEC0A1,
	GeneratedCodeInfo_Equals_mA65F1EB2D6AE57EAB2AB0B72A3F4892252760755,
	GeneratedCodeInfo_GetHashCode_mB9F0A2A53CF8BEB45CBAFE12C8CFCB5819E31C3D,
	GeneratedCodeInfo_ToString_m325B0EB4FF2412BCDDAD96524195745F0517B9D9,
	GeneratedCodeInfo_WriteTo_m66B19D7405A8F6561D49E8A49165FE62712685DF,
	GeneratedCodeInfo_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_mE9F47B77861B199F58CF5AB795C22FA47A0E3FD4,
	GeneratedCodeInfo_CalculateSize_mB7F3C903908BB158E829E98EF242D7BE2070A981,
	GeneratedCodeInfo_MergeFrom_m2AF45EF1C2F717A5957EF6F07C088851083D83D2,
	GeneratedCodeInfo_MergeFrom_m87520A239A84EFAEA34062FD27265F10E6EE7BCE,
	GeneratedCodeInfo_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m68CF657C364AE67E08C90AA4854CA28F745E6AEF,
	GeneratedCodeInfo__cctor_m92C373068D04210DD09EA61906F904DFBDD968D9,
	Annotation_get_Parser_m8134CF19EF949C69B6DDD6103A008A1CF7A51BC7,
	Annotation_get_Descriptor_m03B3B365C873BFD9BECDEB8D977CF8C3CA4D161A,
	Annotation_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m838535C2735EA0FFFB83AB7E49836FBF2E1F51E5,
	Annotation__ctor_m9ED6A93F900EBFD3D4BC4980C3D9B032B8F9B5D7,
	Annotation__ctor_m61D9641ED59D1CDA766721C1A40198C8BF8F6E45,
	Annotation_Clone_mB86AE67CF1DB8FC7CF33ADDCEF2FEEB74BCDFE5D,
	Annotation_get_SourceFile_m91B7098C2E39ECA1A8E13E40DB8A7A740C3C80F6,
	Annotation_set_SourceFile_mB2AED44E9EFB3CA7D5BF5CA03DE3A8F5CE51723E,
	Annotation_get_HasSourceFile_mD261BC3AC77DB72DB7E1BEE3A7A8955D0913C23E,
	Annotation_get_Begin_m8891E38C06E537DCBAC9BEC72E83162AF4F6E9AC,
	Annotation_set_Begin_m5AA2016E643BF3BF22002C930EAACD7AE75ABDD5,
	Annotation_get_HasBegin_m86E2171FE0BDCBB1B5A075E45C7A8DDDB931E97B,
	Annotation_get_End_m1AB89595BE4271A333A470BDC5206C2814C050D9,
	Annotation_set_End_m5BDF9D827DEC0FF6DDA13230D1C99C57817FA7C7,
	Annotation_get_HasEnd_mF1171BE52E3B2EFB80EFD5480675B2655897470E,
	Annotation_Equals_m5C1A51E20393172CE798E42CAD95C3DD71E511C8,
	Annotation_Equals_mD66C9282185C31DF563EED063B1019BE13C2811E,
	Annotation_GetHashCode_m6F1BE21B2168F0D61B4E600E21DDC81A22132BF2,
	Annotation_ToString_m2E543D1B02E6207CB9009CEA5A987A6439466064,
	Annotation_WriteTo_m108B8AEABCE4F89A6E90997BAC8AC9371A53565D,
	Annotation_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m2D0F50AAD5686905CCAA2959D0C6E112FDA37E85,
	Annotation_CalculateSize_m1DE1ABE6707A9C1C6D415715E6EB90D7DDD98AB8,
	Annotation_MergeFrom_m2468E39EA5A58BCFA6F9E35D01002292E3CC450F,
	Annotation_MergeFrom_m8CA8D3549BCBA484E4BA0ADCDE8200E20CCEDFBD,
	Annotation_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m314FDFF4E0741C746FA60D608C45F8C01E8A9117,
	Annotation__cctor_m828EDEED29164F265B88F07BDC87ED3A6B0916AF,
	U3CU3Ec__cctor_m6812A7FE464B0BD846EBAB7F8B83B3E764B351DD,
	U3CU3Ec__ctor_mCC6C861EA46D5F38CE7B1D231D970DAFF8929007,
	U3CU3Ec_U3C_cctorU3Eb__55_0_m3FCF05CD6B82754BB25E87E1A3A8225796468906,
	U3CU3Ec__cctor_mE6C011350A2644975F83F6EDD92390F4D65EABF0,
	U3CU3Ec__ctor_mCC8B95876E8186437C0512E3622F829A8CDEC6BC,
	U3CU3Ec_U3C_cctorU3Eb__28_0_mE7837BAF9CA3B043285CE7FB0009A563519DE96F,
	DescriptorBase__ctor_m3CBAAC5484603E366E74D8555F06DAFD3CE18624,
	NULL,
	DescriptorBase_get_FullName_m915949C2A2E317ADAAA3141CB30DE432F1D6047D,
	DescriptorBase_get_File_mFBA850CF476DBD50BF1AB469555EAD69FFD827A4,
	DescriptorBase_GetNestedDescriptorListForField_m8CF298873F30256140DAB157A67A3B12F607E3A7,
	DescriptorDeclaration_get_StartLine_mECF4D235763DA0659A3E0E32D5ED6F286FAA6920,
	DescriptorDeclaration__ctor_m10B83BB65DA7F729601AD1D939AC0CFD5AC4F593,
	DescriptorDeclaration_FromProto_m99719C1FCF22A196785EBF1080E07B56AE12771E,
	DescriptorPool__ctor_m3446B1F10FAE222DCC0C07601F7643870492D4E4,
	DescriptorPool_ImportPublicDependencies_m3F1100ADD0AFF8C71E175D47D8BBDA8DDE0CB862,
	NULL,
	DescriptorPool_AddPackage_mC40972982B553A005B1CEE0FA8B6445C1C6E927E,
	DescriptorPool_AddSymbol_mC6C924B467AB9351A61B0E6ACFD993F6280AF69D,
	DescriptorPool_ValidateSymbolName_mA33C25FFD11FE6FCE83FEEF25295663C375766C9,
	DescriptorPool_FindFieldByNumber_mC43A5674F9B8820C8AAED2559A7CF18D9F4B65E4,
	DescriptorPool_AddFieldByNumber_m1517BFCF4BE7FB64ED7D2DD35698035103279EBA,
	DescriptorPool_AddEnumValueByNumber_mC17A3CEF22AE41D774AE5FB05525FFF69BAF3D97,
	DescriptorPool_LookupSymbol_m88CAFD3C6CA1FBDE5EADDEC6B49A0AA035612132,
	DescriptorPool__cctor_m90DDEA8CF7C71FF98B0AA65E5C7521A96A3E956A,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	DescriptorValidationException__ctor_mEE5B42C9FE1D522DE79FF92C402AE6B1304D250B,
	EnumDescriptor__ctor_mDA2A55D7BF57FD4C483811A71ECE81E8F610076C,
	EnumDescriptor_get_Name_m30805FE69B2F54A40BFE040B98CACFC8B86BAAD1,
	EnumDescriptor_GetNestedDescriptorListForField_m2350A2452DA44A3A1CE99B7D3B17638BEC742A47,
	EnumDescriptor_get_Values_mC452E4F3B573CFA9BCFB0066C2C2E591C56EF7CA,
	U3CU3Ec__DisplayClass4_0__ctor_m3CD1BD5E0682052BC2736E123DA1E0CD643F5F19,
	U3CU3Ec__DisplayClass4_0_U3C_ctorU3Eb__0_mB714443299145966BFEE6F1D77782F527C0BD297,
	EnumValueDescriptor__ctor_mEA61C9FBBDF476EDD95613E82D81F79A3408E13E,
	EnumValueDescriptor_get_Proto_mDCAD4DA8B13A5C4E600D9851320D498AAF568EE3,
	EnumValueDescriptor_get_Name_m2443B3332402EE679943A717757DE5D5FE4CCEE1,
	EnumValueDescriptor_get_Number_m8D2442A9BF95C527E5AD80513FB64DE556CEC87B,
	EnumValueDescriptor_get_EnumDescriptor_mC63A873A0076016527E5702DD577842216DCFCDC,
	ExtensionAccessor__ctor_m4ACA7FC1F7AAEF568FF3CFF3479691E5E10A14D4,
	ExtensionAccessor_get_Descriptor_m4D3230A77360F27ED2DDBCD1A52FBB7644A3BB9D,
	ExtensionAccessor_Clear_m29286BD928CB332BC464F5DB5483D2226B4D3DE5,
	ExtensionAccessor_HasValue_mE22212F94D6D425DAA7ECB3F9A34D3EAB5866C09,
	ExtensionAccessor_GetValue_m1EF5981EE49DE9FA7FEDCEAE261B42C046593B67,
	ExtensionCollection__ctor_mBC1AC9F2DDFF484254B83482F2AC84AF58E09C70,
	ExtensionCollection__ctor_m56E5E750DFBCAF06299847ECF6DD0D583D500F3E,
	ExtensionCollection_get_UnorderedExtensions_mE70760039915D512775C9C2193AE020D011B0FCD,
	ExtensionCollection_CrossLink_m37C7BCF7C6F0FBF1C9C542D491D6062DA1F9BD73,
	U3CU3Ec__DisplayClass2_0__ctor_m2ED777838168A9CC6F1BB1EC2AC8D02C681B068E,
	U3CU3Ec__DisplayClass2_0_U3C_ctorU3Eb__0_m558AD2CC97BBFEC6B85A7F348CCAA432FBE03A4A,
	U3CU3Ec__DisplayClass3_0__ctor_mB106836DF16F7428328204C09587D10A733F8A66,
	U3CU3Ec__DisplayClass3_0_U3C_ctorU3Eb__0_mA1A32BE5DEE2B7A1A971607E15393B67ECE985A1,
	U3CU3Ec__cctor_m63DAC6F6D5C73A9410138963A8F2A0F5B0D96789,
	U3CU3Ec__ctor_m1A360C8649F78B39FADAB4E32F8B8DCE6ED4C553,
	U3CU3Ec_U3CCrossLinkU3Eb__9_0_mA69041194D5AFD2AEB3994919282DD2C61DBEF4A,
	U3CU3Ec_U3CCrossLinkU3Eb__9_1_m007A16D2D19D320F04BFBB718E4D97541B2C2FCD,
	U3CU3Ec_U3CCrossLinkU3Eb__9_2_mB17CB78CECF7D04F38F880AC236D87C18D11B86A,
	U3CU3Ec_U3CCrossLinkU3Eb__9_3_m71224EA1CE8E870B632CC0125BCD2F957BCEC53F,
	U3CU3Ec_U3CCrossLinkU3Eb__9_4_m4CC0AD06B114348090A7EDEE490784E01BA71C9B,
	FieldAccessorBase__ctor_mDA90EDBE6ABD75ACD7CE4865E1AC0AF671166C7D,
	FieldAccessorBase_get_Descriptor_mB23889A8B4C924C6371B1BE7E93FF6AE85873ABE,
	FieldAccessorBase_GetValue_mFCDE1F63C6914FFBAAA9250C92099283CA476D04,
	NULL,
	NULL,
	NULL,
	FieldDescriptor_get_ContainingType_mBEBF37A2458327C9419A884CB13132958B0009CA,
	FieldDescriptor_get_ContainingOneof_m792FE02B65FBBEFDCFA68FCE08CE1B789C935BE9,
	FieldDescriptor_get_RealContainingOneof_m3226A16C8578309F0FA967FBAC9EB77AC9E5BC38,
	FieldDescriptor_get_JsonName_m37E80D133D994BDE854220D920790A1E1BBDAF50,
	FieldDescriptor_get_HasPresence_m8E6C4D1B98C4A2101F1CC09904BDCC27B6F94D04,
	FieldDescriptor_get_Proto_m193D4347BAF5B48C4CDACF7FEBEB0EB7ECB1A5CB,
	FieldDescriptor_get_Extension_m47CB4AEB81E5372FE16AF15451EE3DB8F96F95BA,
	FieldDescriptor__ctor_mD92149FF28235D1FC63C5A5A2B2FC0406E5AE75C,
	FieldDescriptor_get_Name_mB196272140AC4DEA270A676EFB4D21CC845A3780,
	FieldDescriptor_get_Accessor_m718155654529CB79855E2BFFC48E50B52EB5391F,
	FieldDescriptor_GetFieldTypeFromProtoType_mB9A10AADB44459B7297AB37B159F2F756C733DD4,
	FieldDescriptor_get_IsRepeated_m7F3022FC108ECC88C011541D4F9FD9C3E3055402,
	FieldDescriptor_get_IsMap_m3B9F4C45E70C951A309829EADB9511F613DFFB17,
	FieldDescriptor_get_FieldType_m8CDD564F9C32B9A063FD9F3445E07E4BAEB71BB3,
	FieldDescriptor_get_FieldNumber_mFF0472DC4B8038BA9084A1BF8B0891389E17C19A,
	FieldDescriptor_CompareTo_mA0749274411F2F1B494CE5076DA3F139080FCD22,
	FieldDescriptor_get_ExtendeeType_mFADEA70D2F9458933EABF6DA40185DE074B4B175,
	FieldDescriptor_CrossLink_m484E2D06235B6620B9048EF0A0B7CABCC4483E0D,
	FieldDescriptor_CreateAccessor_m6C2A60C7A38E1C04C77A45D8BF0D472BE9D3C92F,
	FileDescriptor__cctor_m1E81B80D00FA0C1EC79C602D09D33FE091C32FDB,
	FileDescriptor__ctor_m61879DEE1769B5A93F930CB2B582B5E8B8C7B51E,
	FileDescriptor_CreateDeclarationMap_mE75594435C5FC246AEE0CEFA165AD8B7884DC10C,
	FileDescriptor_FindDescriptorForPath_mAF95CCD6F8978D3C6D86EB058902E6E748400F3E,
	FileDescriptor_GetDescriptorFromList_mB2F7045F8B685140BC31D9CC1D50D9680E8FD86C,
	FileDescriptor_GetNestedDescriptorListForField_m1A075CC14E96C6BE62BC9A6179A6F169F9D27777,
	FileDescriptor_ComputeFullName_m1E33F379B1DF41015BAEF029AA5350FDEC2F0BF9,
	FileDescriptor_DeterminePublicDependencies_mE391E9B57841EDA9C511E5E127FFDDF42B1E56AE,
	FileDescriptor_get_Proto_m3861FEA6DC13D62F54CC04BD11AAAAA1CC9D235B,
	FileDescriptor_get_Syntax_mC6D5E49E01F937101A4C15FDA84933E5E3AAD38C,
	FileDescriptor_get_Name_m359361EF303E319D93FD2EE80C56C13BFCE88463,
	FileDescriptor_get_Package_mED949BF62398400887F1E3610052C3DB35D1CF35,
	FileDescriptor_get_MessageTypes_m55CD9B2EB841D0F8FF161F0DB15A2D67266C536C,
	FileDescriptor_get_EnumTypes_m52F61381D43042CA3A0B4B0FF30BB33ED4BE8D44,
	FileDescriptor_get_Services_m111C66F60C4E1EB7FCE260D433F35058563F9E39,
	FileDescriptor_get_Extensions_m3308E404E50DC055A4AF9EDAB142BA227CA7F4B2,
	FileDescriptor_get_Dependencies_m9E9EB5F41B5489AE519540C7027903300E4F5955,
	FileDescriptor_get_PublicDependencies_mAC7D2C8B65EA6260FD1E00427C865B073C26C20C,
	FileDescriptor_Google_Protobuf_Reflection_IDescriptor_get_FullName_mD6B5C71C93A7AC4808B8F36F5205AC4343FB0285,
	FileDescriptor_Google_Protobuf_Reflection_IDescriptor_get_File_m7A7CCEEF3A0850D5E2197D05E6B7BD7FC271D76E,
	FileDescriptor_get_DescriptorPool_m39E958D13A60EBE7CC79BD0456664D61900040A3,
	FileDescriptor_BuildFrom_mCD34070738CA05D87DD536AB4107D914BCED5742,
	FileDescriptor_CrossLink_m8ECB5727793304E7F249AE2CE766443038C5C62A,
	FileDescriptor_FromGeneratedCode_m8D03D78677A8EEED4BFD41D18722EC65306A1E0C,
	FileDescriptor_GetAllExtensions_mC4A06792ECB89A4169D169E0DB3230EE354D1525,
	FileDescriptor_GetAllGeneratedExtensions_mB3D508503C0A808F853E95B8E61BC6952AE7C1FB,
	FileDescriptor_GetAllDependedExtensions_m2AF9D46708DFB5D083FB77F8833ADD6E49AAC53D,
	FileDescriptor_GetAllDependedExtensionsFromMessage_mAF3C1E48FE0C8B9A01DCE72D1F0027A816D11E83,
	FileDescriptor_ToString_m298D74F239FB4E9968E5A4D143710A208FC19FC2,
	NULL,
	U3CU3Ec__DisplayClass2_0__ctor_m1BBBFEF67CF19FC3D913628B8267031E50EE7BD1,
	U3CU3Ec__DisplayClass2_0_U3C_ctorU3Eb__0_mF855F9915F2FDA29420683333DFCB44960768C73,
	U3CU3Ec__DisplayClass2_0_U3C_ctorU3Eb__1_mAF485315B60EA9EC6EA2889CDC446CFEAE199D2C,
	U3CU3Ec__DisplayClass2_0_U3C_ctorU3Eb__2_m741146A6C9B950182E1D5DF31CF8438B179F334F,
	U3CU3Ec__cctor_mB9D86B4B471E23B201A8BA720BEB46C09DB837FA,
	U3CU3Ec__ctor_m514F00EF3119B63948DF8181712FACBC1781C4FF,
	U3CU3Ec_U3CDeterminePublicDependenciesU3Eb__9_0_m2F98535BA4236CD9DDDF38E84AD5B067271D7C72,
	U3CU3Ec_U3CGetAllGeneratedExtensionsU3Eb__53_0_m48DE89AAF572557D4B64DD307D20EC96AB9EF794,
	U3CU3Ec_U3CGetAllDependedExtensionsU3Eb__54_0_mDDE483E7D148A30C453B855CABB4C6FFF1D0432B,
	U3CU3Ec_U3CGetAllDependedExtensionsU3Eb__54_1_m1895951D33B2B511E79771BC2BC40BE852896663,
	U3CU3Ec_U3CGetAllDependedExtensionsFromMessageU3Eb__55_0_m65C957D175FDD3779E86AC7E48FDBF9EBD29A899,
	U3CU3Ec_U3CGetAllDependedExtensionsFromMessageU3Eb__55_1_m1757252CFE8BD09C5192725451A9C30ED1CC1C85,
	GeneratedClrTypeInfo_get_ClrType_mBC268EA87D91A720FC864656F282267E52E5C415,
	GeneratedClrTypeInfo_set_ClrType_m47923E24303E58B15765BA976560FF54060AA463,
	GeneratedClrTypeInfo_get_Parser_mE306A707C2E98793AE20F8F22D73682256A6825A,
	GeneratedClrTypeInfo_get_PropertyNames_m9AC3AF88BE6E4401E8CBE61D2A27205D494C7537,
	GeneratedClrTypeInfo_get_Extensions_mBC35C072C48C25BC9E3E10930E7ABC899E7D0BD9,
	GeneratedClrTypeInfo_get_OneofNames_m8225DF12A9536F188BB8F84100E0026AD38398D3,
	GeneratedClrTypeInfo_get_NestedTypes_mC8CDA4ACD8F8B83388C64EFB5C63E8841E8B7767,
	GeneratedClrTypeInfo_get_NestedEnums_m124D8277A92DC1AD640C130ABE855B6C9B55EB82,
	GeneratedClrTypeInfo__ctor_m87AFD6307EE56C2709AE4416F880230CE5ABB33D,
	GeneratedClrTypeInfo__ctor_m8348FF9744C6AE72A6F4BE44BF26E1F64FE9CA0D,
	GeneratedClrTypeInfo__cctor_m7FBF9CA32832A453B8E2AEA077F3C68B9116B559,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	MapFieldAccessor__ctor_m61136C9F4623BC5A3317ABCC64189464D185B668,
	MapFieldAccessor_Clear_m52A48E029A7C026DB49C871F5A2034B20D933971,
	MapFieldAccessor_HasValue_m50488B3129E6ED46577A49DA005FCA89E9356FE7,
	MapFieldAccessor_SetValue_m8F5FF224C709F04189A2A702C0490D548E50CC91,
	MessageDescriptor__ctor_m697D82B75EF84C2BFB0B3496E4969F100DE9EAA2,
	MessageDescriptor_CreateJsonFieldMap_m3BCA8184AD8B50F81DDA4258D37A4803D7016ED4,
	MessageDescriptor_get_Name_mF83C7F30F48B7153D53ECEFDB5907712AFD7CDA8,
	MessageDescriptor_GetNestedDescriptorListForField_mE4C88416F59B1501F97D889CD095B65B043A0BE9,
	MessageDescriptor_get_Proto_mC19705FDFDE1B7189CE45B38771704217266D87C,
	MessageDescriptor_get_ClrType_m67EDD051E54EF81C60E7C10479F1EAC25F222FFB,
	MessageDescriptor_get_Parser_mBA1FD49A96FD0E2AC88955EA6BCF536555E9B68C,
	MessageDescriptor_get_IsWellKnownType_m098E92A49A8B04698C1147852A8FEE73837DB432,
	MessageDescriptor_get_IsWrapperType_m4F0204430E2A7A742569380A4C0D7647337020B1,
	MessageDescriptor_get_Fields_m7EC5DF1D21CA98B0E798ED2171C276DEDAC39352,
	MessageDescriptor_get_Extensions_mEE45D3BA082A29D28E2390D85156DD1E9F579155,
	MessageDescriptor_get_NestedTypes_m34A524E3C7D560C1537685CF6291AB60DA58FE7E,
	MessageDescriptor_get_EnumTypes_m663896B42139FAB5706A6408FA931F95A60C3F39,
	MessageDescriptor_get_Oneofs_mC26EB7828846AC0A04AB6F892C4D22E79CD1E364,
	MessageDescriptor_FindFieldByNumber_m1E924845EDE30B6290C3D286A5321A48A7162CA7,
	MessageDescriptor_CrossLink_m730D7BEC4896228D0EE1C8342BBFC2C39C5C3CCE,
	MessageDescriptor__cctor_mA6B7A9F65008C855243566808C3A798EB574F67A,
	FieldCollection__ctor_m635BF532C669D474F0DBCEE3C341D1EBB85A8575,
	FieldCollection_InDeclarationOrder_m8973915B3DDF9F261F5DD019B1F96365D9D1E2A8,
	FieldCollection_InFieldNumberOrder_m524EBA77C464F653AAA2966BE3074D779452EF9F,
	FieldCollection_get_Item_m72E66500B2AE74CF46AF26C874E489EC7C90DC5D,
	U3CU3Ec__DisplayClass5_0__ctor_mD5B8E9D6A516E2B443B7E3706D42B8F49DB59C9E,
	U3CU3Ec__DisplayClass5_0_U3C_ctorU3Eb__0_m7F7296467B42A2D888BF6E956523A2CCAA0DB7E9,
	U3CU3Ec__DisplayClass5_0_U3C_ctorU3Eb__1_mDCD04AA0B2B183BFE77262DE788E2E41A9E7B976,
	U3CU3Ec__DisplayClass5_0_U3C_ctorU3Eb__2_mC76375B9A8C8ACD417F8D3D12B0938EF31D3356B,
	U3CU3Ec__DisplayClass5_0_U3C_ctorU3Eb__3_m8C77FD6177F520BAF3853864945C7605A3E204FD,
	U3CU3Ec__cctor_m7927D07E530D7A18F9955C58168BE142DB3D17C8,
	U3CU3Ec__ctor_m656123E4D759619026DC9C0A1C31EB10D5D92771,
	U3CU3Ec_U3C_ctorU3Eb__5_4_mE110F0F6FFC9418FA99CD3FEB029514ABD0A7CEB,
	MethodDescriptor__ctor_m132CEA4CAD2205D2A73DA0AFDB62ECB48D3DD721,
	MethodDescriptor_get_Proto_m85B3E65F7AB108C8BF212BA6B373614DFEF5B7EA,
	MethodDescriptor_get_Name_m51107131036CE66EB661A0575C507E2F75A916BF,
	MethodDescriptor_CrossLink_m1C07F242D0EEC94C7754FB8A8D0BCB15D70D7173,
	OneofAccessor__ctor_m04638176ACFC67393E75EE9B90815DF94B1BCB4F,
	OneofAccessor_ForRegularOneof_m077D8097F0102527660A43A5150DDF02786998D0,
	OneofAccessor_ForSyntheticOneof_m97A860E2AB9B35D8ADAFBE180D755833D2482650,
	OneofAccessor_get_Descriptor_mBDAF0F18B7C8EBBC4C2CF19A267C0AE3F09785C5,
	OneofAccessor_Clear_mB06FD88CAB211F03CCA2F11D5449C1BD8B4385D1,
	OneofAccessor_GetCaseFieldDescriptor_mD29158042A855E4F866DF7F40864DCDDB499A6AB,
	U3CU3Ec__DisplayClass4_0__ctor_m54A4FD9D7E8331F3560233E3633FD4C55502C4E3,
	U3CU3Ec__DisplayClass4_0_U3CForSyntheticOneofU3Eb__0_m8464C36B95B2F3EEB07E1C9BAAD27CE57C81D22B,
	U3CU3Ec__DisplayClass4_0_U3CForSyntheticOneofU3Eb__1_mE617B86AD9732907F68C010E8EF97189B415E9DF,
	OneofDescriptor__ctor_mE372C893E379410EDD55F3F7938EFCEB9033E18C,
	OneofDescriptor_get_Name_m43F733262646E17BC195D6E953894198F6F6ABFF,
	OneofDescriptor_get_ContainingType_m40130FC0260EE9E246C5181B36F9334DE4D16D58,
	OneofDescriptor_get_Fields_mC0948EC448A32E6CFA1B80878E4D859689BCB2E2,
	OneofDescriptor_get_IsSynthetic_m22DC2931DF30D51ABB96C336D1F8A89F37898D17,
	OneofDescriptor_get_Accessor_m733D8CEEB45918E4CB9DA558BA04BE5B4040C308,
	OneofDescriptor_CrossLink_m13EB3F795A61006620EEB3BC83886F6C41F094D8,
	OneofDescriptor_CreateAccessor_mEE00EA091CED277D753F95233CC66CC178DAC50A,
	U3CU3Ec__DisplayClass4_0__ctor_mC4C62E7CBD32174E07B91502F2E90039AA2C2458,
	U3CU3Ec__DisplayClass4_0_U3C_ctorU3Eb__0_mE51D4E8DD73DCEE3E47EDF2C7532EFFD88E5E1E4,
	OriginalNameAttribute_get_Name_m35E068ED514B51E172CF5FE5DD917801F045B819,
	OriginalNameAttribute_set_Name_mC0A333EF33378D9143B3077B4E86270FE05636DA,
	OriginalNameAttribute_get_PreferredAlias_m8B1D004B47D36373572F40238135A599E23095B8,
	OriginalNameAttribute_set_PreferredAlias_m5717AA7D6B145D079B78DE15B31B88C8D6B4C258,
	OriginalNameAttribute__ctor_mB0C6C38C49B9261D2DC423D692E80B8EBC657BF9,
	PackageDescriptor__ctor_m894073A12D24F6326EEAAD2B410485ADBAA0E6E6,
	PackageDescriptor_get_Name_m0F6A6231BEC61A25B1974D9B34DAD9E96FE966EA,
	PackageDescriptor_get_FullName_m567C159C73D1ACFC2026E89DDB985443136D2A28,
	PackageDescriptor_get_File_m29E90BB5391086BE17A857FCC9FE9B795185FD12,
	ReflectionUtil__cctor_m0BEBC8EADA0FDC0B5BFD115604E20C6A235A00A2,
	NULL,
	ReflectionUtil_CreateFuncIMessageObject_m7EB7E65C8F968EFC76315D476F6CABE4DED75C36,
	ReflectionUtil_CreateFuncIMessageInt32_mEB9452DEEB7DE855185B7D73D741A997FE77CE83,
	ReflectionUtil_CreateActionIMessageObject_mB18243688BDE03EDD030D7237CA2062DE1826F4B,
	ReflectionUtil_CreateActionIMessage_mA298FA5EF2B7C1182C43A6411C245705A4D74341,
	ReflectionUtil_CreateFuncIMessageBool_m4A9ED81E058059190B75C43C95DC86F52317C2FB,
	ReflectionUtil_CreateExtensionHelper_m326120533AE4AE52669353D223874610B37A53C4,
	ReflectionUtil_GetReflectionHelper_m92A3653AB63EEB04F467485B59F7E4DCA1E6C6DF,
	ReflectionUtil_get_CanConvertEnumFuncToInt32Func_m14B4A020233464D727830248EED426EBDE5D45EF,
	ReflectionUtil_CheckCanConvertEnumFuncToInt32Func_mE6C18ECB39982549C3500A8DD65603E20177B7CF,
	ReflectionUtil_SampleEnumMethod_m3A940A9C50EEAB34B229B0A4E3BE3A3375E40A1B,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	RepeatedFieldAccessor__ctor_m3811007EB321C48D609E209F0356C5EED92EE31D,
	RepeatedFieldAccessor_Clear_m4B471923A3DD82F2F63F0C2B265B4D3DD481E5B1,
	RepeatedFieldAccessor_HasValue_m51C2E466833B840D288A62390BF36EA7CB83F51A,
	RepeatedFieldAccessor_SetValue_mB77B2AEBEBB96A4FFB4A2840FFAD6E97F0EE8C11,
	ServiceDescriptor__ctor_m1E35DA06921850E3F74D47AC2A2EAB94A5C0A49B,
	ServiceDescriptor_get_Name_mA5DB087BFBB418946B706E72C018817BFA1A21A3,
	ServiceDescriptor_GetNestedDescriptorListForField_m090316EBEC0E64AFF243EFAB68991305AA1A6557,
	ServiceDescriptor_CrossLink_m09200D1B7D2C5D5D7CBCC95F7F4A7A340B552BAC,
	U3CU3Ec__DisplayClass2_0__ctor_mA57CD15AFF22F93543EB4483302868520307C699,
	U3CU3Ec__DisplayClass2_0_U3C_ctorU3Eb__0_m8B1C23CBA2E35B1EBF430937E0688DA3344E0523,
	SingleFieldAccessor__ctor_m6EF8CEFCEF1A3AE8291868B6AA8D65C1487C0029,
	SingleFieldAccessor_Clear_mE6BB9FF9FBB353A7935FCC1027FE3C8BD0FA61AB,
	SingleFieldAccessor_HasValue_m3140954CBB9A3FC88D46ED762AB4870AC8A51C05,
	SingleFieldAccessor_SetValue_m1D4ABBA8146D08B8122B1C60616B93651D763455,
	U3CU3Ec__DisplayClass3_0__ctor_mDC1F9AC11E1EC64AFE7BC876EA7437E050D0485A,
	U3CU3Ec__DisplayClass3_0_U3C_ctorU3Eb__0_m4D68499F01F2D65C8F3BC4979D1FEA22C5951F70,
	U3CU3Ec__DisplayClass3_0_U3C_ctorU3Eb__1_m012D25B971005EF2116E2694C55B4F456E202517,
	U3CU3Ec__DisplayClass3_1__ctor_m4A4866967E04FE04C8939565057C67AF9240DF90,
	U3CU3Ec__DisplayClass3_1_U3C_ctorU3Eb__2_m9BE17FECA2DF3B7BD94067194F4F625C729441A1,
	U3CU3Ec__DisplayClass3_1_U3C_ctorU3Eb__3_m8428E2627162B464F374C62DA9929B4441D56C30,
	U3CU3Ec__DisplayClass3_2__ctor_m223CDF862764DE5D298ECEC01BB24B61AC86A3F8,
	U3CU3Ec__DisplayClass3_2_U3C_ctorU3Eb__5_m5DC9C0EA88D7E40F6E6B5A05C5DD0D56BA9A0B0B,
	U3CU3Ec__cctor_m180CF70E3704C30ADA4200909331204FF0AB4FF1,
	U3CU3Ec__ctor_m7041048761CB9F36498D056F57D33017D3F10A01,
	U3CU3Ec_U3C_ctorU3Eb__3_4_mB4EA619F55628AC055F7D2A4372316C6438523C1,
	TypeRegistry_get_Empty_mE5DF84BF4F8096E4E4DEC88EB1B60542256F54C9,
	TypeRegistry__ctor_m015B123424761E68E7F35E8C70AD3B1702B532EB,
	TypeRegistry_Find_m4DBDC19FDD721BEAF8C9BC5223871AF618A067E0,
	TypeRegistry__cctor_mECDF4DAF9F427B14CBD2F7E37E07CB4243B0A3AF,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ProtobufEqualityComparers_get_BitwiseDoubleEqualityComparer_mE44F7862345571F0CA657DC541C39FE262543714,
	ProtobufEqualityComparers_get_BitwiseSingleEqualityComparer_m5E6A6EB33216A3E9E829ED8FBA161E0B7BBCCBDB,
	ProtobufEqualityComparers_get_BitwiseNullableDoubleEqualityComparer_m137AF49F6F51AC6745E79BB1EDDA611DCDCEE632,
	ProtobufEqualityComparers_get_BitwiseNullableSingleEqualityComparer_m903E7CC373C92BA08C7D8DC4B5569774DFB5A090,
	ProtobufEqualityComparers__cctor_m223E93E99644A8615959C1427CF098ED377FC4D6,
	BitwiseDoubleEqualityComparerImpl_Equals_m3D1DE5B018777F8054F4498447E193DE57E1377A,
	BitwiseDoubleEqualityComparerImpl_GetHashCode_m17D9A5AD333C5C4259F628C9AA83BB0B0518D869,
	BitwiseDoubleEqualityComparerImpl__ctor_m42230DFBD6329922D97BA6121A5869C56873161E,
	BitwiseSingleEqualityComparerImpl_Equals_m8AB7DD8696608B73490722B7D6905D89F057A70A,
	BitwiseSingleEqualityComparerImpl_GetHashCode_mEA1C8E4E164520A05A12B8EF07FB96F01FAC6D5D,
	BitwiseSingleEqualityComparerImpl__ctor_m7480C419D32175FA06D087D0858E36AC01CE2F26,
	BitwiseNullableDoubleEqualityComparerImpl_Equals_m49F8CDD266A123D145A38883D55CB4101752A4C6,
	BitwiseNullableDoubleEqualityComparerImpl_GetHashCode_m17468884ABAA2B04E0C699D1A0E566CAC371AF61,
	BitwiseNullableDoubleEqualityComparerImpl__ctor_m3F13ED9C4964530540A1B7AB182C63423C1C8123,
	BitwiseNullableSingleEqualityComparerImpl_Equals_m71EF7BDCCE81CC09EC352D957239E077848BD442,
	BitwiseNullableSingleEqualityComparerImpl_GetHashCode_mADDC36D6849BE45D0FF7E8E9EFB28F3EBF8530C9,
	BitwiseNullableSingleEqualityComparerImpl__ctor_m7C52BA8E085815257246D2FC6537F17AE8CC3C5B,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
extern void ParseContext_get_LastTag_mD9E2763BC34A1111408D24771A9AC105C074DD53_AdjustorThunk (void);
extern void ParseContext_get_DiscardUnknownFields_mB67E8578452722E0B978BF3D107532C93D3268CB_AdjustorThunk (void);
extern void ParseContext_get_ExtensionRegistry_m4B2B015EBC8CA7328C07E164A6671545CCBD0F00_AdjustorThunk (void);
extern void ParseContext_ReadTag_mEE687A18078235454AEA63F1F61E9E3E9EB0260F_AdjustorThunk (void);
extern void ParseContext_ReadDouble_m8C106CDD420E7FA1311F33B2EAD994E4FD6A99DB_AdjustorThunk (void);
extern void ParseContext_ReadFloat_m1B6730B5A824501C0163F2DE6693B2F3B5939BC8_AdjustorThunk (void);
extern void ParseContext_ReadUInt64_m4E6FC396669BEAB285DFD47E650004F92AC16A48_AdjustorThunk (void);
extern void ParseContext_ReadInt64_mEDF40EF10BA05D0959FAD12976873ACF68163D4D_AdjustorThunk (void);
extern void ParseContext_ReadInt32_m275087DD36E27495CC2EE76EEB08BC90EFAF94B9_AdjustorThunk (void);
extern void ParseContext_ReadFixed64_m196FF86F1C53A073A179AB5CA93828CDBA3052ED_AdjustorThunk (void);
extern void ParseContext_ReadFixed32_m74BD7221FF3B1898D374C4E03EC036C576C677A2_AdjustorThunk (void);
extern void ParseContext_ReadBool_m097C5083B8554A5F33B1799DD8BF30926A5942BE_AdjustorThunk (void);
extern void ParseContext_ReadString_mA21E24240255C281D36669A209628AD6FDBB15AE_AdjustorThunk (void);
extern void ParseContext_ReadMessage_mA72BE697566685D137AE783593B8AB4558090118_AdjustorThunk (void);
extern void ParseContext_ReadBytes_mB08BDD0C46819718BA3813A1A943264DEAC283C5_AdjustorThunk (void);
extern void ParseContext_ReadUInt32_m961D49845C792A0B46CEA9E5B57FB50390FA5FDE_AdjustorThunk (void);
extern void ParseContext_ReadEnum_mB005F6D2C670173280B8EDB79CE4150B83F08374_AdjustorThunk (void);
extern void ParseContext_ReadLength_mABDC3E98A6A4A46C6102FF84C40C42A6C1F0EC87_AdjustorThunk (void);
extern void ParseContext_CopyStateTo_mC016DFBBF2BAF69F8A740391DA9F3C25BB94E273_AdjustorThunk (void);
extern void ParseContext_LoadStateFrom_m74FA6FF020C2458A8787FC3E7643E624517F4CB4_AdjustorThunk (void);
extern void ParserInternalState_get_CodedInputStream_m245B3E90A4D6AC379449EA79C3D1A4E2526BD712_AdjustorThunk (void);
extern void ParserInternalState_get_DiscardUnknownFields_mABFB91062F6B1A730ABD81691F138C516B0E3020_AdjustorThunk (void);
extern void ParserInternalState_set_DiscardUnknownFields_m6374B0D645A0352B3BF53F9B0EFEF62E45501C58_AdjustorThunk (void);
extern void ParserInternalState_get_ExtensionRegistry_m8ABCB61553B7732DBB4DB08481D898E5479347CE_AdjustorThunk (void);
extern void ParserInternalState_set_ExtensionRegistry_mEA5D5DD0A75713D26A2300501B940EB7A67C2B5C_AdjustorThunk (void);
extern void SegmentedBufferHelper_RefillBuffer_m75ABD1D414AA5D56ECB052EF58E151DA8468893F_AdjustorThunk (void);
extern void SegmentedBufferHelper_get_TotalLength_m1F9022D9A2F697A6F81FF34ABA719D983C7926E5_AdjustorThunk (void);
extern void SegmentedBufferHelper_get_CodedInputStream_m5FF2CFFD3C0EF5E732067FA923D9A1059C51E56E_AdjustorThunk (void);
extern void SegmentedBufferHelper_RefillFromReadOnlySequence_m6205503EFBA7900C8DC05FB228F499EF57D76910_AdjustorThunk (void);
extern void SegmentedBufferHelper_RefillFromCodedInputStream_mD18F57A2D807F9653438B2843E58D82E6B58C04F_AdjustorThunk (void);
extern void WriteBufferHelper_get_CodedOutputStream_m92805AD7A4248AFD24342FE0F3AF1ECA9E67CAB1_AdjustorThunk (void);
extern void WriteContext_WriteDouble_m36DE9708C2E9F55E08DF1593168026CD929B4114_AdjustorThunk (void);
extern void WriteContext_WriteFloat_m36D978A87046CB34CE1857EAE1079DD9B8972248_AdjustorThunk (void);
extern void WriteContext_WriteUInt64_mE585A8A413E7581324D6B61B063DAB8CD88D4383_AdjustorThunk (void);
extern void WriteContext_WriteInt64_m20616F89120F9551E46B5074B6C14823C97D0C4F_AdjustorThunk (void);
extern void WriteContext_WriteInt32_mCC3646C9CF9A13E2BEBC15A8E566669C913D4F65_AdjustorThunk (void);
extern void WriteContext_WriteFixed64_mAE849DB91093D95337CB4164CA83C8E2B25C7716_AdjustorThunk (void);
extern void WriteContext_WriteFixed32_mECD70B0022C1DFBE81B61366495335BE6B297AF8_AdjustorThunk (void);
extern void WriteContext_WriteBool_m68B7D58774A48FFC3FF34D9A6F770E579EEFA7DF_AdjustorThunk (void);
extern void WriteContext_WriteString_m1A0F46C5CF383B3F976BFBD24B546EC2E346C68C_AdjustorThunk (void);
extern void WriteContext_WriteMessage_m191D2C3E865C097E18DA174B172FFFA9685C8B98_AdjustorThunk (void);
extern void WriteContext_WriteBytes_m6483E64A6655BB2E1499AD548C549B47FB5EB3E5_AdjustorThunk (void);
extern void WriteContext_WriteUInt32_mA6366289E56066C7C0AB06218534DCF1F5C75353_AdjustorThunk (void);
extern void WriteContext_WriteEnum_mE9C54A63A11FA8F28864359CE2236760B7C7AABC_AdjustorThunk (void);
extern void WriteContext_WriteLength_mEBAD2E61EA0B60DD8789138399357B2E42C7BE4F_AdjustorThunk (void);
extern void WriteContext_WriteTag_m610740BFC2CB1D051CF1DCC94FFE749C801584C9_AdjustorThunk (void);
extern void WriteContext_WriteTag_m6041201C0BF3CBBCE75D5DD21474E69BC4CD80EF_AdjustorThunk (void);
extern void WriteContext_WriteRawTag_m3E37F0B83B4E8B7C7E1CC6472D527AA22B11DA5A_AdjustorThunk (void);
extern void WriteContext_WriteRawTag_m3889E0A7DCDBEE6B057EECBB614B022B4D4F991F_AdjustorThunk (void);
extern void WriteContext_CopyStateTo_mB67D2A3DB1B06B506C65BEDC5CC7EC1FC3A3FB97_AdjustorThunk (void);
extern void WriteContext_LoadStateFrom_mF406ADA99F6893BA926A0AD8950B8A8EF85F2D7A_AdjustorThunk (void);
extern void WriterInternalState_get_CodedOutputStream_m3601FE09E7D068C19BBEE6B3DEAB889130C983C3_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[52] = 
{
	{ 0x06000137, ParseContext_get_LastTag_mD9E2763BC34A1111408D24771A9AC105C074DD53_AdjustorThunk },
	{ 0x06000138, ParseContext_get_DiscardUnknownFields_mB67E8578452722E0B978BF3D107532C93D3268CB_AdjustorThunk },
	{ 0x06000139, ParseContext_get_ExtensionRegistry_m4B2B015EBC8CA7328C07E164A6671545CCBD0F00_AdjustorThunk },
	{ 0x0600013A, ParseContext_ReadTag_mEE687A18078235454AEA63F1F61E9E3E9EB0260F_AdjustorThunk },
	{ 0x0600013B, ParseContext_ReadDouble_m8C106CDD420E7FA1311F33B2EAD994E4FD6A99DB_AdjustorThunk },
	{ 0x0600013C, ParseContext_ReadFloat_m1B6730B5A824501C0163F2DE6693B2F3B5939BC8_AdjustorThunk },
	{ 0x0600013D, ParseContext_ReadUInt64_m4E6FC396669BEAB285DFD47E650004F92AC16A48_AdjustorThunk },
	{ 0x0600013E, ParseContext_ReadInt64_mEDF40EF10BA05D0959FAD12976873ACF68163D4D_AdjustorThunk },
	{ 0x0600013F, ParseContext_ReadInt32_m275087DD36E27495CC2EE76EEB08BC90EFAF94B9_AdjustorThunk },
	{ 0x06000140, ParseContext_ReadFixed64_m196FF86F1C53A073A179AB5CA93828CDBA3052ED_AdjustorThunk },
	{ 0x06000141, ParseContext_ReadFixed32_m74BD7221FF3B1898D374C4E03EC036C576C677A2_AdjustorThunk },
	{ 0x06000142, ParseContext_ReadBool_m097C5083B8554A5F33B1799DD8BF30926A5942BE_AdjustorThunk },
	{ 0x06000143, ParseContext_ReadString_mA21E24240255C281D36669A209628AD6FDBB15AE_AdjustorThunk },
	{ 0x06000144, ParseContext_ReadMessage_mA72BE697566685D137AE783593B8AB4558090118_AdjustorThunk },
	{ 0x06000145, ParseContext_ReadBytes_mB08BDD0C46819718BA3813A1A943264DEAC283C5_AdjustorThunk },
	{ 0x06000146, ParseContext_ReadUInt32_m961D49845C792A0B46CEA9E5B57FB50390FA5FDE_AdjustorThunk },
	{ 0x06000147, ParseContext_ReadEnum_mB005F6D2C670173280B8EDB79CE4150B83F08374_AdjustorThunk },
	{ 0x06000148, ParseContext_ReadLength_mABDC3E98A6A4A46C6102FF84C40C42A6C1F0EC87_AdjustorThunk },
	{ 0x06000149, ParseContext_CopyStateTo_mC016DFBBF2BAF69F8A740391DA9F3C25BB94E273_AdjustorThunk },
	{ 0x0600014A, ParseContext_LoadStateFrom_m74FA6FF020C2458A8787FC3E7643E624517F4CB4_AdjustorThunk },
	{ 0x0600014B, ParserInternalState_get_CodedInputStream_m245B3E90A4D6AC379449EA79C3D1A4E2526BD712_AdjustorThunk },
	{ 0x0600014C, ParserInternalState_get_DiscardUnknownFields_mABFB91062F6B1A730ABD81691F138C516B0E3020_AdjustorThunk },
	{ 0x0600014D, ParserInternalState_set_DiscardUnknownFields_m6374B0D645A0352B3BF53F9B0EFEF62E45501C58_AdjustorThunk },
	{ 0x0600014E, ParserInternalState_get_ExtensionRegistry_m8ABCB61553B7732DBB4DB08481D898E5479347CE_AdjustorThunk },
	{ 0x0600014F, ParserInternalState_set_ExtensionRegistry_mEA5D5DD0A75713D26A2300501B940EB7A67C2B5C_AdjustorThunk },
	{ 0x06000178, SegmentedBufferHelper_RefillBuffer_m75ABD1D414AA5D56ECB052EF58E151DA8468893F_AdjustorThunk },
	{ 0x06000179, SegmentedBufferHelper_get_TotalLength_m1F9022D9A2F697A6F81FF34ABA719D983C7926E5_AdjustorThunk },
	{ 0x0600017A, SegmentedBufferHelper_get_CodedInputStream_m5FF2CFFD3C0EF5E732067FA923D9A1059C51E56E_AdjustorThunk },
	{ 0x0600017F, SegmentedBufferHelper_RefillFromReadOnlySequence_m6205503EFBA7900C8DC05FB228F499EF57D76910_AdjustorThunk },
	{ 0x06000180, SegmentedBufferHelper_RefillFromCodedInputStream_mD18F57A2D807F9653438B2843E58D82E6B58C04F_AdjustorThunk },
	{ 0x060001A4, WriteBufferHelper_get_CodedOutputStream_m92805AD7A4248AFD24342FE0F3AF1ECA9E67CAB1_AdjustorThunk },
	{ 0x060001AC, WriteContext_WriteDouble_m36DE9708C2E9F55E08DF1593168026CD929B4114_AdjustorThunk },
	{ 0x060001AD, WriteContext_WriteFloat_m36D978A87046CB34CE1857EAE1079DD9B8972248_AdjustorThunk },
	{ 0x060001AE, WriteContext_WriteUInt64_mE585A8A413E7581324D6B61B063DAB8CD88D4383_AdjustorThunk },
	{ 0x060001AF, WriteContext_WriteInt64_m20616F89120F9551E46B5074B6C14823C97D0C4F_AdjustorThunk },
	{ 0x060001B0, WriteContext_WriteInt32_mCC3646C9CF9A13E2BEBC15A8E566669C913D4F65_AdjustorThunk },
	{ 0x060001B1, WriteContext_WriteFixed64_mAE849DB91093D95337CB4164CA83C8E2B25C7716_AdjustorThunk },
	{ 0x060001B2, WriteContext_WriteFixed32_mECD70B0022C1DFBE81B61366495335BE6B297AF8_AdjustorThunk },
	{ 0x060001B3, WriteContext_WriteBool_m68B7D58774A48FFC3FF34D9A6F770E579EEFA7DF_AdjustorThunk },
	{ 0x060001B4, WriteContext_WriteString_m1A0F46C5CF383B3F976BFBD24B546EC2E346C68C_AdjustorThunk },
	{ 0x060001B5, WriteContext_WriteMessage_m191D2C3E865C097E18DA174B172FFFA9685C8B98_AdjustorThunk },
	{ 0x060001B6, WriteContext_WriteBytes_m6483E64A6655BB2E1499AD548C549B47FB5EB3E5_AdjustorThunk },
	{ 0x060001B7, WriteContext_WriteUInt32_mA6366289E56066C7C0AB06218534DCF1F5C75353_AdjustorThunk },
	{ 0x060001B8, WriteContext_WriteEnum_mE9C54A63A11FA8F28864359CE2236760B7C7AABC_AdjustorThunk },
	{ 0x060001B9, WriteContext_WriteLength_mEBAD2E61EA0B60DD8789138399357B2E42C7BE4F_AdjustorThunk },
	{ 0x060001BA, WriteContext_WriteTag_m610740BFC2CB1D051CF1DCC94FFE749C801584C9_AdjustorThunk },
	{ 0x060001BB, WriteContext_WriteTag_m6041201C0BF3CBBCE75D5DD21474E69BC4CD80EF_AdjustorThunk },
	{ 0x060001BC, WriteContext_WriteRawTag_m3E37F0B83B4E8B7C7E1CC6472D527AA22B11DA5A_AdjustorThunk },
	{ 0x060001BD, WriteContext_WriteRawTag_m3889E0A7DCDBEE6B057EECBB614B022B4D4F991F_AdjustorThunk },
	{ 0x060001BE, WriteContext_CopyStateTo_mB67D2A3DB1B06B506C65BEDC5CC7EC1FC3A3FB97_AdjustorThunk },
	{ 0x060001BF, WriteContext_LoadStateFrom_mF406ADA99F6893BA926A0AD8950B8A8EF85F2D7A_AdjustorThunk },
	{ 0x060001C0, WriterInternalState_get_CodedOutputStream_m3601FE09E7D068C19BBEE6B3DEAB889130C983C3_AdjustorThunk },
};
static const int32_t s_InvokerIndices[2103] = 
{
	1450,
	1450,
	2344,
	2356,
	1204,
	2445,
	1405,
	1372,
	1416,
	2356,
	1416,
	1416,
	1416,
	2143,
	2143,
	1109,
	1405,
	1109,
	2458,
	1244,
	164,
	1261,
	1244,
	1416,
	1416,
	1385,
	1450,
	1450,
	1405,
	1439,
	1406,
	1405,
	1436,
	1416,
	1244,
	1405,
	1405,
	1406,
	1244,
	2309,
	2309,
	2308,
	2309,
	2308,
	2312,
	2312,
	2312,
	2308,
	2308,
	2308,
	2308,
	2309,
	2308,
	1244,
	494,
	1263,
	1236,
	1235,
	1261,
	1244,
	1244,
	1244,
	1235,
	1261,
	810,
	1450,
	1450,
	1450,
	1416,
	1416,
	1385,
	2458,
	1450,
	1416,
	1416,
	1405,
	1436,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	1450,
	1244,
	1405,
	1436,
	443,
	1244,
	1244,
	1450,
	1109,
	797,
	1416,
	1109,
	1416,
	1416,
	654,
	943,
	1450,
	2458,
	2458,
	1450,
	856,
	989,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	1211,
	1244,
	1211,
	1405,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	2353,
	2353,
	2353,
	2353,
	2076,
	2075,
	2075,
	2078,
	-1,
	2458,
	1450,
	995,
	691,
	919,
	689,
	919,
	689,
	1155,
	693,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	2458,
	1211,
	1211,
	1416,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	1244,
	1244,
	1405,
	1416,
	-1,
	1244,
	2445,
	2445,
	2445,
	2445,
	2445,
	2445,
	2445,
	2445,
	2445,
	2445,
	2458,
	1436,
	1244,
	1002,
	800,
	2356,
	800,
	454,
	453,
	2356,
	2420,
	2143,
	800,
	504,
	800,
	800,
	800,
	800,
	800,
	800,
	800,
	800,
	800,
	2223,
	2219,
	2445,
	2458,
	1436,
	1416,
	1436,
	1261,
	809,
	522,
	2356,
	2356,
	2458,
	2458,
	1450,
	1109,
	1109,
	1002,
	1002,
	2356,
	1779,
	1779,
	1436,
	1416,
	512,
	1002,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	1929,
	2217,
	2192,
	1927,
	1405,
	1436,
	1416,
	1405,
	1400,
	1439,
	1406,
	1406,
	1405,
	1406,
	1405,
	1436,
	1416,
	1244,
	1416,
	1405,
	1405,
	1405,
	1244,
	1244,
	1416,
	1436,
	1261,
	1416,
	1244,
	2025,
	2025,
	1891,
	2025,
	2050,
	2050,
	2025,
	2025,
	2025,
	2025,
	2050,
	2050,
	2010,
	2159,
	2159,
	1851,
	1851,
	1931,
	2069,
	2069,
	1851,
	1851,
	1931,
	2108,
	2112,
	2112,
	1754,
	2198,
	1931,
	2201,
	-1,
	1939,
	2201,
	2416,
	2199,
	2458,
	-1,
	-1,
	2217,
	1926,
	435,
	1366,
	1416,
	2026,
	2199,
	2373,
	2108,
	435,
	435,
	2416,
	2416,
	1450,
	1109,
	1405,
	720,
	935,
	1002,
	-1,
	1000,
	999,
	1000,
	1002,
	1002,
	-1,
	1450,
	1098,
	1244,
	1211,
	1405,
	1109,
	1405,
	999,
	599,
	1066,
	1211,
	2085,
	2081,
	1002,
	2085,
	599,
	2356,
	2308,
	2308,
	2032,
	1416,
	2217,
	2416,
	2304,
	2198,
	2198,
	1929,
	2217,
	1229,
	1263,
	1236,
	1236,
	1235,
	1236,
	1235,
	1261,
	1244,
	1244,
	1244,
	1235,
	1235,
	1235,
	735,
	1235,
	1261,
	810,
	1244,
	1244,
	1416,
	1930,
	1935,
	1935,
	1932,
	1932,
	1931,
	1932,
	1931,
	1934,
	1933,
	1804,
	1933,
	1931,
	1931,
	1931,
	1931,
	1932,
	1931,
	1931,
	1932,
	1932,
	1934,
	1933,
	1928,
	1755,
	1931,
	1934,
	1756,
	1756,
	2458,
	2201,
	2201,
	2445,
	2458,
	2445,
	2445,
	1416,
	1450,
	1244,
	1416,
	1416,
	1244,
	1416,
	1244,
	1109,
	1109,
	1405,
	1416,
	1244,
	1211,
	1405,
	1244,
	1244,
	1211,
	2356,
	2458,
	2458,
	1450,
	1416,
	2445,
	2458,
	2445,
	2445,
	1416,
	1450,
	1244,
	1416,
	1406,
	1236,
	1405,
	1235,
	1109,
	1109,
	1405,
	1416,
	1244,
	1211,
	1405,
	1244,
	1244,
	1211,
	2134,
	1863,
	1416,
	2220,
	2458,
	2458,
	1450,
	1416,
	2445,
	2458,
	2445,
	2445,
	1416,
	1450,
	1244,
	1416,
	1416,
	1109,
	1109,
	1405,
	1416,
	1244,
	1211,
	1405,
	1244,
	1244,
	1211,
	2086,
	1416,
	2380,
	2458,
	2458,
	1450,
	1109,
	1416,
	2445,
	2458,
	2445,
	2445,
	1416,
	1450,
	1244,
	1416,
	1416,
	1244,
	1109,
	1109,
	1405,
	1416,
	1244,
	1211,
	1405,
	1244,
	1244,
	1211,
	2458,
	2458,
	1450,
	1416,
	2445,
	2458,
	2445,
	2445,
	1416,
	1450,
	1244,
	1416,
	1416,
	1109,
	1109,
	1405,
	1416,
	1244,
	1211,
	1405,
	1244,
	1244,
	1211,
	2458,
	2458,
	1450,
	1416,
	2445,
	2445,
	1416,
	1450,
	1244,
	1416,
	1405,
	1235,
	1400,
	1229,
	1416,
	1244,
	1436,
	1261,
	1416,
	1244,
	1416,
	1244,
	1405,
	1109,
	1109,
	1405,
	1416,
	1244,
	1211,
	1405,
	1244,
	1244,
	1211,
	2458,
	2458,
	1450,
	1416,
	2445,
	2445,
	1416,
	1450,
	1244,
	1416,
	1109,
	1109,
	1405,
	1416,
	1244,
	1211,
	1405,
	1244,
	1244,
	1211,
	2458,
	2458,
	1450,
	1416,
	2445,
	2458,
	2445,
	2445,
	1416,
	1450,
	1244,
	1416,
	1406,
	1236,
	1405,
	1235,
	1109,
	1109,
	1405,
	1416,
	1244,
	1211,
	1405,
	1244,
	1244,
	1211,
	2134,
	1863,
	943,
	2143,
	1416,
	2458,
	2458,
	1450,
	1416,
	2445,
	2458,
	2445,
	2445,
	1416,
	1450,
	1244,
	1416,
	1416,
	1244,
	1416,
	1244,
	1405,
	1235,
	1109,
	1109,
	1405,
	1416,
	1244,
	1211,
	1405,
	1244,
	1244,
	1211,
	2458,
	2458,
	1450,
	1416,
	2445,
	2445,
	1416,
	1450,
	1244,
	1416,
	1405,
	1235,
	1405,
	1235,
	1405,
	1235,
	1416,
	1244,
	1416,
	1244,
	1405,
	1235,
	1436,
	1261,
	1416,
	1244,
	1416,
	1244,
	1109,
	1109,
	1405,
	1416,
	1244,
	1211,
	1405,
	1244,
	1244,
	1211,
	2458,
	2458,
	1450,
	1416,
	2445,
	2445,
	1416,
	1450,
	1244,
	1416,
	1416,
	1244,
	1416,
	1244,
	1405,
	1235,
	1109,
	1109,
	1405,
	1416,
	1244,
	1211,
	1405,
	1244,
	1244,
	1211,
	2458,
	2458,
	1450,
	1416,
	2445,
	2445,
	1416,
	1450,
	1244,
	1416,
	1416,
	1244,
	1405,
	1235,
	1109,
	1109,
	1405,
	1416,
	1244,
	1211,
	1405,
	1244,
	1244,
	1211,
	2458,
	2458,
	1450,
	1416,
	2445,
	2445,
	1416,
	1450,
	1244,
	1416,
	1416,
	1244,
	1416,
	1244,
	1109,
	1109,
	1405,
	1416,
	1244,
	1211,
	1405,
	1244,
	1244,
	1211,
	2458,
	2458,
	1450,
	1416,
	2445,
	2458,
	2445,
	2445,
	1416,
	1450,
	1244,
	1416,
	1109,
	1109,
	1405,
	1416,
	1244,
	1211,
	1405,
	1244,
	1244,
	1211,
	2458,
	2458,
	1450,
	1416,
	2445,
	2445,
	1416,
	1450,
	1244,
	1416,
	1416,
	1244,
	1436,
	1416,
	1244,
	1436,
	1416,
	1416,
	1416,
	1416,
	1416,
	1416,
	1416,
	1244,
	1416,
	1244,
	1416,
	1244,
	1436,
	1109,
	1109,
	1405,
	1416,
	1244,
	1211,
	1405,
	1244,
	1244,
	1211,
	2458,
	2458,
	1450,
	1416,
	2445,
	2445,
	1416,
	1450,
	1244,
	1416,
	1416,
	1244,
	1436,
	1416,
	1416,
	1416,
	1416,
	1416,
	1416,
	1244,
	1109,
	1109,
	1405,
	1416,
	1244,
	1211,
	1405,
	1244,
	1244,
	1211,
	2458,
	2445,
	2445,
	1416,
	1450,
	1244,
	1416,
	1405,
	1235,
	1436,
	1405,
	1235,
	1436,
	1416,
	1244,
	1109,
	1109,
	1405,
	1416,
	1244,
	1211,
	1405,
	1244,
	1244,
	1211,
	2458,
	2458,
	1450,
	1416,
	2445,
	2445,
	1416,
	1450,
	1244,
	1416,
	1405,
	1235,
	1436,
	1405,
	1235,
	1436,
	1109,
	1109,
	1405,
	1416,
	1244,
	1211,
	1405,
	1244,
	1244,
	1211,
	2458,
	2458,
	1450,
	1416,
	2458,
	1450,
	1416,
	2445,
	2445,
	1416,
	1450,
	1244,
	1416,
	1109,
	1109,
	1405,
	1416,
	1244,
	1211,
	1405,
	1244,
	1244,
	1211,
	-1,
	-1,
	-1,
	-1,
	-1,
	2458,
	2458,
	1450,
	1416,
	2445,
	2445,
	1416,
	1450,
	1244,
	1416,
	1416,
	1244,
	1436,
	1405,
	1235,
	1436,
	1405,
	1235,
	1436,
	1405,
	1235,
	1436,
	1416,
	1244,
	1436,
	1416,
	1244,
	1436,
	1416,
	1244,
	1436,
	1405,
	1235,
	1436,
	1416,
	1244,
	1436,
	1416,
	1244,
	1436,
	1261,
	1436,
	1109,
	1109,
	1405,
	1416,
	1244,
	1211,
	1405,
	1244,
	1244,
	1211,
	2458,
	2458,
	1450,
	1416,
	2445,
	2445,
	1416,
	1450,
	1244,
	1416,
	1416,
	1244,
	1436,
	1416,
	1244,
	1109,
	1109,
	1405,
	1416,
	1244,
	1211,
	1405,
	1244,
	1244,
	1211,
	2458,
	2458,
	1450,
	1416,
	2445,
	2445,
	1416,
	1450,
	1244,
	1416,
	1416,
	1244,
	1436,
	1416,
	1416,
	1244,
	1109,
	1109,
	1405,
	1416,
	1244,
	1211,
	1405,
	1244,
	1244,
	1211,
	2458,
	2445,
	2445,
	1416,
	1450,
	1244,
	1416,
	1405,
	1235,
	1436,
	1405,
	1235,
	1436,
	1109,
	1109,
	1405,
	1416,
	1244,
	1211,
	1405,
	1244,
	1244,
	1211,
	2458,
	2458,
	1450,
	1416,
	2458,
	1450,
	1416,
	2445,
	2445,
	1416,
	1450,
	1244,
	1416,
	1416,
	1244,
	1436,
	1405,
	1235,
	1436,
	1416,
	1244,
	1109,
	1109,
	1405,
	1416,
	1244,
	1211,
	1405,
	1244,
	1244,
	1211,
	2458,
	2458,
	1450,
	1416,
	2445,
	2445,
	1416,
	1450,
	1244,
	1416,
	1416,
	1244,
	1436,
	1416,
	1416,
	1244,
	1109,
	1109,
	1405,
	1416,
	1244,
	1211,
	1405,
	1244,
	1244,
	1211,
	2458,
	2458,
	1450,
	1416,
	2445,
	2445,
	1416,
	1450,
	1244,
	1416,
	1416,
	1244,
	1436,
	1416,
	1244,
	1436,
	1416,
	1244,
	1436,
	1416,
	1244,
	1436,
	1261,
	1436,
	1436,
	1261,
	1436,
	1109,
	1109,
	1405,
	1416,
	1244,
	1211,
	1405,
	1244,
	1244,
	1211,
	2458,
	2458,
	1450,
	1416,
	2445,
	2445,
	1416,
	1450,
	1244,
	1416,
	1416,
	1244,
	1436,
	1416,
	1244,
	1436,
	1436,
	1261,
	1436,
	1436,
	1261,
	1436,
	1436,
	1261,
	1436,
	1405,
	1235,
	1436,
	1416,
	1244,
	1436,
	1436,
	1261,
	1436,
	1436,
	1261,
	1436,
	1436,
	1261,
	1436,
	1436,
	1261,
	1436,
	1436,
	1261,
	1436,
	1436,
	1261,
	1436,
	1416,
	1244,
	1436,
	1416,
	1244,
	1436,
	1416,
	1244,
	1436,
	1416,
	1244,
	1436,
	1416,
	1244,
	1436,
	1416,
	1244,
	1436,
	1416,
	1244,
	1436,
	1109,
	1109,
	1405,
	1416,
	1244,
	1211,
	1405,
	1244,
	1244,
	1211,
	-1,
	-1,
	-1,
	-1,
	-1,
	2458,
	2458,
	1450,
	1416,
	2445,
	2445,
	1416,
	1450,
	1244,
	1416,
	1436,
	1261,
	1436,
	1436,
	1261,
	1436,
	1436,
	1261,
	1436,
	1436,
	1261,
	1436,
	1109,
	1109,
	1405,
	1416,
	1244,
	1211,
	1405,
	1244,
	1244,
	1211,
	-1,
	-1,
	-1,
	-1,
	-1,
	2458,
	2458,
	1450,
	1416,
	2445,
	2445,
	1416,
	1450,
	1244,
	1416,
	1405,
	1235,
	1436,
	1436,
	1261,
	1436,
	1405,
	1235,
	1436,
	1436,
	1261,
	1436,
	1436,
	1261,
	1436,
	1436,
	1261,
	1436,
	1109,
	1109,
	1405,
	1416,
	1244,
	1211,
	1405,
	1244,
	1244,
	1211,
	-1,
	-1,
	-1,
	-1,
	-1,
	2458,
	2458,
	1450,
	1416,
	2445,
	2445,
	1416,
	1450,
	1244,
	1416,
	1109,
	1109,
	1405,
	1416,
	1244,
	1211,
	1405,
	1244,
	1244,
	1211,
	-1,
	-1,
	-1,
	-1,
	-1,
	2458,
	2458,
	1450,
	1416,
	2445,
	2445,
	1416,
	1450,
	1244,
	1416,
	1436,
	1261,
	1436,
	1436,
	1261,
	1436,
	1109,
	1109,
	1405,
	1416,
	1244,
	1211,
	1405,
	1244,
	1244,
	1211,
	-1,
	-1,
	-1,
	-1,
	-1,
	2458,
	2458,
	1450,
	1416,
	2445,
	2445,
	1416,
	1450,
	1244,
	1416,
	1436,
	1261,
	1436,
	1109,
	1109,
	1405,
	1416,
	1244,
	1211,
	1405,
	1244,
	1244,
	1211,
	-1,
	-1,
	-1,
	-1,
	-1,
	2458,
	2458,
	1450,
	1416,
	2445,
	2445,
	1416,
	1450,
	1244,
	1416,
	1436,
	1261,
	1436,
	1109,
	1109,
	1405,
	1416,
	1244,
	1211,
	1405,
	1244,
	1244,
	1211,
	-1,
	-1,
	-1,
	-1,
	-1,
	2458,
	2458,
	1450,
	1416,
	2445,
	2445,
	1416,
	1450,
	1244,
	1416,
	1436,
	1261,
	1436,
	1405,
	1235,
	1436,
	1109,
	1109,
	1405,
	1416,
	1244,
	1211,
	1405,
	1244,
	1244,
	1211,
	-1,
	-1,
	-1,
	-1,
	-1,
	2458,
	2458,
	1450,
	1416,
	2445,
	2445,
	1416,
	1450,
	1244,
	1416,
	1416,
	1244,
	1436,
	1406,
	1236,
	1436,
	1406,
	1236,
	1436,
	1400,
	1229,
	1436,
	1416,
	1244,
	1436,
	1416,
	1244,
	1436,
	1109,
	1109,
	1405,
	1416,
	1244,
	1211,
	1405,
	1244,
	1244,
	1211,
	2458,
	2445,
	2445,
	1416,
	1450,
	1244,
	1416,
	1416,
	1244,
	1436,
	1436,
	1261,
	1436,
	1109,
	1109,
	1405,
	1416,
	1244,
	1211,
	1405,
	1244,
	1244,
	1211,
	2458,
	2458,
	1450,
	1416,
	2458,
	1450,
	1416,
	2445,
	2445,
	1416,
	1450,
	1244,
	1416,
	1416,
	1109,
	1109,
	1405,
	1416,
	1244,
	1211,
	1405,
	1244,
	1244,
	1211,
	2458,
	2445,
	2445,
	1416,
	1450,
	1244,
	1416,
	1416,
	1416,
	1416,
	1244,
	1436,
	1416,
	1244,
	1436,
	1416,
	1109,
	1109,
	1405,
	1416,
	1244,
	1211,
	1405,
	1244,
	1244,
	1211,
	2458,
	2458,
	1450,
	1416,
	2458,
	1450,
	1416,
	2445,
	2445,
	1416,
	1450,
	1244,
	1416,
	1109,
	1109,
	1405,
	1416,
	1244,
	1211,
	1405,
	1244,
	1244,
	1211,
	2458,
	2445,
	2445,
	1416,
	1450,
	1244,
	1416,
	1416,
	1244,
	1436,
	1405,
	1235,
	1436,
	1405,
	1235,
	1436,
	1109,
	1109,
	1405,
	1416,
	1244,
	1211,
	1405,
	1244,
	1244,
	1211,
	2458,
	2458,
	1450,
	1416,
	2458,
	1450,
	1416,
	503,
	1416,
	1416,
	1416,
	999,
	1405,
	800,
	2085,
	1244,
	1244,
	-1,
	800,
	1244,
	2420,
	604,
	1244,
	1244,
	605,
	2458,
	-1,
	-1,
	-1,
	-1,
	-1,
	800,
	168,
	1416,
	999,
	1416,
	1450,
	604,
	335,
	1416,
	1416,
	1405,
	1416,
	1244,
	1416,
	1244,
	1109,
	1002,
	800,
	800,
	1416,
	1450,
	1450,
	604,
	1450,
	604,
	2458,
	1450,
	990,
	990,
	990,
	990,
	943,
	800,
	1416,
	1002,
	1109,
	1244,
	800,
	1416,
	1416,
	1416,
	1416,
	1436,
	1416,
	1416,
	109,
	1416,
	1416,
	2308,
	1436,
	1436,
	1405,
	1405,
	943,
	1416,
	1450,
	1416,
	2458,
	115,
	1416,
	1002,
	604,
	999,
	605,
	1709,
	1416,
	1405,
	1416,
	1416,
	1416,
	1416,
	1416,
	1416,
	1416,
	1416,
	1416,
	1416,
	1416,
	1592,
	1450,
	1877,
	2085,
	2356,
	2356,
	2356,
	1416,
	-1,
	1450,
	604,
	604,
	604,
	2458,
	1450,
	1002,
	1109,
	1002,
	1109,
	1002,
	1109,
	1416,
	1244,
	1416,
	1416,
	1416,
	1416,
	1416,
	1416,
	62,
	504,
	2458,
	1416,
	1416,
	1416,
	1416,
	1244,
	1002,
	1109,
	800,
	1244,
	1109,
	800,
	168,
	2356,
	1416,
	999,
	1416,
	1416,
	1416,
	1436,
	1436,
	1416,
	1416,
	1416,
	1416,
	1416,
	999,
	1450,
	2458,
	1244,
	1416,
	1416,
	999,
	1450,
	604,
	604,
	604,
	604,
	2458,
	1450,
	943,
	335,
	1416,
	1416,
	1450,
	504,
	1877,
	2356,
	1416,
	1244,
	1002,
	1450,
	943,
	1244,
	168,
	1416,
	1416,
	1416,
	1436,
	1416,
	1450,
	1002,
	1450,
	1109,
	1416,
	1244,
	1436,
	1261,
	1244,
	504,
	1416,
	1416,
	1416,
	2458,
	-1,
	2356,
	2356,
	2356,
	2356,
	2356,
	2356,
	2085,
	2452,
	2452,
	2440,
	1002,
	1002,
	1002,
	1002,
	1002,
	1002,
	1109,
	1244,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	800,
	1244,
	1109,
	800,
	503,
	1416,
	999,
	1450,
	1450,
	604,
	800,
	1244,
	1109,
	800,
	1450,
	1109,
	1244,
	1450,
	1109,
	1244,
	1450,
	1244,
	2458,
	1450,
	1109,
	2445,
	1244,
	1002,
	2458,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	2445,
	2445,
	2445,
	2445,
	2458,
	632,
	930,
	1450,
	663,
	955,
	1450,
	618,
	912,
	1450,
	619,
	913,
	1450,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
};
static const Il2CppTokenRangePair s_rgctxIndices[98] = 
{
	{ 0x02000009, { 0, 4 } },
	{ 0x0200000A, { 4, 3 } },
	{ 0x0200000F, { 40, 4 } },
	{ 0x02000011, { 44, 14 } },
	{ 0x02000012, { 58, 10 } },
	{ 0x02000015, { 88, 2 } },
	{ 0x02000016, { 90, 7 } },
	{ 0x02000019, { 97, 32 } },
	{ 0x0200001D, { 129, 1 } },
	{ 0x0200001E, { 130, 3 } },
	{ 0x0200002D, { 133, 7 } },
	{ 0x0200002E, { 140, 2 } },
	{ 0x0200002F, { 142, 3 } },
	{ 0x020000D3, { 221, 28 } },
	{ 0x020000D4, { 249, 2 } },
	{ 0x020000D5, { 251, 3 } },
	{ 0x020000D6, { 254, 2 } },
	{ 0x020000D7, { 256, 3 } },
	{ 0x020000D8, { 259, 3 } },
	{ 0x020000D9, { 262, 2 } },
	{ 0x020000DA, { 264, 10 } },
	{ 0x020000E6, { 284, 82 } },
	{ 0x020000E7, { 366, 8 } },
	{ 0x020000E9, { 374, 8 } },
	{ 0x020000EA, { 382, 3 } },
	{ 0x020000EB, { 385, 7 } },
	{ 0x020000F1, { 396, 44 } },
	{ 0x020000F2, { 440, 1 } },
	{ 0x0600006B, { 7, 1 } },
	{ 0x0600006C, { 8, 4 } },
	{ 0x0600006D, { 12, 3 } },
	{ 0x0600006E, { 15, 5 } },
	{ 0x0600006F, { 20, 6 } },
	{ 0x06000070, { 26, 1 } },
	{ 0x06000071, { 27, 1 } },
	{ 0x06000072, { 28, 1 } },
	{ 0x06000073, { 29, 1 } },
	{ 0x06000074, { 30, 4 } },
	{ 0x06000075, { 34, 3 } },
	{ 0x06000076, { 37, 3 } },
	{ 0x0600009D, { 68, 20 } },
	{ 0x0600016E, { 145, 12 } },
	{ 0x06000174, { 157, 1 } },
	{ 0x06000175, { 158, 1 } },
	{ 0x06000189, { 159, 4 } },
	{ 0x0600018F, { 163, 3 } },
	{ 0x060003E5, { 166, 1 } },
	{ 0x060003E6, { 167, 1 } },
	{ 0x060003E7, { 168, 1 } },
	{ 0x060003E8, { 169, 1 } },
	{ 0x060003E9, { 170, 1 } },
	{ 0x06000516, { 171, 1 } },
	{ 0x06000517, { 172, 1 } },
	{ 0x06000518, { 173, 1 } },
	{ 0x06000519, { 174, 1 } },
	{ 0x0600051A, { 175, 1 } },
	{ 0x0600053B, { 176, 1 } },
	{ 0x0600053C, { 177, 1 } },
	{ 0x0600053D, { 178, 1 } },
	{ 0x0600053E, { 179, 1 } },
	{ 0x0600053F, { 180, 1 } },
	{ 0x06000566, { 181, 1 } },
	{ 0x06000567, { 182, 1 } },
	{ 0x06000568, { 183, 1 } },
	{ 0x06000569, { 184, 1 } },
	{ 0x0600056A, { 185, 1 } },
	{ 0x0600057F, { 186, 1 } },
	{ 0x06000580, { 187, 1 } },
	{ 0x06000581, { 188, 1 } },
	{ 0x06000582, { 189, 1 } },
	{ 0x06000583, { 190, 1 } },
	{ 0x0600059E, { 191, 1 } },
	{ 0x0600059F, { 192, 1 } },
	{ 0x060005A0, { 193, 1 } },
	{ 0x060005A1, { 194, 1 } },
	{ 0x060005A2, { 195, 1 } },
	{ 0x060005BA, { 196, 1 } },
	{ 0x060005BB, { 197, 1 } },
	{ 0x060005BC, { 198, 1 } },
	{ 0x060005BD, { 199, 1 } },
	{ 0x060005BE, { 200, 1 } },
	{ 0x060005D6, { 201, 1 } },
	{ 0x060005D7, { 202, 1 } },
	{ 0x060005D8, { 203, 1 } },
	{ 0x060005D9, { 204, 1 } },
	{ 0x060005DA, { 205, 1 } },
	{ 0x060005F5, { 206, 1 } },
	{ 0x060005F6, { 207, 1 } },
	{ 0x060005F7, { 208, 1 } },
	{ 0x060005F8, { 209, 1 } },
	{ 0x060005F9, { 210, 1 } },
	{ 0x060006AB, { 211, 1 } },
	{ 0x060006B4, { 212, 6 } },
	{ 0x0600070F, { 218, 1 } },
	{ 0x06000770, { 219, 2 } },
	{ 0x060007B6, { 274, 5 } },
	{ 0x060007B7, { 279, 5 } },
	{ 0x060007F8, { 392, 4 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[441] = 
{
	{ (Il2CppRGCTXDataType)3, 3271 },
	{ (Il2CppRGCTXDataType)1, 387 },
	{ (Il2CppRGCTXDataType)2, 1174 },
	{ (Il2CppRGCTXDataType)3, 3220 },
	{ (Il2CppRGCTXDataType)1, 466 },
	{ (Il2CppRGCTXDataType)2, 3085 },
	{ (Il2CppRGCTXDataType)3, 7702 },
	{ (Il2CppRGCTXDataType)3, 3171 },
	{ (Il2CppRGCTXDataType)3, 10528 },
	{ (Il2CppRGCTXDataType)2, 1173 },
	{ (Il2CppRGCTXDataType)3, 3219 },
	{ (Il2CppRGCTXDataType)3, 3231 },
	{ (Il2CppRGCTXDataType)3, 10526 },
	{ (Il2CppRGCTXDataType)2, 3083 },
	{ (Il2CppRGCTXDataType)3, 7700 },
	{ (Il2CppRGCTXDataType)2, 1147 },
	{ (Il2CppRGCTXDataType)3, 3176 },
	{ (Il2CppRGCTXDataType)3, 3177 },
	{ (Il2CppRGCTXDataType)2, 3084 },
	{ (Il2CppRGCTXDataType)3, 7701 },
	{ (Il2CppRGCTXDataType)3, 10856 },
	{ (Il2CppRGCTXDataType)2, 1151 },
	{ (Il2CppRGCTXDataType)3, 3180 },
	{ (Il2CppRGCTXDataType)3, 3181 },
	{ (Il2CppRGCTXDataType)2, 1172 },
	{ (Il2CppRGCTXDataType)3, 3218 },
	{ (Il2CppRGCTXDataType)3, 10527 },
	{ (Il2CppRGCTXDataType)3, 3178 },
	{ (Il2CppRGCTXDataType)3, 3179 },
	{ (Il2CppRGCTXDataType)3, 10530 },
	{ (Il2CppRGCTXDataType)3, 3173 },
	{ (Il2CppRGCTXDataType)1, 153 },
	{ (Il2CppRGCTXDataType)2, 1144 },
	{ (Il2CppRGCTXDataType)3, 3172 },
	{ (Il2CppRGCTXDataType)2, 1145 },
	{ (Il2CppRGCTXDataType)3, 3174 },
	{ (Il2CppRGCTXDataType)3, 3175 },
	{ (Il2CppRGCTXDataType)2, 1141 },
	{ (Il2CppRGCTXDataType)3, 3169 },
	{ (Il2CppRGCTXDataType)3, 3170 },
	{ (Il2CppRGCTXDataType)1, 384 },
	{ (Il2CppRGCTXDataType)3, 3183 },
	{ (Il2CppRGCTXDataType)2, 1153 },
	{ (Il2CppRGCTXDataType)3, 3182 },
	{ (Il2CppRGCTXDataType)3, 3241 },
	{ (Il2CppRGCTXDataType)3, 3240 },
	{ (Il2CppRGCTXDataType)2, 1171 },
	{ (Il2CppRGCTXDataType)3, 3217 },
	{ (Il2CppRGCTXDataType)2, 385 },
	{ (Il2CppRGCTXDataType)2, 1675 },
	{ (Il2CppRGCTXDataType)3, 3245 },
	{ (Il2CppRGCTXDataType)3, 4077 },
	{ (Il2CppRGCTXDataType)3, 3243 },
	{ (Il2CppRGCTXDataType)3, 8861 },
	{ (Il2CppRGCTXDataType)3, 3244 },
	{ (Il2CppRGCTXDataType)3, 3246 },
	{ (Il2CppRGCTXDataType)3, 8840 },
	{ (Il2CppRGCTXDataType)3, 3242 },
	{ (Il2CppRGCTXDataType)2, 3150 },
	{ (Il2CppRGCTXDataType)3, 7715 },
	{ (Il2CppRGCTXDataType)3, 7718 },
	{ (Il2CppRGCTXDataType)2, 3082 },
	{ (Il2CppRGCTXDataType)3, 7699 },
	{ (Il2CppRGCTXDataType)3, 7719 },
	{ (Il2CppRGCTXDataType)3, 7720 },
	{ (Il2CppRGCTXDataType)3, 7717 },
	{ (Il2CppRGCTXDataType)3, 7716 },
	{ (Il2CppRGCTXDataType)3, 7721 },
	{ (Il2CppRGCTXDataType)2, 769 },
	{ (Il2CppRGCTXDataType)3, 198 },
	{ (Il2CppRGCTXDataType)3, 199 },
	{ (Il2CppRGCTXDataType)2, 3395 },
	{ (Il2CppRGCTXDataType)3, 8792 },
	{ (Il2CppRGCTXDataType)2, 760 },
	{ (Il2CppRGCTXDataType)3, 45 },
	{ (Il2CppRGCTXDataType)2, 3410 },
	{ (Il2CppRGCTXDataType)3, 8839 },
	{ (Il2CppRGCTXDataType)3, 200 },
	{ (Il2CppRGCTXDataType)2, 2421 },
	{ (Il2CppRGCTXDataType)3, 4076 },
	{ (Il2CppRGCTXDataType)3, 46 },
	{ (Il2CppRGCTXDataType)2, 3422 },
	{ (Il2CppRGCTXDataType)3, 8860 },
	{ (Il2CppRGCTXDataType)3, 47 },
	{ (Il2CppRGCTXDataType)2, 1401 },
	{ (Il2CppRGCTXDataType)3, 3520 },
	{ (Il2CppRGCTXDataType)2, 1226 },
	{ (Il2CppRGCTXDataType)3, 3236 },
	{ (Il2CppRGCTXDataType)3, 6422 },
	{ (Il2CppRGCTXDataType)2, 544 },
	{ (Il2CppRGCTXDataType)2, 761 },
	{ (Il2CppRGCTXDataType)3, 48 },
	{ (Il2CppRGCTXDataType)2, 761 },
	{ (Il2CppRGCTXDataType)2, 543 },
	{ (Il2CppRGCTXDataType)2, 1679 },
	{ (Il2CppRGCTXDataType)2, 2288 },
	{ (Il2CppRGCTXDataType)3, 4017 },
	{ (Il2CppRGCTXDataType)3, 10881 },
	{ (Il2CppRGCTXDataType)2, 1229 },
	{ (Il2CppRGCTXDataType)2, 388 },
	{ (Il2CppRGCTXDataType)1, 388 },
	{ (Il2CppRGCTXDataType)2, 770 },
	{ (Il2CppRGCTXDataType)3, 204 },
	{ (Il2CppRGCTXDataType)3, 205 },
	{ (Il2CppRGCTXDataType)2, 1416 },
	{ (Il2CppRGCTXDataType)3, 3526 },
	{ (Il2CppRGCTXDataType)3, 3247 },
	{ (Il2CppRGCTXDataType)2, 771 },
	{ (Il2CppRGCTXDataType)3, 214 },
	{ (Il2CppRGCTXDataType)3, 215 },
	{ (Il2CppRGCTXDataType)2, 2422 },
	{ (Il2CppRGCTXDataType)3, 4078 },
	{ (Il2CppRGCTXDataType)2, 745 },
	{ (Il2CppRGCTXDataType)3, 0 },
	{ (Il2CppRGCTXDataType)2, 3423 },
	{ (Il2CppRGCTXDataType)3, 8862 },
	{ (Il2CppRGCTXDataType)3, 3248 },
	{ (Il2CppRGCTXDataType)3, 3250 },
	{ (Il2CppRGCTXDataType)3, 3249 },
	{ (Il2CppRGCTXDataType)3, 3253 },
	{ (Il2CppRGCTXDataType)3, 3256 },
	{ (Il2CppRGCTXDataType)3, 8841 },
	{ (Il2CppRGCTXDataType)3, 3252 },
	{ (Il2CppRGCTXDataType)3, 3254 },
	{ (Il2CppRGCTXDataType)3, 8793 },
	{ (Il2CppRGCTXDataType)3, 3255 },
	{ (Il2CppRGCTXDataType)3, 3527 },
	{ (Il2CppRGCTXDataType)3, 3251 },
	{ (Il2CppRGCTXDataType)3, 2862 },
	{ (Il2CppRGCTXDataType)3, 8795 },
	{ (Il2CppRGCTXDataType)2, 749 },
	{ (Il2CppRGCTXDataType)3, 4 },
	{ (Il2CppRGCTXDataType)2, 749 },
	{ (Il2CppRGCTXDataType)3, 6421 },
	{ (Il2CppRGCTXDataType)2, 767 },
	{ (Il2CppRGCTXDataType)3, 160 },
	{ (Il2CppRGCTXDataType)3, 161 },
	{ (Il2CppRGCTXDataType)3, 3405 },
	{ (Il2CppRGCTXDataType)2, 439 },
	{ (Il2CppRGCTXDataType)2, 2708 },
	{ (Il2CppRGCTXDataType)3, 3407 },
	{ (Il2CppRGCTXDataType)2, 568 },
	{ (Il2CppRGCTXDataType)2, 449 },
	{ (Il2CppRGCTXDataType)2, 2925 },
	{ (Il2CppRGCTXDataType)3, 6967 },
	{ (Il2CppRGCTXDataType)3, 1075 },
	{ (Il2CppRGCTXDataType)3, 3238 },
	{ (Il2CppRGCTXDataType)3, 1076 },
	{ (Il2CppRGCTXDataType)3, 3269 },
	{ (Il2CppRGCTXDataType)3, 3239 },
	{ (Il2CppRGCTXDataType)3, 3237 },
	{ (Il2CppRGCTXDataType)3, 3270 },
	{ (Il2CppRGCTXDataType)3, 3267 },
	{ (Il2CppRGCTXDataType)2, 634 },
	{ (Il2CppRGCTXDataType)3, 3268 },
	{ (Il2CppRGCTXDataType)2, 2442 },
	{ (Il2CppRGCTXDataType)3, 4740 },
	{ (Il2CppRGCTXDataType)2, 259 },
	{ (Il2CppRGCTXDataType)2, 260 },
	{ (Il2CppRGCTXDataType)2, 1585 },
	{ (Il2CppRGCTXDataType)2, 2534 },
	{ (Il2CppRGCTXDataType)3, 5062 },
	{ (Il2CppRGCTXDataType)3, 5063 },
	{ (Il2CppRGCTXDataType)2, 2533 },
	{ (Il2CppRGCTXDataType)3, 5060 },
	{ (Il2CppRGCTXDataType)3, 5061 },
	{ (Il2CppRGCTXDataType)3, 10458 },
	{ (Il2CppRGCTXDataType)3, 10457 },
	{ (Il2CppRGCTXDataType)3, 10482 },
	{ (Il2CppRGCTXDataType)3, 10496 },
	{ (Il2CppRGCTXDataType)3, 10422 },
	{ (Il2CppRGCTXDataType)3, 10462 },
	{ (Il2CppRGCTXDataType)3, 10461 },
	{ (Il2CppRGCTXDataType)3, 10484 },
	{ (Il2CppRGCTXDataType)3, 10498 },
	{ (Il2CppRGCTXDataType)3, 10424 },
	{ (Il2CppRGCTXDataType)3, 10466 },
	{ (Il2CppRGCTXDataType)3, 10465 },
	{ (Il2CppRGCTXDataType)3, 10486 },
	{ (Il2CppRGCTXDataType)3, 10500 },
	{ (Il2CppRGCTXDataType)3, 10427 },
	{ (Il2CppRGCTXDataType)3, 10460 },
	{ (Il2CppRGCTXDataType)3, 10459 },
	{ (Il2CppRGCTXDataType)3, 10483 },
	{ (Il2CppRGCTXDataType)3, 10497 },
	{ (Il2CppRGCTXDataType)3, 10423 },
	{ (Il2CppRGCTXDataType)3, 10472 },
	{ (Il2CppRGCTXDataType)3, 10471 },
	{ (Il2CppRGCTXDataType)3, 10489 },
	{ (Il2CppRGCTXDataType)3, 10503 },
	{ (Il2CppRGCTXDataType)3, 10431 },
	{ (Il2CppRGCTXDataType)3, 10454 },
	{ (Il2CppRGCTXDataType)3, 10453 },
	{ (Il2CppRGCTXDataType)3, 10480 },
	{ (Il2CppRGCTXDataType)3, 10494 },
	{ (Il2CppRGCTXDataType)3, 10420 },
	{ (Il2CppRGCTXDataType)3, 10456 },
	{ (Il2CppRGCTXDataType)3, 10455 },
	{ (Il2CppRGCTXDataType)3, 10481 },
	{ (Il2CppRGCTXDataType)3, 10495 },
	{ (Il2CppRGCTXDataType)3, 10421 },
	{ (Il2CppRGCTXDataType)3, 10478 },
	{ (Il2CppRGCTXDataType)3, 10477 },
	{ (Il2CppRGCTXDataType)3, 10492 },
	{ (Il2CppRGCTXDataType)3, 10506 },
	{ (Il2CppRGCTXDataType)3, 10436 },
	{ (Il2CppRGCTXDataType)3, 10468 },
	{ (Il2CppRGCTXDataType)3, 10467 },
	{ (Il2CppRGCTXDataType)3, 10487 },
	{ (Il2CppRGCTXDataType)3, 10501 },
	{ (Il2CppRGCTXDataType)3, 10428 },
	{ (Il2CppRGCTXDataType)2, 106 },
	{ (Il2CppRGCTXDataType)2, 1586 },
	{ (Il2CppRGCTXDataType)2, 3865 },
	{ (Il2CppRGCTXDataType)2, 2234 },
	{ (Il2CppRGCTXDataType)3, 4024 },
	{ (Il2CppRGCTXDataType)2, 3032 },
	{ (Il2CppRGCTXDataType)3, 7271 },
	{ (Il2CppRGCTXDataType)3, 10891 },
	{ (Il2CppRGCTXDataType)2, 3081 },
	{ (Il2CppRGCTXDataType)3, 7602 },
	{ (Il2CppRGCTXDataType)2, 763 },
	{ (Il2CppRGCTXDataType)3, 54 },
	{ (Il2CppRGCTXDataType)1, 1452 },
	{ (Il2CppRGCTXDataType)2, 1452 },
	{ (Il2CppRGCTXDataType)3, 55 },
	{ (Il2CppRGCTXDataType)2, 764 },
	{ (Il2CppRGCTXDataType)3, 88 },
	{ (Il2CppRGCTXDataType)1, 1450 },
	{ (Il2CppRGCTXDataType)2, 1450 },
	{ (Il2CppRGCTXDataType)3, 89 },
	{ (Il2CppRGCTXDataType)2, 766 },
	{ (Il2CppRGCTXDataType)3, 126 },
	{ (Il2CppRGCTXDataType)1, 795 },
	{ (Il2CppRGCTXDataType)2, 795 },
	{ (Il2CppRGCTXDataType)3, 127 },
	{ (Il2CppRGCTXDataType)2, 768 },
	{ (Il2CppRGCTXDataType)3, 164 },
	{ (Il2CppRGCTXDataType)3, 165 },
	{ (Il2CppRGCTXDataType)2, 772 },
	{ (Il2CppRGCTXDataType)3, 224 },
	{ (Il2CppRGCTXDataType)1, 821 },
	{ (Il2CppRGCTXDataType)2, 821 },
	{ (Il2CppRGCTXDataType)3, 225 },
	{ (Il2CppRGCTXDataType)2, 773 },
	{ (Il2CppRGCTXDataType)3, 258 },
	{ (Il2CppRGCTXDataType)1, 1451 },
	{ (Il2CppRGCTXDataType)2, 1451 },
	{ (Il2CppRGCTXDataType)3, 259 },
	{ (Il2CppRGCTXDataType)2, 597 },
	{ (Il2CppRGCTXDataType)3, 3542 },
	{ (Il2CppRGCTXDataType)2, 598 },
	{ (Il2CppRGCTXDataType)3, 3543 },
	{ (Il2CppRGCTXDataType)2, 715 },
	{ (Il2CppRGCTXDataType)2, 599 },
	{ (Il2CppRGCTXDataType)3, 476 },
	{ (Il2CppRGCTXDataType)2, 600 },
	{ (Il2CppRGCTXDataType)3, 3544 },
	{ (Il2CppRGCTXDataType)2, 716 },
	{ (Il2CppRGCTXDataType)2, 601 },
	{ (Il2CppRGCTXDataType)2, 717 },
	{ (Il2CppRGCTXDataType)3, 500 },
	{ (Il2CppRGCTXDataType)2, 602 },
	{ (Il2CppRGCTXDataType)3, 3545 },
	{ (Il2CppRGCTXDataType)2, 579 },
	{ (Il2CppRGCTXDataType)2, 1182 },
	{ (Il2CppRGCTXDataType)3, 4003 },
	{ (Il2CppRGCTXDataType)2, 709 },
	{ (Il2CppRGCTXDataType)2, 3091 },
	{ (Il2CppRGCTXDataType)3, 4005 },
	{ (Il2CppRGCTXDataType)3, 4006 },
	{ (Il2CppRGCTXDataType)3, 4002 },
	{ (Il2CppRGCTXDataType)3, 4004 },
	{ (Il2CppRGCTXDataType)3, 7739 },
	{ (Il2CppRGCTXDataType)3, 5048 },
	{ (Il2CppRGCTXDataType)3, 2848 },
	{ (Il2CppRGCTXDataType)2, 1065 },
	{ (Il2CppRGCTXDataType)3, 5049 },
	{ (Il2CppRGCTXDataType)2, 2013 },
	{ (Il2CppRGCTXDataType)3, 5050 },
	{ (Il2CppRGCTXDataType)3, 2447 },
	{ (Il2CppRGCTXDataType)2, 192 },
	{ (Il2CppRGCTXDataType)3, 2446 },
	{ (Il2CppRGCTXDataType)2, 1044 },
	{ (Il2CppRGCTXDataType)2, 2686 },
	{ (Il2CppRGCTXDataType)3, 6292 },
	{ (Il2CppRGCTXDataType)1, 1680 },
	{ (Il2CppRGCTXDataType)1, 661 },
	{ (Il2CppRGCTXDataType)3, 4876 },
	{ (Il2CppRGCTXDataType)3, 2468 },
	{ (Il2CppRGCTXDataType)3, 4750 },
	{ (Il2CppRGCTXDataType)3, 4751 },
	{ (Il2CppRGCTXDataType)2, 661 },
	{ (Il2CppRGCTXDataType)2, 1680 },
	{ (Il2CppRGCTXDataType)3, 6294 },
	{ (Il2CppRGCTXDataType)3, 2467 },
	{ (Il2CppRGCTXDataType)2, 1054 },
	{ (Il2CppRGCTXDataType)3, 6293 },
	{ (Il2CppRGCTXDataType)3, 6296 },
	{ (Il2CppRGCTXDataType)3, 6302 },
	{ (Il2CppRGCTXDataType)3, 10854 },
	{ (Il2CppRGCTXDataType)3, 1482 },
	{ (Il2CppRGCTXDataType)2, 776 },
	{ (Il2CppRGCTXDataType)3, 304 },
	{ (Il2CppRGCTXDataType)3, 305 },
	{ (Il2CppRGCTXDataType)2, 1469 },
	{ (Il2CppRGCTXDataType)3, 3550 },
	{ (Il2CppRGCTXDataType)3, 10274 },
	{ (Il2CppRGCTXDataType)3, 1484 },
	{ (Il2CppRGCTXDataType)3, 1483 },
	{ (Il2CppRGCTXDataType)3, 4844 },
	{ (Il2CppRGCTXDataType)3, 4877 },
	{ (Il2CppRGCTXDataType)3, 4845 },
	{ (Il2CppRGCTXDataType)3, 6300 },
	{ (Il2CppRGCTXDataType)3, 10857 },
	{ (Il2CppRGCTXDataType)3, 4749 },
	{ (Il2CppRGCTXDataType)3, 4846 },
	{ (Il2CppRGCTXDataType)3, 4873 },
	{ (Il2CppRGCTXDataType)3, 1485 },
	{ (Il2CppRGCTXDataType)2, 757 },
	{ (Il2CppRGCTXDataType)3, 39 },
	{ (Il2CppRGCTXDataType)2, 1468 },
	{ (Il2CppRGCTXDataType)3, 3549 },
	{ (Il2CppRGCTXDataType)3, 6297 },
	{ (Il2CppRGCTXDataType)2, 1458 },
	{ (Il2CppRGCTXDataType)3, 3546 },
	{ (Il2CppRGCTXDataType)2, 2692 },
	{ (Il2CppRGCTXDataType)3, 6346 },
	{ (Il2CppRGCTXDataType)3, 10842 },
	{ (Il2CppRGCTXDataType)2, 1889 },
	{ (Il2CppRGCTXDataType)2, 1981 },
	{ (Il2CppRGCTXDataType)3, 6299 },
	{ (Il2CppRGCTXDataType)3, 4874 },
	{ (Il2CppRGCTXDataType)3, 1481 },
	{ (Il2CppRGCTXDataType)2, 2686 },
	{ (Il2CppRGCTXDataType)3, 2896 },
	{ (Il2CppRGCTXDataType)3, 4875 },
	{ (Il2CppRGCTXDataType)2, 436 },
	{ (Il2CppRGCTXDataType)3, 2898 },
	{ (Il2CppRGCTXDataType)2, 1088 },
	{ (Il2CppRGCTXDataType)3, 4878 },
	{ (Il2CppRGCTXDataType)3, 6298 },
	{ (Il2CppRGCTXDataType)3, 2876 },
	{ (Il2CppRGCTXDataType)3, 2897 },
	{ (Il2CppRGCTXDataType)3, 6301 },
	{ (Il2CppRGCTXDataType)3, 10831 },
	{ (Il2CppRGCTXDataType)3, 1078 },
	{ (Il2CppRGCTXDataType)3, 6295 },
	{ (Il2CppRGCTXDataType)3, 1077 },
	{ (Il2CppRGCTXDataType)3, 3258 },
	{ (Il2CppRGCTXDataType)3, 1079 },
	{ (Il2CppRGCTXDataType)3, 3273 },
	{ (Il2CppRGCTXDataType)3, 3257 },
	{ (Il2CppRGCTXDataType)3, 3272 },
	{ (Il2CppRGCTXDataType)2, 930 },
	{ (Il2CppRGCTXDataType)3, 1432 },
	{ (Il2CppRGCTXDataType)3, 38 },
	{ (Il2CppRGCTXDataType)2, 1470 },
	{ (Il2CppRGCTXDataType)3, 3551 },
	{ (Il2CppRGCTXDataType)3, 10322 },
	{ (Il2CppRGCTXDataType)2, 935 },
	{ (Il2CppRGCTXDataType)3, 1480 },
	{ (Il2CppRGCTXDataType)2, 2487 },
	{ (Il2CppRGCTXDataType)3, 4872 },
	{ (Il2CppRGCTXDataType)3, 10884 },
	{ (Il2CppRGCTXDataType)3, 10882 },
	{ (Il2CppRGCTXDataType)3, 1435 },
	{ (Il2CppRGCTXDataType)3, 1436 },
	{ (Il2CppRGCTXDataType)3, 1437 },
	{ (Il2CppRGCTXDataType)2, 1987 },
	{ (Il2CppRGCTXDataType)3, 4764 },
	{ (Il2CppRGCTXDataType)2, 564 },
	{ (Il2CppRGCTXDataType)3, 4765 },
	{ (Il2CppRGCTXDataType)2, 704 },
	{ (Il2CppRGCTXDataType)3, 6303 },
	{ (Il2CppRGCTXDataType)3, 3548 },
	{ (Il2CppRGCTXDataType)3, 6348 },
	{ (Il2CppRGCTXDataType)3, 6347 },
	{ (Il2CppRGCTXDataType)2, 1978 },
	{ (Il2CppRGCTXDataType)3, 10323 },
	{ (Il2CppRGCTXDataType)2, 1884 },
	{ (Il2CppRGCTXDataType)2, 736 },
	{ (Il2CppRGCTXDataType)2, 2687 },
	{ (Il2CppRGCTXDataType)3, 4763 },
	{ (Il2CppRGCTXDataType)3, 2901 },
	{ (Il2CppRGCTXDataType)2, 758 },
	{ (Il2CppRGCTXDataType)3, 40 },
	{ (Il2CppRGCTXDataType)2, 758 },
	{ (Il2CppRGCTXDataType)3, 4762 },
	{ (Il2CppRGCTXDataType)3, 4761 },
	{ (Il2CppRGCTXDataType)2, 561 },
	{ (Il2CppRGCTXDataType)2, 700 },
	{ (Il2CppRGCTXDataType)1, 263 },
	{ (Il2CppRGCTXDataType)3, 2849 },
	{ (Il2CppRGCTXDataType)2, 1066 },
	{ (Il2CppRGCTXDataType)2, 1066 },
	{ (Il2CppRGCTXDataType)2, 3152 },
	{ (Il2CppRGCTXDataType)3, 7722 },
	{ (Il2CppRGCTXDataType)2, 3152 },
	{ (Il2CppRGCTXDataType)2, 3839 },
	{ (Il2CppRGCTXDataType)2, 3876 },
	{ (Il2CppRGCTXDataType)2, 1678 },
	{ (Il2CppRGCTXDataType)3, 7724 },
	{ (Il2CppRGCTXDataType)3, 3264 },
	{ (Il2CppRGCTXDataType)3, 3259 },
	{ (Il2CppRGCTXDataType)2, 1232 },
	{ (Il2CppRGCTXDataType)3, 3261 },
	{ (Il2CppRGCTXDataType)3, 7728 },
	{ (Il2CppRGCTXDataType)3, 8794 },
	{ (Il2CppRGCTXDataType)3, 7723 },
	{ (Il2CppRGCTXDataType)3, 3263 },
	{ (Il2CppRGCTXDataType)3, 3262 },
	{ (Il2CppRGCTXDataType)3, 7726 },
	{ (Il2CppRGCTXDataType)3, 3265 },
	{ (Il2CppRGCTXDataType)3, 3260 },
	{ (Il2CppRGCTXDataType)3, 3529 },
	{ (Il2CppRGCTXDataType)3, 7736 },
	{ (Il2CppRGCTXDataType)3, 7735 },
	{ (Il2CppRGCTXDataType)3, 3266 },
	{ (Il2CppRGCTXDataType)3, 8842 },
	{ (Il2CppRGCTXDataType)3, 7734 },
	{ (Il2CppRGCTXDataType)2, 3839 },
	{ (Il2CppRGCTXDataType)3, 10855 },
	{ (Il2CppRGCTXDataType)3, 7731 },
	{ (Il2CppRGCTXDataType)3, 10843 },
	{ (Il2CppRGCTXDataType)2, 467 },
	{ (Il2CppRGCTXDataType)2, 1849 },
	{ (Il2CppRGCTXDataType)2, 1939 },
	{ (Il2CppRGCTXDataType)3, 7725 },
	{ (Il2CppRGCTXDataType)2, 783 },
	{ (Il2CppRGCTXDataType)3, 353 },
	{ (Il2CppRGCTXDataType)3, 7729 },
	{ (Il2CppRGCTXDataType)3, 7730 },
	{ (Il2CppRGCTXDataType)3, 2880 },
	{ (Il2CppRGCTXDataType)3, 7737 },
	{ (Il2CppRGCTXDataType)3, 7738 },
	{ (Il2CppRGCTXDataType)3, 7727 },
	{ (Il2CppRGCTXDataType)3, 7732 },
	{ (Il2CppRGCTXDataType)3, 7733 },
	{ (Il2CppRGCTXDataType)3, 10883 },
	{ (Il2CppRGCTXDataType)2, 581 },
};
extern const CustomAttributesCacheGenerator g_Google_Protobuf_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_Google_Protobuf_CodeGenModule;
const Il2CppCodeGenModule g_Google_Protobuf_CodeGenModule = 
{
	"Google.Protobuf.dll",
	2103,
	s_methodPointers,
	52,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	98,
	s_rgctxIndices,
	441,
	s_rgctxValues,
	NULL,
	g_Google_Protobuf_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
