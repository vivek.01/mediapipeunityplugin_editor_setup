﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// System.Reflection.AssemblyCompanyAttribute
struct AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4;
// System.Reflection.AssemblyConfigurationAttribute
struct AssemblyConfigurationAttribute_t071B324A83314FBA14A43F37BE7206C420218B7C;
// System.Reflection.AssemblyCopyrightAttribute
struct AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC;
// System.Reflection.AssemblyDescriptionAttribute
struct AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3;
// System.Reflection.AssemblyFileVersionAttribute
struct AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F;
// System.Reflection.AssemblyInformationalVersionAttribute
struct AssemblyInformationalVersionAttribute_t962229DBE84C4A66FB0B542E9AEBC510F55950D0;
// System.Reflection.AssemblyProductAttribute
struct AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA;
// System.Reflection.AssemblyTitleAttribute
struct AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7;
// System.AttributeUsageAttribute
struct AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88;
// System.Diagnostics.DebuggerNonUserCodeAttribute
struct DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41;
// System.Reflection.DefaultMemberAttribute
struct DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5;
// Microsoft.CodeAnalysis.EmbeddedAttribute
struct EmbeddedAttribute_tCE17AEDD3793DB07D23D49461AD1F51E151C6173;
// System.Runtime.CompilerServices.ExtensionAttribute
struct ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC;
// System.Runtime.CompilerServices.InternalsVisibleToAttribute
struct InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C;
// System.Runtime.CompilerServices.IsByRefLikeAttribute
struct IsByRefLikeAttribute_t1F586EB86302883DEC1CCE8E0E6801E76D2601CC;
// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// System.ObsoleteAttribute
struct ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671;
// Google.Protobuf.Reflection.OriginalNameAttribute
struct OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664;
// System.ParamArrayAttribute
struct ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// System.String
struct String_t;
// System.Runtime.Versioning.TargetFrameworkAttribute
struct TargetFrameworkAttribute_t9FA66D5D5B274F0E1A4FE20162AA70F62BFFB517;
// System.Type
struct Type_t;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;

IL2CPP_EXTERN_C const RuntimeType* U3CGetEnumeratorU3Ed__28_t1D89B8849E2BE4C57358D822F30C41B8A6D1339A_0_0_0_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Reflection.AssemblyCompanyAttribute
struct AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyCompanyAttribute::m_company
	String_t* ___m_company_0;

public:
	inline static int32_t get_offset_of_m_company_0() { return static_cast<int32_t>(offsetof(AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4, ___m_company_0)); }
	inline String_t* get_m_company_0() const { return ___m_company_0; }
	inline String_t** get_address_of_m_company_0() { return &___m_company_0; }
	inline void set_m_company_0(String_t* value)
	{
		___m_company_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_company_0), (void*)value);
	}
};


// System.Reflection.AssemblyConfigurationAttribute
struct AssemblyConfigurationAttribute_t071B324A83314FBA14A43F37BE7206C420218B7C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyConfigurationAttribute::m_configuration
	String_t* ___m_configuration_0;

public:
	inline static int32_t get_offset_of_m_configuration_0() { return static_cast<int32_t>(offsetof(AssemblyConfigurationAttribute_t071B324A83314FBA14A43F37BE7206C420218B7C, ___m_configuration_0)); }
	inline String_t* get_m_configuration_0() const { return ___m_configuration_0; }
	inline String_t** get_address_of_m_configuration_0() { return &___m_configuration_0; }
	inline void set_m_configuration_0(String_t* value)
	{
		___m_configuration_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_configuration_0), (void*)value);
	}
};


// System.Reflection.AssemblyCopyrightAttribute
struct AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyCopyrightAttribute::m_copyright
	String_t* ___m_copyright_0;

public:
	inline static int32_t get_offset_of_m_copyright_0() { return static_cast<int32_t>(offsetof(AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC, ___m_copyright_0)); }
	inline String_t* get_m_copyright_0() const { return ___m_copyright_0; }
	inline String_t** get_address_of_m_copyright_0() { return &___m_copyright_0; }
	inline void set_m_copyright_0(String_t* value)
	{
		___m_copyright_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_copyright_0), (void*)value);
	}
};


// System.Reflection.AssemblyDescriptionAttribute
struct AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyDescriptionAttribute::m_description
	String_t* ___m_description_0;

public:
	inline static int32_t get_offset_of_m_description_0() { return static_cast<int32_t>(offsetof(AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3, ___m_description_0)); }
	inline String_t* get_m_description_0() const { return ___m_description_0; }
	inline String_t** get_address_of_m_description_0() { return &___m_description_0; }
	inline void set_m_description_0(String_t* value)
	{
		___m_description_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_description_0), (void*)value);
	}
};


// System.Reflection.AssemblyFileVersionAttribute
struct AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyFileVersionAttribute::_version
	String_t* ____version_0;

public:
	inline static int32_t get_offset_of__version_0() { return static_cast<int32_t>(offsetof(AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F, ____version_0)); }
	inline String_t* get__version_0() const { return ____version_0; }
	inline String_t** get_address_of__version_0() { return &____version_0; }
	inline void set__version_0(String_t* value)
	{
		____version_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____version_0), (void*)value);
	}
};


// System.Reflection.AssemblyInformationalVersionAttribute
struct AssemblyInformationalVersionAttribute_t962229DBE84C4A66FB0B542E9AEBC510F55950D0  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyInformationalVersionAttribute::m_informationalVersion
	String_t* ___m_informationalVersion_0;

public:
	inline static int32_t get_offset_of_m_informationalVersion_0() { return static_cast<int32_t>(offsetof(AssemblyInformationalVersionAttribute_t962229DBE84C4A66FB0B542E9AEBC510F55950D0, ___m_informationalVersion_0)); }
	inline String_t* get_m_informationalVersion_0() const { return ___m_informationalVersion_0; }
	inline String_t** get_address_of_m_informationalVersion_0() { return &___m_informationalVersion_0; }
	inline void set_m_informationalVersion_0(String_t* value)
	{
		___m_informationalVersion_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_informationalVersion_0), (void*)value);
	}
};


// System.Reflection.AssemblyProductAttribute
struct AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyProductAttribute::m_product
	String_t* ___m_product_0;

public:
	inline static int32_t get_offset_of_m_product_0() { return static_cast<int32_t>(offsetof(AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA, ___m_product_0)); }
	inline String_t* get_m_product_0() const { return ___m_product_0; }
	inline String_t** get_address_of_m_product_0() { return &___m_product_0; }
	inline void set_m_product_0(String_t* value)
	{
		___m_product_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_product_0), (void*)value);
	}
};


// System.Reflection.AssemblyTitleAttribute
struct AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyTitleAttribute::m_title
	String_t* ___m_title_0;

public:
	inline static int32_t get_offset_of_m_title_0() { return static_cast<int32_t>(offsetof(AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7, ___m_title_0)); }
	inline String_t* get_m_title_0() const { return ___m_title_0; }
	inline String_t** get_address_of_m_title_0() { return &___m_title_0; }
	inline void set_m_title_0(String_t* value)
	{
		___m_title_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_title_0), (void*)value);
	}
};


// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Diagnostics.DebuggerNonUserCodeAttribute
struct DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Reflection.DefaultMemberAttribute
struct DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.DefaultMemberAttribute::m_memberName
	String_t* ___m_memberName_0;

public:
	inline static int32_t get_offset_of_m_memberName_0() { return static_cast<int32_t>(offsetof(DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5, ___m_memberName_0)); }
	inline String_t* get_m_memberName_0() const { return ___m_memberName_0; }
	inline String_t** get_address_of_m_memberName_0() { return &___m_memberName_0; }
	inline void set_m_memberName_0(String_t* value)
	{
		___m_memberName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_memberName_0), (void*)value);
	}
};


// Microsoft.CodeAnalysis.EmbeddedAttribute
struct EmbeddedAttribute_tCE17AEDD3793DB07D23D49461AD1F51E151C6173  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Runtime.CompilerServices.ExtensionAttribute
struct ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Runtime.CompilerServices.InternalsVisibleToAttribute
struct InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Runtime.CompilerServices.InternalsVisibleToAttribute::_assemblyName
	String_t* ____assemblyName_0;
	// System.Boolean System.Runtime.CompilerServices.InternalsVisibleToAttribute::_allInternalsVisible
	bool ____allInternalsVisible_1;

public:
	inline static int32_t get_offset_of__assemblyName_0() { return static_cast<int32_t>(offsetof(InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C, ____assemblyName_0)); }
	inline String_t* get__assemblyName_0() const { return ____assemblyName_0; }
	inline String_t** get_address_of__assemblyName_0() { return &____assemblyName_0; }
	inline void set__assemblyName_0(String_t* value)
	{
		____assemblyName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____assemblyName_0), (void*)value);
	}

	inline static int32_t get_offset_of__allInternalsVisible_1() { return static_cast<int32_t>(offsetof(InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C, ____allInternalsVisible_1)); }
	inline bool get__allInternalsVisible_1() const { return ____allInternalsVisible_1; }
	inline bool* get_address_of__allInternalsVisible_1() { return &____allInternalsVisible_1; }
	inline void set__allInternalsVisible_1(bool value)
	{
		____allInternalsVisible_1 = value;
	}
};


// System.Runtime.CompilerServices.IsByRefLikeAttribute
struct IsByRefLikeAttribute_t1F586EB86302883DEC1CCE8E0E6801E76D2601CC  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.ObsoleteAttribute
struct ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.ObsoleteAttribute::_message
	String_t* ____message_0;
	// System.Boolean System.ObsoleteAttribute::_error
	bool ____error_1;

public:
	inline static int32_t get_offset_of__message_0() { return static_cast<int32_t>(offsetof(ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671, ____message_0)); }
	inline String_t* get__message_0() const { return ____message_0; }
	inline String_t** get_address_of__message_0() { return &____message_0; }
	inline void set__message_0(String_t* value)
	{
		____message_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_0), (void*)value);
	}

	inline static int32_t get_offset_of__error_1() { return static_cast<int32_t>(offsetof(ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671, ____error_1)); }
	inline bool get__error_1() const { return ____error_1; }
	inline bool* get_address_of__error_1() { return &____error_1; }
	inline void set__error_1(bool value)
	{
		____error_1 = value;
	}
};


// Google.Protobuf.Reflection.OriginalNameAttribute
struct OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String Google.Protobuf.Reflection.OriginalNameAttribute::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;
	// System.Boolean Google.Protobuf.Reflection.OriginalNameAttribute::<PreferredAlias>k__BackingField
	bool ___U3CPreferredAliasU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664, ___U3CNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CNameU3Ek__BackingField_0() const { return ___U3CNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_0() { return &___U3CNameU3Ek__BackingField_0; }
	inline void set_U3CNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CNameU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CPreferredAliasU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664, ___U3CPreferredAliasU3Ek__BackingField_1)); }
	inline bool get_U3CPreferredAliasU3Ek__BackingField_1() const { return ___U3CPreferredAliasU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CPreferredAliasU3Ek__BackingField_1() { return &___U3CPreferredAliasU3Ek__BackingField_1; }
	inline void set_U3CPreferredAliasU3Ek__BackingField_1(bool value)
	{
		___U3CPreferredAliasU3Ek__BackingField_1 = value;
	}
};


// System.ParamArrayAttribute
struct ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// System.Runtime.CompilerServices.StateMachineAttribute
struct StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type System.Runtime.CompilerServices.StateMachineAttribute::<StateMachineType>k__BackingField
	Type_t * ___U3CStateMachineTypeU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CStateMachineTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3, ___U3CStateMachineTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CStateMachineTypeU3Ek__BackingField_0() const { return ___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CStateMachineTypeU3Ek__BackingField_0() { return &___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline void set_U3CStateMachineTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CStateMachineTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CStateMachineTypeU3Ek__BackingField_0), (void*)value);
	}
};


// System.Runtime.Versioning.TargetFrameworkAttribute
struct TargetFrameworkAttribute_t9FA66D5D5B274F0E1A4FE20162AA70F62BFFB517  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Runtime.Versioning.TargetFrameworkAttribute::_frameworkName
	String_t* ____frameworkName_0;
	// System.String System.Runtime.Versioning.TargetFrameworkAttribute::_frameworkDisplayName
	String_t* ____frameworkDisplayName_1;

public:
	inline static int32_t get_offset_of__frameworkName_0() { return static_cast<int32_t>(offsetof(TargetFrameworkAttribute_t9FA66D5D5B274F0E1A4FE20162AA70F62BFFB517, ____frameworkName_0)); }
	inline String_t* get__frameworkName_0() const { return ____frameworkName_0; }
	inline String_t** get_address_of__frameworkName_0() { return &____frameworkName_0; }
	inline void set__frameworkName_0(String_t* value)
	{
		____frameworkName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____frameworkName_0), (void*)value);
	}

	inline static int32_t get_offset_of__frameworkDisplayName_1() { return static_cast<int32_t>(offsetof(TargetFrameworkAttribute_t9FA66D5D5B274F0E1A4FE20162AA70F62BFFB517, ____frameworkDisplayName_1)); }
	inline String_t* get__frameworkDisplayName_1() const { return ____frameworkDisplayName_1; }
	inline String_t** get_address_of__frameworkDisplayName_1() { return &____frameworkDisplayName_1; }
	inline void set__frameworkDisplayName_1(String_t* value)
	{
		____frameworkDisplayName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____frameworkDisplayName_1), (void*)value);
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.AttributeTargets
struct AttributeTargets_t5F71273DFE1D0CA9B8109F02A023A7DBA9BFC923 
{
public:
	// System.Int32 System.AttributeTargets::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AttributeTargets_t5F71273DFE1D0CA9B8109F02A023A7DBA9BFC923, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Reflection.BindingFlags
struct BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830  : public StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3
{
public:

public:
};


// System.RuntimeTypeHandle
struct RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.AttributeUsageAttribute
struct AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.AttributeTargets System.AttributeUsageAttribute::m_attributeTarget
	int32_t ___m_attributeTarget_0;
	// System.Boolean System.AttributeUsageAttribute::m_allowMultiple
	bool ___m_allowMultiple_1;
	// System.Boolean System.AttributeUsageAttribute::m_inherited
	bool ___m_inherited_2;

public:
	inline static int32_t get_offset_of_m_attributeTarget_0() { return static_cast<int32_t>(offsetof(AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C, ___m_attributeTarget_0)); }
	inline int32_t get_m_attributeTarget_0() const { return ___m_attributeTarget_0; }
	inline int32_t* get_address_of_m_attributeTarget_0() { return &___m_attributeTarget_0; }
	inline void set_m_attributeTarget_0(int32_t value)
	{
		___m_attributeTarget_0 = value;
	}

	inline static int32_t get_offset_of_m_allowMultiple_1() { return static_cast<int32_t>(offsetof(AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C, ___m_allowMultiple_1)); }
	inline bool get_m_allowMultiple_1() const { return ___m_allowMultiple_1; }
	inline bool* get_address_of_m_allowMultiple_1() { return &___m_allowMultiple_1; }
	inline void set_m_allowMultiple_1(bool value)
	{
		___m_allowMultiple_1 = value;
	}

	inline static int32_t get_offset_of_m_inherited_2() { return static_cast<int32_t>(offsetof(AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C, ___m_inherited_2)); }
	inline bool get_m_inherited_2() const { return ___m_inherited_2; }
	inline bool* get_address_of_m_inherited_2() { return &___m_inherited_2; }
	inline void set_m_inherited_2(bool value)
	{
		___m_inherited_2 = value;
	}
};

struct AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C_StaticFields
{
public:
	// System.AttributeUsageAttribute System.AttributeUsageAttribute::Default
	AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * ___Default_3;

public:
	inline static int32_t get_offset_of_Default_3() { return static_cast<int32_t>(offsetof(AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C_StaticFields, ___Default_3)); }
	inline AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * get_Default_3() const { return ___Default_3; }
	inline AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C ** get_address_of_Default_3() { return &___Default_3; }
	inline void set_Default_3(AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * value)
	{
		___Default_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Default_3), (void*)value);
	}
};


// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};


// System.Type
struct Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



// System.Void System.Reflection.AssemblyTitleAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyTitleAttribute__ctor_mE239F206B3B369C48AE1F3B4211688778FE99E8D (AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7 * __this, String_t* ___title0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyInformationalVersionAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyInformationalVersionAttribute__ctor_m9BF349D8F980B0ABAB2A6312E422915285FA1678 (AssemblyInformationalVersionAttribute_t962229DBE84C4A66FB0B542E9AEBC510F55950D0 * __this, String_t* ___informationalVersion0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyFileVersionAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyFileVersionAttribute__ctor_mF855AEBC51CB72F4FF913499256741AE57B0F13D (AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F * __this, String_t* ___version0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyDescriptionAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyDescriptionAttribute__ctor_m3A0BD500FF352A67235FBA499FBA58EFF15B1F25 (AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3 * __this, String_t* ___description0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyCopyrightAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyCopyrightAttribute__ctor_mB0B5F5C1A7A8B172289CC694E2711F07A37CE3F3 (AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC * __this, String_t* ___copyright0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyConfigurationAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyConfigurationAttribute__ctor_m6EE76F5A155EDEA71967A32F78D777038ADD0757 (AssemblyConfigurationAttribute_t071B324A83314FBA14A43F37BE7206C420218B7C * __this, String_t* ___configuration0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyCompanyAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyCompanyAttribute__ctor_m435C9FEC405646617645636E67860598A0C46FF0 (AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4 * __this, String_t* ___company0, const RuntimeMethod* method);
// System.Void System.Runtime.Versioning.TargetFrameworkAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TargetFrameworkAttribute__ctor_m0F8E5550F9199AC44F2CBCCD3E968EC26731187D (TargetFrameworkAttribute_t9FA66D5D5B274F0E1A4FE20162AA70F62BFFB517 * __this, String_t* ___frameworkName0, const RuntimeMethod* method);
// System.Void System.Runtime.Versioning.TargetFrameworkAttribute::set_FrameworkDisplayName(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void TargetFrameworkAttribute_set_FrameworkDisplayName_mB89F1A63CB77A414AF46D5695B37CD520EAB52AB_inline (TargetFrameworkAttribute_t9FA66D5D5B274F0E1A4FE20162AA70F62BFFB517 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.InternalsVisibleToAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9 (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * __this, String_t* ___assemblyName0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.ExtensionAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * __this, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyProductAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyProductAttribute__ctor_m26DF1EBC1C86E7DA4786C66B44123899BE8DBCB8 (AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA * __this, String_t* ___product0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
// System.Void Microsoft.CodeAnalysis.EmbeddedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EmbeddedAttribute__ctor_mC4A60001FA556CF2C0A731104B5CEC786B0CB863 (EmbeddedAttribute_tCE17AEDD3793DB07D23D49461AD1F51E151C6173 * __this, const RuntimeMethod* method);
// System.Void System.Reflection.DefaultMemberAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7 (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * __this, String_t* ___memberName0, const RuntimeMethod* method);
// System.Void System.ParamArrayAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719 (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * __this, const RuntimeMethod* method);
// System.Void System.ObsoleteAttribute::.ctor(System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ObsoleteAttribute__ctor_m058D2B798E9AB7E7778A6DB04C3FE7011C660F4C (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * __this, String_t* ___message0, bool ___error1, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.IsByRefLikeAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IsByRefLikeAttribute__ctor_m80997F85AA38401591B7574259E29881904ADC76 (IsByRefLikeAttribute_t1F586EB86302883DEC1CCE8E0E6801E76D2601CC * __this, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerNonUserCodeAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * __this, const RuntimeMethod* method);
// System.Void Google.Protobuf.Reflection.OriginalNameAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OriginalNameAttribute__ctor_mB0C6C38C49B9261D2DC423D692E80B8EBC657BF9 (OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * __this, String_t* ___name0, const RuntimeMethod* method);
// System.Void System.ObsoleteAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ObsoleteAttribute__ctor_m9BC17A80675E9013AA71F9FB38D89FEF56883853 (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * __this, const RuntimeMethod* method);
// System.Void System.AttributeUsageAttribute::.ctor(System.AttributeTargets)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * __this, int32_t ___validOn0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.IteratorStateMachineAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481 (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * __this, Type_t * ___stateMachineType0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerHiddenAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3 (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * __this, const RuntimeMethod* method);
static void Google_Protobuf_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7 * tmp = (AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7 *)cache->attributes[0];
		AssemblyTitleAttribute__ctor_mE239F206B3B369C48AE1F3B4211688778FE99E8D(tmp, il2cpp_codegen_string_new_wrapper("\x47\x6F\x6F\x67\x6C\x65\x20\x50\x72\x6F\x74\x6F\x63\x6F\x6C\x20\x42\x75\x66\x66\x65\x72\x73"), NULL);
	}
	{
		AssemblyInformationalVersionAttribute_t962229DBE84C4A66FB0B542E9AEBC510F55950D0 * tmp = (AssemblyInformationalVersionAttribute_t962229DBE84C4A66FB0B542E9AEBC510F55950D0 *)cache->attributes[1];
		AssemblyInformationalVersionAttribute__ctor_m9BF349D8F980B0ABAB2A6312E422915285FA1678(tmp, il2cpp_codegen_string_new_wrapper("\x33\x2E\x31\x35\x2E\x36\x2B\x36\x61\x61\x35\x33\x39\x62\x66\x30\x31\x39\x35\x66\x31\x38\x38\x66\x66\x38\x36\x65\x66\x65\x36\x66\x62\x38\x62\x66\x61\x32\x62\x36\x37\x36\x63\x64\x64\x34\x36"), NULL);
	}
	{
		AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F * tmp = (AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F *)cache->attributes[2];
		AssemblyFileVersionAttribute__ctor_mF855AEBC51CB72F4FF913499256741AE57B0F13D(tmp, il2cpp_codegen_string_new_wrapper("\x33\x2E\x31\x35\x2E\x36\x2E\x30"), NULL);
	}
	{
		AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3 * tmp = (AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3 *)cache->attributes[3];
		AssemblyDescriptionAttribute__ctor_m3A0BD500FF352A67235FBA499FBA58EFF15B1F25(tmp, il2cpp_codegen_string_new_wrapper("\x43\x23\x20\x72\x75\x6E\x74\x69\x6D\x65\x20\x6C\x69\x62\x72\x61\x72\x79\x20\x66\x6F\x72\x20\x50\x72\x6F\x74\x6F\x63\x6F\x6C\x20\x42\x75\x66\x66\x65\x72\x73\x20\x2D\x20\x47\x6F\x6F\x67\x6C\x65\x27\x73\x20\x64\x61\x74\x61\x20\x69\x6E\x74\x65\x72\x63\x68\x61\x6E\x67\x65\x20\x66\x6F\x72\x6D\x61\x74\x2E"), NULL);
	}
	{
		AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC * tmp = (AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC *)cache->attributes[4];
		AssemblyCopyrightAttribute__ctor_mB0B5F5C1A7A8B172289CC694E2711F07A37CE3F3(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x70\x79\x72\x69\x67\x68\x74\x20\x32\x30\x31\x35\x2C\x20\x47\x6F\x6F\x67\x6C\x65\x20\x49\x6E\x63\x2E"), NULL);
	}
	{
		AssemblyConfigurationAttribute_t071B324A83314FBA14A43F37BE7206C420218B7C * tmp = (AssemblyConfigurationAttribute_t071B324A83314FBA14A43F37BE7206C420218B7C *)cache->attributes[5];
		AssemblyConfigurationAttribute__ctor_m6EE76F5A155EDEA71967A32F78D777038ADD0757(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x6C\x65\x61\x73\x65"), NULL);
	}
	{
		AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4 * tmp = (AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4 *)cache->attributes[6];
		AssemblyCompanyAttribute__ctor_m435C9FEC405646617645636E67860598A0C46FF0(tmp, il2cpp_codegen_string_new_wrapper("\x47\x6F\x6F\x67\x6C\x65\x20\x49\x6E\x63\x2E"), NULL);
	}
	{
		TargetFrameworkAttribute_t9FA66D5D5B274F0E1A4FE20162AA70F62BFFB517 * tmp = (TargetFrameworkAttribute_t9FA66D5D5B274F0E1A4FE20162AA70F62BFFB517 *)cache->attributes[7];
		TargetFrameworkAttribute__ctor_m0F8E5550F9199AC44F2CBCCD3E968EC26731187D(tmp, il2cpp_codegen_string_new_wrapper("\x2E\x4E\x45\x54\x53\x74\x61\x6E\x64\x61\x72\x64\x2C\x56\x65\x72\x73\x69\x6F\x6E\x3D\x76\x32\x2E\x30"), NULL);
		TargetFrameworkAttribute_set_FrameworkDisplayName_mB89F1A63CB77A414AF46D5695B37CD520EAB52AB_inline(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
	{
		InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * tmp = (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C *)cache->attributes[8];
		InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9(tmp, il2cpp_codegen_string_new_wrapper("\x47\x6F\x6F\x67\x6C\x65\x2E\x50\x72\x6F\x74\x6F\x62\x75\x66\x2E\x42\x65\x6E\x63\x68\x6D\x61\x72\x6B\x73\x2C\x20\x50\x75\x62\x6C\x69\x63\x4B\x65\x79\x3D\x30\x30\x32\x34\x30\x30\x30\x30\x30\x34\x38\x30\x30\x30\x30\x30\x39\x34\x30\x30\x30\x30\x30\x30\x30\x36\x30\x32\x30\x30\x30\x30\x30\x30\x32\x34\x30\x30\x30\x30\x35\x32\x35\x33\x34\x31\x33\x31\x30\x30\x30\x34\x30\x30\x30\x30\x30\x31\x30\x30\x30\x31\x30\x30\x32\x35\x38\x30\x30\x66\x62\x63\x66\x63\x36\x33\x61\x31\x37\x63\x36\x36\x62\x33\x30\x33\x61\x61\x65\x38\x30\x62\x30\x33\x61\x36\x62\x65\x61\x61\x31\x37\x36\x62\x62\x36\x62\x65\x66\x38\x38\x33\x62\x65\x34\x33\x36\x66\x32\x61\x31\x35\x37\x39\x65\x64\x64\x38\x30\x63\x65\x32\x33\x65\x64\x66\x31\x35\x31\x61\x31\x66\x34\x63\x65\x64\x39\x37\x61\x66\x38\x33\x61\x62\x63\x64\x39\x38\x31\x32\x30\x37\x30\x34\x31\x66\x64\x35\x62\x32\x64\x61\x33\x62\x34\x39\x38\x33\x34\x36\x66\x63\x66\x63\x64\x39\x34\x39\x31\x30\x64\x35\x32\x66\x32\x35\x35\x33\x37\x63\x34\x61\x34\x33\x63\x65\x33\x66\x62\x65\x31\x37\x64\x63\x37\x64\x34\x33\x65\x36\x63\x62\x64\x62\x34\x64\x38\x66\x31\x32\x34\x32\x64\x63\x62\x36\x62\x64\x39\x62\x35\x39\x30\x36\x62\x65\x37\x34\x64\x61\x38\x64\x61\x61\x37\x64\x37\x32\x38\x30\x66\x39\x37\x31\x33\x30\x66\x33\x31\x38\x61\x31\x36\x63\x30\x37\x62\x61\x66\x31\x31\x38\x38\x33\x39\x62\x31\x35\x36\x32\x39\x39\x61\x34\x38\x35\x32\x32\x66\x39\x66\x61\x65\x32\x33\x37\x31\x63\x39\x36\x36\x35\x63\x35\x61\x65\x39\x63\x62\x36"), NULL);
	}
	{
		InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * tmp = (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C *)cache->attributes[9];
		InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9(tmp, il2cpp_codegen_string_new_wrapper("\x47\x6F\x6F\x67\x6C\x65\x2E\x50\x72\x6F\x74\x6F\x62\x75\x66\x2E\x54\x65\x73\x74\x2C\x20\x50\x75\x62\x6C\x69\x63\x4B\x65\x79\x3D\x30\x30\x32\x34\x30\x30\x30\x30\x30\x34\x38\x30\x30\x30\x30\x30\x39\x34\x30\x30\x30\x30\x30\x30\x30\x36\x30\x32\x30\x30\x30\x30\x30\x30\x32\x34\x30\x30\x30\x30\x35\x32\x35\x33\x34\x31\x33\x31\x30\x30\x30\x34\x30\x30\x30\x30\x30\x31\x30\x30\x30\x31\x30\x30\x32\x35\x38\x30\x30\x66\x62\x63\x66\x63\x36\x33\x61\x31\x37\x63\x36\x36\x62\x33\x30\x33\x61\x61\x65\x38\x30\x62\x30\x33\x61\x36\x62\x65\x61\x61\x31\x37\x36\x62\x62\x36\x62\x65\x66\x38\x38\x33\x62\x65\x34\x33\x36\x66\x32\x61\x31\x35\x37\x39\x65\x64\x64\x38\x30\x63\x65\x32\x33\x65\x64\x66\x31\x35\x31\x61\x31\x66\x34\x63\x65\x64\x39\x37\x61\x66\x38\x33\x61\x62\x63\x64\x39\x38\x31\x32\x30\x37\x30\x34\x31\x66\x64\x35\x62\x32\x64\x61\x33\x62\x34\x39\x38\x33\x34\x36\x66\x63\x66\x63\x64\x39\x34\x39\x31\x30\x64\x35\x32\x66\x32\x35\x35\x33\x37\x63\x34\x61\x34\x33\x63\x65\x33\x66\x62\x65\x31\x37\x64\x63\x37\x64\x34\x33\x65\x36\x63\x62\x64\x62\x34\x64\x38\x66\x31\x32\x34\x32\x64\x63\x62\x36\x62\x64\x39\x62\x35\x39\x30\x36\x62\x65\x37\x34\x64\x61\x38\x64\x61\x61\x37\x64\x37\x32\x38\x30\x66\x39\x37\x31\x33\x30\x66\x33\x31\x38\x61\x31\x36\x63\x30\x37\x62\x61\x66\x31\x31\x38\x38\x33\x39\x62\x31\x35\x36\x32\x39\x39\x61\x34\x38\x35\x32\x32\x66\x39\x66\x61\x65\x32\x33\x37\x31\x63\x39\x36\x36\x35\x63\x35\x61\x65\x39\x63\x62\x36"), NULL);
	}
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[10];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 2LL, NULL);
	}
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[11];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[12];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[13];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
	{
		AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA * tmp = (AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA *)cache->attributes[14];
		AssemblyProductAttribute__ctor_m26DF1EBC1C86E7DA4786C66B44123899BE8DBCB8(tmp, il2cpp_codegen_string_new_wrapper("\x47\x6F\x6F\x67\x6C\x65\x2E\x50\x72\x6F\x74\x6F\x62\x75\x66"), NULL);
	}
}
static void EmbeddedAttribute_tCE17AEDD3793DB07D23D49461AD1F51E151C6173_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		EmbeddedAttribute_tCE17AEDD3793DB07D23D49461AD1F51E151C6173 * tmp = (EmbeddedAttribute_tCE17AEDD3793DB07D23D49461AD1F51E151C6173 *)cache->attributes[1];
		EmbeddedAttribute__ctor_mC4A60001FA556CF2C0A731104B5CEC786B0CB863(tmp, NULL);
	}
}
static void IsByRefLikeAttribute_t1F586EB86302883DEC1CCE8E0E6801E76D2601CC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		EmbeddedAttribute_tCE17AEDD3793DB07D23D49461AD1F51E151C6173 * tmp = (EmbeddedAttribute_tCE17AEDD3793DB07D23D49461AD1F51E151C6173 *)cache->attributes[0];
		EmbeddedAttribute__ctor_mC4A60001FA556CF2C0A731104B5CEC786B0CB863(tmp, NULL);
	}
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ByteString_t3B1232ECD50ECBDD385E2BF61FEEF3B17FC5D76D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * tmp = (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
}
static void ByteString_t3B1232ECD50ECBDD385E2BF61FEEF3B17FC5D76D_CustomAttributesCacheGenerator_ByteString_CopyFrom_m5B311324FC5F998F3DB5B3E2B6AFAB16C497B8B0____bytes0(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void Extension_tB7B39D2C1DC9D59405BA177A9CDABC7926F94A18_CustomAttributesCacheGenerator_U3CFieldNumberU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Extension_tB7B39D2C1DC9D59405BA177A9CDABC7926F94A18_CustomAttributesCacheGenerator_Extension_get_FieldNumber_mA033A36AD6AC429AFD166C84A6BBB13ECD45B579(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_tB01692A37F043FB22CAB0C7A8C9914F601DD29D8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ExtensionSet_1_tFE071C084D6C1699836E2BD707D736B0862EF740_CustomAttributesCacheGenerator_U3CValuesByNumberU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ExtensionSet_1_tFE071C084D6C1699836E2BD707D736B0862EF740_CustomAttributesCacheGenerator_ExtensionSet_1_get_ValuesByNumber_m0299171B7A7A6527A32C02787EA4CE60FC51FA37(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_t4E0AE2EB106891F592691D628191F93E2EA66145_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass32_0_1_tD4FAD14FB524139BEAB58126183FF9496AB3CEE5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__32_1_tC96173B1C6BFD751304AC8417C90CFBB1BC5097B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FieldCodec_1_t713DE327211056F9ECCB1D1CBD9A83826813DBEA_CustomAttributesCacheGenerator_U3CPackedRepeatedFieldU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FieldCodec_1_t713DE327211056F9ECCB1D1CBD9A83826813DBEA_CustomAttributesCacheGenerator_U3CValueWriterU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FieldCodec_1_t713DE327211056F9ECCB1D1CBD9A83826813DBEA_CustomAttributesCacheGenerator_U3CValueSizeCalculatorU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FieldCodec_1_t713DE327211056F9ECCB1D1CBD9A83826813DBEA_CustomAttributesCacheGenerator_U3CValueReaderU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FieldCodec_1_t713DE327211056F9ECCB1D1CBD9A83826813DBEA_CustomAttributesCacheGenerator_U3CValueMergerU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FieldCodec_1_t713DE327211056F9ECCB1D1CBD9A83826813DBEA_CustomAttributesCacheGenerator_U3CFieldMergerU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FieldCodec_1_t713DE327211056F9ECCB1D1CBD9A83826813DBEA_CustomAttributesCacheGenerator_U3CFixedSizeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FieldCodec_1_t713DE327211056F9ECCB1D1CBD9A83826813DBEA_CustomAttributesCacheGenerator_U3CTagU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FieldCodec_1_t713DE327211056F9ECCB1D1CBD9A83826813DBEA_CustomAttributesCacheGenerator_U3CEndTagU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FieldCodec_1_t713DE327211056F9ECCB1D1CBD9A83826813DBEA_CustomAttributesCacheGenerator_U3CDefaultValueU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FieldCodec_1_t713DE327211056F9ECCB1D1CBD9A83826813DBEA_CustomAttributesCacheGenerator_FieldCodec_1_get_PackedRepeatedField_m1D3D6BC695091ED29719A16A83C734449B3FA569(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FieldCodec_1_t713DE327211056F9ECCB1D1CBD9A83826813DBEA_CustomAttributesCacheGenerator_FieldCodec_1_get_ValueWriter_m3579154316AAF4F1A6107B36039D864772957EBF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FieldCodec_1_t713DE327211056F9ECCB1D1CBD9A83826813DBEA_CustomAttributesCacheGenerator_FieldCodec_1_get_ValueSizeCalculator_m0ABFD83323840A9C4545573D572CA2B19CA45928(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FieldCodec_1_t713DE327211056F9ECCB1D1CBD9A83826813DBEA_CustomAttributesCacheGenerator_FieldCodec_1_get_ValueReader_m04C98DF25AB7E34AA61DD75D2F990AAEC2591B98(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FieldCodec_1_t713DE327211056F9ECCB1D1CBD9A83826813DBEA_CustomAttributesCacheGenerator_FieldCodec_1_get_ValueMerger_mA0DBB20140AC7E2B4DFF6E2CE1C7078946C2F54E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FieldCodec_1_t713DE327211056F9ECCB1D1CBD9A83826813DBEA_CustomAttributesCacheGenerator_FieldCodec_1_get_FieldMerger_mDECE5C7CFE87D0D2E2668F931C61DA46FB8263F8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FieldCodec_1_t713DE327211056F9ECCB1D1CBD9A83826813DBEA_CustomAttributesCacheGenerator_FieldCodec_1_get_FixedSize_m9CEFCB6C3142F3ED071D19E26E67C4350CB7D8D1(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FieldCodec_1_t713DE327211056F9ECCB1D1CBD9A83826813DBEA_CustomAttributesCacheGenerator_FieldCodec_1_get_Tag_m83C2F3733CD96ADE96EDB69552B6C248CBD3E9CA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FieldCodec_1_t713DE327211056F9ECCB1D1CBD9A83826813DBEA_CustomAttributesCacheGenerator_FieldCodec_1_get_EndTag_m11129D6820FC5559FCDD388BF1230AE401344E38(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FieldCodec_1_t713DE327211056F9ECCB1D1CBD9A83826813DBEA_CustomAttributesCacheGenerator_FieldCodec_1_get_DefaultValue_m3CCF3372ED00CB6EA3E14ED9BF5F9B8726FD5133(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass38_0_tDCDB86507664B32348A36C610F1F3AA0E5A0D730_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass39_0_t6751F2CA2908287AED77B57BEE3DCED6B3A5259C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_t3EDDCCC5BF90960C2C05B8B94DBC9D82D3D17E39_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonFormatter_t29EDD2B5D01B4FD88FD4DBF274554818F8ED11C7_CustomAttributesCacheGenerator_U3CDefaultU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonFormatter_t29EDD2B5D01B4FD88FD4DBF274554818F8ED11C7_CustomAttributesCacheGenerator_JsonFormatter_get_Default_m5A9C0948E8C0D01D6700160BED899AD2507140D3(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Settings_t8850B4BBB68392D1D89673DC99317C64F663FECE_CustomAttributesCacheGenerator_U3CDefaultU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Settings_t8850B4BBB68392D1D89673DC99317C64F663FECE_CustomAttributesCacheGenerator_U3CFormatDefaultValuesU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Settings_t8850B4BBB68392D1D89673DC99317C64F663FECE_CustomAttributesCacheGenerator_U3CTypeRegistryU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Settings_t8850B4BBB68392D1D89673DC99317C64F663FECE_CustomAttributesCacheGenerator_U3CFormatEnumsAsIntegersU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Settings_t8850B4BBB68392D1D89673DC99317C64F663FECE_CustomAttributesCacheGenerator_Settings_get_Default_mF4FDE9D3D751BD702D8219E4EB5517E0F6FF9B74(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Settings_t8850B4BBB68392D1D89673DC99317C64F663FECE_CustomAttributesCacheGenerator_Settings_get_FormatDefaultValues_m64C456D788C62CB3E0F0996AFF989CC6981239F3(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Settings_t8850B4BBB68392D1D89673DC99317C64F663FECE_CustomAttributesCacheGenerator_Settings_get_TypeRegistry_m9BA673D58183796B0F0C9E2DBBE89F432A6D6D0E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Settings_t8850B4BBB68392D1D89673DC99317C64F663FECE_CustomAttributesCacheGenerator_Settings_get_FormatEnumsAsIntegers_m51CB1E83B7EA54E4D894263F570805407D05CFE1(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_t5555F991758A0DD73D48BCE4AF9A1B14B7F05FA3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MessageExtensions_tC8FEC406E671930064BE225D2C6DC69D04927A39_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void MessageExtensions_tC8FEC406E671930064BE225D2C6DC69D04927A39_CustomAttributesCacheGenerator_MessageExtensions_ToByteArray_m1BBBB6D4DC2D766396375663F29F4427A33D8812(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void MessageExtensions_tC8FEC406E671930064BE225D2C6DC69D04927A39_CustomAttributesCacheGenerator_MessageExtensions_MergeFrom_m1473F9DF3353F288CF4A04AA5CE23D014542C5F4(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void MessageExtensions_tC8FEC406E671930064BE225D2C6DC69D04927A39_CustomAttributesCacheGenerator_MessageExtensions_MergeFrom_mF7F80928C53902B587749A72D0569EFF68C10DA6(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void MessageParser_tB0F97A0DCF31AD5D21F69684004C9745C88099FC_CustomAttributesCacheGenerator_U3CDiscardUnknownFieldsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MessageParser_tB0F97A0DCF31AD5D21F69684004C9745C88099FC_CustomAttributesCacheGenerator_U3CExtensionsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MessageParser_tB0F97A0DCF31AD5D21F69684004C9745C88099FC_CustomAttributesCacheGenerator_MessageParser_get_DiscardUnknownFields_m1B7BDCE4283878E28C0B4ACB3944EF71ED872B98(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MessageParser_tB0F97A0DCF31AD5D21F69684004C9745C88099FC_CustomAttributesCacheGenerator_MessageParser_get_Extensions_m13D4911AAC2CFF3977A4EF7A60853D1234526737(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass2_0_t0FCA4DEEC252A0D179F22DF669C875C54631E41E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ParseContext_t288EAC5562A1D7E0FF8A06DBA58B8CF732ECE349_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * tmp = (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 *)cache->attributes[0];
		ObsoleteAttribute__ctor_m058D2B798E9AB7E7778A6DB04C3FE7011C660F4C(tmp, il2cpp_codegen_string_new_wrapper("\x54\x79\x70\x65\x73\x20\x77\x69\x74\x68\x20\x65\x6D\x62\x65\x64\x64\x65\x64\x20\x72\x65\x66\x65\x72\x65\x6E\x63\x65\x73\x20\x61\x72\x65\x20\x6E\x6F\x74\x20\x73\x75\x70\x70\x6F\x72\x74\x65\x64\x20\x69\x6E\x20\x74\x68\x69\x73\x20\x76\x65\x72\x73\x69\x6F\x6E\x20\x6F\x66\x20\x79\x6F\x75\x72\x20\x63\x6F\x6D\x70\x69\x6C\x65\x72\x2E"), true, NULL);
	}
	{
		IsByRefLikeAttribute_t1F586EB86302883DEC1CCE8E0E6801E76D2601CC * tmp = (IsByRefLikeAttribute_t1F586EB86302883DEC1CCE8E0E6801E76D2601CC *)cache->attributes[1];
		IsByRefLikeAttribute__ctor_m80997F85AA38401591B7574259E29881904ADC76(tmp, NULL);
	}
}
static void ParserInternalState_t08A261A9F88B2B769A10BED876C9622FEAFD2596_CustomAttributesCacheGenerator_U3CDiscardUnknownFieldsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ParserInternalState_t08A261A9F88B2B769A10BED876C9622FEAFD2596_CustomAttributesCacheGenerator_U3CExtensionRegistryU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ParserInternalState_t08A261A9F88B2B769A10BED876C9622FEAFD2596_CustomAttributesCacheGenerator_ParserInternalState_get_DiscardUnknownFields_mABFB91062F6B1A730ABD81691F138C516B0E3020(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ParserInternalState_t08A261A9F88B2B769A10BED876C9622FEAFD2596_CustomAttributesCacheGenerator_ParserInternalState_set_DiscardUnknownFields_m6374B0D645A0352B3BF53F9B0EFEF62E45501C58(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ParserInternalState_t08A261A9F88B2B769A10BED876C9622FEAFD2596_CustomAttributesCacheGenerator_ParserInternalState_get_ExtensionRegistry_m8ABCB61553B7732DBB4DB08481D898E5479347CE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ParserInternalState_t08A261A9F88B2B769A10BED876C9622FEAFD2596_CustomAttributesCacheGenerator_ParserInternalState_set_ExtensionRegistry_mEA5D5DD0A75713D26A2300501B940EB7A67C2B5C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WriteContext_t4011B33FF5B289B23B81D9929C82A44F0A24DFCB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * tmp = (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 *)cache->attributes[0];
		ObsoleteAttribute__ctor_m058D2B798E9AB7E7778A6DB04C3FE7011C660F4C(tmp, il2cpp_codegen_string_new_wrapper("\x54\x79\x70\x65\x73\x20\x77\x69\x74\x68\x20\x65\x6D\x62\x65\x64\x64\x65\x64\x20\x72\x65\x66\x65\x72\x65\x6E\x63\x65\x73\x20\x61\x72\x65\x20\x6E\x6F\x74\x20\x73\x75\x70\x70\x6F\x72\x74\x65\x64\x20\x69\x6E\x20\x74\x68\x69\x73\x20\x76\x65\x72\x73\x69\x6F\x6E\x20\x6F\x66\x20\x79\x6F\x75\x72\x20\x63\x6F\x6D\x70\x69\x6C\x65\x72\x2E"), true, NULL);
	}
	{
		IsByRefLikeAttribute_t1F586EB86302883DEC1CCE8E0E6801E76D2601CC * tmp = (IsByRefLikeAttribute_t1F586EB86302883DEC1CCE8E0E6801E76D2601CC *)cache->attributes[1];
		IsByRefLikeAttribute__ctor_m80997F85AA38401591B7574259E29881904ADC76(tmp, NULL);
	}
}
static void Any_t92B39FAFBB91BF3E94AAA6527FC7D6B113D7D811_CustomAttributesCacheGenerator_Any__ctor_m1ECC62A42AB856A583601A266C7C8CF336408E90(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Any_t92B39FAFBB91BF3E94AAA6527FC7D6B113D7D811_CustomAttributesCacheGenerator_Any__ctor_m2376F8C7176FFA131593622A480CE332EB4B1B13(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Any_t92B39FAFBB91BF3E94AAA6527FC7D6B113D7D811_CustomAttributesCacheGenerator_Any_Clone_m0DED49DFACD7ED97904D1D4290A65A1C494FDA6F(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Any_t92B39FAFBB91BF3E94AAA6527FC7D6B113D7D811_CustomAttributesCacheGenerator_Any_Equals_mD55E6718944CD8AA56B9AFC8B9B584BE6E4BEF43(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Any_t92B39FAFBB91BF3E94AAA6527FC7D6B113D7D811_CustomAttributesCacheGenerator_Any_Equals_m39E1D5E36B954E909B5F6C5D904430B5064AB976(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Any_t92B39FAFBB91BF3E94AAA6527FC7D6B113D7D811_CustomAttributesCacheGenerator_Any_GetHashCode_m07B1193DDB01F335CE42B217AE9BD3A88C6848FB(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Any_t92B39FAFBB91BF3E94AAA6527FC7D6B113D7D811_CustomAttributesCacheGenerator_Any_ToString_m0A75195213C8DF63A33CA3775B9684EEED9E9749(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Any_t92B39FAFBB91BF3E94AAA6527FC7D6B113D7D811_CustomAttributesCacheGenerator_Any_WriteTo_m4F0C9449ECFD97252B6CC7090CACD8A532BD2BFB(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Any_t92B39FAFBB91BF3E94AAA6527FC7D6B113D7D811_CustomAttributesCacheGenerator_Any_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m1A5C5A2DE6524DE3A5A86FB9E4AD5422AFA61562(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Any_t92B39FAFBB91BF3E94AAA6527FC7D6B113D7D811_CustomAttributesCacheGenerator_Any_CalculateSize_m0CC466C0EA20B4192A897E77F5AEDD4E22E6B9A4(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Any_t92B39FAFBB91BF3E94AAA6527FC7D6B113D7D811_CustomAttributesCacheGenerator_Any_MergeFrom_mB3B78279933A5AA5BA4B45E7EB58A4F133BF7177(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Any_t92B39FAFBB91BF3E94AAA6527FC7D6B113D7D811_CustomAttributesCacheGenerator_Any_MergeFrom_mBFB2D9E1EC1825E910098EDFEA6547ABB949BECD(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Any_t92B39FAFBB91BF3E94AAA6527FC7D6B113D7D811_CustomAttributesCacheGenerator_Any_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_mF51F0CE65F862C0DCAF37AD560D439D63DB570CB(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Any_t92B39FAFBB91BF3E94AAA6527FC7D6B113D7D811_CustomAttributesCacheGenerator_Any_t92B39FAFBB91BF3E94AAA6527FC7D6B113D7D811____Parser_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Any_t92B39FAFBB91BF3E94AAA6527FC7D6B113D7D811_CustomAttributesCacheGenerator_Any_t92B39FAFBB91BF3E94AAA6527FC7D6B113D7D811____Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Any_t92B39FAFBB91BF3E94AAA6527FC7D6B113D7D811_CustomAttributesCacheGenerator_Any_t92B39FAFBB91BF3E94AAA6527FC7D6B113D7D811____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Any_t92B39FAFBB91BF3E94AAA6527FC7D6B113D7D811_CustomAttributesCacheGenerator_Any_t92B39FAFBB91BF3E94AAA6527FC7D6B113D7D811____TypeUrl_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Any_t92B39FAFBB91BF3E94AAA6527FC7D6B113D7D811_CustomAttributesCacheGenerator_Any_t92B39FAFBB91BF3E94AAA6527FC7D6B113D7D811____Value_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void U3CU3Ec_t90AA77201246807290E9C9419E604313DEBA6726_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Duration_t40CF63035DE7C0F4A03867C1128E4BB2CCF12A80_CustomAttributesCacheGenerator_Duration__ctor_m6E8317977C3ABF38F7C0DF31CF9FD578DB563EEA(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Duration_t40CF63035DE7C0F4A03867C1128E4BB2CCF12A80_CustomAttributesCacheGenerator_Duration__ctor_mE32B96FC0271D3892E7E9B684B384787EF78E56D(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Duration_t40CF63035DE7C0F4A03867C1128E4BB2CCF12A80_CustomAttributesCacheGenerator_Duration_Clone_mF481B40FEAFC57DDFA82F9357B1E7BCB2A5645EE(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Duration_t40CF63035DE7C0F4A03867C1128E4BB2CCF12A80_CustomAttributesCacheGenerator_Duration_Equals_mC8E45E2881848B04EF30A2A9F507A755840D8895(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Duration_t40CF63035DE7C0F4A03867C1128E4BB2CCF12A80_CustomAttributesCacheGenerator_Duration_Equals_m1BE5DD19D22AF8643B4CE200DE7F7F79853D4292(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Duration_t40CF63035DE7C0F4A03867C1128E4BB2CCF12A80_CustomAttributesCacheGenerator_Duration_GetHashCode_m9177E77337F424B4F7B8895403B9E472EF1C98D8(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Duration_t40CF63035DE7C0F4A03867C1128E4BB2CCF12A80_CustomAttributesCacheGenerator_Duration_ToString_m9A4C19C321E83FD1E47AC98C2F8A2513B2580900(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Duration_t40CF63035DE7C0F4A03867C1128E4BB2CCF12A80_CustomAttributesCacheGenerator_Duration_WriteTo_mBB57F86544E6C7F06853460506A3C7534FFDBC5D(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Duration_t40CF63035DE7C0F4A03867C1128E4BB2CCF12A80_CustomAttributesCacheGenerator_Duration_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_mBA1C04DAEA64BD35140B8B97E305116436D657A3(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Duration_t40CF63035DE7C0F4A03867C1128E4BB2CCF12A80_CustomAttributesCacheGenerator_Duration_CalculateSize_m9E15FCF69AB0CE774A486CB3C0B5C9AA6417EABF(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Duration_t40CF63035DE7C0F4A03867C1128E4BB2CCF12A80_CustomAttributesCacheGenerator_Duration_MergeFrom_m5815E64B100ABE517B1AF02E6EEEE2F9937C6EE9(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Duration_t40CF63035DE7C0F4A03867C1128E4BB2CCF12A80_CustomAttributesCacheGenerator_Duration_MergeFrom_m4150356C75EC52495E8CF0B15F5A1832ACBE8F8A(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Duration_t40CF63035DE7C0F4A03867C1128E4BB2CCF12A80_CustomAttributesCacheGenerator_Duration_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m03941E2F76DE634FD45F0AD9FF80E8FDD8FC8B3C(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Duration_t40CF63035DE7C0F4A03867C1128E4BB2CCF12A80_CustomAttributesCacheGenerator_Duration_t40CF63035DE7C0F4A03867C1128E4BB2CCF12A80____Parser_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Duration_t40CF63035DE7C0F4A03867C1128E4BB2CCF12A80_CustomAttributesCacheGenerator_Duration_t40CF63035DE7C0F4A03867C1128E4BB2CCF12A80____Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Duration_t40CF63035DE7C0F4A03867C1128E4BB2CCF12A80_CustomAttributesCacheGenerator_Duration_t40CF63035DE7C0F4A03867C1128E4BB2CCF12A80____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Duration_t40CF63035DE7C0F4A03867C1128E4BB2CCF12A80_CustomAttributesCacheGenerator_Duration_t40CF63035DE7C0F4A03867C1128E4BB2CCF12A80____Seconds_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Duration_t40CF63035DE7C0F4A03867C1128E4BB2CCF12A80_CustomAttributesCacheGenerator_Duration_t40CF63035DE7C0F4A03867C1128E4BB2CCF12A80____Nanos_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void U3CU3Ec_tAFA19B90DD61DE127AEFD62A58A64551F09A27DF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FieldMask_t0F76999F69A7AA8A3E19EC1466CE438ABB2FA273_CustomAttributesCacheGenerator_FieldMask__ctor_m01D7985ADE2286627A933B2F86237E0C8483D3D5(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FieldMask_t0F76999F69A7AA8A3E19EC1466CE438ABB2FA273_CustomAttributesCacheGenerator_FieldMask__ctor_m77703B996B4EAD8BD22AA0AC7E4C9C768137D853(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FieldMask_t0F76999F69A7AA8A3E19EC1466CE438ABB2FA273_CustomAttributesCacheGenerator_FieldMask_Clone_m2EDD32CF5E0AC140742096AEFE1DAE3ED453CCA3(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FieldMask_t0F76999F69A7AA8A3E19EC1466CE438ABB2FA273_CustomAttributesCacheGenerator_FieldMask_Equals_m1B5C027B9B0A1A66D8B177DAAB4EDBD5FD1442C1(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FieldMask_t0F76999F69A7AA8A3E19EC1466CE438ABB2FA273_CustomAttributesCacheGenerator_FieldMask_Equals_mC23F829C421F954D073A6C046B614DBB687E41AB(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FieldMask_t0F76999F69A7AA8A3E19EC1466CE438ABB2FA273_CustomAttributesCacheGenerator_FieldMask_GetHashCode_mA97105104D19E8CF467DE10EAFC05DC28B86F2D2(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FieldMask_t0F76999F69A7AA8A3E19EC1466CE438ABB2FA273_CustomAttributesCacheGenerator_FieldMask_ToString_m89F5E655ABCBA6ACC9D9B86BE367D9919A837036(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FieldMask_t0F76999F69A7AA8A3E19EC1466CE438ABB2FA273_CustomAttributesCacheGenerator_FieldMask_WriteTo_mE9F97A05551D2F6FBCDF475C259D1BEE4B9ECA43(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FieldMask_t0F76999F69A7AA8A3E19EC1466CE438ABB2FA273_CustomAttributesCacheGenerator_FieldMask_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m663519CE13FFC51F40560EE1C564F4C017133C59(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FieldMask_t0F76999F69A7AA8A3E19EC1466CE438ABB2FA273_CustomAttributesCacheGenerator_FieldMask_CalculateSize_m0A39301CC699EEA0A57D8D8E4846C58F734D8EA5(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FieldMask_t0F76999F69A7AA8A3E19EC1466CE438ABB2FA273_CustomAttributesCacheGenerator_FieldMask_MergeFrom_mEAF05299B243344C2E5FFF1D601F5EA18BAB9F00(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FieldMask_t0F76999F69A7AA8A3E19EC1466CE438ABB2FA273_CustomAttributesCacheGenerator_FieldMask_MergeFrom_mBB29E5E3787C88D4F6813175DF0F3D7E5F8E1E0C(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FieldMask_t0F76999F69A7AA8A3E19EC1466CE438ABB2FA273_CustomAttributesCacheGenerator_FieldMask_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_mA91B89E0519001C72B2CF796A4969D2D87CE04DB(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FieldMask_t0F76999F69A7AA8A3E19EC1466CE438ABB2FA273_CustomAttributesCacheGenerator_FieldMask_t0F76999F69A7AA8A3E19EC1466CE438ABB2FA273____Parser_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FieldMask_t0F76999F69A7AA8A3E19EC1466CE438ABB2FA273_CustomAttributesCacheGenerator_FieldMask_t0F76999F69A7AA8A3E19EC1466CE438ABB2FA273____Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FieldMask_t0F76999F69A7AA8A3E19EC1466CE438ABB2FA273_CustomAttributesCacheGenerator_FieldMask_t0F76999F69A7AA8A3E19EC1466CE438ABB2FA273____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FieldMask_t0F76999F69A7AA8A3E19EC1466CE438ABB2FA273_CustomAttributesCacheGenerator_FieldMask_t0F76999F69A7AA8A3E19EC1466CE438ABB2FA273____Paths_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void U3CU3Ec_t24742AB4C274E002E6BA5C149058C79E1CF86C01_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SourceContext_t7616D6280D262E1831459B553153AB5D68F5077B_CustomAttributesCacheGenerator_SourceContext__ctor_m7DC6A0CEA2DB342A90B3111F54EE7FE20035ED7A(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void SourceContext_t7616D6280D262E1831459B553153AB5D68F5077B_CustomAttributesCacheGenerator_SourceContext__ctor_m53B1A691B9A243427E519584FAA277F82940830C(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void SourceContext_t7616D6280D262E1831459B553153AB5D68F5077B_CustomAttributesCacheGenerator_SourceContext_Clone_mECE4CC09F0CFD276692AED613523558C5B35FF13(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void SourceContext_t7616D6280D262E1831459B553153AB5D68F5077B_CustomAttributesCacheGenerator_SourceContext_Equals_m2404E1B68FD460455E54DD5DA372BA7DD10A56A5(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void SourceContext_t7616D6280D262E1831459B553153AB5D68F5077B_CustomAttributesCacheGenerator_SourceContext_Equals_mDA27BD7606F7EA489A2C1AE54476ED5E0E357143(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void SourceContext_t7616D6280D262E1831459B553153AB5D68F5077B_CustomAttributesCacheGenerator_SourceContext_GetHashCode_m3AF81E3DD69DB21BF3B9DB93411901F8F0C4C4D9(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void SourceContext_t7616D6280D262E1831459B553153AB5D68F5077B_CustomAttributesCacheGenerator_SourceContext_ToString_mCC00619AD401EE2827C8F568E6798478F398F6F1(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void SourceContext_t7616D6280D262E1831459B553153AB5D68F5077B_CustomAttributesCacheGenerator_SourceContext_WriteTo_m29132C1DE0B5144E19D8889A192E79AEDDABA8ED(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void SourceContext_t7616D6280D262E1831459B553153AB5D68F5077B_CustomAttributesCacheGenerator_SourceContext_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m2C30589A2E794D1D6F5D64B6DC35C253E7261066(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void SourceContext_t7616D6280D262E1831459B553153AB5D68F5077B_CustomAttributesCacheGenerator_SourceContext_CalculateSize_mC5AD31CF54346C37E07C001C6A2BD8DCEC68F1F6(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void SourceContext_t7616D6280D262E1831459B553153AB5D68F5077B_CustomAttributesCacheGenerator_SourceContext_MergeFrom_m02826B16AA69141109E52E02757E9B1AF009ACD4(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void SourceContext_t7616D6280D262E1831459B553153AB5D68F5077B_CustomAttributesCacheGenerator_SourceContext_MergeFrom_m052D6EAFE9072262116999725700C7FF8FCB45C1(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void SourceContext_t7616D6280D262E1831459B553153AB5D68F5077B_CustomAttributesCacheGenerator_SourceContext_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_mADDDB228A85C7C3C457347DC2D13075031FD4271(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void SourceContext_t7616D6280D262E1831459B553153AB5D68F5077B_CustomAttributesCacheGenerator_SourceContext_t7616D6280D262E1831459B553153AB5D68F5077B____Parser_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void SourceContext_t7616D6280D262E1831459B553153AB5D68F5077B_CustomAttributesCacheGenerator_SourceContext_t7616D6280D262E1831459B553153AB5D68F5077B____Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void SourceContext_t7616D6280D262E1831459B553153AB5D68F5077B_CustomAttributesCacheGenerator_SourceContext_t7616D6280D262E1831459B553153AB5D68F5077B____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void SourceContext_t7616D6280D262E1831459B553153AB5D68F5077B_CustomAttributesCacheGenerator_SourceContext_t7616D6280D262E1831459B553153AB5D68F5077B____FileName_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void U3CU3Ec_t000235E6741E3C2385418F22D877987949E3CA0A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void NullValue_tEC9E53C12DA09163F9D4C9BBA68385FEC355562A_CustomAttributesCacheGenerator_NullValue(CustomAttributesCache* cache)
{
	{
		OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * tmp = (OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 *)cache->attributes[0];
		OriginalNameAttribute__ctor_mB0C6C38C49B9261D2DC423D692E80B8EBC657BF9(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x55\x4C\x4C\x5F\x56\x41\x4C\x55\x45"), NULL);
	}
}
static void Struct_t4E66B704BB3E73B19AFFF2A87EB12ED16C7C4DD3_CustomAttributesCacheGenerator_Struct__ctor_m11D17B249FA390528B0E14F386ADA2577C6A871C(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Struct_t4E66B704BB3E73B19AFFF2A87EB12ED16C7C4DD3_CustomAttributesCacheGenerator_Struct__ctor_m608179184571F7E0C2F5879E37CF80A00B2B8D7A(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Struct_t4E66B704BB3E73B19AFFF2A87EB12ED16C7C4DD3_CustomAttributesCacheGenerator_Struct_Clone_m0D1BB90781C14E60D625538A9A19D696120475BE(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Struct_t4E66B704BB3E73B19AFFF2A87EB12ED16C7C4DD3_CustomAttributesCacheGenerator_Struct_Equals_m549285D67C027225573A1DB67C5301A0D6454BFA(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Struct_t4E66B704BB3E73B19AFFF2A87EB12ED16C7C4DD3_CustomAttributesCacheGenerator_Struct_Equals_m2532A70D51E9BAB2D6D51B68E969EF10BDA2F728(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Struct_t4E66B704BB3E73B19AFFF2A87EB12ED16C7C4DD3_CustomAttributesCacheGenerator_Struct_GetHashCode_mB35A1295AA470E34F0B3283C926A85E1DC10C61E(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Struct_t4E66B704BB3E73B19AFFF2A87EB12ED16C7C4DD3_CustomAttributesCacheGenerator_Struct_ToString_mB7E3408DD1CC7EA7EF64A6C0AAF69344623328B9(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Struct_t4E66B704BB3E73B19AFFF2A87EB12ED16C7C4DD3_CustomAttributesCacheGenerator_Struct_WriteTo_mB8C1FE19A7AD7FA903647D06FF0DCF1970A32D29(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Struct_t4E66B704BB3E73B19AFFF2A87EB12ED16C7C4DD3_CustomAttributesCacheGenerator_Struct_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_mB4878B03C7F2037F3A6A0B017A57F95D785F0B7F(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Struct_t4E66B704BB3E73B19AFFF2A87EB12ED16C7C4DD3_CustomAttributesCacheGenerator_Struct_CalculateSize_m04764057A3243E48A65E84C497924FE3AE6BEDDC(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Struct_t4E66B704BB3E73B19AFFF2A87EB12ED16C7C4DD3_CustomAttributesCacheGenerator_Struct_MergeFrom_m6AA74D28AA5FDA2211BE8FB37B3E8709136F90B1(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Struct_t4E66B704BB3E73B19AFFF2A87EB12ED16C7C4DD3_CustomAttributesCacheGenerator_Struct_MergeFrom_m6DA60767E2F54627C88F498B3F3D2348F0908D17(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Struct_t4E66B704BB3E73B19AFFF2A87EB12ED16C7C4DD3_CustomAttributesCacheGenerator_Struct_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m48DE2FD28260E4F6EB713D4B2309D7ECC5A3DCAF(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Struct_t4E66B704BB3E73B19AFFF2A87EB12ED16C7C4DD3_CustomAttributesCacheGenerator_Struct_t4E66B704BB3E73B19AFFF2A87EB12ED16C7C4DD3____Parser_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Struct_t4E66B704BB3E73B19AFFF2A87EB12ED16C7C4DD3_CustomAttributesCacheGenerator_Struct_t4E66B704BB3E73B19AFFF2A87EB12ED16C7C4DD3____Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Struct_t4E66B704BB3E73B19AFFF2A87EB12ED16C7C4DD3_CustomAttributesCacheGenerator_Struct_t4E66B704BB3E73B19AFFF2A87EB12ED16C7C4DD3____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Struct_t4E66B704BB3E73B19AFFF2A87EB12ED16C7C4DD3_CustomAttributesCacheGenerator_Struct_t4E66B704BB3E73B19AFFF2A87EB12ED16C7C4DD3____Fields_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void U3CU3Ec_t08DCF5EE055E253C34F66A2152F923B976A9AE11_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Value_t4A50D780AAEDFF7855F6732AA0799306161698CB_CustomAttributesCacheGenerator_Value__ctor_mB36B9A9D61FC019A45BC871BA1478BB50D60E78B(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Value_t4A50D780AAEDFF7855F6732AA0799306161698CB_CustomAttributesCacheGenerator_Value__ctor_m44F71F1C4FF2ED09299528B51BDBA4E73788D40E(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Value_t4A50D780AAEDFF7855F6732AA0799306161698CB_CustomAttributesCacheGenerator_Value_Clone_mACDC4288492E8D9B24FE707A400B1445AD78477F(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Value_t4A50D780AAEDFF7855F6732AA0799306161698CB_CustomAttributesCacheGenerator_Value_Equals_mE007E6C81CFC39B3A5BA78873297DB9C2305C650(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Value_t4A50D780AAEDFF7855F6732AA0799306161698CB_CustomAttributesCacheGenerator_Value_Equals_m5F2BB40CD83A5988F958848F4410366EB7EC59B4(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Value_t4A50D780AAEDFF7855F6732AA0799306161698CB_CustomAttributesCacheGenerator_Value_GetHashCode_m7C544251FD75746E81866D5D36924010535124E6(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Value_t4A50D780AAEDFF7855F6732AA0799306161698CB_CustomAttributesCacheGenerator_Value_ToString_m0435FF1515A70A900374F24375010C41124BFFC1(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Value_t4A50D780AAEDFF7855F6732AA0799306161698CB_CustomAttributesCacheGenerator_Value_WriteTo_m707E9B65D4E8B28CA233D2CC91203558071F8147(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Value_t4A50D780AAEDFF7855F6732AA0799306161698CB_CustomAttributesCacheGenerator_Value_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m8F8E38353725C50938560EC4F37B5F22C75BA7FC(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Value_t4A50D780AAEDFF7855F6732AA0799306161698CB_CustomAttributesCacheGenerator_Value_CalculateSize_m0249098B9D7B5CB6074121B23B55E79FFB7FC9BA(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Value_t4A50D780AAEDFF7855F6732AA0799306161698CB_CustomAttributesCacheGenerator_Value_MergeFrom_mFFA75CAC017BBD75F8C0E116E299BDAC427EF1C5(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Value_t4A50D780AAEDFF7855F6732AA0799306161698CB_CustomAttributesCacheGenerator_Value_MergeFrom_mDFF91157418DCFA0541BAC4E77C0D8E24CCDC62B(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Value_t4A50D780AAEDFF7855F6732AA0799306161698CB_CustomAttributesCacheGenerator_Value_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_mE15A6602E4EDEAB62505CC0E7AD868CBD7CDE78C(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Value_t4A50D780AAEDFF7855F6732AA0799306161698CB_CustomAttributesCacheGenerator_Value_t4A50D780AAEDFF7855F6732AA0799306161698CB____Parser_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Value_t4A50D780AAEDFF7855F6732AA0799306161698CB_CustomAttributesCacheGenerator_Value_t4A50D780AAEDFF7855F6732AA0799306161698CB____Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Value_t4A50D780AAEDFF7855F6732AA0799306161698CB_CustomAttributesCacheGenerator_Value_t4A50D780AAEDFF7855F6732AA0799306161698CB____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Value_t4A50D780AAEDFF7855F6732AA0799306161698CB_CustomAttributesCacheGenerator_Value_t4A50D780AAEDFF7855F6732AA0799306161698CB____NullValue_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Value_t4A50D780AAEDFF7855F6732AA0799306161698CB_CustomAttributesCacheGenerator_Value_t4A50D780AAEDFF7855F6732AA0799306161698CB____NumberValue_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Value_t4A50D780AAEDFF7855F6732AA0799306161698CB_CustomAttributesCacheGenerator_Value_t4A50D780AAEDFF7855F6732AA0799306161698CB____StringValue_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Value_t4A50D780AAEDFF7855F6732AA0799306161698CB_CustomAttributesCacheGenerator_Value_t4A50D780AAEDFF7855F6732AA0799306161698CB____BoolValue_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Value_t4A50D780AAEDFF7855F6732AA0799306161698CB_CustomAttributesCacheGenerator_Value_t4A50D780AAEDFF7855F6732AA0799306161698CB____StructValue_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Value_t4A50D780AAEDFF7855F6732AA0799306161698CB_CustomAttributesCacheGenerator_Value_t4A50D780AAEDFF7855F6732AA0799306161698CB____ListValue_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Value_t4A50D780AAEDFF7855F6732AA0799306161698CB_CustomAttributesCacheGenerator_Value_t4A50D780AAEDFF7855F6732AA0799306161698CB____KindCase_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void U3CU3Ec_t98E1E2EA186D09F72BB3216F53457BD033D47266_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ListValue_t11D7F704E07B21FD590F1D7C14E83402D497D40F_CustomAttributesCacheGenerator_ListValue__ctor_mB9FA676E88F0EA554534D3FFCBECA11AC459A22A(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ListValue_t11D7F704E07B21FD590F1D7C14E83402D497D40F_CustomAttributesCacheGenerator_ListValue__ctor_m28B1D38DB2026931E319F255DEF52941857CE536(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ListValue_t11D7F704E07B21FD590F1D7C14E83402D497D40F_CustomAttributesCacheGenerator_ListValue_Clone_mAE7626FCDDF9C2C6BA65B98C47E483B8C84E800A(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ListValue_t11D7F704E07B21FD590F1D7C14E83402D497D40F_CustomAttributesCacheGenerator_ListValue_Equals_mF92FCAB22A5B02906FEFBA93776FDA4996C0B65B(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ListValue_t11D7F704E07B21FD590F1D7C14E83402D497D40F_CustomAttributesCacheGenerator_ListValue_Equals_mAFE9E0D31D3CFEE41502588A065E6B9ED7C0663F(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ListValue_t11D7F704E07B21FD590F1D7C14E83402D497D40F_CustomAttributesCacheGenerator_ListValue_GetHashCode_mBAB9D28DF3E73B510E816CFF5C20774383DDA53B(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ListValue_t11D7F704E07B21FD590F1D7C14E83402D497D40F_CustomAttributesCacheGenerator_ListValue_ToString_mE555FF95A4A538FD306A7DBD94DA019BCC737EDB(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ListValue_t11D7F704E07B21FD590F1D7C14E83402D497D40F_CustomAttributesCacheGenerator_ListValue_WriteTo_m2200555A25434B19BE50039FFD72B23E104A4A11(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ListValue_t11D7F704E07B21FD590F1D7C14E83402D497D40F_CustomAttributesCacheGenerator_ListValue_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_mC52AE62570621C7C4CA377BD233B78BCBC0178FE(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ListValue_t11D7F704E07B21FD590F1D7C14E83402D497D40F_CustomAttributesCacheGenerator_ListValue_CalculateSize_m997191D513733E472C8705995F87C8119B6FDADC(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ListValue_t11D7F704E07B21FD590F1D7C14E83402D497D40F_CustomAttributesCacheGenerator_ListValue_MergeFrom_mFD20BFF52DA1BFB3F54CC90BE5674A118FC5177B(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ListValue_t11D7F704E07B21FD590F1D7C14E83402D497D40F_CustomAttributesCacheGenerator_ListValue_MergeFrom_m384866A7A963FFBF044B0FC082ADD6A9AA6A4CE2(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ListValue_t11D7F704E07B21FD590F1D7C14E83402D497D40F_CustomAttributesCacheGenerator_ListValue_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m5E6C1BE581E812B32831F486A67887D377AE6EED(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ListValue_t11D7F704E07B21FD590F1D7C14E83402D497D40F_CustomAttributesCacheGenerator_ListValue_t11D7F704E07B21FD590F1D7C14E83402D497D40F____Parser_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ListValue_t11D7F704E07B21FD590F1D7C14E83402D497D40F_CustomAttributesCacheGenerator_ListValue_t11D7F704E07B21FD590F1D7C14E83402D497D40F____Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ListValue_t11D7F704E07B21FD590F1D7C14E83402D497D40F_CustomAttributesCacheGenerator_ListValue_t11D7F704E07B21FD590F1D7C14E83402D497D40F____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void U3CU3Ec_t287C3274FF8FFE690CEB6CD883D55CD3988164AC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Timestamp_t9150476DBE9622F93C1BB771873B5E1B5FA45929_CustomAttributesCacheGenerator_Timestamp__ctor_m8F321B7F87FD2782A209D5251E2509121F91E084(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Timestamp_t9150476DBE9622F93C1BB771873B5E1B5FA45929_CustomAttributesCacheGenerator_Timestamp__ctor_m2664E78CE259B3480E632DA374D7295D9511E644(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Timestamp_t9150476DBE9622F93C1BB771873B5E1B5FA45929_CustomAttributesCacheGenerator_Timestamp_Clone_mBF7E47B82DDCD226CD188726A48B380D7340F063(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Timestamp_t9150476DBE9622F93C1BB771873B5E1B5FA45929_CustomAttributesCacheGenerator_Timestamp_Equals_m43A5485ADA83370447C6ED05E83D93E9FD61F075(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Timestamp_t9150476DBE9622F93C1BB771873B5E1B5FA45929_CustomAttributesCacheGenerator_Timestamp_Equals_m09CB0CAAD41410377A04A2A6B6F93E232A2FD558(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Timestamp_t9150476DBE9622F93C1BB771873B5E1B5FA45929_CustomAttributesCacheGenerator_Timestamp_GetHashCode_m4EF62C49B8D0D43BDCDF3AE4115D63442DCB6AF7(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Timestamp_t9150476DBE9622F93C1BB771873B5E1B5FA45929_CustomAttributesCacheGenerator_Timestamp_ToString_m54CE7B61A60BD4FBE9FA386E69B256FD9CAB0766(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Timestamp_t9150476DBE9622F93C1BB771873B5E1B5FA45929_CustomAttributesCacheGenerator_Timestamp_WriteTo_m7A41B3EBAEDF1DB8EBA83393E27C1D47642EF1E6(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Timestamp_t9150476DBE9622F93C1BB771873B5E1B5FA45929_CustomAttributesCacheGenerator_Timestamp_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m931F0E47EBF9936D807F0165935BE4D7B4DD6D33(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Timestamp_t9150476DBE9622F93C1BB771873B5E1B5FA45929_CustomAttributesCacheGenerator_Timestamp_CalculateSize_mB8AA31386FAA6594A82EBFDA83023F01849EBA42(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Timestamp_t9150476DBE9622F93C1BB771873B5E1B5FA45929_CustomAttributesCacheGenerator_Timestamp_MergeFrom_mB073280D4CC7A8E531F5BE75B1390589537790A4(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Timestamp_t9150476DBE9622F93C1BB771873B5E1B5FA45929_CustomAttributesCacheGenerator_Timestamp_MergeFrom_m47E1E135BC450678854DD2704A26607F69516BBF(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Timestamp_t9150476DBE9622F93C1BB771873B5E1B5FA45929_CustomAttributesCacheGenerator_Timestamp_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m667DF5C1AC94559C80FE08DC714B1E2943D6C16D(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Timestamp_t9150476DBE9622F93C1BB771873B5E1B5FA45929_CustomAttributesCacheGenerator_Timestamp_t9150476DBE9622F93C1BB771873B5E1B5FA45929____Parser_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Timestamp_t9150476DBE9622F93C1BB771873B5E1B5FA45929_CustomAttributesCacheGenerator_Timestamp_t9150476DBE9622F93C1BB771873B5E1B5FA45929____Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Timestamp_t9150476DBE9622F93C1BB771873B5E1B5FA45929_CustomAttributesCacheGenerator_Timestamp_t9150476DBE9622F93C1BB771873B5E1B5FA45929____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Timestamp_t9150476DBE9622F93C1BB771873B5E1B5FA45929_CustomAttributesCacheGenerator_Timestamp_t9150476DBE9622F93C1BB771873B5E1B5FA45929____Seconds_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Timestamp_t9150476DBE9622F93C1BB771873B5E1B5FA45929_CustomAttributesCacheGenerator_Timestamp_t9150476DBE9622F93C1BB771873B5E1B5FA45929____Nanos_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void U3CU3Ec_t50B4F58275D7F3C45E025DA55B9650988039B05D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Syntax_t8B3070D9FA595706155795F37EED0D87D0D308F8_CustomAttributesCacheGenerator_Proto2(CustomAttributesCache* cache)
{
	{
		OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * tmp = (OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 *)cache->attributes[0];
		OriginalNameAttribute__ctor_mB0C6C38C49B9261D2DC423D692E80B8EBC657BF9(tmp, il2cpp_codegen_string_new_wrapper("\x53\x59\x4E\x54\x41\x58\x5F\x50\x52\x4F\x54\x4F\x32"), NULL);
	}
}
static void Syntax_t8B3070D9FA595706155795F37EED0D87D0D308F8_CustomAttributesCacheGenerator_Proto3(CustomAttributesCache* cache)
{
	{
		OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * tmp = (OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 *)cache->attributes[0];
		OriginalNameAttribute__ctor_mB0C6C38C49B9261D2DC423D692E80B8EBC657BF9(tmp, il2cpp_codegen_string_new_wrapper("\x53\x59\x4E\x54\x41\x58\x5F\x50\x52\x4F\x54\x4F\x33"), NULL);
	}
}
static void Type_t1A1580695FE92D3355A256FC0A40309494A8C356_CustomAttributesCacheGenerator_Type__ctor_m72B7DAE9051977572D45AFCF5B543AEB9E84A71F(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Type_t1A1580695FE92D3355A256FC0A40309494A8C356_CustomAttributesCacheGenerator_Type__ctor_m7E6669C7E329257A31BA69B14587D6D7739C49DE(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Type_t1A1580695FE92D3355A256FC0A40309494A8C356_CustomAttributesCacheGenerator_Type_Clone_m45297B0AFB39C2C32EDE75B17513877EAE97D1CC(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Type_t1A1580695FE92D3355A256FC0A40309494A8C356_CustomAttributesCacheGenerator_Type_Equals_mCC222586E6D1B96613944E3D66BEC72D362D9D63(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Type_t1A1580695FE92D3355A256FC0A40309494A8C356_CustomAttributesCacheGenerator_Type_Equals_mE4A16EB9E9DA3DD6BBCFD983D5E76A3B7EF7699B(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Type_t1A1580695FE92D3355A256FC0A40309494A8C356_CustomAttributesCacheGenerator_Type_GetHashCode_m620763684AFC519635D83A5038D9874C8D840B43(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Type_t1A1580695FE92D3355A256FC0A40309494A8C356_CustomAttributesCacheGenerator_Type_ToString_m220D186C08F0D2510BEBAD9ADA9B2AFC9D9C7A2B(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Type_t1A1580695FE92D3355A256FC0A40309494A8C356_CustomAttributesCacheGenerator_Type_WriteTo_mEAC9994F15ADD598116489C2F5DC26A213181E74(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Type_t1A1580695FE92D3355A256FC0A40309494A8C356_CustomAttributesCacheGenerator_Type_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_mF4AB433048B17B8767AE79040EDF944BF398D8C4(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Type_t1A1580695FE92D3355A256FC0A40309494A8C356_CustomAttributesCacheGenerator_Type_CalculateSize_m5AE90542655AB79BC6184ACB7A226E0D004371AA(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Type_t1A1580695FE92D3355A256FC0A40309494A8C356_CustomAttributesCacheGenerator_Type_MergeFrom_m3128264DCA457EBEF76A421739ECF34A8244D3F3(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Type_t1A1580695FE92D3355A256FC0A40309494A8C356_CustomAttributesCacheGenerator_Type_MergeFrom_m71E0740B14181FDFFDA3FE9A92DE72D39471830D(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Type_t1A1580695FE92D3355A256FC0A40309494A8C356_CustomAttributesCacheGenerator_Type_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_mEC3240A4B5DB410713B7697F2A15EA65B3C67EDB(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Type_t1A1580695FE92D3355A256FC0A40309494A8C356_CustomAttributesCacheGenerator_Type_t1A1580695FE92D3355A256FC0A40309494A8C356____Parser_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Type_t1A1580695FE92D3355A256FC0A40309494A8C356_CustomAttributesCacheGenerator_Type_t1A1580695FE92D3355A256FC0A40309494A8C356____Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Type_t1A1580695FE92D3355A256FC0A40309494A8C356_CustomAttributesCacheGenerator_Type_t1A1580695FE92D3355A256FC0A40309494A8C356____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Type_t1A1580695FE92D3355A256FC0A40309494A8C356_CustomAttributesCacheGenerator_Type_t1A1580695FE92D3355A256FC0A40309494A8C356____Name_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Type_t1A1580695FE92D3355A256FC0A40309494A8C356_CustomAttributesCacheGenerator_Type_t1A1580695FE92D3355A256FC0A40309494A8C356____SourceContext_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Type_t1A1580695FE92D3355A256FC0A40309494A8C356_CustomAttributesCacheGenerator_Type_t1A1580695FE92D3355A256FC0A40309494A8C356____Syntax_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void U3CU3Ec_tCFA8B3696619E2E5EF95E2EB6DA10ADCCB9194D5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Field_t79E55EF79874715920B1FA76F36897A5D907AD7C_CustomAttributesCacheGenerator_Field__ctor_m083271BB3DE4449286983AFE69B71C9DE2D68BE8(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Field_t79E55EF79874715920B1FA76F36897A5D907AD7C_CustomAttributesCacheGenerator_Field__ctor_mF445407AC817770B1DD11ED1FC30695C18F0B76B(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Field_t79E55EF79874715920B1FA76F36897A5D907AD7C_CustomAttributesCacheGenerator_Field_Clone_m0CC33BAC52277DA85D019E632E4BEF45987DF7D7(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Field_t79E55EF79874715920B1FA76F36897A5D907AD7C_CustomAttributesCacheGenerator_Field_Equals_m4954B931DEB49176652C38AC8B8047EB9CF2D083(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Field_t79E55EF79874715920B1FA76F36897A5D907AD7C_CustomAttributesCacheGenerator_Field_Equals_m98EF1C08EB6139CFAC12DDAF6587098D0965F8B8(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Field_t79E55EF79874715920B1FA76F36897A5D907AD7C_CustomAttributesCacheGenerator_Field_GetHashCode_m9920F826AB54CAC69212B120FA5196570B2EBF0F(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Field_t79E55EF79874715920B1FA76F36897A5D907AD7C_CustomAttributesCacheGenerator_Field_ToString_mC417A2D84A15530EB338A880F26CAF5B899096D1(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Field_t79E55EF79874715920B1FA76F36897A5D907AD7C_CustomAttributesCacheGenerator_Field_WriteTo_mE97048ABCAD56F48771C6005B929D1D16822F5F9(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Field_t79E55EF79874715920B1FA76F36897A5D907AD7C_CustomAttributesCacheGenerator_Field_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m1403C5B7A9323AAEEE2ECD2E2282CDADD1D3EBBA(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Field_t79E55EF79874715920B1FA76F36897A5D907AD7C_CustomAttributesCacheGenerator_Field_CalculateSize_m29958B0C393E23281EEBB33DE7F86329A78120EA(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Field_t79E55EF79874715920B1FA76F36897A5D907AD7C_CustomAttributesCacheGenerator_Field_MergeFrom_m79C0F4297EB25A155F74B1D4FD88149786994F5A(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Field_t79E55EF79874715920B1FA76F36897A5D907AD7C_CustomAttributesCacheGenerator_Field_MergeFrom_m9C1775ED4AE6363E980B965D39DE0651A2A30AA8(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Field_t79E55EF79874715920B1FA76F36897A5D907AD7C_CustomAttributesCacheGenerator_Field_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_mDCEE2693E502312C726C28AB4F137EAC8AB12640(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Field_t79E55EF79874715920B1FA76F36897A5D907AD7C_CustomAttributesCacheGenerator_Field_t79E55EF79874715920B1FA76F36897A5D907AD7C____Parser_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Field_t79E55EF79874715920B1FA76F36897A5D907AD7C_CustomAttributesCacheGenerator_Field_t79E55EF79874715920B1FA76F36897A5D907AD7C____Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Field_t79E55EF79874715920B1FA76F36897A5D907AD7C_CustomAttributesCacheGenerator_Field_t79E55EF79874715920B1FA76F36897A5D907AD7C____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Field_t79E55EF79874715920B1FA76F36897A5D907AD7C_CustomAttributesCacheGenerator_Field_t79E55EF79874715920B1FA76F36897A5D907AD7C____Kind_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Field_t79E55EF79874715920B1FA76F36897A5D907AD7C_CustomAttributesCacheGenerator_Field_t79E55EF79874715920B1FA76F36897A5D907AD7C____Cardinality_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Field_t79E55EF79874715920B1FA76F36897A5D907AD7C_CustomAttributesCacheGenerator_Field_t79E55EF79874715920B1FA76F36897A5D907AD7C____Number_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Field_t79E55EF79874715920B1FA76F36897A5D907AD7C_CustomAttributesCacheGenerator_Field_t79E55EF79874715920B1FA76F36897A5D907AD7C____Name_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Field_t79E55EF79874715920B1FA76F36897A5D907AD7C_CustomAttributesCacheGenerator_Field_t79E55EF79874715920B1FA76F36897A5D907AD7C____TypeUrl_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Field_t79E55EF79874715920B1FA76F36897A5D907AD7C_CustomAttributesCacheGenerator_Field_t79E55EF79874715920B1FA76F36897A5D907AD7C____OneofIndex_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Field_t79E55EF79874715920B1FA76F36897A5D907AD7C_CustomAttributesCacheGenerator_Field_t79E55EF79874715920B1FA76F36897A5D907AD7C____Packed_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Field_t79E55EF79874715920B1FA76F36897A5D907AD7C_CustomAttributesCacheGenerator_Field_t79E55EF79874715920B1FA76F36897A5D907AD7C____JsonName_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Field_t79E55EF79874715920B1FA76F36897A5D907AD7C_CustomAttributesCacheGenerator_Field_t79E55EF79874715920B1FA76F36897A5D907AD7C____DefaultValue_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Types_tB8E14C4E98A889999072BB6C790763804958A51D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Kind_tC27F5FC567168FED3D17ACA8C7E54C5769A4D532_CustomAttributesCacheGenerator_TypeUnknown(CustomAttributesCache* cache)
{
	{
		OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * tmp = (OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 *)cache->attributes[0];
		OriginalNameAttribute__ctor_mB0C6C38C49B9261D2DC423D692E80B8EBC657BF9(tmp, il2cpp_codegen_string_new_wrapper("\x54\x59\x50\x45\x5F\x55\x4E\x4B\x4E\x4F\x57\x4E"), NULL);
	}
}
static void Kind_tC27F5FC567168FED3D17ACA8C7E54C5769A4D532_CustomAttributesCacheGenerator_TypeDouble(CustomAttributesCache* cache)
{
	{
		OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * tmp = (OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 *)cache->attributes[0];
		OriginalNameAttribute__ctor_mB0C6C38C49B9261D2DC423D692E80B8EBC657BF9(tmp, il2cpp_codegen_string_new_wrapper("\x54\x59\x50\x45\x5F\x44\x4F\x55\x42\x4C\x45"), NULL);
	}
}
static void Kind_tC27F5FC567168FED3D17ACA8C7E54C5769A4D532_CustomAttributesCacheGenerator_TypeFloat(CustomAttributesCache* cache)
{
	{
		OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * tmp = (OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 *)cache->attributes[0];
		OriginalNameAttribute__ctor_mB0C6C38C49B9261D2DC423D692E80B8EBC657BF9(tmp, il2cpp_codegen_string_new_wrapper("\x54\x59\x50\x45\x5F\x46\x4C\x4F\x41\x54"), NULL);
	}
}
static void Kind_tC27F5FC567168FED3D17ACA8C7E54C5769A4D532_CustomAttributesCacheGenerator_TypeInt64(CustomAttributesCache* cache)
{
	{
		OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * tmp = (OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 *)cache->attributes[0];
		OriginalNameAttribute__ctor_mB0C6C38C49B9261D2DC423D692E80B8EBC657BF9(tmp, il2cpp_codegen_string_new_wrapper("\x54\x59\x50\x45\x5F\x49\x4E\x54\x36\x34"), NULL);
	}
}
static void Kind_tC27F5FC567168FED3D17ACA8C7E54C5769A4D532_CustomAttributesCacheGenerator_TypeUint64(CustomAttributesCache* cache)
{
	{
		OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * tmp = (OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 *)cache->attributes[0];
		OriginalNameAttribute__ctor_mB0C6C38C49B9261D2DC423D692E80B8EBC657BF9(tmp, il2cpp_codegen_string_new_wrapper("\x54\x59\x50\x45\x5F\x55\x49\x4E\x54\x36\x34"), NULL);
	}
}
static void Kind_tC27F5FC567168FED3D17ACA8C7E54C5769A4D532_CustomAttributesCacheGenerator_TypeInt32(CustomAttributesCache* cache)
{
	{
		OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * tmp = (OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 *)cache->attributes[0];
		OriginalNameAttribute__ctor_mB0C6C38C49B9261D2DC423D692E80B8EBC657BF9(tmp, il2cpp_codegen_string_new_wrapper("\x54\x59\x50\x45\x5F\x49\x4E\x54\x33\x32"), NULL);
	}
}
static void Kind_tC27F5FC567168FED3D17ACA8C7E54C5769A4D532_CustomAttributesCacheGenerator_TypeFixed64(CustomAttributesCache* cache)
{
	{
		OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * tmp = (OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 *)cache->attributes[0];
		OriginalNameAttribute__ctor_mB0C6C38C49B9261D2DC423D692E80B8EBC657BF9(tmp, il2cpp_codegen_string_new_wrapper("\x54\x59\x50\x45\x5F\x46\x49\x58\x45\x44\x36\x34"), NULL);
	}
}
static void Kind_tC27F5FC567168FED3D17ACA8C7E54C5769A4D532_CustomAttributesCacheGenerator_TypeFixed32(CustomAttributesCache* cache)
{
	{
		OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * tmp = (OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 *)cache->attributes[0];
		OriginalNameAttribute__ctor_mB0C6C38C49B9261D2DC423D692E80B8EBC657BF9(tmp, il2cpp_codegen_string_new_wrapper("\x54\x59\x50\x45\x5F\x46\x49\x58\x45\x44\x33\x32"), NULL);
	}
}
static void Kind_tC27F5FC567168FED3D17ACA8C7E54C5769A4D532_CustomAttributesCacheGenerator_TypeBool(CustomAttributesCache* cache)
{
	{
		OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * tmp = (OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 *)cache->attributes[0];
		OriginalNameAttribute__ctor_mB0C6C38C49B9261D2DC423D692E80B8EBC657BF9(tmp, il2cpp_codegen_string_new_wrapper("\x54\x59\x50\x45\x5F\x42\x4F\x4F\x4C"), NULL);
	}
}
static void Kind_tC27F5FC567168FED3D17ACA8C7E54C5769A4D532_CustomAttributesCacheGenerator_TypeString(CustomAttributesCache* cache)
{
	{
		OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * tmp = (OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 *)cache->attributes[0];
		OriginalNameAttribute__ctor_mB0C6C38C49B9261D2DC423D692E80B8EBC657BF9(tmp, il2cpp_codegen_string_new_wrapper("\x54\x59\x50\x45\x5F\x53\x54\x52\x49\x4E\x47"), NULL);
	}
}
static void Kind_tC27F5FC567168FED3D17ACA8C7E54C5769A4D532_CustomAttributesCacheGenerator_TypeGroup(CustomAttributesCache* cache)
{
	{
		OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * tmp = (OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 *)cache->attributes[0];
		OriginalNameAttribute__ctor_mB0C6C38C49B9261D2DC423D692E80B8EBC657BF9(tmp, il2cpp_codegen_string_new_wrapper("\x54\x59\x50\x45\x5F\x47\x52\x4F\x55\x50"), NULL);
	}
}
static void Kind_tC27F5FC567168FED3D17ACA8C7E54C5769A4D532_CustomAttributesCacheGenerator_TypeMessage(CustomAttributesCache* cache)
{
	{
		OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * tmp = (OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 *)cache->attributes[0];
		OriginalNameAttribute__ctor_mB0C6C38C49B9261D2DC423D692E80B8EBC657BF9(tmp, il2cpp_codegen_string_new_wrapper("\x54\x59\x50\x45\x5F\x4D\x45\x53\x53\x41\x47\x45"), NULL);
	}
}
static void Kind_tC27F5FC567168FED3D17ACA8C7E54C5769A4D532_CustomAttributesCacheGenerator_TypeBytes(CustomAttributesCache* cache)
{
	{
		OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * tmp = (OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 *)cache->attributes[0];
		OriginalNameAttribute__ctor_mB0C6C38C49B9261D2DC423D692E80B8EBC657BF9(tmp, il2cpp_codegen_string_new_wrapper("\x54\x59\x50\x45\x5F\x42\x59\x54\x45\x53"), NULL);
	}
}
static void Kind_tC27F5FC567168FED3D17ACA8C7E54C5769A4D532_CustomAttributesCacheGenerator_TypeUint32(CustomAttributesCache* cache)
{
	{
		OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * tmp = (OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 *)cache->attributes[0];
		OriginalNameAttribute__ctor_mB0C6C38C49B9261D2DC423D692E80B8EBC657BF9(tmp, il2cpp_codegen_string_new_wrapper("\x54\x59\x50\x45\x5F\x55\x49\x4E\x54\x33\x32"), NULL);
	}
}
static void Kind_tC27F5FC567168FED3D17ACA8C7E54C5769A4D532_CustomAttributesCacheGenerator_TypeEnum(CustomAttributesCache* cache)
{
	{
		OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * tmp = (OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 *)cache->attributes[0];
		OriginalNameAttribute__ctor_mB0C6C38C49B9261D2DC423D692E80B8EBC657BF9(tmp, il2cpp_codegen_string_new_wrapper("\x54\x59\x50\x45\x5F\x45\x4E\x55\x4D"), NULL);
	}
}
static void Kind_tC27F5FC567168FED3D17ACA8C7E54C5769A4D532_CustomAttributesCacheGenerator_TypeSfixed32(CustomAttributesCache* cache)
{
	{
		OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * tmp = (OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 *)cache->attributes[0];
		OriginalNameAttribute__ctor_mB0C6C38C49B9261D2DC423D692E80B8EBC657BF9(tmp, il2cpp_codegen_string_new_wrapper("\x54\x59\x50\x45\x5F\x53\x46\x49\x58\x45\x44\x33\x32"), NULL);
	}
}
static void Kind_tC27F5FC567168FED3D17ACA8C7E54C5769A4D532_CustomAttributesCacheGenerator_TypeSfixed64(CustomAttributesCache* cache)
{
	{
		OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * tmp = (OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 *)cache->attributes[0];
		OriginalNameAttribute__ctor_mB0C6C38C49B9261D2DC423D692E80B8EBC657BF9(tmp, il2cpp_codegen_string_new_wrapper("\x54\x59\x50\x45\x5F\x53\x46\x49\x58\x45\x44\x36\x34"), NULL);
	}
}
static void Kind_tC27F5FC567168FED3D17ACA8C7E54C5769A4D532_CustomAttributesCacheGenerator_TypeSint32(CustomAttributesCache* cache)
{
	{
		OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * tmp = (OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 *)cache->attributes[0];
		OriginalNameAttribute__ctor_mB0C6C38C49B9261D2DC423D692E80B8EBC657BF9(tmp, il2cpp_codegen_string_new_wrapper("\x54\x59\x50\x45\x5F\x53\x49\x4E\x54\x33\x32"), NULL);
	}
}
static void Kind_tC27F5FC567168FED3D17ACA8C7E54C5769A4D532_CustomAttributesCacheGenerator_TypeSint64(CustomAttributesCache* cache)
{
	{
		OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * tmp = (OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 *)cache->attributes[0];
		OriginalNameAttribute__ctor_mB0C6C38C49B9261D2DC423D692E80B8EBC657BF9(tmp, il2cpp_codegen_string_new_wrapper("\x54\x59\x50\x45\x5F\x53\x49\x4E\x54\x36\x34"), NULL);
	}
}
static void Cardinality_tFEB23F7BB6AE0122DE5E13D7756F940FD86759F0_CustomAttributesCacheGenerator_Unknown(CustomAttributesCache* cache)
{
	{
		OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * tmp = (OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 *)cache->attributes[0];
		OriginalNameAttribute__ctor_mB0C6C38C49B9261D2DC423D692E80B8EBC657BF9(tmp, il2cpp_codegen_string_new_wrapper("\x43\x41\x52\x44\x49\x4E\x41\x4C\x49\x54\x59\x5F\x55\x4E\x4B\x4E\x4F\x57\x4E"), NULL);
	}
}
static void Cardinality_tFEB23F7BB6AE0122DE5E13D7756F940FD86759F0_CustomAttributesCacheGenerator_Optional(CustomAttributesCache* cache)
{
	{
		OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * tmp = (OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 *)cache->attributes[0];
		OriginalNameAttribute__ctor_mB0C6C38C49B9261D2DC423D692E80B8EBC657BF9(tmp, il2cpp_codegen_string_new_wrapper("\x43\x41\x52\x44\x49\x4E\x41\x4C\x49\x54\x59\x5F\x4F\x50\x54\x49\x4F\x4E\x41\x4C"), NULL);
	}
}
static void Cardinality_tFEB23F7BB6AE0122DE5E13D7756F940FD86759F0_CustomAttributesCacheGenerator_Required(CustomAttributesCache* cache)
{
	{
		OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * tmp = (OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 *)cache->attributes[0];
		OriginalNameAttribute__ctor_mB0C6C38C49B9261D2DC423D692E80B8EBC657BF9(tmp, il2cpp_codegen_string_new_wrapper("\x43\x41\x52\x44\x49\x4E\x41\x4C\x49\x54\x59\x5F\x52\x45\x51\x55\x49\x52\x45\x44"), NULL);
	}
}
static void Cardinality_tFEB23F7BB6AE0122DE5E13D7756F940FD86759F0_CustomAttributesCacheGenerator_Repeated(CustomAttributesCache* cache)
{
	{
		OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * tmp = (OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 *)cache->attributes[0];
		OriginalNameAttribute__ctor_mB0C6C38C49B9261D2DC423D692E80B8EBC657BF9(tmp, il2cpp_codegen_string_new_wrapper("\x43\x41\x52\x44\x49\x4E\x41\x4C\x49\x54\x59\x5F\x52\x45\x50\x45\x41\x54\x45\x44"), NULL);
	}
}
static void U3CU3Ec_t00D69A0C542928EFCE9A40A967F51813B6AC2002_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Enum_tF9B938E22D1A8C7105A3A0A97AF51439675CEF87_CustomAttributesCacheGenerator_Enum__ctor_m6E1C81B05018715B930662010FE77501F5394204(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Enum_tF9B938E22D1A8C7105A3A0A97AF51439675CEF87_CustomAttributesCacheGenerator_Enum__ctor_m48DDA58AECBCB975A007B170E29CAB580FE5880F(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Enum_tF9B938E22D1A8C7105A3A0A97AF51439675CEF87_CustomAttributesCacheGenerator_Enum_Clone_mC8C340BFE917B4374A1D33C35CAC2FBFFB43D904(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Enum_tF9B938E22D1A8C7105A3A0A97AF51439675CEF87_CustomAttributesCacheGenerator_Enum_Equals_mC1F65CAC46C0ADE5A1D38C9FA48A3DED3D4DCE1C(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Enum_tF9B938E22D1A8C7105A3A0A97AF51439675CEF87_CustomAttributesCacheGenerator_Enum_Equals_mFC4CEF2A5597BC4AABEC5B7CC79F18274CC8D7B6(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Enum_tF9B938E22D1A8C7105A3A0A97AF51439675CEF87_CustomAttributesCacheGenerator_Enum_GetHashCode_m84A65EAEBAA7FDBE3579FB7E085A667AB6069380(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Enum_tF9B938E22D1A8C7105A3A0A97AF51439675CEF87_CustomAttributesCacheGenerator_Enum_ToString_m8755890EAF8E3CBD38454AAEE50C219D06BC2AC1(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Enum_tF9B938E22D1A8C7105A3A0A97AF51439675CEF87_CustomAttributesCacheGenerator_Enum_WriteTo_m21A724F3E7465A22E8D2933F97B7AAEA97DDAAD1(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Enum_tF9B938E22D1A8C7105A3A0A97AF51439675CEF87_CustomAttributesCacheGenerator_Enum_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m9E010AB642C27A55002C1798086F1126F5C28E91(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Enum_tF9B938E22D1A8C7105A3A0A97AF51439675CEF87_CustomAttributesCacheGenerator_Enum_CalculateSize_m6AE4EEC93B797E6CF87A9C0F14F2DCE78D805A4A(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Enum_tF9B938E22D1A8C7105A3A0A97AF51439675CEF87_CustomAttributesCacheGenerator_Enum_MergeFrom_m02C3F847695806FAA2E6041572102F44B894C9D6(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Enum_tF9B938E22D1A8C7105A3A0A97AF51439675CEF87_CustomAttributesCacheGenerator_Enum_MergeFrom_mAC108FDF195D5796D71C464C308BEFE7F9BF0B51(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Enum_tF9B938E22D1A8C7105A3A0A97AF51439675CEF87_CustomAttributesCacheGenerator_Enum_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_mDFD4A7F6E813A69B629AFE079EFC31645ABC2881(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Enum_tF9B938E22D1A8C7105A3A0A97AF51439675CEF87_CustomAttributesCacheGenerator_Enum_tF9B938E22D1A8C7105A3A0A97AF51439675CEF87____Parser_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Enum_tF9B938E22D1A8C7105A3A0A97AF51439675CEF87_CustomAttributesCacheGenerator_Enum_tF9B938E22D1A8C7105A3A0A97AF51439675CEF87____Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Enum_tF9B938E22D1A8C7105A3A0A97AF51439675CEF87_CustomAttributesCacheGenerator_Enum_tF9B938E22D1A8C7105A3A0A97AF51439675CEF87____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Enum_tF9B938E22D1A8C7105A3A0A97AF51439675CEF87_CustomAttributesCacheGenerator_Enum_tF9B938E22D1A8C7105A3A0A97AF51439675CEF87____Name_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Enum_tF9B938E22D1A8C7105A3A0A97AF51439675CEF87_CustomAttributesCacheGenerator_Enum_tF9B938E22D1A8C7105A3A0A97AF51439675CEF87____SourceContext_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Enum_tF9B938E22D1A8C7105A3A0A97AF51439675CEF87_CustomAttributesCacheGenerator_Enum_tF9B938E22D1A8C7105A3A0A97AF51439675CEF87____Syntax_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void U3CU3Ec_tECD5412AF4EC7A7A7A8189CC9BA414A243738353_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EnumValue_t24721FCB233B8D4D64FB5DC62D67C63AB4C3B5AE_CustomAttributesCacheGenerator_EnumValue__ctor_m1F9880D30AB86CC9184A09C9244767AD02DBCDAB(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumValue_t24721FCB233B8D4D64FB5DC62D67C63AB4C3B5AE_CustomAttributesCacheGenerator_EnumValue__ctor_mBEB46ABD1DA26AC98C6D2B3CC17B43F3FACE79CD(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumValue_t24721FCB233B8D4D64FB5DC62D67C63AB4C3B5AE_CustomAttributesCacheGenerator_EnumValue_Clone_mAD4F136B74E13F29C57FD9BA516D5690D0D8A2E5(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumValue_t24721FCB233B8D4D64FB5DC62D67C63AB4C3B5AE_CustomAttributesCacheGenerator_EnumValue_Equals_mC7EAF672AF155B8A6E5E83A30AFA5C8C737549CD(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumValue_t24721FCB233B8D4D64FB5DC62D67C63AB4C3B5AE_CustomAttributesCacheGenerator_EnumValue_Equals_m300A77FE3BEA40D06F264421A4DF508A35FFFE42(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumValue_t24721FCB233B8D4D64FB5DC62D67C63AB4C3B5AE_CustomAttributesCacheGenerator_EnumValue_GetHashCode_m0A4617C5682F4BFD756D4E0027B99FB3C9916D0A(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumValue_t24721FCB233B8D4D64FB5DC62D67C63AB4C3B5AE_CustomAttributesCacheGenerator_EnumValue_ToString_mE37319FEB422F63DAE1B920B3F47712CD57858D6(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumValue_t24721FCB233B8D4D64FB5DC62D67C63AB4C3B5AE_CustomAttributesCacheGenerator_EnumValue_WriteTo_mAF6C20F5FC69A6D7FB9990941A81887DFC056FBA(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumValue_t24721FCB233B8D4D64FB5DC62D67C63AB4C3B5AE_CustomAttributesCacheGenerator_EnumValue_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m2D246ECE495DC0034AACF00A7F7D7C749B605088(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumValue_t24721FCB233B8D4D64FB5DC62D67C63AB4C3B5AE_CustomAttributesCacheGenerator_EnumValue_CalculateSize_m4ED2A3BCDB5B45F73CEB8F384D1ABEEDA417A7CF(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumValue_t24721FCB233B8D4D64FB5DC62D67C63AB4C3B5AE_CustomAttributesCacheGenerator_EnumValue_MergeFrom_m16FE96C76760F5E983CDC9DA7E9E6608E718AB2B(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumValue_t24721FCB233B8D4D64FB5DC62D67C63AB4C3B5AE_CustomAttributesCacheGenerator_EnumValue_MergeFrom_mE6B7795A557E42BECB42E02D36B871A5AF3F6F2A(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumValue_t24721FCB233B8D4D64FB5DC62D67C63AB4C3B5AE_CustomAttributesCacheGenerator_EnumValue_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m00F8E5F6718E02F5C8D361D25E5941380E4414C3(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumValue_t24721FCB233B8D4D64FB5DC62D67C63AB4C3B5AE_CustomAttributesCacheGenerator_EnumValue_t24721FCB233B8D4D64FB5DC62D67C63AB4C3B5AE____Parser_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumValue_t24721FCB233B8D4D64FB5DC62D67C63AB4C3B5AE_CustomAttributesCacheGenerator_EnumValue_t24721FCB233B8D4D64FB5DC62D67C63AB4C3B5AE____Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumValue_t24721FCB233B8D4D64FB5DC62D67C63AB4C3B5AE_CustomAttributesCacheGenerator_EnumValue_t24721FCB233B8D4D64FB5DC62D67C63AB4C3B5AE____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumValue_t24721FCB233B8D4D64FB5DC62D67C63AB4C3B5AE_CustomAttributesCacheGenerator_EnumValue_t24721FCB233B8D4D64FB5DC62D67C63AB4C3B5AE____Name_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumValue_t24721FCB233B8D4D64FB5DC62D67C63AB4C3B5AE_CustomAttributesCacheGenerator_EnumValue_t24721FCB233B8D4D64FB5DC62D67C63AB4C3B5AE____Number_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void U3CU3Ec_t54E1462FB3B1DFA4137E99950F93B5F7565E957F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Option_tF75313E4E874EF52E854420D3DEEA04A99AE0E7F_CustomAttributesCacheGenerator_Option__ctor_m61127A3EBF04C32BAECB3FDC43C06A4A0BC34D55(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Option_tF75313E4E874EF52E854420D3DEEA04A99AE0E7F_CustomAttributesCacheGenerator_Option__ctor_mE400E557A643D6FD4336467A7CA7F4EBE15B193B(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Option_tF75313E4E874EF52E854420D3DEEA04A99AE0E7F_CustomAttributesCacheGenerator_Option_Clone_mE36C75B283328B159094B29AD7C378CF96029557(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Option_tF75313E4E874EF52E854420D3DEEA04A99AE0E7F_CustomAttributesCacheGenerator_Option_Equals_m5EE432C5466D73AD284F80AC21D7B022855176E8(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Option_tF75313E4E874EF52E854420D3DEEA04A99AE0E7F_CustomAttributesCacheGenerator_Option_Equals_m851A5A03B1AD4D16DFC04D52FA7548AF198392DD(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Option_tF75313E4E874EF52E854420D3DEEA04A99AE0E7F_CustomAttributesCacheGenerator_Option_GetHashCode_mAB0CB2FD7D75D98123988AECFD80B5C37B4A02D9(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Option_tF75313E4E874EF52E854420D3DEEA04A99AE0E7F_CustomAttributesCacheGenerator_Option_ToString_m87855F8C2E044740FD0712D0F8CF3DBE3143BF82(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Option_tF75313E4E874EF52E854420D3DEEA04A99AE0E7F_CustomAttributesCacheGenerator_Option_WriteTo_m08FFCCEC210A886C2FC6AD2D163A500877A28531(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Option_tF75313E4E874EF52E854420D3DEEA04A99AE0E7F_CustomAttributesCacheGenerator_Option_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_mB2C264C7D44F83BE2E6D5E68825776101ADDD61C(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Option_tF75313E4E874EF52E854420D3DEEA04A99AE0E7F_CustomAttributesCacheGenerator_Option_CalculateSize_mFF4098BC49D348AADE7E6B68D8976452F255D94D(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Option_tF75313E4E874EF52E854420D3DEEA04A99AE0E7F_CustomAttributesCacheGenerator_Option_MergeFrom_mA6F300660905E229B30AF09C9207C6EF44E158B9(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Option_tF75313E4E874EF52E854420D3DEEA04A99AE0E7F_CustomAttributesCacheGenerator_Option_MergeFrom_m9EA5877DB4934007C01D74759B0E63925094987D(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Option_tF75313E4E874EF52E854420D3DEEA04A99AE0E7F_CustomAttributesCacheGenerator_Option_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m1CA4CEF42E2A2A7ADD75A49F9CBE0A644C0D01D6(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Option_tF75313E4E874EF52E854420D3DEEA04A99AE0E7F_CustomAttributesCacheGenerator_Option_tF75313E4E874EF52E854420D3DEEA04A99AE0E7F____Parser_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Option_tF75313E4E874EF52E854420D3DEEA04A99AE0E7F_CustomAttributesCacheGenerator_Option_tF75313E4E874EF52E854420D3DEEA04A99AE0E7F____Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Option_tF75313E4E874EF52E854420D3DEEA04A99AE0E7F_CustomAttributesCacheGenerator_Option_tF75313E4E874EF52E854420D3DEEA04A99AE0E7F____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Option_tF75313E4E874EF52E854420D3DEEA04A99AE0E7F_CustomAttributesCacheGenerator_Option_tF75313E4E874EF52E854420D3DEEA04A99AE0E7F____Name_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Option_tF75313E4E874EF52E854420D3DEEA04A99AE0E7F_CustomAttributesCacheGenerator_Option_tF75313E4E874EF52E854420D3DEEA04A99AE0E7F____Value_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void U3CU3Ec_t087BC17745CAEFC744B320D652159EAED9BDA03F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FileDescriptorSet_t033C2CF3F219A2421776E1BD1042AC2757EFAC3E_CustomAttributesCacheGenerator_FileDescriptorSet__ctor_mA7A07A046215E12A519D8592D84CC8E5D77DE8A4(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileDescriptorSet_t033C2CF3F219A2421776E1BD1042AC2757EFAC3E_CustomAttributesCacheGenerator_FileDescriptorSet__ctor_m109C9F09C6370D20B70E70630294D2C09790FB73(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileDescriptorSet_t033C2CF3F219A2421776E1BD1042AC2757EFAC3E_CustomAttributesCacheGenerator_FileDescriptorSet_Clone_m265C516E8B9C78BD5CF6FC647677B44EA22C19C1(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileDescriptorSet_t033C2CF3F219A2421776E1BD1042AC2757EFAC3E_CustomAttributesCacheGenerator_FileDescriptorSet_Equals_mF6380585A3C4BCBB875C05218072B5FC9E7BEDA4(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileDescriptorSet_t033C2CF3F219A2421776E1BD1042AC2757EFAC3E_CustomAttributesCacheGenerator_FileDescriptorSet_Equals_m6FB81A95B00A2BA8007CD098A1E64BC53CB1E8DE(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileDescriptorSet_t033C2CF3F219A2421776E1BD1042AC2757EFAC3E_CustomAttributesCacheGenerator_FileDescriptorSet_GetHashCode_m5E8932B0438992144D00152506F56564FC008603(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileDescriptorSet_t033C2CF3F219A2421776E1BD1042AC2757EFAC3E_CustomAttributesCacheGenerator_FileDescriptorSet_ToString_m55C6B1E4124A8667B7A472041A15DFA788312E7A(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileDescriptorSet_t033C2CF3F219A2421776E1BD1042AC2757EFAC3E_CustomAttributesCacheGenerator_FileDescriptorSet_WriteTo_m906D2C2B397CDAFF5EF8D444C80AEB8410EAB931(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileDescriptorSet_t033C2CF3F219A2421776E1BD1042AC2757EFAC3E_CustomAttributesCacheGenerator_FileDescriptorSet_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m07D693FF17AE52BE5EBFD65CC7132C30BD5F1838(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileDescriptorSet_t033C2CF3F219A2421776E1BD1042AC2757EFAC3E_CustomAttributesCacheGenerator_FileDescriptorSet_CalculateSize_m71F5ECD29922E5260C9F27F5E9158338697E1A6B(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileDescriptorSet_t033C2CF3F219A2421776E1BD1042AC2757EFAC3E_CustomAttributesCacheGenerator_FileDescriptorSet_MergeFrom_m4AD41D3A6331B1B8AA293288747EE68F38E15C41(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileDescriptorSet_t033C2CF3F219A2421776E1BD1042AC2757EFAC3E_CustomAttributesCacheGenerator_FileDescriptorSet_MergeFrom_m6A837636C5DF50AB53B23E53FE1FC765B6828B27(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileDescriptorSet_t033C2CF3F219A2421776E1BD1042AC2757EFAC3E_CustomAttributesCacheGenerator_FileDescriptorSet_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m30A4D0A83029F411C07921760B1EF8F53B90D6B9(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileDescriptorSet_t033C2CF3F219A2421776E1BD1042AC2757EFAC3E_CustomAttributesCacheGenerator_FileDescriptorSet_t033C2CF3F219A2421776E1BD1042AC2757EFAC3E____Parser_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileDescriptorSet_t033C2CF3F219A2421776E1BD1042AC2757EFAC3E_CustomAttributesCacheGenerator_FileDescriptorSet_t033C2CF3F219A2421776E1BD1042AC2757EFAC3E____Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileDescriptorSet_t033C2CF3F219A2421776E1BD1042AC2757EFAC3E_CustomAttributesCacheGenerator_FileDescriptorSet_t033C2CF3F219A2421776E1BD1042AC2757EFAC3E____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void U3CU3Ec_t6FB6EF4327813A02BBA3BCF295054B27010543D9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0_CustomAttributesCacheGenerator_FileDescriptorProto__ctor_m4485617D3F5C6C7A0B6803FB7A6A9E5CD9289E68(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0_CustomAttributesCacheGenerator_FileDescriptorProto__ctor_m0E39D73BEB59D91943EF2EA1E8B43205EE2AD0D8(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0_CustomAttributesCacheGenerator_FileDescriptorProto_Clone_m8A232664925A576E17BAEEE2B7C78110936F0378(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0_CustomAttributesCacheGenerator_FileDescriptorProto_Equals_mB2CE0475BC597D2DD9156FE405B673152788F432(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0_CustomAttributesCacheGenerator_FileDescriptorProto_Equals_mC0682C79CEED2451C8CDFE709CF4F49307B2FB3A(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0_CustomAttributesCacheGenerator_FileDescriptorProto_GetHashCode_mE4A7E0861EF395F56F05D05B158256F22EE5670A(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0_CustomAttributesCacheGenerator_FileDescriptorProto_ToString_m570AF4B70A7741E75E2065FA39DCF185C8D94CF9(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0_CustomAttributesCacheGenerator_FileDescriptorProto_WriteTo_m29D8F0A32F233CEBB2F2FBCB590D4739642AA0C2(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0_CustomAttributesCacheGenerator_FileDescriptorProto_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m00C04878C6E3CEEAD15F30CCF8564EB613B2AE9B(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0_CustomAttributesCacheGenerator_FileDescriptorProto_CalculateSize_m7F8750FEC9F67C17825224AF365936D26A5AD58F(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0_CustomAttributesCacheGenerator_FileDescriptorProto_MergeFrom_mC85444AE37C8A342C2E0AFCA376603A29A4FDA3D(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0_CustomAttributesCacheGenerator_FileDescriptorProto_MergeFrom_mEE0DD8720B946B400CFEC3D6C2BC73470BCF592E(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0_CustomAttributesCacheGenerator_FileDescriptorProto_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m2ED80E57E0627E94AE7DA4F37CAD6021096B16FC(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0_CustomAttributesCacheGenerator_FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0____Parser_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0_CustomAttributesCacheGenerator_FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0____Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0_CustomAttributesCacheGenerator_FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0_CustomAttributesCacheGenerator_FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0____Name_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0_CustomAttributesCacheGenerator_FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0____HasName_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0_CustomAttributesCacheGenerator_FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0____Package_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0_CustomAttributesCacheGenerator_FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0____HasPackage_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0_CustomAttributesCacheGenerator_FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0____Dependency_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0_CustomAttributesCacheGenerator_FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0____PublicDependency_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0_CustomAttributesCacheGenerator_FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0____MessageType_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0_CustomAttributesCacheGenerator_FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0____EnumType_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0_CustomAttributesCacheGenerator_FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0____Service_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0_CustomAttributesCacheGenerator_FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0____Extension_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0_CustomAttributesCacheGenerator_FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0____Options_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0_CustomAttributesCacheGenerator_FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0____SourceCodeInfo_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0_CustomAttributesCacheGenerator_FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0____Syntax_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0_CustomAttributesCacheGenerator_FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0____HasSyntax_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void U3CU3Ec_t87ED2FDB0970A42C60EE9362A8A7AE6C1CB3B038_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DescriptorProto_tB03AF5EFB75071C3E950F726F78461450496C898_CustomAttributesCacheGenerator_DescriptorProto__ctor_mBA850C613C3DB05A54F24C5E732A05CB9EEA4A4E(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void DescriptorProto_tB03AF5EFB75071C3E950F726F78461450496C898_CustomAttributesCacheGenerator_DescriptorProto__ctor_mCF435119FBBAA9A389619587C9435EC7EC24ED46(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void DescriptorProto_tB03AF5EFB75071C3E950F726F78461450496C898_CustomAttributesCacheGenerator_DescriptorProto_Clone_m8D6A43AB22810E2ABA42EE59D221FFD6F7F7DD58(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void DescriptorProto_tB03AF5EFB75071C3E950F726F78461450496C898_CustomAttributesCacheGenerator_DescriptorProto_Equals_mFBBBAFCA37FEC00E0D4DDE7758E3F23405F72323(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void DescriptorProto_tB03AF5EFB75071C3E950F726F78461450496C898_CustomAttributesCacheGenerator_DescriptorProto_Equals_m2254305BFFB53FE27C4AA5B4E0FC5C1580C7230F(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void DescriptorProto_tB03AF5EFB75071C3E950F726F78461450496C898_CustomAttributesCacheGenerator_DescriptorProto_GetHashCode_m158B9AA4C433C9EEE8BAB3C7C702E46277B64AA5(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void DescriptorProto_tB03AF5EFB75071C3E950F726F78461450496C898_CustomAttributesCacheGenerator_DescriptorProto_ToString_m7300A8A26C0B22F794F933459B56D6B86DF4854B(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void DescriptorProto_tB03AF5EFB75071C3E950F726F78461450496C898_CustomAttributesCacheGenerator_DescriptorProto_WriteTo_mA8BED1F35DFFC420B71BC788C94C974AF366A735(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void DescriptorProto_tB03AF5EFB75071C3E950F726F78461450496C898_CustomAttributesCacheGenerator_DescriptorProto_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m08457FFB232FE6F6902B30F8E14731C57D6FF845(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void DescriptorProto_tB03AF5EFB75071C3E950F726F78461450496C898_CustomAttributesCacheGenerator_DescriptorProto_CalculateSize_mD15A93668659599FB42FF86E335DBCCBB5D828F6(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void DescriptorProto_tB03AF5EFB75071C3E950F726F78461450496C898_CustomAttributesCacheGenerator_DescriptorProto_MergeFrom_m3309DEA6819B3F251E2F114761961232F3A555EF(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void DescriptorProto_tB03AF5EFB75071C3E950F726F78461450496C898_CustomAttributesCacheGenerator_DescriptorProto_MergeFrom_m186FD354E99638C8B67744AE7ED9C2556DCC9E24(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void DescriptorProto_tB03AF5EFB75071C3E950F726F78461450496C898_CustomAttributesCacheGenerator_DescriptorProto_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_mB4A4E23A065E9FD8A6B8030ED9498F52A2181CD2(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void DescriptorProto_tB03AF5EFB75071C3E950F726F78461450496C898_CustomAttributesCacheGenerator_DescriptorProto_tB03AF5EFB75071C3E950F726F78461450496C898____Parser_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void DescriptorProto_tB03AF5EFB75071C3E950F726F78461450496C898_CustomAttributesCacheGenerator_DescriptorProto_tB03AF5EFB75071C3E950F726F78461450496C898____Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void DescriptorProto_tB03AF5EFB75071C3E950F726F78461450496C898_CustomAttributesCacheGenerator_DescriptorProto_tB03AF5EFB75071C3E950F726F78461450496C898____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void DescriptorProto_tB03AF5EFB75071C3E950F726F78461450496C898_CustomAttributesCacheGenerator_DescriptorProto_tB03AF5EFB75071C3E950F726F78461450496C898____Name_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void DescriptorProto_tB03AF5EFB75071C3E950F726F78461450496C898_CustomAttributesCacheGenerator_DescriptorProto_tB03AF5EFB75071C3E950F726F78461450496C898____HasName_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void DescriptorProto_tB03AF5EFB75071C3E950F726F78461450496C898_CustomAttributesCacheGenerator_DescriptorProto_tB03AF5EFB75071C3E950F726F78461450496C898____Field_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void DescriptorProto_tB03AF5EFB75071C3E950F726F78461450496C898_CustomAttributesCacheGenerator_DescriptorProto_tB03AF5EFB75071C3E950F726F78461450496C898____Extension_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void DescriptorProto_tB03AF5EFB75071C3E950F726F78461450496C898_CustomAttributesCacheGenerator_DescriptorProto_tB03AF5EFB75071C3E950F726F78461450496C898____NestedType_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void DescriptorProto_tB03AF5EFB75071C3E950F726F78461450496C898_CustomAttributesCacheGenerator_DescriptorProto_tB03AF5EFB75071C3E950F726F78461450496C898____EnumType_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void DescriptorProto_tB03AF5EFB75071C3E950F726F78461450496C898_CustomAttributesCacheGenerator_DescriptorProto_tB03AF5EFB75071C3E950F726F78461450496C898____OneofDecl_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void DescriptorProto_tB03AF5EFB75071C3E950F726F78461450496C898_CustomAttributesCacheGenerator_DescriptorProto_tB03AF5EFB75071C3E950F726F78461450496C898____Options_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Types_tD2B79473C8915A11C5D5EA0504D881CA5B76B4E6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ExtensionRange_t355AED379D3458AC80443047C2DF72AA44639D84_CustomAttributesCacheGenerator_ExtensionRange__ctor_mCE3CBBCDC18FA5EA4ADBF4AF4AF53D13327D323B(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ExtensionRange_t355AED379D3458AC80443047C2DF72AA44639D84_CustomAttributesCacheGenerator_ExtensionRange__ctor_m86DE2DE4CC81E1797CDC04A3068161C7AE6137D5(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ExtensionRange_t355AED379D3458AC80443047C2DF72AA44639D84_CustomAttributesCacheGenerator_ExtensionRange_Clone_mFFA2D7061BFDF6189B1DAAD7388E9C545D8495C9(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ExtensionRange_t355AED379D3458AC80443047C2DF72AA44639D84_CustomAttributesCacheGenerator_ExtensionRange_Equals_mC16F46A587D2727AB7527B37898019C9EC0436FC(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ExtensionRange_t355AED379D3458AC80443047C2DF72AA44639D84_CustomAttributesCacheGenerator_ExtensionRange_Equals_mBCE0F486ED3163380B949A8F4654DC2359A0EA61(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ExtensionRange_t355AED379D3458AC80443047C2DF72AA44639D84_CustomAttributesCacheGenerator_ExtensionRange_GetHashCode_m1705F6C558349578176C5BA9C2CD918B6A6689E8(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ExtensionRange_t355AED379D3458AC80443047C2DF72AA44639D84_CustomAttributesCacheGenerator_ExtensionRange_ToString_m92C8CC6CEE16909BD1FA3C1323DC5A214AF7FAC4(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ExtensionRange_t355AED379D3458AC80443047C2DF72AA44639D84_CustomAttributesCacheGenerator_ExtensionRange_WriteTo_mCC7686D9A689AC98ED8D425B3131531FFD927C77(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ExtensionRange_t355AED379D3458AC80443047C2DF72AA44639D84_CustomAttributesCacheGenerator_ExtensionRange_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m78E864903CC6C7966483D1C1DADD8FE35236BD02(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ExtensionRange_t355AED379D3458AC80443047C2DF72AA44639D84_CustomAttributesCacheGenerator_ExtensionRange_CalculateSize_mBE8901808FC96A0B6647FEE680B06880F0FA84E7(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ExtensionRange_t355AED379D3458AC80443047C2DF72AA44639D84_CustomAttributesCacheGenerator_ExtensionRange_MergeFrom_m314E38B926BE9F9F1CD9E34681625888E95EB00E(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ExtensionRange_t355AED379D3458AC80443047C2DF72AA44639D84_CustomAttributesCacheGenerator_ExtensionRange_MergeFrom_m99ECB1EF2D098752D7635C356C1CFA50784EB3EA(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ExtensionRange_t355AED379D3458AC80443047C2DF72AA44639D84_CustomAttributesCacheGenerator_ExtensionRange_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m24A97A8F83256C686E2E8AE441EF5A58C03244EE(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ExtensionRange_t355AED379D3458AC80443047C2DF72AA44639D84_CustomAttributesCacheGenerator_ExtensionRange_t355AED379D3458AC80443047C2DF72AA44639D84____Parser_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ExtensionRange_t355AED379D3458AC80443047C2DF72AA44639D84_CustomAttributesCacheGenerator_ExtensionRange_t355AED379D3458AC80443047C2DF72AA44639D84____Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ExtensionRange_t355AED379D3458AC80443047C2DF72AA44639D84_CustomAttributesCacheGenerator_ExtensionRange_t355AED379D3458AC80443047C2DF72AA44639D84____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ExtensionRange_t355AED379D3458AC80443047C2DF72AA44639D84_CustomAttributesCacheGenerator_ExtensionRange_t355AED379D3458AC80443047C2DF72AA44639D84____Start_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ExtensionRange_t355AED379D3458AC80443047C2DF72AA44639D84_CustomAttributesCacheGenerator_ExtensionRange_t355AED379D3458AC80443047C2DF72AA44639D84____HasStart_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ExtensionRange_t355AED379D3458AC80443047C2DF72AA44639D84_CustomAttributesCacheGenerator_ExtensionRange_t355AED379D3458AC80443047C2DF72AA44639D84____End_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ExtensionRange_t355AED379D3458AC80443047C2DF72AA44639D84_CustomAttributesCacheGenerator_ExtensionRange_t355AED379D3458AC80443047C2DF72AA44639D84____HasEnd_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ExtensionRange_t355AED379D3458AC80443047C2DF72AA44639D84_CustomAttributesCacheGenerator_ExtensionRange_t355AED379D3458AC80443047C2DF72AA44639D84____Options_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void U3CU3Ec_tC3789B2A2639D39522B7FC88CE44B0FF78387D85_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ReservedRange_tAED8F62AF040F5F133732196696893B313EA458B_CustomAttributesCacheGenerator_ReservedRange__ctor_mB2F8257CCD2E526672CB4B552FD6799706F38CD5(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ReservedRange_tAED8F62AF040F5F133732196696893B313EA458B_CustomAttributesCacheGenerator_ReservedRange__ctor_m73F9F5F5A8EE837C56568DD545DA961703779FAD(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ReservedRange_tAED8F62AF040F5F133732196696893B313EA458B_CustomAttributesCacheGenerator_ReservedRange_Clone_m30F1DBAF17F68D2418492E3502CC6D8EA1CBDCBB(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ReservedRange_tAED8F62AF040F5F133732196696893B313EA458B_CustomAttributesCacheGenerator_ReservedRange_Equals_m1C20E897B3FFD56BAB9E3248E741DFD243C3EC67(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ReservedRange_tAED8F62AF040F5F133732196696893B313EA458B_CustomAttributesCacheGenerator_ReservedRange_Equals_mE5912F6171510806B9EF02DE03CF8DD3F35B9E4C(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ReservedRange_tAED8F62AF040F5F133732196696893B313EA458B_CustomAttributesCacheGenerator_ReservedRange_GetHashCode_mA843DDE13FDC934E5BF90F4E87226E6895C7DF3A(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ReservedRange_tAED8F62AF040F5F133732196696893B313EA458B_CustomAttributesCacheGenerator_ReservedRange_ToString_m5B37BC81177B03CCEE38CEF2CC602923C1AE18E9(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ReservedRange_tAED8F62AF040F5F133732196696893B313EA458B_CustomAttributesCacheGenerator_ReservedRange_WriteTo_mAE8C9FDDD04DFAA10E55D3CB8E65948E461FBCB7(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ReservedRange_tAED8F62AF040F5F133732196696893B313EA458B_CustomAttributesCacheGenerator_ReservedRange_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m85F3FBF2DF0058D3A982337FD4736926519B8D07(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ReservedRange_tAED8F62AF040F5F133732196696893B313EA458B_CustomAttributesCacheGenerator_ReservedRange_CalculateSize_m76825970AE352D4A345CA58060D3BA6FF4AD90C5(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ReservedRange_tAED8F62AF040F5F133732196696893B313EA458B_CustomAttributesCacheGenerator_ReservedRange_MergeFrom_mE827CEF25B811678E61BB8E50240CBE127E51442(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ReservedRange_tAED8F62AF040F5F133732196696893B313EA458B_CustomAttributesCacheGenerator_ReservedRange_MergeFrom_m79DF028674F84059F761E6CAA9967E750D41E0C6(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ReservedRange_tAED8F62AF040F5F133732196696893B313EA458B_CustomAttributesCacheGenerator_ReservedRange_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m53FFCAA3778D05AEEA1A4930A32451CB6F529B68(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ReservedRange_tAED8F62AF040F5F133732196696893B313EA458B_CustomAttributesCacheGenerator_ReservedRange_tAED8F62AF040F5F133732196696893B313EA458B____Parser_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ReservedRange_tAED8F62AF040F5F133732196696893B313EA458B_CustomAttributesCacheGenerator_ReservedRange_tAED8F62AF040F5F133732196696893B313EA458B____Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ReservedRange_tAED8F62AF040F5F133732196696893B313EA458B_CustomAttributesCacheGenerator_ReservedRange_tAED8F62AF040F5F133732196696893B313EA458B____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ReservedRange_tAED8F62AF040F5F133732196696893B313EA458B_CustomAttributesCacheGenerator_ReservedRange_tAED8F62AF040F5F133732196696893B313EA458B____Start_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ReservedRange_tAED8F62AF040F5F133732196696893B313EA458B_CustomAttributesCacheGenerator_ReservedRange_tAED8F62AF040F5F133732196696893B313EA458B____HasStart_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ReservedRange_tAED8F62AF040F5F133732196696893B313EA458B_CustomAttributesCacheGenerator_ReservedRange_tAED8F62AF040F5F133732196696893B313EA458B____End_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ReservedRange_tAED8F62AF040F5F133732196696893B313EA458B_CustomAttributesCacheGenerator_ReservedRange_tAED8F62AF040F5F133732196696893B313EA458B____HasEnd_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void U3CU3Ec_t14ED7AF2EE0452B68867D51843E5E3FBF40F7D2D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_t89C33FBD99D7599051C877B6F08FD1378E2E3029_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ExtensionRangeOptions_t497AE48098D601FD2AB661860BDA91704902E99F_CustomAttributesCacheGenerator_ExtensionRangeOptions__ctor_m03DB6B7CDC07388AD8EAC496F9F8FE6150421544(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ExtensionRangeOptions_t497AE48098D601FD2AB661860BDA91704902E99F_CustomAttributesCacheGenerator_ExtensionRangeOptions__ctor_m0268E7412A855333005EB632BC2D81177F7CF0D2(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ExtensionRangeOptions_t497AE48098D601FD2AB661860BDA91704902E99F_CustomAttributesCacheGenerator_ExtensionRangeOptions_Clone_mC718522EFD92E71DA33FA9E9C28DB3DDF2C4DA7E(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ExtensionRangeOptions_t497AE48098D601FD2AB661860BDA91704902E99F_CustomAttributesCacheGenerator_ExtensionRangeOptions_Equals_mC28F05C7BAD43AC052787ED90AE10C25DE6F9E5F(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ExtensionRangeOptions_t497AE48098D601FD2AB661860BDA91704902E99F_CustomAttributesCacheGenerator_ExtensionRangeOptions_Equals_m2A50B09E41F199B9161902DC11CEE01EDB02AD93(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ExtensionRangeOptions_t497AE48098D601FD2AB661860BDA91704902E99F_CustomAttributesCacheGenerator_ExtensionRangeOptions_GetHashCode_m1FA993AAD13E3E515CB8E03384BBC859A807F719(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ExtensionRangeOptions_t497AE48098D601FD2AB661860BDA91704902E99F_CustomAttributesCacheGenerator_ExtensionRangeOptions_ToString_mCDAFBAF9D027FD33891829389B30DEEEDA1DA694(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ExtensionRangeOptions_t497AE48098D601FD2AB661860BDA91704902E99F_CustomAttributesCacheGenerator_ExtensionRangeOptions_WriteTo_mE36C0650E68E929057CDB2BEFBD837EB32726F93(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ExtensionRangeOptions_t497AE48098D601FD2AB661860BDA91704902E99F_CustomAttributesCacheGenerator_ExtensionRangeOptions_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m8AAAA629B772292D614BE8DF07E56142B2ACDADE(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ExtensionRangeOptions_t497AE48098D601FD2AB661860BDA91704902E99F_CustomAttributesCacheGenerator_ExtensionRangeOptions_CalculateSize_mE5FABC172FABF85E1427D3110E45BDD0E64E464C(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ExtensionRangeOptions_t497AE48098D601FD2AB661860BDA91704902E99F_CustomAttributesCacheGenerator_ExtensionRangeOptions_MergeFrom_m676DA699130B7B80F30ACA1338B9E369B44940B1(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ExtensionRangeOptions_t497AE48098D601FD2AB661860BDA91704902E99F_CustomAttributesCacheGenerator_ExtensionRangeOptions_MergeFrom_m2E9222BFE09BE7AC39995BB9DF8377E8C5C02486(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ExtensionRangeOptions_t497AE48098D601FD2AB661860BDA91704902E99F_CustomAttributesCacheGenerator_ExtensionRangeOptions_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m67DB4C9A5F1DC35C7852751B65AE26736ED00D11(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ExtensionRangeOptions_t497AE48098D601FD2AB661860BDA91704902E99F_CustomAttributesCacheGenerator_ExtensionRangeOptions_t497AE48098D601FD2AB661860BDA91704902E99F____Parser_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ExtensionRangeOptions_t497AE48098D601FD2AB661860BDA91704902E99F_CustomAttributesCacheGenerator_ExtensionRangeOptions_t497AE48098D601FD2AB661860BDA91704902E99F____Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ExtensionRangeOptions_t497AE48098D601FD2AB661860BDA91704902E99F_CustomAttributesCacheGenerator_ExtensionRangeOptions_t497AE48098D601FD2AB661860BDA91704902E99F____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void U3CU3Ec_t79B3519CE8A82101DDF9B118AA4D84300712C1D4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785_CustomAttributesCacheGenerator_FieldDescriptorProto__ctor_mDB51787020766E9E2EAB543B0184EAFA0982FD22(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785_CustomAttributesCacheGenerator_FieldDescriptorProto__ctor_mF5B66C7A74743444259BC8300F6D960A429A61AA(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785_CustomAttributesCacheGenerator_FieldDescriptorProto_Clone_m3B18B348D372D0C457D0F49FD218045E8D8A89B3(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785_CustomAttributesCacheGenerator_FieldDescriptorProto_Equals_m8280930EB8D7F7C3BFE6BA9FA223C0411C56F47C(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785_CustomAttributesCacheGenerator_FieldDescriptorProto_Equals_m3527F0C058D9FC4E1466AFD93B448721A1F18763(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785_CustomAttributesCacheGenerator_FieldDescriptorProto_GetHashCode_mB99143077FA079BD863F94EB1D18717B09C01CC8(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785_CustomAttributesCacheGenerator_FieldDescriptorProto_ToString_m11F796FE089C2875CDB38AAA3422E19996433A7E(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785_CustomAttributesCacheGenerator_FieldDescriptorProto_WriteTo_mFA22B0175F6D838CEA09CA1F7B6DFE0E76622CC7(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785_CustomAttributesCacheGenerator_FieldDescriptorProto_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m5898B3409384DC7633B83CDAC353FFD5D1D67784(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785_CustomAttributesCacheGenerator_FieldDescriptorProto_CalculateSize_m955D78E94DE36494938994453F571457E48436D2(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785_CustomAttributesCacheGenerator_FieldDescriptorProto_MergeFrom_m0DCDC8CB908E61CF0D7136B40C38940D9F21F986(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785_CustomAttributesCacheGenerator_FieldDescriptorProto_MergeFrom_m70DA833065B73BA3CE80FE249DFDAA745A4981EC(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785_CustomAttributesCacheGenerator_FieldDescriptorProto_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m986E810C00664AFB4CF43B946B88EE7A1DC7B66F(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785_CustomAttributesCacheGenerator_FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785____Parser_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785_CustomAttributesCacheGenerator_FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785____Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785_CustomAttributesCacheGenerator_FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785_CustomAttributesCacheGenerator_FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785____Name_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785_CustomAttributesCacheGenerator_FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785____HasName_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785_CustomAttributesCacheGenerator_FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785____Number_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785_CustomAttributesCacheGenerator_FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785____HasNumber_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785_CustomAttributesCacheGenerator_FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785____Label_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785_CustomAttributesCacheGenerator_FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785____HasLabel_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785_CustomAttributesCacheGenerator_FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785____Type_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785_CustomAttributesCacheGenerator_FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785____HasType_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785_CustomAttributesCacheGenerator_FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785____TypeName_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785_CustomAttributesCacheGenerator_FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785____HasTypeName_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785_CustomAttributesCacheGenerator_FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785____Extendee_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785_CustomAttributesCacheGenerator_FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785____HasExtendee_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785_CustomAttributesCacheGenerator_FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785____DefaultValue_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785_CustomAttributesCacheGenerator_FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785____HasDefaultValue_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785_CustomAttributesCacheGenerator_FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785____OneofIndex_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785_CustomAttributesCacheGenerator_FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785____HasOneofIndex_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785_CustomAttributesCacheGenerator_FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785____JsonName_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785_CustomAttributesCacheGenerator_FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785____HasJsonName_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785_CustomAttributesCacheGenerator_FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785____Options_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785_CustomAttributesCacheGenerator_FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785____Proto3Optional_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785_CustomAttributesCacheGenerator_FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785____HasProto3Optional_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Types_tDF6C4DAD822ECAFD7D2E79C80BDDA72070A53F6C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Type_tA2574C38CEE89946C1AFA608D4182429BB8490A9_CustomAttributesCacheGenerator_Double(CustomAttributesCache* cache)
{
	{
		OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * tmp = (OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 *)cache->attributes[0];
		OriginalNameAttribute__ctor_mB0C6C38C49B9261D2DC423D692E80B8EBC657BF9(tmp, il2cpp_codegen_string_new_wrapper("\x54\x59\x50\x45\x5F\x44\x4F\x55\x42\x4C\x45"), NULL);
	}
}
static void Type_tA2574C38CEE89946C1AFA608D4182429BB8490A9_CustomAttributesCacheGenerator_Float(CustomAttributesCache* cache)
{
	{
		OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * tmp = (OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 *)cache->attributes[0];
		OriginalNameAttribute__ctor_mB0C6C38C49B9261D2DC423D692E80B8EBC657BF9(tmp, il2cpp_codegen_string_new_wrapper("\x54\x59\x50\x45\x5F\x46\x4C\x4F\x41\x54"), NULL);
	}
}
static void Type_tA2574C38CEE89946C1AFA608D4182429BB8490A9_CustomAttributesCacheGenerator_Int64(CustomAttributesCache* cache)
{
	{
		OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * tmp = (OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 *)cache->attributes[0];
		OriginalNameAttribute__ctor_mB0C6C38C49B9261D2DC423D692E80B8EBC657BF9(tmp, il2cpp_codegen_string_new_wrapper("\x54\x59\x50\x45\x5F\x49\x4E\x54\x36\x34"), NULL);
	}
}
static void Type_tA2574C38CEE89946C1AFA608D4182429BB8490A9_CustomAttributesCacheGenerator_Uint64(CustomAttributesCache* cache)
{
	{
		OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * tmp = (OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 *)cache->attributes[0];
		OriginalNameAttribute__ctor_mB0C6C38C49B9261D2DC423D692E80B8EBC657BF9(tmp, il2cpp_codegen_string_new_wrapper("\x54\x59\x50\x45\x5F\x55\x49\x4E\x54\x36\x34"), NULL);
	}
}
static void Type_tA2574C38CEE89946C1AFA608D4182429BB8490A9_CustomAttributesCacheGenerator_Int32(CustomAttributesCache* cache)
{
	{
		OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * tmp = (OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 *)cache->attributes[0];
		OriginalNameAttribute__ctor_mB0C6C38C49B9261D2DC423D692E80B8EBC657BF9(tmp, il2cpp_codegen_string_new_wrapper("\x54\x59\x50\x45\x5F\x49\x4E\x54\x33\x32"), NULL);
	}
}
static void Type_tA2574C38CEE89946C1AFA608D4182429BB8490A9_CustomAttributesCacheGenerator_Fixed64(CustomAttributesCache* cache)
{
	{
		OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * tmp = (OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 *)cache->attributes[0];
		OriginalNameAttribute__ctor_mB0C6C38C49B9261D2DC423D692E80B8EBC657BF9(tmp, il2cpp_codegen_string_new_wrapper("\x54\x59\x50\x45\x5F\x46\x49\x58\x45\x44\x36\x34"), NULL);
	}
}
static void Type_tA2574C38CEE89946C1AFA608D4182429BB8490A9_CustomAttributesCacheGenerator_Fixed32(CustomAttributesCache* cache)
{
	{
		OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * tmp = (OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 *)cache->attributes[0];
		OriginalNameAttribute__ctor_mB0C6C38C49B9261D2DC423D692E80B8EBC657BF9(tmp, il2cpp_codegen_string_new_wrapper("\x54\x59\x50\x45\x5F\x46\x49\x58\x45\x44\x33\x32"), NULL);
	}
}
static void Type_tA2574C38CEE89946C1AFA608D4182429BB8490A9_CustomAttributesCacheGenerator_Bool(CustomAttributesCache* cache)
{
	{
		OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * tmp = (OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 *)cache->attributes[0];
		OriginalNameAttribute__ctor_mB0C6C38C49B9261D2DC423D692E80B8EBC657BF9(tmp, il2cpp_codegen_string_new_wrapper("\x54\x59\x50\x45\x5F\x42\x4F\x4F\x4C"), NULL);
	}
}
static void Type_tA2574C38CEE89946C1AFA608D4182429BB8490A9_CustomAttributesCacheGenerator_String(CustomAttributesCache* cache)
{
	{
		OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * tmp = (OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 *)cache->attributes[0];
		OriginalNameAttribute__ctor_mB0C6C38C49B9261D2DC423D692E80B8EBC657BF9(tmp, il2cpp_codegen_string_new_wrapper("\x54\x59\x50\x45\x5F\x53\x54\x52\x49\x4E\x47"), NULL);
	}
}
static void Type_tA2574C38CEE89946C1AFA608D4182429BB8490A9_CustomAttributesCacheGenerator_Group(CustomAttributesCache* cache)
{
	{
		OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * tmp = (OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 *)cache->attributes[0];
		OriginalNameAttribute__ctor_mB0C6C38C49B9261D2DC423D692E80B8EBC657BF9(tmp, il2cpp_codegen_string_new_wrapper("\x54\x59\x50\x45\x5F\x47\x52\x4F\x55\x50"), NULL);
	}
}
static void Type_tA2574C38CEE89946C1AFA608D4182429BB8490A9_CustomAttributesCacheGenerator_Message(CustomAttributesCache* cache)
{
	{
		OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * tmp = (OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 *)cache->attributes[0];
		OriginalNameAttribute__ctor_mB0C6C38C49B9261D2DC423D692E80B8EBC657BF9(tmp, il2cpp_codegen_string_new_wrapper("\x54\x59\x50\x45\x5F\x4D\x45\x53\x53\x41\x47\x45"), NULL);
	}
}
static void Type_tA2574C38CEE89946C1AFA608D4182429BB8490A9_CustomAttributesCacheGenerator_Bytes(CustomAttributesCache* cache)
{
	{
		OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * tmp = (OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 *)cache->attributes[0];
		OriginalNameAttribute__ctor_mB0C6C38C49B9261D2DC423D692E80B8EBC657BF9(tmp, il2cpp_codegen_string_new_wrapper("\x54\x59\x50\x45\x5F\x42\x59\x54\x45\x53"), NULL);
	}
}
static void Type_tA2574C38CEE89946C1AFA608D4182429BB8490A9_CustomAttributesCacheGenerator_Uint32(CustomAttributesCache* cache)
{
	{
		OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * tmp = (OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 *)cache->attributes[0];
		OriginalNameAttribute__ctor_mB0C6C38C49B9261D2DC423D692E80B8EBC657BF9(tmp, il2cpp_codegen_string_new_wrapper("\x54\x59\x50\x45\x5F\x55\x49\x4E\x54\x33\x32"), NULL);
	}
}
static void Type_tA2574C38CEE89946C1AFA608D4182429BB8490A9_CustomAttributesCacheGenerator_Enum(CustomAttributesCache* cache)
{
	{
		OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * tmp = (OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 *)cache->attributes[0];
		OriginalNameAttribute__ctor_mB0C6C38C49B9261D2DC423D692E80B8EBC657BF9(tmp, il2cpp_codegen_string_new_wrapper("\x54\x59\x50\x45\x5F\x45\x4E\x55\x4D"), NULL);
	}
}
static void Type_tA2574C38CEE89946C1AFA608D4182429BB8490A9_CustomAttributesCacheGenerator_Sfixed32(CustomAttributesCache* cache)
{
	{
		OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * tmp = (OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 *)cache->attributes[0];
		OriginalNameAttribute__ctor_mB0C6C38C49B9261D2DC423D692E80B8EBC657BF9(tmp, il2cpp_codegen_string_new_wrapper("\x54\x59\x50\x45\x5F\x53\x46\x49\x58\x45\x44\x33\x32"), NULL);
	}
}
static void Type_tA2574C38CEE89946C1AFA608D4182429BB8490A9_CustomAttributesCacheGenerator_Sfixed64(CustomAttributesCache* cache)
{
	{
		OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * tmp = (OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 *)cache->attributes[0];
		OriginalNameAttribute__ctor_mB0C6C38C49B9261D2DC423D692E80B8EBC657BF9(tmp, il2cpp_codegen_string_new_wrapper("\x54\x59\x50\x45\x5F\x53\x46\x49\x58\x45\x44\x36\x34"), NULL);
	}
}
static void Type_tA2574C38CEE89946C1AFA608D4182429BB8490A9_CustomAttributesCacheGenerator_Sint32(CustomAttributesCache* cache)
{
	{
		OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * tmp = (OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 *)cache->attributes[0];
		OriginalNameAttribute__ctor_mB0C6C38C49B9261D2DC423D692E80B8EBC657BF9(tmp, il2cpp_codegen_string_new_wrapper("\x54\x59\x50\x45\x5F\x53\x49\x4E\x54\x33\x32"), NULL);
	}
}
static void Type_tA2574C38CEE89946C1AFA608D4182429BB8490A9_CustomAttributesCacheGenerator_Sint64(CustomAttributesCache* cache)
{
	{
		OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * tmp = (OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 *)cache->attributes[0];
		OriginalNameAttribute__ctor_mB0C6C38C49B9261D2DC423D692E80B8EBC657BF9(tmp, il2cpp_codegen_string_new_wrapper("\x54\x59\x50\x45\x5F\x53\x49\x4E\x54\x36\x34"), NULL);
	}
}
static void Label_t5F834D524D61512BADE59642DE16426A43F3A2F7_CustomAttributesCacheGenerator_Optional(CustomAttributesCache* cache)
{
	{
		OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * tmp = (OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 *)cache->attributes[0];
		OriginalNameAttribute__ctor_mB0C6C38C49B9261D2DC423D692E80B8EBC657BF9(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x41\x42\x45\x4C\x5F\x4F\x50\x54\x49\x4F\x4E\x41\x4C"), NULL);
	}
}
static void Label_t5F834D524D61512BADE59642DE16426A43F3A2F7_CustomAttributesCacheGenerator_Required(CustomAttributesCache* cache)
{
	{
		OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * tmp = (OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 *)cache->attributes[0];
		OriginalNameAttribute__ctor_mB0C6C38C49B9261D2DC423D692E80B8EBC657BF9(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x41\x42\x45\x4C\x5F\x52\x45\x51\x55\x49\x52\x45\x44"), NULL);
	}
}
static void Label_t5F834D524D61512BADE59642DE16426A43F3A2F7_CustomAttributesCacheGenerator_Repeated(CustomAttributesCache* cache)
{
	{
		OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * tmp = (OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 *)cache->attributes[0];
		OriginalNameAttribute__ctor_mB0C6C38C49B9261D2DC423D692E80B8EBC657BF9(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x41\x42\x45\x4C\x5F\x52\x45\x50\x45\x41\x54\x45\x44"), NULL);
	}
}
static void U3CU3Ec_tA2AE91D17E19F63E9678879CCF1B4D7A9471FD44_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OneofDescriptorProto_t6319E05084FB5AA0644D3AEF37891689AFF1DD9A_CustomAttributesCacheGenerator_OneofDescriptorProto__ctor_mD2CE062E1C095E49ABDEFDC74B17229A45493047(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void OneofDescriptorProto_t6319E05084FB5AA0644D3AEF37891689AFF1DD9A_CustomAttributesCacheGenerator_OneofDescriptorProto__ctor_m14C97250B9E90E60CA6CA6064C9250354632C3AC(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void OneofDescriptorProto_t6319E05084FB5AA0644D3AEF37891689AFF1DD9A_CustomAttributesCacheGenerator_OneofDescriptorProto_Clone_m7C45A01EF4DAE4EDB8A17F181864989B593E3D11(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void OneofDescriptorProto_t6319E05084FB5AA0644D3AEF37891689AFF1DD9A_CustomAttributesCacheGenerator_OneofDescriptorProto_Equals_m63D5B71E1166BAFAF2BE805067EC147D8D75298D(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void OneofDescriptorProto_t6319E05084FB5AA0644D3AEF37891689AFF1DD9A_CustomAttributesCacheGenerator_OneofDescriptorProto_Equals_mFB6BA3CE309BF9605D4E93E1CDBE45ECAF125EA3(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void OneofDescriptorProto_t6319E05084FB5AA0644D3AEF37891689AFF1DD9A_CustomAttributesCacheGenerator_OneofDescriptorProto_GetHashCode_m44452D2990660A273BDB0CBA23FC660618F20E1A(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void OneofDescriptorProto_t6319E05084FB5AA0644D3AEF37891689AFF1DD9A_CustomAttributesCacheGenerator_OneofDescriptorProto_ToString_mE05FEA563B6444CC0150CAB7800E63589CFF3683(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void OneofDescriptorProto_t6319E05084FB5AA0644D3AEF37891689AFF1DD9A_CustomAttributesCacheGenerator_OneofDescriptorProto_WriteTo_m1122F29DE940E7FDE249B8B49D0A4CE85FA88DF1(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void OneofDescriptorProto_t6319E05084FB5AA0644D3AEF37891689AFF1DD9A_CustomAttributesCacheGenerator_OneofDescriptorProto_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_mC33BBE456CCB3E50F512370278FCB60B20882B58(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void OneofDescriptorProto_t6319E05084FB5AA0644D3AEF37891689AFF1DD9A_CustomAttributesCacheGenerator_OneofDescriptorProto_CalculateSize_mF6EDD0758C04B3A0C663D458BAE874DD78BAE845(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void OneofDescriptorProto_t6319E05084FB5AA0644D3AEF37891689AFF1DD9A_CustomAttributesCacheGenerator_OneofDescriptorProto_MergeFrom_m9929A6BFE74508B75D3C84F099E0473C67499803(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void OneofDescriptorProto_t6319E05084FB5AA0644D3AEF37891689AFF1DD9A_CustomAttributesCacheGenerator_OneofDescriptorProto_MergeFrom_m7E5A8F79E11BF5CC5DC28F66E56916BE9C185511(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void OneofDescriptorProto_t6319E05084FB5AA0644D3AEF37891689AFF1DD9A_CustomAttributesCacheGenerator_OneofDescriptorProto_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_mDF43CF284022D2519858B4865D13C83C21F249A4(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void OneofDescriptorProto_t6319E05084FB5AA0644D3AEF37891689AFF1DD9A_CustomAttributesCacheGenerator_OneofDescriptorProto_t6319E05084FB5AA0644D3AEF37891689AFF1DD9A____Parser_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void OneofDescriptorProto_t6319E05084FB5AA0644D3AEF37891689AFF1DD9A_CustomAttributesCacheGenerator_OneofDescriptorProto_t6319E05084FB5AA0644D3AEF37891689AFF1DD9A____Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void OneofDescriptorProto_t6319E05084FB5AA0644D3AEF37891689AFF1DD9A_CustomAttributesCacheGenerator_OneofDescriptorProto_t6319E05084FB5AA0644D3AEF37891689AFF1DD9A____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void OneofDescriptorProto_t6319E05084FB5AA0644D3AEF37891689AFF1DD9A_CustomAttributesCacheGenerator_OneofDescriptorProto_t6319E05084FB5AA0644D3AEF37891689AFF1DD9A____Name_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void OneofDescriptorProto_t6319E05084FB5AA0644D3AEF37891689AFF1DD9A_CustomAttributesCacheGenerator_OneofDescriptorProto_t6319E05084FB5AA0644D3AEF37891689AFF1DD9A____HasName_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void OneofDescriptorProto_t6319E05084FB5AA0644D3AEF37891689AFF1DD9A_CustomAttributesCacheGenerator_OneofDescriptorProto_t6319E05084FB5AA0644D3AEF37891689AFF1DD9A____Options_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void U3CU3Ec_t6C0F3095056F0E5E4FE1BCCA0CD99FF73F8DC7A0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EnumDescriptorProto_t0E60F3628E51BC270FDA740DCBC83DBF04FA0478_CustomAttributesCacheGenerator_EnumDescriptorProto__ctor_mB3A28E8576A228315F116223DA06685ABD39550A(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumDescriptorProto_t0E60F3628E51BC270FDA740DCBC83DBF04FA0478_CustomAttributesCacheGenerator_EnumDescriptorProto__ctor_m147F600199E44F16A8C1AA7F6991895708CF4282(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumDescriptorProto_t0E60F3628E51BC270FDA740DCBC83DBF04FA0478_CustomAttributesCacheGenerator_EnumDescriptorProto_Clone_mB7008C506A35ACCFE0E5D36AAD55F49F98F6E85A(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumDescriptorProto_t0E60F3628E51BC270FDA740DCBC83DBF04FA0478_CustomAttributesCacheGenerator_EnumDescriptorProto_Equals_m6711ABED6B0C4EC1496AAE697AA452084CB3546B(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumDescriptorProto_t0E60F3628E51BC270FDA740DCBC83DBF04FA0478_CustomAttributesCacheGenerator_EnumDescriptorProto_Equals_m22E80DDF1828BF67B70CDC86776D054CB05F6F23(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumDescriptorProto_t0E60F3628E51BC270FDA740DCBC83DBF04FA0478_CustomAttributesCacheGenerator_EnumDescriptorProto_GetHashCode_m7EACA529EA0BFE654F625A89C93F046781EE0F9F(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumDescriptorProto_t0E60F3628E51BC270FDA740DCBC83DBF04FA0478_CustomAttributesCacheGenerator_EnumDescriptorProto_ToString_m4FB571E200CAA7F881910FBEB583E511661A8340(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumDescriptorProto_t0E60F3628E51BC270FDA740DCBC83DBF04FA0478_CustomAttributesCacheGenerator_EnumDescriptorProto_WriteTo_m9D12A0C8024AA0DA17AE32C6782D2930703EEAE4(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumDescriptorProto_t0E60F3628E51BC270FDA740DCBC83DBF04FA0478_CustomAttributesCacheGenerator_EnumDescriptorProto_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m14AEC404CD6EFADDE5EBA504F48275D2AA0D66F7(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumDescriptorProto_t0E60F3628E51BC270FDA740DCBC83DBF04FA0478_CustomAttributesCacheGenerator_EnumDescriptorProto_CalculateSize_m9F366B59267B5766D2E4B89E64EE02E653AAF2E0(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumDescriptorProto_t0E60F3628E51BC270FDA740DCBC83DBF04FA0478_CustomAttributesCacheGenerator_EnumDescriptorProto_MergeFrom_mE277EA218CDEAB5A29709DA28ED6FA1B6E7A1CCC(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumDescriptorProto_t0E60F3628E51BC270FDA740DCBC83DBF04FA0478_CustomAttributesCacheGenerator_EnumDescriptorProto_MergeFrom_mD7331F6048B5F3A9CBD9BCE98EC931224970178E(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumDescriptorProto_t0E60F3628E51BC270FDA740DCBC83DBF04FA0478_CustomAttributesCacheGenerator_EnumDescriptorProto_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_mEEA70B246D0C85DA555958273DC1F8F2F977F7CF(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumDescriptorProto_t0E60F3628E51BC270FDA740DCBC83DBF04FA0478_CustomAttributesCacheGenerator_EnumDescriptorProto_t0E60F3628E51BC270FDA740DCBC83DBF04FA0478____Parser_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumDescriptorProto_t0E60F3628E51BC270FDA740DCBC83DBF04FA0478_CustomAttributesCacheGenerator_EnumDescriptorProto_t0E60F3628E51BC270FDA740DCBC83DBF04FA0478____Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumDescriptorProto_t0E60F3628E51BC270FDA740DCBC83DBF04FA0478_CustomAttributesCacheGenerator_EnumDescriptorProto_t0E60F3628E51BC270FDA740DCBC83DBF04FA0478____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumDescriptorProto_t0E60F3628E51BC270FDA740DCBC83DBF04FA0478_CustomAttributesCacheGenerator_EnumDescriptorProto_t0E60F3628E51BC270FDA740DCBC83DBF04FA0478____Name_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumDescriptorProto_t0E60F3628E51BC270FDA740DCBC83DBF04FA0478_CustomAttributesCacheGenerator_EnumDescriptorProto_t0E60F3628E51BC270FDA740DCBC83DBF04FA0478____HasName_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumDescriptorProto_t0E60F3628E51BC270FDA740DCBC83DBF04FA0478_CustomAttributesCacheGenerator_EnumDescriptorProto_t0E60F3628E51BC270FDA740DCBC83DBF04FA0478____Value_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumDescriptorProto_t0E60F3628E51BC270FDA740DCBC83DBF04FA0478_CustomAttributesCacheGenerator_EnumDescriptorProto_t0E60F3628E51BC270FDA740DCBC83DBF04FA0478____Options_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Types_tCC3FD4FD7253BA6D7E02876756E671E1AF1F64DF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41_CustomAttributesCacheGenerator_EnumReservedRange__ctor_m02904F51FF5FE5B92BAA1E816D3A1B13534A6E0C(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41_CustomAttributesCacheGenerator_EnumReservedRange__ctor_m4F7BB9BB1007668742F085BDA2A82E889F6D1B6A(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41_CustomAttributesCacheGenerator_EnumReservedRange_Clone_m0478CEBDD1CC260098613EC9608FA8DA619521A4(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41_CustomAttributesCacheGenerator_EnumReservedRange_Equals_m3728912F8E0AD0EA5A74FD69323CF9F73465DA04(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41_CustomAttributesCacheGenerator_EnumReservedRange_Equals_m1FB9B43A21E9D232DFE20485F50C4673D84948E1(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41_CustomAttributesCacheGenerator_EnumReservedRange_GetHashCode_m6415E55F96EE5B92BBE2965CD1FE5D37C2604231(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41_CustomAttributesCacheGenerator_EnumReservedRange_ToString_mA9995D4DB4A84520B09678054B780C0714C18E32(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41_CustomAttributesCacheGenerator_EnumReservedRange_WriteTo_m7D1FE13C055716582D6BC6B24B10AE1654DA1316(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41_CustomAttributesCacheGenerator_EnumReservedRange_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m40335111DC4E167612FEF3662BE9124300D3480A(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41_CustomAttributesCacheGenerator_EnumReservedRange_CalculateSize_m0A62C551FDA64908B0C58397CD1428C5C71BF2F5(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41_CustomAttributesCacheGenerator_EnumReservedRange_MergeFrom_m2FC3A667F2AF4C85C2C7D3AF0E4E471D92019CD2(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41_CustomAttributesCacheGenerator_EnumReservedRange_MergeFrom_mCD0205C8ED2CEB21A1C6F16D967019975292C0A0(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41_CustomAttributesCacheGenerator_EnumReservedRange_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_mDCF2D669D389A95648C8864C743EBF87D6CAC659(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41_CustomAttributesCacheGenerator_EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41____Parser_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41_CustomAttributesCacheGenerator_EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41____Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41_CustomAttributesCacheGenerator_EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41_CustomAttributesCacheGenerator_EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41____Start_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41_CustomAttributesCacheGenerator_EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41____HasStart_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41_CustomAttributesCacheGenerator_EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41____End_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41_CustomAttributesCacheGenerator_EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41____HasEnd_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void U3CU3Ec_t0FB8D11271B41B39C63CE8A142DE7F8F87630794_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_tD1D7ABD9623694573682F9D85408033855EA825C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EnumValueDescriptorProto_t24F4825414596CE28D7979C4F352A8EE30AE8315_CustomAttributesCacheGenerator_EnumValueDescriptorProto__ctor_mCD6381E84158229867FFB9D4C43817B859E9A183(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumValueDescriptorProto_t24F4825414596CE28D7979C4F352A8EE30AE8315_CustomAttributesCacheGenerator_EnumValueDescriptorProto__ctor_m0E2CA6496FBAE77FB5E1F05802586BD4BE3A3D3D(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumValueDescriptorProto_t24F4825414596CE28D7979C4F352A8EE30AE8315_CustomAttributesCacheGenerator_EnumValueDescriptorProto_Clone_m1611AE2A72549FFE60B8119A55D815BA696E862D(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumValueDescriptorProto_t24F4825414596CE28D7979C4F352A8EE30AE8315_CustomAttributesCacheGenerator_EnumValueDescriptorProto_Equals_mAEBB3D490CDECFB762189684B4B64227E2759C07(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumValueDescriptorProto_t24F4825414596CE28D7979C4F352A8EE30AE8315_CustomAttributesCacheGenerator_EnumValueDescriptorProto_Equals_m4676E76A2633C7C43EA50E9A532A5CFBAA32C1BF(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumValueDescriptorProto_t24F4825414596CE28D7979C4F352A8EE30AE8315_CustomAttributesCacheGenerator_EnumValueDescriptorProto_GetHashCode_mB2267753F8624960C3BE6FEF07DE4F221E82E7D0(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumValueDescriptorProto_t24F4825414596CE28D7979C4F352A8EE30AE8315_CustomAttributesCacheGenerator_EnumValueDescriptorProto_ToString_m2A605BE66BAB0ADDB88DF7F6D39D38E4AE30E399(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumValueDescriptorProto_t24F4825414596CE28D7979C4F352A8EE30AE8315_CustomAttributesCacheGenerator_EnumValueDescriptorProto_WriteTo_m628970DB0F15CF7E6A2C600B877D999E92B0AF30(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumValueDescriptorProto_t24F4825414596CE28D7979C4F352A8EE30AE8315_CustomAttributesCacheGenerator_EnumValueDescriptorProto_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m742B37B6C10FF627FF03157232ABA74C1918CEA7(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumValueDescriptorProto_t24F4825414596CE28D7979C4F352A8EE30AE8315_CustomAttributesCacheGenerator_EnumValueDescriptorProto_CalculateSize_m1F7A660E62D7576C260035086428BF3452D89C36(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumValueDescriptorProto_t24F4825414596CE28D7979C4F352A8EE30AE8315_CustomAttributesCacheGenerator_EnumValueDescriptorProto_MergeFrom_mC16859FC588043BE69253243292C173544C0BC14(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumValueDescriptorProto_t24F4825414596CE28D7979C4F352A8EE30AE8315_CustomAttributesCacheGenerator_EnumValueDescriptorProto_MergeFrom_m415B9988016FE0741689B94605032A8CD4033362(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumValueDescriptorProto_t24F4825414596CE28D7979C4F352A8EE30AE8315_CustomAttributesCacheGenerator_EnumValueDescriptorProto_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m586D380CAEE945088D1FDBB1C08489F63B823E5D(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumValueDescriptorProto_t24F4825414596CE28D7979C4F352A8EE30AE8315_CustomAttributesCacheGenerator_EnumValueDescriptorProto_t24F4825414596CE28D7979C4F352A8EE30AE8315____Parser_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumValueDescriptorProto_t24F4825414596CE28D7979C4F352A8EE30AE8315_CustomAttributesCacheGenerator_EnumValueDescriptorProto_t24F4825414596CE28D7979C4F352A8EE30AE8315____Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumValueDescriptorProto_t24F4825414596CE28D7979C4F352A8EE30AE8315_CustomAttributesCacheGenerator_EnumValueDescriptorProto_t24F4825414596CE28D7979C4F352A8EE30AE8315____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumValueDescriptorProto_t24F4825414596CE28D7979C4F352A8EE30AE8315_CustomAttributesCacheGenerator_EnumValueDescriptorProto_t24F4825414596CE28D7979C4F352A8EE30AE8315____Name_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumValueDescriptorProto_t24F4825414596CE28D7979C4F352A8EE30AE8315_CustomAttributesCacheGenerator_EnumValueDescriptorProto_t24F4825414596CE28D7979C4F352A8EE30AE8315____HasName_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumValueDescriptorProto_t24F4825414596CE28D7979C4F352A8EE30AE8315_CustomAttributesCacheGenerator_EnumValueDescriptorProto_t24F4825414596CE28D7979C4F352A8EE30AE8315____Number_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumValueDescriptorProto_t24F4825414596CE28D7979C4F352A8EE30AE8315_CustomAttributesCacheGenerator_EnumValueDescriptorProto_t24F4825414596CE28D7979C4F352A8EE30AE8315____HasNumber_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumValueDescriptorProto_t24F4825414596CE28D7979C4F352A8EE30AE8315_CustomAttributesCacheGenerator_EnumValueDescriptorProto_t24F4825414596CE28D7979C4F352A8EE30AE8315____Options_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void U3CU3Ec_t79691CCE8A4B48B45898130F70DF802BAFBCF800_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ServiceDescriptorProto_t39FE93732D4E2D7AA71D61E86B211AAADA573DB3_CustomAttributesCacheGenerator_ServiceDescriptorProto__ctor_m2ABEEBBB877E11DC2EB775785478B01367CEFDC5(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ServiceDescriptorProto_t39FE93732D4E2D7AA71D61E86B211AAADA573DB3_CustomAttributesCacheGenerator_ServiceDescriptorProto__ctor_mF76948802201FEE2ACD8D67A55B5C75EBFF5FDF1(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ServiceDescriptorProto_t39FE93732D4E2D7AA71D61E86B211AAADA573DB3_CustomAttributesCacheGenerator_ServiceDescriptorProto_Clone_mE601AB9C3641148DB0AC9C3B6BD5F3281DC57109(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ServiceDescriptorProto_t39FE93732D4E2D7AA71D61E86B211AAADA573DB3_CustomAttributesCacheGenerator_ServiceDescriptorProto_Equals_m5C75FE57DE6DE100164657FB1129840A9B9489AB(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ServiceDescriptorProto_t39FE93732D4E2D7AA71D61E86B211AAADA573DB3_CustomAttributesCacheGenerator_ServiceDescriptorProto_Equals_mFBE6BA3EC312B0C6F693C79A2D723347CD013C55(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ServiceDescriptorProto_t39FE93732D4E2D7AA71D61E86B211AAADA573DB3_CustomAttributesCacheGenerator_ServiceDescriptorProto_GetHashCode_m7BA75A38679BF8C53EA10F293060F72BFDC46396(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ServiceDescriptorProto_t39FE93732D4E2D7AA71D61E86B211AAADA573DB3_CustomAttributesCacheGenerator_ServiceDescriptorProto_ToString_m97875482BD469952825E67CBE1ACB629241E775A(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ServiceDescriptorProto_t39FE93732D4E2D7AA71D61E86B211AAADA573DB3_CustomAttributesCacheGenerator_ServiceDescriptorProto_WriteTo_m4D6E98E4F13566154B1EBFB0309D2DF3A4B71A38(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ServiceDescriptorProto_t39FE93732D4E2D7AA71D61E86B211AAADA573DB3_CustomAttributesCacheGenerator_ServiceDescriptorProto_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m4E48FC87B5F4637449255AF403AAF0971BD9FF91(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ServiceDescriptorProto_t39FE93732D4E2D7AA71D61E86B211AAADA573DB3_CustomAttributesCacheGenerator_ServiceDescriptorProto_CalculateSize_mFD1610268B60E21FDA5CA531B00065C1E4A6AB8E(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ServiceDescriptorProto_t39FE93732D4E2D7AA71D61E86B211AAADA573DB3_CustomAttributesCacheGenerator_ServiceDescriptorProto_MergeFrom_m6581ECAC1AF5ECDFFCDEC9A551D9F71BCEC0005B(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ServiceDescriptorProto_t39FE93732D4E2D7AA71D61E86B211AAADA573DB3_CustomAttributesCacheGenerator_ServiceDescriptorProto_MergeFrom_mA53EA15C18F1199B95F5F1DF3AD52A81F6E5AB7E(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ServiceDescriptorProto_t39FE93732D4E2D7AA71D61E86B211AAADA573DB3_CustomAttributesCacheGenerator_ServiceDescriptorProto_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m70DB24C4CDA1363FF025EA7F7CBE3449C1A4C49C(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ServiceDescriptorProto_t39FE93732D4E2D7AA71D61E86B211AAADA573DB3_CustomAttributesCacheGenerator_ServiceDescriptorProto_t39FE93732D4E2D7AA71D61E86B211AAADA573DB3____Parser_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ServiceDescriptorProto_t39FE93732D4E2D7AA71D61E86B211AAADA573DB3_CustomAttributesCacheGenerator_ServiceDescriptorProto_t39FE93732D4E2D7AA71D61E86B211AAADA573DB3____Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ServiceDescriptorProto_t39FE93732D4E2D7AA71D61E86B211AAADA573DB3_CustomAttributesCacheGenerator_ServiceDescriptorProto_t39FE93732D4E2D7AA71D61E86B211AAADA573DB3____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ServiceDescriptorProto_t39FE93732D4E2D7AA71D61E86B211AAADA573DB3_CustomAttributesCacheGenerator_ServiceDescriptorProto_t39FE93732D4E2D7AA71D61E86B211AAADA573DB3____Name_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ServiceDescriptorProto_t39FE93732D4E2D7AA71D61E86B211AAADA573DB3_CustomAttributesCacheGenerator_ServiceDescriptorProto_t39FE93732D4E2D7AA71D61E86B211AAADA573DB3____HasName_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ServiceDescriptorProto_t39FE93732D4E2D7AA71D61E86B211AAADA573DB3_CustomAttributesCacheGenerator_ServiceDescriptorProto_t39FE93732D4E2D7AA71D61E86B211AAADA573DB3____Method_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ServiceDescriptorProto_t39FE93732D4E2D7AA71D61E86B211AAADA573DB3_CustomAttributesCacheGenerator_ServiceDescriptorProto_t39FE93732D4E2D7AA71D61E86B211AAADA573DB3____Options_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void U3CU3Ec_t58AC09743BD6215CAA60D27EDAD693FE4CE51008_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MethodDescriptorProto_t88EE2E0AFE0DEEDDD27A1DAFA0451A259924D638_CustomAttributesCacheGenerator_MethodDescriptorProto__ctor_m3181F7006234380068049FEDB5E800625B8193C8(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MethodDescriptorProto_t88EE2E0AFE0DEEDDD27A1DAFA0451A259924D638_CustomAttributesCacheGenerator_MethodDescriptorProto__ctor_mEF5E8090FBE2C5FB52278A283B26A67284FBD781(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MethodDescriptorProto_t88EE2E0AFE0DEEDDD27A1DAFA0451A259924D638_CustomAttributesCacheGenerator_MethodDescriptorProto_Clone_mA90669FE9E8DE792CEC9323E367F334926F95A21(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MethodDescriptorProto_t88EE2E0AFE0DEEDDD27A1DAFA0451A259924D638_CustomAttributesCacheGenerator_MethodDescriptorProto_Equals_mE404C60BAAC311AF6BC9E81457AE4901A2EC69F1(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MethodDescriptorProto_t88EE2E0AFE0DEEDDD27A1DAFA0451A259924D638_CustomAttributesCacheGenerator_MethodDescriptorProto_Equals_m15C523245227F5007A8438DA67437535DADCA8A0(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MethodDescriptorProto_t88EE2E0AFE0DEEDDD27A1DAFA0451A259924D638_CustomAttributesCacheGenerator_MethodDescriptorProto_GetHashCode_mBE5452AA6B4265114D0C57FDC8BC4496C01211CB(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MethodDescriptorProto_t88EE2E0AFE0DEEDDD27A1DAFA0451A259924D638_CustomAttributesCacheGenerator_MethodDescriptorProto_ToString_mF5CA9EB9E35C30D1141628CDA8D36F3183524337(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MethodDescriptorProto_t88EE2E0AFE0DEEDDD27A1DAFA0451A259924D638_CustomAttributesCacheGenerator_MethodDescriptorProto_WriteTo_m81FE666F3F825DFE70F6A79A3E8EC18E54F3CBBA(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MethodDescriptorProto_t88EE2E0AFE0DEEDDD27A1DAFA0451A259924D638_CustomAttributesCacheGenerator_MethodDescriptorProto_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m3D279F4D543CF3AEDDA8A726D1DDE892CF58D8D2(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MethodDescriptorProto_t88EE2E0AFE0DEEDDD27A1DAFA0451A259924D638_CustomAttributesCacheGenerator_MethodDescriptorProto_CalculateSize_mD02B74F49B50CFCB0A8504ACE320C81D1815D622(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MethodDescriptorProto_t88EE2E0AFE0DEEDDD27A1DAFA0451A259924D638_CustomAttributesCacheGenerator_MethodDescriptorProto_MergeFrom_m2B10CD2A53C04E575D652B444AF50326300A36C6(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MethodDescriptorProto_t88EE2E0AFE0DEEDDD27A1DAFA0451A259924D638_CustomAttributesCacheGenerator_MethodDescriptorProto_MergeFrom_mB331EAAF45C06DE2158A063B329836049B00650A(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MethodDescriptorProto_t88EE2E0AFE0DEEDDD27A1DAFA0451A259924D638_CustomAttributesCacheGenerator_MethodDescriptorProto_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_mEEC631A9308E9E0931247AD1FC67A84AD79F7E88(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MethodDescriptorProto_t88EE2E0AFE0DEEDDD27A1DAFA0451A259924D638_CustomAttributesCacheGenerator_MethodDescriptorProto_t88EE2E0AFE0DEEDDD27A1DAFA0451A259924D638____Parser_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MethodDescriptorProto_t88EE2E0AFE0DEEDDD27A1DAFA0451A259924D638_CustomAttributesCacheGenerator_MethodDescriptorProto_t88EE2E0AFE0DEEDDD27A1DAFA0451A259924D638____Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MethodDescriptorProto_t88EE2E0AFE0DEEDDD27A1DAFA0451A259924D638_CustomAttributesCacheGenerator_MethodDescriptorProto_t88EE2E0AFE0DEEDDD27A1DAFA0451A259924D638____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MethodDescriptorProto_t88EE2E0AFE0DEEDDD27A1DAFA0451A259924D638_CustomAttributesCacheGenerator_MethodDescriptorProto_t88EE2E0AFE0DEEDDD27A1DAFA0451A259924D638____Name_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MethodDescriptorProto_t88EE2E0AFE0DEEDDD27A1DAFA0451A259924D638_CustomAttributesCacheGenerator_MethodDescriptorProto_t88EE2E0AFE0DEEDDD27A1DAFA0451A259924D638____HasName_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MethodDescriptorProto_t88EE2E0AFE0DEEDDD27A1DAFA0451A259924D638_CustomAttributesCacheGenerator_MethodDescriptorProto_t88EE2E0AFE0DEEDDD27A1DAFA0451A259924D638____InputType_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MethodDescriptorProto_t88EE2E0AFE0DEEDDD27A1DAFA0451A259924D638_CustomAttributesCacheGenerator_MethodDescriptorProto_t88EE2E0AFE0DEEDDD27A1DAFA0451A259924D638____HasInputType_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MethodDescriptorProto_t88EE2E0AFE0DEEDDD27A1DAFA0451A259924D638_CustomAttributesCacheGenerator_MethodDescriptorProto_t88EE2E0AFE0DEEDDD27A1DAFA0451A259924D638____OutputType_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MethodDescriptorProto_t88EE2E0AFE0DEEDDD27A1DAFA0451A259924D638_CustomAttributesCacheGenerator_MethodDescriptorProto_t88EE2E0AFE0DEEDDD27A1DAFA0451A259924D638____HasOutputType_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MethodDescriptorProto_t88EE2E0AFE0DEEDDD27A1DAFA0451A259924D638_CustomAttributesCacheGenerator_MethodDescriptorProto_t88EE2E0AFE0DEEDDD27A1DAFA0451A259924D638____Options_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MethodDescriptorProto_t88EE2E0AFE0DEEDDD27A1DAFA0451A259924D638_CustomAttributesCacheGenerator_MethodDescriptorProto_t88EE2E0AFE0DEEDDD27A1DAFA0451A259924D638____ClientStreaming_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MethodDescriptorProto_t88EE2E0AFE0DEEDDD27A1DAFA0451A259924D638_CustomAttributesCacheGenerator_MethodDescriptorProto_t88EE2E0AFE0DEEDDD27A1DAFA0451A259924D638____HasClientStreaming_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MethodDescriptorProto_t88EE2E0AFE0DEEDDD27A1DAFA0451A259924D638_CustomAttributesCacheGenerator_MethodDescriptorProto_t88EE2E0AFE0DEEDDD27A1DAFA0451A259924D638____ServerStreaming_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MethodDescriptorProto_t88EE2E0AFE0DEEDDD27A1DAFA0451A259924D638_CustomAttributesCacheGenerator_MethodDescriptorProto_t88EE2E0AFE0DEEDDD27A1DAFA0451A259924D638____HasServerStreaming_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void U3CU3Ec_tB84B815A20EC05C80390E84E48C3928E16E92F53_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions__ctor_m53E8E18F45026B68293C1D6E27ADD3C809A81E75(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions__ctor_mE14316BC374AEFD5371F0306BB87D7635BE51612(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_Clone_m4AE4FD9F2AA9ACE2E4FEA61EF8CE2996995E6D79(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_Equals_m269B71EC6513379C07694356E74F494902C8BCCA(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_Equals_m9A68AC5269CD93866D6B3AE6103426CA0B62C75F(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_GetHashCode_mC6302B4BBD76B6B1B81E42621DC3B414AFBB2093(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_ToString_m1F624CC78700C658ED7BCFCFC64F1932A9DE8591(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_WriteTo_m46AC0C3DC74648185755CF83BB96E3097A430512(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_mE7D6ECEA3BB13F301585BA7374B00F84DFE61CC7(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_CalculateSize_m1CBAF20ABA4BCFD4FAC533D9EDF18F7C65AF1105(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_MergeFrom_m602802909D9E87D110BEA1BB3D6ECDA93F9030E6(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_MergeFrom_mD0E3ECF95A4D41396D5A6CD667943E22F2D163AC(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m9A84CB596D5C261B6CD9973D22690134C98313FC(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30____Parser_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30____Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30____JavaPackage_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30____HasJavaPackage_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30____JavaOuterClassname_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30____HasJavaOuterClassname_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30____JavaMultipleFiles_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30____HasJavaMultipleFiles_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30____JavaGenerateEqualsAndHash_PropertyInfo(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * tmp = (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 *)cache->attributes[0];
		ObsoleteAttribute__ctor_m9BC17A80675E9013AA71F9FB38D89FEF56883853(tmp, NULL);
	}
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[1];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30____HasJavaGenerateEqualsAndHash_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
	{
		ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * tmp = (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 *)cache->attributes[1];
		ObsoleteAttribute__ctor_m9BC17A80675E9013AA71F9FB38D89FEF56883853(tmp, NULL);
	}
}
static void FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30____JavaStringCheckUtf8_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30____HasJavaStringCheckUtf8_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30____OptimizeFor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30____HasOptimizeFor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30____GoPackage_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30____HasGoPackage_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30____CcGenericServices_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30____HasCcGenericServices_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30____JavaGenericServices_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30____HasJavaGenericServices_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30____PyGenericServices_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30____HasPyGenericServices_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30____PhpGenericServices_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30____HasPhpGenericServices_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30____Deprecated_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30____HasDeprecated_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30____CcEnableArenas_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30____HasCcEnableArenas_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30____ObjcClassPrefix_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30____HasObjcClassPrefix_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30____CsharpNamespace_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30____HasCsharpNamespace_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30____SwiftPrefix_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30____HasSwiftPrefix_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30____PhpClassPrefix_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30____HasPhpClassPrefix_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30____PhpNamespace_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30____HasPhpNamespace_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30____PhpMetadataNamespace_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30____HasPhpMetadataNamespace_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30____RubyPackage_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30____HasRubyPackage_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Types_t60A4AC26614D017A3BEF3B1A7EC099FCC2E6511B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void OptimizeMode_tD2A7B67F01AF2A53A5C73E35E3B0AD381FBEAF90_CustomAttributesCacheGenerator_Speed(CustomAttributesCache* cache)
{
	{
		OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * tmp = (OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 *)cache->attributes[0];
		OriginalNameAttribute__ctor_mB0C6C38C49B9261D2DC423D692E80B8EBC657BF9(tmp, il2cpp_codegen_string_new_wrapper("\x53\x50\x45\x45\x44"), NULL);
	}
}
static void OptimizeMode_tD2A7B67F01AF2A53A5C73E35E3B0AD381FBEAF90_CustomAttributesCacheGenerator_CodeSize(CustomAttributesCache* cache)
{
	{
		OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * tmp = (OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 *)cache->attributes[0];
		OriginalNameAttribute__ctor_mB0C6C38C49B9261D2DC423D692E80B8EBC657BF9(tmp, il2cpp_codegen_string_new_wrapper("\x43\x4F\x44\x45\x5F\x53\x49\x5A\x45"), NULL);
	}
}
static void OptimizeMode_tD2A7B67F01AF2A53A5C73E35E3B0AD381FBEAF90_CustomAttributesCacheGenerator_LiteRuntime(CustomAttributesCache* cache)
{
	{
		OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * tmp = (OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 *)cache->attributes[0];
		OriginalNameAttribute__ctor_mB0C6C38C49B9261D2DC423D692E80B8EBC657BF9(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x49\x54\x45\x5F\x52\x55\x4E\x54\x49\x4D\x45"), NULL);
	}
}
static void U3CU3Ec_t04CDD685CEDF47C4A32C6D4A77AD0CD5E61B14D7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MessageOptions_t007E64DA64D7132536A101E9AA32FBFE31FFADBD_CustomAttributesCacheGenerator_MessageOptions__ctor_m04D30C43D35878336875570DC6A5E3E6BC862DC8(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MessageOptions_t007E64DA64D7132536A101E9AA32FBFE31FFADBD_CustomAttributesCacheGenerator_MessageOptions__ctor_m624EB105BF4DBD4102C96FBB4E33A6298249DADD(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MessageOptions_t007E64DA64D7132536A101E9AA32FBFE31FFADBD_CustomAttributesCacheGenerator_MessageOptions_Clone_m145FE6784EC0872E03A2A8F80ED305D98CA4B885(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MessageOptions_t007E64DA64D7132536A101E9AA32FBFE31FFADBD_CustomAttributesCacheGenerator_MessageOptions_Equals_m1B9DA96523A1EBF257B4C05943A355ABDEF8D4A3(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MessageOptions_t007E64DA64D7132536A101E9AA32FBFE31FFADBD_CustomAttributesCacheGenerator_MessageOptions_Equals_m140F555A06D1A7D663A75E01F530CE9504F73BCB(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MessageOptions_t007E64DA64D7132536A101E9AA32FBFE31FFADBD_CustomAttributesCacheGenerator_MessageOptions_GetHashCode_m5FB4F16D0E258B4EA5B190B1FC9BA4B92CE54A0D(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MessageOptions_t007E64DA64D7132536A101E9AA32FBFE31FFADBD_CustomAttributesCacheGenerator_MessageOptions_ToString_mD783887EDD978433A31E4DDCDF8A1F73C927BE1D(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MessageOptions_t007E64DA64D7132536A101E9AA32FBFE31FFADBD_CustomAttributesCacheGenerator_MessageOptions_WriteTo_mB3320A7754F94A90E6A9DC30D4FE1D3E6D933849(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MessageOptions_t007E64DA64D7132536A101E9AA32FBFE31FFADBD_CustomAttributesCacheGenerator_MessageOptions_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m47F229E21C08BCE7F2BE882F5811ACBBBF789A13(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MessageOptions_t007E64DA64D7132536A101E9AA32FBFE31FFADBD_CustomAttributesCacheGenerator_MessageOptions_CalculateSize_mC3FBEC0AC2FF4F8C44D2E5F8387DF31F177936A3(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MessageOptions_t007E64DA64D7132536A101E9AA32FBFE31FFADBD_CustomAttributesCacheGenerator_MessageOptions_MergeFrom_m79A658F6D709B1A47BF52BDFE737C37609FCC755(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MessageOptions_t007E64DA64D7132536A101E9AA32FBFE31FFADBD_CustomAttributesCacheGenerator_MessageOptions_MergeFrom_m3DF6E1BE37AE3869439B0A6FA4DA0BE58835014F(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MessageOptions_t007E64DA64D7132536A101E9AA32FBFE31FFADBD_CustomAttributesCacheGenerator_MessageOptions_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m3009CFC09F4F10050F4E663DD16E6151A0A3F556(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MessageOptions_t007E64DA64D7132536A101E9AA32FBFE31FFADBD_CustomAttributesCacheGenerator_MessageOptions_t007E64DA64D7132536A101E9AA32FBFE31FFADBD____Parser_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MessageOptions_t007E64DA64D7132536A101E9AA32FBFE31FFADBD_CustomAttributesCacheGenerator_MessageOptions_t007E64DA64D7132536A101E9AA32FBFE31FFADBD____Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MessageOptions_t007E64DA64D7132536A101E9AA32FBFE31FFADBD_CustomAttributesCacheGenerator_MessageOptions_t007E64DA64D7132536A101E9AA32FBFE31FFADBD____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MessageOptions_t007E64DA64D7132536A101E9AA32FBFE31FFADBD_CustomAttributesCacheGenerator_MessageOptions_t007E64DA64D7132536A101E9AA32FBFE31FFADBD____MessageSetWireFormat_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MessageOptions_t007E64DA64D7132536A101E9AA32FBFE31FFADBD_CustomAttributesCacheGenerator_MessageOptions_t007E64DA64D7132536A101E9AA32FBFE31FFADBD____HasMessageSetWireFormat_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MessageOptions_t007E64DA64D7132536A101E9AA32FBFE31FFADBD_CustomAttributesCacheGenerator_MessageOptions_t007E64DA64D7132536A101E9AA32FBFE31FFADBD____NoStandardDescriptorAccessor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MessageOptions_t007E64DA64D7132536A101E9AA32FBFE31FFADBD_CustomAttributesCacheGenerator_MessageOptions_t007E64DA64D7132536A101E9AA32FBFE31FFADBD____HasNoStandardDescriptorAccessor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MessageOptions_t007E64DA64D7132536A101E9AA32FBFE31FFADBD_CustomAttributesCacheGenerator_MessageOptions_t007E64DA64D7132536A101E9AA32FBFE31FFADBD____Deprecated_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MessageOptions_t007E64DA64D7132536A101E9AA32FBFE31FFADBD_CustomAttributesCacheGenerator_MessageOptions_t007E64DA64D7132536A101E9AA32FBFE31FFADBD____HasDeprecated_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MessageOptions_t007E64DA64D7132536A101E9AA32FBFE31FFADBD_CustomAttributesCacheGenerator_MessageOptions_t007E64DA64D7132536A101E9AA32FBFE31FFADBD____MapEntry_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MessageOptions_t007E64DA64D7132536A101E9AA32FBFE31FFADBD_CustomAttributesCacheGenerator_MessageOptions_t007E64DA64D7132536A101E9AA32FBFE31FFADBD____HasMapEntry_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void U3CU3Ec_t7F53D6B7CEDEDD933BBD477D57B941004F7AABDB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FieldOptions_t3B2D61638B0B06A886A821421DA06C09E4A78BEA_CustomAttributesCacheGenerator_FieldOptions__ctor_mAEDAAFD3E1E1476831562F5E223B4AF2F806D42E(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FieldOptions_t3B2D61638B0B06A886A821421DA06C09E4A78BEA_CustomAttributesCacheGenerator_FieldOptions__ctor_mEBACF18F7CD68BE7590F93E5181DEB9E8D8FB13C(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FieldOptions_t3B2D61638B0B06A886A821421DA06C09E4A78BEA_CustomAttributesCacheGenerator_FieldOptions_Clone_m2B5511B42C346DF381B638EBDD946CF86BA411F3(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FieldOptions_t3B2D61638B0B06A886A821421DA06C09E4A78BEA_CustomAttributesCacheGenerator_FieldOptions_Equals_m35E10FF061C593610F69C8A497CBBB5345B641DA(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FieldOptions_t3B2D61638B0B06A886A821421DA06C09E4A78BEA_CustomAttributesCacheGenerator_FieldOptions_Equals_mC82884DE34A33E7799B4B537DB3E5327FFF93DE8(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FieldOptions_t3B2D61638B0B06A886A821421DA06C09E4A78BEA_CustomAttributesCacheGenerator_FieldOptions_GetHashCode_m2C8BB8DAB5A6D69F940B9714BAAF1632D551814B(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FieldOptions_t3B2D61638B0B06A886A821421DA06C09E4A78BEA_CustomAttributesCacheGenerator_FieldOptions_ToString_m9E9A624E4C63E7B5FB16D55551A2B3E1BCE409F5(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FieldOptions_t3B2D61638B0B06A886A821421DA06C09E4A78BEA_CustomAttributesCacheGenerator_FieldOptions_WriteTo_mB619B555A1CF0FABD74BAB7646DAAEC68B206B4C(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FieldOptions_t3B2D61638B0B06A886A821421DA06C09E4A78BEA_CustomAttributesCacheGenerator_FieldOptions_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_mCE93A239A7677AE5873AAE5CB3E6F3979A60247D(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FieldOptions_t3B2D61638B0B06A886A821421DA06C09E4A78BEA_CustomAttributesCacheGenerator_FieldOptions_CalculateSize_m28F032BDF8603529AFE3FF729BF891A8B035A4F2(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FieldOptions_t3B2D61638B0B06A886A821421DA06C09E4A78BEA_CustomAttributesCacheGenerator_FieldOptions_MergeFrom_m78199E61A3B42CB84FC665C80A5C16C79E81E330(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FieldOptions_t3B2D61638B0B06A886A821421DA06C09E4A78BEA_CustomAttributesCacheGenerator_FieldOptions_MergeFrom_mC9159B562BFFE7A8E521072DCCD5F4D9E42043B4(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FieldOptions_t3B2D61638B0B06A886A821421DA06C09E4A78BEA_CustomAttributesCacheGenerator_FieldOptions_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m19D5BFCA155031073581BDEA524C8D1A712D5393(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FieldOptions_t3B2D61638B0B06A886A821421DA06C09E4A78BEA_CustomAttributesCacheGenerator_FieldOptions_t3B2D61638B0B06A886A821421DA06C09E4A78BEA____Parser_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FieldOptions_t3B2D61638B0B06A886A821421DA06C09E4A78BEA_CustomAttributesCacheGenerator_FieldOptions_t3B2D61638B0B06A886A821421DA06C09E4A78BEA____Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FieldOptions_t3B2D61638B0B06A886A821421DA06C09E4A78BEA_CustomAttributesCacheGenerator_FieldOptions_t3B2D61638B0B06A886A821421DA06C09E4A78BEA____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FieldOptions_t3B2D61638B0B06A886A821421DA06C09E4A78BEA_CustomAttributesCacheGenerator_FieldOptions_t3B2D61638B0B06A886A821421DA06C09E4A78BEA____Ctype_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FieldOptions_t3B2D61638B0B06A886A821421DA06C09E4A78BEA_CustomAttributesCacheGenerator_FieldOptions_t3B2D61638B0B06A886A821421DA06C09E4A78BEA____HasCtype_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FieldOptions_t3B2D61638B0B06A886A821421DA06C09E4A78BEA_CustomAttributesCacheGenerator_FieldOptions_t3B2D61638B0B06A886A821421DA06C09E4A78BEA____Packed_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FieldOptions_t3B2D61638B0B06A886A821421DA06C09E4A78BEA_CustomAttributesCacheGenerator_FieldOptions_t3B2D61638B0B06A886A821421DA06C09E4A78BEA____HasPacked_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FieldOptions_t3B2D61638B0B06A886A821421DA06C09E4A78BEA_CustomAttributesCacheGenerator_FieldOptions_t3B2D61638B0B06A886A821421DA06C09E4A78BEA____Jstype_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FieldOptions_t3B2D61638B0B06A886A821421DA06C09E4A78BEA_CustomAttributesCacheGenerator_FieldOptions_t3B2D61638B0B06A886A821421DA06C09E4A78BEA____HasJstype_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FieldOptions_t3B2D61638B0B06A886A821421DA06C09E4A78BEA_CustomAttributesCacheGenerator_FieldOptions_t3B2D61638B0B06A886A821421DA06C09E4A78BEA____Lazy_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FieldOptions_t3B2D61638B0B06A886A821421DA06C09E4A78BEA_CustomAttributesCacheGenerator_FieldOptions_t3B2D61638B0B06A886A821421DA06C09E4A78BEA____HasLazy_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FieldOptions_t3B2D61638B0B06A886A821421DA06C09E4A78BEA_CustomAttributesCacheGenerator_FieldOptions_t3B2D61638B0B06A886A821421DA06C09E4A78BEA____Deprecated_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FieldOptions_t3B2D61638B0B06A886A821421DA06C09E4A78BEA_CustomAttributesCacheGenerator_FieldOptions_t3B2D61638B0B06A886A821421DA06C09E4A78BEA____HasDeprecated_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FieldOptions_t3B2D61638B0B06A886A821421DA06C09E4A78BEA_CustomAttributesCacheGenerator_FieldOptions_t3B2D61638B0B06A886A821421DA06C09E4A78BEA____Weak_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FieldOptions_t3B2D61638B0B06A886A821421DA06C09E4A78BEA_CustomAttributesCacheGenerator_FieldOptions_t3B2D61638B0B06A886A821421DA06C09E4A78BEA____HasWeak_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Types_t4FB39A98BCC7CF65EABE43A578A985F625E4E136_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void CType_t8692E658D4DB9762025F3FF59D004E25835CF4FB_CustomAttributesCacheGenerator_String(CustomAttributesCache* cache)
{
	{
		OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * tmp = (OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 *)cache->attributes[0];
		OriginalNameAttribute__ctor_mB0C6C38C49B9261D2DC423D692E80B8EBC657BF9(tmp, il2cpp_codegen_string_new_wrapper("\x53\x54\x52\x49\x4E\x47"), NULL);
	}
}
static void CType_t8692E658D4DB9762025F3FF59D004E25835CF4FB_CustomAttributesCacheGenerator_Cord(CustomAttributesCache* cache)
{
	{
		OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * tmp = (OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 *)cache->attributes[0];
		OriginalNameAttribute__ctor_mB0C6C38C49B9261D2DC423D692E80B8EBC657BF9(tmp, il2cpp_codegen_string_new_wrapper("\x43\x4F\x52\x44"), NULL);
	}
}
static void CType_t8692E658D4DB9762025F3FF59D004E25835CF4FB_CustomAttributesCacheGenerator_StringPiece(CustomAttributesCache* cache)
{
	{
		OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * tmp = (OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 *)cache->attributes[0];
		OriginalNameAttribute__ctor_mB0C6C38C49B9261D2DC423D692E80B8EBC657BF9(tmp, il2cpp_codegen_string_new_wrapper("\x53\x54\x52\x49\x4E\x47\x5F\x50\x49\x45\x43\x45"), NULL);
	}
}
static void JSType_t6C0B15B541F17DFCB78C389DE391B514E6BCD78B_CustomAttributesCacheGenerator_JsNormal(CustomAttributesCache* cache)
{
	{
		OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * tmp = (OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 *)cache->attributes[0];
		OriginalNameAttribute__ctor_mB0C6C38C49B9261D2DC423D692E80B8EBC657BF9(tmp, il2cpp_codegen_string_new_wrapper("\x4A\x53\x5F\x4E\x4F\x52\x4D\x41\x4C"), NULL);
	}
}
static void JSType_t6C0B15B541F17DFCB78C389DE391B514E6BCD78B_CustomAttributesCacheGenerator_JsString(CustomAttributesCache* cache)
{
	{
		OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * tmp = (OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 *)cache->attributes[0];
		OriginalNameAttribute__ctor_mB0C6C38C49B9261D2DC423D692E80B8EBC657BF9(tmp, il2cpp_codegen_string_new_wrapper("\x4A\x53\x5F\x53\x54\x52\x49\x4E\x47"), NULL);
	}
}
static void JSType_t6C0B15B541F17DFCB78C389DE391B514E6BCD78B_CustomAttributesCacheGenerator_JsNumber(CustomAttributesCache* cache)
{
	{
		OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * tmp = (OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 *)cache->attributes[0];
		OriginalNameAttribute__ctor_mB0C6C38C49B9261D2DC423D692E80B8EBC657BF9(tmp, il2cpp_codegen_string_new_wrapper("\x4A\x53\x5F\x4E\x55\x4D\x42\x45\x52"), NULL);
	}
}
static void U3CU3Ec_t44A43453DEA21C6FB8BEAABE8A1433BCB9168244_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OneofOptions_tE3261CFC4DF0841400F23CBA8578D1191DE59AE7_CustomAttributesCacheGenerator_OneofOptions__ctor_mCB3DEA19D4B2D302F3960474C65B68869AE65A6C(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void OneofOptions_tE3261CFC4DF0841400F23CBA8578D1191DE59AE7_CustomAttributesCacheGenerator_OneofOptions__ctor_m87704EF2A87D953EEE82B1634AB6326C00D36690(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void OneofOptions_tE3261CFC4DF0841400F23CBA8578D1191DE59AE7_CustomAttributesCacheGenerator_OneofOptions_Clone_m861C126BB5E08AD41DFCC26AF082E5E53435DB55(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void OneofOptions_tE3261CFC4DF0841400F23CBA8578D1191DE59AE7_CustomAttributesCacheGenerator_OneofOptions_Equals_mF33E711DDF4A91CE5F2BB106B53D2AC4688D058B(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void OneofOptions_tE3261CFC4DF0841400F23CBA8578D1191DE59AE7_CustomAttributesCacheGenerator_OneofOptions_Equals_mD4943C6868C3350846C89F186BABEF1E024925B6(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void OneofOptions_tE3261CFC4DF0841400F23CBA8578D1191DE59AE7_CustomAttributesCacheGenerator_OneofOptions_GetHashCode_m9F0EBEE5FCA5AC438B7DDCF82E19623AE486A957(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void OneofOptions_tE3261CFC4DF0841400F23CBA8578D1191DE59AE7_CustomAttributesCacheGenerator_OneofOptions_ToString_m472557171AD5BB71572A06F3169E632E1322C621(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void OneofOptions_tE3261CFC4DF0841400F23CBA8578D1191DE59AE7_CustomAttributesCacheGenerator_OneofOptions_WriteTo_mA65BE0E9F2AE867975411486CE00F66754F593DB(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void OneofOptions_tE3261CFC4DF0841400F23CBA8578D1191DE59AE7_CustomAttributesCacheGenerator_OneofOptions_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m63942D15544A3D5AC7BEB303FE4FA711E3364C69(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void OneofOptions_tE3261CFC4DF0841400F23CBA8578D1191DE59AE7_CustomAttributesCacheGenerator_OneofOptions_CalculateSize_m2018ADE928533BABBA3CCDEA40E9FC13978CC4F0(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void OneofOptions_tE3261CFC4DF0841400F23CBA8578D1191DE59AE7_CustomAttributesCacheGenerator_OneofOptions_MergeFrom_mBCB2053FE63A76031534E6E4CD2DBD96E73118D2(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void OneofOptions_tE3261CFC4DF0841400F23CBA8578D1191DE59AE7_CustomAttributesCacheGenerator_OneofOptions_MergeFrom_mD36D6943FAFB5C64CC30EA67CF7DFAD976C846EA(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void OneofOptions_tE3261CFC4DF0841400F23CBA8578D1191DE59AE7_CustomAttributesCacheGenerator_OneofOptions_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_mDF39C909A8356CFEBE50ADE3B724A1C7B306E84D(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void OneofOptions_tE3261CFC4DF0841400F23CBA8578D1191DE59AE7_CustomAttributesCacheGenerator_OneofOptions_tE3261CFC4DF0841400F23CBA8578D1191DE59AE7____Parser_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void OneofOptions_tE3261CFC4DF0841400F23CBA8578D1191DE59AE7_CustomAttributesCacheGenerator_OneofOptions_tE3261CFC4DF0841400F23CBA8578D1191DE59AE7____Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void OneofOptions_tE3261CFC4DF0841400F23CBA8578D1191DE59AE7_CustomAttributesCacheGenerator_OneofOptions_tE3261CFC4DF0841400F23CBA8578D1191DE59AE7____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void U3CU3Ec_t555A80B41BAADB831E0BBE5DC035154EEC4697C7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EnumOptions_t84BF0A0DAC3BF1F447E6F0DABA6E4EB9B3AF4918_CustomAttributesCacheGenerator_EnumOptions__ctor_m5385E9F25EF8615050334EEC3708BA5725590EA6(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumOptions_t84BF0A0DAC3BF1F447E6F0DABA6E4EB9B3AF4918_CustomAttributesCacheGenerator_EnumOptions__ctor_m6CC675C9336E6028155824F6AC523E84B9B72900(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumOptions_t84BF0A0DAC3BF1F447E6F0DABA6E4EB9B3AF4918_CustomAttributesCacheGenerator_EnumOptions_Clone_m607D859024E1D3E98EADC2D87FE1A6004633A364(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumOptions_t84BF0A0DAC3BF1F447E6F0DABA6E4EB9B3AF4918_CustomAttributesCacheGenerator_EnumOptions_Equals_m7AE883DD0D520647C6A466B4B7731AA4C3ECC696(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumOptions_t84BF0A0DAC3BF1F447E6F0DABA6E4EB9B3AF4918_CustomAttributesCacheGenerator_EnumOptions_Equals_mA6AEAD2E23DF4EA72CB302A48CA00FC4D7844CB0(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumOptions_t84BF0A0DAC3BF1F447E6F0DABA6E4EB9B3AF4918_CustomAttributesCacheGenerator_EnumOptions_GetHashCode_mC319C69B06D57266DF45D6E68DBAD4A8A24CB638(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumOptions_t84BF0A0DAC3BF1F447E6F0DABA6E4EB9B3AF4918_CustomAttributesCacheGenerator_EnumOptions_ToString_m5D36D78A38E16C3589A17C93AF80ABC42F5304F7(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumOptions_t84BF0A0DAC3BF1F447E6F0DABA6E4EB9B3AF4918_CustomAttributesCacheGenerator_EnumOptions_WriteTo_m5689DE4D2F562F444B47E032A670D6F495472D74(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumOptions_t84BF0A0DAC3BF1F447E6F0DABA6E4EB9B3AF4918_CustomAttributesCacheGenerator_EnumOptions_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m014FF1827E80308CE342F0F91A38C87AD1E6FFAA(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumOptions_t84BF0A0DAC3BF1F447E6F0DABA6E4EB9B3AF4918_CustomAttributesCacheGenerator_EnumOptions_CalculateSize_mE57649652E7F429D04DDCB6BA25843D83D858366(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumOptions_t84BF0A0DAC3BF1F447E6F0DABA6E4EB9B3AF4918_CustomAttributesCacheGenerator_EnumOptions_MergeFrom_m8783323089C9BE38786C2F133AE42096DF6374AF(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumOptions_t84BF0A0DAC3BF1F447E6F0DABA6E4EB9B3AF4918_CustomAttributesCacheGenerator_EnumOptions_MergeFrom_mE5A3CD277AF799DB94EF81CFEBC4C84E13947F53(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumOptions_t84BF0A0DAC3BF1F447E6F0DABA6E4EB9B3AF4918_CustomAttributesCacheGenerator_EnumOptions_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m7703AE4472D5FDEB96708EFA3BD302633F5211F4(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumOptions_t84BF0A0DAC3BF1F447E6F0DABA6E4EB9B3AF4918_CustomAttributesCacheGenerator_EnumOptions_t84BF0A0DAC3BF1F447E6F0DABA6E4EB9B3AF4918____Parser_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumOptions_t84BF0A0DAC3BF1F447E6F0DABA6E4EB9B3AF4918_CustomAttributesCacheGenerator_EnumOptions_t84BF0A0DAC3BF1F447E6F0DABA6E4EB9B3AF4918____Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumOptions_t84BF0A0DAC3BF1F447E6F0DABA6E4EB9B3AF4918_CustomAttributesCacheGenerator_EnumOptions_t84BF0A0DAC3BF1F447E6F0DABA6E4EB9B3AF4918____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumOptions_t84BF0A0DAC3BF1F447E6F0DABA6E4EB9B3AF4918_CustomAttributesCacheGenerator_EnumOptions_t84BF0A0DAC3BF1F447E6F0DABA6E4EB9B3AF4918____AllowAlias_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumOptions_t84BF0A0DAC3BF1F447E6F0DABA6E4EB9B3AF4918_CustomAttributesCacheGenerator_EnumOptions_t84BF0A0DAC3BF1F447E6F0DABA6E4EB9B3AF4918____HasAllowAlias_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumOptions_t84BF0A0DAC3BF1F447E6F0DABA6E4EB9B3AF4918_CustomAttributesCacheGenerator_EnumOptions_t84BF0A0DAC3BF1F447E6F0DABA6E4EB9B3AF4918____Deprecated_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumOptions_t84BF0A0DAC3BF1F447E6F0DABA6E4EB9B3AF4918_CustomAttributesCacheGenerator_EnumOptions_t84BF0A0DAC3BF1F447E6F0DABA6E4EB9B3AF4918____HasDeprecated_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void U3CU3Ec_tC4B2AD54A8F08DBE9BD6F9E0B367AE343715DF1F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EnumValueOptions_t3521CCE0345304E9A1355EE2DA6AD9C0621C87A9_CustomAttributesCacheGenerator_EnumValueOptions__ctor_mEDC4489E058024D0EE74CD1882B267796D0E6F05(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumValueOptions_t3521CCE0345304E9A1355EE2DA6AD9C0621C87A9_CustomAttributesCacheGenerator_EnumValueOptions__ctor_m1E94702D09BCE26AC8F7CBFDD1D52C1AEFBB183E(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumValueOptions_t3521CCE0345304E9A1355EE2DA6AD9C0621C87A9_CustomAttributesCacheGenerator_EnumValueOptions_Clone_m6C604B56C82AC36BE5F555CD73F471B01B831730(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumValueOptions_t3521CCE0345304E9A1355EE2DA6AD9C0621C87A9_CustomAttributesCacheGenerator_EnumValueOptions_Equals_mBE4B57FD48FF5410BEB1956196C76DA4B5B451CD(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumValueOptions_t3521CCE0345304E9A1355EE2DA6AD9C0621C87A9_CustomAttributesCacheGenerator_EnumValueOptions_Equals_m4275B004B5AB046B0CCC1C05C4C9C88C672D4B27(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumValueOptions_t3521CCE0345304E9A1355EE2DA6AD9C0621C87A9_CustomAttributesCacheGenerator_EnumValueOptions_GetHashCode_m3CDA5B21FFC6387A8313F2E0230D63C5CC4D9699(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumValueOptions_t3521CCE0345304E9A1355EE2DA6AD9C0621C87A9_CustomAttributesCacheGenerator_EnumValueOptions_ToString_m689014E160C318BA3C63EFD328D3B0F7DCA8908E(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumValueOptions_t3521CCE0345304E9A1355EE2DA6AD9C0621C87A9_CustomAttributesCacheGenerator_EnumValueOptions_WriteTo_m2C38B59E54CC93366529373A55DCBBF1805B2FBC(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumValueOptions_t3521CCE0345304E9A1355EE2DA6AD9C0621C87A9_CustomAttributesCacheGenerator_EnumValueOptions_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m1E4497803CFE23240560E2EFDD0C4ED503A98338(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumValueOptions_t3521CCE0345304E9A1355EE2DA6AD9C0621C87A9_CustomAttributesCacheGenerator_EnumValueOptions_CalculateSize_m5CEBE3C36B5DFD8EB4139E0BC860D680C1A1EB19(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumValueOptions_t3521CCE0345304E9A1355EE2DA6AD9C0621C87A9_CustomAttributesCacheGenerator_EnumValueOptions_MergeFrom_m5ED39DC9CFB8446A5595EFED2D18B56BFE2F761E(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumValueOptions_t3521CCE0345304E9A1355EE2DA6AD9C0621C87A9_CustomAttributesCacheGenerator_EnumValueOptions_MergeFrom_m9CB27E7FFBCF59A93EFB81F345EEEE0018780465(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumValueOptions_t3521CCE0345304E9A1355EE2DA6AD9C0621C87A9_CustomAttributesCacheGenerator_EnumValueOptions_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m9A7A72C5D302BD0113B44F30F4D93A74E69CB7CD(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumValueOptions_t3521CCE0345304E9A1355EE2DA6AD9C0621C87A9_CustomAttributesCacheGenerator_EnumValueOptions_t3521CCE0345304E9A1355EE2DA6AD9C0621C87A9____Parser_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumValueOptions_t3521CCE0345304E9A1355EE2DA6AD9C0621C87A9_CustomAttributesCacheGenerator_EnumValueOptions_t3521CCE0345304E9A1355EE2DA6AD9C0621C87A9____Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumValueOptions_t3521CCE0345304E9A1355EE2DA6AD9C0621C87A9_CustomAttributesCacheGenerator_EnumValueOptions_t3521CCE0345304E9A1355EE2DA6AD9C0621C87A9____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumValueOptions_t3521CCE0345304E9A1355EE2DA6AD9C0621C87A9_CustomAttributesCacheGenerator_EnumValueOptions_t3521CCE0345304E9A1355EE2DA6AD9C0621C87A9____Deprecated_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void EnumValueOptions_t3521CCE0345304E9A1355EE2DA6AD9C0621C87A9_CustomAttributesCacheGenerator_EnumValueOptions_t3521CCE0345304E9A1355EE2DA6AD9C0621C87A9____HasDeprecated_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void U3CU3Ec_t4C02F076494955F9B45D28530DBF54CE68974CD3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ServiceOptions_t3CCBCF9F8215D476834CC7A5808FED49F161D11A_CustomAttributesCacheGenerator_ServiceOptions__ctor_mF53F4F57784851D29B2274A2B2C0B53A044FC5F2(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ServiceOptions_t3CCBCF9F8215D476834CC7A5808FED49F161D11A_CustomAttributesCacheGenerator_ServiceOptions__ctor_mB0C390F674A6FD4B7E940F7DF26EE01E8B72486C(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ServiceOptions_t3CCBCF9F8215D476834CC7A5808FED49F161D11A_CustomAttributesCacheGenerator_ServiceOptions_Clone_m548C4ABD6A7B28FF5D8339A2878E617F8FF0FC0F(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ServiceOptions_t3CCBCF9F8215D476834CC7A5808FED49F161D11A_CustomAttributesCacheGenerator_ServiceOptions_Equals_m8F72F53030D77F97C862B939A18F0B803318DE2F(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ServiceOptions_t3CCBCF9F8215D476834CC7A5808FED49F161D11A_CustomAttributesCacheGenerator_ServiceOptions_Equals_m8103227D46B27ED1BC216DB305C3973DC4F2DF76(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ServiceOptions_t3CCBCF9F8215D476834CC7A5808FED49F161D11A_CustomAttributesCacheGenerator_ServiceOptions_GetHashCode_m6843CA5920027100EC678C9E12D958ABBB0B7A2F(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ServiceOptions_t3CCBCF9F8215D476834CC7A5808FED49F161D11A_CustomAttributesCacheGenerator_ServiceOptions_ToString_m9C3A29E7B7D222E10DD900C0AF541A804B3130F8(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ServiceOptions_t3CCBCF9F8215D476834CC7A5808FED49F161D11A_CustomAttributesCacheGenerator_ServiceOptions_WriteTo_mC3CCEB2117A0EFC47E65EBCFF2D95500CAB4F504(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ServiceOptions_t3CCBCF9F8215D476834CC7A5808FED49F161D11A_CustomAttributesCacheGenerator_ServiceOptions_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m595286491DBE361A862243B1A0F5AA2C64F48739(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ServiceOptions_t3CCBCF9F8215D476834CC7A5808FED49F161D11A_CustomAttributesCacheGenerator_ServiceOptions_CalculateSize_mF38C58E3DD52815641DE6B3734035AD147E28E37(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ServiceOptions_t3CCBCF9F8215D476834CC7A5808FED49F161D11A_CustomAttributesCacheGenerator_ServiceOptions_MergeFrom_m0F4BD8AC991185BC2C06F1541C340B56202A6AE4(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ServiceOptions_t3CCBCF9F8215D476834CC7A5808FED49F161D11A_CustomAttributesCacheGenerator_ServiceOptions_MergeFrom_m91ACECFAAC88BE4A3B9AF6E32A85D5ACD847507D(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ServiceOptions_t3CCBCF9F8215D476834CC7A5808FED49F161D11A_CustomAttributesCacheGenerator_ServiceOptions_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_mB1A0155B8F628B062613DE44B49A8F6B680587AB(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ServiceOptions_t3CCBCF9F8215D476834CC7A5808FED49F161D11A_CustomAttributesCacheGenerator_ServiceOptions_t3CCBCF9F8215D476834CC7A5808FED49F161D11A____Parser_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ServiceOptions_t3CCBCF9F8215D476834CC7A5808FED49F161D11A_CustomAttributesCacheGenerator_ServiceOptions_t3CCBCF9F8215D476834CC7A5808FED49F161D11A____Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ServiceOptions_t3CCBCF9F8215D476834CC7A5808FED49F161D11A_CustomAttributesCacheGenerator_ServiceOptions_t3CCBCF9F8215D476834CC7A5808FED49F161D11A____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ServiceOptions_t3CCBCF9F8215D476834CC7A5808FED49F161D11A_CustomAttributesCacheGenerator_ServiceOptions_t3CCBCF9F8215D476834CC7A5808FED49F161D11A____Deprecated_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ServiceOptions_t3CCBCF9F8215D476834CC7A5808FED49F161D11A_CustomAttributesCacheGenerator_ServiceOptions_t3CCBCF9F8215D476834CC7A5808FED49F161D11A____HasDeprecated_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void U3CU3Ec_t6D3D46FF36020021A5BE4FD1D18730154BCE94FA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MethodOptions_t94D248D11F5D87A48A25D56B4E21F6E942ED9F4F_CustomAttributesCacheGenerator_MethodOptions__ctor_m8E6C41386BE75CAF13EBCA4773DE2C9455DCAACB(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MethodOptions_t94D248D11F5D87A48A25D56B4E21F6E942ED9F4F_CustomAttributesCacheGenerator_MethodOptions__ctor_m5D84C74E7A241DC3FEB9E244C786A3CA5A9EA6C1(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MethodOptions_t94D248D11F5D87A48A25D56B4E21F6E942ED9F4F_CustomAttributesCacheGenerator_MethodOptions_Clone_m4F7F9963D8DD66C5CBCDD6DE8D9044C573755C7B(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MethodOptions_t94D248D11F5D87A48A25D56B4E21F6E942ED9F4F_CustomAttributesCacheGenerator_MethodOptions_Equals_mC19CF9BE571A1064D708C490AFBB4DF8508597E2(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MethodOptions_t94D248D11F5D87A48A25D56B4E21F6E942ED9F4F_CustomAttributesCacheGenerator_MethodOptions_Equals_mB3CF6D193D1EB5531625EA2135546118AF0F4DA7(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MethodOptions_t94D248D11F5D87A48A25D56B4E21F6E942ED9F4F_CustomAttributesCacheGenerator_MethodOptions_GetHashCode_m419CD1A58A8AFCDB6D673259EB2CD03A31400414(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MethodOptions_t94D248D11F5D87A48A25D56B4E21F6E942ED9F4F_CustomAttributesCacheGenerator_MethodOptions_ToString_m582EC2A358621D0F1398769E1AF5733E93B2C166(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MethodOptions_t94D248D11F5D87A48A25D56B4E21F6E942ED9F4F_CustomAttributesCacheGenerator_MethodOptions_WriteTo_m8DC61AF4DBB7A265BD61718BFB2BA3E149F4CED0(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MethodOptions_t94D248D11F5D87A48A25D56B4E21F6E942ED9F4F_CustomAttributesCacheGenerator_MethodOptions_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m308FFA3BF40A93AD6A3BFD1CE927BD5745F4BA4B(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MethodOptions_t94D248D11F5D87A48A25D56B4E21F6E942ED9F4F_CustomAttributesCacheGenerator_MethodOptions_CalculateSize_m7AE8BB6308CC41A549C3409B7DD10E7459F38347(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MethodOptions_t94D248D11F5D87A48A25D56B4E21F6E942ED9F4F_CustomAttributesCacheGenerator_MethodOptions_MergeFrom_mDBD557C9249B039843EEA039CFBD3A5604D9FCD5(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MethodOptions_t94D248D11F5D87A48A25D56B4E21F6E942ED9F4F_CustomAttributesCacheGenerator_MethodOptions_MergeFrom_m55F1821842F961B9E76D746DEB8F09AA2F93032D(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MethodOptions_t94D248D11F5D87A48A25D56B4E21F6E942ED9F4F_CustomAttributesCacheGenerator_MethodOptions_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_mD8A99A524FB7D62431721187321E3FCA9124E0F1(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MethodOptions_t94D248D11F5D87A48A25D56B4E21F6E942ED9F4F_CustomAttributesCacheGenerator_MethodOptions_t94D248D11F5D87A48A25D56B4E21F6E942ED9F4F____Parser_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MethodOptions_t94D248D11F5D87A48A25D56B4E21F6E942ED9F4F_CustomAttributesCacheGenerator_MethodOptions_t94D248D11F5D87A48A25D56B4E21F6E942ED9F4F____Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MethodOptions_t94D248D11F5D87A48A25D56B4E21F6E942ED9F4F_CustomAttributesCacheGenerator_MethodOptions_t94D248D11F5D87A48A25D56B4E21F6E942ED9F4F____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MethodOptions_t94D248D11F5D87A48A25D56B4E21F6E942ED9F4F_CustomAttributesCacheGenerator_MethodOptions_t94D248D11F5D87A48A25D56B4E21F6E942ED9F4F____Deprecated_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MethodOptions_t94D248D11F5D87A48A25D56B4E21F6E942ED9F4F_CustomAttributesCacheGenerator_MethodOptions_t94D248D11F5D87A48A25D56B4E21F6E942ED9F4F____HasDeprecated_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MethodOptions_t94D248D11F5D87A48A25D56B4E21F6E942ED9F4F_CustomAttributesCacheGenerator_MethodOptions_t94D248D11F5D87A48A25D56B4E21F6E942ED9F4F____IdempotencyLevel_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MethodOptions_t94D248D11F5D87A48A25D56B4E21F6E942ED9F4F_CustomAttributesCacheGenerator_MethodOptions_t94D248D11F5D87A48A25D56B4E21F6E942ED9F4F____HasIdempotencyLevel_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Types_tB23AD38B494C3E7C0B0BA27DBC128F2E05A3EFD5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void IdempotencyLevel_tF1CB740909022AF43C9AA8E9B0E20AF1D38649C3_CustomAttributesCacheGenerator_IdempotencyUnknown(CustomAttributesCache* cache)
{
	{
		OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * tmp = (OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 *)cache->attributes[0];
		OriginalNameAttribute__ctor_mB0C6C38C49B9261D2DC423D692E80B8EBC657BF9(tmp, il2cpp_codegen_string_new_wrapper("\x49\x44\x45\x4D\x50\x4F\x54\x45\x4E\x43\x59\x5F\x55\x4E\x4B\x4E\x4F\x57\x4E"), NULL);
	}
}
static void IdempotencyLevel_tF1CB740909022AF43C9AA8E9B0E20AF1D38649C3_CustomAttributesCacheGenerator_NoSideEffects(CustomAttributesCache* cache)
{
	{
		OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * tmp = (OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 *)cache->attributes[0];
		OriginalNameAttribute__ctor_mB0C6C38C49B9261D2DC423D692E80B8EBC657BF9(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x4F\x5F\x53\x49\x44\x45\x5F\x45\x46\x46\x45\x43\x54\x53"), NULL);
	}
}
static void IdempotencyLevel_tF1CB740909022AF43C9AA8E9B0E20AF1D38649C3_CustomAttributesCacheGenerator_Idempotent(CustomAttributesCache* cache)
{
	{
		OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * tmp = (OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 *)cache->attributes[0];
		OriginalNameAttribute__ctor_mB0C6C38C49B9261D2DC423D692E80B8EBC657BF9(tmp, il2cpp_codegen_string_new_wrapper("\x49\x44\x45\x4D\x50\x4F\x54\x45\x4E\x54"), NULL);
	}
}
static void U3CU3Ec_t1D94F8B22F7F01675BEF565B6100984E73D976A8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728_CustomAttributesCacheGenerator_UninterpretedOption__ctor_m01FCE0DA392AF8E60B04329799B2FD66C2A4676D(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728_CustomAttributesCacheGenerator_UninterpretedOption__ctor_m11693D3C42B33BE6E844F5F6F2810E399BF413EC(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728_CustomAttributesCacheGenerator_UninterpretedOption_Clone_m0B1D69F3BE93968ACEAECA8835FEB1B5542ECE6A(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728_CustomAttributesCacheGenerator_UninterpretedOption_Equals_m480D8D7A06EC8E37BDDAE05B1A4A3709F6FC0850(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728_CustomAttributesCacheGenerator_UninterpretedOption_Equals_m695AA83BD422C1433861AB2DFEA337174A603910(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728_CustomAttributesCacheGenerator_UninterpretedOption_GetHashCode_m9E5BF7D1BE4AA11718BA1841A1A0F0A010FC1ECC(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728_CustomAttributesCacheGenerator_UninterpretedOption_ToString_m5C88AF1EC3E062A8E98637D0316F62C9874A5E5F(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728_CustomAttributesCacheGenerator_UninterpretedOption_WriteTo_mE3E2D1AEAF4379F1E60A80DB89EDD27EE8285255(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728_CustomAttributesCacheGenerator_UninterpretedOption_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m790A1EEC06943590A53B5FE56456EFA59EB44034(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728_CustomAttributesCacheGenerator_UninterpretedOption_CalculateSize_m0073EE08E92667DDF1A0764667B6890C0765E0D1(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728_CustomAttributesCacheGenerator_UninterpretedOption_MergeFrom_mCDAB1F9A576C61CDC4FAA103105C9C3BED5DFCE9(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728_CustomAttributesCacheGenerator_UninterpretedOption_MergeFrom_m0464D25AE497BE73F7067D4A56C1AFB748FBB16B(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728_CustomAttributesCacheGenerator_UninterpretedOption_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_mD2F9D9A892C3A6A090D637C759815A6BFA835FE3(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728_CustomAttributesCacheGenerator_UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728____Parser_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728_CustomAttributesCacheGenerator_UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728____Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728_CustomAttributesCacheGenerator_UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728_CustomAttributesCacheGenerator_UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728____IdentifierValue_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728_CustomAttributesCacheGenerator_UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728____HasIdentifierValue_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728_CustomAttributesCacheGenerator_UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728____PositiveIntValue_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728_CustomAttributesCacheGenerator_UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728____HasPositiveIntValue_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728_CustomAttributesCacheGenerator_UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728____NegativeIntValue_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728_CustomAttributesCacheGenerator_UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728____HasNegativeIntValue_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728_CustomAttributesCacheGenerator_UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728____DoubleValue_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728_CustomAttributesCacheGenerator_UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728____HasDoubleValue_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728_CustomAttributesCacheGenerator_UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728____StringValue_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728_CustomAttributesCacheGenerator_UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728____HasStringValue_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728_CustomAttributesCacheGenerator_UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728____AggregateValue_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728_CustomAttributesCacheGenerator_UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728____HasAggregateValue_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Types_t9BD23E35FC17FB4A53AD87682F7E66FDDF6D22A5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22_CustomAttributesCacheGenerator_NamePart__ctor_mA9A9103300EE0850829D3BFE67B52EAD4512F085(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22_CustomAttributesCacheGenerator_NamePart__ctor_mFBC708208AD503A756034768ABF66A5E5E0AF4DD(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22_CustomAttributesCacheGenerator_NamePart_Clone_m43B2E0BD86099C758B706C19DB038F56A2409C6F(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22_CustomAttributesCacheGenerator_NamePart_Equals_m9F4FC0E7DAF91E9141210A33B1AFE8F2127F68BB(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22_CustomAttributesCacheGenerator_NamePart_Equals_m4F68F7BFA6DC8252C7BF46F9B7B7A4561BC9B297(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22_CustomAttributesCacheGenerator_NamePart_GetHashCode_m84C037E032844843C3501CBBF1D5579160297F11(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22_CustomAttributesCacheGenerator_NamePart_ToString_mF10088283D361B38968A6E57F78255654D3BB2D1(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22_CustomAttributesCacheGenerator_NamePart_WriteTo_m9F9C644A5C5530F04A945671E7BBA3FEF7061448(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22_CustomAttributesCacheGenerator_NamePart_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m0396A0FC112B2CFF1E5B1FD1C0AE400359E96A01(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22_CustomAttributesCacheGenerator_NamePart_CalculateSize_mE593E657449DE890C6273E4C9DC3B90A948C8749(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22_CustomAttributesCacheGenerator_NamePart_MergeFrom_mB5380B100DD6F68EDEEF200E1BB000EFC4906DA7(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22_CustomAttributesCacheGenerator_NamePart_MergeFrom_m4889B005A581C203986474994D59CAD2FC9F5F30(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22_CustomAttributesCacheGenerator_NamePart_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m161542DC71B24D4416D0327851A5EFD2B8A093D2(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22_CustomAttributesCacheGenerator_NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22____Parser_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22_CustomAttributesCacheGenerator_NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22____Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22_CustomAttributesCacheGenerator_NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22_CustomAttributesCacheGenerator_NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22____NamePart__PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22_CustomAttributesCacheGenerator_NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22____HasNamePart__PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22_CustomAttributesCacheGenerator_NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22____IsExtension_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22_CustomAttributesCacheGenerator_NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22____HasIsExtension_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void U3CU3Ec_t70C29F3AE73CD37C0D77DCC96A139645BE59A453_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_t4168CEB7B72566F0CB5098F58D358C4338EEA60F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SourceCodeInfo_tE9110A5CAA36A130E03CC9F3DE6E0EF7E7BF412B_CustomAttributesCacheGenerator_SourceCodeInfo__ctor_m0A1DC09D0F3B17BC20F44D8201C5D0DB841145E9(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void SourceCodeInfo_tE9110A5CAA36A130E03CC9F3DE6E0EF7E7BF412B_CustomAttributesCacheGenerator_SourceCodeInfo__ctor_m12C51AC307B6497E9CF7C2887ADB23E01B33392A(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void SourceCodeInfo_tE9110A5CAA36A130E03CC9F3DE6E0EF7E7BF412B_CustomAttributesCacheGenerator_SourceCodeInfo_Clone_m879DC87218D95C6E0907ABB23C9F355F21C158BA(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void SourceCodeInfo_tE9110A5CAA36A130E03CC9F3DE6E0EF7E7BF412B_CustomAttributesCacheGenerator_SourceCodeInfo_Equals_mAC74617ADE26118FC282D20867FF972CF4B90786(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void SourceCodeInfo_tE9110A5CAA36A130E03CC9F3DE6E0EF7E7BF412B_CustomAttributesCacheGenerator_SourceCodeInfo_Equals_mA2B30829479F4760E5B91E5B65D41B02889BED41(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void SourceCodeInfo_tE9110A5CAA36A130E03CC9F3DE6E0EF7E7BF412B_CustomAttributesCacheGenerator_SourceCodeInfo_GetHashCode_mCD706831C8044FE179D8117D2526273B88B073A3(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void SourceCodeInfo_tE9110A5CAA36A130E03CC9F3DE6E0EF7E7BF412B_CustomAttributesCacheGenerator_SourceCodeInfo_ToString_mE4FC459AEF9A122B2893BE043EFA5EF455BA1CAD(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void SourceCodeInfo_tE9110A5CAA36A130E03CC9F3DE6E0EF7E7BF412B_CustomAttributesCacheGenerator_SourceCodeInfo_WriteTo_m11D19F228C061C27C584002104687141B0D514AE(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void SourceCodeInfo_tE9110A5CAA36A130E03CC9F3DE6E0EF7E7BF412B_CustomAttributesCacheGenerator_SourceCodeInfo_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m601DD6F66EB4F4AF24126704725F9979A31EE648(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void SourceCodeInfo_tE9110A5CAA36A130E03CC9F3DE6E0EF7E7BF412B_CustomAttributesCacheGenerator_SourceCodeInfo_CalculateSize_m5530408BED9841C7EDC134DC3B2CB88F2691B4CA(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void SourceCodeInfo_tE9110A5CAA36A130E03CC9F3DE6E0EF7E7BF412B_CustomAttributesCacheGenerator_SourceCodeInfo_MergeFrom_mA3BFA5056081A4CFA0CE5DFF332A6815646EEF0A(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void SourceCodeInfo_tE9110A5CAA36A130E03CC9F3DE6E0EF7E7BF412B_CustomAttributesCacheGenerator_SourceCodeInfo_MergeFrom_mEC664CB41897C3A5E540B205E64E6658866D29FC(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void SourceCodeInfo_tE9110A5CAA36A130E03CC9F3DE6E0EF7E7BF412B_CustomAttributesCacheGenerator_SourceCodeInfo_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m1E724139176220624BC89C54831F1E7A68FA3815(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void SourceCodeInfo_tE9110A5CAA36A130E03CC9F3DE6E0EF7E7BF412B_CustomAttributesCacheGenerator_SourceCodeInfo_tE9110A5CAA36A130E03CC9F3DE6E0EF7E7BF412B____Parser_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void SourceCodeInfo_tE9110A5CAA36A130E03CC9F3DE6E0EF7E7BF412B_CustomAttributesCacheGenerator_SourceCodeInfo_tE9110A5CAA36A130E03CC9F3DE6E0EF7E7BF412B____Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void SourceCodeInfo_tE9110A5CAA36A130E03CC9F3DE6E0EF7E7BF412B_CustomAttributesCacheGenerator_SourceCodeInfo_tE9110A5CAA36A130E03CC9F3DE6E0EF7E7BF412B____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void SourceCodeInfo_tE9110A5CAA36A130E03CC9F3DE6E0EF7E7BF412B_CustomAttributesCacheGenerator_SourceCodeInfo_tE9110A5CAA36A130E03CC9F3DE6E0EF7E7BF412B____Location_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Types_t938123A603A401FC9E5EE0E7211A8C54532FD7B9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_CustomAttributesCacheGenerator_Location__ctor_mFDC279769BEAE3382B8775935C8683621235F622(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_CustomAttributesCacheGenerator_Location__ctor_m28EFCE79F70879A58E3CB6F54CE731BD38BE41DD(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_CustomAttributesCacheGenerator_Location_Clone_m3C4C0598AE1D773542602ABBFBFC912ABB270BEE(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_CustomAttributesCacheGenerator_Location_Equals_m85AD8ECE48C60DADB8768733AF1F6563AF4AC281(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_CustomAttributesCacheGenerator_Location_Equals_m8D982EACDA1888B6588CD7689C00D5EBB2985368(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_CustomAttributesCacheGenerator_Location_GetHashCode_m6ACF33DBF8F3F18D5D7DD313D7729EA5839A174B(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_CustomAttributesCacheGenerator_Location_ToString_m9B282141CD535FF8261C8195BF3CDDF528C40A4C(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_CustomAttributesCacheGenerator_Location_WriteTo_m4210E1B1440977E2A413E0B8F75133982F73EC69(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_CustomAttributesCacheGenerator_Location_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m504F51927BE7797E64ECDEC999C03BB04846EB33(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_CustomAttributesCacheGenerator_Location_CalculateSize_m4FE7C1E7A7BB0E885F9A9779A16EC3D562B6BA3F(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_CustomAttributesCacheGenerator_Location_MergeFrom_mFA64C2E2E2D47FD3294936AD3C696EAE3B0CD8F7(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_CustomAttributesCacheGenerator_Location_MergeFrom_mD097D430111330C3EE05F9F06685B68DB4A7077D(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_CustomAttributesCacheGenerator_Location_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m30960A0BA81FA14E2F5806B1821B17CEB6EC74A8(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_CustomAttributesCacheGenerator_Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4____Parser_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_CustomAttributesCacheGenerator_Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4____Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_CustomAttributesCacheGenerator_Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_CustomAttributesCacheGenerator_Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4____Path_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_CustomAttributesCacheGenerator_Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4____Span_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_CustomAttributesCacheGenerator_Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4____LeadingComments_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_CustomAttributesCacheGenerator_Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4____HasLeadingComments_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_CustomAttributesCacheGenerator_Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4____TrailingComments_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_CustomAttributesCacheGenerator_Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4____HasTrailingComments_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_CustomAttributesCacheGenerator_Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4____LeadingDetachedComments_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void U3CU3Ec_tE6E47C051562B8F8FCE9347E1B99439CF27AB1AF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_tB54E6E726C6A3C0B8843E8A2452EABBDADEC9F28_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GeneratedCodeInfo_t88B2B7880C3E0445510519523DE05618C3B1A315_CustomAttributesCacheGenerator_GeneratedCodeInfo__ctor_mDE0BB1D90115580D0C21C264412FD0C6BDC79827(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void GeneratedCodeInfo_t88B2B7880C3E0445510519523DE05618C3B1A315_CustomAttributesCacheGenerator_GeneratedCodeInfo__ctor_m5588A1B1D68A18DA88CA5DCB4533BE39BA987B0B(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void GeneratedCodeInfo_t88B2B7880C3E0445510519523DE05618C3B1A315_CustomAttributesCacheGenerator_GeneratedCodeInfo_Clone_mB2E07DB9A3075EAAF762DFDCE21F99B40521F491(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void GeneratedCodeInfo_t88B2B7880C3E0445510519523DE05618C3B1A315_CustomAttributesCacheGenerator_GeneratedCodeInfo_Equals_m881F3A7864546DCD98F1AC8E5C753DA3B1AEC0A1(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void GeneratedCodeInfo_t88B2B7880C3E0445510519523DE05618C3B1A315_CustomAttributesCacheGenerator_GeneratedCodeInfo_Equals_mA65F1EB2D6AE57EAB2AB0B72A3F4892252760755(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void GeneratedCodeInfo_t88B2B7880C3E0445510519523DE05618C3B1A315_CustomAttributesCacheGenerator_GeneratedCodeInfo_GetHashCode_mB9F0A2A53CF8BEB45CBAFE12C8CFCB5819E31C3D(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void GeneratedCodeInfo_t88B2B7880C3E0445510519523DE05618C3B1A315_CustomAttributesCacheGenerator_GeneratedCodeInfo_ToString_m325B0EB4FF2412BCDDAD96524195745F0517B9D9(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void GeneratedCodeInfo_t88B2B7880C3E0445510519523DE05618C3B1A315_CustomAttributesCacheGenerator_GeneratedCodeInfo_WriteTo_m66B19D7405A8F6561D49E8A49165FE62712685DF(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void GeneratedCodeInfo_t88B2B7880C3E0445510519523DE05618C3B1A315_CustomAttributesCacheGenerator_GeneratedCodeInfo_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_mE9F47B77861B199F58CF5AB795C22FA47A0E3FD4(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void GeneratedCodeInfo_t88B2B7880C3E0445510519523DE05618C3B1A315_CustomAttributesCacheGenerator_GeneratedCodeInfo_CalculateSize_mB7F3C903908BB158E829E98EF242D7BE2070A981(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void GeneratedCodeInfo_t88B2B7880C3E0445510519523DE05618C3B1A315_CustomAttributesCacheGenerator_GeneratedCodeInfo_MergeFrom_m2AF45EF1C2F717A5957EF6F07C088851083D83D2(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void GeneratedCodeInfo_t88B2B7880C3E0445510519523DE05618C3B1A315_CustomAttributesCacheGenerator_GeneratedCodeInfo_MergeFrom_m87520A239A84EFAEA34062FD27265F10E6EE7BCE(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void GeneratedCodeInfo_t88B2B7880C3E0445510519523DE05618C3B1A315_CustomAttributesCacheGenerator_GeneratedCodeInfo_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m68CF657C364AE67E08C90AA4854CA28F745E6AEF(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void GeneratedCodeInfo_t88B2B7880C3E0445510519523DE05618C3B1A315_CustomAttributesCacheGenerator_GeneratedCodeInfo_t88B2B7880C3E0445510519523DE05618C3B1A315____Parser_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void GeneratedCodeInfo_t88B2B7880C3E0445510519523DE05618C3B1A315_CustomAttributesCacheGenerator_GeneratedCodeInfo_t88B2B7880C3E0445510519523DE05618C3B1A315____Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void GeneratedCodeInfo_t88B2B7880C3E0445510519523DE05618C3B1A315_CustomAttributesCacheGenerator_GeneratedCodeInfo_t88B2B7880C3E0445510519523DE05618C3B1A315____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Types_t50292842BB5717A7DF5408D073E8593E5D93ABFA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_CustomAttributesCacheGenerator_Annotation__ctor_m9ED6A93F900EBFD3D4BC4980C3D9B032B8F9B5D7(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_CustomAttributesCacheGenerator_Annotation__ctor_m61D9641ED59D1CDA766721C1A40198C8BF8F6E45(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_CustomAttributesCacheGenerator_Annotation_Clone_mB86AE67CF1DB8FC7CF33ADDCEF2FEEB74BCDFE5D(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_CustomAttributesCacheGenerator_Annotation_Equals_m5C1A51E20393172CE798E42CAD95C3DD71E511C8(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_CustomAttributesCacheGenerator_Annotation_Equals_mD66C9282185C31DF563EED063B1019BE13C2811E(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_CustomAttributesCacheGenerator_Annotation_GetHashCode_m6F1BE21B2168F0D61B4E600E21DDC81A22132BF2(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_CustomAttributesCacheGenerator_Annotation_ToString_m2E543D1B02E6207CB9009CEA5A987A6439466064(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_CustomAttributesCacheGenerator_Annotation_WriteTo_m108B8AEABCE4F89A6E90997BAC8AC9371A53565D(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_CustomAttributesCacheGenerator_Annotation_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m2D0F50AAD5686905CCAA2959D0C6E112FDA37E85(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_CustomAttributesCacheGenerator_Annotation_CalculateSize_m1DE1ABE6707A9C1C6D415715E6EB90D7DDD98AB8(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_CustomAttributesCacheGenerator_Annotation_MergeFrom_m2468E39EA5A58BCFA6F9E35D01002292E3CC450F(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_CustomAttributesCacheGenerator_Annotation_MergeFrom_m8CA8D3549BCBA484E4BA0ADCDE8200E20CCEDFBD(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_CustomAttributesCacheGenerator_Annotation_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m314FDFF4E0741C746FA60D608C45F8C01E8A9117(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_CustomAttributesCacheGenerator_Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405____Parser_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_CustomAttributesCacheGenerator_Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405____Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_CustomAttributesCacheGenerator_Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_CustomAttributesCacheGenerator_Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405____SourceFile_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_CustomAttributesCacheGenerator_Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405____HasSourceFile_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_CustomAttributesCacheGenerator_Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405____Begin_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_CustomAttributesCacheGenerator_Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405____HasBegin_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_CustomAttributesCacheGenerator_Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405____End_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_CustomAttributesCacheGenerator_Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405____HasEnd_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void U3CU3Ec_t48164166658FE6E4AA4BC81699272A7660D5A345_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_tBD193B147926CA34945206F084AC82D159DAA97F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DescriptorBase_tF6C2BA893A9882D75C7CD171CB21EBA88FA82CEA_CustomAttributesCacheGenerator_U3CIndexU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DescriptorBase_tF6C2BA893A9882D75C7CD171CB21EBA88FA82CEA_CustomAttributesCacheGenerator_U3CFullNameU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DescriptorBase_tF6C2BA893A9882D75C7CD171CB21EBA88FA82CEA_CustomAttributesCacheGenerator_U3CFileU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DescriptorBase_tF6C2BA893A9882D75C7CD171CB21EBA88FA82CEA_CustomAttributesCacheGenerator_DescriptorBase_get_FullName_m915949C2A2E317ADAAA3141CB30DE432F1D6047D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DescriptorBase_tF6C2BA893A9882D75C7CD171CB21EBA88FA82CEA_CustomAttributesCacheGenerator_DescriptorBase_get_File_mFBA850CF476DBD50BF1AB469555EAD69FFD827A4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DescriptorDeclaration_tA267DA7CAD0094614E29377401372F3EB4C395FB_CustomAttributesCacheGenerator_U3CDescriptorU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DescriptorDeclaration_tA267DA7CAD0094614E29377401372F3EB4C395FB_CustomAttributesCacheGenerator_U3CStartLineU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DescriptorDeclaration_tA267DA7CAD0094614E29377401372F3EB4C395FB_CustomAttributesCacheGenerator_U3CStartColumnU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DescriptorDeclaration_tA267DA7CAD0094614E29377401372F3EB4C395FB_CustomAttributesCacheGenerator_U3CEndLineU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DescriptorDeclaration_tA267DA7CAD0094614E29377401372F3EB4C395FB_CustomAttributesCacheGenerator_U3CEndColumnU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DescriptorDeclaration_tA267DA7CAD0094614E29377401372F3EB4C395FB_CustomAttributesCacheGenerator_U3CLeadingCommentsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DescriptorDeclaration_tA267DA7CAD0094614E29377401372F3EB4C395FB_CustomAttributesCacheGenerator_U3CTrailingCommentsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DescriptorDeclaration_tA267DA7CAD0094614E29377401372F3EB4C395FB_CustomAttributesCacheGenerator_U3CLeadingDetachedCommentsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DescriptorDeclaration_tA267DA7CAD0094614E29377401372F3EB4C395FB_CustomAttributesCacheGenerator_DescriptorDeclaration_get_StartLine_mECF4D235763DA0659A3E0E32D5ED6F286FAA6920(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass4_0_tC125EB0E5276232874526BC3333E73223EF8C09A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ExtensionAccessor_t8FD60C5A0CEA561F542BE3A83BD8634D11C38118_CustomAttributesCacheGenerator_U3CDescriptorU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ExtensionAccessor_t8FD60C5A0CEA561F542BE3A83BD8634D11C38118_CustomAttributesCacheGenerator_ExtensionAccessor_get_Descriptor_m4D3230A77360F27ED2DDBCD1A52FBB7644A3BB9D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ExtensionCollection_t52756747D345D5B934DCD6FF9876823214980772_CustomAttributesCacheGenerator_U3CUnorderedExtensionsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ExtensionCollection_t52756747D345D5B934DCD6FF9876823214980772_CustomAttributesCacheGenerator_ExtensionCollection_get_UnorderedExtensions_mE70760039915D512775C9C2193AE020D011B0FCD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass2_0_t69A25B41E29EF18D3906A126830960FA58B7E1B6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass3_0_t07F42F32B123CC5B1CC4D48B3703EE051A52956A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_tD9ABE4542EBE3438CDB091E751A07D8B9C876BB9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FieldDescriptor_t0D57515C5C0DD2F7D37E1EB3838AE950F7C609BD_CustomAttributesCacheGenerator_U3CContainingTypeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FieldDescriptor_t0D57515C5C0DD2F7D37E1EB3838AE950F7C609BD_CustomAttributesCacheGenerator_U3CContainingOneofU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FieldDescriptor_t0D57515C5C0DD2F7D37E1EB3838AE950F7C609BD_CustomAttributesCacheGenerator_U3CJsonNameU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FieldDescriptor_t0D57515C5C0DD2F7D37E1EB3838AE950F7C609BD_CustomAttributesCacheGenerator_U3CProtoU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FieldDescriptor_t0D57515C5C0DD2F7D37E1EB3838AE950F7C609BD_CustomAttributesCacheGenerator_U3CExtensionU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FieldDescriptor_t0D57515C5C0DD2F7D37E1EB3838AE950F7C609BD_CustomAttributesCacheGenerator_FieldDescriptor_get_ContainingType_mBEBF37A2458327C9419A884CB13132958B0009CA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FieldDescriptor_t0D57515C5C0DD2F7D37E1EB3838AE950F7C609BD_CustomAttributesCacheGenerator_FieldDescriptor_get_ContainingOneof_m792FE02B65FBBEFDCFA68FCE08CE1B789C935BE9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FieldDescriptor_t0D57515C5C0DD2F7D37E1EB3838AE950F7C609BD_CustomAttributesCacheGenerator_FieldDescriptor_get_JsonName_m37E80D133D994BDE854220D920790A1E1BBDAF50(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FieldDescriptor_t0D57515C5C0DD2F7D37E1EB3838AE950F7C609BD_CustomAttributesCacheGenerator_FieldDescriptor_get_Proto_m193D4347BAF5B48C4CDACF7FEBEB0EB7ECB1A5CB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FieldDescriptor_t0D57515C5C0DD2F7D37E1EB3838AE950F7C609BD_CustomAttributesCacheGenerator_FieldDescriptor_get_Extension_m47CB4AEB81E5372FE16AF15451EE3DB8F96F95BA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FileDescriptor_tA2E229ECE64C897445BBCA0229BD41F8E028ECD4_CustomAttributesCacheGenerator_U3CProtoU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FileDescriptor_tA2E229ECE64C897445BBCA0229BD41F8E028ECD4_CustomAttributesCacheGenerator_U3CSyntaxU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FileDescriptor_tA2E229ECE64C897445BBCA0229BD41F8E028ECD4_CustomAttributesCacheGenerator_U3CMessageTypesU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FileDescriptor_tA2E229ECE64C897445BBCA0229BD41F8E028ECD4_CustomAttributesCacheGenerator_U3CEnumTypesU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FileDescriptor_tA2E229ECE64C897445BBCA0229BD41F8E028ECD4_CustomAttributesCacheGenerator_U3CServicesU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FileDescriptor_tA2E229ECE64C897445BBCA0229BD41F8E028ECD4_CustomAttributesCacheGenerator_U3CExtensionsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FileDescriptor_tA2E229ECE64C897445BBCA0229BD41F8E028ECD4_CustomAttributesCacheGenerator_U3CDependenciesU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FileDescriptor_tA2E229ECE64C897445BBCA0229BD41F8E028ECD4_CustomAttributesCacheGenerator_U3CPublicDependenciesU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FileDescriptor_tA2E229ECE64C897445BBCA0229BD41F8E028ECD4_CustomAttributesCacheGenerator_U3CSerializedDataU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FileDescriptor_tA2E229ECE64C897445BBCA0229BD41F8E028ECD4_CustomAttributesCacheGenerator_U3CDescriptorPoolU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FileDescriptor_tA2E229ECE64C897445BBCA0229BD41F8E028ECD4_CustomAttributesCacheGenerator_FileDescriptor_get_Proto_m3861FEA6DC13D62F54CC04BD11AAAAA1CC9D235B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FileDescriptor_tA2E229ECE64C897445BBCA0229BD41F8E028ECD4_CustomAttributesCacheGenerator_FileDescriptor_get_Syntax_mC6D5E49E01F937101A4C15FDA84933E5E3AAD38C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FileDescriptor_tA2E229ECE64C897445BBCA0229BD41F8E028ECD4_CustomAttributesCacheGenerator_FileDescriptor_get_MessageTypes_m55CD9B2EB841D0F8FF161F0DB15A2D67266C536C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FileDescriptor_tA2E229ECE64C897445BBCA0229BD41F8E028ECD4_CustomAttributesCacheGenerator_FileDescriptor_get_EnumTypes_m52F61381D43042CA3A0B4B0FF30BB33ED4BE8D44(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FileDescriptor_tA2E229ECE64C897445BBCA0229BD41F8E028ECD4_CustomAttributesCacheGenerator_FileDescriptor_get_Services_m111C66F60C4E1EB7FCE260D433F35058563F9E39(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FileDescriptor_tA2E229ECE64C897445BBCA0229BD41F8E028ECD4_CustomAttributesCacheGenerator_FileDescriptor_get_Extensions_m3308E404E50DC055A4AF9EDAB142BA227CA7F4B2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FileDescriptor_tA2E229ECE64C897445BBCA0229BD41F8E028ECD4_CustomAttributesCacheGenerator_FileDescriptor_get_Dependencies_m9E9EB5F41B5489AE519540C7027903300E4F5955(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FileDescriptor_tA2E229ECE64C897445BBCA0229BD41F8E028ECD4_CustomAttributesCacheGenerator_FileDescriptor_get_PublicDependencies_mAC7D2C8B65EA6260FD1E00427C865B073C26C20C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FileDescriptor_tA2E229ECE64C897445BBCA0229BD41F8E028ECD4_CustomAttributesCacheGenerator_FileDescriptor_get_DescriptorPool_m39E958D13A60EBE7CC79BD0456664D61900040A3(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass2_0_t2575137C3063791A132A5C6E4032E7B5290F484E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_tF92FFF77B6725E8DCBB4BACE18AF8D12FCCFC182_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GeneratedClrTypeInfo_t26E71D04A6D31D12375C412ACFDF303153379EDE_CustomAttributesCacheGenerator_U3CClrTypeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GeneratedClrTypeInfo_t26E71D04A6D31D12375C412ACFDF303153379EDE_CustomAttributesCacheGenerator_U3CParserU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GeneratedClrTypeInfo_t26E71D04A6D31D12375C412ACFDF303153379EDE_CustomAttributesCacheGenerator_U3CPropertyNamesU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GeneratedClrTypeInfo_t26E71D04A6D31D12375C412ACFDF303153379EDE_CustomAttributesCacheGenerator_U3CExtensionsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GeneratedClrTypeInfo_t26E71D04A6D31D12375C412ACFDF303153379EDE_CustomAttributesCacheGenerator_U3COneofNamesU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GeneratedClrTypeInfo_t26E71D04A6D31D12375C412ACFDF303153379EDE_CustomAttributesCacheGenerator_U3CNestedTypesU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GeneratedClrTypeInfo_t26E71D04A6D31D12375C412ACFDF303153379EDE_CustomAttributesCacheGenerator_U3CNestedEnumsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GeneratedClrTypeInfo_t26E71D04A6D31D12375C412ACFDF303153379EDE_CustomAttributesCacheGenerator_GeneratedClrTypeInfo_get_ClrType_mBC268EA87D91A720FC864656F282267E52E5C415(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GeneratedClrTypeInfo_t26E71D04A6D31D12375C412ACFDF303153379EDE_CustomAttributesCacheGenerator_GeneratedClrTypeInfo_set_ClrType_m47923E24303E58B15765BA976560FF54060AA463(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GeneratedClrTypeInfo_t26E71D04A6D31D12375C412ACFDF303153379EDE_CustomAttributesCacheGenerator_GeneratedClrTypeInfo_get_Parser_mE306A707C2E98793AE20F8F22D73682256A6825A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GeneratedClrTypeInfo_t26E71D04A6D31D12375C412ACFDF303153379EDE_CustomAttributesCacheGenerator_GeneratedClrTypeInfo_get_PropertyNames_m9AC3AF88BE6E4401E8CBE61D2A27205D494C7537(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GeneratedClrTypeInfo_t26E71D04A6D31D12375C412ACFDF303153379EDE_CustomAttributesCacheGenerator_GeneratedClrTypeInfo_get_Extensions_mBC35C072C48C25BC9E3E10930E7ABC899E7D0BD9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GeneratedClrTypeInfo_t26E71D04A6D31D12375C412ACFDF303153379EDE_CustomAttributesCacheGenerator_GeneratedClrTypeInfo_get_OneofNames_m8225DF12A9536F188BB8F84100E0026AD38398D3(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GeneratedClrTypeInfo_t26E71D04A6D31D12375C412ACFDF303153379EDE_CustomAttributesCacheGenerator_GeneratedClrTypeInfo_get_NestedTypes_mC8CDA4ACD8F8B83388C64EFB5C63E8841E8B7767(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GeneratedClrTypeInfo_t26E71D04A6D31D12375C412ACFDF303153379EDE_CustomAttributesCacheGenerator_GeneratedClrTypeInfo_get_NestedEnums_m124D8277A92DC1AD640C130ABE855B6C9B55EB82(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13_CustomAttributesCacheGenerator_U3CProtoU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13_CustomAttributesCacheGenerator_U3CClrTypeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13_CustomAttributesCacheGenerator_U3CParserU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13_CustomAttributesCacheGenerator_U3CContainingTypeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13_CustomAttributesCacheGenerator_U3CFieldsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13_CustomAttributesCacheGenerator_U3CExtensionsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13_CustomAttributesCacheGenerator_U3CNestedTypesU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13_CustomAttributesCacheGenerator_U3CEnumTypesU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13_CustomAttributesCacheGenerator_U3COneofsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13_CustomAttributesCacheGenerator_U3CRealOneofCountU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13_CustomAttributesCacheGenerator_MessageDescriptor_get_Proto_mC19705FDFDE1B7189CE45B38771704217266D87C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13_CustomAttributesCacheGenerator_MessageDescriptor_get_ClrType_m67EDD051E54EF81C60E7C10479F1EAC25F222FFB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13_CustomAttributesCacheGenerator_MessageDescriptor_get_Parser_mBA1FD49A96FD0E2AC88955EA6BCF536555E9B68C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13_CustomAttributesCacheGenerator_MessageDescriptor_get_Fields_m7EC5DF1D21CA98B0E798ED2171C276DEDAC39352(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13_CustomAttributesCacheGenerator_MessageDescriptor_get_Extensions_mEE45D3BA082A29D28E2390D85156DD1E9F579155(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13_CustomAttributesCacheGenerator_MessageDescriptor_get_NestedTypes_m34A524E3C7D560C1537685CF6291AB60DA58FE7E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13_CustomAttributesCacheGenerator_MessageDescriptor_get_EnumTypes_m663896B42139FAB5706A6408FA931F95A60C3F39(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13_CustomAttributesCacheGenerator_MessageDescriptor_get_Oneofs_mC26EB7828846AC0A04AB6F892C4D22E79CD1E364(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FieldCollection_tF44D33A936EF52997608D546A3213EBE6C88F089_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * tmp = (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
}
static void U3CU3Ec__DisplayClass5_0_t96662B1133029EB10C2571FA476F2D85BF4DC246_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_t38FB397D8A68277FF9F30AA00AF8958BB3823E93_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OneofAccessor_t9CFA9BCA5E91251304EC17E543378C61EEE6FB5E_CustomAttributesCacheGenerator_U3CDescriptorU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OneofAccessor_t9CFA9BCA5E91251304EC17E543378C61EEE6FB5E_CustomAttributesCacheGenerator_OneofAccessor_get_Descriptor_mBDAF0F18B7C8EBBC4C2CF19A267C0AE3F09785C5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass4_0_t53CDDB11A5EFAB5E22115399290F1FBCD9E3F940_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OneofDescriptor_tAAAEA98A626026CC9ECA0E1A6171617F989251A8_CustomAttributesCacheGenerator_U3CIsSyntheticU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OneofDescriptor_tAAAEA98A626026CC9ECA0E1A6171617F989251A8_CustomAttributesCacheGenerator_OneofDescriptor_get_IsSynthetic_m22DC2931DF30D51ABB96C336D1F8A89F37898D17(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass4_0_tF01FC7AD83F6B0BD0D4219F54433D86DFDFC9C64_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 256LL, NULL);
	}
}
static void OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664_CustomAttributesCacheGenerator_U3CNameU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664_CustomAttributesCacheGenerator_U3CPreferredAliasU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664_CustomAttributesCacheGenerator_OriginalNameAttribute_get_Name_m35E068ED514B51E172CF5FE5DD917801F045B819(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664_CustomAttributesCacheGenerator_OriginalNameAttribute_set_Name_mC0A333EF33378D9143B3077B4E86270FE05636DA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664_CustomAttributesCacheGenerator_OriginalNameAttribute_get_PreferredAlias_m8B1D004B47D36373572F40238135A599E23095B8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664_CustomAttributesCacheGenerator_OriginalNameAttribute_set_PreferredAlias_m5717AA7D6B145D079B78DE15B31B88C8D6B4C258(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ReflectionUtil_t36D2A3AC0248B85EC6227BA3ED9C9A81DF922F1B_CustomAttributesCacheGenerator_U3CCanConvertEnumFuncToInt32FuncU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ReflectionUtil_t36D2A3AC0248B85EC6227BA3ED9C9A81DF922F1B_CustomAttributesCacheGenerator_ReflectionUtil_get_CanConvertEnumFuncToInt32Func_m14B4A020233464D727830248EED426EBDE5D45EF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass0_0_t411CCDB18E6786DA9842C0F860B5D7F572D47D1C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass0_1_t8D79113317861A64435B67C9AAADABC539A05BDF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass1_0_tF95E404D96623A7FAB2B210159D66C3AB53461C5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass2_0_t17B031C18B0B092BA00EAA06987631BA3DA05C2F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass3_0_t2C9AF141A4E8ED485E7FE20E761B468C013121C6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass4_0_tA0E15E14FEB071EAF5C50EB350EC0D65E1872BE6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass2_0_t0FD7FB796D1B047DC01102EFD39FCC9C44305BF9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass3_0_t4FF750E8071D8A8894D049B76A421C3F38696B52_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass3_1_tD197B16CADA61B4B3373290E8DE8C95247B27992_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass3_2_tBD615882E5FA463E9EBD94743AACBC9E8B5D161B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_t899158066C70DFF9C401972A4EEE5611EC4F9D96_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TypeRegistry_t82646E0517B3E7727E1973D5743C3D6416AAF476_CustomAttributesCacheGenerator_U3CEmptyU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TypeRegistry_t82646E0517B3E7727E1973D5743C3D6416AAF476_CustomAttributesCacheGenerator_TypeRegistry_get_Empty_mE5DF84BF4F8096E4E4DEC88EB1B60542256F54C9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MapField_2_tA4C73A6C6DF348AE54397C57349BB6DEC9146990_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * tmp = (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
}
static void U3CU3Ec__DisplayClass7_0_tE166C904C06F1F055457E76CE3BA0482830A888F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_t52B3A5F5968F590942B02BD2B1DDB8AF21861C46_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ProtobufEqualityComparers_tDE90D1B40462AFF4F1886F7315B35DE79C035C3E_CustomAttributesCacheGenerator_U3CBitwiseDoubleEqualityComparerU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ProtobufEqualityComparers_tDE90D1B40462AFF4F1886F7315B35DE79C035C3E_CustomAttributesCacheGenerator_U3CBitwiseSingleEqualityComparerU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ProtobufEqualityComparers_tDE90D1B40462AFF4F1886F7315B35DE79C035C3E_CustomAttributesCacheGenerator_U3CBitwiseNullableDoubleEqualityComparerU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ProtobufEqualityComparers_tDE90D1B40462AFF4F1886F7315B35DE79C035C3E_CustomAttributesCacheGenerator_U3CBitwiseNullableSingleEqualityComparerU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ProtobufEqualityComparers_tDE90D1B40462AFF4F1886F7315B35DE79C035C3E_CustomAttributesCacheGenerator_ProtobufEqualityComparers_get_BitwiseDoubleEqualityComparer_mE44F7862345571F0CA657DC541C39FE262543714(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ProtobufEqualityComparers_tDE90D1B40462AFF4F1886F7315B35DE79C035C3E_CustomAttributesCacheGenerator_ProtobufEqualityComparers_get_BitwiseSingleEqualityComparer_m5E6A6EB33216A3E9E829ED8FBA161E0B7BBCCBDB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ProtobufEqualityComparers_tDE90D1B40462AFF4F1886F7315B35DE79C035C3E_CustomAttributesCacheGenerator_ProtobufEqualityComparers_get_BitwiseNullableDoubleEqualityComparer_m137AF49F6F51AC6745E79BB1EDDA611DCDCEE632(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ProtobufEqualityComparers_tDE90D1B40462AFF4F1886F7315B35DE79C035C3E_CustomAttributesCacheGenerator_ProtobufEqualityComparers_get_BitwiseNullableSingleEqualityComparer_m903E7CC373C92BA08C7D8DC4B5569774DFB5A090(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RepeatedField_1_t26F74AB41CEB8AB4728C9AB65712DBBF2B738C89_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * tmp = (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
}
static void RepeatedField_1_t26F74AB41CEB8AB4728C9AB65712DBBF2B738C89_CustomAttributesCacheGenerator_RepeatedField_1_GetEnumerator_m2F086EA93B2B496851734024D484CAF9A8C67267(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CGetEnumeratorU3Ed__28_t1D89B8849E2BE4C57358D822F30C41B8A6D1339A_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CGetEnumeratorU3Ed__28_t1D89B8849E2BE4C57358D822F30C41B8A6D1339A_0_0_0_var), NULL);
	}
}
static void U3CGetEnumeratorU3Ed__28_t1D89B8849E2BE4C57358D822F30C41B8A6D1339A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGetEnumeratorU3Ed__28_t1D89B8849E2BE4C57358D822F30C41B8A6D1339A_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__28__ctor_mFABF109F443CD341CD6C0E0ECD1BA7651AC38C1F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetEnumeratorU3Ed__28_t1D89B8849E2BE4C57358D822F30C41B8A6D1339A_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__28_System_IDisposable_Dispose_mF014E23F4497286EFF080674AC6D3C697F9AA955(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetEnumeratorU3Ed__28_t1D89B8849E2BE4C57358D822F30C41B8A6D1339A_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__28_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_mCF5E0E46F0619B8F959C7714546CB8FE2479FAFA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetEnumeratorU3Ed__28_t1D89B8849E2BE4C57358D822F30C41B8A6D1339A_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__28_System_Collections_IEnumerator_Reset_m4D608FFBDDC33385D5EFE6E09923CC715DA6ECAC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetEnumeratorU3Ed__28_t1D89B8849E2BE4C57358D822F30C41B8A6D1339A_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__28_System_Collections_IEnumerator_get_Current_m0778F046811669B42A886BAAB48D7275BC6BAAE1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_Google_Protobuf_AttributeGenerators[];
const CustomAttributesCacheGenerator g_Google_Protobuf_AttributeGenerators[1169] = 
{
	EmbeddedAttribute_tCE17AEDD3793DB07D23D49461AD1F51E151C6173_CustomAttributesCacheGenerator,
	IsByRefLikeAttribute_t1F586EB86302883DEC1CCE8E0E6801E76D2601CC_CustomAttributesCacheGenerator,
	ByteString_t3B1232ECD50ECBDD385E2BF61FEEF3B17FC5D76D_CustomAttributesCacheGenerator,
	U3CU3Ec_tB01692A37F043FB22CAB0C7A8C9914F601DD29D8_CustomAttributesCacheGenerator,
	U3CU3Ec_t4E0AE2EB106891F592691D628191F93E2EA66145_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass32_0_1_tD4FAD14FB524139BEAB58126183FF9496AB3CEE5_CustomAttributesCacheGenerator,
	U3CU3Ec__32_1_tC96173B1C6BFD751304AC8417C90CFBB1BC5097B_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass38_0_tDCDB86507664B32348A36C610F1F3AA0E5A0D730_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass39_0_t6751F2CA2908287AED77B57BEE3DCED6B3A5259C_CustomAttributesCacheGenerator,
	U3CU3Ec_t3EDDCCC5BF90960C2C05B8B94DBC9D82D3D17E39_CustomAttributesCacheGenerator,
	U3CU3Ec_t5555F991758A0DD73D48BCE4AF9A1B14B7F05FA3_CustomAttributesCacheGenerator,
	MessageExtensions_tC8FEC406E671930064BE225D2C6DC69D04927A39_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass2_0_t0FCA4DEEC252A0D179F22DF669C875C54631E41E_CustomAttributesCacheGenerator,
	ParseContext_t288EAC5562A1D7E0FF8A06DBA58B8CF732ECE349_CustomAttributesCacheGenerator,
	WriteContext_t4011B33FF5B289B23B81D9929C82A44F0A24DFCB_CustomAttributesCacheGenerator,
	U3CU3Ec_t90AA77201246807290E9C9419E604313DEBA6726_CustomAttributesCacheGenerator,
	U3CU3Ec_tAFA19B90DD61DE127AEFD62A58A64551F09A27DF_CustomAttributesCacheGenerator,
	U3CU3Ec_t24742AB4C274E002E6BA5C149058C79E1CF86C01_CustomAttributesCacheGenerator,
	U3CU3Ec_t000235E6741E3C2385418F22D877987949E3CA0A_CustomAttributesCacheGenerator,
	U3CU3Ec_t08DCF5EE055E253C34F66A2152F923B976A9AE11_CustomAttributesCacheGenerator,
	U3CU3Ec_t98E1E2EA186D09F72BB3216F53457BD033D47266_CustomAttributesCacheGenerator,
	U3CU3Ec_t287C3274FF8FFE690CEB6CD883D55CD3988164AC_CustomAttributesCacheGenerator,
	U3CU3Ec_t50B4F58275D7F3C45E025DA55B9650988039B05D_CustomAttributesCacheGenerator,
	U3CU3Ec_tCFA8B3696619E2E5EF95E2EB6DA10ADCCB9194D5_CustomAttributesCacheGenerator,
	Types_tB8E14C4E98A889999072BB6C790763804958A51D_CustomAttributesCacheGenerator,
	U3CU3Ec_t00D69A0C542928EFCE9A40A967F51813B6AC2002_CustomAttributesCacheGenerator,
	U3CU3Ec_tECD5412AF4EC7A7A7A8189CC9BA414A243738353_CustomAttributesCacheGenerator,
	U3CU3Ec_t54E1462FB3B1DFA4137E99950F93B5F7565E957F_CustomAttributesCacheGenerator,
	U3CU3Ec_t087BC17745CAEFC744B320D652159EAED9BDA03F_CustomAttributesCacheGenerator,
	U3CU3Ec_t6FB6EF4327813A02BBA3BCF295054B27010543D9_CustomAttributesCacheGenerator,
	U3CU3Ec_t87ED2FDB0970A42C60EE9362A8A7AE6C1CB3B038_CustomAttributesCacheGenerator,
	Types_tD2B79473C8915A11C5D5EA0504D881CA5B76B4E6_CustomAttributesCacheGenerator,
	U3CU3Ec_tC3789B2A2639D39522B7FC88CE44B0FF78387D85_CustomAttributesCacheGenerator,
	U3CU3Ec_t14ED7AF2EE0452B68867D51843E5E3FBF40F7D2D_CustomAttributesCacheGenerator,
	U3CU3Ec_t89C33FBD99D7599051C877B6F08FD1378E2E3029_CustomAttributesCacheGenerator,
	U3CU3Ec_t79B3519CE8A82101DDF9B118AA4D84300712C1D4_CustomAttributesCacheGenerator,
	Types_tDF6C4DAD822ECAFD7D2E79C80BDDA72070A53F6C_CustomAttributesCacheGenerator,
	U3CU3Ec_tA2AE91D17E19F63E9678879CCF1B4D7A9471FD44_CustomAttributesCacheGenerator,
	U3CU3Ec_t6C0F3095056F0E5E4FE1BCCA0CD99FF73F8DC7A0_CustomAttributesCacheGenerator,
	Types_tCC3FD4FD7253BA6D7E02876756E671E1AF1F64DF_CustomAttributesCacheGenerator,
	U3CU3Ec_t0FB8D11271B41B39C63CE8A142DE7F8F87630794_CustomAttributesCacheGenerator,
	U3CU3Ec_tD1D7ABD9623694573682F9D85408033855EA825C_CustomAttributesCacheGenerator,
	U3CU3Ec_t79691CCE8A4B48B45898130F70DF802BAFBCF800_CustomAttributesCacheGenerator,
	U3CU3Ec_t58AC09743BD6215CAA60D27EDAD693FE4CE51008_CustomAttributesCacheGenerator,
	U3CU3Ec_tB84B815A20EC05C80390E84E48C3928E16E92F53_CustomAttributesCacheGenerator,
	Types_t60A4AC26614D017A3BEF3B1A7EC099FCC2E6511B_CustomAttributesCacheGenerator,
	U3CU3Ec_t04CDD685CEDF47C4A32C6D4A77AD0CD5E61B14D7_CustomAttributesCacheGenerator,
	U3CU3Ec_t7F53D6B7CEDEDD933BBD477D57B941004F7AABDB_CustomAttributesCacheGenerator,
	Types_t4FB39A98BCC7CF65EABE43A578A985F625E4E136_CustomAttributesCacheGenerator,
	U3CU3Ec_t44A43453DEA21C6FB8BEAABE8A1433BCB9168244_CustomAttributesCacheGenerator,
	U3CU3Ec_t555A80B41BAADB831E0BBE5DC035154EEC4697C7_CustomAttributesCacheGenerator,
	U3CU3Ec_tC4B2AD54A8F08DBE9BD6F9E0B367AE343715DF1F_CustomAttributesCacheGenerator,
	U3CU3Ec_t4C02F076494955F9B45D28530DBF54CE68974CD3_CustomAttributesCacheGenerator,
	U3CU3Ec_t6D3D46FF36020021A5BE4FD1D18730154BCE94FA_CustomAttributesCacheGenerator,
	Types_tB23AD38B494C3E7C0B0BA27DBC128F2E05A3EFD5_CustomAttributesCacheGenerator,
	U3CU3Ec_t1D94F8B22F7F01675BEF565B6100984E73D976A8_CustomAttributesCacheGenerator,
	Types_t9BD23E35FC17FB4A53AD87682F7E66FDDF6D22A5_CustomAttributesCacheGenerator,
	U3CU3Ec_t70C29F3AE73CD37C0D77DCC96A139645BE59A453_CustomAttributesCacheGenerator,
	U3CU3Ec_t4168CEB7B72566F0CB5098F58D358C4338EEA60F_CustomAttributesCacheGenerator,
	Types_t938123A603A401FC9E5EE0E7211A8C54532FD7B9_CustomAttributesCacheGenerator,
	U3CU3Ec_tE6E47C051562B8F8FCE9347E1B99439CF27AB1AF_CustomAttributesCacheGenerator,
	U3CU3Ec_tB54E6E726C6A3C0B8843E8A2452EABBDADEC9F28_CustomAttributesCacheGenerator,
	Types_t50292842BB5717A7DF5408D073E8593E5D93ABFA_CustomAttributesCacheGenerator,
	U3CU3Ec_t48164166658FE6E4AA4BC81699272A7660D5A345_CustomAttributesCacheGenerator,
	U3CU3Ec_tBD193B147926CA34945206F084AC82D159DAA97F_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass4_0_tC125EB0E5276232874526BC3333E73223EF8C09A_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass2_0_t69A25B41E29EF18D3906A126830960FA58B7E1B6_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass3_0_t07F42F32B123CC5B1CC4D48B3703EE051A52956A_CustomAttributesCacheGenerator,
	U3CU3Ec_tD9ABE4542EBE3438CDB091E751A07D8B9C876BB9_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass2_0_t2575137C3063791A132A5C6E4032E7B5290F484E_CustomAttributesCacheGenerator,
	U3CU3Ec_tF92FFF77B6725E8DCBB4BACE18AF8D12FCCFC182_CustomAttributesCacheGenerator,
	FieldCollection_tF44D33A936EF52997608D546A3213EBE6C88F089_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass5_0_t96662B1133029EB10C2571FA476F2D85BF4DC246_CustomAttributesCacheGenerator,
	U3CU3Ec_t38FB397D8A68277FF9F30AA00AF8958BB3823E93_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass4_0_t53CDDB11A5EFAB5E22115399290F1FBCD9E3F940_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass4_0_tF01FC7AD83F6B0BD0D4219F54433D86DFDFC9C64_CustomAttributesCacheGenerator,
	OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass0_0_t411CCDB18E6786DA9842C0F860B5D7F572D47D1C_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass0_1_t8D79113317861A64435B67C9AAADABC539A05BDF_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass1_0_tF95E404D96623A7FAB2B210159D66C3AB53461C5_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass2_0_t17B031C18B0B092BA00EAA06987631BA3DA05C2F_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass3_0_t2C9AF141A4E8ED485E7FE20E761B468C013121C6_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass4_0_tA0E15E14FEB071EAF5C50EB350EC0D65E1872BE6_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass2_0_t0FD7FB796D1B047DC01102EFD39FCC9C44305BF9_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass3_0_t4FF750E8071D8A8894D049B76A421C3F38696B52_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass3_1_tD197B16CADA61B4B3373290E8DE8C95247B27992_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass3_2_tBD615882E5FA463E9EBD94743AACBC9E8B5D161B_CustomAttributesCacheGenerator,
	U3CU3Ec_t899158066C70DFF9C401972A4EEE5611EC4F9D96_CustomAttributesCacheGenerator,
	MapField_2_tA4C73A6C6DF348AE54397C57349BB6DEC9146990_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass7_0_tE166C904C06F1F055457E76CE3BA0482830A888F_CustomAttributesCacheGenerator,
	U3CU3Ec_t52B3A5F5968F590942B02BD2B1DDB8AF21861C46_CustomAttributesCacheGenerator,
	RepeatedField_1_t26F74AB41CEB8AB4728C9AB65712DBBF2B738C89_CustomAttributesCacheGenerator,
	U3CGetEnumeratorU3Ed__28_t1D89B8849E2BE4C57358D822F30C41B8A6D1339A_CustomAttributesCacheGenerator,
	Extension_tB7B39D2C1DC9D59405BA177A9CDABC7926F94A18_CustomAttributesCacheGenerator_U3CFieldNumberU3Ek__BackingField,
	ExtensionSet_1_tFE071C084D6C1699836E2BD707D736B0862EF740_CustomAttributesCacheGenerator_U3CValuesByNumberU3Ek__BackingField,
	FieldCodec_1_t713DE327211056F9ECCB1D1CBD9A83826813DBEA_CustomAttributesCacheGenerator_U3CPackedRepeatedFieldU3Ek__BackingField,
	FieldCodec_1_t713DE327211056F9ECCB1D1CBD9A83826813DBEA_CustomAttributesCacheGenerator_U3CValueWriterU3Ek__BackingField,
	FieldCodec_1_t713DE327211056F9ECCB1D1CBD9A83826813DBEA_CustomAttributesCacheGenerator_U3CValueSizeCalculatorU3Ek__BackingField,
	FieldCodec_1_t713DE327211056F9ECCB1D1CBD9A83826813DBEA_CustomAttributesCacheGenerator_U3CValueReaderU3Ek__BackingField,
	FieldCodec_1_t713DE327211056F9ECCB1D1CBD9A83826813DBEA_CustomAttributesCacheGenerator_U3CValueMergerU3Ek__BackingField,
	FieldCodec_1_t713DE327211056F9ECCB1D1CBD9A83826813DBEA_CustomAttributesCacheGenerator_U3CFieldMergerU3Ek__BackingField,
	FieldCodec_1_t713DE327211056F9ECCB1D1CBD9A83826813DBEA_CustomAttributesCacheGenerator_U3CFixedSizeU3Ek__BackingField,
	FieldCodec_1_t713DE327211056F9ECCB1D1CBD9A83826813DBEA_CustomAttributesCacheGenerator_U3CTagU3Ek__BackingField,
	FieldCodec_1_t713DE327211056F9ECCB1D1CBD9A83826813DBEA_CustomAttributesCacheGenerator_U3CEndTagU3Ek__BackingField,
	FieldCodec_1_t713DE327211056F9ECCB1D1CBD9A83826813DBEA_CustomAttributesCacheGenerator_U3CDefaultValueU3Ek__BackingField,
	JsonFormatter_t29EDD2B5D01B4FD88FD4DBF274554818F8ED11C7_CustomAttributesCacheGenerator_U3CDefaultU3Ek__BackingField,
	Settings_t8850B4BBB68392D1D89673DC99317C64F663FECE_CustomAttributesCacheGenerator_U3CDefaultU3Ek__BackingField,
	Settings_t8850B4BBB68392D1D89673DC99317C64F663FECE_CustomAttributesCacheGenerator_U3CFormatDefaultValuesU3Ek__BackingField,
	Settings_t8850B4BBB68392D1D89673DC99317C64F663FECE_CustomAttributesCacheGenerator_U3CTypeRegistryU3Ek__BackingField,
	Settings_t8850B4BBB68392D1D89673DC99317C64F663FECE_CustomAttributesCacheGenerator_U3CFormatEnumsAsIntegersU3Ek__BackingField,
	MessageParser_tB0F97A0DCF31AD5D21F69684004C9745C88099FC_CustomAttributesCacheGenerator_U3CDiscardUnknownFieldsU3Ek__BackingField,
	MessageParser_tB0F97A0DCF31AD5D21F69684004C9745C88099FC_CustomAttributesCacheGenerator_U3CExtensionsU3Ek__BackingField,
	ParserInternalState_t08A261A9F88B2B769A10BED876C9622FEAFD2596_CustomAttributesCacheGenerator_U3CDiscardUnknownFieldsU3Ek__BackingField,
	ParserInternalState_t08A261A9F88B2B769A10BED876C9622FEAFD2596_CustomAttributesCacheGenerator_U3CExtensionRegistryU3Ek__BackingField,
	NullValue_tEC9E53C12DA09163F9D4C9BBA68385FEC355562A_CustomAttributesCacheGenerator_NullValue,
	Syntax_t8B3070D9FA595706155795F37EED0D87D0D308F8_CustomAttributesCacheGenerator_Proto2,
	Syntax_t8B3070D9FA595706155795F37EED0D87D0D308F8_CustomAttributesCacheGenerator_Proto3,
	Kind_tC27F5FC567168FED3D17ACA8C7E54C5769A4D532_CustomAttributesCacheGenerator_TypeUnknown,
	Kind_tC27F5FC567168FED3D17ACA8C7E54C5769A4D532_CustomAttributesCacheGenerator_TypeDouble,
	Kind_tC27F5FC567168FED3D17ACA8C7E54C5769A4D532_CustomAttributesCacheGenerator_TypeFloat,
	Kind_tC27F5FC567168FED3D17ACA8C7E54C5769A4D532_CustomAttributesCacheGenerator_TypeInt64,
	Kind_tC27F5FC567168FED3D17ACA8C7E54C5769A4D532_CustomAttributesCacheGenerator_TypeUint64,
	Kind_tC27F5FC567168FED3D17ACA8C7E54C5769A4D532_CustomAttributesCacheGenerator_TypeInt32,
	Kind_tC27F5FC567168FED3D17ACA8C7E54C5769A4D532_CustomAttributesCacheGenerator_TypeFixed64,
	Kind_tC27F5FC567168FED3D17ACA8C7E54C5769A4D532_CustomAttributesCacheGenerator_TypeFixed32,
	Kind_tC27F5FC567168FED3D17ACA8C7E54C5769A4D532_CustomAttributesCacheGenerator_TypeBool,
	Kind_tC27F5FC567168FED3D17ACA8C7E54C5769A4D532_CustomAttributesCacheGenerator_TypeString,
	Kind_tC27F5FC567168FED3D17ACA8C7E54C5769A4D532_CustomAttributesCacheGenerator_TypeGroup,
	Kind_tC27F5FC567168FED3D17ACA8C7E54C5769A4D532_CustomAttributesCacheGenerator_TypeMessage,
	Kind_tC27F5FC567168FED3D17ACA8C7E54C5769A4D532_CustomAttributesCacheGenerator_TypeBytes,
	Kind_tC27F5FC567168FED3D17ACA8C7E54C5769A4D532_CustomAttributesCacheGenerator_TypeUint32,
	Kind_tC27F5FC567168FED3D17ACA8C7E54C5769A4D532_CustomAttributesCacheGenerator_TypeEnum,
	Kind_tC27F5FC567168FED3D17ACA8C7E54C5769A4D532_CustomAttributesCacheGenerator_TypeSfixed32,
	Kind_tC27F5FC567168FED3D17ACA8C7E54C5769A4D532_CustomAttributesCacheGenerator_TypeSfixed64,
	Kind_tC27F5FC567168FED3D17ACA8C7E54C5769A4D532_CustomAttributesCacheGenerator_TypeSint32,
	Kind_tC27F5FC567168FED3D17ACA8C7E54C5769A4D532_CustomAttributesCacheGenerator_TypeSint64,
	Cardinality_tFEB23F7BB6AE0122DE5E13D7756F940FD86759F0_CustomAttributesCacheGenerator_Unknown,
	Cardinality_tFEB23F7BB6AE0122DE5E13D7756F940FD86759F0_CustomAttributesCacheGenerator_Optional,
	Cardinality_tFEB23F7BB6AE0122DE5E13D7756F940FD86759F0_CustomAttributesCacheGenerator_Required,
	Cardinality_tFEB23F7BB6AE0122DE5E13D7756F940FD86759F0_CustomAttributesCacheGenerator_Repeated,
	Type_tA2574C38CEE89946C1AFA608D4182429BB8490A9_CustomAttributesCacheGenerator_Double,
	Type_tA2574C38CEE89946C1AFA608D4182429BB8490A9_CustomAttributesCacheGenerator_Float,
	Type_tA2574C38CEE89946C1AFA608D4182429BB8490A9_CustomAttributesCacheGenerator_Int64,
	Type_tA2574C38CEE89946C1AFA608D4182429BB8490A9_CustomAttributesCacheGenerator_Uint64,
	Type_tA2574C38CEE89946C1AFA608D4182429BB8490A9_CustomAttributesCacheGenerator_Int32,
	Type_tA2574C38CEE89946C1AFA608D4182429BB8490A9_CustomAttributesCacheGenerator_Fixed64,
	Type_tA2574C38CEE89946C1AFA608D4182429BB8490A9_CustomAttributesCacheGenerator_Fixed32,
	Type_tA2574C38CEE89946C1AFA608D4182429BB8490A9_CustomAttributesCacheGenerator_Bool,
	Type_tA2574C38CEE89946C1AFA608D4182429BB8490A9_CustomAttributesCacheGenerator_String,
	Type_tA2574C38CEE89946C1AFA608D4182429BB8490A9_CustomAttributesCacheGenerator_Group,
	Type_tA2574C38CEE89946C1AFA608D4182429BB8490A9_CustomAttributesCacheGenerator_Message,
	Type_tA2574C38CEE89946C1AFA608D4182429BB8490A9_CustomAttributesCacheGenerator_Bytes,
	Type_tA2574C38CEE89946C1AFA608D4182429BB8490A9_CustomAttributesCacheGenerator_Uint32,
	Type_tA2574C38CEE89946C1AFA608D4182429BB8490A9_CustomAttributesCacheGenerator_Enum,
	Type_tA2574C38CEE89946C1AFA608D4182429BB8490A9_CustomAttributesCacheGenerator_Sfixed32,
	Type_tA2574C38CEE89946C1AFA608D4182429BB8490A9_CustomAttributesCacheGenerator_Sfixed64,
	Type_tA2574C38CEE89946C1AFA608D4182429BB8490A9_CustomAttributesCacheGenerator_Sint32,
	Type_tA2574C38CEE89946C1AFA608D4182429BB8490A9_CustomAttributesCacheGenerator_Sint64,
	Label_t5F834D524D61512BADE59642DE16426A43F3A2F7_CustomAttributesCacheGenerator_Optional,
	Label_t5F834D524D61512BADE59642DE16426A43F3A2F7_CustomAttributesCacheGenerator_Required,
	Label_t5F834D524D61512BADE59642DE16426A43F3A2F7_CustomAttributesCacheGenerator_Repeated,
	OptimizeMode_tD2A7B67F01AF2A53A5C73E35E3B0AD381FBEAF90_CustomAttributesCacheGenerator_Speed,
	OptimizeMode_tD2A7B67F01AF2A53A5C73E35E3B0AD381FBEAF90_CustomAttributesCacheGenerator_CodeSize,
	OptimizeMode_tD2A7B67F01AF2A53A5C73E35E3B0AD381FBEAF90_CustomAttributesCacheGenerator_LiteRuntime,
	CType_t8692E658D4DB9762025F3FF59D004E25835CF4FB_CustomAttributesCacheGenerator_String,
	CType_t8692E658D4DB9762025F3FF59D004E25835CF4FB_CustomAttributesCacheGenerator_Cord,
	CType_t8692E658D4DB9762025F3FF59D004E25835CF4FB_CustomAttributesCacheGenerator_StringPiece,
	JSType_t6C0B15B541F17DFCB78C389DE391B514E6BCD78B_CustomAttributesCacheGenerator_JsNormal,
	JSType_t6C0B15B541F17DFCB78C389DE391B514E6BCD78B_CustomAttributesCacheGenerator_JsString,
	JSType_t6C0B15B541F17DFCB78C389DE391B514E6BCD78B_CustomAttributesCacheGenerator_JsNumber,
	IdempotencyLevel_tF1CB740909022AF43C9AA8E9B0E20AF1D38649C3_CustomAttributesCacheGenerator_IdempotencyUnknown,
	IdempotencyLevel_tF1CB740909022AF43C9AA8E9B0E20AF1D38649C3_CustomAttributesCacheGenerator_NoSideEffects,
	IdempotencyLevel_tF1CB740909022AF43C9AA8E9B0E20AF1D38649C3_CustomAttributesCacheGenerator_Idempotent,
	DescriptorBase_tF6C2BA893A9882D75C7CD171CB21EBA88FA82CEA_CustomAttributesCacheGenerator_U3CIndexU3Ek__BackingField,
	DescriptorBase_tF6C2BA893A9882D75C7CD171CB21EBA88FA82CEA_CustomAttributesCacheGenerator_U3CFullNameU3Ek__BackingField,
	DescriptorBase_tF6C2BA893A9882D75C7CD171CB21EBA88FA82CEA_CustomAttributesCacheGenerator_U3CFileU3Ek__BackingField,
	DescriptorDeclaration_tA267DA7CAD0094614E29377401372F3EB4C395FB_CustomAttributesCacheGenerator_U3CDescriptorU3Ek__BackingField,
	DescriptorDeclaration_tA267DA7CAD0094614E29377401372F3EB4C395FB_CustomAttributesCacheGenerator_U3CStartLineU3Ek__BackingField,
	DescriptorDeclaration_tA267DA7CAD0094614E29377401372F3EB4C395FB_CustomAttributesCacheGenerator_U3CStartColumnU3Ek__BackingField,
	DescriptorDeclaration_tA267DA7CAD0094614E29377401372F3EB4C395FB_CustomAttributesCacheGenerator_U3CEndLineU3Ek__BackingField,
	DescriptorDeclaration_tA267DA7CAD0094614E29377401372F3EB4C395FB_CustomAttributesCacheGenerator_U3CEndColumnU3Ek__BackingField,
	DescriptorDeclaration_tA267DA7CAD0094614E29377401372F3EB4C395FB_CustomAttributesCacheGenerator_U3CLeadingCommentsU3Ek__BackingField,
	DescriptorDeclaration_tA267DA7CAD0094614E29377401372F3EB4C395FB_CustomAttributesCacheGenerator_U3CTrailingCommentsU3Ek__BackingField,
	DescriptorDeclaration_tA267DA7CAD0094614E29377401372F3EB4C395FB_CustomAttributesCacheGenerator_U3CLeadingDetachedCommentsU3Ek__BackingField,
	ExtensionAccessor_t8FD60C5A0CEA561F542BE3A83BD8634D11C38118_CustomAttributesCacheGenerator_U3CDescriptorU3Ek__BackingField,
	ExtensionCollection_t52756747D345D5B934DCD6FF9876823214980772_CustomAttributesCacheGenerator_U3CUnorderedExtensionsU3Ek__BackingField,
	FieldDescriptor_t0D57515C5C0DD2F7D37E1EB3838AE950F7C609BD_CustomAttributesCacheGenerator_U3CContainingTypeU3Ek__BackingField,
	FieldDescriptor_t0D57515C5C0DD2F7D37E1EB3838AE950F7C609BD_CustomAttributesCacheGenerator_U3CContainingOneofU3Ek__BackingField,
	FieldDescriptor_t0D57515C5C0DD2F7D37E1EB3838AE950F7C609BD_CustomAttributesCacheGenerator_U3CJsonNameU3Ek__BackingField,
	FieldDescriptor_t0D57515C5C0DD2F7D37E1EB3838AE950F7C609BD_CustomAttributesCacheGenerator_U3CProtoU3Ek__BackingField,
	FieldDescriptor_t0D57515C5C0DD2F7D37E1EB3838AE950F7C609BD_CustomAttributesCacheGenerator_U3CExtensionU3Ek__BackingField,
	FileDescriptor_tA2E229ECE64C897445BBCA0229BD41F8E028ECD4_CustomAttributesCacheGenerator_U3CProtoU3Ek__BackingField,
	FileDescriptor_tA2E229ECE64C897445BBCA0229BD41F8E028ECD4_CustomAttributesCacheGenerator_U3CSyntaxU3Ek__BackingField,
	FileDescriptor_tA2E229ECE64C897445BBCA0229BD41F8E028ECD4_CustomAttributesCacheGenerator_U3CMessageTypesU3Ek__BackingField,
	FileDescriptor_tA2E229ECE64C897445BBCA0229BD41F8E028ECD4_CustomAttributesCacheGenerator_U3CEnumTypesU3Ek__BackingField,
	FileDescriptor_tA2E229ECE64C897445BBCA0229BD41F8E028ECD4_CustomAttributesCacheGenerator_U3CServicesU3Ek__BackingField,
	FileDescriptor_tA2E229ECE64C897445BBCA0229BD41F8E028ECD4_CustomAttributesCacheGenerator_U3CExtensionsU3Ek__BackingField,
	FileDescriptor_tA2E229ECE64C897445BBCA0229BD41F8E028ECD4_CustomAttributesCacheGenerator_U3CDependenciesU3Ek__BackingField,
	FileDescriptor_tA2E229ECE64C897445BBCA0229BD41F8E028ECD4_CustomAttributesCacheGenerator_U3CPublicDependenciesU3Ek__BackingField,
	FileDescriptor_tA2E229ECE64C897445BBCA0229BD41F8E028ECD4_CustomAttributesCacheGenerator_U3CSerializedDataU3Ek__BackingField,
	FileDescriptor_tA2E229ECE64C897445BBCA0229BD41F8E028ECD4_CustomAttributesCacheGenerator_U3CDescriptorPoolU3Ek__BackingField,
	GeneratedClrTypeInfo_t26E71D04A6D31D12375C412ACFDF303153379EDE_CustomAttributesCacheGenerator_U3CClrTypeU3Ek__BackingField,
	GeneratedClrTypeInfo_t26E71D04A6D31D12375C412ACFDF303153379EDE_CustomAttributesCacheGenerator_U3CParserU3Ek__BackingField,
	GeneratedClrTypeInfo_t26E71D04A6D31D12375C412ACFDF303153379EDE_CustomAttributesCacheGenerator_U3CPropertyNamesU3Ek__BackingField,
	GeneratedClrTypeInfo_t26E71D04A6D31D12375C412ACFDF303153379EDE_CustomAttributesCacheGenerator_U3CExtensionsU3Ek__BackingField,
	GeneratedClrTypeInfo_t26E71D04A6D31D12375C412ACFDF303153379EDE_CustomAttributesCacheGenerator_U3COneofNamesU3Ek__BackingField,
	GeneratedClrTypeInfo_t26E71D04A6D31D12375C412ACFDF303153379EDE_CustomAttributesCacheGenerator_U3CNestedTypesU3Ek__BackingField,
	GeneratedClrTypeInfo_t26E71D04A6D31D12375C412ACFDF303153379EDE_CustomAttributesCacheGenerator_U3CNestedEnumsU3Ek__BackingField,
	MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13_CustomAttributesCacheGenerator_U3CProtoU3Ek__BackingField,
	MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13_CustomAttributesCacheGenerator_U3CClrTypeU3Ek__BackingField,
	MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13_CustomAttributesCacheGenerator_U3CParserU3Ek__BackingField,
	MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13_CustomAttributesCacheGenerator_U3CContainingTypeU3Ek__BackingField,
	MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13_CustomAttributesCacheGenerator_U3CFieldsU3Ek__BackingField,
	MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13_CustomAttributesCacheGenerator_U3CExtensionsU3Ek__BackingField,
	MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13_CustomAttributesCacheGenerator_U3CNestedTypesU3Ek__BackingField,
	MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13_CustomAttributesCacheGenerator_U3CEnumTypesU3Ek__BackingField,
	MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13_CustomAttributesCacheGenerator_U3COneofsU3Ek__BackingField,
	MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13_CustomAttributesCacheGenerator_U3CRealOneofCountU3Ek__BackingField,
	OneofAccessor_t9CFA9BCA5E91251304EC17E543378C61EEE6FB5E_CustomAttributesCacheGenerator_U3CDescriptorU3Ek__BackingField,
	OneofDescriptor_tAAAEA98A626026CC9ECA0E1A6171617F989251A8_CustomAttributesCacheGenerator_U3CIsSyntheticU3Ek__BackingField,
	OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664_CustomAttributesCacheGenerator_U3CNameU3Ek__BackingField,
	OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664_CustomAttributesCacheGenerator_U3CPreferredAliasU3Ek__BackingField,
	ReflectionUtil_t36D2A3AC0248B85EC6227BA3ED9C9A81DF922F1B_CustomAttributesCacheGenerator_U3CCanConvertEnumFuncToInt32FuncU3Ek__BackingField,
	TypeRegistry_t82646E0517B3E7727E1973D5743C3D6416AAF476_CustomAttributesCacheGenerator_U3CEmptyU3Ek__BackingField,
	ProtobufEqualityComparers_tDE90D1B40462AFF4F1886F7315B35DE79C035C3E_CustomAttributesCacheGenerator_U3CBitwiseDoubleEqualityComparerU3Ek__BackingField,
	ProtobufEqualityComparers_tDE90D1B40462AFF4F1886F7315B35DE79C035C3E_CustomAttributesCacheGenerator_U3CBitwiseSingleEqualityComparerU3Ek__BackingField,
	ProtobufEqualityComparers_tDE90D1B40462AFF4F1886F7315B35DE79C035C3E_CustomAttributesCacheGenerator_U3CBitwiseNullableDoubleEqualityComparerU3Ek__BackingField,
	ProtobufEqualityComparers_tDE90D1B40462AFF4F1886F7315B35DE79C035C3E_CustomAttributesCacheGenerator_U3CBitwiseNullableSingleEqualityComparerU3Ek__BackingField,
	Extension_tB7B39D2C1DC9D59405BA177A9CDABC7926F94A18_CustomAttributesCacheGenerator_Extension_get_FieldNumber_mA033A36AD6AC429AFD166C84A6BBB13ECD45B579,
	ExtensionSet_1_tFE071C084D6C1699836E2BD707D736B0862EF740_CustomAttributesCacheGenerator_ExtensionSet_1_get_ValuesByNumber_m0299171B7A7A6527A32C02787EA4CE60FC51FA37,
	FieldCodec_1_t713DE327211056F9ECCB1D1CBD9A83826813DBEA_CustomAttributesCacheGenerator_FieldCodec_1_get_PackedRepeatedField_m1D3D6BC695091ED29719A16A83C734449B3FA569,
	FieldCodec_1_t713DE327211056F9ECCB1D1CBD9A83826813DBEA_CustomAttributesCacheGenerator_FieldCodec_1_get_ValueWriter_m3579154316AAF4F1A6107B36039D864772957EBF,
	FieldCodec_1_t713DE327211056F9ECCB1D1CBD9A83826813DBEA_CustomAttributesCacheGenerator_FieldCodec_1_get_ValueSizeCalculator_m0ABFD83323840A9C4545573D572CA2B19CA45928,
	FieldCodec_1_t713DE327211056F9ECCB1D1CBD9A83826813DBEA_CustomAttributesCacheGenerator_FieldCodec_1_get_ValueReader_m04C98DF25AB7E34AA61DD75D2F990AAEC2591B98,
	FieldCodec_1_t713DE327211056F9ECCB1D1CBD9A83826813DBEA_CustomAttributesCacheGenerator_FieldCodec_1_get_ValueMerger_mA0DBB20140AC7E2B4DFF6E2CE1C7078946C2F54E,
	FieldCodec_1_t713DE327211056F9ECCB1D1CBD9A83826813DBEA_CustomAttributesCacheGenerator_FieldCodec_1_get_FieldMerger_mDECE5C7CFE87D0D2E2668F931C61DA46FB8263F8,
	FieldCodec_1_t713DE327211056F9ECCB1D1CBD9A83826813DBEA_CustomAttributesCacheGenerator_FieldCodec_1_get_FixedSize_m9CEFCB6C3142F3ED071D19E26E67C4350CB7D8D1,
	FieldCodec_1_t713DE327211056F9ECCB1D1CBD9A83826813DBEA_CustomAttributesCacheGenerator_FieldCodec_1_get_Tag_m83C2F3733CD96ADE96EDB69552B6C248CBD3E9CA,
	FieldCodec_1_t713DE327211056F9ECCB1D1CBD9A83826813DBEA_CustomAttributesCacheGenerator_FieldCodec_1_get_EndTag_m11129D6820FC5559FCDD388BF1230AE401344E38,
	FieldCodec_1_t713DE327211056F9ECCB1D1CBD9A83826813DBEA_CustomAttributesCacheGenerator_FieldCodec_1_get_DefaultValue_m3CCF3372ED00CB6EA3E14ED9BF5F9B8726FD5133,
	JsonFormatter_t29EDD2B5D01B4FD88FD4DBF274554818F8ED11C7_CustomAttributesCacheGenerator_JsonFormatter_get_Default_m5A9C0948E8C0D01D6700160BED899AD2507140D3,
	Settings_t8850B4BBB68392D1D89673DC99317C64F663FECE_CustomAttributesCacheGenerator_Settings_get_Default_mF4FDE9D3D751BD702D8219E4EB5517E0F6FF9B74,
	Settings_t8850B4BBB68392D1D89673DC99317C64F663FECE_CustomAttributesCacheGenerator_Settings_get_FormatDefaultValues_m64C456D788C62CB3E0F0996AFF989CC6981239F3,
	Settings_t8850B4BBB68392D1D89673DC99317C64F663FECE_CustomAttributesCacheGenerator_Settings_get_TypeRegistry_m9BA673D58183796B0F0C9E2DBBE89F432A6D6D0E,
	Settings_t8850B4BBB68392D1D89673DC99317C64F663FECE_CustomAttributesCacheGenerator_Settings_get_FormatEnumsAsIntegers_m51CB1E83B7EA54E4D894263F570805407D05CFE1,
	MessageExtensions_tC8FEC406E671930064BE225D2C6DC69D04927A39_CustomAttributesCacheGenerator_MessageExtensions_ToByteArray_m1BBBB6D4DC2D766396375663F29F4427A33D8812,
	MessageExtensions_tC8FEC406E671930064BE225D2C6DC69D04927A39_CustomAttributesCacheGenerator_MessageExtensions_MergeFrom_m1473F9DF3353F288CF4A04AA5CE23D014542C5F4,
	MessageExtensions_tC8FEC406E671930064BE225D2C6DC69D04927A39_CustomAttributesCacheGenerator_MessageExtensions_MergeFrom_mF7F80928C53902B587749A72D0569EFF68C10DA6,
	MessageParser_tB0F97A0DCF31AD5D21F69684004C9745C88099FC_CustomAttributesCacheGenerator_MessageParser_get_DiscardUnknownFields_m1B7BDCE4283878E28C0B4ACB3944EF71ED872B98,
	MessageParser_tB0F97A0DCF31AD5D21F69684004C9745C88099FC_CustomAttributesCacheGenerator_MessageParser_get_Extensions_m13D4911AAC2CFF3977A4EF7A60853D1234526737,
	ParserInternalState_t08A261A9F88B2B769A10BED876C9622FEAFD2596_CustomAttributesCacheGenerator_ParserInternalState_get_DiscardUnknownFields_mABFB91062F6B1A730ABD81691F138C516B0E3020,
	ParserInternalState_t08A261A9F88B2B769A10BED876C9622FEAFD2596_CustomAttributesCacheGenerator_ParserInternalState_set_DiscardUnknownFields_m6374B0D645A0352B3BF53F9B0EFEF62E45501C58,
	ParserInternalState_t08A261A9F88B2B769A10BED876C9622FEAFD2596_CustomAttributesCacheGenerator_ParserInternalState_get_ExtensionRegistry_m8ABCB61553B7732DBB4DB08481D898E5479347CE,
	ParserInternalState_t08A261A9F88B2B769A10BED876C9622FEAFD2596_CustomAttributesCacheGenerator_ParserInternalState_set_ExtensionRegistry_mEA5D5DD0A75713D26A2300501B940EB7A67C2B5C,
	Any_t92B39FAFBB91BF3E94AAA6527FC7D6B113D7D811_CustomAttributesCacheGenerator_Any__ctor_m1ECC62A42AB856A583601A266C7C8CF336408E90,
	Any_t92B39FAFBB91BF3E94AAA6527FC7D6B113D7D811_CustomAttributesCacheGenerator_Any__ctor_m2376F8C7176FFA131593622A480CE332EB4B1B13,
	Any_t92B39FAFBB91BF3E94AAA6527FC7D6B113D7D811_CustomAttributesCacheGenerator_Any_Clone_m0DED49DFACD7ED97904D1D4290A65A1C494FDA6F,
	Any_t92B39FAFBB91BF3E94AAA6527FC7D6B113D7D811_CustomAttributesCacheGenerator_Any_Equals_mD55E6718944CD8AA56B9AFC8B9B584BE6E4BEF43,
	Any_t92B39FAFBB91BF3E94AAA6527FC7D6B113D7D811_CustomAttributesCacheGenerator_Any_Equals_m39E1D5E36B954E909B5F6C5D904430B5064AB976,
	Any_t92B39FAFBB91BF3E94AAA6527FC7D6B113D7D811_CustomAttributesCacheGenerator_Any_GetHashCode_m07B1193DDB01F335CE42B217AE9BD3A88C6848FB,
	Any_t92B39FAFBB91BF3E94AAA6527FC7D6B113D7D811_CustomAttributesCacheGenerator_Any_ToString_m0A75195213C8DF63A33CA3775B9684EEED9E9749,
	Any_t92B39FAFBB91BF3E94AAA6527FC7D6B113D7D811_CustomAttributesCacheGenerator_Any_WriteTo_m4F0C9449ECFD97252B6CC7090CACD8A532BD2BFB,
	Any_t92B39FAFBB91BF3E94AAA6527FC7D6B113D7D811_CustomAttributesCacheGenerator_Any_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m1A5C5A2DE6524DE3A5A86FB9E4AD5422AFA61562,
	Any_t92B39FAFBB91BF3E94AAA6527FC7D6B113D7D811_CustomAttributesCacheGenerator_Any_CalculateSize_m0CC466C0EA20B4192A897E77F5AEDD4E22E6B9A4,
	Any_t92B39FAFBB91BF3E94AAA6527FC7D6B113D7D811_CustomAttributesCacheGenerator_Any_MergeFrom_mB3B78279933A5AA5BA4B45E7EB58A4F133BF7177,
	Any_t92B39FAFBB91BF3E94AAA6527FC7D6B113D7D811_CustomAttributesCacheGenerator_Any_MergeFrom_mBFB2D9E1EC1825E910098EDFEA6547ABB949BECD,
	Any_t92B39FAFBB91BF3E94AAA6527FC7D6B113D7D811_CustomAttributesCacheGenerator_Any_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_mF51F0CE65F862C0DCAF37AD560D439D63DB570CB,
	Duration_t40CF63035DE7C0F4A03867C1128E4BB2CCF12A80_CustomAttributesCacheGenerator_Duration__ctor_m6E8317977C3ABF38F7C0DF31CF9FD578DB563EEA,
	Duration_t40CF63035DE7C0F4A03867C1128E4BB2CCF12A80_CustomAttributesCacheGenerator_Duration__ctor_mE32B96FC0271D3892E7E9B684B384787EF78E56D,
	Duration_t40CF63035DE7C0F4A03867C1128E4BB2CCF12A80_CustomAttributesCacheGenerator_Duration_Clone_mF481B40FEAFC57DDFA82F9357B1E7BCB2A5645EE,
	Duration_t40CF63035DE7C0F4A03867C1128E4BB2CCF12A80_CustomAttributesCacheGenerator_Duration_Equals_mC8E45E2881848B04EF30A2A9F507A755840D8895,
	Duration_t40CF63035DE7C0F4A03867C1128E4BB2CCF12A80_CustomAttributesCacheGenerator_Duration_Equals_m1BE5DD19D22AF8643B4CE200DE7F7F79853D4292,
	Duration_t40CF63035DE7C0F4A03867C1128E4BB2CCF12A80_CustomAttributesCacheGenerator_Duration_GetHashCode_m9177E77337F424B4F7B8895403B9E472EF1C98D8,
	Duration_t40CF63035DE7C0F4A03867C1128E4BB2CCF12A80_CustomAttributesCacheGenerator_Duration_ToString_m9A4C19C321E83FD1E47AC98C2F8A2513B2580900,
	Duration_t40CF63035DE7C0F4A03867C1128E4BB2CCF12A80_CustomAttributesCacheGenerator_Duration_WriteTo_mBB57F86544E6C7F06853460506A3C7534FFDBC5D,
	Duration_t40CF63035DE7C0F4A03867C1128E4BB2CCF12A80_CustomAttributesCacheGenerator_Duration_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_mBA1C04DAEA64BD35140B8B97E305116436D657A3,
	Duration_t40CF63035DE7C0F4A03867C1128E4BB2CCF12A80_CustomAttributesCacheGenerator_Duration_CalculateSize_m9E15FCF69AB0CE774A486CB3C0B5C9AA6417EABF,
	Duration_t40CF63035DE7C0F4A03867C1128E4BB2CCF12A80_CustomAttributesCacheGenerator_Duration_MergeFrom_m5815E64B100ABE517B1AF02E6EEEE2F9937C6EE9,
	Duration_t40CF63035DE7C0F4A03867C1128E4BB2CCF12A80_CustomAttributesCacheGenerator_Duration_MergeFrom_m4150356C75EC52495E8CF0B15F5A1832ACBE8F8A,
	Duration_t40CF63035DE7C0F4A03867C1128E4BB2CCF12A80_CustomAttributesCacheGenerator_Duration_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m03941E2F76DE634FD45F0AD9FF80E8FDD8FC8B3C,
	FieldMask_t0F76999F69A7AA8A3E19EC1466CE438ABB2FA273_CustomAttributesCacheGenerator_FieldMask__ctor_m01D7985ADE2286627A933B2F86237E0C8483D3D5,
	FieldMask_t0F76999F69A7AA8A3E19EC1466CE438ABB2FA273_CustomAttributesCacheGenerator_FieldMask__ctor_m77703B996B4EAD8BD22AA0AC7E4C9C768137D853,
	FieldMask_t0F76999F69A7AA8A3E19EC1466CE438ABB2FA273_CustomAttributesCacheGenerator_FieldMask_Clone_m2EDD32CF5E0AC140742096AEFE1DAE3ED453CCA3,
	FieldMask_t0F76999F69A7AA8A3E19EC1466CE438ABB2FA273_CustomAttributesCacheGenerator_FieldMask_Equals_m1B5C027B9B0A1A66D8B177DAAB4EDBD5FD1442C1,
	FieldMask_t0F76999F69A7AA8A3E19EC1466CE438ABB2FA273_CustomAttributesCacheGenerator_FieldMask_Equals_mC23F829C421F954D073A6C046B614DBB687E41AB,
	FieldMask_t0F76999F69A7AA8A3E19EC1466CE438ABB2FA273_CustomAttributesCacheGenerator_FieldMask_GetHashCode_mA97105104D19E8CF467DE10EAFC05DC28B86F2D2,
	FieldMask_t0F76999F69A7AA8A3E19EC1466CE438ABB2FA273_CustomAttributesCacheGenerator_FieldMask_ToString_m89F5E655ABCBA6ACC9D9B86BE367D9919A837036,
	FieldMask_t0F76999F69A7AA8A3E19EC1466CE438ABB2FA273_CustomAttributesCacheGenerator_FieldMask_WriteTo_mE9F97A05551D2F6FBCDF475C259D1BEE4B9ECA43,
	FieldMask_t0F76999F69A7AA8A3E19EC1466CE438ABB2FA273_CustomAttributesCacheGenerator_FieldMask_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m663519CE13FFC51F40560EE1C564F4C017133C59,
	FieldMask_t0F76999F69A7AA8A3E19EC1466CE438ABB2FA273_CustomAttributesCacheGenerator_FieldMask_CalculateSize_m0A39301CC699EEA0A57D8D8E4846C58F734D8EA5,
	FieldMask_t0F76999F69A7AA8A3E19EC1466CE438ABB2FA273_CustomAttributesCacheGenerator_FieldMask_MergeFrom_mEAF05299B243344C2E5FFF1D601F5EA18BAB9F00,
	FieldMask_t0F76999F69A7AA8A3E19EC1466CE438ABB2FA273_CustomAttributesCacheGenerator_FieldMask_MergeFrom_mBB29E5E3787C88D4F6813175DF0F3D7E5F8E1E0C,
	FieldMask_t0F76999F69A7AA8A3E19EC1466CE438ABB2FA273_CustomAttributesCacheGenerator_FieldMask_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_mA91B89E0519001C72B2CF796A4969D2D87CE04DB,
	SourceContext_t7616D6280D262E1831459B553153AB5D68F5077B_CustomAttributesCacheGenerator_SourceContext__ctor_m7DC6A0CEA2DB342A90B3111F54EE7FE20035ED7A,
	SourceContext_t7616D6280D262E1831459B553153AB5D68F5077B_CustomAttributesCacheGenerator_SourceContext__ctor_m53B1A691B9A243427E519584FAA277F82940830C,
	SourceContext_t7616D6280D262E1831459B553153AB5D68F5077B_CustomAttributesCacheGenerator_SourceContext_Clone_mECE4CC09F0CFD276692AED613523558C5B35FF13,
	SourceContext_t7616D6280D262E1831459B553153AB5D68F5077B_CustomAttributesCacheGenerator_SourceContext_Equals_m2404E1B68FD460455E54DD5DA372BA7DD10A56A5,
	SourceContext_t7616D6280D262E1831459B553153AB5D68F5077B_CustomAttributesCacheGenerator_SourceContext_Equals_mDA27BD7606F7EA489A2C1AE54476ED5E0E357143,
	SourceContext_t7616D6280D262E1831459B553153AB5D68F5077B_CustomAttributesCacheGenerator_SourceContext_GetHashCode_m3AF81E3DD69DB21BF3B9DB93411901F8F0C4C4D9,
	SourceContext_t7616D6280D262E1831459B553153AB5D68F5077B_CustomAttributesCacheGenerator_SourceContext_ToString_mCC00619AD401EE2827C8F568E6798478F398F6F1,
	SourceContext_t7616D6280D262E1831459B553153AB5D68F5077B_CustomAttributesCacheGenerator_SourceContext_WriteTo_m29132C1DE0B5144E19D8889A192E79AEDDABA8ED,
	SourceContext_t7616D6280D262E1831459B553153AB5D68F5077B_CustomAttributesCacheGenerator_SourceContext_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m2C30589A2E794D1D6F5D64B6DC35C253E7261066,
	SourceContext_t7616D6280D262E1831459B553153AB5D68F5077B_CustomAttributesCacheGenerator_SourceContext_CalculateSize_mC5AD31CF54346C37E07C001C6A2BD8DCEC68F1F6,
	SourceContext_t7616D6280D262E1831459B553153AB5D68F5077B_CustomAttributesCacheGenerator_SourceContext_MergeFrom_m02826B16AA69141109E52E02757E9B1AF009ACD4,
	SourceContext_t7616D6280D262E1831459B553153AB5D68F5077B_CustomAttributesCacheGenerator_SourceContext_MergeFrom_m052D6EAFE9072262116999725700C7FF8FCB45C1,
	SourceContext_t7616D6280D262E1831459B553153AB5D68F5077B_CustomAttributesCacheGenerator_SourceContext_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_mADDDB228A85C7C3C457347DC2D13075031FD4271,
	Struct_t4E66B704BB3E73B19AFFF2A87EB12ED16C7C4DD3_CustomAttributesCacheGenerator_Struct__ctor_m11D17B249FA390528B0E14F386ADA2577C6A871C,
	Struct_t4E66B704BB3E73B19AFFF2A87EB12ED16C7C4DD3_CustomAttributesCacheGenerator_Struct__ctor_m608179184571F7E0C2F5879E37CF80A00B2B8D7A,
	Struct_t4E66B704BB3E73B19AFFF2A87EB12ED16C7C4DD3_CustomAttributesCacheGenerator_Struct_Clone_m0D1BB90781C14E60D625538A9A19D696120475BE,
	Struct_t4E66B704BB3E73B19AFFF2A87EB12ED16C7C4DD3_CustomAttributesCacheGenerator_Struct_Equals_m549285D67C027225573A1DB67C5301A0D6454BFA,
	Struct_t4E66B704BB3E73B19AFFF2A87EB12ED16C7C4DD3_CustomAttributesCacheGenerator_Struct_Equals_m2532A70D51E9BAB2D6D51B68E969EF10BDA2F728,
	Struct_t4E66B704BB3E73B19AFFF2A87EB12ED16C7C4DD3_CustomAttributesCacheGenerator_Struct_GetHashCode_mB35A1295AA470E34F0B3283C926A85E1DC10C61E,
	Struct_t4E66B704BB3E73B19AFFF2A87EB12ED16C7C4DD3_CustomAttributesCacheGenerator_Struct_ToString_mB7E3408DD1CC7EA7EF64A6C0AAF69344623328B9,
	Struct_t4E66B704BB3E73B19AFFF2A87EB12ED16C7C4DD3_CustomAttributesCacheGenerator_Struct_WriteTo_mB8C1FE19A7AD7FA903647D06FF0DCF1970A32D29,
	Struct_t4E66B704BB3E73B19AFFF2A87EB12ED16C7C4DD3_CustomAttributesCacheGenerator_Struct_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_mB4878B03C7F2037F3A6A0B017A57F95D785F0B7F,
	Struct_t4E66B704BB3E73B19AFFF2A87EB12ED16C7C4DD3_CustomAttributesCacheGenerator_Struct_CalculateSize_m04764057A3243E48A65E84C497924FE3AE6BEDDC,
	Struct_t4E66B704BB3E73B19AFFF2A87EB12ED16C7C4DD3_CustomAttributesCacheGenerator_Struct_MergeFrom_m6AA74D28AA5FDA2211BE8FB37B3E8709136F90B1,
	Struct_t4E66B704BB3E73B19AFFF2A87EB12ED16C7C4DD3_CustomAttributesCacheGenerator_Struct_MergeFrom_m6DA60767E2F54627C88F498B3F3D2348F0908D17,
	Struct_t4E66B704BB3E73B19AFFF2A87EB12ED16C7C4DD3_CustomAttributesCacheGenerator_Struct_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m48DE2FD28260E4F6EB713D4B2309D7ECC5A3DCAF,
	Value_t4A50D780AAEDFF7855F6732AA0799306161698CB_CustomAttributesCacheGenerator_Value__ctor_mB36B9A9D61FC019A45BC871BA1478BB50D60E78B,
	Value_t4A50D780AAEDFF7855F6732AA0799306161698CB_CustomAttributesCacheGenerator_Value__ctor_m44F71F1C4FF2ED09299528B51BDBA4E73788D40E,
	Value_t4A50D780AAEDFF7855F6732AA0799306161698CB_CustomAttributesCacheGenerator_Value_Clone_mACDC4288492E8D9B24FE707A400B1445AD78477F,
	Value_t4A50D780AAEDFF7855F6732AA0799306161698CB_CustomAttributesCacheGenerator_Value_Equals_mE007E6C81CFC39B3A5BA78873297DB9C2305C650,
	Value_t4A50D780AAEDFF7855F6732AA0799306161698CB_CustomAttributesCacheGenerator_Value_Equals_m5F2BB40CD83A5988F958848F4410366EB7EC59B4,
	Value_t4A50D780AAEDFF7855F6732AA0799306161698CB_CustomAttributesCacheGenerator_Value_GetHashCode_m7C544251FD75746E81866D5D36924010535124E6,
	Value_t4A50D780AAEDFF7855F6732AA0799306161698CB_CustomAttributesCacheGenerator_Value_ToString_m0435FF1515A70A900374F24375010C41124BFFC1,
	Value_t4A50D780AAEDFF7855F6732AA0799306161698CB_CustomAttributesCacheGenerator_Value_WriteTo_m707E9B65D4E8B28CA233D2CC91203558071F8147,
	Value_t4A50D780AAEDFF7855F6732AA0799306161698CB_CustomAttributesCacheGenerator_Value_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m8F8E38353725C50938560EC4F37B5F22C75BA7FC,
	Value_t4A50D780AAEDFF7855F6732AA0799306161698CB_CustomAttributesCacheGenerator_Value_CalculateSize_m0249098B9D7B5CB6074121B23B55E79FFB7FC9BA,
	Value_t4A50D780AAEDFF7855F6732AA0799306161698CB_CustomAttributesCacheGenerator_Value_MergeFrom_mFFA75CAC017BBD75F8C0E116E299BDAC427EF1C5,
	Value_t4A50D780AAEDFF7855F6732AA0799306161698CB_CustomAttributesCacheGenerator_Value_MergeFrom_mDFF91157418DCFA0541BAC4E77C0D8E24CCDC62B,
	Value_t4A50D780AAEDFF7855F6732AA0799306161698CB_CustomAttributesCacheGenerator_Value_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_mE15A6602E4EDEAB62505CC0E7AD868CBD7CDE78C,
	ListValue_t11D7F704E07B21FD590F1D7C14E83402D497D40F_CustomAttributesCacheGenerator_ListValue__ctor_mB9FA676E88F0EA554534D3FFCBECA11AC459A22A,
	ListValue_t11D7F704E07B21FD590F1D7C14E83402D497D40F_CustomAttributesCacheGenerator_ListValue__ctor_m28B1D38DB2026931E319F255DEF52941857CE536,
	ListValue_t11D7F704E07B21FD590F1D7C14E83402D497D40F_CustomAttributesCacheGenerator_ListValue_Clone_mAE7626FCDDF9C2C6BA65B98C47E483B8C84E800A,
	ListValue_t11D7F704E07B21FD590F1D7C14E83402D497D40F_CustomAttributesCacheGenerator_ListValue_Equals_mF92FCAB22A5B02906FEFBA93776FDA4996C0B65B,
	ListValue_t11D7F704E07B21FD590F1D7C14E83402D497D40F_CustomAttributesCacheGenerator_ListValue_Equals_mAFE9E0D31D3CFEE41502588A065E6B9ED7C0663F,
	ListValue_t11D7F704E07B21FD590F1D7C14E83402D497D40F_CustomAttributesCacheGenerator_ListValue_GetHashCode_mBAB9D28DF3E73B510E816CFF5C20774383DDA53B,
	ListValue_t11D7F704E07B21FD590F1D7C14E83402D497D40F_CustomAttributesCacheGenerator_ListValue_ToString_mE555FF95A4A538FD306A7DBD94DA019BCC737EDB,
	ListValue_t11D7F704E07B21FD590F1D7C14E83402D497D40F_CustomAttributesCacheGenerator_ListValue_WriteTo_m2200555A25434B19BE50039FFD72B23E104A4A11,
	ListValue_t11D7F704E07B21FD590F1D7C14E83402D497D40F_CustomAttributesCacheGenerator_ListValue_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_mC52AE62570621C7C4CA377BD233B78BCBC0178FE,
	ListValue_t11D7F704E07B21FD590F1D7C14E83402D497D40F_CustomAttributesCacheGenerator_ListValue_CalculateSize_m997191D513733E472C8705995F87C8119B6FDADC,
	ListValue_t11D7F704E07B21FD590F1D7C14E83402D497D40F_CustomAttributesCacheGenerator_ListValue_MergeFrom_mFD20BFF52DA1BFB3F54CC90BE5674A118FC5177B,
	ListValue_t11D7F704E07B21FD590F1D7C14E83402D497D40F_CustomAttributesCacheGenerator_ListValue_MergeFrom_m384866A7A963FFBF044B0FC082ADD6A9AA6A4CE2,
	ListValue_t11D7F704E07B21FD590F1D7C14E83402D497D40F_CustomAttributesCacheGenerator_ListValue_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m5E6C1BE581E812B32831F486A67887D377AE6EED,
	Timestamp_t9150476DBE9622F93C1BB771873B5E1B5FA45929_CustomAttributesCacheGenerator_Timestamp__ctor_m8F321B7F87FD2782A209D5251E2509121F91E084,
	Timestamp_t9150476DBE9622F93C1BB771873B5E1B5FA45929_CustomAttributesCacheGenerator_Timestamp__ctor_m2664E78CE259B3480E632DA374D7295D9511E644,
	Timestamp_t9150476DBE9622F93C1BB771873B5E1B5FA45929_CustomAttributesCacheGenerator_Timestamp_Clone_mBF7E47B82DDCD226CD188726A48B380D7340F063,
	Timestamp_t9150476DBE9622F93C1BB771873B5E1B5FA45929_CustomAttributesCacheGenerator_Timestamp_Equals_m43A5485ADA83370447C6ED05E83D93E9FD61F075,
	Timestamp_t9150476DBE9622F93C1BB771873B5E1B5FA45929_CustomAttributesCacheGenerator_Timestamp_Equals_m09CB0CAAD41410377A04A2A6B6F93E232A2FD558,
	Timestamp_t9150476DBE9622F93C1BB771873B5E1B5FA45929_CustomAttributesCacheGenerator_Timestamp_GetHashCode_m4EF62C49B8D0D43BDCDF3AE4115D63442DCB6AF7,
	Timestamp_t9150476DBE9622F93C1BB771873B5E1B5FA45929_CustomAttributesCacheGenerator_Timestamp_ToString_m54CE7B61A60BD4FBE9FA386E69B256FD9CAB0766,
	Timestamp_t9150476DBE9622F93C1BB771873B5E1B5FA45929_CustomAttributesCacheGenerator_Timestamp_WriteTo_m7A41B3EBAEDF1DB8EBA83393E27C1D47642EF1E6,
	Timestamp_t9150476DBE9622F93C1BB771873B5E1B5FA45929_CustomAttributesCacheGenerator_Timestamp_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m931F0E47EBF9936D807F0165935BE4D7B4DD6D33,
	Timestamp_t9150476DBE9622F93C1BB771873B5E1B5FA45929_CustomAttributesCacheGenerator_Timestamp_CalculateSize_mB8AA31386FAA6594A82EBFDA83023F01849EBA42,
	Timestamp_t9150476DBE9622F93C1BB771873B5E1B5FA45929_CustomAttributesCacheGenerator_Timestamp_MergeFrom_mB073280D4CC7A8E531F5BE75B1390589537790A4,
	Timestamp_t9150476DBE9622F93C1BB771873B5E1B5FA45929_CustomAttributesCacheGenerator_Timestamp_MergeFrom_m47E1E135BC450678854DD2704A26607F69516BBF,
	Timestamp_t9150476DBE9622F93C1BB771873B5E1B5FA45929_CustomAttributesCacheGenerator_Timestamp_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m667DF5C1AC94559C80FE08DC714B1E2943D6C16D,
	Type_t1A1580695FE92D3355A256FC0A40309494A8C356_CustomAttributesCacheGenerator_Type__ctor_m72B7DAE9051977572D45AFCF5B543AEB9E84A71F,
	Type_t1A1580695FE92D3355A256FC0A40309494A8C356_CustomAttributesCacheGenerator_Type__ctor_m7E6669C7E329257A31BA69B14587D6D7739C49DE,
	Type_t1A1580695FE92D3355A256FC0A40309494A8C356_CustomAttributesCacheGenerator_Type_Clone_m45297B0AFB39C2C32EDE75B17513877EAE97D1CC,
	Type_t1A1580695FE92D3355A256FC0A40309494A8C356_CustomAttributesCacheGenerator_Type_Equals_mCC222586E6D1B96613944E3D66BEC72D362D9D63,
	Type_t1A1580695FE92D3355A256FC0A40309494A8C356_CustomAttributesCacheGenerator_Type_Equals_mE4A16EB9E9DA3DD6BBCFD983D5E76A3B7EF7699B,
	Type_t1A1580695FE92D3355A256FC0A40309494A8C356_CustomAttributesCacheGenerator_Type_GetHashCode_m620763684AFC519635D83A5038D9874C8D840B43,
	Type_t1A1580695FE92D3355A256FC0A40309494A8C356_CustomAttributesCacheGenerator_Type_ToString_m220D186C08F0D2510BEBAD9ADA9B2AFC9D9C7A2B,
	Type_t1A1580695FE92D3355A256FC0A40309494A8C356_CustomAttributesCacheGenerator_Type_WriteTo_mEAC9994F15ADD598116489C2F5DC26A213181E74,
	Type_t1A1580695FE92D3355A256FC0A40309494A8C356_CustomAttributesCacheGenerator_Type_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_mF4AB433048B17B8767AE79040EDF944BF398D8C4,
	Type_t1A1580695FE92D3355A256FC0A40309494A8C356_CustomAttributesCacheGenerator_Type_CalculateSize_m5AE90542655AB79BC6184ACB7A226E0D004371AA,
	Type_t1A1580695FE92D3355A256FC0A40309494A8C356_CustomAttributesCacheGenerator_Type_MergeFrom_m3128264DCA457EBEF76A421739ECF34A8244D3F3,
	Type_t1A1580695FE92D3355A256FC0A40309494A8C356_CustomAttributesCacheGenerator_Type_MergeFrom_m71E0740B14181FDFFDA3FE9A92DE72D39471830D,
	Type_t1A1580695FE92D3355A256FC0A40309494A8C356_CustomAttributesCacheGenerator_Type_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_mEC3240A4B5DB410713B7697F2A15EA65B3C67EDB,
	Field_t79E55EF79874715920B1FA76F36897A5D907AD7C_CustomAttributesCacheGenerator_Field__ctor_m083271BB3DE4449286983AFE69B71C9DE2D68BE8,
	Field_t79E55EF79874715920B1FA76F36897A5D907AD7C_CustomAttributesCacheGenerator_Field__ctor_mF445407AC817770B1DD11ED1FC30695C18F0B76B,
	Field_t79E55EF79874715920B1FA76F36897A5D907AD7C_CustomAttributesCacheGenerator_Field_Clone_m0CC33BAC52277DA85D019E632E4BEF45987DF7D7,
	Field_t79E55EF79874715920B1FA76F36897A5D907AD7C_CustomAttributesCacheGenerator_Field_Equals_m4954B931DEB49176652C38AC8B8047EB9CF2D083,
	Field_t79E55EF79874715920B1FA76F36897A5D907AD7C_CustomAttributesCacheGenerator_Field_Equals_m98EF1C08EB6139CFAC12DDAF6587098D0965F8B8,
	Field_t79E55EF79874715920B1FA76F36897A5D907AD7C_CustomAttributesCacheGenerator_Field_GetHashCode_m9920F826AB54CAC69212B120FA5196570B2EBF0F,
	Field_t79E55EF79874715920B1FA76F36897A5D907AD7C_CustomAttributesCacheGenerator_Field_ToString_mC417A2D84A15530EB338A880F26CAF5B899096D1,
	Field_t79E55EF79874715920B1FA76F36897A5D907AD7C_CustomAttributesCacheGenerator_Field_WriteTo_mE97048ABCAD56F48771C6005B929D1D16822F5F9,
	Field_t79E55EF79874715920B1FA76F36897A5D907AD7C_CustomAttributesCacheGenerator_Field_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m1403C5B7A9323AAEEE2ECD2E2282CDADD1D3EBBA,
	Field_t79E55EF79874715920B1FA76F36897A5D907AD7C_CustomAttributesCacheGenerator_Field_CalculateSize_m29958B0C393E23281EEBB33DE7F86329A78120EA,
	Field_t79E55EF79874715920B1FA76F36897A5D907AD7C_CustomAttributesCacheGenerator_Field_MergeFrom_m79C0F4297EB25A155F74B1D4FD88149786994F5A,
	Field_t79E55EF79874715920B1FA76F36897A5D907AD7C_CustomAttributesCacheGenerator_Field_MergeFrom_m9C1775ED4AE6363E980B965D39DE0651A2A30AA8,
	Field_t79E55EF79874715920B1FA76F36897A5D907AD7C_CustomAttributesCacheGenerator_Field_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_mDCEE2693E502312C726C28AB4F137EAC8AB12640,
	Enum_tF9B938E22D1A8C7105A3A0A97AF51439675CEF87_CustomAttributesCacheGenerator_Enum__ctor_m6E1C81B05018715B930662010FE77501F5394204,
	Enum_tF9B938E22D1A8C7105A3A0A97AF51439675CEF87_CustomAttributesCacheGenerator_Enum__ctor_m48DDA58AECBCB975A007B170E29CAB580FE5880F,
	Enum_tF9B938E22D1A8C7105A3A0A97AF51439675CEF87_CustomAttributesCacheGenerator_Enum_Clone_mC8C340BFE917B4374A1D33C35CAC2FBFFB43D904,
	Enum_tF9B938E22D1A8C7105A3A0A97AF51439675CEF87_CustomAttributesCacheGenerator_Enum_Equals_mC1F65CAC46C0ADE5A1D38C9FA48A3DED3D4DCE1C,
	Enum_tF9B938E22D1A8C7105A3A0A97AF51439675CEF87_CustomAttributesCacheGenerator_Enum_Equals_mFC4CEF2A5597BC4AABEC5B7CC79F18274CC8D7B6,
	Enum_tF9B938E22D1A8C7105A3A0A97AF51439675CEF87_CustomAttributesCacheGenerator_Enum_GetHashCode_m84A65EAEBAA7FDBE3579FB7E085A667AB6069380,
	Enum_tF9B938E22D1A8C7105A3A0A97AF51439675CEF87_CustomAttributesCacheGenerator_Enum_ToString_m8755890EAF8E3CBD38454AAEE50C219D06BC2AC1,
	Enum_tF9B938E22D1A8C7105A3A0A97AF51439675CEF87_CustomAttributesCacheGenerator_Enum_WriteTo_m21A724F3E7465A22E8D2933F97B7AAEA97DDAAD1,
	Enum_tF9B938E22D1A8C7105A3A0A97AF51439675CEF87_CustomAttributesCacheGenerator_Enum_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m9E010AB642C27A55002C1798086F1126F5C28E91,
	Enum_tF9B938E22D1A8C7105A3A0A97AF51439675CEF87_CustomAttributesCacheGenerator_Enum_CalculateSize_m6AE4EEC93B797E6CF87A9C0F14F2DCE78D805A4A,
	Enum_tF9B938E22D1A8C7105A3A0A97AF51439675CEF87_CustomAttributesCacheGenerator_Enum_MergeFrom_m02C3F847695806FAA2E6041572102F44B894C9D6,
	Enum_tF9B938E22D1A8C7105A3A0A97AF51439675CEF87_CustomAttributesCacheGenerator_Enum_MergeFrom_mAC108FDF195D5796D71C464C308BEFE7F9BF0B51,
	Enum_tF9B938E22D1A8C7105A3A0A97AF51439675CEF87_CustomAttributesCacheGenerator_Enum_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_mDFD4A7F6E813A69B629AFE079EFC31645ABC2881,
	EnumValue_t24721FCB233B8D4D64FB5DC62D67C63AB4C3B5AE_CustomAttributesCacheGenerator_EnumValue__ctor_m1F9880D30AB86CC9184A09C9244767AD02DBCDAB,
	EnumValue_t24721FCB233B8D4D64FB5DC62D67C63AB4C3B5AE_CustomAttributesCacheGenerator_EnumValue__ctor_mBEB46ABD1DA26AC98C6D2B3CC17B43F3FACE79CD,
	EnumValue_t24721FCB233B8D4D64FB5DC62D67C63AB4C3B5AE_CustomAttributesCacheGenerator_EnumValue_Clone_mAD4F136B74E13F29C57FD9BA516D5690D0D8A2E5,
	EnumValue_t24721FCB233B8D4D64FB5DC62D67C63AB4C3B5AE_CustomAttributesCacheGenerator_EnumValue_Equals_mC7EAF672AF155B8A6E5E83A30AFA5C8C737549CD,
	EnumValue_t24721FCB233B8D4D64FB5DC62D67C63AB4C3B5AE_CustomAttributesCacheGenerator_EnumValue_Equals_m300A77FE3BEA40D06F264421A4DF508A35FFFE42,
	EnumValue_t24721FCB233B8D4D64FB5DC62D67C63AB4C3B5AE_CustomAttributesCacheGenerator_EnumValue_GetHashCode_m0A4617C5682F4BFD756D4E0027B99FB3C9916D0A,
	EnumValue_t24721FCB233B8D4D64FB5DC62D67C63AB4C3B5AE_CustomAttributesCacheGenerator_EnumValue_ToString_mE37319FEB422F63DAE1B920B3F47712CD57858D6,
	EnumValue_t24721FCB233B8D4D64FB5DC62D67C63AB4C3B5AE_CustomAttributesCacheGenerator_EnumValue_WriteTo_mAF6C20F5FC69A6D7FB9990941A81887DFC056FBA,
	EnumValue_t24721FCB233B8D4D64FB5DC62D67C63AB4C3B5AE_CustomAttributesCacheGenerator_EnumValue_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m2D246ECE495DC0034AACF00A7F7D7C749B605088,
	EnumValue_t24721FCB233B8D4D64FB5DC62D67C63AB4C3B5AE_CustomAttributesCacheGenerator_EnumValue_CalculateSize_m4ED2A3BCDB5B45F73CEB8F384D1ABEEDA417A7CF,
	EnumValue_t24721FCB233B8D4D64FB5DC62D67C63AB4C3B5AE_CustomAttributesCacheGenerator_EnumValue_MergeFrom_m16FE96C76760F5E983CDC9DA7E9E6608E718AB2B,
	EnumValue_t24721FCB233B8D4D64FB5DC62D67C63AB4C3B5AE_CustomAttributesCacheGenerator_EnumValue_MergeFrom_mE6B7795A557E42BECB42E02D36B871A5AF3F6F2A,
	EnumValue_t24721FCB233B8D4D64FB5DC62D67C63AB4C3B5AE_CustomAttributesCacheGenerator_EnumValue_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m00F8E5F6718E02F5C8D361D25E5941380E4414C3,
	Option_tF75313E4E874EF52E854420D3DEEA04A99AE0E7F_CustomAttributesCacheGenerator_Option__ctor_m61127A3EBF04C32BAECB3FDC43C06A4A0BC34D55,
	Option_tF75313E4E874EF52E854420D3DEEA04A99AE0E7F_CustomAttributesCacheGenerator_Option__ctor_mE400E557A643D6FD4336467A7CA7F4EBE15B193B,
	Option_tF75313E4E874EF52E854420D3DEEA04A99AE0E7F_CustomAttributesCacheGenerator_Option_Clone_mE36C75B283328B159094B29AD7C378CF96029557,
	Option_tF75313E4E874EF52E854420D3DEEA04A99AE0E7F_CustomAttributesCacheGenerator_Option_Equals_m5EE432C5466D73AD284F80AC21D7B022855176E8,
	Option_tF75313E4E874EF52E854420D3DEEA04A99AE0E7F_CustomAttributesCacheGenerator_Option_Equals_m851A5A03B1AD4D16DFC04D52FA7548AF198392DD,
	Option_tF75313E4E874EF52E854420D3DEEA04A99AE0E7F_CustomAttributesCacheGenerator_Option_GetHashCode_mAB0CB2FD7D75D98123988AECFD80B5C37B4A02D9,
	Option_tF75313E4E874EF52E854420D3DEEA04A99AE0E7F_CustomAttributesCacheGenerator_Option_ToString_m87855F8C2E044740FD0712D0F8CF3DBE3143BF82,
	Option_tF75313E4E874EF52E854420D3DEEA04A99AE0E7F_CustomAttributesCacheGenerator_Option_WriteTo_m08FFCCEC210A886C2FC6AD2D163A500877A28531,
	Option_tF75313E4E874EF52E854420D3DEEA04A99AE0E7F_CustomAttributesCacheGenerator_Option_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_mB2C264C7D44F83BE2E6D5E68825776101ADDD61C,
	Option_tF75313E4E874EF52E854420D3DEEA04A99AE0E7F_CustomAttributesCacheGenerator_Option_CalculateSize_mFF4098BC49D348AADE7E6B68D8976452F255D94D,
	Option_tF75313E4E874EF52E854420D3DEEA04A99AE0E7F_CustomAttributesCacheGenerator_Option_MergeFrom_mA6F300660905E229B30AF09C9207C6EF44E158B9,
	Option_tF75313E4E874EF52E854420D3DEEA04A99AE0E7F_CustomAttributesCacheGenerator_Option_MergeFrom_m9EA5877DB4934007C01D74759B0E63925094987D,
	Option_tF75313E4E874EF52E854420D3DEEA04A99AE0E7F_CustomAttributesCacheGenerator_Option_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m1CA4CEF42E2A2A7ADD75A49F9CBE0A644C0D01D6,
	FileDescriptorSet_t033C2CF3F219A2421776E1BD1042AC2757EFAC3E_CustomAttributesCacheGenerator_FileDescriptorSet__ctor_mA7A07A046215E12A519D8592D84CC8E5D77DE8A4,
	FileDescriptorSet_t033C2CF3F219A2421776E1BD1042AC2757EFAC3E_CustomAttributesCacheGenerator_FileDescriptorSet__ctor_m109C9F09C6370D20B70E70630294D2C09790FB73,
	FileDescriptorSet_t033C2CF3F219A2421776E1BD1042AC2757EFAC3E_CustomAttributesCacheGenerator_FileDescriptorSet_Clone_m265C516E8B9C78BD5CF6FC647677B44EA22C19C1,
	FileDescriptorSet_t033C2CF3F219A2421776E1BD1042AC2757EFAC3E_CustomAttributesCacheGenerator_FileDescriptorSet_Equals_mF6380585A3C4BCBB875C05218072B5FC9E7BEDA4,
	FileDescriptorSet_t033C2CF3F219A2421776E1BD1042AC2757EFAC3E_CustomAttributesCacheGenerator_FileDescriptorSet_Equals_m6FB81A95B00A2BA8007CD098A1E64BC53CB1E8DE,
	FileDescriptorSet_t033C2CF3F219A2421776E1BD1042AC2757EFAC3E_CustomAttributesCacheGenerator_FileDescriptorSet_GetHashCode_m5E8932B0438992144D00152506F56564FC008603,
	FileDescriptorSet_t033C2CF3F219A2421776E1BD1042AC2757EFAC3E_CustomAttributesCacheGenerator_FileDescriptorSet_ToString_m55C6B1E4124A8667B7A472041A15DFA788312E7A,
	FileDescriptorSet_t033C2CF3F219A2421776E1BD1042AC2757EFAC3E_CustomAttributesCacheGenerator_FileDescriptorSet_WriteTo_m906D2C2B397CDAFF5EF8D444C80AEB8410EAB931,
	FileDescriptorSet_t033C2CF3F219A2421776E1BD1042AC2757EFAC3E_CustomAttributesCacheGenerator_FileDescriptorSet_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m07D693FF17AE52BE5EBFD65CC7132C30BD5F1838,
	FileDescriptorSet_t033C2CF3F219A2421776E1BD1042AC2757EFAC3E_CustomAttributesCacheGenerator_FileDescriptorSet_CalculateSize_m71F5ECD29922E5260C9F27F5E9158338697E1A6B,
	FileDescriptorSet_t033C2CF3F219A2421776E1BD1042AC2757EFAC3E_CustomAttributesCacheGenerator_FileDescriptorSet_MergeFrom_m4AD41D3A6331B1B8AA293288747EE68F38E15C41,
	FileDescriptorSet_t033C2CF3F219A2421776E1BD1042AC2757EFAC3E_CustomAttributesCacheGenerator_FileDescriptorSet_MergeFrom_m6A837636C5DF50AB53B23E53FE1FC765B6828B27,
	FileDescriptorSet_t033C2CF3F219A2421776E1BD1042AC2757EFAC3E_CustomAttributesCacheGenerator_FileDescriptorSet_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m30A4D0A83029F411C07921760B1EF8F53B90D6B9,
	FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0_CustomAttributesCacheGenerator_FileDescriptorProto__ctor_m4485617D3F5C6C7A0B6803FB7A6A9E5CD9289E68,
	FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0_CustomAttributesCacheGenerator_FileDescriptorProto__ctor_m0E39D73BEB59D91943EF2EA1E8B43205EE2AD0D8,
	FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0_CustomAttributesCacheGenerator_FileDescriptorProto_Clone_m8A232664925A576E17BAEEE2B7C78110936F0378,
	FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0_CustomAttributesCacheGenerator_FileDescriptorProto_Equals_mB2CE0475BC597D2DD9156FE405B673152788F432,
	FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0_CustomAttributesCacheGenerator_FileDescriptorProto_Equals_mC0682C79CEED2451C8CDFE709CF4F49307B2FB3A,
	FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0_CustomAttributesCacheGenerator_FileDescriptorProto_GetHashCode_mE4A7E0861EF395F56F05D05B158256F22EE5670A,
	FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0_CustomAttributesCacheGenerator_FileDescriptorProto_ToString_m570AF4B70A7741E75E2065FA39DCF185C8D94CF9,
	FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0_CustomAttributesCacheGenerator_FileDescriptorProto_WriteTo_m29D8F0A32F233CEBB2F2FBCB590D4739642AA0C2,
	FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0_CustomAttributesCacheGenerator_FileDescriptorProto_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m00C04878C6E3CEEAD15F30CCF8564EB613B2AE9B,
	FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0_CustomAttributesCacheGenerator_FileDescriptorProto_CalculateSize_m7F8750FEC9F67C17825224AF365936D26A5AD58F,
	FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0_CustomAttributesCacheGenerator_FileDescriptorProto_MergeFrom_mC85444AE37C8A342C2E0AFCA376603A29A4FDA3D,
	FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0_CustomAttributesCacheGenerator_FileDescriptorProto_MergeFrom_mEE0DD8720B946B400CFEC3D6C2BC73470BCF592E,
	FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0_CustomAttributesCacheGenerator_FileDescriptorProto_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m2ED80E57E0627E94AE7DA4F37CAD6021096B16FC,
	DescriptorProto_tB03AF5EFB75071C3E950F726F78461450496C898_CustomAttributesCacheGenerator_DescriptorProto__ctor_mBA850C613C3DB05A54F24C5E732A05CB9EEA4A4E,
	DescriptorProto_tB03AF5EFB75071C3E950F726F78461450496C898_CustomAttributesCacheGenerator_DescriptorProto__ctor_mCF435119FBBAA9A389619587C9435EC7EC24ED46,
	DescriptorProto_tB03AF5EFB75071C3E950F726F78461450496C898_CustomAttributesCacheGenerator_DescriptorProto_Clone_m8D6A43AB22810E2ABA42EE59D221FFD6F7F7DD58,
	DescriptorProto_tB03AF5EFB75071C3E950F726F78461450496C898_CustomAttributesCacheGenerator_DescriptorProto_Equals_mFBBBAFCA37FEC00E0D4DDE7758E3F23405F72323,
	DescriptorProto_tB03AF5EFB75071C3E950F726F78461450496C898_CustomAttributesCacheGenerator_DescriptorProto_Equals_m2254305BFFB53FE27C4AA5B4E0FC5C1580C7230F,
	DescriptorProto_tB03AF5EFB75071C3E950F726F78461450496C898_CustomAttributesCacheGenerator_DescriptorProto_GetHashCode_m158B9AA4C433C9EEE8BAB3C7C702E46277B64AA5,
	DescriptorProto_tB03AF5EFB75071C3E950F726F78461450496C898_CustomAttributesCacheGenerator_DescriptorProto_ToString_m7300A8A26C0B22F794F933459B56D6B86DF4854B,
	DescriptorProto_tB03AF5EFB75071C3E950F726F78461450496C898_CustomAttributesCacheGenerator_DescriptorProto_WriteTo_mA8BED1F35DFFC420B71BC788C94C974AF366A735,
	DescriptorProto_tB03AF5EFB75071C3E950F726F78461450496C898_CustomAttributesCacheGenerator_DescriptorProto_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m08457FFB232FE6F6902B30F8E14731C57D6FF845,
	DescriptorProto_tB03AF5EFB75071C3E950F726F78461450496C898_CustomAttributesCacheGenerator_DescriptorProto_CalculateSize_mD15A93668659599FB42FF86E335DBCCBB5D828F6,
	DescriptorProto_tB03AF5EFB75071C3E950F726F78461450496C898_CustomAttributesCacheGenerator_DescriptorProto_MergeFrom_m3309DEA6819B3F251E2F114761961232F3A555EF,
	DescriptorProto_tB03AF5EFB75071C3E950F726F78461450496C898_CustomAttributesCacheGenerator_DescriptorProto_MergeFrom_m186FD354E99638C8B67744AE7ED9C2556DCC9E24,
	DescriptorProto_tB03AF5EFB75071C3E950F726F78461450496C898_CustomAttributesCacheGenerator_DescriptorProto_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_mB4A4E23A065E9FD8A6B8030ED9498F52A2181CD2,
	ExtensionRange_t355AED379D3458AC80443047C2DF72AA44639D84_CustomAttributesCacheGenerator_ExtensionRange__ctor_mCE3CBBCDC18FA5EA4ADBF4AF4AF53D13327D323B,
	ExtensionRange_t355AED379D3458AC80443047C2DF72AA44639D84_CustomAttributesCacheGenerator_ExtensionRange__ctor_m86DE2DE4CC81E1797CDC04A3068161C7AE6137D5,
	ExtensionRange_t355AED379D3458AC80443047C2DF72AA44639D84_CustomAttributesCacheGenerator_ExtensionRange_Clone_mFFA2D7061BFDF6189B1DAAD7388E9C545D8495C9,
	ExtensionRange_t355AED379D3458AC80443047C2DF72AA44639D84_CustomAttributesCacheGenerator_ExtensionRange_Equals_mC16F46A587D2727AB7527B37898019C9EC0436FC,
	ExtensionRange_t355AED379D3458AC80443047C2DF72AA44639D84_CustomAttributesCacheGenerator_ExtensionRange_Equals_mBCE0F486ED3163380B949A8F4654DC2359A0EA61,
	ExtensionRange_t355AED379D3458AC80443047C2DF72AA44639D84_CustomAttributesCacheGenerator_ExtensionRange_GetHashCode_m1705F6C558349578176C5BA9C2CD918B6A6689E8,
	ExtensionRange_t355AED379D3458AC80443047C2DF72AA44639D84_CustomAttributesCacheGenerator_ExtensionRange_ToString_m92C8CC6CEE16909BD1FA3C1323DC5A214AF7FAC4,
	ExtensionRange_t355AED379D3458AC80443047C2DF72AA44639D84_CustomAttributesCacheGenerator_ExtensionRange_WriteTo_mCC7686D9A689AC98ED8D425B3131531FFD927C77,
	ExtensionRange_t355AED379D3458AC80443047C2DF72AA44639D84_CustomAttributesCacheGenerator_ExtensionRange_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m78E864903CC6C7966483D1C1DADD8FE35236BD02,
	ExtensionRange_t355AED379D3458AC80443047C2DF72AA44639D84_CustomAttributesCacheGenerator_ExtensionRange_CalculateSize_mBE8901808FC96A0B6647FEE680B06880F0FA84E7,
	ExtensionRange_t355AED379D3458AC80443047C2DF72AA44639D84_CustomAttributesCacheGenerator_ExtensionRange_MergeFrom_m314E38B926BE9F9F1CD9E34681625888E95EB00E,
	ExtensionRange_t355AED379D3458AC80443047C2DF72AA44639D84_CustomAttributesCacheGenerator_ExtensionRange_MergeFrom_m99ECB1EF2D098752D7635C356C1CFA50784EB3EA,
	ExtensionRange_t355AED379D3458AC80443047C2DF72AA44639D84_CustomAttributesCacheGenerator_ExtensionRange_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m24A97A8F83256C686E2E8AE441EF5A58C03244EE,
	ReservedRange_tAED8F62AF040F5F133732196696893B313EA458B_CustomAttributesCacheGenerator_ReservedRange__ctor_mB2F8257CCD2E526672CB4B552FD6799706F38CD5,
	ReservedRange_tAED8F62AF040F5F133732196696893B313EA458B_CustomAttributesCacheGenerator_ReservedRange__ctor_m73F9F5F5A8EE837C56568DD545DA961703779FAD,
	ReservedRange_tAED8F62AF040F5F133732196696893B313EA458B_CustomAttributesCacheGenerator_ReservedRange_Clone_m30F1DBAF17F68D2418492E3502CC6D8EA1CBDCBB,
	ReservedRange_tAED8F62AF040F5F133732196696893B313EA458B_CustomAttributesCacheGenerator_ReservedRange_Equals_m1C20E897B3FFD56BAB9E3248E741DFD243C3EC67,
	ReservedRange_tAED8F62AF040F5F133732196696893B313EA458B_CustomAttributesCacheGenerator_ReservedRange_Equals_mE5912F6171510806B9EF02DE03CF8DD3F35B9E4C,
	ReservedRange_tAED8F62AF040F5F133732196696893B313EA458B_CustomAttributesCacheGenerator_ReservedRange_GetHashCode_mA843DDE13FDC934E5BF90F4E87226E6895C7DF3A,
	ReservedRange_tAED8F62AF040F5F133732196696893B313EA458B_CustomAttributesCacheGenerator_ReservedRange_ToString_m5B37BC81177B03CCEE38CEF2CC602923C1AE18E9,
	ReservedRange_tAED8F62AF040F5F133732196696893B313EA458B_CustomAttributesCacheGenerator_ReservedRange_WriteTo_mAE8C9FDDD04DFAA10E55D3CB8E65948E461FBCB7,
	ReservedRange_tAED8F62AF040F5F133732196696893B313EA458B_CustomAttributesCacheGenerator_ReservedRange_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m85F3FBF2DF0058D3A982337FD4736926519B8D07,
	ReservedRange_tAED8F62AF040F5F133732196696893B313EA458B_CustomAttributesCacheGenerator_ReservedRange_CalculateSize_m76825970AE352D4A345CA58060D3BA6FF4AD90C5,
	ReservedRange_tAED8F62AF040F5F133732196696893B313EA458B_CustomAttributesCacheGenerator_ReservedRange_MergeFrom_mE827CEF25B811678E61BB8E50240CBE127E51442,
	ReservedRange_tAED8F62AF040F5F133732196696893B313EA458B_CustomAttributesCacheGenerator_ReservedRange_MergeFrom_m79DF028674F84059F761E6CAA9967E750D41E0C6,
	ReservedRange_tAED8F62AF040F5F133732196696893B313EA458B_CustomAttributesCacheGenerator_ReservedRange_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m53FFCAA3778D05AEEA1A4930A32451CB6F529B68,
	ExtensionRangeOptions_t497AE48098D601FD2AB661860BDA91704902E99F_CustomAttributesCacheGenerator_ExtensionRangeOptions__ctor_m03DB6B7CDC07388AD8EAC496F9F8FE6150421544,
	ExtensionRangeOptions_t497AE48098D601FD2AB661860BDA91704902E99F_CustomAttributesCacheGenerator_ExtensionRangeOptions__ctor_m0268E7412A855333005EB632BC2D81177F7CF0D2,
	ExtensionRangeOptions_t497AE48098D601FD2AB661860BDA91704902E99F_CustomAttributesCacheGenerator_ExtensionRangeOptions_Clone_mC718522EFD92E71DA33FA9E9C28DB3DDF2C4DA7E,
	ExtensionRangeOptions_t497AE48098D601FD2AB661860BDA91704902E99F_CustomAttributesCacheGenerator_ExtensionRangeOptions_Equals_mC28F05C7BAD43AC052787ED90AE10C25DE6F9E5F,
	ExtensionRangeOptions_t497AE48098D601FD2AB661860BDA91704902E99F_CustomAttributesCacheGenerator_ExtensionRangeOptions_Equals_m2A50B09E41F199B9161902DC11CEE01EDB02AD93,
	ExtensionRangeOptions_t497AE48098D601FD2AB661860BDA91704902E99F_CustomAttributesCacheGenerator_ExtensionRangeOptions_GetHashCode_m1FA993AAD13E3E515CB8E03384BBC859A807F719,
	ExtensionRangeOptions_t497AE48098D601FD2AB661860BDA91704902E99F_CustomAttributesCacheGenerator_ExtensionRangeOptions_ToString_mCDAFBAF9D027FD33891829389B30DEEEDA1DA694,
	ExtensionRangeOptions_t497AE48098D601FD2AB661860BDA91704902E99F_CustomAttributesCacheGenerator_ExtensionRangeOptions_WriteTo_mE36C0650E68E929057CDB2BEFBD837EB32726F93,
	ExtensionRangeOptions_t497AE48098D601FD2AB661860BDA91704902E99F_CustomAttributesCacheGenerator_ExtensionRangeOptions_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m8AAAA629B772292D614BE8DF07E56142B2ACDADE,
	ExtensionRangeOptions_t497AE48098D601FD2AB661860BDA91704902E99F_CustomAttributesCacheGenerator_ExtensionRangeOptions_CalculateSize_mE5FABC172FABF85E1427D3110E45BDD0E64E464C,
	ExtensionRangeOptions_t497AE48098D601FD2AB661860BDA91704902E99F_CustomAttributesCacheGenerator_ExtensionRangeOptions_MergeFrom_m676DA699130B7B80F30ACA1338B9E369B44940B1,
	ExtensionRangeOptions_t497AE48098D601FD2AB661860BDA91704902E99F_CustomAttributesCacheGenerator_ExtensionRangeOptions_MergeFrom_m2E9222BFE09BE7AC39995BB9DF8377E8C5C02486,
	ExtensionRangeOptions_t497AE48098D601FD2AB661860BDA91704902E99F_CustomAttributesCacheGenerator_ExtensionRangeOptions_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m67DB4C9A5F1DC35C7852751B65AE26736ED00D11,
	FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785_CustomAttributesCacheGenerator_FieldDescriptorProto__ctor_mDB51787020766E9E2EAB543B0184EAFA0982FD22,
	FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785_CustomAttributesCacheGenerator_FieldDescriptorProto__ctor_mF5B66C7A74743444259BC8300F6D960A429A61AA,
	FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785_CustomAttributesCacheGenerator_FieldDescriptorProto_Clone_m3B18B348D372D0C457D0F49FD218045E8D8A89B3,
	FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785_CustomAttributesCacheGenerator_FieldDescriptorProto_Equals_m8280930EB8D7F7C3BFE6BA9FA223C0411C56F47C,
	FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785_CustomAttributesCacheGenerator_FieldDescriptorProto_Equals_m3527F0C058D9FC4E1466AFD93B448721A1F18763,
	FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785_CustomAttributesCacheGenerator_FieldDescriptorProto_GetHashCode_mB99143077FA079BD863F94EB1D18717B09C01CC8,
	FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785_CustomAttributesCacheGenerator_FieldDescriptorProto_ToString_m11F796FE089C2875CDB38AAA3422E19996433A7E,
	FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785_CustomAttributesCacheGenerator_FieldDescriptorProto_WriteTo_mFA22B0175F6D838CEA09CA1F7B6DFE0E76622CC7,
	FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785_CustomAttributesCacheGenerator_FieldDescriptorProto_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m5898B3409384DC7633B83CDAC353FFD5D1D67784,
	FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785_CustomAttributesCacheGenerator_FieldDescriptorProto_CalculateSize_m955D78E94DE36494938994453F571457E48436D2,
	FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785_CustomAttributesCacheGenerator_FieldDescriptorProto_MergeFrom_m0DCDC8CB908E61CF0D7136B40C38940D9F21F986,
	FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785_CustomAttributesCacheGenerator_FieldDescriptorProto_MergeFrom_m70DA833065B73BA3CE80FE249DFDAA745A4981EC,
	FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785_CustomAttributesCacheGenerator_FieldDescriptorProto_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m986E810C00664AFB4CF43B946B88EE7A1DC7B66F,
	OneofDescriptorProto_t6319E05084FB5AA0644D3AEF37891689AFF1DD9A_CustomAttributesCacheGenerator_OneofDescriptorProto__ctor_mD2CE062E1C095E49ABDEFDC74B17229A45493047,
	OneofDescriptorProto_t6319E05084FB5AA0644D3AEF37891689AFF1DD9A_CustomAttributesCacheGenerator_OneofDescriptorProto__ctor_m14C97250B9E90E60CA6CA6064C9250354632C3AC,
	OneofDescriptorProto_t6319E05084FB5AA0644D3AEF37891689AFF1DD9A_CustomAttributesCacheGenerator_OneofDescriptorProto_Clone_m7C45A01EF4DAE4EDB8A17F181864989B593E3D11,
	OneofDescriptorProto_t6319E05084FB5AA0644D3AEF37891689AFF1DD9A_CustomAttributesCacheGenerator_OneofDescriptorProto_Equals_m63D5B71E1166BAFAF2BE805067EC147D8D75298D,
	OneofDescriptorProto_t6319E05084FB5AA0644D3AEF37891689AFF1DD9A_CustomAttributesCacheGenerator_OneofDescriptorProto_Equals_mFB6BA3CE309BF9605D4E93E1CDBE45ECAF125EA3,
	OneofDescriptorProto_t6319E05084FB5AA0644D3AEF37891689AFF1DD9A_CustomAttributesCacheGenerator_OneofDescriptorProto_GetHashCode_m44452D2990660A273BDB0CBA23FC660618F20E1A,
	OneofDescriptorProto_t6319E05084FB5AA0644D3AEF37891689AFF1DD9A_CustomAttributesCacheGenerator_OneofDescriptorProto_ToString_mE05FEA563B6444CC0150CAB7800E63589CFF3683,
	OneofDescriptorProto_t6319E05084FB5AA0644D3AEF37891689AFF1DD9A_CustomAttributesCacheGenerator_OneofDescriptorProto_WriteTo_m1122F29DE940E7FDE249B8B49D0A4CE85FA88DF1,
	OneofDescriptorProto_t6319E05084FB5AA0644D3AEF37891689AFF1DD9A_CustomAttributesCacheGenerator_OneofDescriptorProto_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_mC33BBE456CCB3E50F512370278FCB60B20882B58,
	OneofDescriptorProto_t6319E05084FB5AA0644D3AEF37891689AFF1DD9A_CustomAttributesCacheGenerator_OneofDescriptorProto_CalculateSize_mF6EDD0758C04B3A0C663D458BAE874DD78BAE845,
	OneofDescriptorProto_t6319E05084FB5AA0644D3AEF37891689AFF1DD9A_CustomAttributesCacheGenerator_OneofDescriptorProto_MergeFrom_m9929A6BFE74508B75D3C84F099E0473C67499803,
	OneofDescriptorProto_t6319E05084FB5AA0644D3AEF37891689AFF1DD9A_CustomAttributesCacheGenerator_OneofDescriptorProto_MergeFrom_m7E5A8F79E11BF5CC5DC28F66E56916BE9C185511,
	OneofDescriptorProto_t6319E05084FB5AA0644D3AEF37891689AFF1DD9A_CustomAttributesCacheGenerator_OneofDescriptorProto_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_mDF43CF284022D2519858B4865D13C83C21F249A4,
	EnumDescriptorProto_t0E60F3628E51BC270FDA740DCBC83DBF04FA0478_CustomAttributesCacheGenerator_EnumDescriptorProto__ctor_mB3A28E8576A228315F116223DA06685ABD39550A,
	EnumDescriptorProto_t0E60F3628E51BC270FDA740DCBC83DBF04FA0478_CustomAttributesCacheGenerator_EnumDescriptorProto__ctor_m147F600199E44F16A8C1AA7F6991895708CF4282,
	EnumDescriptorProto_t0E60F3628E51BC270FDA740DCBC83DBF04FA0478_CustomAttributesCacheGenerator_EnumDescriptorProto_Clone_mB7008C506A35ACCFE0E5D36AAD55F49F98F6E85A,
	EnumDescriptorProto_t0E60F3628E51BC270FDA740DCBC83DBF04FA0478_CustomAttributesCacheGenerator_EnumDescriptorProto_Equals_m6711ABED6B0C4EC1496AAE697AA452084CB3546B,
	EnumDescriptorProto_t0E60F3628E51BC270FDA740DCBC83DBF04FA0478_CustomAttributesCacheGenerator_EnumDescriptorProto_Equals_m22E80DDF1828BF67B70CDC86776D054CB05F6F23,
	EnumDescriptorProto_t0E60F3628E51BC270FDA740DCBC83DBF04FA0478_CustomAttributesCacheGenerator_EnumDescriptorProto_GetHashCode_m7EACA529EA0BFE654F625A89C93F046781EE0F9F,
	EnumDescriptorProto_t0E60F3628E51BC270FDA740DCBC83DBF04FA0478_CustomAttributesCacheGenerator_EnumDescriptorProto_ToString_m4FB571E200CAA7F881910FBEB583E511661A8340,
	EnumDescriptorProto_t0E60F3628E51BC270FDA740DCBC83DBF04FA0478_CustomAttributesCacheGenerator_EnumDescriptorProto_WriteTo_m9D12A0C8024AA0DA17AE32C6782D2930703EEAE4,
	EnumDescriptorProto_t0E60F3628E51BC270FDA740DCBC83DBF04FA0478_CustomAttributesCacheGenerator_EnumDescriptorProto_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m14AEC404CD6EFADDE5EBA504F48275D2AA0D66F7,
	EnumDescriptorProto_t0E60F3628E51BC270FDA740DCBC83DBF04FA0478_CustomAttributesCacheGenerator_EnumDescriptorProto_CalculateSize_m9F366B59267B5766D2E4B89E64EE02E653AAF2E0,
	EnumDescriptorProto_t0E60F3628E51BC270FDA740DCBC83DBF04FA0478_CustomAttributesCacheGenerator_EnumDescriptorProto_MergeFrom_mE277EA218CDEAB5A29709DA28ED6FA1B6E7A1CCC,
	EnumDescriptorProto_t0E60F3628E51BC270FDA740DCBC83DBF04FA0478_CustomAttributesCacheGenerator_EnumDescriptorProto_MergeFrom_mD7331F6048B5F3A9CBD9BCE98EC931224970178E,
	EnumDescriptorProto_t0E60F3628E51BC270FDA740DCBC83DBF04FA0478_CustomAttributesCacheGenerator_EnumDescriptorProto_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_mEEA70B246D0C85DA555958273DC1F8F2F977F7CF,
	EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41_CustomAttributesCacheGenerator_EnumReservedRange__ctor_m02904F51FF5FE5B92BAA1E816D3A1B13534A6E0C,
	EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41_CustomAttributesCacheGenerator_EnumReservedRange__ctor_m4F7BB9BB1007668742F085BDA2A82E889F6D1B6A,
	EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41_CustomAttributesCacheGenerator_EnumReservedRange_Clone_m0478CEBDD1CC260098613EC9608FA8DA619521A4,
	EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41_CustomAttributesCacheGenerator_EnumReservedRange_Equals_m3728912F8E0AD0EA5A74FD69323CF9F73465DA04,
	EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41_CustomAttributesCacheGenerator_EnumReservedRange_Equals_m1FB9B43A21E9D232DFE20485F50C4673D84948E1,
	EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41_CustomAttributesCacheGenerator_EnumReservedRange_GetHashCode_m6415E55F96EE5B92BBE2965CD1FE5D37C2604231,
	EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41_CustomAttributesCacheGenerator_EnumReservedRange_ToString_mA9995D4DB4A84520B09678054B780C0714C18E32,
	EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41_CustomAttributesCacheGenerator_EnumReservedRange_WriteTo_m7D1FE13C055716582D6BC6B24B10AE1654DA1316,
	EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41_CustomAttributesCacheGenerator_EnumReservedRange_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m40335111DC4E167612FEF3662BE9124300D3480A,
	EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41_CustomAttributesCacheGenerator_EnumReservedRange_CalculateSize_m0A62C551FDA64908B0C58397CD1428C5C71BF2F5,
	EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41_CustomAttributesCacheGenerator_EnumReservedRange_MergeFrom_m2FC3A667F2AF4C85C2C7D3AF0E4E471D92019CD2,
	EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41_CustomAttributesCacheGenerator_EnumReservedRange_MergeFrom_mCD0205C8ED2CEB21A1C6F16D967019975292C0A0,
	EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41_CustomAttributesCacheGenerator_EnumReservedRange_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_mDCF2D669D389A95648C8864C743EBF87D6CAC659,
	EnumValueDescriptorProto_t24F4825414596CE28D7979C4F352A8EE30AE8315_CustomAttributesCacheGenerator_EnumValueDescriptorProto__ctor_mCD6381E84158229867FFB9D4C43817B859E9A183,
	EnumValueDescriptorProto_t24F4825414596CE28D7979C4F352A8EE30AE8315_CustomAttributesCacheGenerator_EnumValueDescriptorProto__ctor_m0E2CA6496FBAE77FB5E1F05802586BD4BE3A3D3D,
	EnumValueDescriptorProto_t24F4825414596CE28D7979C4F352A8EE30AE8315_CustomAttributesCacheGenerator_EnumValueDescriptorProto_Clone_m1611AE2A72549FFE60B8119A55D815BA696E862D,
	EnumValueDescriptorProto_t24F4825414596CE28D7979C4F352A8EE30AE8315_CustomAttributesCacheGenerator_EnumValueDescriptorProto_Equals_mAEBB3D490CDECFB762189684B4B64227E2759C07,
	EnumValueDescriptorProto_t24F4825414596CE28D7979C4F352A8EE30AE8315_CustomAttributesCacheGenerator_EnumValueDescriptorProto_Equals_m4676E76A2633C7C43EA50E9A532A5CFBAA32C1BF,
	EnumValueDescriptorProto_t24F4825414596CE28D7979C4F352A8EE30AE8315_CustomAttributesCacheGenerator_EnumValueDescriptorProto_GetHashCode_mB2267753F8624960C3BE6FEF07DE4F221E82E7D0,
	EnumValueDescriptorProto_t24F4825414596CE28D7979C4F352A8EE30AE8315_CustomAttributesCacheGenerator_EnumValueDescriptorProto_ToString_m2A605BE66BAB0ADDB88DF7F6D39D38E4AE30E399,
	EnumValueDescriptorProto_t24F4825414596CE28D7979C4F352A8EE30AE8315_CustomAttributesCacheGenerator_EnumValueDescriptorProto_WriteTo_m628970DB0F15CF7E6A2C600B877D999E92B0AF30,
	EnumValueDescriptorProto_t24F4825414596CE28D7979C4F352A8EE30AE8315_CustomAttributesCacheGenerator_EnumValueDescriptorProto_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m742B37B6C10FF627FF03157232ABA74C1918CEA7,
	EnumValueDescriptorProto_t24F4825414596CE28D7979C4F352A8EE30AE8315_CustomAttributesCacheGenerator_EnumValueDescriptorProto_CalculateSize_m1F7A660E62D7576C260035086428BF3452D89C36,
	EnumValueDescriptorProto_t24F4825414596CE28D7979C4F352A8EE30AE8315_CustomAttributesCacheGenerator_EnumValueDescriptorProto_MergeFrom_mC16859FC588043BE69253243292C173544C0BC14,
	EnumValueDescriptorProto_t24F4825414596CE28D7979C4F352A8EE30AE8315_CustomAttributesCacheGenerator_EnumValueDescriptorProto_MergeFrom_m415B9988016FE0741689B94605032A8CD4033362,
	EnumValueDescriptorProto_t24F4825414596CE28D7979C4F352A8EE30AE8315_CustomAttributesCacheGenerator_EnumValueDescriptorProto_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m586D380CAEE945088D1FDBB1C08489F63B823E5D,
	ServiceDescriptorProto_t39FE93732D4E2D7AA71D61E86B211AAADA573DB3_CustomAttributesCacheGenerator_ServiceDescriptorProto__ctor_m2ABEEBBB877E11DC2EB775785478B01367CEFDC5,
	ServiceDescriptorProto_t39FE93732D4E2D7AA71D61E86B211AAADA573DB3_CustomAttributesCacheGenerator_ServiceDescriptorProto__ctor_mF76948802201FEE2ACD8D67A55B5C75EBFF5FDF1,
	ServiceDescriptorProto_t39FE93732D4E2D7AA71D61E86B211AAADA573DB3_CustomAttributesCacheGenerator_ServiceDescriptorProto_Clone_mE601AB9C3641148DB0AC9C3B6BD5F3281DC57109,
	ServiceDescriptorProto_t39FE93732D4E2D7AA71D61E86B211AAADA573DB3_CustomAttributesCacheGenerator_ServiceDescriptorProto_Equals_m5C75FE57DE6DE100164657FB1129840A9B9489AB,
	ServiceDescriptorProto_t39FE93732D4E2D7AA71D61E86B211AAADA573DB3_CustomAttributesCacheGenerator_ServiceDescriptorProto_Equals_mFBE6BA3EC312B0C6F693C79A2D723347CD013C55,
	ServiceDescriptorProto_t39FE93732D4E2D7AA71D61E86B211AAADA573DB3_CustomAttributesCacheGenerator_ServiceDescriptorProto_GetHashCode_m7BA75A38679BF8C53EA10F293060F72BFDC46396,
	ServiceDescriptorProto_t39FE93732D4E2D7AA71D61E86B211AAADA573DB3_CustomAttributesCacheGenerator_ServiceDescriptorProto_ToString_m97875482BD469952825E67CBE1ACB629241E775A,
	ServiceDescriptorProto_t39FE93732D4E2D7AA71D61E86B211AAADA573DB3_CustomAttributesCacheGenerator_ServiceDescriptorProto_WriteTo_m4D6E98E4F13566154B1EBFB0309D2DF3A4B71A38,
	ServiceDescriptorProto_t39FE93732D4E2D7AA71D61E86B211AAADA573DB3_CustomAttributesCacheGenerator_ServiceDescriptorProto_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m4E48FC87B5F4637449255AF403AAF0971BD9FF91,
	ServiceDescriptorProto_t39FE93732D4E2D7AA71D61E86B211AAADA573DB3_CustomAttributesCacheGenerator_ServiceDescriptorProto_CalculateSize_mFD1610268B60E21FDA5CA531B00065C1E4A6AB8E,
	ServiceDescriptorProto_t39FE93732D4E2D7AA71D61E86B211AAADA573DB3_CustomAttributesCacheGenerator_ServiceDescriptorProto_MergeFrom_m6581ECAC1AF5ECDFFCDEC9A551D9F71BCEC0005B,
	ServiceDescriptorProto_t39FE93732D4E2D7AA71D61E86B211AAADA573DB3_CustomAttributesCacheGenerator_ServiceDescriptorProto_MergeFrom_mA53EA15C18F1199B95F5F1DF3AD52A81F6E5AB7E,
	ServiceDescriptorProto_t39FE93732D4E2D7AA71D61E86B211AAADA573DB3_CustomAttributesCacheGenerator_ServiceDescriptorProto_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m70DB24C4CDA1363FF025EA7F7CBE3449C1A4C49C,
	MethodDescriptorProto_t88EE2E0AFE0DEEDDD27A1DAFA0451A259924D638_CustomAttributesCacheGenerator_MethodDescriptorProto__ctor_m3181F7006234380068049FEDB5E800625B8193C8,
	MethodDescriptorProto_t88EE2E0AFE0DEEDDD27A1DAFA0451A259924D638_CustomAttributesCacheGenerator_MethodDescriptorProto__ctor_mEF5E8090FBE2C5FB52278A283B26A67284FBD781,
	MethodDescriptorProto_t88EE2E0AFE0DEEDDD27A1DAFA0451A259924D638_CustomAttributesCacheGenerator_MethodDescriptorProto_Clone_mA90669FE9E8DE792CEC9323E367F334926F95A21,
	MethodDescriptorProto_t88EE2E0AFE0DEEDDD27A1DAFA0451A259924D638_CustomAttributesCacheGenerator_MethodDescriptorProto_Equals_mE404C60BAAC311AF6BC9E81457AE4901A2EC69F1,
	MethodDescriptorProto_t88EE2E0AFE0DEEDDD27A1DAFA0451A259924D638_CustomAttributesCacheGenerator_MethodDescriptorProto_Equals_m15C523245227F5007A8438DA67437535DADCA8A0,
	MethodDescriptorProto_t88EE2E0AFE0DEEDDD27A1DAFA0451A259924D638_CustomAttributesCacheGenerator_MethodDescriptorProto_GetHashCode_mBE5452AA6B4265114D0C57FDC8BC4496C01211CB,
	MethodDescriptorProto_t88EE2E0AFE0DEEDDD27A1DAFA0451A259924D638_CustomAttributesCacheGenerator_MethodDescriptorProto_ToString_mF5CA9EB9E35C30D1141628CDA8D36F3183524337,
	MethodDescriptorProto_t88EE2E0AFE0DEEDDD27A1DAFA0451A259924D638_CustomAttributesCacheGenerator_MethodDescriptorProto_WriteTo_m81FE666F3F825DFE70F6A79A3E8EC18E54F3CBBA,
	MethodDescriptorProto_t88EE2E0AFE0DEEDDD27A1DAFA0451A259924D638_CustomAttributesCacheGenerator_MethodDescriptorProto_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m3D279F4D543CF3AEDDA8A726D1DDE892CF58D8D2,
	MethodDescriptorProto_t88EE2E0AFE0DEEDDD27A1DAFA0451A259924D638_CustomAttributesCacheGenerator_MethodDescriptorProto_CalculateSize_mD02B74F49B50CFCB0A8504ACE320C81D1815D622,
	MethodDescriptorProto_t88EE2E0AFE0DEEDDD27A1DAFA0451A259924D638_CustomAttributesCacheGenerator_MethodDescriptorProto_MergeFrom_m2B10CD2A53C04E575D652B444AF50326300A36C6,
	MethodDescriptorProto_t88EE2E0AFE0DEEDDD27A1DAFA0451A259924D638_CustomAttributesCacheGenerator_MethodDescriptorProto_MergeFrom_mB331EAAF45C06DE2158A063B329836049B00650A,
	MethodDescriptorProto_t88EE2E0AFE0DEEDDD27A1DAFA0451A259924D638_CustomAttributesCacheGenerator_MethodDescriptorProto_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_mEEC631A9308E9E0931247AD1FC67A84AD79F7E88,
	FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions__ctor_m53E8E18F45026B68293C1D6E27ADD3C809A81E75,
	FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions__ctor_mE14316BC374AEFD5371F0306BB87D7635BE51612,
	FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_Clone_m4AE4FD9F2AA9ACE2E4FEA61EF8CE2996995E6D79,
	FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_Equals_m269B71EC6513379C07694356E74F494902C8BCCA,
	FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_Equals_m9A68AC5269CD93866D6B3AE6103426CA0B62C75F,
	FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_GetHashCode_mC6302B4BBD76B6B1B81E42621DC3B414AFBB2093,
	FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_ToString_m1F624CC78700C658ED7BCFCFC64F1932A9DE8591,
	FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_WriteTo_m46AC0C3DC74648185755CF83BB96E3097A430512,
	FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_mE7D6ECEA3BB13F301585BA7374B00F84DFE61CC7,
	FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_CalculateSize_m1CBAF20ABA4BCFD4FAC533D9EDF18F7C65AF1105,
	FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_MergeFrom_m602802909D9E87D110BEA1BB3D6ECDA93F9030E6,
	FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_MergeFrom_mD0E3ECF95A4D41396D5A6CD667943E22F2D163AC,
	FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m9A84CB596D5C261B6CD9973D22690134C98313FC,
	MessageOptions_t007E64DA64D7132536A101E9AA32FBFE31FFADBD_CustomAttributesCacheGenerator_MessageOptions__ctor_m04D30C43D35878336875570DC6A5E3E6BC862DC8,
	MessageOptions_t007E64DA64D7132536A101E9AA32FBFE31FFADBD_CustomAttributesCacheGenerator_MessageOptions__ctor_m624EB105BF4DBD4102C96FBB4E33A6298249DADD,
	MessageOptions_t007E64DA64D7132536A101E9AA32FBFE31FFADBD_CustomAttributesCacheGenerator_MessageOptions_Clone_m145FE6784EC0872E03A2A8F80ED305D98CA4B885,
	MessageOptions_t007E64DA64D7132536A101E9AA32FBFE31FFADBD_CustomAttributesCacheGenerator_MessageOptions_Equals_m1B9DA96523A1EBF257B4C05943A355ABDEF8D4A3,
	MessageOptions_t007E64DA64D7132536A101E9AA32FBFE31FFADBD_CustomAttributesCacheGenerator_MessageOptions_Equals_m140F555A06D1A7D663A75E01F530CE9504F73BCB,
	MessageOptions_t007E64DA64D7132536A101E9AA32FBFE31FFADBD_CustomAttributesCacheGenerator_MessageOptions_GetHashCode_m5FB4F16D0E258B4EA5B190B1FC9BA4B92CE54A0D,
	MessageOptions_t007E64DA64D7132536A101E9AA32FBFE31FFADBD_CustomAttributesCacheGenerator_MessageOptions_ToString_mD783887EDD978433A31E4DDCDF8A1F73C927BE1D,
	MessageOptions_t007E64DA64D7132536A101E9AA32FBFE31FFADBD_CustomAttributesCacheGenerator_MessageOptions_WriteTo_mB3320A7754F94A90E6A9DC30D4FE1D3E6D933849,
	MessageOptions_t007E64DA64D7132536A101E9AA32FBFE31FFADBD_CustomAttributesCacheGenerator_MessageOptions_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m47F229E21C08BCE7F2BE882F5811ACBBBF789A13,
	MessageOptions_t007E64DA64D7132536A101E9AA32FBFE31FFADBD_CustomAttributesCacheGenerator_MessageOptions_CalculateSize_mC3FBEC0AC2FF4F8C44D2E5F8387DF31F177936A3,
	MessageOptions_t007E64DA64D7132536A101E9AA32FBFE31FFADBD_CustomAttributesCacheGenerator_MessageOptions_MergeFrom_m79A658F6D709B1A47BF52BDFE737C37609FCC755,
	MessageOptions_t007E64DA64D7132536A101E9AA32FBFE31FFADBD_CustomAttributesCacheGenerator_MessageOptions_MergeFrom_m3DF6E1BE37AE3869439B0A6FA4DA0BE58835014F,
	MessageOptions_t007E64DA64D7132536A101E9AA32FBFE31FFADBD_CustomAttributesCacheGenerator_MessageOptions_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m3009CFC09F4F10050F4E663DD16E6151A0A3F556,
	FieldOptions_t3B2D61638B0B06A886A821421DA06C09E4A78BEA_CustomAttributesCacheGenerator_FieldOptions__ctor_mAEDAAFD3E1E1476831562F5E223B4AF2F806D42E,
	FieldOptions_t3B2D61638B0B06A886A821421DA06C09E4A78BEA_CustomAttributesCacheGenerator_FieldOptions__ctor_mEBACF18F7CD68BE7590F93E5181DEB9E8D8FB13C,
	FieldOptions_t3B2D61638B0B06A886A821421DA06C09E4A78BEA_CustomAttributesCacheGenerator_FieldOptions_Clone_m2B5511B42C346DF381B638EBDD946CF86BA411F3,
	FieldOptions_t3B2D61638B0B06A886A821421DA06C09E4A78BEA_CustomAttributesCacheGenerator_FieldOptions_Equals_m35E10FF061C593610F69C8A497CBBB5345B641DA,
	FieldOptions_t3B2D61638B0B06A886A821421DA06C09E4A78BEA_CustomAttributesCacheGenerator_FieldOptions_Equals_mC82884DE34A33E7799B4B537DB3E5327FFF93DE8,
	FieldOptions_t3B2D61638B0B06A886A821421DA06C09E4A78BEA_CustomAttributesCacheGenerator_FieldOptions_GetHashCode_m2C8BB8DAB5A6D69F940B9714BAAF1632D551814B,
	FieldOptions_t3B2D61638B0B06A886A821421DA06C09E4A78BEA_CustomAttributesCacheGenerator_FieldOptions_ToString_m9E9A624E4C63E7B5FB16D55551A2B3E1BCE409F5,
	FieldOptions_t3B2D61638B0B06A886A821421DA06C09E4A78BEA_CustomAttributesCacheGenerator_FieldOptions_WriteTo_mB619B555A1CF0FABD74BAB7646DAAEC68B206B4C,
	FieldOptions_t3B2D61638B0B06A886A821421DA06C09E4A78BEA_CustomAttributesCacheGenerator_FieldOptions_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_mCE93A239A7677AE5873AAE5CB3E6F3979A60247D,
	FieldOptions_t3B2D61638B0B06A886A821421DA06C09E4A78BEA_CustomAttributesCacheGenerator_FieldOptions_CalculateSize_m28F032BDF8603529AFE3FF729BF891A8B035A4F2,
	FieldOptions_t3B2D61638B0B06A886A821421DA06C09E4A78BEA_CustomAttributesCacheGenerator_FieldOptions_MergeFrom_m78199E61A3B42CB84FC665C80A5C16C79E81E330,
	FieldOptions_t3B2D61638B0B06A886A821421DA06C09E4A78BEA_CustomAttributesCacheGenerator_FieldOptions_MergeFrom_mC9159B562BFFE7A8E521072DCCD5F4D9E42043B4,
	FieldOptions_t3B2D61638B0B06A886A821421DA06C09E4A78BEA_CustomAttributesCacheGenerator_FieldOptions_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m19D5BFCA155031073581BDEA524C8D1A712D5393,
	OneofOptions_tE3261CFC4DF0841400F23CBA8578D1191DE59AE7_CustomAttributesCacheGenerator_OneofOptions__ctor_mCB3DEA19D4B2D302F3960474C65B68869AE65A6C,
	OneofOptions_tE3261CFC4DF0841400F23CBA8578D1191DE59AE7_CustomAttributesCacheGenerator_OneofOptions__ctor_m87704EF2A87D953EEE82B1634AB6326C00D36690,
	OneofOptions_tE3261CFC4DF0841400F23CBA8578D1191DE59AE7_CustomAttributesCacheGenerator_OneofOptions_Clone_m861C126BB5E08AD41DFCC26AF082E5E53435DB55,
	OneofOptions_tE3261CFC4DF0841400F23CBA8578D1191DE59AE7_CustomAttributesCacheGenerator_OneofOptions_Equals_mF33E711DDF4A91CE5F2BB106B53D2AC4688D058B,
	OneofOptions_tE3261CFC4DF0841400F23CBA8578D1191DE59AE7_CustomAttributesCacheGenerator_OneofOptions_Equals_mD4943C6868C3350846C89F186BABEF1E024925B6,
	OneofOptions_tE3261CFC4DF0841400F23CBA8578D1191DE59AE7_CustomAttributesCacheGenerator_OneofOptions_GetHashCode_m9F0EBEE5FCA5AC438B7DDCF82E19623AE486A957,
	OneofOptions_tE3261CFC4DF0841400F23CBA8578D1191DE59AE7_CustomAttributesCacheGenerator_OneofOptions_ToString_m472557171AD5BB71572A06F3169E632E1322C621,
	OneofOptions_tE3261CFC4DF0841400F23CBA8578D1191DE59AE7_CustomAttributesCacheGenerator_OneofOptions_WriteTo_mA65BE0E9F2AE867975411486CE00F66754F593DB,
	OneofOptions_tE3261CFC4DF0841400F23CBA8578D1191DE59AE7_CustomAttributesCacheGenerator_OneofOptions_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m63942D15544A3D5AC7BEB303FE4FA711E3364C69,
	OneofOptions_tE3261CFC4DF0841400F23CBA8578D1191DE59AE7_CustomAttributesCacheGenerator_OneofOptions_CalculateSize_m2018ADE928533BABBA3CCDEA40E9FC13978CC4F0,
	OneofOptions_tE3261CFC4DF0841400F23CBA8578D1191DE59AE7_CustomAttributesCacheGenerator_OneofOptions_MergeFrom_mBCB2053FE63A76031534E6E4CD2DBD96E73118D2,
	OneofOptions_tE3261CFC4DF0841400F23CBA8578D1191DE59AE7_CustomAttributesCacheGenerator_OneofOptions_MergeFrom_mD36D6943FAFB5C64CC30EA67CF7DFAD976C846EA,
	OneofOptions_tE3261CFC4DF0841400F23CBA8578D1191DE59AE7_CustomAttributesCacheGenerator_OneofOptions_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_mDF39C909A8356CFEBE50ADE3B724A1C7B306E84D,
	EnumOptions_t84BF0A0DAC3BF1F447E6F0DABA6E4EB9B3AF4918_CustomAttributesCacheGenerator_EnumOptions__ctor_m5385E9F25EF8615050334EEC3708BA5725590EA6,
	EnumOptions_t84BF0A0DAC3BF1F447E6F0DABA6E4EB9B3AF4918_CustomAttributesCacheGenerator_EnumOptions__ctor_m6CC675C9336E6028155824F6AC523E84B9B72900,
	EnumOptions_t84BF0A0DAC3BF1F447E6F0DABA6E4EB9B3AF4918_CustomAttributesCacheGenerator_EnumOptions_Clone_m607D859024E1D3E98EADC2D87FE1A6004633A364,
	EnumOptions_t84BF0A0DAC3BF1F447E6F0DABA6E4EB9B3AF4918_CustomAttributesCacheGenerator_EnumOptions_Equals_m7AE883DD0D520647C6A466B4B7731AA4C3ECC696,
	EnumOptions_t84BF0A0DAC3BF1F447E6F0DABA6E4EB9B3AF4918_CustomAttributesCacheGenerator_EnumOptions_Equals_mA6AEAD2E23DF4EA72CB302A48CA00FC4D7844CB0,
	EnumOptions_t84BF0A0DAC3BF1F447E6F0DABA6E4EB9B3AF4918_CustomAttributesCacheGenerator_EnumOptions_GetHashCode_mC319C69B06D57266DF45D6E68DBAD4A8A24CB638,
	EnumOptions_t84BF0A0DAC3BF1F447E6F0DABA6E4EB9B3AF4918_CustomAttributesCacheGenerator_EnumOptions_ToString_m5D36D78A38E16C3589A17C93AF80ABC42F5304F7,
	EnumOptions_t84BF0A0DAC3BF1F447E6F0DABA6E4EB9B3AF4918_CustomAttributesCacheGenerator_EnumOptions_WriteTo_m5689DE4D2F562F444B47E032A670D6F495472D74,
	EnumOptions_t84BF0A0DAC3BF1F447E6F0DABA6E4EB9B3AF4918_CustomAttributesCacheGenerator_EnumOptions_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m014FF1827E80308CE342F0F91A38C87AD1E6FFAA,
	EnumOptions_t84BF0A0DAC3BF1F447E6F0DABA6E4EB9B3AF4918_CustomAttributesCacheGenerator_EnumOptions_CalculateSize_mE57649652E7F429D04DDCB6BA25843D83D858366,
	EnumOptions_t84BF0A0DAC3BF1F447E6F0DABA6E4EB9B3AF4918_CustomAttributesCacheGenerator_EnumOptions_MergeFrom_m8783323089C9BE38786C2F133AE42096DF6374AF,
	EnumOptions_t84BF0A0DAC3BF1F447E6F0DABA6E4EB9B3AF4918_CustomAttributesCacheGenerator_EnumOptions_MergeFrom_mE5A3CD277AF799DB94EF81CFEBC4C84E13947F53,
	EnumOptions_t84BF0A0DAC3BF1F447E6F0DABA6E4EB9B3AF4918_CustomAttributesCacheGenerator_EnumOptions_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m7703AE4472D5FDEB96708EFA3BD302633F5211F4,
	EnumValueOptions_t3521CCE0345304E9A1355EE2DA6AD9C0621C87A9_CustomAttributesCacheGenerator_EnumValueOptions__ctor_mEDC4489E058024D0EE74CD1882B267796D0E6F05,
	EnumValueOptions_t3521CCE0345304E9A1355EE2DA6AD9C0621C87A9_CustomAttributesCacheGenerator_EnumValueOptions__ctor_m1E94702D09BCE26AC8F7CBFDD1D52C1AEFBB183E,
	EnumValueOptions_t3521CCE0345304E9A1355EE2DA6AD9C0621C87A9_CustomAttributesCacheGenerator_EnumValueOptions_Clone_m6C604B56C82AC36BE5F555CD73F471B01B831730,
	EnumValueOptions_t3521CCE0345304E9A1355EE2DA6AD9C0621C87A9_CustomAttributesCacheGenerator_EnumValueOptions_Equals_mBE4B57FD48FF5410BEB1956196C76DA4B5B451CD,
	EnumValueOptions_t3521CCE0345304E9A1355EE2DA6AD9C0621C87A9_CustomAttributesCacheGenerator_EnumValueOptions_Equals_m4275B004B5AB046B0CCC1C05C4C9C88C672D4B27,
	EnumValueOptions_t3521CCE0345304E9A1355EE2DA6AD9C0621C87A9_CustomAttributesCacheGenerator_EnumValueOptions_GetHashCode_m3CDA5B21FFC6387A8313F2E0230D63C5CC4D9699,
	EnumValueOptions_t3521CCE0345304E9A1355EE2DA6AD9C0621C87A9_CustomAttributesCacheGenerator_EnumValueOptions_ToString_m689014E160C318BA3C63EFD328D3B0F7DCA8908E,
	EnumValueOptions_t3521CCE0345304E9A1355EE2DA6AD9C0621C87A9_CustomAttributesCacheGenerator_EnumValueOptions_WriteTo_m2C38B59E54CC93366529373A55DCBBF1805B2FBC,
	EnumValueOptions_t3521CCE0345304E9A1355EE2DA6AD9C0621C87A9_CustomAttributesCacheGenerator_EnumValueOptions_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m1E4497803CFE23240560E2EFDD0C4ED503A98338,
	EnumValueOptions_t3521CCE0345304E9A1355EE2DA6AD9C0621C87A9_CustomAttributesCacheGenerator_EnumValueOptions_CalculateSize_m5CEBE3C36B5DFD8EB4139E0BC860D680C1A1EB19,
	EnumValueOptions_t3521CCE0345304E9A1355EE2DA6AD9C0621C87A9_CustomAttributesCacheGenerator_EnumValueOptions_MergeFrom_m5ED39DC9CFB8446A5595EFED2D18B56BFE2F761E,
	EnumValueOptions_t3521CCE0345304E9A1355EE2DA6AD9C0621C87A9_CustomAttributesCacheGenerator_EnumValueOptions_MergeFrom_m9CB27E7FFBCF59A93EFB81F345EEEE0018780465,
	EnumValueOptions_t3521CCE0345304E9A1355EE2DA6AD9C0621C87A9_CustomAttributesCacheGenerator_EnumValueOptions_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m9A7A72C5D302BD0113B44F30F4D93A74E69CB7CD,
	ServiceOptions_t3CCBCF9F8215D476834CC7A5808FED49F161D11A_CustomAttributesCacheGenerator_ServiceOptions__ctor_mF53F4F57784851D29B2274A2B2C0B53A044FC5F2,
	ServiceOptions_t3CCBCF9F8215D476834CC7A5808FED49F161D11A_CustomAttributesCacheGenerator_ServiceOptions__ctor_mB0C390F674A6FD4B7E940F7DF26EE01E8B72486C,
	ServiceOptions_t3CCBCF9F8215D476834CC7A5808FED49F161D11A_CustomAttributesCacheGenerator_ServiceOptions_Clone_m548C4ABD6A7B28FF5D8339A2878E617F8FF0FC0F,
	ServiceOptions_t3CCBCF9F8215D476834CC7A5808FED49F161D11A_CustomAttributesCacheGenerator_ServiceOptions_Equals_m8F72F53030D77F97C862B939A18F0B803318DE2F,
	ServiceOptions_t3CCBCF9F8215D476834CC7A5808FED49F161D11A_CustomAttributesCacheGenerator_ServiceOptions_Equals_m8103227D46B27ED1BC216DB305C3973DC4F2DF76,
	ServiceOptions_t3CCBCF9F8215D476834CC7A5808FED49F161D11A_CustomAttributesCacheGenerator_ServiceOptions_GetHashCode_m6843CA5920027100EC678C9E12D958ABBB0B7A2F,
	ServiceOptions_t3CCBCF9F8215D476834CC7A5808FED49F161D11A_CustomAttributesCacheGenerator_ServiceOptions_ToString_m9C3A29E7B7D222E10DD900C0AF541A804B3130F8,
	ServiceOptions_t3CCBCF9F8215D476834CC7A5808FED49F161D11A_CustomAttributesCacheGenerator_ServiceOptions_WriteTo_mC3CCEB2117A0EFC47E65EBCFF2D95500CAB4F504,
	ServiceOptions_t3CCBCF9F8215D476834CC7A5808FED49F161D11A_CustomAttributesCacheGenerator_ServiceOptions_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m595286491DBE361A862243B1A0F5AA2C64F48739,
	ServiceOptions_t3CCBCF9F8215D476834CC7A5808FED49F161D11A_CustomAttributesCacheGenerator_ServiceOptions_CalculateSize_mF38C58E3DD52815641DE6B3734035AD147E28E37,
	ServiceOptions_t3CCBCF9F8215D476834CC7A5808FED49F161D11A_CustomAttributesCacheGenerator_ServiceOptions_MergeFrom_m0F4BD8AC991185BC2C06F1541C340B56202A6AE4,
	ServiceOptions_t3CCBCF9F8215D476834CC7A5808FED49F161D11A_CustomAttributesCacheGenerator_ServiceOptions_MergeFrom_m91ACECFAAC88BE4A3B9AF6E32A85D5ACD847507D,
	ServiceOptions_t3CCBCF9F8215D476834CC7A5808FED49F161D11A_CustomAttributesCacheGenerator_ServiceOptions_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_mB1A0155B8F628B062613DE44B49A8F6B680587AB,
	MethodOptions_t94D248D11F5D87A48A25D56B4E21F6E942ED9F4F_CustomAttributesCacheGenerator_MethodOptions__ctor_m8E6C41386BE75CAF13EBCA4773DE2C9455DCAACB,
	MethodOptions_t94D248D11F5D87A48A25D56B4E21F6E942ED9F4F_CustomAttributesCacheGenerator_MethodOptions__ctor_m5D84C74E7A241DC3FEB9E244C786A3CA5A9EA6C1,
	MethodOptions_t94D248D11F5D87A48A25D56B4E21F6E942ED9F4F_CustomAttributesCacheGenerator_MethodOptions_Clone_m4F7F9963D8DD66C5CBCDD6DE8D9044C573755C7B,
	MethodOptions_t94D248D11F5D87A48A25D56B4E21F6E942ED9F4F_CustomAttributesCacheGenerator_MethodOptions_Equals_mC19CF9BE571A1064D708C490AFBB4DF8508597E2,
	MethodOptions_t94D248D11F5D87A48A25D56B4E21F6E942ED9F4F_CustomAttributesCacheGenerator_MethodOptions_Equals_mB3CF6D193D1EB5531625EA2135546118AF0F4DA7,
	MethodOptions_t94D248D11F5D87A48A25D56B4E21F6E942ED9F4F_CustomAttributesCacheGenerator_MethodOptions_GetHashCode_m419CD1A58A8AFCDB6D673259EB2CD03A31400414,
	MethodOptions_t94D248D11F5D87A48A25D56B4E21F6E942ED9F4F_CustomAttributesCacheGenerator_MethodOptions_ToString_m582EC2A358621D0F1398769E1AF5733E93B2C166,
	MethodOptions_t94D248D11F5D87A48A25D56B4E21F6E942ED9F4F_CustomAttributesCacheGenerator_MethodOptions_WriteTo_m8DC61AF4DBB7A265BD61718BFB2BA3E149F4CED0,
	MethodOptions_t94D248D11F5D87A48A25D56B4E21F6E942ED9F4F_CustomAttributesCacheGenerator_MethodOptions_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m308FFA3BF40A93AD6A3BFD1CE927BD5745F4BA4B,
	MethodOptions_t94D248D11F5D87A48A25D56B4E21F6E942ED9F4F_CustomAttributesCacheGenerator_MethodOptions_CalculateSize_m7AE8BB6308CC41A549C3409B7DD10E7459F38347,
	MethodOptions_t94D248D11F5D87A48A25D56B4E21F6E942ED9F4F_CustomAttributesCacheGenerator_MethodOptions_MergeFrom_mDBD557C9249B039843EEA039CFBD3A5604D9FCD5,
	MethodOptions_t94D248D11F5D87A48A25D56B4E21F6E942ED9F4F_CustomAttributesCacheGenerator_MethodOptions_MergeFrom_m55F1821842F961B9E76D746DEB8F09AA2F93032D,
	MethodOptions_t94D248D11F5D87A48A25D56B4E21F6E942ED9F4F_CustomAttributesCacheGenerator_MethodOptions_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_mD8A99A524FB7D62431721187321E3FCA9124E0F1,
	UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728_CustomAttributesCacheGenerator_UninterpretedOption__ctor_m01FCE0DA392AF8E60B04329799B2FD66C2A4676D,
	UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728_CustomAttributesCacheGenerator_UninterpretedOption__ctor_m11693D3C42B33BE6E844F5F6F2810E399BF413EC,
	UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728_CustomAttributesCacheGenerator_UninterpretedOption_Clone_m0B1D69F3BE93968ACEAECA8835FEB1B5542ECE6A,
	UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728_CustomAttributesCacheGenerator_UninterpretedOption_Equals_m480D8D7A06EC8E37BDDAE05B1A4A3709F6FC0850,
	UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728_CustomAttributesCacheGenerator_UninterpretedOption_Equals_m695AA83BD422C1433861AB2DFEA337174A603910,
	UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728_CustomAttributesCacheGenerator_UninterpretedOption_GetHashCode_m9E5BF7D1BE4AA11718BA1841A1A0F0A010FC1ECC,
	UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728_CustomAttributesCacheGenerator_UninterpretedOption_ToString_m5C88AF1EC3E062A8E98637D0316F62C9874A5E5F,
	UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728_CustomAttributesCacheGenerator_UninterpretedOption_WriteTo_mE3E2D1AEAF4379F1E60A80DB89EDD27EE8285255,
	UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728_CustomAttributesCacheGenerator_UninterpretedOption_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m790A1EEC06943590A53B5FE56456EFA59EB44034,
	UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728_CustomAttributesCacheGenerator_UninterpretedOption_CalculateSize_m0073EE08E92667DDF1A0764667B6890C0765E0D1,
	UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728_CustomAttributesCacheGenerator_UninterpretedOption_MergeFrom_mCDAB1F9A576C61CDC4FAA103105C9C3BED5DFCE9,
	UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728_CustomAttributesCacheGenerator_UninterpretedOption_MergeFrom_m0464D25AE497BE73F7067D4A56C1AFB748FBB16B,
	UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728_CustomAttributesCacheGenerator_UninterpretedOption_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_mD2F9D9A892C3A6A090D637C759815A6BFA835FE3,
	NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22_CustomAttributesCacheGenerator_NamePart__ctor_mA9A9103300EE0850829D3BFE67B52EAD4512F085,
	NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22_CustomAttributesCacheGenerator_NamePart__ctor_mFBC708208AD503A756034768ABF66A5E5E0AF4DD,
	NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22_CustomAttributesCacheGenerator_NamePart_Clone_m43B2E0BD86099C758B706C19DB038F56A2409C6F,
	NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22_CustomAttributesCacheGenerator_NamePart_Equals_m9F4FC0E7DAF91E9141210A33B1AFE8F2127F68BB,
	NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22_CustomAttributesCacheGenerator_NamePart_Equals_m4F68F7BFA6DC8252C7BF46F9B7B7A4561BC9B297,
	NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22_CustomAttributesCacheGenerator_NamePart_GetHashCode_m84C037E032844843C3501CBBF1D5579160297F11,
	NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22_CustomAttributesCacheGenerator_NamePart_ToString_mF10088283D361B38968A6E57F78255654D3BB2D1,
	NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22_CustomAttributesCacheGenerator_NamePart_WriteTo_m9F9C644A5C5530F04A945671E7BBA3FEF7061448,
	NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22_CustomAttributesCacheGenerator_NamePart_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m0396A0FC112B2CFF1E5B1FD1C0AE400359E96A01,
	NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22_CustomAttributesCacheGenerator_NamePart_CalculateSize_mE593E657449DE890C6273E4C9DC3B90A948C8749,
	NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22_CustomAttributesCacheGenerator_NamePart_MergeFrom_mB5380B100DD6F68EDEEF200E1BB000EFC4906DA7,
	NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22_CustomAttributesCacheGenerator_NamePart_MergeFrom_m4889B005A581C203986474994D59CAD2FC9F5F30,
	NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22_CustomAttributesCacheGenerator_NamePart_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m161542DC71B24D4416D0327851A5EFD2B8A093D2,
	SourceCodeInfo_tE9110A5CAA36A130E03CC9F3DE6E0EF7E7BF412B_CustomAttributesCacheGenerator_SourceCodeInfo__ctor_m0A1DC09D0F3B17BC20F44D8201C5D0DB841145E9,
	SourceCodeInfo_tE9110A5CAA36A130E03CC9F3DE6E0EF7E7BF412B_CustomAttributesCacheGenerator_SourceCodeInfo__ctor_m12C51AC307B6497E9CF7C2887ADB23E01B33392A,
	SourceCodeInfo_tE9110A5CAA36A130E03CC9F3DE6E0EF7E7BF412B_CustomAttributesCacheGenerator_SourceCodeInfo_Clone_m879DC87218D95C6E0907ABB23C9F355F21C158BA,
	SourceCodeInfo_tE9110A5CAA36A130E03CC9F3DE6E0EF7E7BF412B_CustomAttributesCacheGenerator_SourceCodeInfo_Equals_mAC74617ADE26118FC282D20867FF972CF4B90786,
	SourceCodeInfo_tE9110A5CAA36A130E03CC9F3DE6E0EF7E7BF412B_CustomAttributesCacheGenerator_SourceCodeInfo_Equals_mA2B30829479F4760E5B91E5B65D41B02889BED41,
	SourceCodeInfo_tE9110A5CAA36A130E03CC9F3DE6E0EF7E7BF412B_CustomAttributesCacheGenerator_SourceCodeInfo_GetHashCode_mCD706831C8044FE179D8117D2526273B88B073A3,
	SourceCodeInfo_tE9110A5CAA36A130E03CC9F3DE6E0EF7E7BF412B_CustomAttributesCacheGenerator_SourceCodeInfo_ToString_mE4FC459AEF9A122B2893BE043EFA5EF455BA1CAD,
	SourceCodeInfo_tE9110A5CAA36A130E03CC9F3DE6E0EF7E7BF412B_CustomAttributesCacheGenerator_SourceCodeInfo_WriteTo_m11D19F228C061C27C584002104687141B0D514AE,
	SourceCodeInfo_tE9110A5CAA36A130E03CC9F3DE6E0EF7E7BF412B_CustomAttributesCacheGenerator_SourceCodeInfo_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m601DD6F66EB4F4AF24126704725F9979A31EE648,
	SourceCodeInfo_tE9110A5CAA36A130E03CC9F3DE6E0EF7E7BF412B_CustomAttributesCacheGenerator_SourceCodeInfo_CalculateSize_m5530408BED9841C7EDC134DC3B2CB88F2691B4CA,
	SourceCodeInfo_tE9110A5CAA36A130E03CC9F3DE6E0EF7E7BF412B_CustomAttributesCacheGenerator_SourceCodeInfo_MergeFrom_mA3BFA5056081A4CFA0CE5DFF332A6815646EEF0A,
	SourceCodeInfo_tE9110A5CAA36A130E03CC9F3DE6E0EF7E7BF412B_CustomAttributesCacheGenerator_SourceCodeInfo_MergeFrom_mEC664CB41897C3A5E540B205E64E6658866D29FC,
	SourceCodeInfo_tE9110A5CAA36A130E03CC9F3DE6E0EF7E7BF412B_CustomAttributesCacheGenerator_SourceCodeInfo_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m1E724139176220624BC89C54831F1E7A68FA3815,
	Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_CustomAttributesCacheGenerator_Location__ctor_mFDC279769BEAE3382B8775935C8683621235F622,
	Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_CustomAttributesCacheGenerator_Location__ctor_m28EFCE79F70879A58E3CB6F54CE731BD38BE41DD,
	Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_CustomAttributesCacheGenerator_Location_Clone_m3C4C0598AE1D773542602ABBFBFC912ABB270BEE,
	Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_CustomAttributesCacheGenerator_Location_Equals_m85AD8ECE48C60DADB8768733AF1F6563AF4AC281,
	Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_CustomAttributesCacheGenerator_Location_Equals_m8D982EACDA1888B6588CD7689C00D5EBB2985368,
	Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_CustomAttributesCacheGenerator_Location_GetHashCode_m6ACF33DBF8F3F18D5D7DD313D7729EA5839A174B,
	Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_CustomAttributesCacheGenerator_Location_ToString_m9B282141CD535FF8261C8195BF3CDDF528C40A4C,
	Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_CustomAttributesCacheGenerator_Location_WriteTo_m4210E1B1440977E2A413E0B8F75133982F73EC69,
	Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_CustomAttributesCacheGenerator_Location_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m504F51927BE7797E64ECDEC999C03BB04846EB33,
	Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_CustomAttributesCacheGenerator_Location_CalculateSize_m4FE7C1E7A7BB0E885F9A9779A16EC3D562B6BA3F,
	Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_CustomAttributesCacheGenerator_Location_MergeFrom_mFA64C2E2E2D47FD3294936AD3C696EAE3B0CD8F7,
	Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_CustomAttributesCacheGenerator_Location_MergeFrom_mD097D430111330C3EE05F9F06685B68DB4A7077D,
	Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_CustomAttributesCacheGenerator_Location_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m30960A0BA81FA14E2F5806B1821B17CEB6EC74A8,
	GeneratedCodeInfo_t88B2B7880C3E0445510519523DE05618C3B1A315_CustomAttributesCacheGenerator_GeneratedCodeInfo__ctor_mDE0BB1D90115580D0C21C264412FD0C6BDC79827,
	GeneratedCodeInfo_t88B2B7880C3E0445510519523DE05618C3B1A315_CustomAttributesCacheGenerator_GeneratedCodeInfo__ctor_m5588A1B1D68A18DA88CA5DCB4533BE39BA987B0B,
	GeneratedCodeInfo_t88B2B7880C3E0445510519523DE05618C3B1A315_CustomAttributesCacheGenerator_GeneratedCodeInfo_Clone_mB2E07DB9A3075EAAF762DFDCE21F99B40521F491,
	GeneratedCodeInfo_t88B2B7880C3E0445510519523DE05618C3B1A315_CustomAttributesCacheGenerator_GeneratedCodeInfo_Equals_m881F3A7864546DCD98F1AC8E5C753DA3B1AEC0A1,
	GeneratedCodeInfo_t88B2B7880C3E0445510519523DE05618C3B1A315_CustomAttributesCacheGenerator_GeneratedCodeInfo_Equals_mA65F1EB2D6AE57EAB2AB0B72A3F4892252760755,
	GeneratedCodeInfo_t88B2B7880C3E0445510519523DE05618C3B1A315_CustomAttributesCacheGenerator_GeneratedCodeInfo_GetHashCode_mB9F0A2A53CF8BEB45CBAFE12C8CFCB5819E31C3D,
	GeneratedCodeInfo_t88B2B7880C3E0445510519523DE05618C3B1A315_CustomAttributesCacheGenerator_GeneratedCodeInfo_ToString_m325B0EB4FF2412BCDDAD96524195745F0517B9D9,
	GeneratedCodeInfo_t88B2B7880C3E0445510519523DE05618C3B1A315_CustomAttributesCacheGenerator_GeneratedCodeInfo_WriteTo_m66B19D7405A8F6561D49E8A49165FE62712685DF,
	GeneratedCodeInfo_t88B2B7880C3E0445510519523DE05618C3B1A315_CustomAttributesCacheGenerator_GeneratedCodeInfo_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_mE9F47B77861B199F58CF5AB795C22FA47A0E3FD4,
	GeneratedCodeInfo_t88B2B7880C3E0445510519523DE05618C3B1A315_CustomAttributesCacheGenerator_GeneratedCodeInfo_CalculateSize_mB7F3C903908BB158E829E98EF242D7BE2070A981,
	GeneratedCodeInfo_t88B2B7880C3E0445510519523DE05618C3B1A315_CustomAttributesCacheGenerator_GeneratedCodeInfo_MergeFrom_m2AF45EF1C2F717A5957EF6F07C088851083D83D2,
	GeneratedCodeInfo_t88B2B7880C3E0445510519523DE05618C3B1A315_CustomAttributesCacheGenerator_GeneratedCodeInfo_MergeFrom_m87520A239A84EFAEA34062FD27265F10E6EE7BCE,
	GeneratedCodeInfo_t88B2B7880C3E0445510519523DE05618C3B1A315_CustomAttributesCacheGenerator_GeneratedCodeInfo_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m68CF657C364AE67E08C90AA4854CA28F745E6AEF,
	Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_CustomAttributesCacheGenerator_Annotation__ctor_m9ED6A93F900EBFD3D4BC4980C3D9B032B8F9B5D7,
	Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_CustomAttributesCacheGenerator_Annotation__ctor_m61D9641ED59D1CDA766721C1A40198C8BF8F6E45,
	Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_CustomAttributesCacheGenerator_Annotation_Clone_mB86AE67CF1DB8FC7CF33ADDCEF2FEEB74BCDFE5D,
	Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_CustomAttributesCacheGenerator_Annotation_Equals_m5C1A51E20393172CE798E42CAD95C3DD71E511C8,
	Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_CustomAttributesCacheGenerator_Annotation_Equals_mD66C9282185C31DF563EED063B1019BE13C2811E,
	Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_CustomAttributesCacheGenerator_Annotation_GetHashCode_m6F1BE21B2168F0D61B4E600E21DDC81A22132BF2,
	Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_CustomAttributesCacheGenerator_Annotation_ToString_m2E543D1B02E6207CB9009CEA5A987A6439466064,
	Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_CustomAttributesCacheGenerator_Annotation_WriteTo_m108B8AEABCE4F89A6E90997BAC8AC9371A53565D,
	Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_CustomAttributesCacheGenerator_Annotation_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalWriteTo_m2D0F50AAD5686905CCAA2959D0C6E112FDA37E85,
	Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_CustomAttributesCacheGenerator_Annotation_CalculateSize_m1DE1ABE6707A9C1C6D415715E6EB90D7DDD98AB8,
	Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_CustomAttributesCacheGenerator_Annotation_MergeFrom_m2468E39EA5A58BCFA6F9E35D01002292E3CC450F,
	Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_CustomAttributesCacheGenerator_Annotation_MergeFrom_m8CA8D3549BCBA484E4BA0ADCDE8200E20CCEDFBD,
	Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_CustomAttributesCacheGenerator_Annotation_pbU3AU3AGoogle_Protobuf_IBufferMessage_InternalMergeFrom_m314FDFF4E0741C746FA60D608C45F8C01E8A9117,
	DescriptorBase_tF6C2BA893A9882D75C7CD171CB21EBA88FA82CEA_CustomAttributesCacheGenerator_DescriptorBase_get_FullName_m915949C2A2E317ADAAA3141CB30DE432F1D6047D,
	DescriptorBase_tF6C2BA893A9882D75C7CD171CB21EBA88FA82CEA_CustomAttributesCacheGenerator_DescriptorBase_get_File_mFBA850CF476DBD50BF1AB469555EAD69FFD827A4,
	DescriptorDeclaration_tA267DA7CAD0094614E29377401372F3EB4C395FB_CustomAttributesCacheGenerator_DescriptorDeclaration_get_StartLine_mECF4D235763DA0659A3E0E32D5ED6F286FAA6920,
	ExtensionAccessor_t8FD60C5A0CEA561F542BE3A83BD8634D11C38118_CustomAttributesCacheGenerator_ExtensionAccessor_get_Descriptor_m4D3230A77360F27ED2DDBCD1A52FBB7644A3BB9D,
	ExtensionCollection_t52756747D345D5B934DCD6FF9876823214980772_CustomAttributesCacheGenerator_ExtensionCollection_get_UnorderedExtensions_mE70760039915D512775C9C2193AE020D011B0FCD,
	FieldDescriptor_t0D57515C5C0DD2F7D37E1EB3838AE950F7C609BD_CustomAttributesCacheGenerator_FieldDescriptor_get_ContainingType_mBEBF37A2458327C9419A884CB13132958B0009CA,
	FieldDescriptor_t0D57515C5C0DD2F7D37E1EB3838AE950F7C609BD_CustomAttributesCacheGenerator_FieldDescriptor_get_ContainingOneof_m792FE02B65FBBEFDCFA68FCE08CE1B789C935BE9,
	FieldDescriptor_t0D57515C5C0DD2F7D37E1EB3838AE950F7C609BD_CustomAttributesCacheGenerator_FieldDescriptor_get_JsonName_m37E80D133D994BDE854220D920790A1E1BBDAF50,
	FieldDescriptor_t0D57515C5C0DD2F7D37E1EB3838AE950F7C609BD_CustomAttributesCacheGenerator_FieldDescriptor_get_Proto_m193D4347BAF5B48C4CDACF7FEBEB0EB7ECB1A5CB,
	FieldDescriptor_t0D57515C5C0DD2F7D37E1EB3838AE950F7C609BD_CustomAttributesCacheGenerator_FieldDescriptor_get_Extension_m47CB4AEB81E5372FE16AF15451EE3DB8F96F95BA,
	FileDescriptor_tA2E229ECE64C897445BBCA0229BD41F8E028ECD4_CustomAttributesCacheGenerator_FileDescriptor_get_Proto_m3861FEA6DC13D62F54CC04BD11AAAAA1CC9D235B,
	FileDescriptor_tA2E229ECE64C897445BBCA0229BD41F8E028ECD4_CustomAttributesCacheGenerator_FileDescriptor_get_Syntax_mC6D5E49E01F937101A4C15FDA84933E5E3AAD38C,
	FileDescriptor_tA2E229ECE64C897445BBCA0229BD41F8E028ECD4_CustomAttributesCacheGenerator_FileDescriptor_get_MessageTypes_m55CD9B2EB841D0F8FF161F0DB15A2D67266C536C,
	FileDescriptor_tA2E229ECE64C897445BBCA0229BD41F8E028ECD4_CustomAttributesCacheGenerator_FileDescriptor_get_EnumTypes_m52F61381D43042CA3A0B4B0FF30BB33ED4BE8D44,
	FileDescriptor_tA2E229ECE64C897445BBCA0229BD41F8E028ECD4_CustomAttributesCacheGenerator_FileDescriptor_get_Services_m111C66F60C4E1EB7FCE260D433F35058563F9E39,
	FileDescriptor_tA2E229ECE64C897445BBCA0229BD41F8E028ECD4_CustomAttributesCacheGenerator_FileDescriptor_get_Extensions_m3308E404E50DC055A4AF9EDAB142BA227CA7F4B2,
	FileDescriptor_tA2E229ECE64C897445BBCA0229BD41F8E028ECD4_CustomAttributesCacheGenerator_FileDescriptor_get_Dependencies_m9E9EB5F41B5489AE519540C7027903300E4F5955,
	FileDescriptor_tA2E229ECE64C897445BBCA0229BD41F8E028ECD4_CustomAttributesCacheGenerator_FileDescriptor_get_PublicDependencies_mAC7D2C8B65EA6260FD1E00427C865B073C26C20C,
	FileDescriptor_tA2E229ECE64C897445BBCA0229BD41F8E028ECD4_CustomAttributesCacheGenerator_FileDescriptor_get_DescriptorPool_m39E958D13A60EBE7CC79BD0456664D61900040A3,
	GeneratedClrTypeInfo_t26E71D04A6D31D12375C412ACFDF303153379EDE_CustomAttributesCacheGenerator_GeneratedClrTypeInfo_get_ClrType_mBC268EA87D91A720FC864656F282267E52E5C415,
	GeneratedClrTypeInfo_t26E71D04A6D31D12375C412ACFDF303153379EDE_CustomAttributesCacheGenerator_GeneratedClrTypeInfo_set_ClrType_m47923E24303E58B15765BA976560FF54060AA463,
	GeneratedClrTypeInfo_t26E71D04A6D31D12375C412ACFDF303153379EDE_CustomAttributesCacheGenerator_GeneratedClrTypeInfo_get_Parser_mE306A707C2E98793AE20F8F22D73682256A6825A,
	GeneratedClrTypeInfo_t26E71D04A6D31D12375C412ACFDF303153379EDE_CustomAttributesCacheGenerator_GeneratedClrTypeInfo_get_PropertyNames_m9AC3AF88BE6E4401E8CBE61D2A27205D494C7537,
	GeneratedClrTypeInfo_t26E71D04A6D31D12375C412ACFDF303153379EDE_CustomAttributesCacheGenerator_GeneratedClrTypeInfo_get_Extensions_mBC35C072C48C25BC9E3E10930E7ABC899E7D0BD9,
	GeneratedClrTypeInfo_t26E71D04A6D31D12375C412ACFDF303153379EDE_CustomAttributesCacheGenerator_GeneratedClrTypeInfo_get_OneofNames_m8225DF12A9536F188BB8F84100E0026AD38398D3,
	GeneratedClrTypeInfo_t26E71D04A6D31D12375C412ACFDF303153379EDE_CustomAttributesCacheGenerator_GeneratedClrTypeInfo_get_NestedTypes_mC8CDA4ACD8F8B83388C64EFB5C63E8841E8B7767,
	GeneratedClrTypeInfo_t26E71D04A6D31D12375C412ACFDF303153379EDE_CustomAttributesCacheGenerator_GeneratedClrTypeInfo_get_NestedEnums_m124D8277A92DC1AD640C130ABE855B6C9B55EB82,
	MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13_CustomAttributesCacheGenerator_MessageDescriptor_get_Proto_mC19705FDFDE1B7189CE45B38771704217266D87C,
	MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13_CustomAttributesCacheGenerator_MessageDescriptor_get_ClrType_m67EDD051E54EF81C60E7C10479F1EAC25F222FFB,
	MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13_CustomAttributesCacheGenerator_MessageDescriptor_get_Parser_mBA1FD49A96FD0E2AC88955EA6BCF536555E9B68C,
	MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13_CustomAttributesCacheGenerator_MessageDescriptor_get_Fields_m7EC5DF1D21CA98B0E798ED2171C276DEDAC39352,
	MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13_CustomAttributesCacheGenerator_MessageDescriptor_get_Extensions_mEE45D3BA082A29D28E2390D85156DD1E9F579155,
	MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13_CustomAttributesCacheGenerator_MessageDescriptor_get_NestedTypes_m34A524E3C7D560C1537685CF6291AB60DA58FE7E,
	MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13_CustomAttributesCacheGenerator_MessageDescriptor_get_EnumTypes_m663896B42139FAB5706A6408FA931F95A60C3F39,
	MessageDescriptor_tB6D43F06E244E220EFE94990FA86AC8D8DA76D13_CustomAttributesCacheGenerator_MessageDescriptor_get_Oneofs_mC26EB7828846AC0A04AB6F892C4D22E79CD1E364,
	OneofAccessor_t9CFA9BCA5E91251304EC17E543378C61EEE6FB5E_CustomAttributesCacheGenerator_OneofAccessor_get_Descriptor_mBDAF0F18B7C8EBBC4C2CF19A267C0AE3F09785C5,
	OneofDescriptor_tAAAEA98A626026CC9ECA0E1A6171617F989251A8_CustomAttributesCacheGenerator_OneofDescriptor_get_IsSynthetic_m22DC2931DF30D51ABB96C336D1F8A89F37898D17,
	OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664_CustomAttributesCacheGenerator_OriginalNameAttribute_get_Name_m35E068ED514B51E172CF5FE5DD917801F045B819,
	OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664_CustomAttributesCacheGenerator_OriginalNameAttribute_set_Name_mC0A333EF33378D9143B3077B4E86270FE05636DA,
	OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664_CustomAttributesCacheGenerator_OriginalNameAttribute_get_PreferredAlias_m8B1D004B47D36373572F40238135A599E23095B8,
	OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664_CustomAttributesCacheGenerator_OriginalNameAttribute_set_PreferredAlias_m5717AA7D6B145D079B78DE15B31B88C8D6B4C258,
	ReflectionUtil_t36D2A3AC0248B85EC6227BA3ED9C9A81DF922F1B_CustomAttributesCacheGenerator_ReflectionUtil_get_CanConvertEnumFuncToInt32Func_m14B4A020233464D727830248EED426EBDE5D45EF,
	TypeRegistry_t82646E0517B3E7727E1973D5743C3D6416AAF476_CustomAttributesCacheGenerator_TypeRegistry_get_Empty_mE5DF84BF4F8096E4E4DEC88EB1B60542256F54C9,
	ProtobufEqualityComparers_tDE90D1B40462AFF4F1886F7315B35DE79C035C3E_CustomAttributesCacheGenerator_ProtobufEqualityComparers_get_BitwiseDoubleEqualityComparer_mE44F7862345571F0CA657DC541C39FE262543714,
	ProtobufEqualityComparers_tDE90D1B40462AFF4F1886F7315B35DE79C035C3E_CustomAttributesCacheGenerator_ProtobufEqualityComparers_get_BitwiseSingleEqualityComparer_m5E6A6EB33216A3E9E829ED8FBA161E0B7BBCCBDB,
	ProtobufEqualityComparers_tDE90D1B40462AFF4F1886F7315B35DE79C035C3E_CustomAttributesCacheGenerator_ProtobufEqualityComparers_get_BitwiseNullableDoubleEqualityComparer_m137AF49F6F51AC6745E79BB1EDDA611DCDCEE632,
	ProtobufEqualityComparers_tDE90D1B40462AFF4F1886F7315B35DE79C035C3E_CustomAttributesCacheGenerator_ProtobufEqualityComparers_get_BitwiseNullableSingleEqualityComparer_m903E7CC373C92BA08C7D8DC4B5569774DFB5A090,
	RepeatedField_1_t26F74AB41CEB8AB4728C9AB65712DBBF2B738C89_CustomAttributesCacheGenerator_RepeatedField_1_GetEnumerator_m2F086EA93B2B496851734024D484CAF9A8C67267,
	U3CGetEnumeratorU3Ed__28_t1D89B8849E2BE4C57358D822F30C41B8A6D1339A_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__28__ctor_mFABF109F443CD341CD6C0E0ECD1BA7651AC38C1F,
	U3CGetEnumeratorU3Ed__28_t1D89B8849E2BE4C57358D822F30C41B8A6D1339A_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__28_System_IDisposable_Dispose_mF014E23F4497286EFF080674AC6D3C697F9AA955,
	U3CGetEnumeratorU3Ed__28_t1D89B8849E2BE4C57358D822F30C41B8A6D1339A_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__28_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_mCF5E0E46F0619B8F959C7714546CB8FE2479FAFA,
	U3CGetEnumeratorU3Ed__28_t1D89B8849E2BE4C57358D822F30C41B8A6D1339A_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__28_System_Collections_IEnumerator_Reset_m4D608FFBDDC33385D5EFE6E09923CC715DA6ECAC,
	U3CGetEnumeratorU3Ed__28_t1D89B8849E2BE4C57358D822F30C41B8A6D1339A_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__28_System_Collections_IEnumerator_get_Current_m0778F046811669B42A886BAAB48D7275BC6BAAE1,
	ByteString_t3B1232ECD50ECBDD385E2BF61FEEF3B17FC5D76D_CustomAttributesCacheGenerator_ByteString_CopyFrom_m5B311324FC5F998F3DB5B3E2B6AFAB16C497B8B0____bytes0,
	Any_t92B39FAFBB91BF3E94AAA6527FC7D6B113D7D811_CustomAttributesCacheGenerator_Any_t92B39FAFBB91BF3E94AAA6527FC7D6B113D7D811____Parser_PropertyInfo,
	Any_t92B39FAFBB91BF3E94AAA6527FC7D6B113D7D811_CustomAttributesCacheGenerator_Any_t92B39FAFBB91BF3E94AAA6527FC7D6B113D7D811____Descriptor_PropertyInfo,
	Any_t92B39FAFBB91BF3E94AAA6527FC7D6B113D7D811_CustomAttributesCacheGenerator_Any_t92B39FAFBB91BF3E94AAA6527FC7D6B113D7D811____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo,
	Any_t92B39FAFBB91BF3E94AAA6527FC7D6B113D7D811_CustomAttributesCacheGenerator_Any_t92B39FAFBB91BF3E94AAA6527FC7D6B113D7D811____TypeUrl_PropertyInfo,
	Any_t92B39FAFBB91BF3E94AAA6527FC7D6B113D7D811_CustomAttributesCacheGenerator_Any_t92B39FAFBB91BF3E94AAA6527FC7D6B113D7D811____Value_PropertyInfo,
	Duration_t40CF63035DE7C0F4A03867C1128E4BB2CCF12A80_CustomAttributesCacheGenerator_Duration_t40CF63035DE7C0F4A03867C1128E4BB2CCF12A80____Parser_PropertyInfo,
	Duration_t40CF63035DE7C0F4A03867C1128E4BB2CCF12A80_CustomAttributesCacheGenerator_Duration_t40CF63035DE7C0F4A03867C1128E4BB2CCF12A80____Descriptor_PropertyInfo,
	Duration_t40CF63035DE7C0F4A03867C1128E4BB2CCF12A80_CustomAttributesCacheGenerator_Duration_t40CF63035DE7C0F4A03867C1128E4BB2CCF12A80____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo,
	Duration_t40CF63035DE7C0F4A03867C1128E4BB2CCF12A80_CustomAttributesCacheGenerator_Duration_t40CF63035DE7C0F4A03867C1128E4BB2CCF12A80____Seconds_PropertyInfo,
	Duration_t40CF63035DE7C0F4A03867C1128E4BB2CCF12A80_CustomAttributesCacheGenerator_Duration_t40CF63035DE7C0F4A03867C1128E4BB2CCF12A80____Nanos_PropertyInfo,
	FieldMask_t0F76999F69A7AA8A3E19EC1466CE438ABB2FA273_CustomAttributesCacheGenerator_FieldMask_t0F76999F69A7AA8A3E19EC1466CE438ABB2FA273____Parser_PropertyInfo,
	FieldMask_t0F76999F69A7AA8A3E19EC1466CE438ABB2FA273_CustomAttributesCacheGenerator_FieldMask_t0F76999F69A7AA8A3E19EC1466CE438ABB2FA273____Descriptor_PropertyInfo,
	FieldMask_t0F76999F69A7AA8A3E19EC1466CE438ABB2FA273_CustomAttributesCacheGenerator_FieldMask_t0F76999F69A7AA8A3E19EC1466CE438ABB2FA273____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo,
	FieldMask_t0F76999F69A7AA8A3E19EC1466CE438ABB2FA273_CustomAttributesCacheGenerator_FieldMask_t0F76999F69A7AA8A3E19EC1466CE438ABB2FA273____Paths_PropertyInfo,
	SourceContext_t7616D6280D262E1831459B553153AB5D68F5077B_CustomAttributesCacheGenerator_SourceContext_t7616D6280D262E1831459B553153AB5D68F5077B____Parser_PropertyInfo,
	SourceContext_t7616D6280D262E1831459B553153AB5D68F5077B_CustomAttributesCacheGenerator_SourceContext_t7616D6280D262E1831459B553153AB5D68F5077B____Descriptor_PropertyInfo,
	SourceContext_t7616D6280D262E1831459B553153AB5D68F5077B_CustomAttributesCacheGenerator_SourceContext_t7616D6280D262E1831459B553153AB5D68F5077B____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo,
	SourceContext_t7616D6280D262E1831459B553153AB5D68F5077B_CustomAttributesCacheGenerator_SourceContext_t7616D6280D262E1831459B553153AB5D68F5077B____FileName_PropertyInfo,
	Struct_t4E66B704BB3E73B19AFFF2A87EB12ED16C7C4DD3_CustomAttributesCacheGenerator_Struct_t4E66B704BB3E73B19AFFF2A87EB12ED16C7C4DD3____Parser_PropertyInfo,
	Struct_t4E66B704BB3E73B19AFFF2A87EB12ED16C7C4DD3_CustomAttributesCacheGenerator_Struct_t4E66B704BB3E73B19AFFF2A87EB12ED16C7C4DD3____Descriptor_PropertyInfo,
	Struct_t4E66B704BB3E73B19AFFF2A87EB12ED16C7C4DD3_CustomAttributesCacheGenerator_Struct_t4E66B704BB3E73B19AFFF2A87EB12ED16C7C4DD3____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo,
	Struct_t4E66B704BB3E73B19AFFF2A87EB12ED16C7C4DD3_CustomAttributesCacheGenerator_Struct_t4E66B704BB3E73B19AFFF2A87EB12ED16C7C4DD3____Fields_PropertyInfo,
	Value_t4A50D780AAEDFF7855F6732AA0799306161698CB_CustomAttributesCacheGenerator_Value_t4A50D780AAEDFF7855F6732AA0799306161698CB____Parser_PropertyInfo,
	Value_t4A50D780AAEDFF7855F6732AA0799306161698CB_CustomAttributesCacheGenerator_Value_t4A50D780AAEDFF7855F6732AA0799306161698CB____Descriptor_PropertyInfo,
	Value_t4A50D780AAEDFF7855F6732AA0799306161698CB_CustomAttributesCacheGenerator_Value_t4A50D780AAEDFF7855F6732AA0799306161698CB____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo,
	Value_t4A50D780AAEDFF7855F6732AA0799306161698CB_CustomAttributesCacheGenerator_Value_t4A50D780AAEDFF7855F6732AA0799306161698CB____NullValue_PropertyInfo,
	Value_t4A50D780AAEDFF7855F6732AA0799306161698CB_CustomAttributesCacheGenerator_Value_t4A50D780AAEDFF7855F6732AA0799306161698CB____NumberValue_PropertyInfo,
	Value_t4A50D780AAEDFF7855F6732AA0799306161698CB_CustomAttributesCacheGenerator_Value_t4A50D780AAEDFF7855F6732AA0799306161698CB____StringValue_PropertyInfo,
	Value_t4A50D780AAEDFF7855F6732AA0799306161698CB_CustomAttributesCacheGenerator_Value_t4A50D780AAEDFF7855F6732AA0799306161698CB____BoolValue_PropertyInfo,
	Value_t4A50D780AAEDFF7855F6732AA0799306161698CB_CustomAttributesCacheGenerator_Value_t4A50D780AAEDFF7855F6732AA0799306161698CB____StructValue_PropertyInfo,
	Value_t4A50D780AAEDFF7855F6732AA0799306161698CB_CustomAttributesCacheGenerator_Value_t4A50D780AAEDFF7855F6732AA0799306161698CB____ListValue_PropertyInfo,
	Value_t4A50D780AAEDFF7855F6732AA0799306161698CB_CustomAttributesCacheGenerator_Value_t4A50D780AAEDFF7855F6732AA0799306161698CB____KindCase_PropertyInfo,
	ListValue_t11D7F704E07B21FD590F1D7C14E83402D497D40F_CustomAttributesCacheGenerator_ListValue_t11D7F704E07B21FD590F1D7C14E83402D497D40F____Parser_PropertyInfo,
	ListValue_t11D7F704E07B21FD590F1D7C14E83402D497D40F_CustomAttributesCacheGenerator_ListValue_t11D7F704E07B21FD590F1D7C14E83402D497D40F____Descriptor_PropertyInfo,
	ListValue_t11D7F704E07B21FD590F1D7C14E83402D497D40F_CustomAttributesCacheGenerator_ListValue_t11D7F704E07B21FD590F1D7C14E83402D497D40F____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo,
	Timestamp_t9150476DBE9622F93C1BB771873B5E1B5FA45929_CustomAttributesCacheGenerator_Timestamp_t9150476DBE9622F93C1BB771873B5E1B5FA45929____Parser_PropertyInfo,
	Timestamp_t9150476DBE9622F93C1BB771873B5E1B5FA45929_CustomAttributesCacheGenerator_Timestamp_t9150476DBE9622F93C1BB771873B5E1B5FA45929____Descriptor_PropertyInfo,
	Timestamp_t9150476DBE9622F93C1BB771873B5E1B5FA45929_CustomAttributesCacheGenerator_Timestamp_t9150476DBE9622F93C1BB771873B5E1B5FA45929____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo,
	Timestamp_t9150476DBE9622F93C1BB771873B5E1B5FA45929_CustomAttributesCacheGenerator_Timestamp_t9150476DBE9622F93C1BB771873B5E1B5FA45929____Seconds_PropertyInfo,
	Timestamp_t9150476DBE9622F93C1BB771873B5E1B5FA45929_CustomAttributesCacheGenerator_Timestamp_t9150476DBE9622F93C1BB771873B5E1B5FA45929____Nanos_PropertyInfo,
	Type_t1A1580695FE92D3355A256FC0A40309494A8C356_CustomAttributesCacheGenerator_Type_t1A1580695FE92D3355A256FC0A40309494A8C356____Parser_PropertyInfo,
	Type_t1A1580695FE92D3355A256FC0A40309494A8C356_CustomAttributesCacheGenerator_Type_t1A1580695FE92D3355A256FC0A40309494A8C356____Descriptor_PropertyInfo,
	Type_t1A1580695FE92D3355A256FC0A40309494A8C356_CustomAttributesCacheGenerator_Type_t1A1580695FE92D3355A256FC0A40309494A8C356____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo,
	Type_t1A1580695FE92D3355A256FC0A40309494A8C356_CustomAttributesCacheGenerator_Type_t1A1580695FE92D3355A256FC0A40309494A8C356____Name_PropertyInfo,
	Type_t1A1580695FE92D3355A256FC0A40309494A8C356_CustomAttributesCacheGenerator_Type_t1A1580695FE92D3355A256FC0A40309494A8C356____SourceContext_PropertyInfo,
	Type_t1A1580695FE92D3355A256FC0A40309494A8C356_CustomAttributesCacheGenerator_Type_t1A1580695FE92D3355A256FC0A40309494A8C356____Syntax_PropertyInfo,
	Field_t79E55EF79874715920B1FA76F36897A5D907AD7C_CustomAttributesCacheGenerator_Field_t79E55EF79874715920B1FA76F36897A5D907AD7C____Parser_PropertyInfo,
	Field_t79E55EF79874715920B1FA76F36897A5D907AD7C_CustomAttributesCacheGenerator_Field_t79E55EF79874715920B1FA76F36897A5D907AD7C____Descriptor_PropertyInfo,
	Field_t79E55EF79874715920B1FA76F36897A5D907AD7C_CustomAttributesCacheGenerator_Field_t79E55EF79874715920B1FA76F36897A5D907AD7C____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo,
	Field_t79E55EF79874715920B1FA76F36897A5D907AD7C_CustomAttributesCacheGenerator_Field_t79E55EF79874715920B1FA76F36897A5D907AD7C____Kind_PropertyInfo,
	Field_t79E55EF79874715920B1FA76F36897A5D907AD7C_CustomAttributesCacheGenerator_Field_t79E55EF79874715920B1FA76F36897A5D907AD7C____Cardinality_PropertyInfo,
	Field_t79E55EF79874715920B1FA76F36897A5D907AD7C_CustomAttributesCacheGenerator_Field_t79E55EF79874715920B1FA76F36897A5D907AD7C____Number_PropertyInfo,
	Field_t79E55EF79874715920B1FA76F36897A5D907AD7C_CustomAttributesCacheGenerator_Field_t79E55EF79874715920B1FA76F36897A5D907AD7C____Name_PropertyInfo,
	Field_t79E55EF79874715920B1FA76F36897A5D907AD7C_CustomAttributesCacheGenerator_Field_t79E55EF79874715920B1FA76F36897A5D907AD7C____TypeUrl_PropertyInfo,
	Field_t79E55EF79874715920B1FA76F36897A5D907AD7C_CustomAttributesCacheGenerator_Field_t79E55EF79874715920B1FA76F36897A5D907AD7C____OneofIndex_PropertyInfo,
	Field_t79E55EF79874715920B1FA76F36897A5D907AD7C_CustomAttributesCacheGenerator_Field_t79E55EF79874715920B1FA76F36897A5D907AD7C____Packed_PropertyInfo,
	Field_t79E55EF79874715920B1FA76F36897A5D907AD7C_CustomAttributesCacheGenerator_Field_t79E55EF79874715920B1FA76F36897A5D907AD7C____JsonName_PropertyInfo,
	Field_t79E55EF79874715920B1FA76F36897A5D907AD7C_CustomAttributesCacheGenerator_Field_t79E55EF79874715920B1FA76F36897A5D907AD7C____DefaultValue_PropertyInfo,
	Enum_tF9B938E22D1A8C7105A3A0A97AF51439675CEF87_CustomAttributesCacheGenerator_Enum_tF9B938E22D1A8C7105A3A0A97AF51439675CEF87____Parser_PropertyInfo,
	Enum_tF9B938E22D1A8C7105A3A0A97AF51439675CEF87_CustomAttributesCacheGenerator_Enum_tF9B938E22D1A8C7105A3A0A97AF51439675CEF87____Descriptor_PropertyInfo,
	Enum_tF9B938E22D1A8C7105A3A0A97AF51439675CEF87_CustomAttributesCacheGenerator_Enum_tF9B938E22D1A8C7105A3A0A97AF51439675CEF87____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo,
	Enum_tF9B938E22D1A8C7105A3A0A97AF51439675CEF87_CustomAttributesCacheGenerator_Enum_tF9B938E22D1A8C7105A3A0A97AF51439675CEF87____Name_PropertyInfo,
	Enum_tF9B938E22D1A8C7105A3A0A97AF51439675CEF87_CustomAttributesCacheGenerator_Enum_tF9B938E22D1A8C7105A3A0A97AF51439675CEF87____SourceContext_PropertyInfo,
	Enum_tF9B938E22D1A8C7105A3A0A97AF51439675CEF87_CustomAttributesCacheGenerator_Enum_tF9B938E22D1A8C7105A3A0A97AF51439675CEF87____Syntax_PropertyInfo,
	EnumValue_t24721FCB233B8D4D64FB5DC62D67C63AB4C3B5AE_CustomAttributesCacheGenerator_EnumValue_t24721FCB233B8D4D64FB5DC62D67C63AB4C3B5AE____Parser_PropertyInfo,
	EnumValue_t24721FCB233B8D4D64FB5DC62D67C63AB4C3B5AE_CustomAttributesCacheGenerator_EnumValue_t24721FCB233B8D4D64FB5DC62D67C63AB4C3B5AE____Descriptor_PropertyInfo,
	EnumValue_t24721FCB233B8D4D64FB5DC62D67C63AB4C3B5AE_CustomAttributesCacheGenerator_EnumValue_t24721FCB233B8D4D64FB5DC62D67C63AB4C3B5AE____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo,
	EnumValue_t24721FCB233B8D4D64FB5DC62D67C63AB4C3B5AE_CustomAttributesCacheGenerator_EnumValue_t24721FCB233B8D4D64FB5DC62D67C63AB4C3B5AE____Name_PropertyInfo,
	EnumValue_t24721FCB233B8D4D64FB5DC62D67C63AB4C3B5AE_CustomAttributesCacheGenerator_EnumValue_t24721FCB233B8D4D64FB5DC62D67C63AB4C3B5AE____Number_PropertyInfo,
	Option_tF75313E4E874EF52E854420D3DEEA04A99AE0E7F_CustomAttributesCacheGenerator_Option_tF75313E4E874EF52E854420D3DEEA04A99AE0E7F____Parser_PropertyInfo,
	Option_tF75313E4E874EF52E854420D3DEEA04A99AE0E7F_CustomAttributesCacheGenerator_Option_tF75313E4E874EF52E854420D3DEEA04A99AE0E7F____Descriptor_PropertyInfo,
	Option_tF75313E4E874EF52E854420D3DEEA04A99AE0E7F_CustomAttributesCacheGenerator_Option_tF75313E4E874EF52E854420D3DEEA04A99AE0E7F____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo,
	Option_tF75313E4E874EF52E854420D3DEEA04A99AE0E7F_CustomAttributesCacheGenerator_Option_tF75313E4E874EF52E854420D3DEEA04A99AE0E7F____Name_PropertyInfo,
	Option_tF75313E4E874EF52E854420D3DEEA04A99AE0E7F_CustomAttributesCacheGenerator_Option_tF75313E4E874EF52E854420D3DEEA04A99AE0E7F____Value_PropertyInfo,
	FileDescriptorSet_t033C2CF3F219A2421776E1BD1042AC2757EFAC3E_CustomAttributesCacheGenerator_FileDescriptorSet_t033C2CF3F219A2421776E1BD1042AC2757EFAC3E____Parser_PropertyInfo,
	FileDescriptorSet_t033C2CF3F219A2421776E1BD1042AC2757EFAC3E_CustomAttributesCacheGenerator_FileDescriptorSet_t033C2CF3F219A2421776E1BD1042AC2757EFAC3E____Descriptor_PropertyInfo,
	FileDescriptorSet_t033C2CF3F219A2421776E1BD1042AC2757EFAC3E_CustomAttributesCacheGenerator_FileDescriptorSet_t033C2CF3F219A2421776E1BD1042AC2757EFAC3E____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo,
	FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0_CustomAttributesCacheGenerator_FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0____Parser_PropertyInfo,
	FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0_CustomAttributesCacheGenerator_FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0____Descriptor_PropertyInfo,
	FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0_CustomAttributesCacheGenerator_FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo,
	FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0_CustomAttributesCacheGenerator_FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0____Name_PropertyInfo,
	FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0_CustomAttributesCacheGenerator_FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0____HasName_PropertyInfo,
	FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0_CustomAttributesCacheGenerator_FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0____Package_PropertyInfo,
	FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0_CustomAttributesCacheGenerator_FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0____HasPackage_PropertyInfo,
	FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0_CustomAttributesCacheGenerator_FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0____Dependency_PropertyInfo,
	FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0_CustomAttributesCacheGenerator_FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0____PublicDependency_PropertyInfo,
	FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0_CustomAttributesCacheGenerator_FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0____MessageType_PropertyInfo,
	FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0_CustomAttributesCacheGenerator_FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0____EnumType_PropertyInfo,
	FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0_CustomAttributesCacheGenerator_FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0____Service_PropertyInfo,
	FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0_CustomAttributesCacheGenerator_FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0____Extension_PropertyInfo,
	FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0_CustomAttributesCacheGenerator_FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0____Options_PropertyInfo,
	FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0_CustomAttributesCacheGenerator_FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0____SourceCodeInfo_PropertyInfo,
	FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0_CustomAttributesCacheGenerator_FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0____Syntax_PropertyInfo,
	FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0_CustomAttributesCacheGenerator_FileDescriptorProto_t964EA206FAE099BA26C4954F9E0AA97B643020E0____HasSyntax_PropertyInfo,
	DescriptorProto_tB03AF5EFB75071C3E950F726F78461450496C898_CustomAttributesCacheGenerator_DescriptorProto_tB03AF5EFB75071C3E950F726F78461450496C898____Parser_PropertyInfo,
	DescriptorProto_tB03AF5EFB75071C3E950F726F78461450496C898_CustomAttributesCacheGenerator_DescriptorProto_tB03AF5EFB75071C3E950F726F78461450496C898____Descriptor_PropertyInfo,
	DescriptorProto_tB03AF5EFB75071C3E950F726F78461450496C898_CustomAttributesCacheGenerator_DescriptorProto_tB03AF5EFB75071C3E950F726F78461450496C898____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo,
	DescriptorProto_tB03AF5EFB75071C3E950F726F78461450496C898_CustomAttributesCacheGenerator_DescriptorProto_tB03AF5EFB75071C3E950F726F78461450496C898____Name_PropertyInfo,
	DescriptorProto_tB03AF5EFB75071C3E950F726F78461450496C898_CustomAttributesCacheGenerator_DescriptorProto_tB03AF5EFB75071C3E950F726F78461450496C898____HasName_PropertyInfo,
	DescriptorProto_tB03AF5EFB75071C3E950F726F78461450496C898_CustomAttributesCacheGenerator_DescriptorProto_tB03AF5EFB75071C3E950F726F78461450496C898____Field_PropertyInfo,
	DescriptorProto_tB03AF5EFB75071C3E950F726F78461450496C898_CustomAttributesCacheGenerator_DescriptorProto_tB03AF5EFB75071C3E950F726F78461450496C898____Extension_PropertyInfo,
	DescriptorProto_tB03AF5EFB75071C3E950F726F78461450496C898_CustomAttributesCacheGenerator_DescriptorProto_tB03AF5EFB75071C3E950F726F78461450496C898____NestedType_PropertyInfo,
	DescriptorProto_tB03AF5EFB75071C3E950F726F78461450496C898_CustomAttributesCacheGenerator_DescriptorProto_tB03AF5EFB75071C3E950F726F78461450496C898____EnumType_PropertyInfo,
	DescriptorProto_tB03AF5EFB75071C3E950F726F78461450496C898_CustomAttributesCacheGenerator_DescriptorProto_tB03AF5EFB75071C3E950F726F78461450496C898____OneofDecl_PropertyInfo,
	DescriptorProto_tB03AF5EFB75071C3E950F726F78461450496C898_CustomAttributesCacheGenerator_DescriptorProto_tB03AF5EFB75071C3E950F726F78461450496C898____Options_PropertyInfo,
	ExtensionRange_t355AED379D3458AC80443047C2DF72AA44639D84_CustomAttributesCacheGenerator_ExtensionRange_t355AED379D3458AC80443047C2DF72AA44639D84____Parser_PropertyInfo,
	ExtensionRange_t355AED379D3458AC80443047C2DF72AA44639D84_CustomAttributesCacheGenerator_ExtensionRange_t355AED379D3458AC80443047C2DF72AA44639D84____Descriptor_PropertyInfo,
	ExtensionRange_t355AED379D3458AC80443047C2DF72AA44639D84_CustomAttributesCacheGenerator_ExtensionRange_t355AED379D3458AC80443047C2DF72AA44639D84____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo,
	ExtensionRange_t355AED379D3458AC80443047C2DF72AA44639D84_CustomAttributesCacheGenerator_ExtensionRange_t355AED379D3458AC80443047C2DF72AA44639D84____Start_PropertyInfo,
	ExtensionRange_t355AED379D3458AC80443047C2DF72AA44639D84_CustomAttributesCacheGenerator_ExtensionRange_t355AED379D3458AC80443047C2DF72AA44639D84____HasStart_PropertyInfo,
	ExtensionRange_t355AED379D3458AC80443047C2DF72AA44639D84_CustomAttributesCacheGenerator_ExtensionRange_t355AED379D3458AC80443047C2DF72AA44639D84____End_PropertyInfo,
	ExtensionRange_t355AED379D3458AC80443047C2DF72AA44639D84_CustomAttributesCacheGenerator_ExtensionRange_t355AED379D3458AC80443047C2DF72AA44639D84____HasEnd_PropertyInfo,
	ExtensionRange_t355AED379D3458AC80443047C2DF72AA44639D84_CustomAttributesCacheGenerator_ExtensionRange_t355AED379D3458AC80443047C2DF72AA44639D84____Options_PropertyInfo,
	ReservedRange_tAED8F62AF040F5F133732196696893B313EA458B_CustomAttributesCacheGenerator_ReservedRange_tAED8F62AF040F5F133732196696893B313EA458B____Parser_PropertyInfo,
	ReservedRange_tAED8F62AF040F5F133732196696893B313EA458B_CustomAttributesCacheGenerator_ReservedRange_tAED8F62AF040F5F133732196696893B313EA458B____Descriptor_PropertyInfo,
	ReservedRange_tAED8F62AF040F5F133732196696893B313EA458B_CustomAttributesCacheGenerator_ReservedRange_tAED8F62AF040F5F133732196696893B313EA458B____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo,
	ReservedRange_tAED8F62AF040F5F133732196696893B313EA458B_CustomAttributesCacheGenerator_ReservedRange_tAED8F62AF040F5F133732196696893B313EA458B____Start_PropertyInfo,
	ReservedRange_tAED8F62AF040F5F133732196696893B313EA458B_CustomAttributesCacheGenerator_ReservedRange_tAED8F62AF040F5F133732196696893B313EA458B____HasStart_PropertyInfo,
	ReservedRange_tAED8F62AF040F5F133732196696893B313EA458B_CustomAttributesCacheGenerator_ReservedRange_tAED8F62AF040F5F133732196696893B313EA458B____End_PropertyInfo,
	ReservedRange_tAED8F62AF040F5F133732196696893B313EA458B_CustomAttributesCacheGenerator_ReservedRange_tAED8F62AF040F5F133732196696893B313EA458B____HasEnd_PropertyInfo,
	ExtensionRangeOptions_t497AE48098D601FD2AB661860BDA91704902E99F_CustomAttributesCacheGenerator_ExtensionRangeOptions_t497AE48098D601FD2AB661860BDA91704902E99F____Parser_PropertyInfo,
	ExtensionRangeOptions_t497AE48098D601FD2AB661860BDA91704902E99F_CustomAttributesCacheGenerator_ExtensionRangeOptions_t497AE48098D601FD2AB661860BDA91704902E99F____Descriptor_PropertyInfo,
	ExtensionRangeOptions_t497AE48098D601FD2AB661860BDA91704902E99F_CustomAttributesCacheGenerator_ExtensionRangeOptions_t497AE48098D601FD2AB661860BDA91704902E99F____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo,
	FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785_CustomAttributesCacheGenerator_FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785____Parser_PropertyInfo,
	FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785_CustomAttributesCacheGenerator_FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785____Descriptor_PropertyInfo,
	FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785_CustomAttributesCacheGenerator_FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo,
	FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785_CustomAttributesCacheGenerator_FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785____Name_PropertyInfo,
	FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785_CustomAttributesCacheGenerator_FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785____HasName_PropertyInfo,
	FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785_CustomAttributesCacheGenerator_FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785____Number_PropertyInfo,
	FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785_CustomAttributesCacheGenerator_FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785____HasNumber_PropertyInfo,
	FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785_CustomAttributesCacheGenerator_FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785____Label_PropertyInfo,
	FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785_CustomAttributesCacheGenerator_FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785____HasLabel_PropertyInfo,
	FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785_CustomAttributesCacheGenerator_FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785____Type_PropertyInfo,
	FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785_CustomAttributesCacheGenerator_FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785____HasType_PropertyInfo,
	FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785_CustomAttributesCacheGenerator_FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785____TypeName_PropertyInfo,
	FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785_CustomAttributesCacheGenerator_FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785____HasTypeName_PropertyInfo,
	FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785_CustomAttributesCacheGenerator_FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785____Extendee_PropertyInfo,
	FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785_CustomAttributesCacheGenerator_FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785____HasExtendee_PropertyInfo,
	FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785_CustomAttributesCacheGenerator_FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785____DefaultValue_PropertyInfo,
	FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785_CustomAttributesCacheGenerator_FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785____HasDefaultValue_PropertyInfo,
	FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785_CustomAttributesCacheGenerator_FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785____OneofIndex_PropertyInfo,
	FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785_CustomAttributesCacheGenerator_FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785____HasOneofIndex_PropertyInfo,
	FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785_CustomAttributesCacheGenerator_FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785____JsonName_PropertyInfo,
	FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785_CustomAttributesCacheGenerator_FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785____HasJsonName_PropertyInfo,
	FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785_CustomAttributesCacheGenerator_FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785____Options_PropertyInfo,
	FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785_CustomAttributesCacheGenerator_FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785____Proto3Optional_PropertyInfo,
	FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785_CustomAttributesCacheGenerator_FieldDescriptorProto_t4E2002BDA1B5858C31B5A71E198580D27B1DF785____HasProto3Optional_PropertyInfo,
	OneofDescriptorProto_t6319E05084FB5AA0644D3AEF37891689AFF1DD9A_CustomAttributesCacheGenerator_OneofDescriptorProto_t6319E05084FB5AA0644D3AEF37891689AFF1DD9A____Parser_PropertyInfo,
	OneofDescriptorProto_t6319E05084FB5AA0644D3AEF37891689AFF1DD9A_CustomAttributesCacheGenerator_OneofDescriptorProto_t6319E05084FB5AA0644D3AEF37891689AFF1DD9A____Descriptor_PropertyInfo,
	OneofDescriptorProto_t6319E05084FB5AA0644D3AEF37891689AFF1DD9A_CustomAttributesCacheGenerator_OneofDescriptorProto_t6319E05084FB5AA0644D3AEF37891689AFF1DD9A____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo,
	OneofDescriptorProto_t6319E05084FB5AA0644D3AEF37891689AFF1DD9A_CustomAttributesCacheGenerator_OneofDescriptorProto_t6319E05084FB5AA0644D3AEF37891689AFF1DD9A____Name_PropertyInfo,
	OneofDescriptorProto_t6319E05084FB5AA0644D3AEF37891689AFF1DD9A_CustomAttributesCacheGenerator_OneofDescriptorProto_t6319E05084FB5AA0644D3AEF37891689AFF1DD9A____HasName_PropertyInfo,
	OneofDescriptorProto_t6319E05084FB5AA0644D3AEF37891689AFF1DD9A_CustomAttributesCacheGenerator_OneofDescriptorProto_t6319E05084FB5AA0644D3AEF37891689AFF1DD9A____Options_PropertyInfo,
	EnumDescriptorProto_t0E60F3628E51BC270FDA740DCBC83DBF04FA0478_CustomAttributesCacheGenerator_EnumDescriptorProto_t0E60F3628E51BC270FDA740DCBC83DBF04FA0478____Parser_PropertyInfo,
	EnumDescriptorProto_t0E60F3628E51BC270FDA740DCBC83DBF04FA0478_CustomAttributesCacheGenerator_EnumDescriptorProto_t0E60F3628E51BC270FDA740DCBC83DBF04FA0478____Descriptor_PropertyInfo,
	EnumDescriptorProto_t0E60F3628E51BC270FDA740DCBC83DBF04FA0478_CustomAttributesCacheGenerator_EnumDescriptorProto_t0E60F3628E51BC270FDA740DCBC83DBF04FA0478____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo,
	EnumDescriptorProto_t0E60F3628E51BC270FDA740DCBC83DBF04FA0478_CustomAttributesCacheGenerator_EnumDescriptorProto_t0E60F3628E51BC270FDA740DCBC83DBF04FA0478____Name_PropertyInfo,
	EnumDescriptorProto_t0E60F3628E51BC270FDA740DCBC83DBF04FA0478_CustomAttributesCacheGenerator_EnumDescriptorProto_t0E60F3628E51BC270FDA740DCBC83DBF04FA0478____HasName_PropertyInfo,
	EnumDescriptorProto_t0E60F3628E51BC270FDA740DCBC83DBF04FA0478_CustomAttributesCacheGenerator_EnumDescriptorProto_t0E60F3628E51BC270FDA740DCBC83DBF04FA0478____Value_PropertyInfo,
	EnumDescriptorProto_t0E60F3628E51BC270FDA740DCBC83DBF04FA0478_CustomAttributesCacheGenerator_EnumDescriptorProto_t0E60F3628E51BC270FDA740DCBC83DBF04FA0478____Options_PropertyInfo,
	EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41_CustomAttributesCacheGenerator_EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41____Parser_PropertyInfo,
	EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41_CustomAttributesCacheGenerator_EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41____Descriptor_PropertyInfo,
	EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41_CustomAttributesCacheGenerator_EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo,
	EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41_CustomAttributesCacheGenerator_EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41____Start_PropertyInfo,
	EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41_CustomAttributesCacheGenerator_EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41____HasStart_PropertyInfo,
	EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41_CustomAttributesCacheGenerator_EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41____End_PropertyInfo,
	EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41_CustomAttributesCacheGenerator_EnumReservedRange_tC8CD8C11ED76FC8A6354737EF0BB837447C04F41____HasEnd_PropertyInfo,
	EnumValueDescriptorProto_t24F4825414596CE28D7979C4F352A8EE30AE8315_CustomAttributesCacheGenerator_EnumValueDescriptorProto_t24F4825414596CE28D7979C4F352A8EE30AE8315____Parser_PropertyInfo,
	EnumValueDescriptorProto_t24F4825414596CE28D7979C4F352A8EE30AE8315_CustomAttributesCacheGenerator_EnumValueDescriptorProto_t24F4825414596CE28D7979C4F352A8EE30AE8315____Descriptor_PropertyInfo,
	EnumValueDescriptorProto_t24F4825414596CE28D7979C4F352A8EE30AE8315_CustomAttributesCacheGenerator_EnumValueDescriptorProto_t24F4825414596CE28D7979C4F352A8EE30AE8315____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo,
	EnumValueDescriptorProto_t24F4825414596CE28D7979C4F352A8EE30AE8315_CustomAttributesCacheGenerator_EnumValueDescriptorProto_t24F4825414596CE28D7979C4F352A8EE30AE8315____Name_PropertyInfo,
	EnumValueDescriptorProto_t24F4825414596CE28D7979C4F352A8EE30AE8315_CustomAttributesCacheGenerator_EnumValueDescriptorProto_t24F4825414596CE28D7979C4F352A8EE30AE8315____HasName_PropertyInfo,
	EnumValueDescriptorProto_t24F4825414596CE28D7979C4F352A8EE30AE8315_CustomAttributesCacheGenerator_EnumValueDescriptorProto_t24F4825414596CE28D7979C4F352A8EE30AE8315____Number_PropertyInfo,
	EnumValueDescriptorProto_t24F4825414596CE28D7979C4F352A8EE30AE8315_CustomAttributesCacheGenerator_EnumValueDescriptorProto_t24F4825414596CE28D7979C4F352A8EE30AE8315____HasNumber_PropertyInfo,
	EnumValueDescriptorProto_t24F4825414596CE28D7979C4F352A8EE30AE8315_CustomAttributesCacheGenerator_EnumValueDescriptorProto_t24F4825414596CE28D7979C4F352A8EE30AE8315____Options_PropertyInfo,
	ServiceDescriptorProto_t39FE93732D4E2D7AA71D61E86B211AAADA573DB3_CustomAttributesCacheGenerator_ServiceDescriptorProto_t39FE93732D4E2D7AA71D61E86B211AAADA573DB3____Parser_PropertyInfo,
	ServiceDescriptorProto_t39FE93732D4E2D7AA71D61E86B211AAADA573DB3_CustomAttributesCacheGenerator_ServiceDescriptorProto_t39FE93732D4E2D7AA71D61E86B211AAADA573DB3____Descriptor_PropertyInfo,
	ServiceDescriptorProto_t39FE93732D4E2D7AA71D61E86B211AAADA573DB3_CustomAttributesCacheGenerator_ServiceDescriptorProto_t39FE93732D4E2D7AA71D61E86B211AAADA573DB3____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo,
	ServiceDescriptorProto_t39FE93732D4E2D7AA71D61E86B211AAADA573DB3_CustomAttributesCacheGenerator_ServiceDescriptorProto_t39FE93732D4E2D7AA71D61E86B211AAADA573DB3____Name_PropertyInfo,
	ServiceDescriptorProto_t39FE93732D4E2D7AA71D61E86B211AAADA573DB3_CustomAttributesCacheGenerator_ServiceDescriptorProto_t39FE93732D4E2D7AA71D61E86B211AAADA573DB3____HasName_PropertyInfo,
	ServiceDescriptorProto_t39FE93732D4E2D7AA71D61E86B211AAADA573DB3_CustomAttributesCacheGenerator_ServiceDescriptorProto_t39FE93732D4E2D7AA71D61E86B211AAADA573DB3____Method_PropertyInfo,
	ServiceDescriptorProto_t39FE93732D4E2D7AA71D61E86B211AAADA573DB3_CustomAttributesCacheGenerator_ServiceDescriptorProto_t39FE93732D4E2D7AA71D61E86B211AAADA573DB3____Options_PropertyInfo,
	MethodDescriptorProto_t88EE2E0AFE0DEEDDD27A1DAFA0451A259924D638_CustomAttributesCacheGenerator_MethodDescriptorProto_t88EE2E0AFE0DEEDDD27A1DAFA0451A259924D638____Parser_PropertyInfo,
	MethodDescriptorProto_t88EE2E0AFE0DEEDDD27A1DAFA0451A259924D638_CustomAttributesCacheGenerator_MethodDescriptorProto_t88EE2E0AFE0DEEDDD27A1DAFA0451A259924D638____Descriptor_PropertyInfo,
	MethodDescriptorProto_t88EE2E0AFE0DEEDDD27A1DAFA0451A259924D638_CustomAttributesCacheGenerator_MethodDescriptorProto_t88EE2E0AFE0DEEDDD27A1DAFA0451A259924D638____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo,
	MethodDescriptorProto_t88EE2E0AFE0DEEDDD27A1DAFA0451A259924D638_CustomAttributesCacheGenerator_MethodDescriptorProto_t88EE2E0AFE0DEEDDD27A1DAFA0451A259924D638____Name_PropertyInfo,
	MethodDescriptorProto_t88EE2E0AFE0DEEDDD27A1DAFA0451A259924D638_CustomAttributesCacheGenerator_MethodDescriptorProto_t88EE2E0AFE0DEEDDD27A1DAFA0451A259924D638____HasName_PropertyInfo,
	MethodDescriptorProto_t88EE2E0AFE0DEEDDD27A1DAFA0451A259924D638_CustomAttributesCacheGenerator_MethodDescriptorProto_t88EE2E0AFE0DEEDDD27A1DAFA0451A259924D638____InputType_PropertyInfo,
	MethodDescriptorProto_t88EE2E0AFE0DEEDDD27A1DAFA0451A259924D638_CustomAttributesCacheGenerator_MethodDescriptorProto_t88EE2E0AFE0DEEDDD27A1DAFA0451A259924D638____HasInputType_PropertyInfo,
	MethodDescriptorProto_t88EE2E0AFE0DEEDDD27A1DAFA0451A259924D638_CustomAttributesCacheGenerator_MethodDescriptorProto_t88EE2E0AFE0DEEDDD27A1DAFA0451A259924D638____OutputType_PropertyInfo,
	MethodDescriptorProto_t88EE2E0AFE0DEEDDD27A1DAFA0451A259924D638_CustomAttributesCacheGenerator_MethodDescriptorProto_t88EE2E0AFE0DEEDDD27A1DAFA0451A259924D638____HasOutputType_PropertyInfo,
	MethodDescriptorProto_t88EE2E0AFE0DEEDDD27A1DAFA0451A259924D638_CustomAttributesCacheGenerator_MethodDescriptorProto_t88EE2E0AFE0DEEDDD27A1DAFA0451A259924D638____Options_PropertyInfo,
	MethodDescriptorProto_t88EE2E0AFE0DEEDDD27A1DAFA0451A259924D638_CustomAttributesCacheGenerator_MethodDescriptorProto_t88EE2E0AFE0DEEDDD27A1DAFA0451A259924D638____ClientStreaming_PropertyInfo,
	MethodDescriptorProto_t88EE2E0AFE0DEEDDD27A1DAFA0451A259924D638_CustomAttributesCacheGenerator_MethodDescriptorProto_t88EE2E0AFE0DEEDDD27A1DAFA0451A259924D638____HasClientStreaming_PropertyInfo,
	MethodDescriptorProto_t88EE2E0AFE0DEEDDD27A1DAFA0451A259924D638_CustomAttributesCacheGenerator_MethodDescriptorProto_t88EE2E0AFE0DEEDDD27A1DAFA0451A259924D638____ServerStreaming_PropertyInfo,
	MethodDescriptorProto_t88EE2E0AFE0DEEDDD27A1DAFA0451A259924D638_CustomAttributesCacheGenerator_MethodDescriptorProto_t88EE2E0AFE0DEEDDD27A1DAFA0451A259924D638____HasServerStreaming_PropertyInfo,
	FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30____Parser_PropertyInfo,
	FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30____Descriptor_PropertyInfo,
	FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo,
	FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30____JavaPackage_PropertyInfo,
	FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30____HasJavaPackage_PropertyInfo,
	FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30____JavaOuterClassname_PropertyInfo,
	FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30____HasJavaOuterClassname_PropertyInfo,
	FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30____JavaMultipleFiles_PropertyInfo,
	FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30____HasJavaMultipleFiles_PropertyInfo,
	FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30____JavaGenerateEqualsAndHash_PropertyInfo,
	FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30____HasJavaGenerateEqualsAndHash_PropertyInfo,
	FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30____JavaStringCheckUtf8_PropertyInfo,
	FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30____HasJavaStringCheckUtf8_PropertyInfo,
	FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30____OptimizeFor_PropertyInfo,
	FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30____HasOptimizeFor_PropertyInfo,
	FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30____GoPackage_PropertyInfo,
	FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30____HasGoPackage_PropertyInfo,
	FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30____CcGenericServices_PropertyInfo,
	FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30____HasCcGenericServices_PropertyInfo,
	FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30____JavaGenericServices_PropertyInfo,
	FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30____HasJavaGenericServices_PropertyInfo,
	FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30____PyGenericServices_PropertyInfo,
	FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30____HasPyGenericServices_PropertyInfo,
	FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30____PhpGenericServices_PropertyInfo,
	FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30____HasPhpGenericServices_PropertyInfo,
	FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30____Deprecated_PropertyInfo,
	FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30____HasDeprecated_PropertyInfo,
	FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30____CcEnableArenas_PropertyInfo,
	FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30____HasCcEnableArenas_PropertyInfo,
	FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30____ObjcClassPrefix_PropertyInfo,
	FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30____HasObjcClassPrefix_PropertyInfo,
	FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30____CsharpNamespace_PropertyInfo,
	FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30____HasCsharpNamespace_PropertyInfo,
	FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30____SwiftPrefix_PropertyInfo,
	FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30____HasSwiftPrefix_PropertyInfo,
	FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30____PhpClassPrefix_PropertyInfo,
	FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30____HasPhpClassPrefix_PropertyInfo,
	FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30____PhpNamespace_PropertyInfo,
	FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30____HasPhpNamespace_PropertyInfo,
	FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30____PhpMetadataNamespace_PropertyInfo,
	FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30____HasPhpMetadataNamespace_PropertyInfo,
	FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30____RubyPackage_PropertyInfo,
	FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30_CustomAttributesCacheGenerator_FileOptions_t1546660DD3F822C837E586DF72F77DB451BC7F30____HasRubyPackage_PropertyInfo,
	MessageOptions_t007E64DA64D7132536A101E9AA32FBFE31FFADBD_CustomAttributesCacheGenerator_MessageOptions_t007E64DA64D7132536A101E9AA32FBFE31FFADBD____Parser_PropertyInfo,
	MessageOptions_t007E64DA64D7132536A101E9AA32FBFE31FFADBD_CustomAttributesCacheGenerator_MessageOptions_t007E64DA64D7132536A101E9AA32FBFE31FFADBD____Descriptor_PropertyInfo,
	MessageOptions_t007E64DA64D7132536A101E9AA32FBFE31FFADBD_CustomAttributesCacheGenerator_MessageOptions_t007E64DA64D7132536A101E9AA32FBFE31FFADBD____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo,
	MessageOptions_t007E64DA64D7132536A101E9AA32FBFE31FFADBD_CustomAttributesCacheGenerator_MessageOptions_t007E64DA64D7132536A101E9AA32FBFE31FFADBD____MessageSetWireFormat_PropertyInfo,
	MessageOptions_t007E64DA64D7132536A101E9AA32FBFE31FFADBD_CustomAttributesCacheGenerator_MessageOptions_t007E64DA64D7132536A101E9AA32FBFE31FFADBD____HasMessageSetWireFormat_PropertyInfo,
	MessageOptions_t007E64DA64D7132536A101E9AA32FBFE31FFADBD_CustomAttributesCacheGenerator_MessageOptions_t007E64DA64D7132536A101E9AA32FBFE31FFADBD____NoStandardDescriptorAccessor_PropertyInfo,
	MessageOptions_t007E64DA64D7132536A101E9AA32FBFE31FFADBD_CustomAttributesCacheGenerator_MessageOptions_t007E64DA64D7132536A101E9AA32FBFE31FFADBD____HasNoStandardDescriptorAccessor_PropertyInfo,
	MessageOptions_t007E64DA64D7132536A101E9AA32FBFE31FFADBD_CustomAttributesCacheGenerator_MessageOptions_t007E64DA64D7132536A101E9AA32FBFE31FFADBD____Deprecated_PropertyInfo,
	MessageOptions_t007E64DA64D7132536A101E9AA32FBFE31FFADBD_CustomAttributesCacheGenerator_MessageOptions_t007E64DA64D7132536A101E9AA32FBFE31FFADBD____HasDeprecated_PropertyInfo,
	MessageOptions_t007E64DA64D7132536A101E9AA32FBFE31FFADBD_CustomAttributesCacheGenerator_MessageOptions_t007E64DA64D7132536A101E9AA32FBFE31FFADBD____MapEntry_PropertyInfo,
	MessageOptions_t007E64DA64D7132536A101E9AA32FBFE31FFADBD_CustomAttributesCacheGenerator_MessageOptions_t007E64DA64D7132536A101E9AA32FBFE31FFADBD____HasMapEntry_PropertyInfo,
	FieldOptions_t3B2D61638B0B06A886A821421DA06C09E4A78BEA_CustomAttributesCacheGenerator_FieldOptions_t3B2D61638B0B06A886A821421DA06C09E4A78BEA____Parser_PropertyInfo,
	FieldOptions_t3B2D61638B0B06A886A821421DA06C09E4A78BEA_CustomAttributesCacheGenerator_FieldOptions_t3B2D61638B0B06A886A821421DA06C09E4A78BEA____Descriptor_PropertyInfo,
	FieldOptions_t3B2D61638B0B06A886A821421DA06C09E4A78BEA_CustomAttributesCacheGenerator_FieldOptions_t3B2D61638B0B06A886A821421DA06C09E4A78BEA____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo,
	FieldOptions_t3B2D61638B0B06A886A821421DA06C09E4A78BEA_CustomAttributesCacheGenerator_FieldOptions_t3B2D61638B0B06A886A821421DA06C09E4A78BEA____Ctype_PropertyInfo,
	FieldOptions_t3B2D61638B0B06A886A821421DA06C09E4A78BEA_CustomAttributesCacheGenerator_FieldOptions_t3B2D61638B0B06A886A821421DA06C09E4A78BEA____HasCtype_PropertyInfo,
	FieldOptions_t3B2D61638B0B06A886A821421DA06C09E4A78BEA_CustomAttributesCacheGenerator_FieldOptions_t3B2D61638B0B06A886A821421DA06C09E4A78BEA____Packed_PropertyInfo,
	FieldOptions_t3B2D61638B0B06A886A821421DA06C09E4A78BEA_CustomAttributesCacheGenerator_FieldOptions_t3B2D61638B0B06A886A821421DA06C09E4A78BEA____HasPacked_PropertyInfo,
	FieldOptions_t3B2D61638B0B06A886A821421DA06C09E4A78BEA_CustomAttributesCacheGenerator_FieldOptions_t3B2D61638B0B06A886A821421DA06C09E4A78BEA____Jstype_PropertyInfo,
	FieldOptions_t3B2D61638B0B06A886A821421DA06C09E4A78BEA_CustomAttributesCacheGenerator_FieldOptions_t3B2D61638B0B06A886A821421DA06C09E4A78BEA____HasJstype_PropertyInfo,
	FieldOptions_t3B2D61638B0B06A886A821421DA06C09E4A78BEA_CustomAttributesCacheGenerator_FieldOptions_t3B2D61638B0B06A886A821421DA06C09E4A78BEA____Lazy_PropertyInfo,
	FieldOptions_t3B2D61638B0B06A886A821421DA06C09E4A78BEA_CustomAttributesCacheGenerator_FieldOptions_t3B2D61638B0B06A886A821421DA06C09E4A78BEA____HasLazy_PropertyInfo,
	FieldOptions_t3B2D61638B0B06A886A821421DA06C09E4A78BEA_CustomAttributesCacheGenerator_FieldOptions_t3B2D61638B0B06A886A821421DA06C09E4A78BEA____Deprecated_PropertyInfo,
	FieldOptions_t3B2D61638B0B06A886A821421DA06C09E4A78BEA_CustomAttributesCacheGenerator_FieldOptions_t3B2D61638B0B06A886A821421DA06C09E4A78BEA____HasDeprecated_PropertyInfo,
	FieldOptions_t3B2D61638B0B06A886A821421DA06C09E4A78BEA_CustomAttributesCacheGenerator_FieldOptions_t3B2D61638B0B06A886A821421DA06C09E4A78BEA____Weak_PropertyInfo,
	FieldOptions_t3B2D61638B0B06A886A821421DA06C09E4A78BEA_CustomAttributesCacheGenerator_FieldOptions_t3B2D61638B0B06A886A821421DA06C09E4A78BEA____HasWeak_PropertyInfo,
	OneofOptions_tE3261CFC4DF0841400F23CBA8578D1191DE59AE7_CustomAttributesCacheGenerator_OneofOptions_tE3261CFC4DF0841400F23CBA8578D1191DE59AE7____Parser_PropertyInfo,
	OneofOptions_tE3261CFC4DF0841400F23CBA8578D1191DE59AE7_CustomAttributesCacheGenerator_OneofOptions_tE3261CFC4DF0841400F23CBA8578D1191DE59AE7____Descriptor_PropertyInfo,
	OneofOptions_tE3261CFC4DF0841400F23CBA8578D1191DE59AE7_CustomAttributesCacheGenerator_OneofOptions_tE3261CFC4DF0841400F23CBA8578D1191DE59AE7____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo,
	EnumOptions_t84BF0A0DAC3BF1F447E6F0DABA6E4EB9B3AF4918_CustomAttributesCacheGenerator_EnumOptions_t84BF0A0DAC3BF1F447E6F0DABA6E4EB9B3AF4918____Parser_PropertyInfo,
	EnumOptions_t84BF0A0DAC3BF1F447E6F0DABA6E4EB9B3AF4918_CustomAttributesCacheGenerator_EnumOptions_t84BF0A0DAC3BF1F447E6F0DABA6E4EB9B3AF4918____Descriptor_PropertyInfo,
	EnumOptions_t84BF0A0DAC3BF1F447E6F0DABA6E4EB9B3AF4918_CustomAttributesCacheGenerator_EnumOptions_t84BF0A0DAC3BF1F447E6F0DABA6E4EB9B3AF4918____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo,
	EnumOptions_t84BF0A0DAC3BF1F447E6F0DABA6E4EB9B3AF4918_CustomAttributesCacheGenerator_EnumOptions_t84BF0A0DAC3BF1F447E6F0DABA6E4EB9B3AF4918____AllowAlias_PropertyInfo,
	EnumOptions_t84BF0A0DAC3BF1F447E6F0DABA6E4EB9B3AF4918_CustomAttributesCacheGenerator_EnumOptions_t84BF0A0DAC3BF1F447E6F0DABA6E4EB9B3AF4918____HasAllowAlias_PropertyInfo,
	EnumOptions_t84BF0A0DAC3BF1F447E6F0DABA6E4EB9B3AF4918_CustomAttributesCacheGenerator_EnumOptions_t84BF0A0DAC3BF1F447E6F0DABA6E4EB9B3AF4918____Deprecated_PropertyInfo,
	EnumOptions_t84BF0A0DAC3BF1F447E6F0DABA6E4EB9B3AF4918_CustomAttributesCacheGenerator_EnumOptions_t84BF0A0DAC3BF1F447E6F0DABA6E4EB9B3AF4918____HasDeprecated_PropertyInfo,
	EnumValueOptions_t3521CCE0345304E9A1355EE2DA6AD9C0621C87A9_CustomAttributesCacheGenerator_EnumValueOptions_t3521CCE0345304E9A1355EE2DA6AD9C0621C87A9____Parser_PropertyInfo,
	EnumValueOptions_t3521CCE0345304E9A1355EE2DA6AD9C0621C87A9_CustomAttributesCacheGenerator_EnumValueOptions_t3521CCE0345304E9A1355EE2DA6AD9C0621C87A9____Descriptor_PropertyInfo,
	EnumValueOptions_t3521CCE0345304E9A1355EE2DA6AD9C0621C87A9_CustomAttributesCacheGenerator_EnumValueOptions_t3521CCE0345304E9A1355EE2DA6AD9C0621C87A9____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo,
	EnumValueOptions_t3521CCE0345304E9A1355EE2DA6AD9C0621C87A9_CustomAttributesCacheGenerator_EnumValueOptions_t3521CCE0345304E9A1355EE2DA6AD9C0621C87A9____Deprecated_PropertyInfo,
	EnumValueOptions_t3521CCE0345304E9A1355EE2DA6AD9C0621C87A9_CustomAttributesCacheGenerator_EnumValueOptions_t3521CCE0345304E9A1355EE2DA6AD9C0621C87A9____HasDeprecated_PropertyInfo,
	ServiceOptions_t3CCBCF9F8215D476834CC7A5808FED49F161D11A_CustomAttributesCacheGenerator_ServiceOptions_t3CCBCF9F8215D476834CC7A5808FED49F161D11A____Parser_PropertyInfo,
	ServiceOptions_t3CCBCF9F8215D476834CC7A5808FED49F161D11A_CustomAttributesCacheGenerator_ServiceOptions_t3CCBCF9F8215D476834CC7A5808FED49F161D11A____Descriptor_PropertyInfo,
	ServiceOptions_t3CCBCF9F8215D476834CC7A5808FED49F161D11A_CustomAttributesCacheGenerator_ServiceOptions_t3CCBCF9F8215D476834CC7A5808FED49F161D11A____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo,
	ServiceOptions_t3CCBCF9F8215D476834CC7A5808FED49F161D11A_CustomAttributesCacheGenerator_ServiceOptions_t3CCBCF9F8215D476834CC7A5808FED49F161D11A____Deprecated_PropertyInfo,
	ServiceOptions_t3CCBCF9F8215D476834CC7A5808FED49F161D11A_CustomAttributesCacheGenerator_ServiceOptions_t3CCBCF9F8215D476834CC7A5808FED49F161D11A____HasDeprecated_PropertyInfo,
	MethodOptions_t94D248D11F5D87A48A25D56B4E21F6E942ED9F4F_CustomAttributesCacheGenerator_MethodOptions_t94D248D11F5D87A48A25D56B4E21F6E942ED9F4F____Parser_PropertyInfo,
	MethodOptions_t94D248D11F5D87A48A25D56B4E21F6E942ED9F4F_CustomAttributesCacheGenerator_MethodOptions_t94D248D11F5D87A48A25D56B4E21F6E942ED9F4F____Descriptor_PropertyInfo,
	MethodOptions_t94D248D11F5D87A48A25D56B4E21F6E942ED9F4F_CustomAttributesCacheGenerator_MethodOptions_t94D248D11F5D87A48A25D56B4E21F6E942ED9F4F____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo,
	MethodOptions_t94D248D11F5D87A48A25D56B4E21F6E942ED9F4F_CustomAttributesCacheGenerator_MethodOptions_t94D248D11F5D87A48A25D56B4E21F6E942ED9F4F____Deprecated_PropertyInfo,
	MethodOptions_t94D248D11F5D87A48A25D56B4E21F6E942ED9F4F_CustomAttributesCacheGenerator_MethodOptions_t94D248D11F5D87A48A25D56B4E21F6E942ED9F4F____HasDeprecated_PropertyInfo,
	MethodOptions_t94D248D11F5D87A48A25D56B4E21F6E942ED9F4F_CustomAttributesCacheGenerator_MethodOptions_t94D248D11F5D87A48A25D56B4E21F6E942ED9F4F____IdempotencyLevel_PropertyInfo,
	MethodOptions_t94D248D11F5D87A48A25D56B4E21F6E942ED9F4F_CustomAttributesCacheGenerator_MethodOptions_t94D248D11F5D87A48A25D56B4E21F6E942ED9F4F____HasIdempotencyLevel_PropertyInfo,
	UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728_CustomAttributesCacheGenerator_UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728____Parser_PropertyInfo,
	UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728_CustomAttributesCacheGenerator_UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728____Descriptor_PropertyInfo,
	UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728_CustomAttributesCacheGenerator_UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo,
	UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728_CustomAttributesCacheGenerator_UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728____IdentifierValue_PropertyInfo,
	UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728_CustomAttributesCacheGenerator_UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728____HasIdentifierValue_PropertyInfo,
	UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728_CustomAttributesCacheGenerator_UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728____PositiveIntValue_PropertyInfo,
	UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728_CustomAttributesCacheGenerator_UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728____HasPositiveIntValue_PropertyInfo,
	UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728_CustomAttributesCacheGenerator_UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728____NegativeIntValue_PropertyInfo,
	UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728_CustomAttributesCacheGenerator_UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728____HasNegativeIntValue_PropertyInfo,
	UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728_CustomAttributesCacheGenerator_UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728____DoubleValue_PropertyInfo,
	UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728_CustomAttributesCacheGenerator_UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728____HasDoubleValue_PropertyInfo,
	UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728_CustomAttributesCacheGenerator_UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728____StringValue_PropertyInfo,
	UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728_CustomAttributesCacheGenerator_UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728____HasStringValue_PropertyInfo,
	UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728_CustomAttributesCacheGenerator_UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728____AggregateValue_PropertyInfo,
	UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728_CustomAttributesCacheGenerator_UninterpretedOption_t2A87FC9A02625D20FEFDFB9DD9CBF32DF3C41728____HasAggregateValue_PropertyInfo,
	NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22_CustomAttributesCacheGenerator_NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22____Parser_PropertyInfo,
	NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22_CustomAttributesCacheGenerator_NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22____Descriptor_PropertyInfo,
	NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22_CustomAttributesCacheGenerator_NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo,
	NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22_CustomAttributesCacheGenerator_NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22____NamePart__PropertyInfo,
	NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22_CustomAttributesCacheGenerator_NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22____HasNamePart__PropertyInfo,
	NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22_CustomAttributesCacheGenerator_NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22____IsExtension_PropertyInfo,
	NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22_CustomAttributesCacheGenerator_NamePart_t8792F24704E1912597E655EA2B1305BCD0FCCF22____HasIsExtension_PropertyInfo,
	SourceCodeInfo_tE9110A5CAA36A130E03CC9F3DE6E0EF7E7BF412B_CustomAttributesCacheGenerator_SourceCodeInfo_tE9110A5CAA36A130E03CC9F3DE6E0EF7E7BF412B____Parser_PropertyInfo,
	SourceCodeInfo_tE9110A5CAA36A130E03CC9F3DE6E0EF7E7BF412B_CustomAttributesCacheGenerator_SourceCodeInfo_tE9110A5CAA36A130E03CC9F3DE6E0EF7E7BF412B____Descriptor_PropertyInfo,
	SourceCodeInfo_tE9110A5CAA36A130E03CC9F3DE6E0EF7E7BF412B_CustomAttributesCacheGenerator_SourceCodeInfo_tE9110A5CAA36A130E03CC9F3DE6E0EF7E7BF412B____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo,
	SourceCodeInfo_tE9110A5CAA36A130E03CC9F3DE6E0EF7E7BF412B_CustomAttributesCacheGenerator_SourceCodeInfo_tE9110A5CAA36A130E03CC9F3DE6E0EF7E7BF412B____Location_PropertyInfo,
	Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_CustomAttributesCacheGenerator_Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4____Parser_PropertyInfo,
	Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_CustomAttributesCacheGenerator_Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4____Descriptor_PropertyInfo,
	Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_CustomAttributesCacheGenerator_Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo,
	Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_CustomAttributesCacheGenerator_Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4____Path_PropertyInfo,
	Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_CustomAttributesCacheGenerator_Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4____Span_PropertyInfo,
	Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_CustomAttributesCacheGenerator_Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4____LeadingComments_PropertyInfo,
	Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_CustomAttributesCacheGenerator_Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4____HasLeadingComments_PropertyInfo,
	Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_CustomAttributesCacheGenerator_Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4____TrailingComments_PropertyInfo,
	Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_CustomAttributesCacheGenerator_Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4____HasTrailingComments_PropertyInfo,
	Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4_CustomAttributesCacheGenerator_Location_tE9C42132174081A65478A0275D3D9F0E87B87AA4____LeadingDetachedComments_PropertyInfo,
	GeneratedCodeInfo_t88B2B7880C3E0445510519523DE05618C3B1A315_CustomAttributesCacheGenerator_GeneratedCodeInfo_t88B2B7880C3E0445510519523DE05618C3B1A315____Parser_PropertyInfo,
	GeneratedCodeInfo_t88B2B7880C3E0445510519523DE05618C3B1A315_CustomAttributesCacheGenerator_GeneratedCodeInfo_t88B2B7880C3E0445510519523DE05618C3B1A315____Descriptor_PropertyInfo,
	GeneratedCodeInfo_t88B2B7880C3E0445510519523DE05618C3B1A315_CustomAttributesCacheGenerator_GeneratedCodeInfo_t88B2B7880C3E0445510519523DE05618C3B1A315____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo,
	Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_CustomAttributesCacheGenerator_Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405____Parser_PropertyInfo,
	Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_CustomAttributesCacheGenerator_Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405____Descriptor_PropertyInfo,
	Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_CustomAttributesCacheGenerator_Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo,
	Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_CustomAttributesCacheGenerator_Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405____SourceFile_PropertyInfo,
	Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_CustomAttributesCacheGenerator_Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405____HasSourceFile_PropertyInfo,
	Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_CustomAttributesCacheGenerator_Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405____Begin_PropertyInfo,
	Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_CustomAttributesCacheGenerator_Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405____HasBegin_PropertyInfo,
	Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_CustomAttributesCacheGenerator_Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405____End_PropertyInfo,
	Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405_CustomAttributesCacheGenerator_Annotation_t8A5A2E8B01C5D6C98CC7B03BC7E7A49F70F6D405____HasEnd_PropertyInfo,
	Google_Protobuf_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void TargetFrameworkAttribute_set_FrameworkDisplayName_mB89F1A63CB77A414AF46D5695B37CD520EAB52AB_inline (TargetFrameworkAttribute_t9FA66D5D5B274F0E1A4FE20162AA70F62BFFB517 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set__frameworkDisplayName_1(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}
