﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88;
// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// AOT.MonoPInvokeCallbackAttribute
struct MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;

IL2CPP_EXTERN_C const RuntimeType* DeletionCallback_tE04CF8846846D69906AF6AAF6F20A8F5B2BE7A7D_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* NativeGlStatusFunction_t3CE1DA38CF9E8424C0EA9C377E4F5D962AE046E1_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* NativePacketCallback_tDF34C4B13782FDF5F405BED21231631FC06972CC_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* PluginCallback_t42C8799C1EFBFDEB1558218D6A30233BB8A54033_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CResetScreenU3Ed__16_t35B295390641252273E3846EB92DDF8E43F46C89_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CRunGraphU3Ed__24_t03B7713933548F64AF7A96F27F0C6320346AA3EE_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CRunGraphU3Ed__7_t7B57CF589220D3218A8F2520D9B40ECB9091BB18_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartU3Ed__2_t729CF6DB68E379AB62FB4461D44DBAD915703537_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CWaitForCameraU3Ed__26_t763BCA36C29246D23CF919ADDF985E25C36DBE0D_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CWaitForGraphU3Ed__25_t82883FECA873B5E87E658A3C6F302DF8FCBD7F0E_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CWaitForTextureFrameU3Ed__14_t898A8E55E0A85C0800EAD3A44AD520F7ACE2E57C_0_0_0_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// AOT.MonoPInvokeCallbackAttribute
struct MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.StateMachineAttribute
struct StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type System.Runtime.CompilerServices.StateMachineAttribute::<StateMachineType>k__BackingField
	Type_t * ___U3CStateMachineTypeU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CStateMachineTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3, ___U3CStateMachineTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CStateMachineTypeU3Ek__BackingField_0() const { return ___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CStateMachineTypeU3Ek__BackingField_0() { return &___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline void set_U3CStateMachineTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CStateMachineTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CStateMachineTypeU3Ek__BackingField_0), (void*)value);
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Reflection.BindingFlags
struct BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830  : public StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3
{
public:

public:
};


// System.RuntimeTypeHandle
struct RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};


// System.Type
struct Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void UnityEngine.SerializeField::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3 (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.IteratorStateMachineAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481 (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * __this, Type_t * ___stateMachineType0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerHiddenAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3 (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * __this, const RuntimeMethod* method);
// System.Void AOT.MonoPInvokeCallbackAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoPInvokeCallbackAttribute__ctor_mA23FC56D16F76D90C3F76B1C41D0765BB21FE344 (MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E * __this, Type_t * ___type0, const RuntimeMethod* method);
static void AssemblyU2DCSharp_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[0];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[1];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[2];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 2LL, NULL);
	}
}
static void FaceDetectionGraph_t0D3C755E2B205A867E229A8E1FAF38E2291F7AE8_CustomAttributesCacheGenerator_modelType(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FaceMeshGraph_t13A4B604EA07A10D6A00688337D34C9146A22CBB_CustomAttributesCacheGenerator_numFaces(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void HandTrackingAnnotationController_tE0866337E29E64105C8ADB323E961B21C401A08C_CustomAttributesCacheGenerator_handLandmarkListsPrefab(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void HandTrackingAnnotationController_tE0866337E29E64105C8ADB323E961B21C401A08C_CustomAttributesCacheGenerator_palmRectsPrefab(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void HandTrackingAnnotationController_tE0866337E29E64105C8ADB323E961B21C401A08C_CustomAttributesCacheGenerator_palmDetectionsPrefab(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void HelloWorld_t68EB797C01F276EDA0B319244AAF66A7C26000AB_CustomAttributesCacheGenerator_HelloWorld_RunGraph_mEC050C49115FEC8EDB2DA7CEC4CDC6786D220FE9(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CRunGraphU3Ed__7_t7B57CF589220D3218A8F2520D9B40ECB9091BB18_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CRunGraphU3Ed__7_t7B57CF589220D3218A8F2520D9B40ECB9091BB18_0_0_0_var), NULL);
	}
}
static void U3CRunGraphU3Ed__7_t7B57CF589220D3218A8F2520D9B40ECB9091BB18_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CRunGraphU3Ed__7_t7B57CF589220D3218A8F2520D9B40ECB9091BB18_CustomAttributesCacheGenerator_U3CRunGraphU3Ed__7__ctor_m3DFA86D230949C52467903E06DD7147BEC7CD009(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRunGraphU3Ed__7_t7B57CF589220D3218A8F2520D9B40ECB9091BB18_CustomAttributesCacheGenerator_U3CRunGraphU3Ed__7_System_IDisposable_Dispose_m274C2E9415C732F3D596F53C22C7F0FA82F94094(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRunGraphU3Ed__7_t7B57CF589220D3218A8F2520D9B40ECB9091BB18_CustomAttributesCacheGenerator_U3CRunGraphU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC95A3572B36E5FA9CAB03A0B7412AA63218F2CB2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRunGraphU3Ed__7_t7B57CF589220D3218A8F2520D9B40ECB9091BB18_CustomAttributesCacheGenerator_U3CRunGraphU3Ed__7_System_Collections_IEnumerator_Reset_m301ADA817F03B03DA7898E44710ACCA8220EC79F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRunGraphU3Ed__7_t7B57CF589220D3218A8F2520D9B40ECB9091BB18_CustomAttributesCacheGenerator_U3CRunGraphU3Ed__7_System_Collections_IEnumerator_get_Current_mFCF11A73BC49AE0753C24A27BABB665A8E2171D3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void HolisticAnnotationController_tCAC4A481589B4751B976080BD281D093C923F26F_CustomAttributesCacheGenerator_poseLandmarksPrefab(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void HolisticAnnotationController_tCAC4A481589B4751B976080BD281D093C923F26F_CustomAttributesCacheGenerator_poseRoiPrefab(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void HolisticAnnotationController_tCAC4A481589B4751B976080BD281D093C923F26F_CustomAttributesCacheGenerator_poseDetectionPrefab(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void HolisticAnnotationController_tCAC4A481589B4751B976080BD281D093C923F26F_CustomAttributesCacheGenerator_faceLandmarksPrefab(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void HolisticAnnotationController_tCAC4A481589B4751B976080BD281D093C923F26F_CustomAttributesCacheGenerator_irisLandmarksPrefab(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void HolisticAnnotationController_tCAC4A481589B4751B976080BD281D093C923F26F_CustomAttributesCacheGenerator_handLandmarksPrefab(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void HolisticGraph_t67702C84B9CBFE2EC26C42FE5939C504B8A95D88_CustomAttributesCacheGenerator_detectIris(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void HolisticGraph_t67702C84B9CBFE2EC26C42FE5939C504B8A95D88_CustomAttributesCacheGenerator_modelComplexity(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void HolisticGraph_t67702C84B9CBFE2EC26C42FE5939C504B8A95D88_CustomAttributesCacheGenerator_smoothLandmarks(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void InstantMotionTrackingGraph_t7C0F8ABB61110905748A422BCA4BBB928D1F247B_CustomAttributesCacheGenerator_texture3dAsset(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void InstantMotionTrackingGraph_t7C0F8ABB61110905748A422BCA4BBB928D1F247B_CustomAttributesCacheGenerator_InstantMotionTrackingGraph_MatrixCallback_mC846E6B8E080F5495EE9AB511DA6C35EFBC65C6D(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NativePacketCallback_tDF34C4B13782FDF5F405BED21231631FC06972CC_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E * tmp = (MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E *)cache->attributes[0];
		MonoPInvokeCallbackAttribute__ctor_mA23FC56D16F76D90C3F76B1C41D0765BB21FE344(tmp, il2cpp_codegen_type_get_object(NativePacketCallback_tDF34C4B13782FDF5F405BED21231631FC06972CC_0_0_0_var), NULL);
	}
}
static void IrisAnnotationController_t85B1448744D43BBC3450F3CC0A156758C8CCCCB1_CustomAttributesCacheGenerator_nodePrefab(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void IrisAnnotationController_t85B1448744D43BBC3450F3CC0A156758C8CCCCB1_CustomAttributesCacheGenerator_circlePrefab(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObjectDetection3dGraph_tB739863BCD00131B2C956EB72D2E34F8430C7AEB_CustomAttributesCacheGenerator_category(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObjectDetection3dGraph_tB739863BCD00131B2C956EB72D2E34F8430C7AEB_CustomAttributesCacheGenerator_maxNumObjects(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObjectDetection3dGraph_tB739863BCD00131B2C956EB72D2E34F8430C7AEB_CustomAttributesCacheGenerator_objTextureAsset(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObjectDetection3dGraph_tB739863BCD00131B2C956EB72D2E34F8430C7AEB_CustomAttributesCacheGenerator_boxTextureAsset(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void OfficialDemoGraph_tA7BB6776A466F240A4B3508BFC6272F0DC810CD1_CustomAttributesCacheGenerator_OfficialDemoGraph_BuildDestination_m8A2E396648876205DF379CDB3B2C198AB6A5E70D(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NativeGlStatusFunction_t3CE1DA38CF9E8424C0EA9C377E4F5D962AE046E1_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E * tmp = (MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E *)cache->attributes[0];
		MonoPInvokeCallbackAttribute__ctor_mA23FC56D16F76D90C3F76B1C41D0765BB21FE344(tmp, il2cpp_codegen_type_get_object(NativeGlStatusFunction_t3CE1DA38CF9E8424C0EA9C377E4F5D962AE046E1_0_0_0_var), NULL);
	}
}
static void OfficialDemoGraph_tA7BB6776A466F240A4B3508BFC6272F0DC810CD1_CustomAttributesCacheGenerator_OfficialDemoGraph_OnReleaseDestinationTexture_mC9040C05CB2B2BC7C3D8126BCCFD6FF9B24B199D(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DeletionCallback_tE04CF8846846D69906AF6AAF6F20A8F5B2BE7A7D_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E * tmp = (MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E *)cache->attributes[0];
		MonoPInvokeCallbackAttribute__ctor_mA23FC56D16F76D90C3F76B1C41D0765BB21FE344(tmp, il2cpp_codegen_type_get_object(DeletionCallback_tE04CF8846846D69906AF6AAF6F20A8F5B2BE7A7D_0_0_0_var), NULL);
	}
}
static void U3CU3Ec_t7FF69882A7772859195163F881857D6876959D11_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PoseTrackingAnnotationController_t0AADF9018DB28BFE9EB200171978E38A261114DF_CustomAttributesCacheGenerator_poseLandmarkListPrefab(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PoseTrackingAnnotationController_t0AADF9018DB28BFE9EB200171978E38A261114DF_CustomAttributesCacheGenerator_poseDetectionPrefab(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PoseTrackingGraph_t35730B842AF55B22930CA6F3349BAD0C01FB626A_CustomAttributesCacheGenerator_modelComplexity(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PoseTrackingGraph_t35730B842AF55B22930CA6F3349BAD0C01FB626A_CustomAttributesCacheGenerator_smoothLandmarks(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DemoGraph_tA43C50A26D874E0C63FED4DA74FA73D2FD0CBF12_CustomAttributesCacheGenerator_gpuConfig(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DemoGraph_tA43C50A26D874E0C63FED4DA74FA73D2FD0CBF12_CustomAttributesCacheGenerator_cpuConfig(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DemoGraph_tA43C50A26D874E0C63FED4DA74FA73D2FD0CBF12_CustomAttributesCacheGenerator_androidConfig(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DemoGraph_tA43C50A26D874E0C63FED4DA74FA73D2FD0CBF12_CustomAttributesCacheGenerator_DemoGraph_PushInputInGlContext_m2000278CAE8C8F4281A45E55684BD7498AEE66E7(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NativeGlStatusFunction_t3CE1DA38CF9E8424C0EA9C377E4F5D962AE046E1_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E * tmp = (MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E *)cache->attributes[0];
		MonoPInvokeCallbackAttribute__ctor_mA23FC56D16F76D90C3F76B1C41D0765BB21FE344(tmp, il2cpp_codegen_type_get_object(NativeGlStatusFunction_t3CE1DA38CF9E8424C0EA9C377E4F5D962AE046E1_0_0_0_var), NULL);
	}
}
static void GraphSelectorController_t19039917C2C6BC6200DDC7803F9505BEC8F9F261_CustomAttributesCacheGenerator_faceDetectionGraph(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GraphSelectorController_t19039917C2C6BC6200DDC7803F9505BEC8F9F261_CustomAttributesCacheGenerator_faceMeshGraph(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GraphSelectorController_t19039917C2C6BC6200DDC7803F9505BEC8F9F261_CustomAttributesCacheGenerator_irisTrackingGraph(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GraphSelectorController_t19039917C2C6BC6200DDC7803F9505BEC8F9F261_CustomAttributesCacheGenerator_handTrackingGraph(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GraphSelectorController_t19039917C2C6BC6200DDC7803F9505BEC8F9F261_CustomAttributesCacheGenerator_poseTrackingGraph(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GraphSelectorController_t19039917C2C6BC6200DDC7803F9505BEC8F9F261_CustomAttributesCacheGenerator_holisticGraph(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GraphSelectorController_t19039917C2C6BC6200DDC7803F9505BEC8F9F261_CustomAttributesCacheGenerator_hairSegmentationGraph(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GraphSelectorController_t19039917C2C6BC6200DDC7803F9505BEC8F9F261_CustomAttributesCacheGenerator_objectDetectionGraph(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GraphSelectorController_t19039917C2C6BC6200DDC7803F9505BEC8F9F261_CustomAttributesCacheGenerator_objectDetection3dGraph(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GraphSelectorController_t19039917C2C6BC6200DDC7803F9505BEC8F9F261_CustomAttributesCacheGenerator_boxTrackingGraph(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GraphSelectorController_t19039917C2C6BC6200DDC7803F9505BEC8F9F261_CustomAttributesCacheGenerator_instantMotionTrackingGraph(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GraphSelectorController_t19039917C2C6BC6200DDC7803F9505BEC8F9F261_CustomAttributesCacheGenerator_officialDemoGraph(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass14_0_tA121084BE7374D716AC14CB0D485C3F2ECF722B6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_tC889CF2180986A38848DD6867ABF207343D66E2F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TextureFrame_t2D3466EF091AAB30B54F10071DA9D67133200C22_CustomAttributesCacheGenerator_U3CwidthU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TextureFrame_t2D3466EF091AAB30B54F10071DA9D67133200C22_CustomAttributesCacheGenerator_U3CheightU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TextureFrame_t2D3466EF091AAB30B54F10071DA9D67133200C22_CustomAttributesCacheGenerator_TextureFrame_get_width_mB17FE2BFBE4D72D820BE811F28970E4DF2152286(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TextureFrame_t2D3466EF091AAB30B54F10071DA9D67133200C22_CustomAttributesCacheGenerator_TextureFrame_set_width_m8341215AAEEE53D7BF045E837978BB36DC498509(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TextureFrame_t2D3466EF091AAB30B54F10071DA9D67133200C22_CustomAttributesCacheGenerator_TextureFrame_get_height_mE5C87876C38DEFA9AC9E5CBC26F1FEF36F4D3C01(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TextureFrame_t2D3466EF091AAB30B54F10071DA9D67133200C22_CustomAttributesCacheGenerator_TextureFrame_set_height_m2B6DC0460751E6E07DDA94C4469DA7EFCA6559F3(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TextureFramePool_tCDC1F096499E2ECCCC73D2D8AC28B5430D061F46_CustomAttributesCacheGenerator_poolSize(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TextureFramePool_tCDC1F096499E2ECCCC73D2D8AC28B5430D061F46_CustomAttributesCacheGenerator_TextureFramePool_OnTextureFrameRelease_m3F46346F6E43543DD9BE828528E788F0066D9B6D(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DeletionCallback_tE04CF8846846D69906AF6AAF6F20A8F5B2BE7A7D_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E * tmp = (MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E *)cache->attributes[0];
		MonoPInvokeCallbackAttribute__ctor_mA23FC56D16F76D90C3F76B1C41D0765BB21FE344(tmp, il2cpp_codegen_type_get_object(DeletionCallback_tE04CF8846846D69906AF6AAF6F20A8F5B2BE7A7D_0_0_0_var), NULL);
	}
}
static void TextureFramePool_tCDC1F096499E2ECCCC73D2D8AC28B5430D061F46_CustomAttributesCacheGenerator_TextureFramePool_WaitForTextureFrame_m529EA3FF190A1A49B5FBAA2C223A9FAABA01CA64(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CWaitForTextureFrameU3Ed__14_t898A8E55E0A85C0800EAD3A44AD520F7ACE2E57C_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CWaitForTextureFrameU3Ed__14_t898A8E55E0A85C0800EAD3A44AD520F7ACE2E57C_0_0_0_var), NULL);
	}
}
static void TextureFramePool_tCDC1F096499E2ECCCC73D2D8AC28B5430D061F46_CustomAttributesCacheGenerator_TextureFramePool_U3CWaitForTextureFrameU3Eb__14_0_m67C6BA48945B02E3183AB2EF7B18D3062D1A388A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TextureFrameRequest_t5ABF55F342DD484044C1BBFF3FF2388D9AE30D71_CustomAttributesCacheGenerator_U3CtextureFrameU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TextureFrameRequest_t5ABF55F342DD484044C1BBFF3FF2388D9AE30D71_CustomAttributesCacheGenerator_TextureFrameRequest_get_textureFrame_m16903FC7DAE1826ADC2FA455E6FC22164C9A3B6A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TextureFrameRequest_t5ABF55F342DD484044C1BBFF3FF2388D9AE30D71_CustomAttributesCacheGenerator_TextureFrameRequest_set_textureFrame_m9144A4180722C034BC62972B4782BA63D4932740(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass7_0_t0211EF869AB9D4E3C4DD20F55EA8C217251E199D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWaitForTextureFrameU3Ed__14_t898A8E55E0A85C0800EAD3A44AD520F7ACE2E57C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWaitForTextureFrameU3Ed__14_t898A8E55E0A85C0800EAD3A44AD520F7ACE2E57C_CustomAttributesCacheGenerator_U3CWaitForTextureFrameU3Ed__14__ctor_mF771738CC4E23FBA80292BA09F52FD4F8D6B9D28(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForTextureFrameU3Ed__14_t898A8E55E0A85C0800EAD3A44AD520F7ACE2E57C_CustomAttributesCacheGenerator_U3CWaitForTextureFrameU3Ed__14_System_IDisposable_Dispose_m0856FDFBE77BC309EFB4E259C2C1F5C449608973(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForTextureFrameU3Ed__14_t898A8E55E0A85C0800EAD3A44AD520F7ACE2E57C_CustomAttributesCacheGenerator_U3CWaitForTextureFrameU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m53532F7067493675A7491BEBD5F356D352EFD9D6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForTextureFrameU3Ed__14_t898A8E55E0A85C0800EAD3A44AD520F7ACE2E57C_CustomAttributesCacheGenerator_U3CWaitForTextureFrameU3Ed__14_System_Collections_IEnumerator_Reset_m46689A7498CC85E7622FE1AD0C2735D6CEF2AC20(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForTextureFrameU3Ed__14_t898A8E55E0A85C0800EAD3A44AD520F7ACE2E57C_CustomAttributesCacheGenerator_U3CWaitForTextureFrameU3Ed__14_System_Collections_IEnumerator_get_Current_m1D796BDA08361D499F44C847EF0197DE55C3D817(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void SceneDirectors_t2A0127CC9EBDE2D091AACA821A4BD656827DC83B_CustomAttributesCacheGenerator_useGPU(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SceneDirectors_t2A0127CC9EBDE2D091AACA821A4BD656827DC83B_CustomAttributesCacheGenerator_SceneDirectors_GetCurrentContext_mCCE71B73479FC8A1F8D65066ED794FE54AF5401C(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PluginCallback_t42C8799C1EFBFDEB1558218D6A30233BB8A54033_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E * tmp = (MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E *)cache->attributes[0];
		MonoPInvokeCallbackAttribute__ctor_mA23FC56D16F76D90C3F76B1C41D0765BB21FE344(tmp, il2cpp_codegen_type_get_object(PluginCallback_t42C8799C1EFBFDEB1558218D6A30233BB8A54033_0_0_0_var), NULL);
	}
}
static void SceneDirectors_t2A0127CC9EBDE2D091AACA821A4BD656827DC83B_CustomAttributesCacheGenerator_SceneDirectors_RunGraph_m538FB86082B4D6CC9D2871496FD8F79ECF0E7DFC(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CRunGraphU3Ed__24_t03B7713933548F64AF7A96F27F0C6320346AA3EE_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CRunGraphU3Ed__24_t03B7713933548F64AF7A96F27F0C6320346AA3EE_0_0_0_var), NULL);
	}
}
static void SceneDirectors_t2A0127CC9EBDE2D091AACA821A4BD656827DC83B_CustomAttributesCacheGenerator_SceneDirectors_WaitForGraph_m336B2A27DAC594C63F79DBC26A63439A4E1908A2(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CWaitForGraphU3Ed__25_t82883FECA873B5E87E658A3C6F302DF8FCBD7F0E_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CWaitForGraphU3Ed__25_t82883FECA873B5E87E658A3C6F302DF8FCBD7F0E_0_0_0_var), NULL);
	}
}
static void SceneDirectors_t2A0127CC9EBDE2D091AACA821A4BD656827DC83B_CustomAttributesCacheGenerator_SceneDirectors_WaitForCamera_m275D49CD83631A2DCB68FB4B48C8E7F6CB350E44(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CWaitForCameraU3Ed__26_t763BCA36C29246D23CF919ADDF985E25C36DBE0D_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CWaitForCameraU3Ed__26_t763BCA36C29246D23CF919ADDF985E25C36DBE0D_0_0_0_var), NULL);
	}
}
static void U3CRunGraphU3Ed__24_t03B7713933548F64AF7A96F27F0C6320346AA3EE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CRunGraphU3Ed__24_t03B7713933548F64AF7A96F27F0C6320346AA3EE_CustomAttributesCacheGenerator_U3CRunGraphU3Ed__24__ctor_mD8E548AF383C56A2B2A8AEF57581758506147835(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRunGraphU3Ed__24_t03B7713933548F64AF7A96F27F0C6320346AA3EE_CustomAttributesCacheGenerator_U3CRunGraphU3Ed__24_System_IDisposable_Dispose_m1A65857C24F53A20E78ACA865CEF9BC40FA2FCF8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRunGraphU3Ed__24_t03B7713933548F64AF7A96F27F0C6320346AA3EE_CustomAttributesCacheGenerator_U3CRunGraphU3Ed__24_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD2AD458E8533A88202382ACC60A91B838B2DDBAB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRunGraphU3Ed__24_t03B7713933548F64AF7A96F27F0C6320346AA3EE_CustomAttributesCacheGenerator_U3CRunGraphU3Ed__24_System_Collections_IEnumerator_Reset_mF598EC361ACE423F3AAA808AE02A3FFD0799AB53(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRunGraphU3Ed__24_t03B7713933548F64AF7A96F27F0C6320346AA3EE_CustomAttributesCacheGenerator_U3CRunGraphU3Ed__24_System_Collections_IEnumerator_get_Current_m0F69806467AF047BC3C6FB4F9EBD727FD3E1E9A8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass25_0_tDFEFA5C3020E1F974B850D14647440A4C208784A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWaitForGraphU3Ed__25_t82883FECA873B5E87E658A3C6F302DF8FCBD7F0E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWaitForGraphU3Ed__25_t82883FECA873B5E87E658A3C6F302DF8FCBD7F0E_CustomAttributesCacheGenerator_U3CWaitForGraphU3Ed__25__ctor_m5CA156DE5E91E041B8EE6C3B206858ED6B2FDE62(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForGraphU3Ed__25_t82883FECA873B5E87E658A3C6F302DF8FCBD7F0E_CustomAttributesCacheGenerator_U3CWaitForGraphU3Ed__25_System_IDisposable_Dispose_m4AA842376297CE56575BA85E691DD6D60B254E78(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForGraphU3Ed__25_t82883FECA873B5E87E658A3C6F302DF8FCBD7F0E_CustomAttributesCacheGenerator_U3CWaitForGraphU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF0E9DF6BE3D9F085FA4BB436AAC9E15332D245EC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForGraphU3Ed__25_t82883FECA873B5E87E658A3C6F302DF8FCBD7F0E_CustomAttributesCacheGenerator_U3CWaitForGraphU3Ed__25_System_Collections_IEnumerator_Reset_m04AE7F2FE9D453891514E4C6FED67FFD1876A5A5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForGraphU3Ed__25_t82883FECA873B5E87E658A3C6F302DF8FCBD7F0E_CustomAttributesCacheGenerator_U3CWaitForGraphU3Ed__25_System_Collections_IEnumerator_get_Current_m49604FA593536760B6C891E6A0A1D2CBAEA241E9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass26_0_tD4710485FFB5963A66F255A14B122D732812591B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWaitForCameraU3Ed__26_t763BCA36C29246D23CF919ADDF985E25C36DBE0D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWaitForCameraU3Ed__26_t763BCA36C29246D23CF919ADDF985E25C36DBE0D_CustomAttributesCacheGenerator_U3CWaitForCameraU3Ed__26__ctor_mEF0BA9234B5326F8D1DA18B5BC3D54C2EDB621A8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForCameraU3Ed__26_t763BCA36C29246D23CF919ADDF985E25C36DBE0D_CustomAttributesCacheGenerator_U3CWaitForCameraU3Ed__26_System_IDisposable_Dispose_m798B59916A7F7B04C2473B999C112FAA56D2E800(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForCameraU3Ed__26_t763BCA36C29246D23CF919ADDF985E25C36DBE0D_CustomAttributesCacheGenerator_U3CWaitForCameraU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF9C3E046BC69EEED73E7E43B6788A781A09AFC3F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForCameraU3Ed__26_t763BCA36C29246D23CF919ADDF985E25C36DBE0D_CustomAttributesCacheGenerator_U3CWaitForCameraU3Ed__26_System_Collections_IEnumerator_Reset_m3CB2E113834F80E3427C87910C5CA706FA5DDA4B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForCameraU3Ed__26_t763BCA36C29246D23CF919ADDF985E25C36DBE0D_CustomAttributesCacheGenerator_U3CWaitForCameraU3Ed__26_System_Collections_IEnumerator_get_Current_m6590C064728CBBE508A79AF2027ABFBBF28B93B2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void WebCamDeviceSelectorController_t66725BA2909CE33463B8E826C910C3A259F17593_CustomAttributesCacheGenerator_WebCamDeviceSelectorController_Start_m519A2B7FD0AA40F570C8EC6EF6A0AEEB057560F2(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartU3Ed__2_t729CF6DB68E379AB62FB4461D44DBAD915703537_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CStartU3Ed__2_t729CF6DB68E379AB62FB4461D44DBAD915703537_0_0_0_var), NULL);
	}
}
static void U3CU3Ec__DisplayClass2_0_t399811E92AE027B9E7CDE4BA0918B04E7AD28B77_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__2_t729CF6DB68E379AB62FB4461D44DBAD915703537_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__2_t729CF6DB68E379AB62FB4461D44DBAD915703537_CustomAttributesCacheGenerator_U3CStartU3Ed__2__ctor_m906CABD8C45330E13A122514A7C09E2EC5FC0039(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__2_t729CF6DB68E379AB62FB4461D44DBAD915703537_CustomAttributesCacheGenerator_U3CStartU3Ed__2_System_IDisposable_Dispose_m0139BD6575B1E2FA511461685412C8259DCA96E1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__2_t729CF6DB68E379AB62FB4461D44DBAD915703537_CustomAttributesCacheGenerator_U3CStartU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC9597C9A7823BD016D2A3C49B00789A8CB933A45(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__2_t729CF6DB68E379AB62FB4461D44DBAD915703537_CustomAttributesCacheGenerator_U3CStartU3Ed__2_System_Collections_IEnumerator_Reset_m3B48B6C9ABBB439192150CDDCD2EFF1DE7DFC150(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__2_t729CF6DB68E379AB62FB4461D44DBAD915703537_CustomAttributesCacheGenerator_U3CStartU3Ed__2_System_Collections_IEnumerator_get_Current_mD2E5B1382AFEAA92756EEC8CEF1A480E6EE3F82D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec_tCD3AAFC81BF36C878C4E6AC6541EB56CA6820B29_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WebCamScreenController_t13EEFE51EC0F7BEA25B5EFAE7AF0733A980F14F7_CustomAttributesCacheGenerator_Width(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void WebCamScreenController_t13EEFE51EC0F7BEA25B5EFAE7AF0733A980F14F7_CustomAttributesCacheGenerator_Height(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void WebCamScreenController_t13EEFE51EC0F7BEA25B5EFAE7AF0733A980F14F7_CustomAttributesCacheGenerator_FPS(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void WebCamScreenController_t13EEFE51EC0F7BEA25B5EFAE7AF0733A980F14F7_CustomAttributesCacheGenerator_FocalLengthPx(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void WebCamScreenController_t13EEFE51EC0F7BEA25B5EFAE7AF0733A980F14F7_CustomAttributesCacheGenerator_WebCamScreenController_ResetScreen_mAE3830893EA75A8259087B49F90DE6FC58162FE6(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CResetScreenU3Ed__16_t35B295390641252273E3846EB92DDF8E43F46C89_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CResetScreenU3Ed__16_t35B295390641252273E3846EB92DDF8E43F46C89_0_0_0_var), NULL);
	}
}
static void WebCamScreenController_t13EEFE51EC0F7BEA25B5EFAE7AF0733A980F14F7_CustomAttributesCacheGenerator_WebCamScreenController_U3CRequestNextFrameU3Eb__25_0_m8AB50133A528C658E544CB7ACE354C0728F7971D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass16_0_t649434D43F6062317ADA829579D8B64A79C9244A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CResetScreenU3Ed__16_t35B295390641252273E3846EB92DDF8E43F46C89_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CResetScreenU3Ed__16_t35B295390641252273E3846EB92DDF8E43F46C89_CustomAttributesCacheGenerator_U3CResetScreenU3Ed__16__ctor_mDAD1FEE5686D6597DCB7579537D2EB62649F79D9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CResetScreenU3Ed__16_t35B295390641252273E3846EB92DDF8E43F46C89_CustomAttributesCacheGenerator_U3CResetScreenU3Ed__16_System_IDisposable_Dispose_m7C7966A9BC91BB8B9411C59299BE4A64AD4F3F73(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CResetScreenU3Ed__16_t35B295390641252273E3846EB92DDF8E43F46C89_CustomAttributesCacheGenerator_U3CResetScreenU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4832D1E17D71F3ECAF696FCCCF39237400C816C3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CResetScreenU3Ed__16_t35B295390641252273E3846EB92DDF8E43F46C89_CustomAttributesCacheGenerator_U3CResetScreenU3Ed__16_System_Collections_IEnumerator_Reset_m60226C6644A0E904B746230543DABF1C5BCD4336(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CResetScreenU3Ed__16_t35B295390641252273E3846EB92DDF8E43F46C89_CustomAttributesCacheGenerator_U3CResetScreenU3Ed__16_System_Collections_IEnumerator_get_Current_mB8C7F8DC2FAE2DD8A27D76DF2D91AB5F7B2AA38A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void PoseEstimation0fBody_t93903E1BE54B7BA1C1BC69FC0635B18E845AFA5C_CustomAttributesCacheGenerator_holisticGraph(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass3_0_t2B4E1472E24B9841A91108FC90F16822E15D6C65_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_tBF5F82A94C0478F85AC1B793ED02E615E065452C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FaceMeshAnnotationController_t22BF6D1E927FFFC910D1FDCBF24375024B23DEBF_CustomAttributesCacheGenerator_faceLandmarkListsPrefab(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FaceMeshAnnotationController_t22BF6D1E927FFFC910D1FDCBF24375024B23DEBF_CustomAttributesCacheGenerator_faceRectsPrefab(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FaceMeshAnnotationController_t22BF6D1E927FFFC910D1FDCBF24375024B23DEBF_CustomAttributesCacheGenerator_faceDetectionsPrefab(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void IrisTrackingAnnotationController_tAB4C786DC204F29220528742DC56383226046519_CustomAttributesCacheGenerator_irisPrefab(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void IrisTrackingAnnotationController_tAB4C786DC204F29220528742DC56383226046519_CustomAttributesCacheGenerator_faceLandmarkListPrefab(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void IrisTrackingAnnotationController_tAB4C786DC204F29220528742DC56383226046519_CustomAttributesCacheGenerator_faceRectPrefab(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void IrisTrackingAnnotationController_tAB4C786DC204F29220528742DC56383226046519_CustomAttributesCacheGenerator_faceDetectionsPrefab(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void U3CPrivateImplementationDetailsU3E_t6BC7664D9CD46304D39A7D175BB8FFBE0B9F4528_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[133] = 
{
	U3CRunGraphU3Ed__7_t7B57CF589220D3218A8F2520D9B40ECB9091BB18_CustomAttributesCacheGenerator,
	U3CU3Ec_t7FF69882A7772859195163F881857D6876959D11_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass14_0_tA121084BE7374D716AC14CB0D485C3F2ECF722B6_CustomAttributesCacheGenerator,
	U3CU3Ec_tC889CF2180986A38848DD6867ABF207343D66E2F_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass7_0_t0211EF869AB9D4E3C4DD20F55EA8C217251E199D_CustomAttributesCacheGenerator,
	U3CWaitForTextureFrameU3Ed__14_t898A8E55E0A85C0800EAD3A44AD520F7ACE2E57C_CustomAttributesCacheGenerator,
	U3CRunGraphU3Ed__24_t03B7713933548F64AF7A96F27F0C6320346AA3EE_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass25_0_tDFEFA5C3020E1F974B850D14647440A4C208784A_CustomAttributesCacheGenerator,
	U3CWaitForGraphU3Ed__25_t82883FECA873B5E87E658A3C6F302DF8FCBD7F0E_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass26_0_tD4710485FFB5963A66F255A14B122D732812591B_CustomAttributesCacheGenerator,
	U3CWaitForCameraU3Ed__26_t763BCA36C29246D23CF919ADDF985E25C36DBE0D_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass2_0_t399811E92AE027B9E7CDE4BA0918B04E7AD28B77_CustomAttributesCacheGenerator,
	U3CStartU3Ed__2_t729CF6DB68E379AB62FB4461D44DBAD915703537_CustomAttributesCacheGenerator,
	U3CU3Ec_tCD3AAFC81BF36C878C4E6AC6541EB56CA6820B29_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass16_0_t649434D43F6062317ADA829579D8B64A79C9244A_CustomAttributesCacheGenerator,
	U3CResetScreenU3Ed__16_t35B295390641252273E3846EB92DDF8E43F46C89_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass3_0_t2B4E1472E24B9841A91108FC90F16822E15D6C65_CustomAttributesCacheGenerator,
	U3CU3Ec_tBF5F82A94C0478F85AC1B793ED02E615E065452C_CustomAttributesCacheGenerator,
	U3CPrivateImplementationDetailsU3E_t6BC7664D9CD46304D39A7D175BB8FFBE0B9F4528_CustomAttributesCacheGenerator,
	FaceDetectionGraph_t0D3C755E2B205A867E229A8E1FAF38E2291F7AE8_CustomAttributesCacheGenerator_modelType,
	FaceMeshGraph_t13A4B604EA07A10D6A00688337D34C9146A22CBB_CustomAttributesCacheGenerator_numFaces,
	HandTrackingAnnotationController_tE0866337E29E64105C8ADB323E961B21C401A08C_CustomAttributesCacheGenerator_handLandmarkListsPrefab,
	HandTrackingAnnotationController_tE0866337E29E64105C8ADB323E961B21C401A08C_CustomAttributesCacheGenerator_palmRectsPrefab,
	HandTrackingAnnotationController_tE0866337E29E64105C8ADB323E961B21C401A08C_CustomAttributesCacheGenerator_palmDetectionsPrefab,
	HolisticAnnotationController_tCAC4A481589B4751B976080BD281D093C923F26F_CustomAttributesCacheGenerator_poseLandmarksPrefab,
	HolisticAnnotationController_tCAC4A481589B4751B976080BD281D093C923F26F_CustomAttributesCacheGenerator_poseRoiPrefab,
	HolisticAnnotationController_tCAC4A481589B4751B976080BD281D093C923F26F_CustomAttributesCacheGenerator_poseDetectionPrefab,
	HolisticAnnotationController_tCAC4A481589B4751B976080BD281D093C923F26F_CustomAttributesCacheGenerator_faceLandmarksPrefab,
	HolisticAnnotationController_tCAC4A481589B4751B976080BD281D093C923F26F_CustomAttributesCacheGenerator_irisLandmarksPrefab,
	HolisticAnnotationController_tCAC4A481589B4751B976080BD281D093C923F26F_CustomAttributesCacheGenerator_handLandmarksPrefab,
	HolisticGraph_t67702C84B9CBFE2EC26C42FE5939C504B8A95D88_CustomAttributesCacheGenerator_detectIris,
	HolisticGraph_t67702C84B9CBFE2EC26C42FE5939C504B8A95D88_CustomAttributesCacheGenerator_modelComplexity,
	HolisticGraph_t67702C84B9CBFE2EC26C42FE5939C504B8A95D88_CustomAttributesCacheGenerator_smoothLandmarks,
	InstantMotionTrackingGraph_t7C0F8ABB61110905748A422BCA4BBB928D1F247B_CustomAttributesCacheGenerator_texture3dAsset,
	IrisAnnotationController_t85B1448744D43BBC3450F3CC0A156758C8CCCCB1_CustomAttributesCacheGenerator_nodePrefab,
	IrisAnnotationController_t85B1448744D43BBC3450F3CC0A156758C8CCCCB1_CustomAttributesCacheGenerator_circlePrefab,
	ObjectDetection3dGraph_tB739863BCD00131B2C956EB72D2E34F8430C7AEB_CustomAttributesCacheGenerator_category,
	ObjectDetection3dGraph_tB739863BCD00131B2C956EB72D2E34F8430C7AEB_CustomAttributesCacheGenerator_maxNumObjects,
	ObjectDetection3dGraph_tB739863BCD00131B2C956EB72D2E34F8430C7AEB_CustomAttributesCacheGenerator_objTextureAsset,
	ObjectDetection3dGraph_tB739863BCD00131B2C956EB72D2E34F8430C7AEB_CustomAttributesCacheGenerator_boxTextureAsset,
	PoseTrackingAnnotationController_t0AADF9018DB28BFE9EB200171978E38A261114DF_CustomAttributesCacheGenerator_poseLandmarkListPrefab,
	PoseTrackingAnnotationController_t0AADF9018DB28BFE9EB200171978E38A261114DF_CustomAttributesCacheGenerator_poseDetectionPrefab,
	PoseTrackingGraph_t35730B842AF55B22930CA6F3349BAD0C01FB626A_CustomAttributesCacheGenerator_modelComplexity,
	PoseTrackingGraph_t35730B842AF55B22930CA6F3349BAD0C01FB626A_CustomAttributesCacheGenerator_smoothLandmarks,
	DemoGraph_tA43C50A26D874E0C63FED4DA74FA73D2FD0CBF12_CustomAttributesCacheGenerator_gpuConfig,
	DemoGraph_tA43C50A26D874E0C63FED4DA74FA73D2FD0CBF12_CustomAttributesCacheGenerator_cpuConfig,
	DemoGraph_tA43C50A26D874E0C63FED4DA74FA73D2FD0CBF12_CustomAttributesCacheGenerator_androidConfig,
	GraphSelectorController_t19039917C2C6BC6200DDC7803F9505BEC8F9F261_CustomAttributesCacheGenerator_faceDetectionGraph,
	GraphSelectorController_t19039917C2C6BC6200DDC7803F9505BEC8F9F261_CustomAttributesCacheGenerator_faceMeshGraph,
	GraphSelectorController_t19039917C2C6BC6200DDC7803F9505BEC8F9F261_CustomAttributesCacheGenerator_irisTrackingGraph,
	GraphSelectorController_t19039917C2C6BC6200DDC7803F9505BEC8F9F261_CustomAttributesCacheGenerator_handTrackingGraph,
	GraphSelectorController_t19039917C2C6BC6200DDC7803F9505BEC8F9F261_CustomAttributesCacheGenerator_poseTrackingGraph,
	GraphSelectorController_t19039917C2C6BC6200DDC7803F9505BEC8F9F261_CustomAttributesCacheGenerator_holisticGraph,
	GraphSelectorController_t19039917C2C6BC6200DDC7803F9505BEC8F9F261_CustomAttributesCacheGenerator_hairSegmentationGraph,
	GraphSelectorController_t19039917C2C6BC6200DDC7803F9505BEC8F9F261_CustomAttributesCacheGenerator_objectDetectionGraph,
	GraphSelectorController_t19039917C2C6BC6200DDC7803F9505BEC8F9F261_CustomAttributesCacheGenerator_objectDetection3dGraph,
	GraphSelectorController_t19039917C2C6BC6200DDC7803F9505BEC8F9F261_CustomAttributesCacheGenerator_boxTrackingGraph,
	GraphSelectorController_t19039917C2C6BC6200DDC7803F9505BEC8F9F261_CustomAttributesCacheGenerator_instantMotionTrackingGraph,
	GraphSelectorController_t19039917C2C6BC6200DDC7803F9505BEC8F9F261_CustomAttributesCacheGenerator_officialDemoGraph,
	TextureFrame_t2D3466EF091AAB30B54F10071DA9D67133200C22_CustomAttributesCacheGenerator_U3CwidthU3Ek__BackingField,
	TextureFrame_t2D3466EF091AAB30B54F10071DA9D67133200C22_CustomAttributesCacheGenerator_U3CheightU3Ek__BackingField,
	TextureFramePool_tCDC1F096499E2ECCCC73D2D8AC28B5430D061F46_CustomAttributesCacheGenerator_poolSize,
	TextureFrameRequest_t5ABF55F342DD484044C1BBFF3FF2388D9AE30D71_CustomAttributesCacheGenerator_U3CtextureFrameU3Ek__BackingField,
	SceneDirectors_t2A0127CC9EBDE2D091AACA821A4BD656827DC83B_CustomAttributesCacheGenerator_useGPU,
	WebCamScreenController_t13EEFE51EC0F7BEA25B5EFAE7AF0733A980F14F7_CustomAttributesCacheGenerator_Width,
	WebCamScreenController_t13EEFE51EC0F7BEA25B5EFAE7AF0733A980F14F7_CustomAttributesCacheGenerator_Height,
	WebCamScreenController_t13EEFE51EC0F7BEA25B5EFAE7AF0733A980F14F7_CustomAttributesCacheGenerator_FPS,
	WebCamScreenController_t13EEFE51EC0F7BEA25B5EFAE7AF0733A980F14F7_CustomAttributesCacheGenerator_FocalLengthPx,
	PoseEstimation0fBody_t93903E1BE54B7BA1C1BC69FC0635B18E845AFA5C_CustomAttributesCacheGenerator_holisticGraph,
	FaceMeshAnnotationController_t22BF6D1E927FFFC910D1FDCBF24375024B23DEBF_CustomAttributesCacheGenerator_faceLandmarkListsPrefab,
	FaceMeshAnnotationController_t22BF6D1E927FFFC910D1FDCBF24375024B23DEBF_CustomAttributesCacheGenerator_faceRectsPrefab,
	FaceMeshAnnotationController_t22BF6D1E927FFFC910D1FDCBF24375024B23DEBF_CustomAttributesCacheGenerator_faceDetectionsPrefab,
	IrisTrackingAnnotationController_tAB4C786DC204F29220528742DC56383226046519_CustomAttributesCacheGenerator_irisPrefab,
	IrisTrackingAnnotationController_tAB4C786DC204F29220528742DC56383226046519_CustomAttributesCacheGenerator_faceLandmarkListPrefab,
	IrisTrackingAnnotationController_tAB4C786DC204F29220528742DC56383226046519_CustomAttributesCacheGenerator_faceRectPrefab,
	IrisTrackingAnnotationController_tAB4C786DC204F29220528742DC56383226046519_CustomAttributesCacheGenerator_faceDetectionsPrefab,
	HelloWorld_t68EB797C01F276EDA0B319244AAF66A7C26000AB_CustomAttributesCacheGenerator_HelloWorld_RunGraph_mEC050C49115FEC8EDB2DA7CEC4CDC6786D220FE9,
	U3CRunGraphU3Ed__7_t7B57CF589220D3218A8F2520D9B40ECB9091BB18_CustomAttributesCacheGenerator_U3CRunGraphU3Ed__7__ctor_m3DFA86D230949C52467903E06DD7147BEC7CD009,
	U3CRunGraphU3Ed__7_t7B57CF589220D3218A8F2520D9B40ECB9091BB18_CustomAttributesCacheGenerator_U3CRunGraphU3Ed__7_System_IDisposable_Dispose_m274C2E9415C732F3D596F53C22C7F0FA82F94094,
	U3CRunGraphU3Ed__7_t7B57CF589220D3218A8F2520D9B40ECB9091BB18_CustomAttributesCacheGenerator_U3CRunGraphU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC95A3572B36E5FA9CAB03A0B7412AA63218F2CB2,
	U3CRunGraphU3Ed__7_t7B57CF589220D3218A8F2520D9B40ECB9091BB18_CustomAttributesCacheGenerator_U3CRunGraphU3Ed__7_System_Collections_IEnumerator_Reset_m301ADA817F03B03DA7898E44710ACCA8220EC79F,
	U3CRunGraphU3Ed__7_t7B57CF589220D3218A8F2520D9B40ECB9091BB18_CustomAttributesCacheGenerator_U3CRunGraphU3Ed__7_System_Collections_IEnumerator_get_Current_mFCF11A73BC49AE0753C24A27BABB665A8E2171D3,
	InstantMotionTrackingGraph_t7C0F8ABB61110905748A422BCA4BBB928D1F247B_CustomAttributesCacheGenerator_InstantMotionTrackingGraph_MatrixCallback_mC846E6B8E080F5495EE9AB511DA6C35EFBC65C6D,
	OfficialDemoGraph_tA7BB6776A466F240A4B3508BFC6272F0DC810CD1_CustomAttributesCacheGenerator_OfficialDemoGraph_BuildDestination_m8A2E396648876205DF379CDB3B2C198AB6A5E70D,
	OfficialDemoGraph_tA7BB6776A466F240A4B3508BFC6272F0DC810CD1_CustomAttributesCacheGenerator_OfficialDemoGraph_OnReleaseDestinationTexture_mC9040C05CB2B2BC7C3D8126BCCFD6FF9B24B199D,
	DemoGraph_tA43C50A26D874E0C63FED4DA74FA73D2FD0CBF12_CustomAttributesCacheGenerator_DemoGraph_PushInputInGlContext_m2000278CAE8C8F4281A45E55684BD7498AEE66E7,
	TextureFrame_t2D3466EF091AAB30B54F10071DA9D67133200C22_CustomAttributesCacheGenerator_TextureFrame_get_width_mB17FE2BFBE4D72D820BE811F28970E4DF2152286,
	TextureFrame_t2D3466EF091AAB30B54F10071DA9D67133200C22_CustomAttributesCacheGenerator_TextureFrame_set_width_m8341215AAEEE53D7BF045E837978BB36DC498509,
	TextureFrame_t2D3466EF091AAB30B54F10071DA9D67133200C22_CustomAttributesCacheGenerator_TextureFrame_get_height_mE5C87876C38DEFA9AC9E5CBC26F1FEF36F4D3C01,
	TextureFrame_t2D3466EF091AAB30B54F10071DA9D67133200C22_CustomAttributesCacheGenerator_TextureFrame_set_height_m2B6DC0460751E6E07DDA94C4469DA7EFCA6559F3,
	TextureFramePool_tCDC1F096499E2ECCCC73D2D8AC28B5430D061F46_CustomAttributesCacheGenerator_TextureFramePool_OnTextureFrameRelease_m3F46346F6E43543DD9BE828528E788F0066D9B6D,
	TextureFramePool_tCDC1F096499E2ECCCC73D2D8AC28B5430D061F46_CustomAttributesCacheGenerator_TextureFramePool_WaitForTextureFrame_m529EA3FF190A1A49B5FBAA2C223A9FAABA01CA64,
	TextureFramePool_tCDC1F096499E2ECCCC73D2D8AC28B5430D061F46_CustomAttributesCacheGenerator_TextureFramePool_U3CWaitForTextureFrameU3Eb__14_0_m67C6BA48945B02E3183AB2EF7B18D3062D1A388A,
	TextureFrameRequest_t5ABF55F342DD484044C1BBFF3FF2388D9AE30D71_CustomAttributesCacheGenerator_TextureFrameRequest_get_textureFrame_m16903FC7DAE1826ADC2FA455E6FC22164C9A3B6A,
	TextureFrameRequest_t5ABF55F342DD484044C1BBFF3FF2388D9AE30D71_CustomAttributesCacheGenerator_TextureFrameRequest_set_textureFrame_m9144A4180722C034BC62972B4782BA63D4932740,
	U3CWaitForTextureFrameU3Ed__14_t898A8E55E0A85C0800EAD3A44AD520F7ACE2E57C_CustomAttributesCacheGenerator_U3CWaitForTextureFrameU3Ed__14__ctor_mF771738CC4E23FBA80292BA09F52FD4F8D6B9D28,
	U3CWaitForTextureFrameU3Ed__14_t898A8E55E0A85C0800EAD3A44AD520F7ACE2E57C_CustomAttributesCacheGenerator_U3CWaitForTextureFrameU3Ed__14_System_IDisposable_Dispose_m0856FDFBE77BC309EFB4E259C2C1F5C449608973,
	U3CWaitForTextureFrameU3Ed__14_t898A8E55E0A85C0800EAD3A44AD520F7ACE2E57C_CustomAttributesCacheGenerator_U3CWaitForTextureFrameU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m53532F7067493675A7491BEBD5F356D352EFD9D6,
	U3CWaitForTextureFrameU3Ed__14_t898A8E55E0A85C0800EAD3A44AD520F7ACE2E57C_CustomAttributesCacheGenerator_U3CWaitForTextureFrameU3Ed__14_System_Collections_IEnumerator_Reset_m46689A7498CC85E7622FE1AD0C2735D6CEF2AC20,
	U3CWaitForTextureFrameU3Ed__14_t898A8E55E0A85C0800EAD3A44AD520F7ACE2E57C_CustomAttributesCacheGenerator_U3CWaitForTextureFrameU3Ed__14_System_Collections_IEnumerator_get_Current_m1D796BDA08361D499F44C847EF0197DE55C3D817,
	SceneDirectors_t2A0127CC9EBDE2D091AACA821A4BD656827DC83B_CustomAttributesCacheGenerator_SceneDirectors_GetCurrentContext_mCCE71B73479FC8A1F8D65066ED794FE54AF5401C,
	SceneDirectors_t2A0127CC9EBDE2D091AACA821A4BD656827DC83B_CustomAttributesCacheGenerator_SceneDirectors_RunGraph_m538FB86082B4D6CC9D2871496FD8F79ECF0E7DFC,
	SceneDirectors_t2A0127CC9EBDE2D091AACA821A4BD656827DC83B_CustomAttributesCacheGenerator_SceneDirectors_WaitForGraph_m336B2A27DAC594C63F79DBC26A63439A4E1908A2,
	SceneDirectors_t2A0127CC9EBDE2D091AACA821A4BD656827DC83B_CustomAttributesCacheGenerator_SceneDirectors_WaitForCamera_m275D49CD83631A2DCB68FB4B48C8E7F6CB350E44,
	U3CRunGraphU3Ed__24_t03B7713933548F64AF7A96F27F0C6320346AA3EE_CustomAttributesCacheGenerator_U3CRunGraphU3Ed__24__ctor_mD8E548AF383C56A2B2A8AEF57581758506147835,
	U3CRunGraphU3Ed__24_t03B7713933548F64AF7A96F27F0C6320346AA3EE_CustomAttributesCacheGenerator_U3CRunGraphU3Ed__24_System_IDisposable_Dispose_m1A65857C24F53A20E78ACA865CEF9BC40FA2FCF8,
	U3CRunGraphU3Ed__24_t03B7713933548F64AF7A96F27F0C6320346AA3EE_CustomAttributesCacheGenerator_U3CRunGraphU3Ed__24_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD2AD458E8533A88202382ACC60A91B838B2DDBAB,
	U3CRunGraphU3Ed__24_t03B7713933548F64AF7A96F27F0C6320346AA3EE_CustomAttributesCacheGenerator_U3CRunGraphU3Ed__24_System_Collections_IEnumerator_Reset_mF598EC361ACE423F3AAA808AE02A3FFD0799AB53,
	U3CRunGraphU3Ed__24_t03B7713933548F64AF7A96F27F0C6320346AA3EE_CustomAttributesCacheGenerator_U3CRunGraphU3Ed__24_System_Collections_IEnumerator_get_Current_m0F69806467AF047BC3C6FB4F9EBD727FD3E1E9A8,
	U3CWaitForGraphU3Ed__25_t82883FECA873B5E87E658A3C6F302DF8FCBD7F0E_CustomAttributesCacheGenerator_U3CWaitForGraphU3Ed__25__ctor_m5CA156DE5E91E041B8EE6C3B206858ED6B2FDE62,
	U3CWaitForGraphU3Ed__25_t82883FECA873B5E87E658A3C6F302DF8FCBD7F0E_CustomAttributesCacheGenerator_U3CWaitForGraphU3Ed__25_System_IDisposable_Dispose_m4AA842376297CE56575BA85E691DD6D60B254E78,
	U3CWaitForGraphU3Ed__25_t82883FECA873B5E87E658A3C6F302DF8FCBD7F0E_CustomAttributesCacheGenerator_U3CWaitForGraphU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF0E9DF6BE3D9F085FA4BB436AAC9E15332D245EC,
	U3CWaitForGraphU3Ed__25_t82883FECA873B5E87E658A3C6F302DF8FCBD7F0E_CustomAttributesCacheGenerator_U3CWaitForGraphU3Ed__25_System_Collections_IEnumerator_Reset_m04AE7F2FE9D453891514E4C6FED67FFD1876A5A5,
	U3CWaitForGraphU3Ed__25_t82883FECA873B5E87E658A3C6F302DF8FCBD7F0E_CustomAttributesCacheGenerator_U3CWaitForGraphU3Ed__25_System_Collections_IEnumerator_get_Current_m49604FA593536760B6C891E6A0A1D2CBAEA241E9,
	U3CWaitForCameraU3Ed__26_t763BCA36C29246D23CF919ADDF985E25C36DBE0D_CustomAttributesCacheGenerator_U3CWaitForCameraU3Ed__26__ctor_mEF0BA9234B5326F8D1DA18B5BC3D54C2EDB621A8,
	U3CWaitForCameraU3Ed__26_t763BCA36C29246D23CF919ADDF985E25C36DBE0D_CustomAttributesCacheGenerator_U3CWaitForCameraU3Ed__26_System_IDisposable_Dispose_m798B59916A7F7B04C2473B999C112FAA56D2E800,
	U3CWaitForCameraU3Ed__26_t763BCA36C29246D23CF919ADDF985E25C36DBE0D_CustomAttributesCacheGenerator_U3CWaitForCameraU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF9C3E046BC69EEED73E7E43B6788A781A09AFC3F,
	U3CWaitForCameraU3Ed__26_t763BCA36C29246D23CF919ADDF985E25C36DBE0D_CustomAttributesCacheGenerator_U3CWaitForCameraU3Ed__26_System_Collections_IEnumerator_Reset_m3CB2E113834F80E3427C87910C5CA706FA5DDA4B,
	U3CWaitForCameraU3Ed__26_t763BCA36C29246D23CF919ADDF985E25C36DBE0D_CustomAttributesCacheGenerator_U3CWaitForCameraU3Ed__26_System_Collections_IEnumerator_get_Current_m6590C064728CBBE508A79AF2027ABFBBF28B93B2,
	WebCamDeviceSelectorController_t66725BA2909CE33463B8E826C910C3A259F17593_CustomAttributesCacheGenerator_WebCamDeviceSelectorController_Start_m519A2B7FD0AA40F570C8EC6EF6A0AEEB057560F2,
	U3CStartU3Ed__2_t729CF6DB68E379AB62FB4461D44DBAD915703537_CustomAttributesCacheGenerator_U3CStartU3Ed__2__ctor_m906CABD8C45330E13A122514A7C09E2EC5FC0039,
	U3CStartU3Ed__2_t729CF6DB68E379AB62FB4461D44DBAD915703537_CustomAttributesCacheGenerator_U3CStartU3Ed__2_System_IDisposable_Dispose_m0139BD6575B1E2FA511461685412C8259DCA96E1,
	U3CStartU3Ed__2_t729CF6DB68E379AB62FB4461D44DBAD915703537_CustomAttributesCacheGenerator_U3CStartU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC9597C9A7823BD016D2A3C49B00789A8CB933A45,
	U3CStartU3Ed__2_t729CF6DB68E379AB62FB4461D44DBAD915703537_CustomAttributesCacheGenerator_U3CStartU3Ed__2_System_Collections_IEnumerator_Reset_m3B48B6C9ABBB439192150CDDCD2EFF1DE7DFC150,
	U3CStartU3Ed__2_t729CF6DB68E379AB62FB4461D44DBAD915703537_CustomAttributesCacheGenerator_U3CStartU3Ed__2_System_Collections_IEnumerator_get_Current_mD2E5B1382AFEAA92756EEC8CEF1A480E6EE3F82D,
	WebCamScreenController_t13EEFE51EC0F7BEA25B5EFAE7AF0733A980F14F7_CustomAttributesCacheGenerator_WebCamScreenController_ResetScreen_mAE3830893EA75A8259087B49F90DE6FC58162FE6,
	WebCamScreenController_t13EEFE51EC0F7BEA25B5EFAE7AF0733A980F14F7_CustomAttributesCacheGenerator_WebCamScreenController_U3CRequestNextFrameU3Eb__25_0_m8AB50133A528C658E544CB7ACE354C0728F7971D,
	U3CResetScreenU3Ed__16_t35B295390641252273E3846EB92DDF8E43F46C89_CustomAttributesCacheGenerator_U3CResetScreenU3Ed__16__ctor_mDAD1FEE5686D6597DCB7579537D2EB62649F79D9,
	U3CResetScreenU3Ed__16_t35B295390641252273E3846EB92DDF8E43F46C89_CustomAttributesCacheGenerator_U3CResetScreenU3Ed__16_System_IDisposable_Dispose_m7C7966A9BC91BB8B9411C59299BE4A64AD4F3F73,
	U3CResetScreenU3Ed__16_t35B295390641252273E3846EB92DDF8E43F46C89_CustomAttributesCacheGenerator_U3CResetScreenU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4832D1E17D71F3ECAF696FCCCF39237400C816C3,
	U3CResetScreenU3Ed__16_t35B295390641252273E3846EB92DDF8E43F46C89_CustomAttributesCacheGenerator_U3CResetScreenU3Ed__16_System_Collections_IEnumerator_Reset_m60226C6644A0E904B746230543DABF1C5BCD4336,
	U3CResetScreenU3Ed__16_t35B295390641252273E3846EB92DDF8E43F46C89_CustomAttributesCacheGenerator_U3CResetScreenU3Ed__16_System_Collections_IEnumerator_get_Current_mB8C7F8DC2FAE2DD8A27D76DF2D91AB5F7B2AA38A,
	AssemblyU2DCSharp_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}
