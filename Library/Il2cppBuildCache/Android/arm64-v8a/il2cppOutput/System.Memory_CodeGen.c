﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void Microsoft.CodeAnalysis.EmbeddedAttribute::.ctor()
extern void EmbeddedAttribute__ctor_m7D7D024DA2EA05AEDF8B8C470F678A5DA96C8EB8 (void);
// 0x00000002 System.Void System.Runtime.CompilerServices.IsReadOnlyAttribute::.ctor()
extern void IsReadOnlyAttribute__ctor_m07A8C937D13DE79AF8ED555F18E5AE9FDA6C3879 (void);
// 0x00000003 System.Void System.Runtime.CompilerServices.IsByRefLikeAttribute::.ctor()
extern void IsByRefLikeAttribute__ctor_m3F813C04C0FAF02B5AF712ED98929300CD6E44DD (void);
// 0x00000004 System.Void System.SequencePosition::.ctor(System.Object,System.Int32)
extern void SequencePosition__ctor_m881E247213B0B28B3903475A1FC0237C56B5F0B0 (void);
// 0x00000005 System.Object System.SequencePosition::GetObject()
extern void SequencePosition_GetObject_m33D4D02B2042DFCCC2549006639381910F1F3525 (void);
// 0x00000006 System.Int32 System.SequencePosition::GetInteger()
extern void SequencePosition_GetInteger_mE4D2683EB441F31A3C1474845ABBD0FA78C130DE (void);
// 0x00000007 System.Boolean System.SequencePosition::Equals(System.SequencePosition)
extern void SequencePosition_Equals_mEA7C1FF9F5C4661547A30C192DC3702CB7647795 (void);
// 0x00000008 System.Boolean System.SequencePosition::Equals(System.Object)
extern void SequencePosition_Equals_mEA56903889413D851A4F93FC2C96D0A6BA823A58 (void);
// 0x00000009 System.Int32 System.SequencePosition::GetHashCode()
extern void SequencePosition_GetHashCode_m1BEFA85FBA8965A92F4A2408AA491758C0CD7DF2 (void);
// 0x0000000A System.Void System.ThrowHelper::ThrowArgumentNullException(System.ExceptionArgument)
extern void ThrowHelper_ThrowArgumentNullException_m3B4D674B817C817E97F4687F0130007D315F8B34 (void);
// 0x0000000B System.Exception System.ThrowHelper::CreateArgumentNullException(System.ExceptionArgument)
extern void ThrowHelper_CreateArgumentNullException_mA70D942EBA7503962BA72170F026A966513590FC (void);
// 0x0000000C System.Void System.ThrowHelper::ThrowArrayTypeMismatchException()
extern void ThrowHelper_ThrowArrayTypeMismatchException_mFC0D7756FD2EA1A7E41D8426D819369FDBD728FC (void);
// 0x0000000D System.Exception System.ThrowHelper::CreateArrayTypeMismatchException()
extern void ThrowHelper_CreateArrayTypeMismatchException_m23F27BF82F951A64682A2CF14E0EDE9F3B54C93F (void);
// 0x0000000E System.Void System.ThrowHelper::ThrowArgumentException_InvalidTypeWithPointersNotSupported(System.Type)
extern void ThrowHelper_ThrowArgumentException_InvalidTypeWithPointersNotSupported_m4A71872D4B069AF36758A61E4CA3FB663B4E8EC4 (void);
// 0x0000000F System.Exception System.ThrowHelper::CreateArgumentException_InvalidTypeWithPointersNotSupported(System.Type)
extern void ThrowHelper_CreateArgumentException_InvalidTypeWithPointersNotSupported_m4019588ED8511C985604C8CC9AD4AB6414676945 (void);
// 0x00000010 System.Void System.ThrowHelper::ThrowArgumentException_DestinationTooShort()
extern void ThrowHelper_ThrowArgumentException_DestinationTooShort_mD9C82D6A62948DA443166283990BF760F77C76C8 (void);
// 0x00000011 System.Exception System.ThrowHelper::CreateArgumentException_DestinationTooShort()
extern void ThrowHelper_CreateArgumentException_DestinationTooShort_m75CF4B3D7F56B0383E0BC84D86C085AA0CE90CD1 (void);
// 0x00000012 System.Void System.ThrowHelper::ThrowIndexOutOfRangeException()
extern void ThrowHelper_ThrowIndexOutOfRangeException_m4D1EB8558F17DFE372ECF87D9BCAD112A7F5E6BC (void);
// 0x00000013 System.Exception System.ThrowHelper::CreateIndexOutOfRangeException()
extern void ThrowHelper_CreateIndexOutOfRangeException_m8C8886676269B09CC5241BA6F5330D78B26F527B (void);
// 0x00000014 System.Void System.ThrowHelper::ThrowArgumentOutOfRangeException()
extern void ThrowHelper_ThrowArgumentOutOfRangeException_mB72471F11341E214DA380AF2B87C3F28EC51CA59 (void);
// 0x00000015 System.Exception System.ThrowHelper::CreateArgumentOutOfRangeException()
extern void ThrowHelper_CreateArgumentOutOfRangeException_m0841E9BF864372D7BF0512A13456F985C53FC03D (void);
// 0x00000016 System.Void System.ThrowHelper::ThrowArgumentOutOfRangeException(System.ExceptionArgument)
extern void ThrowHelper_ThrowArgumentOutOfRangeException_m86EB6B05BDE45B6F92A7599E80E0179C17391AB5 (void);
// 0x00000017 System.Exception System.ThrowHelper::CreateArgumentOutOfRangeException(System.ExceptionArgument)
extern void ThrowHelper_CreateArgumentOutOfRangeException_m3ED3DA6D593699354BA4D397790440F3BFE84AEA (void);
// 0x00000018 System.Void System.ThrowHelper::ThrowInvalidOperationException_EndPositionNotReached()
extern void ThrowHelper_ThrowInvalidOperationException_EndPositionNotReached_m67B5DCC8C43494E0A491781D118E147337664DB0 (void);
// 0x00000019 System.Exception System.ThrowHelper::CreateInvalidOperationException_EndPositionNotReached()
extern void ThrowHelper_CreateInvalidOperationException_EndPositionNotReached_m35A30B605551B8CACAE6B842C8B525BC7078FE72 (void);
// 0x0000001A System.Void System.Memory`1::.ctor(T[])
// 0x0000001B System.Void System.Memory`1::.ctor(System.Buffers.MemoryManager`1<T>,System.Int32)
// 0x0000001C System.Void System.Memory`1::.ctor(System.Object,System.Int32,System.Int32)
// 0x0000001D System.ReadOnlyMemory`1<T> System.Memory`1::op_Implicit(System.Memory`1<T>)
// 0x0000001E System.String System.Memory`1::ToString()
// 0x0000001F System.Memory`1<T> System.Memory`1::Slice(System.Int32,System.Int32)
// 0x00000020 System.Span`1<T> System.Memory`1::get_Span()
// 0x00000021 System.Boolean System.Memory`1::Equals(System.Object)
// 0x00000022 System.Boolean System.Memory`1::Equals(System.Memory`1<T>)
// 0x00000023 System.Int32 System.Memory`1::GetHashCode()
// 0x00000024 System.Int32 System.Memory`1::CombineHashCodes(System.Int32,System.Int32)
// 0x00000025 System.Int32 System.Memory`1::CombineHashCodes(System.Int32,System.Int32,System.Int32)
// 0x00000026 System.Boolean System.MemoryExtensions::SequenceEqual(System.ReadOnlySpan`1<T>,System.ReadOnlySpan`1<T>)
// 0x00000027 System.Void System.MemoryExtensions::Reverse(System.Span`1<T>)
// 0x00000028 System.Memory`1<T> System.MemoryExtensions::AsMemory(T[])
// 0x00000029 System.Boolean System.MemoryExtensions::IsTypeComparableAsBytes(System.NUInt&)
// 0x0000002A System.ReadOnlySpan`1<System.Char> System.MemoryExtensions::AsSpan(System.String)
extern void MemoryExtensions_AsSpan_m7527C7806D1DD24C012DC60C12280A9E1AEA8F15 (void);
// 0x0000002B System.ReadOnlyMemory`1<System.Char> System.MemoryExtensions::AsMemory(System.String,System.Int32,System.Int32)
extern void MemoryExtensions_AsMemory_m9F2378B1710076CA61B1FC3E687A06BC6063A9DC (void);
// 0x0000002C System.IntPtr System.MemoryExtensions::MeasureStringAdjustment()
extern void MemoryExtensions_MeasureStringAdjustment_m8E2719E3CCAD24803BEF8B9C9873DDFAA528C762 (void);
// 0x0000002D System.Void System.MemoryExtensions::.cctor()
extern void MemoryExtensions__cctor_mC634116818572F66DC5A4416FB29AFBFCE859EBE (void);
// 0x0000002E System.Void System.ReadOnlyMemory`1::.ctor(T[])
// 0x0000002F System.Void System.ReadOnlyMemory`1::.ctor(T[],System.Int32,System.Int32)
// 0x00000030 System.Void System.ReadOnlyMemory`1::.ctor(System.Object,System.Int32,System.Int32)
// 0x00000031 System.ReadOnlyMemory`1<T> System.ReadOnlyMemory`1::op_Implicit(T[])
// 0x00000032 System.Int32 System.ReadOnlyMemory`1::get_Length()
// 0x00000033 System.String System.ReadOnlyMemory`1::ToString()
// 0x00000034 System.ReadOnlyMemory`1<T> System.ReadOnlyMemory`1::Slice(System.Int32)
// 0x00000035 System.ReadOnlyMemory`1<T> System.ReadOnlyMemory`1::Slice(System.Int32,System.Int32)
// 0x00000036 System.ReadOnlySpan`1<T> System.ReadOnlyMemory`1::get_Span()
// 0x00000037 T[] System.ReadOnlyMemory`1::ToArray()
// 0x00000038 System.Boolean System.ReadOnlyMemory`1::Equals(System.Object)
// 0x00000039 System.Boolean System.ReadOnlyMemory`1::Equals(System.ReadOnlyMemory`1<T>)
// 0x0000003A System.Int32 System.ReadOnlyMemory`1::GetHashCode()
// 0x0000003B System.Int32 System.ReadOnlyMemory`1::CombineHashCodes(System.Int32,System.Int32)
// 0x0000003C System.Int32 System.ReadOnlyMemory`1::CombineHashCodes(System.Int32,System.Int32,System.Int32)
// 0x0000003D System.Object System.ReadOnlyMemory`1::GetObjectStartLength(System.Int32&,System.Int32&)
// 0x0000003E System.Int32 System.ReadOnlySpan`1::get_Length()
// 0x0000003F System.Boolean System.ReadOnlySpan`1::Equals(System.Object)
// 0x00000040 System.Int32 System.ReadOnlySpan`1::GetHashCode()
// 0x00000041 System.Void System.ReadOnlySpan`1::.ctor(T[])
// 0x00000042 System.Void System.ReadOnlySpan`1::.ctor(T[],System.Int32,System.Int32)
// 0x00000043 System.Void System.ReadOnlySpan`1::.ctor(System.Pinnable`1<T>,System.IntPtr,System.Int32)
// 0x00000044 T& modreq(System.Runtime.InteropServices.InAttribute) System.ReadOnlySpan`1::get_Item(System.Int32)
// 0x00000045 System.Void System.ReadOnlySpan`1::CopyTo(System.Span`1<T>)
// 0x00000046 System.Boolean System.ReadOnlySpan`1::TryCopyTo(System.Span`1<T>)
// 0x00000047 System.String System.ReadOnlySpan`1::ToString()
// 0x00000048 System.ReadOnlySpan`1<T> System.ReadOnlySpan`1::Slice(System.Int32,System.Int32)
// 0x00000049 T[] System.ReadOnlySpan`1::ToArray()
// 0x0000004A T& System.ReadOnlySpan`1::DangerousGetPinnableReference()
// 0x0000004B System.Pinnable`1<T> System.ReadOnlySpan`1::get_Pinnable()
// 0x0000004C System.IntPtr System.ReadOnlySpan`1::get_ByteOffset()
// 0x0000004D System.Int32 System.Span`1::get_Length()
// 0x0000004E System.Boolean System.Span`1::Equals(System.Object)
// 0x0000004F System.Int32 System.Span`1::GetHashCode()
// 0x00000050 System.Span`1<T> System.Span`1::op_Implicit(T[])
// 0x00000051 System.Void System.Span`1::.ctor(T[])
// 0x00000052 System.Void System.Span`1::.ctor(T[],System.Int32,System.Int32)
// 0x00000053 System.Void System.Span`1::.ctor(System.Void*,System.Int32)
// 0x00000054 System.Void System.Span`1::.ctor(System.Pinnable`1<T>,System.IntPtr,System.Int32)
// 0x00000055 T& System.Span`1::get_Item(System.Int32)
// 0x00000056 System.ReadOnlySpan`1<T> System.Span`1::op_Implicit(System.Span`1<T>)
// 0x00000057 System.String System.Span`1::ToString()
// 0x00000058 System.Span`1<T> System.Span`1::Slice(System.Int32)
// 0x00000059 System.Span`1<T> System.Span`1::Slice(System.Int32,System.Int32)
// 0x0000005A T& System.Span`1::DangerousGetPinnableReference()
// 0x0000005B System.Pinnable`1<T> System.Span`1::get_Pinnable()
// 0x0000005C System.IntPtr System.Span`1::get_ByteOffset()
// 0x0000005D System.Boolean System.SpanHelpers::SequenceEqual(System.Byte&,System.Byte&,System.NUInt)
extern void SpanHelpers_SequenceEqual_mDEB0F358BB173EA24BEEB0609454A997E9273A89 (void);
// 0x0000005E System.Boolean System.SpanHelpers::SequenceEqual(T&,T&,System.Int32)
// 0x0000005F System.Void System.SpanHelpers::CopyTo(T&,System.Int32,T&,System.Int32)
// 0x00000060 System.IntPtr System.SpanHelpers::Add(System.IntPtr,System.Int32)
// 0x00000061 System.Boolean System.SpanHelpers::IsReferenceOrContainsReferences()
// 0x00000062 System.Boolean System.SpanHelpers::IsReferenceOrContainsReferencesCore(System.Type)
extern void SpanHelpers_IsReferenceOrContainsReferencesCore_m4046A8EAD00DA02AA423C292A8FCBB08268AD781 (void);
// 0x00000063 System.IntPtr System.SpanHelpers/PerTypeValues`1::MeasureArrayAdjustment()
// 0x00000064 System.Void System.SpanHelpers/PerTypeValues`1::.cctor()
// 0x00000065 System.Void System.NUInt::.ctor(System.UInt32)
extern void NUInt__ctor_m34A1178C5D59B395E905B670FCF390D1AA5DC85E (void);
// 0x00000066 System.Void System.NUInt::.ctor(System.UInt64)
extern void NUInt__ctor_mBD99E19E274774DF07488C672C5DFC90F4B21973 (void);
// 0x00000067 System.NUInt System.NUInt::op_Explicit(System.Int32)
extern void NUInt_op_Explicit_m680513883587956D1452B1EB6D321D4C3A0C8366 (void);
// 0x00000068 System.Void* System.NUInt::op_Explicit(System.NUInt)
extern void NUInt_op_Explicit_mAC8186F05FC1F16BAEB9A73957491CB24A067D46 (void);
// 0x00000069 System.NUInt System.NUInt::op_Multiply(System.NUInt,System.NUInt)
extern void NUInt_op_Multiply_mABFB3E10A51F74FDC0CD9B799B7BF35C2C5D8D85 (void);
// 0x0000006A System.Resources.ResourceManager System.SR::get_ResourceManager()
extern void SR_get_ResourceManager_m18A791F4D611559D5B214B3020BAB11F2AC869EC (void);
// 0x0000006B System.Boolean System.SR::UsingResourceKeys()
extern void SR_UsingResourceKeys_m08DBDDDDF80E9F0013615CAB611F552F836BB526 (void);
// 0x0000006C System.String System.SR::GetResourceString(System.String,System.String)
extern void SR_GetResourceString_mEC79B3C28B26B1540E26C3CD899938CC955A4748 (void);
// 0x0000006D System.String System.SR::Format(System.String,System.Object)
extern void SR_Format_m4480ECD777F2A905A368094827DDCB43478A8053 (void);
// 0x0000006E System.Type System.SR::get_ResourceType()
extern void SR_get_ResourceType_mA677195FD1721150495B84739EFFDCB9366A5541 (void);
// 0x0000006F System.String System.SR::get_NotSupported_CannotCallEqualsOnSpan()
extern void SR_get_NotSupported_CannotCallEqualsOnSpan_mACE24A88A0ADF9880C315FDC0963BA17E66B0394 (void);
// 0x00000070 System.String System.SR::get_NotSupported_CannotCallGetHashCodeOnSpan()
extern void SR_get_NotSupported_CannotCallGetHashCodeOnSpan_m4BC3D1B6994913E69BDD4028026F18A279A9DBDB (void);
// 0x00000071 System.String System.SR::get_Argument_InvalidTypeWithPointersNotSupported()
extern void SR_get_Argument_InvalidTypeWithPointersNotSupported_m2FD2DCBFF1853C8F9616D4C55DD1C14163A06B75 (void);
// 0x00000072 System.String System.SR::get_Argument_DestinationTooShort()
extern void SR_get_Argument_DestinationTooShort_mDD536A55FFA1BD1CF5C34D9E074420C183905559 (void);
// 0x00000073 System.String System.SR::get_EndPositionNotReached()
extern void SR_get_EndPositionNotReached_m26E126334F58B1570EEAAA53A48B9518F3C17913 (void);
// 0x00000074 System.Void System.SR::.cctor()
extern void SR__cctor_m4CA77DF9E538A3B432DD3F12C4D3E655983629DB (void);
// 0x00000075 System.Int32 System.Numerics.Hashing.HashHelpers::Combine(System.Int32,System.Int32)
extern void HashHelpers_Combine_m0B422F3A90AC3CD046375C8195F8ED339B83ED46 (void);
// 0x00000076 System.Void System.Numerics.Hashing.HashHelpers::.cctor()
extern void HashHelpers__cctor_mE4D846284DBD325D39520B0D94CE6D08B7A937E2 (void);
// 0x00000077 System.Boolean System.Runtime.InteropServices.SequenceMarshal::TryGetString(System.Buffers.ReadOnlySequence`1<System.Char>,System.String&,System.Int32&,System.Int32&)
extern void SequenceMarshal_TryGetString_mBA81A10F83642193C6BB3862B3E847222BE88F7B (void);
// 0x00000078 System.Boolean System.Runtime.InteropServices.MemoryMarshal::TryGetArray(System.ReadOnlyMemory`1<T>,System.ArraySegment`1<T>&)
// 0x00000079 System.Collections.Generic.IEnumerable`1<T> System.Runtime.InteropServices.MemoryMarshal::ToEnumerable(System.ReadOnlyMemory`1<T>)
// 0x0000007A T System.Runtime.InteropServices.MemoryMarshal::Read(System.ReadOnlySpan`1<System.Byte>)
// 0x0000007B System.Void System.Runtime.InteropServices.MemoryMarshal::Write(System.Span`1<System.Byte>,T&)
// 0x0000007C T& System.Runtime.InteropServices.MemoryMarshal::GetReference(System.Span`1<T>)
// 0x0000007D T& System.Runtime.InteropServices.MemoryMarshal::GetReference(System.ReadOnlySpan`1<T>)
// 0x0000007E System.Void System.Runtime.InteropServices.MemoryMarshal/<ToEnumerable>d__3`1::.ctor(System.Int32)
// 0x0000007F System.Void System.Runtime.InteropServices.MemoryMarshal/<ToEnumerable>d__3`1::System.IDisposable.Dispose()
// 0x00000080 System.Boolean System.Runtime.InteropServices.MemoryMarshal/<ToEnumerable>d__3`1::MoveNext()
// 0x00000081 T System.Runtime.InteropServices.MemoryMarshal/<ToEnumerable>d__3`1::System.Collections.Generic.IEnumerator<T>.get_Current()
// 0x00000082 System.Void System.Runtime.InteropServices.MemoryMarshal/<ToEnumerable>d__3`1::System.Collections.IEnumerator.Reset()
// 0x00000083 System.Object System.Runtime.InteropServices.MemoryMarshal/<ToEnumerable>d__3`1::System.Collections.IEnumerator.get_Current()
// 0x00000084 System.Collections.Generic.IEnumerator`1<T> System.Runtime.InteropServices.MemoryMarshal/<ToEnumerable>d__3`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x00000085 System.Collections.IEnumerator System.Runtime.InteropServices.MemoryMarshal/<ToEnumerable>d__3`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000086 System.Void System.Buffers.BuffersExtensions::CopyTo(System.Buffers.ReadOnlySequence`1<T>&,System.Span`1<T>)
// 0x00000087 System.Void System.Buffers.BuffersExtensions::CopyToMultiSegment(System.Buffers.ReadOnlySequence`1<T>&,System.Span`1<T>)
// 0x00000088 T[] System.Buffers.BuffersExtensions::ToArray(System.Buffers.ReadOnlySequence`1<T>&)
// 0x00000089 System.Void System.Buffers.IBufferWriter`1::Advance(System.Int32)
// 0x0000008A System.Span`1<T> System.Buffers.IBufferWriter`1::GetSpan(System.Int32)
// 0x0000008B System.Int64 System.Buffers.ReadOnlySequence`1::get_Length()
// 0x0000008C System.Boolean System.Buffers.ReadOnlySequence`1::get_IsSingleSegment()
// 0x0000008D System.ReadOnlyMemory`1<T> System.Buffers.ReadOnlySequence`1::get_First()
// 0x0000008E System.SequencePosition System.Buffers.ReadOnlySequence`1::get_Start()
// 0x0000008F System.Void System.Buffers.ReadOnlySequence`1::.ctor(T[])
// 0x00000090 System.String System.Buffers.ReadOnlySequence`1::ToString()
// 0x00000091 System.Buffers.ReadOnlySequence`1/Enumerator<T> System.Buffers.ReadOnlySequence`1::GetEnumerator()
// 0x00000092 System.Boolean System.Buffers.ReadOnlySequence`1::TryGet(System.SequencePosition&,System.ReadOnlyMemory`1<T>&,System.Boolean)
// 0x00000093 System.Boolean System.Buffers.ReadOnlySequence`1::TryGetBuffer(System.SequencePosition&,System.ReadOnlyMemory`1<T>&,System.SequencePosition&)
// 0x00000094 System.ReadOnlyMemory`1<T> System.Buffers.ReadOnlySequence`1::GetFirstBuffer()
// 0x00000095 System.Buffers.ReadOnlySequence`1/SequenceType<T> System.Buffers.ReadOnlySequence`1::GetSequenceType()
// 0x00000096 System.Int32 System.Buffers.ReadOnlySequence`1::GetIndex(System.SequencePosition&)
// 0x00000097 System.Int64 System.Buffers.ReadOnlySequence`1::GetLength()
// 0x00000098 System.Boolean System.Buffers.ReadOnlySequence`1::TryGetString(System.String&,System.Int32&,System.Int32&)
// 0x00000099 System.Void System.Buffers.ReadOnlySequence`1::.cctor()
// 0x0000009A System.Void System.Buffers.ReadOnlySequence`1/Enumerator::.ctor(System.Buffers.ReadOnlySequence`1<T>&)
// 0x0000009B System.ReadOnlyMemory`1<T> System.Buffers.ReadOnlySequence`1/Enumerator::get_Current()
// 0x0000009C System.Boolean System.Buffers.ReadOnlySequence`1/Enumerator::MoveNext()
// 0x0000009D System.Int32 System.Buffers.ReadOnlySequence::ArrayToSequenceStart(System.Int32)
extern void ReadOnlySequence_ArrayToSequenceStart_mCEAF0855FE164270628814C10B531646278E0855 (void);
// 0x0000009E System.Int32 System.Buffers.ReadOnlySequence::ArrayToSequenceEnd(System.Int32)
extern void ReadOnlySequence_ArrayToSequenceEnd_mFF69FF0508B383A32C7EF9F89E701787D112BAB6 (void);
// 0x0000009F System.ReadOnlyMemory`1<T> System.Buffers.ReadOnlySequenceSegment`1::get_Memory()
// 0x000000A0 System.Buffers.ReadOnlySequenceSegment`1<T> System.Buffers.ReadOnlySequenceSegment`1::get_Next()
// 0x000000A1 System.Int64 System.Buffers.ReadOnlySequenceSegment`1::get_RunningIndex()
// 0x000000A2 System.Memory`1<T> System.Buffers.MemoryManager`1::get_Memory()
// 0x000000A3 System.Span`1<T> System.Buffers.MemoryManager`1::GetSpan()
// 0x000000A4 System.Boolean System.Buffers.MemoryManager`1::TryGetArray(System.ArraySegment`1<T>&)
// 0x000000A5 System.UInt32 System.Buffers.Binary.BinaryPrimitives::ReverseEndianness(System.UInt32)
extern void BinaryPrimitives_ReverseEndianness_m7C562C76F215F77432B9600686CB25A54E88CC20 (void);
// 0x000000A6 System.UInt64 System.Buffers.Binary.BinaryPrimitives::ReverseEndianness(System.UInt64)
extern void BinaryPrimitives_ReverseEndianness_m6268D4E81221B0851A9F12D3446F3B488B804696 (void);
// 0x000000A7 System.UInt64 System.Buffers.Binary.BinaryPrimitives::ReadUInt64LittleEndian(System.ReadOnlySpan`1<System.Byte>)
extern void BinaryPrimitives_ReadUInt64LittleEndian_m48F9AEE3848A21C018BB45F2D4A275497D824DD5 (void);
// 0x000000A8 System.Void System.Buffers.Binary.BinaryPrimitives::WriteUInt32LittleEndian(System.Span`1<System.Byte>,System.UInt32)
extern void BinaryPrimitives_WriteUInt32LittleEndian_m3287950DDEAF58E0CB38C821C2C449A29A7E40AD (void);
// 0x000000A9 System.Void System.Buffers.Binary.BinaryPrimitives::WriteUInt64LittleEndian(System.Span`1<System.Byte>,System.UInt64)
extern void BinaryPrimitives_WriteUInt64LittleEndian_m4E27EC6B15226FD87A93FB5B59C6840DE3807F41 (void);
static Il2CppMethodPointer s_methodPointers[169] = 
{
	EmbeddedAttribute__ctor_m7D7D024DA2EA05AEDF8B8C470F678A5DA96C8EB8,
	IsReadOnlyAttribute__ctor_m07A8C937D13DE79AF8ED555F18E5AE9FDA6C3879,
	IsByRefLikeAttribute__ctor_m3F813C04C0FAF02B5AF712ED98929300CD6E44DD,
	SequencePosition__ctor_m881E247213B0B28B3903475A1FC0237C56B5F0B0,
	SequencePosition_GetObject_m33D4D02B2042DFCCC2549006639381910F1F3525,
	SequencePosition_GetInteger_mE4D2683EB441F31A3C1474845ABBD0FA78C130DE,
	SequencePosition_Equals_mEA7C1FF9F5C4661547A30C192DC3702CB7647795,
	SequencePosition_Equals_mEA56903889413D851A4F93FC2C96D0A6BA823A58,
	SequencePosition_GetHashCode_m1BEFA85FBA8965A92F4A2408AA491758C0CD7DF2,
	ThrowHelper_ThrowArgumentNullException_m3B4D674B817C817E97F4687F0130007D315F8B34,
	ThrowHelper_CreateArgumentNullException_mA70D942EBA7503962BA72170F026A966513590FC,
	ThrowHelper_ThrowArrayTypeMismatchException_mFC0D7756FD2EA1A7E41D8426D819369FDBD728FC,
	ThrowHelper_CreateArrayTypeMismatchException_m23F27BF82F951A64682A2CF14E0EDE9F3B54C93F,
	ThrowHelper_ThrowArgumentException_InvalidTypeWithPointersNotSupported_m4A71872D4B069AF36758A61E4CA3FB663B4E8EC4,
	ThrowHelper_CreateArgumentException_InvalidTypeWithPointersNotSupported_m4019588ED8511C985604C8CC9AD4AB6414676945,
	ThrowHelper_ThrowArgumentException_DestinationTooShort_mD9C82D6A62948DA443166283990BF760F77C76C8,
	ThrowHelper_CreateArgumentException_DestinationTooShort_m75CF4B3D7F56B0383E0BC84D86C085AA0CE90CD1,
	ThrowHelper_ThrowIndexOutOfRangeException_m4D1EB8558F17DFE372ECF87D9BCAD112A7F5E6BC,
	ThrowHelper_CreateIndexOutOfRangeException_m8C8886676269B09CC5241BA6F5330D78B26F527B,
	ThrowHelper_ThrowArgumentOutOfRangeException_mB72471F11341E214DA380AF2B87C3F28EC51CA59,
	ThrowHelper_CreateArgumentOutOfRangeException_m0841E9BF864372D7BF0512A13456F985C53FC03D,
	ThrowHelper_ThrowArgumentOutOfRangeException_m86EB6B05BDE45B6F92A7599E80E0179C17391AB5,
	ThrowHelper_CreateArgumentOutOfRangeException_m3ED3DA6D593699354BA4D397790440F3BFE84AEA,
	ThrowHelper_ThrowInvalidOperationException_EndPositionNotReached_m67B5DCC8C43494E0A491781D118E147337664DB0,
	ThrowHelper_CreateInvalidOperationException_EndPositionNotReached_m35A30B605551B8CACAE6B842C8B525BC7078FE72,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	MemoryExtensions_AsSpan_m7527C7806D1DD24C012DC60C12280A9E1AEA8F15,
	MemoryExtensions_AsMemory_m9F2378B1710076CA61B1FC3E687A06BC6063A9DC,
	MemoryExtensions_MeasureStringAdjustment_m8E2719E3CCAD24803BEF8B9C9873DDFAA528C762,
	MemoryExtensions__cctor_mC634116818572F66DC5A4416FB29AFBFCE859EBE,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	SpanHelpers_SequenceEqual_mDEB0F358BB173EA24BEEB0609454A997E9273A89,
	NULL,
	NULL,
	NULL,
	NULL,
	SpanHelpers_IsReferenceOrContainsReferencesCore_m4046A8EAD00DA02AA423C292A8FCBB08268AD781,
	NULL,
	NULL,
	NUInt__ctor_m34A1178C5D59B395E905B670FCF390D1AA5DC85E,
	NUInt__ctor_mBD99E19E274774DF07488C672C5DFC90F4B21973,
	NUInt_op_Explicit_m680513883587956D1452B1EB6D321D4C3A0C8366,
	NUInt_op_Explicit_mAC8186F05FC1F16BAEB9A73957491CB24A067D46,
	NUInt_op_Multiply_mABFB3E10A51F74FDC0CD9B799B7BF35C2C5D8D85,
	SR_get_ResourceManager_m18A791F4D611559D5B214B3020BAB11F2AC869EC,
	SR_UsingResourceKeys_m08DBDDDDF80E9F0013615CAB611F552F836BB526,
	SR_GetResourceString_mEC79B3C28B26B1540E26C3CD899938CC955A4748,
	SR_Format_m4480ECD777F2A905A368094827DDCB43478A8053,
	SR_get_ResourceType_mA677195FD1721150495B84739EFFDCB9366A5541,
	SR_get_NotSupported_CannotCallEqualsOnSpan_mACE24A88A0ADF9880C315FDC0963BA17E66B0394,
	SR_get_NotSupported_CannotCallGetHashCodeOnSpan_m4BC3D1B6994913E69BDD4028026F18A279A9DBDB,
	SR_get_Argument_InvalidTypeWithPointersNotSupported_m2FD2DCBFF1853C8F9616D4C55DD1C14163A06B75,
	SR_get_Argument_DestinationTooShort_mDD536A55FFA1BD1CF5C34D9E074420C183905559,
	SR_get_EndPositionNotReached_m26E126334F58B1570EEAAA53A48B9518F3C17913,
	SR__cctor_m4CA77DF9E538A3B432DD3F12C4D3E655983629DB,
	HashHelpers_Combine_m0B422F3A90AC3CD046375C8195F8ED339B83ED46,
	HashHelpers__cctor_mE4D846284DBD325D39520B0D94CE6D08B7A937E2,
	SequenceMarshal_TryGetString_mBA81A10F83642193C6BB3862B3E847222BE88F7B,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ReadOnlySequence_ArrayToSequenceStart_mCEAF0855FE164270628814C10B531646278E0855,
	ReadOnlySequence_ArrayToSequenceEnd_mFF69FF0508B383A32C7EF9F89E701787D112BAB6,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	BinaryPrimitives_ReverseEndianness_m7C562C76F215F77432B9600686CB25A54E88CC20,
	BinaryPrimitives_ReverseEndianness_m6268D4E81221B0851A9F12D3446F3B488B804696,
	BinaryPrimitives_ReadUInt64LittleEndian_m48F9AEE3848A21C018BB45F2D4A275497D824DD5,
	BinaryPrimitives_WriteUInt32LittleEndian_m3287950DDEAF58E0CB38C821C2C449A29A7E40AD,
	BinaryPrimitives_WriteUInt64LittleEndian_m4E27EC6B15226FD87A93FB5B59C6840DE3807F41,
};
extern void SequencePosition__ctor_m881E247213B0B28B3903475A1FC0237C56B5F0B0_AdjustorThunk (void);
extern void SequencePosition_GetObject_m33D4D02B2042DFCCC2549006639381910F1F3525_AdjustorThunk (void);
extern void SequencePosition_GetInteger_mE4D2683EB441F31A3C1474845ABBD0FA78C130DE_AdjustorThunk (void);
extern void SequencePosition_Equals_mEA7C1FF9F5C4661547A30C192DC3702CB7647795_AdjustorThunk (void);
extern void SequencePosition_Equals_mEA56903889413D851A4F93FC2C96D0A6BA823A58_AdjustorThunk (void);
extern void SequencePosition_GetHashCode_m1BEFA85FBA8965A92F4A2408AA491758C0CD7DF2_AdjustorThunk (void);
extern void NUInt__ctor_m34A1178C5D59B395E905B670FCF390D1AA5DC85E_AdjustorThunk (void);
extern void NUInt__ctor_mBD99E19E274774DF07488C672C5DFC90F4B21973_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[8] = 
{
	{ 0x06000004, SequencePosition__ctor_m881E247213B0B28B3903475A1FC0237C56B5F0B0_AdjustorThunk },
	{ 0x06000005, SequencePosition_GetObject_m33D4D02B2042DFCCC2549006639381910F1F3525_AdjustorThunk },
	{ 0x06000006, SequencePosition_GetInteger_mE4D2683EB441F31A3C1474845ABBD0FA78C130DE_AdjustorThunk },
	{ 0x06000007, SequencePosition_Equals_mEA7C1FF9F5C4661547A30C192DC3702CB7647795_AdjustorThunk },
	{ 0x06000008, SequencePosition_Equals_mEA56903889413D851A4F93FC2C96D0A6BA823A58_AdjustorThunk },
	{ 0x06000009, SequencePosition_GetHashCode_m1BEFA85FBA8965A92F4A2408AA491758C0CD7DF2_AdjustorThunk },
	{ 0x06000065, NUInt__ctor_m34A1178C5D59B395E905B670FCF390D1AA5DC85E_AdjustorThunk },
	{ 0x06000066, NUInt__ctor_mBD99E19E274774DF07488C672C5DFC90F4B21973_AdjustorThunk },
};
static const int32_t s_InvokerIndices[169] = 
{
	1450,
	1450,
	1450,
	797,
	1416,
	1405,
	1128,
	1109,
	1405,
	2418,
	2353,
	2458,
	2445,
	2420,
	2356,
	2458,
	2445,
	2458,
	2445,
	2458,
	2445,
	2418,
	2353,
	2458,
	2445,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	2252,
	1791,
	2442,
	2458,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	1892,
	-1,
	-1,
	-1,
	-1,
	2380,
	-1,
	-1,
	1235,
	1236,
	2338,
	2267,
	2067,
	2445,
	2452,
	2085,
	2085,
	2445,
	2445,
	2445,
	2445,
	2445,
	2445,
	2458,
	2032,
	2458,
	1717,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	2308,
	2308,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	2308,
	2321,
	2315,
	2194,
	2195,
};
static const Il2CppTokenRangePair s_rgctxIndices[26] = 
{
	{ 0x02000009, { 0, 22 } },
	{ 0x0200000C, { 34, 24 } },
	{ 0x0200000D, { 58, 19 } },
	{ 0x0200000E, { 77, 17 } },
	{ 0x02000011, { 105, 6 } },
	{ 0x02000018, { 139, 7 } },
	{ 0x0200001B, { 164, 28 } },
	{ 0x0200001C, { 192, 2 } },
	{ 0x02000021, { 194, 4 } },
	{ 0x06000026, { 22, 6 } },
	{ 0x06000027, { 28, 3 } },
	{ 0x06000028, { 31, 2 } },
	{ 0x06000029, { 33, 1 } },
	{ 0x0600005E, { 94, 5 } },
	{ 0x0600005F, { 99, 4 } },
	{ 0x06000060, { 103, 1 } },
	{ 0x06000061, { 104, 1 } },
	{ 0x06000078, { 111, 10 } },
	{ 0x06000079, { 121, 2 } },
	{ 0x0600007A, { 123, 4 } },
	{ 0x0600007B, { 127, 4 } },
	{ 0x0600007C, { 131, 4 } },
	{ 0x0600007D, { 135, 4 } },
	{ 0x06000086, { 146, 7 } },
	{ 0x06000087, { 153, 6 } },
	{ 0x06000088, { 159, 5 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[198] = 
{
	{ (Il2CppRGCTXDataType)2, 438 },
	{ (Il2CppRGCTXDataType)1, 3831 },
	{ (Il2CppRGCTXDataType)3, 11058 },
	{ (Il2CppRGCTXDataType)1, 438 },
	{ (Il2CppRGCTXDataType)3, 6379 },
	{ (Il2CppRGCTXDataType)2, 3258 },
	{ (Il2CppRGCTXDataType)2, 2699 },
	{ (Il2CppRGCTXDataType)3, 6375 },
	{ (Il2CppRGCTXDataType)2, 2694 },
	{ (Il2CppRGCTXDataType)3, 6363 },
	{ (Il2CppRGCTXDataType)3, 8210 },
	{ (Il2CppRGCTXDataType)3, 11047 },
	{ (Il2CppRGCTXDataType)3, 8208 },
	{ (Il2CppRGCTXDataType)2, 3831 },
	{ (Il2CppRGCTXDataType)3, 8209 },
	{ (Il2CppRGCTXDataType)2, 3047 },
	{ (Il2CppRGCTXDataType)3, 6380 },
	{ (Il2CppRGCTXDataType)2, 2699 },
	{ (Il2CppRGCTXDataType)3, 7409 },
	{ (Il2CppRGCTXDataType)3, 6378 },
	{ (Il2CppRGCTXDataType)3, 6377 },
	{ (Il2CppRGCTXDataType)3, 6376 },
	{ (Il2CppRGCTXDataType)3, 7544 },
	{ (Il2CppRGCTXDataType)2, 204 },
	{ (Il2CppRGCTXDataType)3, 10730 },
	{ (Il2CppRGCTXDataType)3, 10737 },
	{ (Il2CppRGCTXDataType)3, 11054 },
	{ (Il2CppRGCTXDataType)3, 10979 },
	{ (Il2CppRGCTXDataType)3, 10738 },
	{ (Il2CppRGCTXDataType)3, 8204 },
	{ (Il2CppRGCTXDataType)3, 11027 },
	{ (Il2CppRGCTXDataType)2, 2697 },
	{ (Il2CppRGCTXDataType)3, 6373 },
	{ (Il2CppRGCTXDataType)1, 203 },
	{ (Il2CppRGCTXDataType)2, 3048 },
	{ (Il2CppRGCTXDataType)3, 7410 },
	{ (Il2CppRGCTXDataType)1, 461 },
	{ (Il2CppRGCTXDataType)3, 7415 },
	{ (Il2CppRGCTXDataType)2, 3075 },
	{ (Il2CppRGCTXDataType)3, 7411 },
	{ (Il2CppRGCTXDataType)2, 2695 },
	{ (Il2CppRGCTXDataType)3, 6364 },
	{ (Il2CppRGCTXDataType)3, 8211 },
	{ (Il2CppRGCTXDataType)3, 8212 },
	{ (Il2CppRGCTXDataType)2, 3259 },
	{ (Il2CppRGCTXDataType)3, 11048 },
	{ (Il2CppRGCTXDataType)3, 7547 },
	{ (Il2CppRGCTXDataType)3, 7549 },
	{ (Il2CppRGCTXDataType)2, 3836 },
	{ (Il2CppRGCTXDataType)3, 7548 },
	{ (Il2CppRGCTXDataType)3, 7550 },
	{ (Il2CppRGCTXDataType)3, 7414 },
	{ (Il2CppRGCTXDataType)2, 2700 },
	{ (Il2CppRGCTXDataType)3, 6381 },
	{ (Il2CppRGCTXDataType)2, 2700 },
	{ (Il2CppRGCTXDataType)3, 7413 },
	{ (Il2CppRGCTXDataType)2, 3048 },
	{ (Il2CppRGCTXDataType)3, 7412 },
	{ (Il2CppRGCTXDataType)3, 11049 },
	{ (Il2CppRGCTXDataType)2, 2990 },
	{ (Il2CppRGCTXDataType)3, 10961 },
	{ (Il2CppRGCTXDataType)3, 11075 },
	{ (Il2CppRGCTXDataType)3, 11030 },
	{ (Il2CppRGCTXDataType)3, 11039 },
	{ (Il2CppRGCTXDataType)3, 7554 },
	{ (Il2CppRGCTXDataType)3, 8214 },
	{ (Il2CppRGCTXDataType)3, 7553 },
	{ (Il2CppRGCTXDataType)3, 8213 },
	{ (Il2CppRGCTXDataType)3, 10966 },
	{ (Il2CppRGCTXDataType)1, 464 },
	{ (Il2CppRGCTXDataType)3, 11056 },
	{ (Il2CppRGCTXDataType)2, 3076 },
	{ (Il2CppRGCTXDataType)3, 7551 },
	{ (Il2CppRGCTXDataType)2, 3838 },
	{ (Il2CppRGCTXDataType)3, 8215 },
	{ (Il2CppRGCTXDataType)2, 3260 },
	{ (Il2CppRGCTXDataType)3, 7552 },
	{ (Il2CppRGCTXDataType)2, 3261 },
	{ (Il2CppRGCTXDataType)3, 8216 },
	{ (Il2CppRGCTXDataType)2, 474 },
	{ (Il2CppRGCTXDataType)1, 3842 },
	{ (Il2CppRGCTXDataType)3, 11050 },
	{ (Il2CppRGCTXDataType)2, 2991 },
	{ (Il2CppRGCTXDataType)3, 10962 },
	{ (Il2CppRGCTXDataType)3, 10973 },
	{ (Il2CppRGCTXDataType)1, 474 },
	{ (Il2CppRGCTXDataType)3, 11076 },
	{ (Il2CppRGCTXDataType)3, 11031 },
	{ (Il2CppRGCTXDataType)3, 11040 },
	{ (Il2CppRGCTXDataType)2, 3077 },
	{ (Il2CppRGCTXDataType)3, 7555 },
	{ (Il2CppRGCTXDataType)3, 8218 },
	{ (Il2CppRGCTXDataType)3, 11057 },
	{ (Il2CppRGCTXDataType)3, 8217 },
	{ (Il2CppRGCTXDataType)3, 11044 },
	{ (Il2CppRGCTXDataType)3, 11028 },
	{ (Il2CppRGCTXDataType)2, 279 },
	{ (Il2CppRGCTXDataType)2, 2041 },
	{ (Il2CppRGCTXDataType)3, 3998 },
	{ (Il2CppRGCTXDataType)3, 11029 },
	{ (Il2CppRGCTXDataType)3, 11080 },
	{ (Il2CppRGCTXDataType)3, 10972 },
	{ (Il2CppRGCTXDataType)3, 11055 },
	{ (Il2CppRGCTXDataType)3, 11096 },
	{ (Il2CppRGCTXDataType)2, 2988 },
	{ (Il2CppRGCTXDataType)2, 3854 },
	{ (Il2CppRGCTXDataType)3, 11051 },
	{ (Il2CppRGCTXDataType)3, 11081 },
	{ (Il2CppRGCTXDataType)1, 586 },
	{ (Il2CppRGCTXDataType)2, 2992 },
	{ (Il2CppRGCTXDataType)3, 7142 },
	{ (Il2CppRGCTXDataType)3, 7408 },
	{ (Il2CppRGCTXDataType)2, 2693 },
	{ (Il2CppRGCTXDataType)3, 6361 },
	{ (Il2CppRGCTXDataType)3, 616 },
	{ (Il2CppRGCTXDataType)3, 617 },
	{ (Il2CppRGCTXDataType)2, 841 },
	{ (Il2CppRGCTXDataType)3, 615 },
	{ (Il2CppRGCTXDataType)2, 3797 },
	{ (Il2CppRGCTXDataType)2, 2987 },
	{ (Il2CppRGCTXDataType)3, 614 },
	{ (Il2CppRGCTXDataType)2, 792 },
	{ (Il2CppRGCTXDataType)3, 456 },
	{ (Il2CppRGCTXDataType)3, 10971 },
	{ (Il2CppRGCTXDataType)1, 210 },
	{ (Il2CppRGCTXDataType)3, 11095 },
	{ (Il2CppRGCTXDataType)3, 11087 },
	{ (Il2CppRGCTXDataType)3, 10970 },
	{ (Il2CppRGCTXDataType)1, 209 },
	{ (Il2CppRGCTXDataType)3, 11094 },
	{ (Il2CppRGCTXDataType)3, 11102 },
	{ (Il2CppRGCTXDataType)3, 8206 },
	{ (Il2CppRGCTXDataType)3, 8205 },
	{ (Il2CppRGCTXDataType)3, 11074 },
	{ (Il2CppRGCTXDataType)3, 11038 },
	{ (Il2CppRGCTXDataType)3, 7546 },
	{ (Il2CppRGCTXDataType)3, 7545 },
	{ (Il2CppRGCTXDataType)3, 11073 },
	{ (Il2CppRGCTXDataType)3, 11037 },
	{ (Il2CppRGCTXDataType)3, 7420 },
	{ (Il2CppRGCTXDataType)3, 7556 },
	{ (Il2CppRGCTXDataType)3, 7419 },
	{ (Il2CppRGCTXDataType)2, 566 },
	{ (Il2CppRGCTXDataType)2, 793 },
	{ (Il2CppRGCTXDataType)3, 457 },
	{ (Il2CppRGCTXDataType)3, 458 },
	{ (Il2CppRGCTXDataType)3, 7483 },
	{ (Il2CppRGCTXDataType)3, 8201 },
	{ (Il2CppRGCTXDataType)3, 7482 },
	{ (Il2CppRGCTXDataType)3, 7481 },
	{ (Il2CppRGCTXDataType)3, 7406 },
	{ (Il2CppRGCTXDataType)3, 7541 },
	{ (Il2CppRGCTXDataType)3, 10143 },
	{ (Il2CppRGCTXDataType)3, 7485 },
	{ (Il2CppRGCTXDataType)3, 7484 },
	{ (Il2CppRGCTXDataType)3, 7407 },
	{ (Il2CppRGCTXDataType)3, 7542 },
	{ (Il2CppRGCTXDataType)3, 7543 },
	{ (Il2CppRGCTXDataType)3, 8202 },
	{ (Il2CppRGCTXDataType)3, 7486 },
	{ (Il2CppRGCTXDataType)2, 3786 },
	{ (Il2CppRGCTXDataType)3, 8203 },
	{ (Il2CppRGCTXDataType)2, 3253 },
	{ (Il2CppRGCTXDataType)3, 10140 },
	{ (Il2CppRGCTXDataType)3, 7490 },
	{ (Il2CppRGCTXDataType)3, 7488 },
	{ (Il2CppRGCTXDataType)1, 463 },
	{ (Il2CppRGCTXDataType)3, 11062 },
	{ (Il2CppRGCTXDataType)3, 7493 },
	{ (Il2CppRGCTXDataType)2, 1050 },
	{ (Il2CppRGCTXDataType)3, 2453 },
	{ (Il2CppRGCTXDataType)3, 7492 },
	{ (Il2CppRGCTXDataType)3, 7491 },
	{ (Il2CppRGCTXDataType)3, 7489 },
	{ (Il2CppRGCTXDataType)2, 3066 },
	{ (Il2CppRGCTXDataType)2, 3062 },
	{ (Il2CppRGCTXDataType)3, 7470 },
	{ (Il2CppRGCTXDataType)3, 7469 },
	{ (Il2CppRGCTXDataType)3, 7417 },
	{ (Il2CppRGCTXDataType)3, 7418 },
	{ (Il2CppRGCTXDataType)2, 3837 },
	{ (Il2CppRGCTXDataType)2, 3051 },
	{ (Il2CppRGCTXDataType)3, 7416 },
	{ (Il2CppRGCTXDataType)2, 2696 },
	{ (Il2CppRGCTXDataType)3, 6365 },
	{ (Il2CppRGCTXDataType)3, 6382 },
	{ (Il2CppRGCTXDataType)3, 6383 },
	{ (Il2CppRGCTXDataType)2, 2701 },
	{ (Il2CppRGCTXDataType)3, 7471 },
	{ (Il2CppRGCTXDataType)2, 2989 },
	{ (Il2CppRGCTXDataType)2, 3066 },
	{ (Il2CppRGCTXDataType)3, 7487 },
	{ (Il2CppRGCTXDataType)3, 7495 },
	{ (Il2CppRGCTXDataType)3, 7494 },
	{ (Il2CppRGCTXDataType)3, 6362 },
	{ (Il2CppRGCTXDataType)3, 8207 },
	{ (Il2CppRGCTXDataType)2, 2698 },
	{ (Il2CppRGCTXDataType)3, 6374 },
};
extern const CustomAttributesCacheGenerator g_System_Memory_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_System_Memory_CodeGenModule;
const Il2CppCodeGenModule g_System_Memory_CodeGenModule = 
{
	"System.Memory.dll",
	169,
	s_methodPointers,
	8,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	26,
	s_rgctxIndices,
	198,
	s_rgctxValues,
	NULL,
	g_System_Memory_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
