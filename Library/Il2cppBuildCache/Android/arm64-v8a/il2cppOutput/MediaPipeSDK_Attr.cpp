﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.String[]
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// System.Runtime.CompilerServices.AsyncStateMachineAttribute
struct AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88;
// System.Diagnostics.DebuggerNonUserCodeAttribute
struct DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41;
// Microsoft.CodeAnalysis.EmbeddedAttribute
struct EmbeddedAttribute_t152A226BB4E86AF918B0AAB3CEA83EFB90DE388E;
// System.Runtime.CompilerServices.ExtensionAttribute
struct ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// AOT.MonoPInvokeCallbackAttribute
struct MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E;
// System.ObsoleteAttribute
struct ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671;
// Google.Protobuf.Reflection.OriginalNameAttribute
struct OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25;
// System.String
struct String_t;
// System.Security.SuppressUnmanagedCodeSecurityAttribute
struct SuppressUnmanagedCodeSecurityAttribute_tFFA93751D33F94038F06E0204BDFCDF8D31106F7;
// System.Runtime.CompilerServices.TupleElementNamesAttribute
struct TupleElementNamesAttribute_tA4BB7E54E3D9A06A7EA4334EC48A0BFC809F65FD;
// System.Type
struct Type_t;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;

IL2CPP_EXTERN_C RuntimeClass* StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C const RuntimeType* Deleter_t4F908C9E2012DDE32B43B1A00D9E16C571D593E2_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* PathResolver_t704DEBCFBC148702F09DD0F438E150616748594A_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* ProtobufLogHandler_t0F21059AE06D2D395278269AEAA66A5F51B9FB16_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* ResourceProvider_t729C6AE767A25D3EF21A51FC68F578F164DC6FCF_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CLoadAssetBundleAsyncU3Ed__21_t580563DF049EEAABF939C11D81845B846D3B687D_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CPrepareAssetAsyncU3Ed__23_tE0D0088355DC75E2373DA3E87446BEC021802D5A_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CPrepareAssetAsyncU3Ed__9_t6EC072023AF74799B02659540A1E75A0260F55C6_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CWriteCacheFileAsyncU3Ed__28_tAD75A60D5B46B633653D85972A00F53DAF9D62C1_0_0_0_var;

struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Diagnostics.DebuggerNonUserCodeAttribute
struct DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// Microsoft.CodeAnalysis.EmbeddedAttribute
struct EmbeddedAttribute_t152A226BB4E86AF918B0AAB3CEA83EFB90DE388E  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Runtime.CompilerServices.ExtensionAttribute
struct ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// AOT.MonoPInvokeCallbackAttribute
struct MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.ObsoleteAttribute
struct ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.ObsoleteAttribute::_message
	String_t* ____message_0;
	// System.Boolean System.ObsoleteAttribute::_error
	bool ____error_1;

public:
	inline static int32_t get_offset_of__message_0() { return static_cast<int32_t>(offsetof(ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671, ____message_0)); }
	inline String_t* get__message_0() const { return ____message_0; }
	inline String_t** get_address_of__message_0() { return &____message_0; }
	inline void set__message_0(String_t* value)
	{
		____message_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_0), (void*)value);
	}

	inline static int32_t get_offset_of__error_1() { return static_cast<int32_t>(offsetof(ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671, ____error_1)); }
	inline bool get__error_1() const { return ____error_1; }
	inline bool* get_address_of__error_1() { return &____error_1; }
	inline void set__error_1(bool value)
	{
		____error_1 = value;
	}
};


// Google.Protobuf.Reflection.OriginalNameAttribute
struct OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String Google.Protobuf.Reflection.OriginalNameAttribute::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;
	// System.Boolean Google.Protobuf.Reflection.OriginalNameAttribute::<PreferredAlias>k__BackingField
	bool ___U3CPreferredAliasU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664, ___U3CNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CNameU3Ek__BackingField_0() const { return ___U3CNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_0() { return &___U3CNameU3Ek__BackingField_0; }
	inline void set_U3CNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CNameU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CPreferredAliasU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664, ___U3CPreferredAliasU3Ek__BackingField_1)); }
	inline bool get_U3CPreferredAliasU3Ek__BackingField_1() const { return ___U3CPreferredAliasU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CPreferredAliasU3Ek__BackingField_1() { return &___U3CPreferredAliasU3Ek__BackingField_1; }
	inline void set_U3CPreferredAliasU3Ek__BackingField_1(bool value)
	{
		___U3CPreferredAliasU3Ek__BackingField_1 = value;
	}
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.StateMachineAttribute
struct StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type System.Runtime.CompilerServices.StateMachineAttribute::<StateMachineType>k__BackingField
	Type_t * ___U3CStateMachineTypeU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CStateMachineTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3, ___U3CStateMachineTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CStateMachineTypeU3Ek__BackingField_0() const { return ___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CStateMachineTypeU3Ek__BackingField_0() { return &___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline void set_U3CStateMachineTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CStateMachineTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CStateMachineTypeU3Ek__BackingField_0), (void*)value);
	}
};


// System.Security.SuppressUnmanagedCodeSecurityAttribute
struct SuppressUnmanagedCodeSecurityAttribute_tFFA93751D33F94038F06E0204BDFCDF8D31106F7  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.TupleElementNamesAttribute
struct TupleElementNamesAttribute_tA4BB7E54E3D9A06A7EA4334EC48A0BFC809F65FD  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String[] System.Runtime.CompilerServices.TupleElementNamesAttribute::_transformNames
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ____transformNames_0;

public:
	inline static int32_t get_offset_of__transformNames_0() { return static_cast<int32_t>(offsetof(TupleElementNamesAttribute_tA4BB7E54E3D9A06A7EA4334EC48A0BFC809F65FD, ____transformNames_0)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get__transformNames_0() const { return ____transformNames_0; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of__transformNames_0() { return &____transformNames_0; }
	inline void set__transformNames_0(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		____transformNames_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____transformNames_0), (void*)value);
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Runtime.CompilerServices.AsyncStateMachineAttribute
struct AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6  : public StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3
{
public:

public:
};


// System.Reflection.BindingFlags
struct BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.RuntimeTypeHandle
struct RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};


// System.Type
struct Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.String[]
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};



// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.ExtensionAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * __this, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
// System.Void Microsoft.CodeAnalysis.EmbeddedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EmbeddedAttribute__ctor_mFD149DC63111CEA4726998614A96442713AD600F (EmbeddedAttribute_t152A226BB4E86AF918B0AAB3CEA83EFB90DE388E * __this, const RuntimeMethod* method);
// System.Void UnityEngine.SerializeField::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3 (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * __this, const RuntimeMethod* method);
// System.Void AOT.MonoPInvokeCallbackAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoPInvokeCallbackAttribute__ctor_mA23FC56D16F76D90C3F76B1C41D0765BB21FE344 (MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E * __this, Type_t * ___type0, const RuntimeMethod* method);
// System.Void System.ObsoleteAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ObsoleteAttribute__ctor_mAC32A5CCD287DA84CDA9F08282C1C8B0DB7B9868 (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * __this, String_t* ___message0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.TupleElementNamesAttribute::.ctor(System.String[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TupleElementNamesAttribute__ctor_mAA971B5CB6FAE0690C06558CA92983ABC486068E (TupleElementNamesAttribute_tA4BB7E54E3D9A06A7EA4334EC48A0BFC809F65FD * __this, StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___transformNames0, const RuntimeMethod* method);
// System.Void System.Security.SuppressUnmanagedCodeSecurityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SuppressUnmanagedCodeSecurityAttribute__ctor_m5A705F8D9C893C43A992ABF6F0FEA9159EEBC253 (SuppressUnmanagedCodeSecurityAttribute_tFFA93751D33F94038F06E0204BDFCDF8D31106F7 * __this, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerNonUserCodeAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * __this, const RuntimeMethod* method);
// System.Void Google.Protobuf.Reflection.OriginalNameAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OriginalNameAttribute__ctor_mB0C6C38C49B9261D2DC423D692E80B8EBC657BF9 (OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * __this, String_t* ___name0, const RuntimeMethod* method);
// System.Void System.ObsoleteAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ObsoleteAttribute__ctor_m9BC17A80675E9013AA71F9FB38D89FEF56883853 (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.AsyncStateMachineAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * __this, Type_t * ___stateMachineType0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerHiddenAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3 (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * __this, const RuntimeMethod* method);
static void MediaPipeSDK_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[0];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[1];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[2];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[3];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 2LL, NULL);
	}
}
static void EmbeddedAttribute_t152A226BB4E86AF918B0AAB3CEA83EFB90DE388E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		EmbeddedAttribute_t152A226BB4E86AF918B0AAB3CEA83EFB90DE388E * tmp = (EmbeddedAttribute_t152A226BB4E86AF918B0AAB3CEA83EFB90DE388E *)cache->attributes[1];
		EmbeddedAttribute__ctor_mFD149DC63111CEA4726998614A96442713AD600F(tmp, NULL);
	}
}
static void IsUnmanagedAttribute_tCC30ED83A257B813A2D28B603A345F0CB5F04BD9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		EmbeddedAttribute_t152A226BB4E86AF918B0AAB3CEA83EFB90DE388E * tmp = (EmbeddedAttribute_t152A226BB4E86AF918B0AAB3CEA83EFB90DE388E *)cache->attributes[0];
		EmbeddedAttribute__ctor_mFD149DC63111CEA4726998614A96442713AD600F(tmp, NULL);
	}
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CircleAnnotationController_tA7923999378E3E1A95DF49FEBF82EBB07219AE54_CustomAttributesCacheGenerator_PositionSize(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DetectionAnnotationController_tE65DA01B36F319F72CEA4AA803EDBCB20C838EA2_CustomAttributesCacheGenerator_relativeKeypointPrefab(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LandmarkListAnnotationController_t5DFFA52714162A66D82D024FD4894E8407E51BC6_CustomAttributesCacheGenerator_nodePrefab(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LandmarkListAnnotationController_t5DFFA52714162A66D82D024FD4894E8407E51BC6_CustomAttributesCacheGenerator_edgePrefab(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LandmarkListAnnotationController_t5DFFA52714162A66D82D024FD4894E8407E51BC6_CustomAttributesCacheGenerator_NodeScale(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void U3CU3Ec_t2BC66AEDA6FFAE79441F6EF68ADDDCB204959396_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ListAnnotationController_1_t28493CB41E2E27D515F31137D65D48A80F4A9D27_CustomAttributesCacheGenerator_annotationPrefab(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ListAnnotationController_1_t28493CB41E2E27D515F31137D65D48A80F4A9D27_CustomAttributesCacheGenerator_DefaultMaxSize(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ListAnnotationController_1_t28493CB41E2E27D515F31137D65D48A80F4A9D27_CustomAttributesCacheGenerator_U3CMaxSizeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ListAnnotationController_1_t28493CB41E2E27D515F31137D65D48A80F4A9D27_CustomAttributesCacheGenerator_ListAnnotationController_1_get_MaxSize_mAC2AD4B6BCBE8DBE5E0126FFB2159C6332D3024E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ListAnnotationController_1_t28493CB41E2E27D515F31137D65D48A80F4A9D27_CustomAttributesCacheGenerator_ListAnnotationController_1_set_MaxSize_m51FD63C4AA84FE9EDD55A66C6FCD8F89B662D115(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DisposableObject_tEF59059AABEEFCC912FE2A5C8BE0CA5D1AE5F09D_CustomAttributesCacheGenerator_U3CisDisposedU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DisposableObject_tEF59059AABEEFCC912FE2A5C8BE0CA5D1AE5F09D_CustomAttributesCacheGenerator_U3CisOwnerU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DisposableObject_tEF59059AABEEFCC912FE2A5C8BE0CA5D1AE5F09D_CustomAttributesCacheGenerator_DisposableObject_get_isDisposed_mBAC3411868F2F3866C93C0FD29569C5C6A589E4E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DisposableObject_tEF59059AABEEFCC912FE2A5C8BE0CA5D1AE5F09D_CustomAttributesCacheGenerator_DisposableObject_set_isDisposed_m4810231F19F321DDBB7ACEB59D310DA3AF8689FA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DisposableObject_tEF59059AABEEFCC912FE2A5C8BE0CA5D1AE5F09D_CustomAttributesCacheGenerator_DisposableObject_get_isOwner_m05D0D142AD0928EFB4EBF5C9BE83A8C4D003B7C5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DisposableObject_tEF59059AABEEFCC912FE2A5C8BE0CA5D1AE5F09D_CustomAttributesCacheGenerator_DisposableObject_set_isOwner_m14F37B0D67E76791FAD35719A01F7924A796DE42(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Protobuf_t8CE0334167D95EA75FFD886A6585A57E9E73A585_CustomAttributesCacheGenerator_Protobuf_LogProtobufMessage_m1B59C21023D21E2AB5CA458EC71B9F43DA4E587D(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ProtobufLogHandler_t0F21059AE06D2D395278269AEAA66A5F51B9FB16_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E * tmp = (MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E *)cache->attributes[0];
		MonoPInvokeCallbackAttribute__ctor_mA23FC56D16F76D90C3F76B1C41D0765BB21FE344(tmp, il2cpp_codegen_type_get_object(ProtobufLogHandler_t0F21059AE06D2D395278269AEAA66A5F51B9FB16_0_0_0_var), NULL);
	}
}
static void Format_tF31D9B5F4DBE60F77A8FA0B17EEADF7CB2872FBA_CustomAttributesCacheGenerator_Format_FromPixels32_mBA75BC96B4A07BD34189075F273B236DEC47F7D6(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * tmp = (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 *)cache->attributes[0];
		ObsoleteAttribute__ctor_mAC32A5CCD287DA84CDA9F08282C1C8B0DB7B9868(tmp, il2cpp_codegen_string_new_wrapper("\x46\x72\x6F\x6D\x50\x69\x78\x65\x6C\x73\x33\x32\x20\x69\x73\x20\x64\x65\x70\x72\x65\x63\x61\x74\x65\x64\x2C\x20\x75\x73\x65\x20\x54\x65\x78\x74\x75\x72\x65\x32\x44\x23\x47\x65\x74\x52\x61\x77\x4E\x61\x74\x69\x76\x65\x44\x61\x74\x61"), NULL);
	}
}
static void Format_tF31D9B5F4DBE60F77A8FA0B17EEADF7CB2872FBA_CustomAttributesCacheGenerator_Format_FromBytePtr_m241B5F9B3CA63A791E94B49E80FD4EAE6453619A(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * tmp = (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 *)cache->attributes[0];
		ObsoleteAttribute__ctor_mAC32A5CCD287DA84CDA9F08282C1C8B0DB7B9868(tmp, il2cpp_codegen_string_new_wrapper("\x46\x72\x6F\x6D\x42\x79\x74\x65\x50\x74\x72\x20\x69\x73\x20\x64\x65\x70\x72\x65\x63\x61\x74\x65\x64\x2C\x20\x75\x73\x65\x20\x54\x65\x78\x74\x75\x72\x65\x32\x44\x23\x4C\x6F\x61\x64\x52\x61\x77\x54\x65\x78\x74\x75\x72\x65\x44\x61\x74\x61"), NULL);
	}
}
static void U3CU3Ec__DisplayClass11_0_2_t0505406910875832174BED162EC70EE7FA4B580A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CalculatorGraphConfigExtension_t9558861CC274C820B31F5D69FAEE6E0ACE45B6A3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void CalculatorGraphConfigExtension_t9558861CC274C820B31F5D69FAEE6E0ACE45B6A3_CustomAttributesCacheGenerator_CalculatorGraphConfigExtension_ParseFromTextFormat_mC08DFC40A3661668ABA0670D41E52A2DD78F74D6(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ImageFrame_t8089742C09B5D80723C4FF7BEF27781AE14A450D_CustomAttributesCacheGenerator_ImageFrame_ReleasePixelData_mDF81706134475903E835D1F61193281D8FB96A78(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Deleter_t4F908C9E2012DDE32B43B1A00D9E16C571D593E2_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E * tmp = (MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E *)cache->attributes[0];
		MonoPInvokeCallbackAttribute__ctor_mA23FC56D16F76D90C3F76B1C41D0765BB21FE344(tmp, il2cpp_codegen_type_get_object(Deleter_t4F908C9E2012DDE32B43B1A00D9E16C571D593E2_0_0_0_var), NULL);
	}
}
static void ImageFrame_t8089742C09B5D80723C4FF7BEF27781AE14A450D_CustomAttributesCacheGenerator_ImageFrame_GetColor32s_mD2096829CCF07C9F53B7A33C849225B33E3B56F3(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * tmp = (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 *)cache->attributes[0];
		ObsoleteAttribute__ctor_mAC32A5CCD287DA84CDA9F08282C1C8B0DB7B9868(tmp, il2cpp_codegen_string_new_wrapper("\x47\x65\x74\x43\x6F\x6C\x6F\x72\x33\x32\x73\x28\x29\x20\x69\x73\x20\x64\x65\x70\x72\x65\x63\x61\x74\x65\x64"), NULL);
	}
}
static void ImageFrame_t8089742C09B5D80723C4FF7BEF27781AE14A450D_CustomAttributesCacheGenerator_ImageFrame_FromPixels32_m9120673B8A0650BD04B61D67A06C46308ED5F1C8(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * tmp = (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 *)cache->attributes[0];
		ObsoleteAttribute__ctor_mAC32A5CCD287DA84CDA9F08282C1C8B0DB7B9868(tmp, il2cpp_codegen_string_new_wrapper("\x46\x72\x6F\x6D\x50\x69\x78\x65\x6C\x73\x33\x32\x28\x29\x20\x69\x73\x20\x64\x65\x70\x72\x65\x63\x61\x74\x65\x64"), NULL);
	}
}
static void Status_t08160DCBEC7A0170340AC75F91703B1E1EAC1267_CustomAttributesCacheGenerator_Status_IsOk_m1AB7D6193A62F662ECA6115FE48501605923C4BF(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * tmp = (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 *)cache->attributes[0];
		ObsoleteAttribute__ctor_mAC32A5CCD287DA84CDA9F08282C1C8B0DB7B9868(tmp, il2cpp_codegen_string_new_wrapper("\x49\x73\x4F\x6B\x28\x29\x20\x69\x73\x20\x64\x65\x70\x72\x65\x63\x61\x74\x65\x64\x2C\x20\x75\x73\x65\x20\x6F\x6B"), NULL);
	}
}
static void Status_t08160DCBEC7A0170340AC75F91703B1E1EAC1267_CustomAttributesCacheGenerator_Status_GetPtr_mB5ACB15D7C5332303206547CF6AF7E1AFF3DC267(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * tmp = (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 *)cache->attributes[0];
		ObsoleteAttribute__ctor_mAC32A5CCD287DA84CDA9F08282C1C8B0DB7B9868(tmp, il2cpp_codegen_string_new_wrapper("\x47\x65\x74\x50\x74\x72\x20\x69\x73\x20\x64\x65\x70\x72\x65\x63\x61\x74\x65\x64\x2C\x20\x75\x73\x65\x20\x6D\x70\x50\x74\x72"), NULL);
	}
}
static void U3CU3Ec_t3C5228983E3519B9CCEEE7A8822E6205037E999A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_t3C5228983E3519B9CCEEE7A8822E6205037E999A_CustomAttributesCacheGenerator_U3CU3E9__2_0(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* _tmp_0 = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, 2);
		(_tmp_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), NULL);
		(_tmp_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), il2cpp_codegen_string_new_wrapper("\x69"));
		TupleElementNamesAttribute_tA4BB7E54E3D9A06A7EA4334EC48A0BFC809F65FD * tmp = (TupleElementNamesAttribute_tA4BB7E54E3D9A06A7EA4334EC48A0BFC809F65FD *)cache->attributes[0];
		TupleElementNamesAttribute__ctor_mAA971B5CB6FAE0690C06558CA92983ABC486068E(tmp, _tmp_0, NULL);
	}
}
static void U3CU3Ec__DisplayClass2_0_t82048CC1732D57D29FE279E3CAF06974BD411658_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass2_0_t82048CC1732D57D29FE279E3CAF06974BD411658_CustomAttributesCacheGenerator_U3CU3Ec__DisplayClass2_0_U3CCanonicalNodeNameU3Eb__1_mFF877148EC50CBF017DFA3AC2C7B076B8592640C____pair0(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* _tmp_0 = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, 2);
		(_tmp_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), NULL);
		(_tmp_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), il2cpp_codegen_string_new_wrapper("\x69"));
		TupleElementNamesAttribute_tA4BB7E54E3D9A06A7EA4334EC48A0BFC809F65FD * tmp = (TupleElementNamesAttribute_tA4BB7E54E3D9A06A7EA4334EC48A0BFC809F65FD *)cache->attributes[0];
		TupleElementNamesAttribute__ctor_mAA971B5CB6FAE0690C06558CA92983ABC486068E(tmp, _tmp_0, NULL);
	}
}
static void U3CU3Ec__DisplayClass2_0_t82048CC1732D57D29FE279E3CAF06974BD411658_CustomAttributesCacheGenerator_U3CU3Ec__DisplayClass2_0_U3CCanonicalNodeNameU3Eb__2_m3D71E7837D418B4AE8BED40F3FBF79EEB1090097____pair0(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* _tmp_0 = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, 2);
		(_tmp_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), NULL);
		(_tmp_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), il2cpp_codegen_string_new_wrapper("\x69"));
		TupleElementNamesAttribute_tA4BB7E54E3D9A06A7EA4334EC48A0BFC809F65FD * tmp = (TupleElementNamesAttribute_tA4BB7E54E3D9A06A7EA4334EC48A0BFC809F65FD *)cache->attributes[0];
		TupleElementNamesAttribute__ctor_mAA971B5CB6FAE0690C06558CA92983ABC486068E(tmp, _tmp_0, NULL);
	}
}
static void U3CU3Ec__DisplayClass6_0_tBBF7376F93226F24C90D9B6763B3512BA708B4C4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GpuBufferFormatExtension_t6AC277E4C201DDBC6B17F4E43B1DBF3C5B3E863C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void GpuBufferFormatExtension_t6AC277E4C201DDBC6B17F4E43B1DBF3C5B3E863C_CustomAttributesCacheGenerator_GpuBufferFormatExtension_ImageFormatFor_m0DC71BAD40318374571505A1C1A3DA9009A74CE1(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void GpuBufferFormatExtension_t6AC277E4C201DDBC6B17F4E43B1DBF3C5B3E863C_CustomAttributesCacheGenerator_GpuBufferFormatExtension_GlTextureInfoFor_mF1C0CB8FEABA534B7FECD9FF2A531F6B027F2FFF(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void MpReturnCodeExtension_t303D50EB4156CB6B1A7C14DA771FA5F563F2D81C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void MpReturnCodeExtension_t303D50EB4156CB6B1A7C14DA771FA5F563F2D81C_CustomAttributesCacheGenerator_MpReturnCodeExtension_Assert_mD720CAA4396143E0113F15BBE369066AB04044EC(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void SafeNativeMethods_t8AAF6F1747D3D43EA2E96E312112591EE6944018_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		SuppressUnmanagedCodeSecurityAttribute_tFFA93751D33F94038F06E0204BDFCDF8D31106F7 * tmp = (SuppressUnmanagedCodeSecurityAttribute_tFFA93751D33F94038F06E0204BDFCDF8D31106F7 *)cache->attributes[0];
		SuppressUnmanagedCodeSecurityAttribute__ctor_m5A705F8D9C893C43A992ABF6F0FEA9159EEBC253(tmp, NULL);
	}
}
static void UnsafeNativeMethods_t5286816A1FB919D621EEB37AD9EC79D205B1E44D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		SuppressUnmanagedCodeSecurityAttribute_tFFA93751D33F94038F06E0204BDFCDF8D31106F7 * tmp = (SuppressUnmanagedCodeSecurityAttribute_tFFA93751D33F94038F06E0204BDFCDF8D31106F7 *)cache->attributes[0];
		SuppressUnmanagedCodeSecurityAttribute__ctor_m5A705F8D9C893C43A992ABF6F0FEA9159EEBC253(tmp, NULL);
	}
}
static void ExecutorConfig_t2755CF63D93D5DC1B5534655FCC554960354EE7F_CustomAttributesCacheGenerator_ExecutorConfig__ctor_m3100C42DF28C84401E6BEF2F84829F2EBBC6A79C(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ExecutorConfig_t2755CF63D93D5DC1B5534655FCC554960354EE7F_CustomAttributesCacheGenerator_ExecutorConfig__ctor_mD8F55C2743A66A7A21FF05F95AC46741582F3F16(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ExecutorConfig_t2755CF63D93D5DC1B5534655FCC554960354EE7F_CustomAttributesCacheGenerator_ExecutorConfig_Clone_mBCF44B0A1985F000BBEC12B6FF68B32D57D3AB29(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ExecutorConfig_t2755CF63D93D5DC1B5534655FCC554960354EE7F_CustomAttributesCacheGenerator_ExecutorConfig_Equals_m3951D8E5FFF9897153DCBB93E5DE556807BBE51D(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ExecutorConfig_t2755CF63D93D5DC1B5534655FCC554960354EE7F_CustomAttributesCacheGenerator_ExecutorConfig_Equals_mFD6B3AC46F2086E4F1F526630E167EC0C8543318(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ExecutorConfig_t2755CF63D93D5DC1B5534655FCC554960354EE7F_CustomAttributesCacheGenerator_ExecutorConfig_GetHashCode_m7BDD5B881BD685E1ED277BBBCFD80C4986FFE164(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ExecutorConfig_t2755CF63D93D5DC1B5534655FCC554960354EE7F_CustomAttributesCacheGenerator_ExecutorConfig_ToString_mF0A8963B6D15984AFB1E8BFF76FABB4F9F173917(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ExecutorConfig_t2755CF63D93D5DC1B5534655FCC554960354EE7F_CustomAttributesCacheGenerator_ExecutorConfig_WriteTo_mD1274E15A379E9C0DD0C28CE7EB63EAAA74C67D4(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ExecutorConfig_t2755CF63D93D5DC1B5534655FCC554960354EE7F_CustomAttributesCacheGenerator_ExecutorConfig_CalculateSize_mA55EE68840B8DD1CC6F4AE0974D39CFC0AA61E9C(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ExecutorConfig_t2755CF63D93D5DC1B5534655FCC554960354EE7F_CustomAttributesCacheGenerator_ExecutorConfig_MergeFrom_m5C5312A118CD794BA85F9895A164F3B9D27864D8(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ExecutorConfig_t2755CF63D93D5DC1B5534655FCC554960354EE7F_CustomAttributesCacheGenerator_ExecutorConfig_MergeFrom_mB9D3A9DC69128998567149232D6167740C1D86B0(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ExecutorConfig_t2755CF63D93D5DC1B5534655FCC554960354EE7F_CustomAttributesCacheGenerator_ExecutorConfig_t2755CF63D93D5DC1B5534655FCC554960354EE7F____Parser_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ExecutorConfig_t2755CF63D93D5DC1B5534655FCC554960354EE7F_CustomAttributesCacheGenerator_ExecutorConfig_t2755CF63D93D5DC1B5534655FCC554960354EE7F____Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ExecutorConfig_t2755CF63D93D5DC1B5534655FCC554960354EE7F_CustomAttributesCacheGenerator_ExecutorConfig_t2755CF63D93D5DC1B5534655FCC554960354EE7F____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ExecutorConfig_t2755CF63D93D5DC1B5534655FCC554960354EE7F_CustomAttributesCacheGenerator_ExecutorConfig_t2755CF63D93D5DC1B5534655FCC554960354EE7F____Name_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ExecutorConfig_t2755CF63D93D5DC1B5534655FCC554960354EE7F_CustomAttributesCacheGenerator_ExecutorConfig_t2755CF63D93D5DC1B5534655FCC554960354EE7F____Type_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ExecutorConfig_t2755CF63D93D5DC1B5534655FCC554960354EE7F_CustomAttributesCacheGenerator_ExecutorConfig_t2755CF63D93D5DC1B5534655FCC554960354EE7F____Options_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void U3CU3Ec_t1107DB4858A6272FD61890EF04733CE45CDFCC56_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InputCollection_tA4C4583EB805BFBA48936774F9E2EF02712021D2_CustomAttributesCacheGenerator_InputCollection__ctor_m6C6B1C4710314CCF5B8B7924C90E035C6FAB2DD8(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void InputCollection_tA4C4583EB805BFBA48936774F9E2EF02712021D2_CustomAttributesCacheGenerator_InputCollection__ctor_m0D27217EBD9D8F66693BFD6643F490520429AA4D(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void InputCollection_tA4C4583EB805BFBA48936774F9E2EF02712021D2_CustomAttributesCacheGenerator_InputCollection_Clone_m1B45CE9CCB12659381144DB30162D6975759F471(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void InputCollection_tA4C4583EB805BFBA48936774F9E2EF02712021D2_CustomAttributesCacheGenerator_InputCollection_Equals_mFED987F83BABE5D3B89DDA35313DD18A2746CEBB(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void InputCollection_tA4C4583EB805BFBA48936774F9E2EF02712021D2_CustomAttributesCacheGenerator_InputCollection_Equals_m240639A5CB435A8E02FD15D41B30E9AFB7C9D2FB(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void InputCollection_tA4C4583EB805BFBA48936774F9E2EF02712021D2_CustomAttributesCacheGenerator_InputCollection_GetHashCode_mAECDBE9B9032BA48A73EC8D59C4ED5F1D473E5B7(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void InputCollection_tA4C4583EB805BFBA48936774F9E2EF02712021D2_CustomAttributesCacheGenerator_InputCollection_ToString_m7FA0413087B5A0AD62F527DD3F15ECDCB9981DEF(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void InputCollection_tA4C4583EB805BFBA48936774F9E2EF02712021D2_CustomAttributesCacheGenerator_InputCollection_WriteTo_m606B5E61E0DD7AC8D05F5C64A3EDD68625ACA63C(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void InputCollection_tA4C4583EB805BFBA48936774F9E2EF02712021D2_CustomAttributesCacheGenerator_InputCollection_CalculateSize_mB74CC608EB1D546473A2A7D93F6037CFA54E60FD(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void InputCollection_tA4C4583EB805BFBA48936774F9E2EF02712021D2_CustomAttributesCacheGenerator_InputCollection_MergeFrom_mD3C06AAE9303FC3CD361361748B7CD0BE5C46D38(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void InputCollection_tA4C4583EB805BFBA48936774F9E2EF02712021D2_CustomAttributesCacheGenerator_InputCollection_MergeFrom_mFADA258A81187638466BFEF0AFC6C8351A94374B(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void InputCollection_tA4C4583EB805BFBA48936774F9E2EF02712021D2_CustomAttributesCacheGenerator_InputCollection_tA4C4583EB805BFBA48936774F9E2EF02712021D2____Parser_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void InputCollection_tA4C4583EB805BFBA48936774F9E2EF02712021D2_CustomAttributesCacheGenerator_InputCollection_tA4C4583EB805BFBA48936774F9E2EF02712021D2____Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void InputCollection_tA4C4583EB805BFBA48936774F9E2EF02712021D2_CustomAttributesCacheGenerator_InputCollection_tA4C4583EB805BFBA48936774F9E2EF02712021D2____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void InputCollection_tA4C4583EB805BFBA48936774F9E2EF02712021D2_CustomAttributesCacheGenerator_InputCollection_tA4C4583EB805BFBA48936774F9E2EF02712021D2____Name_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void InputCollection_tA4C4583EB805BFBA48936774F9E2EF02712021D2_CustomAttributesCacheGenerator_InputCollection_tA4C4583EB805BFBA48936774F9E2EF02712021D2____SidePacketName_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void InputCollection_tA4C4583EB805BFBA48936774F9E2EF02712021D2_CustomAttributesCacheGenerator_InputCollection_tA4C4583EB805BFBA48936774F9E2EF02712021D2____ExternalInputName_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void InputCollection_tA4C4583EB805BFBA48936774F9E2EF02712021D2_CustomAttributesCacheGenerator_InputCollection_tA4C4583EB805BFBA48936774F9E2EF02712021D2____InputType_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void InputCollection_tA4C4583EB805BFBA48936774F9E2EF02712021D2_CustomAttributesCacheGenerator_InputCollection_tA4C4583EB805BFBA48936774F9E2EF02712021D2____FileName_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Types_t17F11D4BF4592BCA38E72F4CC148526F3CE91E82_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void InputType_t06CD6E3FD08548CD0BB0B2462CED0C0A11F1E9AE_CustomAttributesCacheGenerator_Unknown(CustomAttributesCache* cache)
{
	{
		OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * tmp = (OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 *)cache->attributes[0];
		OriginalNameAttribute__ctor_mB0C6C38C49B9261D2DC423D692E80B8EBC657BF9(tmp, il2cpp_codegen_string_new_wrapper("\x55\x4E\x4B\x4E\x4F\x57\x4E"), NULL);
	}
}
static void InputType_t06CD6E3FD08548CD0BB0B2462CED0C0A11F1E9AE_CustomAttributesCacheGenerator_Recordio(CustomAttributesCache* cache)
{
	{
		OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * tmp = (OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 *)cache->attributes[0];
		OriginalNameAttribute__ctor_mB0C6C38C49B9261D2DC423D692E80B8EBC657BF9(tmp, il2cpp_codegen_string_new_wrapper("\x52\x45\x43\x4F\x52\x44\x49\x4F"), NULL);
	}
}
static void InputType_t06CD6E3FD08548CD0BB0B2462CED0C0A11F1E9AE_CustomAttributesCacheGenerator_ForeignRecordio(CustomAttributesCache* cache)
{
	{
		OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * tmp = (OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 *)cache->attributes[0];
		OriginalNameAttribute__ctor_mB0C6C38C49B9261D2DC423D692E80B8EBC657BF9(tmp, il2cpp_codegen_string_new_wrapper("\x46\x4F\x52\x45\x49\x47\x4E\x5F\x52\x45\x43\x4F\x52\x44\x49\x4F"), NULL);
	}
}
static void InputType_t06CD6E3FD08548CD0BB0B2462CED0C0A11F1E9AE_CustomAttributesCacheGenerator_ForeignCsvText(CustomAttributesCache* cache)
{
	{
		OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * tmp = (OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 *)cache->attributes[0];
		OriginalNameAttribute__ctor_mB0C6C38C49B9261D2DC423D692E80B8EBC657BF9(tmp, il2cpp_codegen_string_new_wrapper("\x46\x4F\x52\x45\x49\x47\x4E\x5F\x43\x53\x56\x5F\x54\x45\x58\x54"), NULL);
	}
}
static void InputType_t06CD6E3FD08548CD0BB0B2462CED0C0A11F1E9AE_CustomAttributesCacheGenerator_InvalidUpperBound(CustomAttributesCache* cache)
{
	{
		OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * tmp = (OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 *)cache->attributes[0];
		OriginalNameAttribute__ctor_mB0C6C38C49B9261D2DC423D692E80B8EBC657BF9(tmp, il2cpp_codegen_string_new_wrapper("\x49\x4E\x56\x41\x4C\x49\x44\x5F\x55\x50\x50\x45\x52\x5F\x42\x4F\x55\x4E\x44"), NULL);
	}
}
static void U3CU3Ec_t9F13509DBCE5115FCDC74F9839F12A056BE8153C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InputCollectionSet_tAF58E12C5E427F540512C06F0E46D278E52D89AA_CustomAttributesCacheGenerator_InputCollectionSet__ctor_m106104988333DB6E965B4966687686D1085762F5(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void InputCollectionSet_tAF58E12C5E427F540512C06F0E46D278E52D89AA_CustomAttributesCacheGenerator_InputCollectionSet__ctor_m5B651C10C9348EA4E9BAAEED0053D0D7A17CD82C(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void InputCollectionSet_tAF58E12C5E427F540512C06F0E46D278E52D89AA_CustomAttributesCacheGenerator_InputCollectionSet_Clone_mE409D46CDF2E45AFCF1BCCFA1245ED50361A972D(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void InputCollectionSet_tAF58E12C5E427F540512C06F0E46D278E52D89AA_CustomAttributesCacheGenerator_InputCollectionSet_Equals_mD0F5B94B7B317360CA85154981CC2B5193459D97(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void InputCollectionSet_tAF58E12C5E427F540512C06F0E46D278E52D89AA_CustomAttributesCacheGenerator_InputCollectionSet_Equals_mD2DF8C4AB1E38FE7EB5F3AE5991D7C2E7B686019(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void InputCollectionSet_tAF58E12C5E427F540512C06F0E46D278E52D89AA_CustomAttributesCacheGenerator_InputCollectionSet_GetHashCode_m6AC079DDE2E44C5212E7E62F1FF43C86B2B37D5E(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void InputCollectionSet_tAF58E12C5E427F540512C06F0E46D278E52D89AA_CustomAttributesCacheGenerator_InputCollectionSet_ToString_m9CABAE3D201DE47B317FA7DC708336FFF7E16607(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void InputCollectionSet_tAF58E12C5E427F540512C06F0E46D278E52D89AA_CustomAttributesCacheGenerator_InputCollectionSet_WriteTo_m929290F3104395CB0869AADAC6EAC99A02107476(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void InputCollectionSet_tAF58E12C5E427F540512C06F0E46D278E52D89AA_CustomAttributesCacheGenerator_InputCollectionSet_CalculateSize_m3035110C513A4AC4869481684000D6E163838F20(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void InputCollectionSet_tAF58E12C5E427F540512C06F0E46D278E52D89AA_CustomAttributesCacheGenerator_InputCollectionSet_MergeFrom_m57652BBC66362FB0B8E52A4C24A972069DCA12A3(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void InputCollectionSet_tAF58E12C5E427F540512C06F0E46D278E52D89AA_CustomAttributesCacheGenerator_InputCollectionSet_MergeFrom_m3ECEAC1E1ED1FF19AE13930016EE8AF9744C6C32(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void InputCollectionSet_tAF58E12C5E427F540512C06F0E46D278E52D89AA_CustomAttributesCacheGenerator_InputCollectionSet_tAF58E12C5E427F540512C06F0E46D278E52D89AA____Parser_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void InputCollectionSet_tAF58E12C5E427F540512C06F0E46D278E52D89AA_CustomAttributesCacheGenerator_InputCollectionSet_tAF58E12C5E427F540512C06F0E46D278E52D89AA____Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void InputCollectionSet_tAF58E12C5E427F540512C06F0E46D278E52D89AA_CustomAttributesCacheGenerator_InputCollectionSet_tAF58E12C5E427F540512C06F0E46D278E52D89AA____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void InputCollectionSet_tAF58E12C5E427F540512C06F0E46D278E52D89AA_CustomAttributesCacheGenerator_InputCollectionSet_tAF58E12C5E427F540512C06F0E46D278E52D89AA____InputCollection_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void U3CU3Ec_t7F9B2B6B274545FC6060B8F50F7BA4A00CD73954_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InputStreamInfo_t1D0174BE2B5EB4CF4B87459D2B634D2C17CB6E0B_CustomAttributesCacheGenerator_InputStreamInfo__ctor_mD1115B096AA44CBE3BAE01F934CB1DC0E2C8AA78(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void InputStreamInfo_t1D0174BE2B5EB4CF4B87459D2B634D2C17CB6E0B_CustomAttributesCacheGenerator_InputStreamInfo__ctor_m1B40C1CDA73DFEAD87E29B99BCEA761AE4C7A703(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void InputStreamInfo_t1D0174BE2B5EB4CF4B87459D2B634D2C17CB6E0B_CustomAttributesCacheGenerator_InputStreamInfo_Clone_mDFE3D66297DD000E2BA4F70D7856F18919BB7D44(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void InputStreamInfo_t1D0174BE2B5EB4CF4B87459D2B634D2C17CB6E0B_CustomAttributesCacheGenerator_InputStreamInfo_Equals_mD3672C103FBD9DF61B4EB0D8DD7B63EF8863A7C3(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void InputStreamInfo_t1D0174BE2B5EB4CF4B87459D2B634D2C17CB6E0B_CustomAttributesCacheGenerator_InputStreamInfo_Equals_m936804A131005237445BABB5F4EDFC06C448B018(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void InputStreamInfo_t1D0174BE2B5EB4CF4B87459D2B634D2C17CB6E0B_CustomAttributesCacheGenerator_InputStreamInfo_GetHashCode_m3AE4587C347B5910CAF5CC12E662B2064136D676(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void InputStreamInfo_t1D0174BE2B5EB4CF4B87459D2B634D2C17CB6E0B_CustomAttributesCacheGenerator_InputStreamInfo_ToString_m7F4E5F79EAC6EFBC730259B570B113870ACEA553(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void InputStreamInfo_t1D0174BE2B5EB4CF4B87459D2B634D2C17CB6E0B_CustomAttributesCacheGenerator_InputStreamInfo_WriteTo_mD01FACD335355F68B1D530EA10BF2E346EB45567(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void InputStreamInfo_t1D0174BE2B5EB4CF4B87459D2B634D2C17CB6E0B_CustomAttributesCacheGenerator_InputStreamInfo_CalculateSize_mD3CDD8A026DEA91C9FE050645038DD708196E41E(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void InputStreamInfo_t1D0174BE2B5EB4CF4B87459D2B634D2C17CB6E0B_CustomAttributesCacheGenerator_InputStreamInfo_MergeFrom_m9AE0F0C70340F4B1399211E4364CC883F752D325(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void InputStreamInfo_t1D0174BE2B5EB4CF4B87459D2B634D2C17CB6E0B_CustomAttributesCacheGenerator_InputStreamInfo_MergeFrom_mD104C197A6E64A40CB866B1ADC51E93F42761B5E(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void InputStreamInfo_t1D0174BE2B5EB4CF4B87459D2B634D2C17CB6E0B_CustomAttributesCacheGenerator_InputStreamInfo_t1D0174BE2B5EB4CF4B87459D2B634D2C17CB6E0B____Parser_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void InputStreamInfo_t1D0174BE2B5EB4CF4B87459D2B634D2C17CB6E0B_CustomAttributesCacheGenerator_InputStreamInfo_t1D0174BE2B5EB4CF4B87459D2B634D2C17CB6E0B____Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void InputStreamInfo_t1D0174BE2B5EB4CF4B87459D2B634D2C17CB6E0B_CustomAttributesCacheGenerator_InputStreamInfo_t1D0174BE2B5EB4CF4B87459D2B634D2C17CB6E0B____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void InputStreamInfo_t1D0174BE2B5EB4CF4B87459D2B634D2C17CB6E0B_CustomAttributesCacheGenerator_InputStreamInfo_t1D0174BE2B5EB4CF4B87459D2B634D2C17CB6E0B____TagIndex_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void InputStreamInfo_t1D0174BE2B5EB4CF4B87459D2B634D2C17CB6E0B_CustomAttributesCacheGenerator_InputStreamInfo_t1D0174BE2B5EB4CF4B87459D2B634D2C17CB6E0B____BackEdge_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void U3CU3Ec_t6244C7E01E3E624B6801BF78FB9959E508B7D113_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968_CustomAttributesCacheGenerator_ProfilerConfig__ctor_m023B7A29789ACE07320D0B31C8CA499115617D79(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968_CustomAttributesCacheGenerator_ProfilerConfig__ctor_m262E9327CC6CA09F9622D776162028033D1BE963(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968_CustomAttributesCacheGenerator_ProfilerConfig_Clone_m9412AE0F8B8C1AF838F6267D697F1ED44148B3D5(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968_CustomAttributesCacheGenerator_ProfilerConfig_Equals_mB3F233DC0F6A2B5D3A0A3E9E77F192F684323BD3(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968_CustomAttributesCacheGenerator_ProfilerConfig_Equals_m8F7A20EE6CD5D39FCC8B178E93A994BE8E2E49B2(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968_CustomAttributesCacheGenerator_ProfilerConfig_GetHashCode_m54E95CD9692BE7C31B86676C97728DD0E633F982(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968_CustomAttributesCacheGenerator_ProfilerConfig_ToString_m3B748ED748B01176BCB4494E2D750DAD217AC2A9(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968_CustomAttributesCacheGenerator_ProfilerConfig_WriteTo_m358D63D2EC966F259D61B184EFE314BD91EFC003(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968_CustomAttributesCacheGenerator_ProfilerConfig_CalculateSize_mAD4E24F9EC25953D971CBFDCA5B9E122B8D29EED(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968_CustomAttributesCacheGenerator_ProfilerConfig_MergeFrom_m832DCC37551A1090377D636FBF3741DD738508C8(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968_CustomAttributesCacheGenerator_ProfilerConfig_MergeFrom_m83749E2EB94F44989249949BF89412B0BE6EEF1B(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968_CustomAttributesCacheGenerator_ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968____Parser_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968_CustomAttributesCacheGenerator_ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968____Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968_CustomAttributesCacheGenerator_ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968_CustomAttributesCacheGenerator_ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968____HistogramIntervalSizeUsec_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968_CustomAttributesCacheGenerator_ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968____NumHistogramIntervals_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968_CustomAttributesCacheGenerator_ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968____EnableInputOutputLatency_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
	{
		ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * tmp = (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 *)cache->attributes[1];
		ObsoleteAttribute__ctor_m9BC17A80675E9013AA71F9FB38D89FEF56883853(tmp, NULL);
	}
}
static void ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968_CustomAttributesCacheGenerator_ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968____EnableProfiler_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968_CustomAttributesCacheGenerator_ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968____EnableStreamLatency_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968_CustomAttributesCacheGenerator_ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968____UsePacketTimestampForAddedPacket_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968_CustomAttributesCacheGenerator_ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968____TraceLogCapacity_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968_CustomAttributesCacheGenerator_ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968____TraceEventTypesDisabled_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968_CustomAttributesCacheGenerator_ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968____TraceLogPath_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968_CustomAttributesCacheGenerator_ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968____TraceLogCount_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968_CustomAttributesCacheGenerator_ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968____TraceLogIntervalUsec_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968_CustomAttributesCacheGenerator_ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968____TraceLogMarginUsec_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968_CustomAttributesCacheGenerator_ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968____TraceLogDurationEvents_PropertyInfo(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * tmp = (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 *)cache->attributes[0];
		ObsoleteAttribute__ctor_m9BC17A80675E9013AA71F9FB38D89FEF56883853(tmp, NULL);
	}
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[1];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968_CustomAttributesCacheGenerator_ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968____TraceLogIntervalCount_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968_CustomAttributesCacheGenerator_ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968____TraceLogDisabled_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968_CustomAttributesCacheGenerator_ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968____TraceEnabled_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968_CustomAttributesCacheGenerator_ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968____TraceLogInstantEvents_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void U3CU3Ec_t88399BB3483FBD7BEF6C78CA1C86B500A1BB64BF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348_CustomAttributesCacheGenerator_CalculatorGraphConfig__ctor_m2B2B9C3D725A811963E37BFACE3320D1B01AAD6B(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348_CustomAttributesCacheGenerator_CalculatorGraphConfig__ctor_mC7561D31FBB0998EA48446E954D238BB32F6D258(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348_CustomAttributesCacheGenerator_CalculatorGraphConfig_Clone_m17FBA506A44FF42CB08BBA16B66D753A23FD7F8E(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348_CustomAttributesCacheGenerator_CalculatorGraphConfig_Equals_m64171AE8626030825A893CF84181F3DB88686A78(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348_CustomAttributesCacheGenerator_CalculatorGraphConfig_Equals_m34E76CADA083771B042327F2E6A05098D82BC128(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348_CustomAttributesCacheGenerator_CalculatorGraphConfig_GetHashCode_mB45EBF4282A3719E6557603FD57749C19E2315C2(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348_CustomAttributesCacheGenerator_CalculatorGraphConfig_ToString_mA48B2E2730D472DDD57D0D36B0A0350E05BEE497(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348_CustomAttributesCacheGenerator_CalculatorGraphConfig_WriteTo_m0937B7E7FB01FA089C7EABB73DF1ACCCDB08D5EC(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348_CustomAttributesCacheGenerator_CalculatorGraphConfig_CalculateSize_m47318B281CC424A1787F9864750F2A6056BA8EEC(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348_CustomAttributesCacheGenerator_CalculatorGraphConfig_MergeFrom_m6A80FA1E5F6CD141888C842AD93B8979616CDB2F(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348_CustomAttributesCacheGenerator_CalculatorGraphConfig_MergeFrom_m75F230C7A43154402BAF91F2D7837F2D0C79BA4E(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348_CustomAttributesCacheGenerator_CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348____Parser_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348_CustomAttributesCacheGenerator_CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348____Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348_CustomAttributesCacheGenerator_CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348_CustomAttributesCacheGenerator_CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348____Node_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348_CustomAttributesCacheGenerator_CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348____PacketFactory_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348_CustomAttributesCacheGenerator_CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348____PacketGenerator_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348_CustomAttributesCacheGenerator_CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348____NumThreads_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348_CustomAttributesCacheGenerator_CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348____StatusHandler_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348_CustomAttributesCacheGenerator_CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348____InputStream_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348_CustomAttributesCacheGenerator_CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348____OutputStream_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348_CustomAttributesCacheGenerator_CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348____InputSidePacket_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348_CustomAttributesCacheGenerator_CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348____OutputSidePacket_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348_CustomAttributesCacheGenerator_CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348____MaxQueueSize_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348_CustomAttributesCacheGenerator_CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348____ReportDeadlock_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348_CustomAttributesCacheGenerator_CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348____InputStreamHandler_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348_CustomAttributesCacheGenerator_CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348____OutputStreamHandler_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348_CustomAttributesCacheGenerator_CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348____Executor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348_CustomAttributesCacheGenerator_CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348____ProfilerConfig_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348_CustomAttributesCacheGenerator_CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348____Package_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348_CustomAttributesCacheGenerator_CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348____Type_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348_CustomAttributesCacheGenerator_CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348____Options_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348_CustomAttributesCacheGenerator_CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348____GraphOptions_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Types_tAA56A21CDC0818B0260ABE6A9B9C933266ACBCBE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483_CustomAttributesCacheGenerator_Node__ctor_m5A45C5EA9496703F28618D3FDCD3D9603F43A1C4(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483_CustomAttributesCacheGenerator_Node__ctor_mEF6F5AF4FEF24FF0B17E590D1A99D89ED393FFD4(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483_CustomAttributesCacheGenerator_Node_Clone_m4A14B5A6E780D1C69310984C336E4E8A91214084(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483_CustomAttributesCacheGenerator_Node_Equals_m63B25BF70104CFF3B8CB2C3A3E9DFCAF0D225024(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483_CustomAttributesCacheGenerator_Node_Equals_mCAA8CD4AAA40FC247C882742F39D72EE8EF7C59B(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483_CustomAttributesCacheGenerator_Node_GetHashCode_m426A32AEA408311AC8B43458F2470D78F00F996D(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483_CustomAttributesCacheGenerator_Node_ToString_m6C22CB185110B11818FDB6AE1D71D1DF73DD72F7(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483_CustomAttributesCacheGenerator_Node_WriteTo_m2EAD9127103E77DC06897434BD2F8E070F00E0B8(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483_CustomAttributesCacheGenerator_Node_CalculateSize_m2DC063FE5B6CBB1980AA72F0AA9D7564C541C6DE(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483_CustomAttributesCacheGenerator_Node_MergeFrom_m1D3E147794EB001C4E41FECDBD9EDA04D1803378(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483_CustomAttributesCacheGenerator_Node_MergeFrom_mD3BB28988C717504CE6F43642FF64E4EAF34D512(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483_CustomAttributesCacheGenerator_Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483____Parser_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483_CustomAttributesCacheGenerator_Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483____Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483_CustomAttributesCacheGenerator_Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483_CustomAttributesCacheGenerator_Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483____Name_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483_CustomAttributesCacheGenerator_Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483____Calculator_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483_CustomAttributesCacheGenerator_Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483____InputStream_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483_CustomAttributesCacheGenerator_Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483____OutputStream_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483_CustomAttributesCacheGenerator_Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483____InputSidePacket_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483_CustomAttributesCacheGenerator_Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483____OutputSidePacket_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483_CustomAttributesCacheGenerator_Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483____Options_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483_CustomAttributesCacheGenerator_Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483____NodeOptions_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483_CustomAttributesCacheGenerator_Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483____SourceLayer_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483_CustomAttributesCacheGenerator_Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483____BufferSizeHint_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483_CustomAttributesCacheGenerator_Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483____InputStreamHandler_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483_CustomAttributesCacheGenerator_Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483____OutputStreamHandler_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483_CustomAttributesCacheGenerator_Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483____InputStreamInfo_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483_CustomAttributesCacheGenerator_Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483____Executor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483_CustomAttributesCacheGenerator_Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483____ProfilerConfig_PropertyInfo(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * tmp = (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 *)cache->attributes[0];
		ObsoleteAttribute__ctor_m9BC17A80675E9013AA71F9FB38D89FEF56883853(tmp, NULL);
	}
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[1];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483_CustomAttributesCacheGenerator_Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483____MaxInFlight_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483_CustomAttributesCacheGenerator_Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483____ExternalInput_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void U3CU3Ec_tD2B580CCD6FBAFA17B247C8F673ACF51461DBBF1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_t4B2AB6DB1BF7690B634459FC9E4703BD9FE44B7A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CalculatorOptions_t258E073EB6EBC7E44A9C100A180A4F40D5F89857_CustomAttributesCacheGenerator_CalculatorOptions__ctor_m9A66335BA5FAB6F4F057182BAF253B3A586C55FA(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void CalculatorOptions_t258E073EB6EBC7E44A9C100A180A4F40D5F89857_CustomAttributesCacheGenerator_CalculatorOptions__ctor_m022E05735F6CE2036FC8135812D6AFAF853F2D16(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void CalculatorOptions_t258E073EB6EBC7E44A9C100A180A4F40D5F89857_CustomAttributesCacheGenerator_CalculatorOptions_Clone_m717403D7365C851A4F8558834B83CD542CEE0571(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void CalculatorOptions_t258E073EB6EBC7E44A9C100A180A4F40D5F89857_CustomAttributesCacheGenerator_CalculatorOptions_ClearMergeFields_mFD3A53D320B135AFE511B684F9919729200C090D(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * tmp = (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 *)cache->attributes[0];
		ObsoleteAttribute__ctor_m9BC17A80675E9013AA71F9FB38D89FEF56883853(tmp, NULL);
	}
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[1];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void CalculatorOptions_t258E073EB6EBC7E44A9C100A180A4F40D5F89857_CustomAttributesCacheGenerator_CalculatorOptions_Equals_m1A6E5C0DB2FEB400FA0D5F76665FA4F37BDE8B03(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void CalculatorOptions_t258E073EB6EBC7E44A9C100A180A4F40D5F89857_CustomAttributesCacheGenerator_CalculatorOptions_Equals_m1511C43D99252F232CAF2CE903A223A9B7924B87(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void CalculatorOptions_t258E073EB6EBC7E44A9C100A180A4F40D5F89857_CustomAttributesCacheGenerator_CalculatorOptions_GetHashCode_m5879C42E02EDE756A47DFDE711951052D2BBF4D1(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void CalculatorOptions_t258E073EB6EBC7E44A9C100A180A4F40D5F89857_CustomAttributesCacheGenerator_CalculatorOptions_ToString_mB5C0791D4DB12DAAC8A21F528A1F1DA7BFC06ADF(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void CalculatorOptions_t258E073EB6EBC7E44A9C100A180A4F40D5F89857_CustomAttributesCacheGenerator_CalculatorOptions_WriteTo_m55AAD179414854C4BD37ED1895396F9EBBFEB1E7(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void CalculatorOptions_t258E073EB6EBC7E44A9C100A180A4F40D5F89857_CustomAttributesCacheGenerator_CalculatorOptions_CalculateSize_m9612BCA7552905D230E60EEAAFBAF60DA3767572(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void CalculatorOptions_t258E073EB6EBC7E44A9C100A180A4F40D5F89857_CustomAttributesCacheGenerator_CalculatorOptions_MergeFrom_mB9BBBF0230136216E2CB33D75343BC9CBD5EDE97(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void CalculatorOptions_t258E073EB6EBC7E44A9C100A180A4F40D5F89857_CustomAttributesCacheGenerator_CalculatorOptions_MergeFrom_m8F46A283DBD6012F2F985C795D06856AD6CD0E72(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void CalculatorOptions_t258E073EB6EBC7E44A9C100A180A4F40D5F89857_CustomAttributesCacheGenerator_CalculatorOptions_t258E073EB6EBC7E44A9C100A180A4F40D5F89857____Parser_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void CalculatorOptions_t258E073EB6EBC7E44A9C100A180A4F40D5F89857_CustomAttributesCacheGenerator_CalculatorOptions_t258E073EB6EBC7E44A9C100A180A4F40D5F89857____Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void CalculatorOptions_t258E073EB6EBC7E44A9C100A180A4F40D5F89857_CustomAttributesCacheGenerator_CalculatorOptions_t258E073EB6EBC7E44A9C100A180A4F40D5F89857____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void CalculatorOptions_t258E073EB6EBC7E44A9C100A180A4F40D5F89857_CustomAttributesCacheGenerator_CalculatorOptions_t258E073EB6EBC7E44A9C100A180A4F40D5F89857____MergeFields_PropertyInfo(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * tmp = (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 *)cache->attributes[0];
		ObsoleteAttribute__ctor_m9BC17A80675E9013AA71F9FB38D89FEF56883853(tmp, NULL);
	}
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[1];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void CalculatorOptions_t258E073EB6EBC7E44A9C100A180A4F40D5F89857_CustomAttributesCacheGenerator_CalculatorOptions_t258E073EB6EBC7E44A9C100A180A4F40D5F89857____HasMergeFields_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
	{
		ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * tmp = (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 *)cache->attributes[1];
		ObsoleteAttribute__ctor_m9BC17A80675E9013AA71F9FB38D89FEF56883853(tmp, NULL);
	}
}
static void U3CU3Ec_t720FE2495FF33A1525C1241449A7F483543FD4E5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Classification_tE9A954065398D143A5B6EEA01FAED5BCD7D81BB9_CustomAttributesCacheGenerator_Classification__ctor_mA0FD3F6BC90023C990B4B88F09F9CA77B8DBF669(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Classification_tE9A954065398D143A5B6EEA01FAED5BCD7D81BB9_CustomAttributesCacheGenerator_Classification__ctor_mEA745239FD6942EA8EBFCC2C66AA5C12645FD828(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Classification_tE9A954065398D143A5B6EEA01FAED5BCD7D81BB9_CustomAttributesCacheGenerator_Classification_Clone_m75EB64A47676D4684C1EF7F86F3053F9360ECD5B(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Classification_tE9A954065398D143A5B6EEA01FAED5BCD7D81BB9_CustomAttributesCacheGenerator_Classification_ClearIndex_m19867CF6E45A80A42275612C11C9AAAE7B684CA6(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Classification_tE9A954065398D143A5B6EEA01FAED5BCD7D81BB9_CustomAttributesCacheGenerator_Classification_ClearScore_m16E3BC42F4227EB6B6ECADA43520EF7363DA1278(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Classification_tE9A954065398D143A5B6EEA01FAED5BCD7D81BB9_CustomAttributesCacheGenerator_Classification_ClearLabel_m6F8D5E7CF22D2A405DA815F24248399CDCA66134(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Classification_tE9A954065398D143A5B6EEA01FAED5BCD7D81BB9_CustomAttributesCacheGenerator_Classification_ClearDisplayName_mAA08FE13D01E327D673D03547E9AAB60250C59C4(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Classification_tE9A954065398D143A5B6EEA01FAED5BCD7D81BB9_CustomAttributesCacheGenerator_Classification_Equals_mA50D3D642CB1069D2540F7FECA301BCB9E7ABF8F(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Classification_tE9A954065398D143A5B6EEA01FAED5BCD7D81BB9_CustomAttributesCacheGenerator_Classification_Equals_m46491A3E7C368EAD299737A771C47E78F8744E88(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Classification_tE9A954065398D143A5B6EEA01FAED5BCD7D81BB9_CustomAttributesCacheGenerator_Classification_GetHashCode_mC63AA2FB66A9CB60B0969C8410E40986ED657247(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Classification_tE9A954065398D143A5B6EEA01FAED5BCD7D81BB9_CustomAttributesCacheGenerator_Classification_ToString_m6D17450C980C2A8E9082FC523C0A78943D27E6F0(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Classification_tE9A954065398D143A5B6EEA01FAED5BCD7D81BB9_CustomAttributesCacheGenerator_Classification_WriteTo_m53061790010FE08E18DB482140160B2063EB583C(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Classification_tE9A954065398D143A5B6EEA01FAED5BCD7D81BB9_CustomAttributesCacheGenerator_Classification_CalculateSize_mFF773292996625892D0F77C07A43D0B65C7C931D(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Classification_tE9A954065398D143A5B6EEA01FAED5BCD7D81BB9_CustomAttributesCacheGenerator_Classification_MergeFrom_m8D981270B6DB8836F391140E0597189E1FCC74E3(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Classification_tE9A954065398D143A5B6EEA01FAED5BCD7D81BB9_CustomAttributesCacheGenerator_Classification_MergeFrom_m928242B2CE6F22D7577F5ACB8A01B507B34995C2(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Classification_tE9A954065398D143A5B6EEA01FAED5BCD7D81BB9_CustomAttributesCacheGenerator_Classification_tE9A954065398D143A5B6EEA01FAED5BCD7D81BB9____Parser_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Classification_tE9A954065398D143A5B6EEA01FAED5BCD7D81BB9_CustomAttributesCacheGenerator_Classification_tE9A954065398D143A5B6EEA01FAED5BCD7D81BB9____Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Classification_tE9A954065398D143A5B6EEA01FAED5BCD7D81BB9_CustomAttributesCacheGenerator_Classification_tE9A954065398D143A5B6EEA01FAED5BCD7D81BB9____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Classification_tE9A954065398D143A5B6EEA01FAED5BCD7D81BB9_CustomAttributesCacheGenerator_Classification_tE9A954065398D143A5B6EEA01FAED5BCD7D81BB9____Index_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Classification_tE9A954065398D143A5B6EEA01FAED5BCD7D81BB9_CustomAttributesCacheGenerator_Classification_tE9A954065398D143A5B6EEA01FAED5BCD7D81BB9____HasIndex_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Classification_tE9A954065398D143A5B6EEA01FAED5BCD7D81BB9_CustomAttributesCacheGenerator_Classification_tE9A954065398D143A5B6EEA01FAED5BCD7D81BB9____Score_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Classification_tE9A954065398D143A5B6EEA01FAED5BCD7D81BB9_CustomAttributesCacheGenerator_Classification_tE9A954065398D143A5B6EEA01FAED5BCD7D81BB9____HasScore_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Classification_tE9A954065398D143A5B6EEA01FAED5BCD7D81BB9_CustomAttributesCacheGenerator_Classification_tE9A954065398D143A5B6EEA01FAED5BCD7D81BB9____Label_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Classification_tE9A954065398D143A5B6EEA01FAED5BCD7D81BB9_CustomAttributesCacheGenerator_Classification_tE9A954065398D143A5B6EEA01FAED5BCD7D81BB9____HasLabel_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Classification_tE9A954065398D143A5B6EEA01FAED5BCD7D81BB9_CustomAttributesCacheGenerator_Classification_tE9A954065398D143A5B6EEA01FAED5BCD7D81BB9____DisplayName_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Classification_tE9A954065398D143A5B6EEA01FAED5BCD7D81BB9_CustomAttributesCacheGenerator_Classification_tE9A954065398D143A5B6EEA01FAED5BCD7D81BB9____HasDisplayName_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void U3CU3Ec_tC84E0C10849A469DF73B913A742DF92D8FA1D0DE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ClassificationList_tC58BDD049F1D6A853B63B07F07DCFA52DC02A8B5_CustomAttributesCacheGenerator_ClassificationList__ctor_m99E71A343606FDE18A16A30431F3FB10C8E85400(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ClassificationList_tC58BDD049F1D6A853B63B07F07DCFA52DC02A8B5_CustomAttributesCacheGenerator_ClassificationList__ctor_mCF0BED5C2EF4578C32BAF890A31DF4548E394480(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ClassificationList_tC58BDD049F1D6A853B63B07F07DCFA52DC02A8B5_CustomAttributesCacheGenerator_ClassificationList_Clone_m1BEC3137BD0BD0ADC62E511E6D6C00FC616B30A3(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ClassificationList_tC58BDD049F1D6A853B63B07F07DCFA52DC02A8B5_CustomAttributesCacheGenerator_ClassificationList_Equals_m2EBF80429667EEC0655AFE46B55349A6A4ECAB79(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ClassificationList_tC58BDD049F1D6A853B63B07F07DCFA52DC02A8B5_CustomAttributesCacheGenerator_ClassificationList_Equals_mC9CB38807A2EC3B87251459B9C540C3DB2D8AA91(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ClassificationList_tC58BDD049F1D6A853B63B07F07DCFA52DC02A8B5_CustomAttributesCacheGenerator_ClassificationList_GetHashCode_mA2375D105268BBA492A629CB6D74F776ED246C01(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ClassificationList_tC58BDD049F1D6A853B63B07F07DCFA52DC02A8B5_CustomAttributesCacheGenerator_ClassificationList_ToString_m8A6C7A594C9DA6CFAEB291FAF1C4419350870DE2(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ClassificationList_tC58BDD049F1D6A853B63B07F07DCFA52DC02A8B5_CustomAttributesCacheGenerator_ClassificationList_WriteTo_m5C28B1914E8CBA3CDD0CC26E009A49FDACE1E1B1(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ClassificationList_tC58BDD049F1D6A853B63B07F07DCFA52DC02A8B5_CustomAttributesCacheGenerator_ClassificationList_CalculateSize_m06E066709F39339F2F9880D3EB7A9CCDD1950F88(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ClassificationList_tC58BDD049F1D6A853B63B07F07DCFA52DC02A8B5_CustomAttributesCacheGenerator_ClassificationList_MergeFrom_m771CE3B97011B6A02527AA5F8AB056CC280A4759(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ClassificationList_tC58BDD049F1D6A853B63B07F07DCFA52DC02A8B5_CustomAttributesCacheGenerator_ClassificationList_MergeFrom_m11F8284D369551729C93A32BC16394DA6990280C(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ClassificationList_tC58BDD049F1D6A853B63B07F07DCFA52DC02A8B5_CustomAttributesCacheGenerator_ClassificationList_tC58BDD049F1D6A853B63B07F07DCFA52DC02A8B5____Parser_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ClassificationList_tC58BDD049F1D6A853B63B07F07DCFA52DC02A8B5_CustomAttributesCacheGenerator_ClassificationList_tC58BDD049F1D6A853B63B07F07DCFA52DC02A8B5____Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ClassificationList_tC58BDD049F1D6A853B63B07F07DCFA52DC02A8B5_CustomAttributesCacheGenerator_ClassificationList_tC58BDD049F1D6A853B63B07F07DCFA52DC02A8B5____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void ClassificationList_tC58BDD049F1D6A853B63B07F07DCFA52DC02A8B5_CustomAttributesCacheGenerator_ClassificationList_tC58BDD049F1D6A853B63B07F07DCFA52DC02A8B5____Classification_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void U3CU3Ec_tD243BB7950BD2A816658CA48C18E7379AE136A1D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E_CustomAttributesCacheGenerator_Detection__ctor_m55A7113BDFA8EF7BE08FA4B349F34A3565313C2C(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E_CustomAttributesCacheGenerator_Detection__ctor_m9A9FD199DC21FB37D14C03361D20D3D8E727FA25(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E_CustomAttributesCacheGenerator_Detection_Clone_mD7CBC5BAEBC3C6C75A793B8F3C68C9302369C26B(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E_CustomAttributesCacheGenerator_Detection_ClearLocationData_m1F6F3AB12BA2553C34D4438B8DAA691413B2F900(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E_CustomAttributesCacheGenerator_Detection_ClearFeatureTag_m2E2491436E69DB5B53F661B3E472B28FAEA25EFE(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E_CustomAttributesCacheGenerator_Detection_ClearTrackId_m966839C219ACA6DCC0636BFD8EDFF70048B82DC9(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E_CustomAttributesCacheGenerator_Detection_ClearDetectionId_m9CB64FD41C0C973CFD6AFAAA30170431AEC808F7(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E_CustomAttributesCacheGenerator_Detection_ClearTimestampUsec_m5E7F1296F66D3F6C41227404FD63C067189A97D9(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E_CustomAttributesCacheGenerator_Detection_Equals_mF155165DC1D42CDC67B1CA09A89384560162D4B0(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E_CustomAttributesCacheGenerator_Detection_Equals_mD313C1A5318C95D4E59E243292D9C147B7A49460(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E_CustomAttributesCacheGenerator_Detection_GetHashCode_mB7B8FB777B8621E0928027D44910CC0971BFA294(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E_CustomAttributesCacheGenerator_Detection_ToString_mE695F914BD60F8E148D00D86E4FBB5C060007026(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E_CustomAttributesCacheGenerator_Detection_WriteTo_m871A5B247BDFCCA600B3C8FE18949C639DB691B9(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E_CustomAttributesCacheGenerator_Detection_CalculateSize_m729B385BE443B130004A0016D33D88914B391E01(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E_CustomAttributesCacheGenerator_Detection_MergeFrom_m6FD510B8AA365A2AB532AF3FA7AC12E7CA62B444(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E_CustomAttributesCacheGenerator_Detection_MergeFrom_m677B85361862396FF00F51831D73EFF565CA7532(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E_CustomAttributesCacheGenerator_Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E____Parser_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E_CustomAttributesCacheGenerator_Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E____Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E_CustomAttributesCacheGenerator_Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E_CustomAttributesCacheGenerator_Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E____Label_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E_CustomAttributesCacheGenerator_Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E____LabelId_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E_CustomAttributesCacheGenerator_Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E____Score_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E_CustomAttributesCacheGenerator_Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E____LocationData_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E_CustomAttributesCacheGenerator_Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E____HasLocationData_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E_CustomAttributesCacheGenerator_Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E____FeatureTag_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E_CustomAttributesCacheGenerator_Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E____HasFeatureTag_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E_CustomAttributesCacheGenerator_Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E____TrackId_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E_CustomAttributesCacheGenerator_Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E____HasTrackId_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E_CustomAttributesCacheGenerator_Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E____DetectionId_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E_CustomAttributesCacheGenerator_Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E____HasDetectionId_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E_CustomAttributesCacheGenerator_Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E____AssociatedDetections_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E_CustomAttributesCacheGenerator_Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E____DisplayName_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E_CustomAttributesCacheGenerator_Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E____TimestampUsec_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E_CustomAttributesCacheGenerator_Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E____HasTimestampUsec_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Types_tF264D77BD89891DC8875C3BB0C62675FF604E5C3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void AssociatedDetection_t24212822C1761635604505177F1691F542A4DF88_CustomAttributesCacheGenerator_AssociatedDetection__ctor_mA166F2C623931F25847860332A520ABAA5CC9DB4(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void AssociatedDetection_t24212822C1761635604505177F1691F542A4DF88_CustomAttributesCacheGenerator_AssociatedDetection__ctor_m1BFE2C623E6B59BB029246429752139FFA5455BC(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void AssociatedDetection_t24212822C1761635604505177F1691F542A4DF88_CustomAttributesCacheGenerator_AssociatedDetection_Clone_m11C498A5BFF91B15128640FE1FF6C186DC791115(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void AssociatedDetection_t24212822C1761635604505177F1691F542A4DF88_CustomAttributesCacheGenerator_AssociatedDetection_ClearId_m1329E13167EA8CDBCFD82765534017CF7CCBC2F4(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void AssociatedDetection_t24212822C1761635604505177F1691F542A4DF88_CustomAttributesCacheGenerator_AssociatedDetection_ClearConfidence_m552D9205076452F3657B3C6A145064F6FE0CEEAD(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void AssociatedDetection_t24212822C1761635604505177F1691F542A4DF88_CustomAttributesCacheGenerator_AssociatedDetection_Equals_mD472A75917A686CB5371B84ADC4B8667091541FF(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void AssociatedDetection_t24212822C1761635604505177F1691F542A4DF88_CustomAttributesCacheGenerator_AssociatedDetection_Equals_m1069816F34AFE7A08AD3953CFC280417427FDA02(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void AssociatedDetection_t24212822C1761635604505177F1691F542A4DF88_CustomAttributesCacheGenerator_AssociatedDetection_GetHashCode_mE8B82EDD364192C96CFECF67D9C44ACD43128F64(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void AssociatedDetection_t24212822C1761635604505177F1691F542A4DF88_CustomAttributesCacheGenerator_AssociatedDetection_ToString_m7AF4B716FBE2D34C584115309FD4DC10F3B1D396(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void AssociatedDetection_t24212822C1761635604505177F1691F542A4DF88_CustomAttributesCacheGenerator_AssociatedDetection_WriteTo_m6CF6A142DD499EA8E2564F71291BE2FCD068DF87(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void AssociatedDetection_t24212822C1761635604505177F1691F542A4DF88_CustomAttributesCacheGenerator_AssociatedDetection_CalculateSize_m31A05FE7461704430FFD30B726F2EFE701EA7F05(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void AssociatedDetection_t24212822C1761635604505177F1691F542A4DF88_CustomAttributesCacheGenerator_AssociatedDetection_MergeFrom_m1C3C649269388963B9FA33ED78AA4FF2FA0DB27D(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void AssociatedDetection_t24212822C1761635604505177F1691F542A4DF88_CustomAttributesCacheGenerator_AssociatedDetection_MergeFrom_m2BB477AACB83B0CF3CDF2C0161D97898E2152612(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void AssociatedDetection_t24212822C1761635604505177F1691F542A4DF88_CustomAttributesCacheGenerator_AssociatedDetection_t24212822C1761635604505177F1691F542A4DF88____Parser_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void AssociatedDetection_t24212822C1761635604505177F1691F542A4DF88_CustomAttributesCacheGenerator_AssociatedDetection_t24212822C1761635604505177F1691F542A4DF88____Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void AssociatedDetection_t24212822C1761635604505177F1691F542A4DF88_CustomAttributesCacheGenerator_AssociatedDetection_t24212822C1761635604505177F1691F542A4DF88____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void AssociatedDetection_t24212822C1761635604505177F1691F542A4DF88_CustomAttributesCacheGenerator_AssociatedDetection_t24212822C1761635604505177F1691F542A4DF88____Id_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void AssociatedDetection_t24212822C1761635604505177F1691F542A4DF88_CustomAttributesCacheGenerator_AssociatedDetection_t24212822C1761635604505177F1691F542A4DF88____HasId_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void AssociatedDetection_t24212822C1761635604505177F1691F542A4DF88_CustomAttributesCacheGenerator_AssociatedDetection_t24212822C1761635604505177F1691F542A4DF88____Confidence_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void AssociatedDetection_t24212822C1761635604505177F1691F542A4DF88_CustomAttributesCacheGenerator_AssociatedDetection_t24212822C1761635604505177F1691F542A4DF88____HasConfidence_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void U3CU3Ec_t76319172D0AD039F5C4D0408B5BCC93F3F9B096E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_t581A289842F8AC72AF35AC7151889D7FC2C87931_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DetectionList_tA50FF81C129AF30DDF87A1F025CD9B8B4A10EF29_CustomAttributesCacheGenerator_DetectionList__ctor_m06FDB5957ED0F6F0B39F5738E06C2F1159B37183(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void DetectionList_tA50FF81C129AF30DDF87A1F025CD9B8B4A10EF29_CustomAttributesCacheGenerator_DetectionList__ctor_mAE98482B38CBE076985F9128D7281E33546913D1(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void DetectionList_tA50FF81C129AF30DDF87A1F025CD9B8B4A10EF29_CustomAttributesCacheGenerator_DetectionList_Clone_m5331EB486F6B61CFB67E0BF4ECD4BE8AB966B807(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void DetectionList_tA50FF81C129AF30DDF87A1F025CD9B8B4A10EF29_CustomAttributesCacheGenerator_DetectionList_Equals_m298DE49B1E060D9C24B467E5A55A8C1D145923D4(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void DetectionList_tA50FF81C129AF30DDF87A1F025CD9B8B4A10EF29_CustomAttributesCacheGenerator_DetectionList_Equals_m6E7B500E4EADA19BAFBE795DFDCFBA9A8B44596F(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void DetectionList_tA50FF81C129AF30DDF87A1F025CD9B8B4A10EF29_CustomAttributesCacheGenerator_DetectionList_GetHashCode_m807A6737A246CB6010A539204908F6F27215C8EE(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void DetectionList_tA50FF81C129AF30DDF87A1F025CD9B8B4A10EF29_CustomAttributesCacheGenerator_DetectionList_ToString_mB5437A519C9DDF1885D468B7324AADDE901CB12D(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void DetectionList_tA50FF81C129AF30DDF87A1F025CD9B8B4A10EF29_CustomAttributesCacheGenerator_DetectionList_WriteTo_m7BA51D678FE5CAD8ADD3A9FDEFE9211D36CB8420(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void DetectionList_tA50FF81C129AF30DDF87A1F025CD9B8B4A10EF29_CustomAttributesCacheGenerator_DetectionList_CalculateSize_mD096AE8E4FAC18AAA69EFAE4CACF35BDA4A4AC58(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void DetectionList_tA50FF81C129AF30DDF87A1F025CD9B8B4A10EF29_CustomAttributesCacheGenerator_DetectionList_MergeFrom_mDF65E3B58421A40462E72B81742D574CC1C9458B(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void DetectionList_tA50FF81C129AF30DDF87A1F025CD9B8B4A10EF29_CustomAttributesCacheGenerator_DetectionList_MergeFrom_m630B2FE189B963D229CE290BA57F9AA276645AEC(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void DetectionList_tA50FF81C129AF30DDF87A1F025CD9B8B4A10EF29_CustomAttributesCacheGenerator_DetectionList_tA50FF81C129AF30DDF87A1F025CD9B8B4A10EF29____Parser_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void DetectionList_tA50FF81C129AF30DDF87A1F025CD9B8B4A10EF29_CustomAttributesCacheGenerator_DetectionList_tA50FF81C129AF30DDF87A1F025CD9B8B4A10EF29____Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void DetectionList_tA50FF81C129AF30DDF87A1F025CD9B8B4A10EF29_CustomAttributesCacheGenerator_DetectionList_tA50FF81C129AF30DDF87A1F025CD9B8B4A10EF29____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void DetectionList_tA50FF81C129AF30DDF87A1F025CD9B8B4A10EF29_CustomAttributesCacheGenerator_DetectionList_tA50FF81C129AF30DDF87A1F025CD9B8B4A10EF29____Detection_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void U3CU3Ec_t89CE3936129D9A47D63F4C5B6855F334D9F62DF4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Landmark_t593F12E6EAABFA00011E82F8D4408C3F75E4EFE3_CustomAttributesCacheGenerator_Landmark__ctor_m32AB2F02006F1DA904DB9B95FBA6F54438CB7E09(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Landmark_t593F12E6EAABFA00011E82F8D4408C3F75E4EFE3_CustomAttributesCacheGenerator_Landmark__ctor_mB5954D418050FA757FEEAA123D3DB388279E0D66(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Landmark_t593F12E6EAABFA00011E82F8D4408C3F75E4EFE3_CustomAttributesCacheGenerator_Landmark_Clone_m2F263E193E5C29A90092AE5EE1AC5B3880CAAAEC(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Landmark_t593F12E6EAABFA00011E82F8D4408C3F75E4EFE3_CustomAttributesCacheGenerator_Landmark_ClearX_m75472AA9F683DEC71B30978EA0A088121462A016(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Landmark_t593F12E6EAABFA00011E82F8D4408C3F75E4EFE3_CustomAttributesCacheGenerator_Landmark_ClearY_mFE69B3F9E85D3947683E92A28E5F6D43F0FC2BAA(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Landmark_t593F12E6EAABFA00011E82F8D4408C3F75E4EFE3_CustomAttributesCacheGenerator_Landmark_ClearZ_m77CF2955E6B9A2F237E810067B15CD756B9B3A3A(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Landmark_t593F12E6EAABFA00011E82F8D4408C3F75E4EFE3_CustomAttributesCacheGenerator_Landmark_ClearVisibility_m63565970A485B48F1E5A549D2A9468B6CB05EF7D(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Landmark_t593F12E6EAABFA00011E82F8D4408C3F75E4EFE3_CustomAttributesCacheGenerator_Landmark_ClearPresence_m04EEA91E043A0665A4B35DCC26206F73D5643763(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Landmark_t593F12E6EAABFA00011E82F8D4408C3F75E4EFE3_CustomAttributesCacheGenerator_Landmark_Equals_m5DF27AE33F5ADCA0F166401F6EF682568172E3CD(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Landmark_t593F12E6EAABFA00011E82F8D4408C3F75E4EFE3_CustomAttributesCacheGenerator_Landmark_Equals_m5DCEFC736DAAB541ECAF07682E0CBA342E06F8C5(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Landmark_t593F12E6EAABFA00011E82F8D4408C3F75E4EFE3_CustomAttributesCacheGenerator_Landmark_GetHashCode_m1D05AEC667DFD74EF7BD565F23D18E9F2D9EBABF(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Landmark_t593F12E6EAABFA00011E82F8D4408C3F75E4EFE3_CustomAttributesCacheGenerator_Landmark_ToString_m8A1ADCAF99F8EAB736244D2D64180F410747E9B6(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Landmark_t593F12E6EAABFA00011E82F8D4408C3F75E4EFE3_CustomAttributesCacheGenerator_Landmark_WriteTo_m2426178F2F1BE3E779166512562C66D5CE55E2DB(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Landmark_t593F12E6EAABFA00011E82F8D4408C3F75E4EFE3_CustomAttributesCacheGenerator_Landmark_CalculateSize_m57A5261BCE0E7D5FAD4404AB917F2F7D5709CA8A(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Landmark_t593F12E6EAABFA00011E82F8D4408C3F75E4EFE3_CustomAttributesCacheGenerator_Landmark_MergeFrom_m601FEE1790CD75767288FECA657CEEC9516EB0AB(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Landmark_t593F12E6EAABFA00011E82F8D4408C3F75E4EFE3_CustomAttributesCacheGenerator_Landmark_MergeFrom_mA2E18E48D58A71220844E0C26DBA09D4E2DED0BB(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Landmark_t593F12E6EAABFA00011E82F8D4408C3F75E4EFE3_CustomAttributesCacheGenerator_Landmark_t593F12E6EAABFA00011E82F8D4408C3F75E4EFE3____Parser_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Landmark_t593F12E6EAABFA00011E82F8D4408C3F75E4EFE3_CustomAttributesCacheGenerator_Landmark_t593F12E6EAABFA00011E82F8D4408C3F75E4EFE3____Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Landmark_t593F12E6EAABFA00011E82F8D4408C3F75E4EFE3_CustomAttributesCacheGenerator_Landmark_t593F12E6EAABFA00011E82F8D4408C3F75E4EFE3____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Landmark_t593F12E6EAABFA00011E82F8D4408C3F75E4EFE3_CustomAttributesCacheGenerator_Landmark_t593F12E6EAABFA00011E82F8D4408C3F75E4EFE3____X_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Landmark_t593F12E6EAABFA00011E82F8D4408C3F75E4EFE3_CustomAttributesCacheGenerator_Landmark_t593F12E6EAABFA00011E82F8D4408C3F75E4EFE3____HasX_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Landmark_t593F12E6EAABFA00011E82F8D4408C3F75E4EFE3_CustomAttributesCacheGenerator_Landmark_t593F12E6EAABFA00011E82F8D4408C3F75E4EFE3____Y_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Landmark_t593F12E6EAABFA00011E82F8D4408C3F75E4EFE3_CustomAttributesCacheGenerator_Landmark_t593F12E6EAABFA00011E82F8D4408C3F75E4EFE3____HasY_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Landmark_t593F12E6EAABFA00011E82F8D4408C3F75E4EFE3_CustomAttributesCacheGenerator_Landmark_t593F12E6EAABFA00011E82F8D4408C3F75E4EFE3____Z_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Landmark_t593F12E6EAABFA00011E82F8D4408C3F75E4EFE3_CustomAttributesCacheGenerator_Landmark_t593F12E6EAABFA00011E82F8D4408C3F75E4EFE3____HasZ_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Landmark_t593F12E6EAABFA00011E82F8D4408C3F75E4EFE3_CustomAttributesCacheGenerator_Landmark_t593F12E6EAABFA00011E82F8D4408C3F75E4EFE3____Visibility_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Landmark_t593F12E6EAABFA00011E82F8D4408C3F75E4EFE3_CustomAttributesCacheGenerator_Landmark_t593F12E6EAABFA00011E82F8D4408C3F75E4EFE3____HasVisibility_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Landmark_t593F12E6EAABFA00011E82F8D4408C3F75E4EFE3_CustomAttributesCacheGenerator_Landmark_t593F12E6EAABFA00011E82F8D4408C3F75E4EFE3____Presence_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Landmark_t593F12E6EAABFA00011E82F8D4408C3F75E4EFE3_CustomAttributesCacheGenerator_Landmark_t593F12E6EAABFA00011E82F8D4408C3F75E4EFE3____HasPresence_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void U3CU3Ec_tEAAF53EEB29ABDAD486D2E4FFBFA931A57AA1973_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LandmarkList_t81090E798AEC4F7C446243CBAD3BDB14C16525EA_CustomAttributesCacheGenerator_LandmarkList__ctor_mB39DEFB418B9FB30F8E826F7398A5D2F2993154B(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void LandmarkList_t81090E798AEC4F7C446243CBAD3BDB14C16525EA_CustomAttributesCacheGenerator_LandmarkList__ctor_mAB0C124B8A91ECD95FEEA961A5B7AB35704CBF9A(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void LandmarkList_t81090E798AEC4F7C446243CBAD3BDB14C16525EA_CustomAttributesCacheGenerator_LandmarkList_Clone_m162BFB1581722DA392C42B14FF4453FE63924E8A(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void LandmarkList_t81090E798AEC4F7C446243CBAD3BDB14C16525EA_CustomAttributesCacheGenerator_LandmarkList_Equals_m67DF4B25C4F479352B3A65E7BE42E3FC87122CAA(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void LandmarkList_t81090E798AEC4F7C446243CBAD3BDB14C16525EA_CustomAttributesCacheGenerator_LandmarkList_Equals_mEF4EFD2347D65884B176A40B3F9C6A316D41C8B4(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void LandmarkList_t81090E798AEC4F7C446243CBAD3BDB14C16525EA_CustomAttributesCacheGenerator_LandmarkList_GetHashCode_m9BD8E4A4309FF6C0CEE9C524C47140FB34A2565E(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void LandmarkList_t81090E798AEC4F7C446243CBAD3BDB14C16525EA_CustomAttributesCacheGenerator_LandmarkList_ToString_m8B21B5CB0BA7CB7F08FED7BA6D77FC136CF2DF3C(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void LandmarkList_t81090E798AEC4F7C446243CBAD3BDB14C16525EA_CustomAttributesCacheGenerator_LandmarkList_WriteTo_m31B2E7A45E2503C6D1600AB5EF7902F9B5245D07(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void LandmarkList_t81090E798AEC4F7C446243CBAD3BDB14C16525EA_CustomAttributesCacheGenerator_LandmarkList_CalculateSize_m53AFAD8AED8C89539F906D99555FDAFF55CC0A5D(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void LandmarkList_t81090E798AEC4F7C446243CBAD3BDB14C16525EA_CustomAttributesCacheGenerator_LandmarkList_MergeFrom_m8FAB382380EFEDB9FDC963AB56EE75A624A26283(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void LandmarkList_t81090E798AEC4F7C446243CBAD3BDB14C16525EA_CustomAttributesCacheGenerator_LandmarkList_MergeFrom_m84474E94FCBB33DCEFCAFE1F005BACDF7D381A67(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void LandmarkList_t81090E798AEC4F7C446243CBAD3BDB14C16525EA_CustomAttributesCacheGenerator_LandmarkList_t81090E798AEC4F7C446243CBAD3BDB14C16525EA____Parser_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void LandmarkList_t81090E798AEC4F7C446243CBAD3BDB14C16525EA_CustomAttributesCacheGenerator_LandmarkList_t81090E798AEC4F7C446243CBAD3BDB14C16525EA____Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void LandmarkList_t81090E798AEC4F7C446243CBAD3BDB14C16525EA_CustomAttributesCacheGenerator_LandmarkList_t81090E798AEC4F7C446243CBAD3BDB14C16525EA____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void LandmarkList_t81090E798AEC4F7C446243CBAD3BDB14C16525EA_CustomAttributesCacheGenerator_LandmarkList_t81090E798AEC4F7C446243CBAD3BDB14C16525EA____Landmark_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void U3CU3Ec_t436CC23D732B78B12BA7AF69DAA40088A6033D7C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void NormalizedLandmark_t5FAF8FE108D8B6740A93A860AF7786612D36A48C_CustomAttributesCacheGenerator_NormalizedLandmark__ctor_m749D7C799F92DEC13A50FBF151B384E803BFFF52(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NormalizedLandmark_t5FAF8FE108D8B6740A93A860AF7786612D36A48C_CustomAttributesCacheGenerator_NormalizedLandmark__ctor_mB9A73BE195E17D391BC26DC9C4A9FE7CD3FCF784(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NormalizedLandmark_t5FAF8FE108D8B6740A93A860AF7786612D36A48C_CustomAttributesCacheGenerator_NormalizedLandmark_Clone_mEA928C357031BFF03D7B3463A38391F589530EFA(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NormalizedLandmark_t5FAF8FE108D8B6740A93A860AF7786612D36A48C_CustomAttributesCacheGenerator_NormalizedLandmark_ClearX_mF00D31AC4974CD5DFC81875A4A6FC3143DE11626(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NormalizedLandmark_t5FAF8FE108D8B6740A93A860AF7786612D36A48C_CustomAttributesCacheGenerator_NormalizedLandmark_ClearY_mBA1143EE451962D9CCD7D6490DEE4B109E402477(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NormalizedLandmark_t5FAF8FE108D8B6740A93A860AF7786612D36A48C_CustomAttributesCacheGenerator_NormalizedLandmark_ClearZ_m43166BF7D6C2B8CC8455116E3ED711FFAD77E03B(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NormalizedLandmark_t5FAF8FE108D8B6740A93A860AF7786612D36A48C_CustomAttributesCacheGenerator_NormalizedLandmark_ClearVisibility_m462FB2CAAF3A6987E414033FC435E9EEA260EC45(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NormalizedLandmark_t5FAF8FE108D8B6740A93A860AF7786612D36A48C_CustomAttributesCacheGenerator_NormalizedLandmark_ClearPresence_m3BA6187034736183F1237A19186AB4F7C5D98B68(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NormalizedLandmark_t5FAF8FE108D8B6740A93A860AF7786612D36A48C_CustomAttributesCacheGenerator_NormalizedLandmark_Equals_m7212B45EE8C95C27A16E61219CF0BC7B63508158(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NormalizedLandmark_t5FAF8FE108D8B6740A93A860AF7786612D36A48C_CustomAttributesCacheGenerator_NormalizedLandmark_Equals_m694303789E20659F4755195A665597050EA7E661(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NormalizedLandmark_t5FAF8FE108D8B6740A93A860AF7786612D36A48C_CustomAttributesCacheGenerator_NormalizedLandmark_GetHashCode_m2ABE8B45C235972D5D5E33F4E688B9B6014840F5(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NormalizedLandmark_t5FAF8FE108D8B6740A93A860AF7786612D36A48C_CustomAttributesCacheGenerator_NormalizedLandmark_ToString_m2071AEC6E2374C9F11165F7CDFFE5720E273362E(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NormalizedLandmark_t5FAF8FE108D8B6740A93A860AF7786612D36A48C_CustomAttributesCacheGenerator_NormalizedLandmark_WriteTo_m33870DE137A9B7ED976F9A092FFA7B9F0A1930B5(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NormalizedLandmark_t5FAF8FE108D8B6740A93A860AF7786612D36A48C_CustomAttributesCacheGenerator_NormalizedLandmark_CalculateSize_mE3D06A71DF8BE59866DE7837D6A848673525A73A(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NormalizedLandmark_t5FAF8FE108D8B6740A93A860AF7786612D36A48C_CustomAttributesCacheGenerator_NormalizedLandmark_MergeFrom_m903A7C27238CE6DC37619530278D38EC5E7124B1(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NormalizedLandmark_t5FAF8FE108D8B6740A93A860AF7786612D36A48C_CustomAttributesCacheGenerator_NormalizedLandmark_MergeFrom_m0348956EE9DEA0CDEE4760CCBBE0B275EE0DB93A(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NormalizedLandmark_t5FAF8FE108D8B6740A93A860AF7786612D36A48C_CustomAttributesCacheGenerator_NormalizedLandmark_t5FAF8FE108D8B6740A93A860AF7786612D36A48C____Parser_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NormalizedLandmark_t5FAF8FE108D8B6740A93A860AF7786612D36A48C_CustomAttributesCacheGenerator_NormalizedLandmark_t5FAF8FE108D8B6740A93A860AF7786612D36A48C____Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NormalizedLandmark_t5FAF8FE108D8B6740A93A860AF7786612D36A48C_CustomAttributesCacheGenerator_NormalizedLandmark_t5FAF8FE108D8B6740A93A860AF7786612D36A48C____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NormalizedLandmark_t5FAF8FE108D8B6740A93A860AF7786612D36A48C_CustomAttributesCacheGenerator_NormalizedLandmark_t5FAF8FE108D8B6740A93A860AF7786612D36A48C____X_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NormalizedLandmark_t5FAF8FE108D8B6740A93A860AF7786612D36A48C_CustomAttributesCacheGenerator_NormalizedLandmark_t5FAF8FE108D8B6740A93A860AF7786612D36A48C____HasX_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NormalizedLandmark_t5FAF8FE108D8B6740A93A860AF7786612D36A48C_CustomAttributesCacheGenerator_NormalizedLandmark_t5FAF8FE108D8B6740A93A860AF7786612D36A48C____Y_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NormalizedLandmark_t5FAF8FE108D8B6740A93A860AF7786612D36A48C_CustomAttributesCacheGenerator_NormalizedLandmark_t5FAF8FE108D8B6740A93A860AF7786612D36A48C____HasY_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NormalizedLandmark_t5FAF8FE108D8B6740A93A860AF7786612D36A48C_CustomAttributesCacheGenerator_NormalizedLandmark_t5FAF8FE108D8B6740A93A860AF7786612D36A48C____Z_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NormalizedLandmark_t5FAF8FE108D8B6740A93A860AF7786612D36A48C_CustomAttributesCacheGenerator_NormalizedLandmark_t5FAF8FE108D8B6740A93A860AF7786612D36A48C____HasZ_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NormalizedLandmark_t5FAF8FE108D8B6740A93A860AF7786612D36A48C_CustomAttributesCacheGenerator_NormalizedLandmark_t5FAF8FE108D8B6740A93A860AF7786612D36A48C____Visibility_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NormalizedLandmark_t5FAF8FE108D8B6740A93A860AF7786612D36A48C_CustomAttributesCacheGenerator_NormalizedLandmark_t5FAF8FE108D8B6740A93A860AF7786612D36A48C____HasVisibility_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NormalizedLandmark_t5FAF8FE108D8B6740A93A860AF7786612D36A48C_CustomAttributesCacheGenerator_NormalizedLandmark_t5FAF8FE108D8B6740A93A860AF7786612D36A48C____Presence_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NormalizedLandmark_t5FAF8FE108D8B6740A93A860AF7786612D36A48C_CustomAttributesCacheGenerator_NormalizedLandmark_t5FAF8FE108D8B6740A93A860AF7786612D36A48C____HasPresence_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void U3CU3Ec_tA8DAFFD20122CFA86C1D826C0831F521C3D73021_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void NormalizedLandmarkList_tD96817E5BBC04C5051186DE23E72CA9FB2E8E11E_CustomAttributesCacheGenerator_NormalizedLandmarkList__ctor_m01D41F6D909C6F3B3B7EEE22709861C7B29D61FA(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NormalizedLandmarkList_tD96817E5BBC04C5051186DE23E72CA9FB2E8E11E_CustomAttributesCacheGenerator_NormalizedLandmarkList__ctor_m75C1E44F5F089A869ED0E929367C75160499360C(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NormalizedLandmarkList_tD96817E5BBC04C5051186DE23E72CA9FB2E8E11E_CustomAttributesCacheGenerator_NormalizedLandmarkList_Clone_m0858F45BAAB39546DBF5853A5FCB0E2395F34E54(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NormalizedLandmarkList_tD96817E5BBC04C5051186DE23E72CA9FB2E8E11E_CustomAttributesCacheGenerator_NormalizedLandmarkList_Equals_m0B854C7B06A98D47E68E15B965FE8EDEE4445CDD(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NormalizedLandmarkList_tD96817E5BBC04C5051186DE23E72CA9FB2E8E11E_CustomAttributesCacheGenerator_NormalizedLandmarkList_Equals_mF6220EBEBE45837C5C7ABE8A60139A168CFD6555(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NormalizedLandmarkList_tD96817E5BBC04C5051186DE23E72CA9FB2E8E11E_CustomAttributesCacheGenerator_NormalizedLandmarkList_GetHashCode_m8815B70BC600FCAC3B59DFF49C681B3351F4C493(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NormalizedLandmarkList_tD96817E5BBC04C5051186DE23E72CA9FB2E8E11E_CustomAttributesCacheGenerator_NormalizedLandmarkList_ToString_mE0E29E7405F94D7B9B37CC75B53A64A473B142E9(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NormalizedLandmarkList_tD96817E5BBC04C5051186DE23E72CA9FB2E8E11E_CustomAttributesCacheGenerator_NormalizedLandmarkList_WriteTo_m7030BCD0F34648E08815E519717EF288531D0070(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NormalizedLandmarkList_tD96817E5BBC04C5051186DE23E72CA9FB2E8E11E_CustomAttributesCacheGenerator_NormalizedLandmarkList_CalculateSize_mDBDAE3F1AA8E835537862295EC26955E3ECBAB71(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NormalizedLandmarkList_tD96817E5BBC04C5051186DE23E72CA9FB2E8E11E_CustomAttributesCacheGenerator_NormalizedLandmarkList_MergeFrom_m9E3B0324A50B637AEC4F25E875C6061F5FF0231F(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NormalizedLandmarkList_tD96817E5BBC04C5051186DE23E72CA9FB2E8E11E_CustomAttributesCacheGenerator_NormalizedLandmarkList_MergeFrom_mC7D95F8D50AF1080D9B19AAC9DD77E7376766312(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NormalizedLandmarkList_tD96817E5BBC04C5051186DE23E72CA9FB2E8E11E_CustomAttributesCacheGenerator_NormalizedLandmarkList_tD96817E5BBC04C5051186DE23E72CA9FB2E8E11E____Parser_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NormalizedLandmarkList_tD96817E5BBC04C5051186DE23E72CA9FB2E8E11E_CustomAttributesCacheGenerator_NormalizedLandmarkList_tD96817E5BBC04C5051186DE23E72CA9FB2E8E11E____Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NormalizedLandmarkList_tD96817E5BBC04C5051186DE23E72CA9FB2E8E11E_CustomAttributesCacheGenerator_NormalizedLandmarkList_tD96817E5BBC04C5051186DE23E72CA9FB2E8E11E____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NormalizedLandmarkList_tD96817E5BBC04C5051186DE23E72CA9FB2E8E11E_CustomAttributesCacheGenerator_NormalizedLandmarkList_tD96817E5BBC04C5051186DE23E72CA9FB2E8E11E____Landmark_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void U3CU3Ec_tBFAE5CA9FD4858EB6B7C4622EAB68A31A61E0B9B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LocationData_t137C5EE37EACB9E28B218F2FF28B3BFFF27BC81A_CustomAttributesCacheGenerator_LocationData__ctor_mB604C634DB577934AE64462669C6C3764CB2000D(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void LocationData_t137C5EE37EACB9E28B218F2FF28B3BFFF27BC81A_CustomAttributesCacheGenerator_LocationData__ctor_m6D1AEDF1838BB6FC4150FD00A021962307D60372(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void LocationData_t137C5EE37EACB9E28B218F2FF28B3BFFF27BC81A_CustomAttributesCacheGenerator_LocationData_Clone_m79CB2770CCAA00A4C9BB99459225994D1291D8F9(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void LocationData_t137C5EE37EACB9E28B218F2FF28B3BFFF27BC81A_CustomAttributesCacheGenerator_LocationData_ClearFormat_mC84F4F1946BB3CE469972A09C41E2008F4B0A7CB(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void LocationData_t137C5EE37EACB9E28B218F2FF28B3BFFF27BC81A_CustomAttributesCacheGenerator_LocationData_ClearBoundingBox_m5EDCE0CB5DC3978B1728178FDBDBA170265FC6F5(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void LocationData_t137C5EE37EACB9E28B218F2FF28B3BFFF27BC81A_CustomAttributesCacheGenerator_LocationData_ClearRelativeBoundingBox_m30E69638BD31A7ABFF6EA8607F0B8716ED807B99(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void LocationData_t137C5EE37EACB9E28B218F2FF28B3BFFF27BC81A_CustomAttributesCacheGenerator_LocationData_ClearMask_m76CE8607652C20FFBAEAE510127E861DDB3677CC(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void LocationData_t137C5EE37EACB9E28B218F2FF28B3BFFF27BC81A_CustomAttributesCacheGenerator_LocationData_Equals_m170A452AFFA5AD61A8E91F3A2C414E43B776262E(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void LocationData_t137C5EE37EACB9E28B218F2FF28B3BFFF27BC81A_CustomAttributesCacheGenerator_LocationData_Equals_m898CD12FC26B6B0589CB403E9C6B29550877A69D(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void LocationData_t137C5EE37EACB9E28B218F2FF28B3BFFF27BC81A_CustomAttributesCacheGenerator_LocationData_GetHashCode_m307B9D8787153A4DFD1C54C01FE4D9BD05672255(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void LocationData_t137C5EE37EACB9E28B218F2FF28B3BFFF27BC81A_CustomAttributesCacheGenerator_LocationData_ToString_m29E48F542440D30B1734C7DB05DFB9CCE5202A05(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void LocationData_t137C5EE37EACB9E28B218F2FF28B3BFFF27BC81A_CustomAttributesCacheGenerator_LocationData_WriteTo_mA5DA809FB04E3707BF837E4352734EC94D6B2B95(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void LocationData_t137C5EE37EACB9E28B218F2FF28B3BFFF27BC81A_CustomAttributesCacheGenerator_LocationData_CalculateSize_m0E812979AB5E6E5CCEA276AEA562C84BB021E4CB(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void LocationData_t137C5EE37EACB9E28B218F2FF28B3BFFF27BC81A_CustomAttributesCacheGenerator_LocationData_MergeFrom_mCAA4BC513485F808DD7A20C6281A4B546A237D05(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void LocationData_t137C5EE37EACB9E28B218F2FF28B3BFFF27BC81A_CustomAttributesCacheGenerator_LocationData_MergeFrom_m388838B258FB88CE12864E45E68DD5F22274BE5C(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void LocationData_t137C5EE37EACB9E28B218F2FF28B3BFFF27BC81A_CustomAttributesCacheGenerator_LocationData_t137C5EE37EACB9E28B218F2FF28B3BFFF27BC81A____Parser_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void LocationData_t137C5EE37EACB9E28B218F2FF28B3BFFF27BC81A_CustomAttributesCacheGenerator_LocationData_t137C5EE37EACB9E28B218F2FF28B3BFFF27BC81A____Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void LocationData_t137C5EE37EACB9E28B218F2FF28B3BFFF27BC81A_CustomAttributesCacheGenerator_LocationData_t137C5EE37EACB9E28B218F2FF28B3BFFF27BC81A____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void LocationData_t137C5EE37EACB9E28B218F2FF28B3BFFF27BC81A_CustomAttributesCacheGenerator_LocationData_t137C5EE37EACB9E28B218F2FF28B3BFFF27BC81A____Format_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void LocationData_t137C5EE37EACB9E28B218F2FF28B3BFFF27BC81A_CustomAttributesCacheGenerator_LocationData_t137C5EE37EACB9E28B218F2FF28B3BFFF27BC81A____HasFormat_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void LocationData_t137C5EE37EACB9E28B218F2FF28B3BFFF27BC81A_CustomAttributesCacheGenerator_LocationData_t137C5EE37EACB9E28B218F2FF28B3BFFF27BC81A____BoundingBox_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void LocationData_t137C5EE37EACB9E28B218F2FF28B3BFFF27BC81A_CustomAttributesCacheGenerator_LocationData_t137C5EE37EACB9E28B218F2FF28B3BFFF27BC81A____HasBoundingBox_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void LocationData_t137C5EE37EACB9E28B218F2FF28B3BFFF27BC81A_CustomAttributesCacheGenerator_LocationData_t137C5EE37EACB9E28B218F2FF28B3BFFF27BC81A____RelativeBoundingBox_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void LocationData_t137C5EE37EACB9E28B218F2FF28B3BFFF27BC81A_CustomAttributesCacheGenerator_LocationData_t137C5EE37EACB9E28B218F2FF28B3BFFF27BC81A____HasRelativeBoundingBox_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void LocationData_t137C5EE37EACB9E28B218F2FF28B3BFFF27BC81A_CustomAttributesCacheGenerator_LocationData_t137C5EE37EACB9E28B218F2FF28B3BFFF27BC81A____Mask_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void LocationData_t137C5EE37EACB9E28B218F2FF28B3BFFF27BC81A_CustomAttributesCacheGenerator_LocationData_t137C5EE37EACB9E28B218F2FF28B3BFFF27BC81A____HasMask_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void LocationData_t137C5EE37EACB9E28B218F2FF28B3BFFF27BC81A_CustomAttributesCacheGenerator_LocationData_t137C5EE37EACB9E28B218F2FF28B3BFFF27BC81A____RelativeKeypoints_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Types_t331A01D2554D11320784F4555E3035B794969908_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Format_t899CE640F3FB09E97A6F7796F8A0AFB346CFC49E_CustomAttributesCacheGenerator_Global(CustomAttributesCache* cache)
{
	{
		OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * tmp = (OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 *)cache->attributes[0];
		OriginalNameAttribute__ctor_mB0C6C38C49B9261D2DC423D692E80B8EBC657BF9(tmp, il2cpp_codegen_string_new_wrapper("\x47\x4C\x4F\x42\x41\x4C"), NULL);
	}
}
static void Format_t899CE640F3FB09E97A6F7796F8A0AFB346CFC49E_CustomAttributesCacheGenerator_BoundingBox(CustomAttributesCache* cache)
{
	{
		OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * tmp = (OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 *)cache->attributes[0];
		OriginalNameAttribute__ctor_mB0C6C38C49B9261D2DC423D692E80B8EBC657BF9(tmp, il2cpp_codegen_string_new_wrapper("\x42\x4F\x55\x4E\x44\x49\x4E\x47\x5F\x42\x4F\x58"), NULL);
	}
}
static void Format_t899CE640F3FB09E97A6F7796F8A0AFB346CFC49E_CustomAttributesCacheGenerator_RelativeBoundingBox(CustomAttributesCache* cache)
{
	{
		OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * tmp = (OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 *)cache->attributes[0];
		OriginalNameAttribute__ctor_mB0C6C38C49B9261D2DC423D692E80B8EBC657BF9(tmp, il2cpp_codegen_string_new_wrapper("\x52\x45\x4C\x41\x54\x49\x56\x45\x5F\x42\x4F\x55\x4E\x44\x49\x4E\x47\x5F\x42\x4F\x58"), NULL);
	}
}
static void Format_t899CE640F3FB09E97A6F7796F8A0AFB346CFC49E_CustomAttributesCacheGenerator_Mask(CustomAttributesCache* cache)
{
	{
		OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * tmp = (OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 *)cache->attributes[0];
		OriginalNameAttribute__ctor_mB0C6C38C49B9261D2DC423D692E80B8EBC657BF9(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x41\x53\x4B"), NULL);
	}
}
static void BoundingBox_tF4DCB9A65CC691059CAC02F205F73759C1A7ADAC_CustomAttributesCacheGenerator_BoundingBox__ctor_m593B06506BFD3A2B0775E81312CF30B4E63C4D65(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void BoundingBox_tF4DCB9A65CC691059CAC02F205F73759C1A7ADAC_CustomAttributesCacheGenerator_BoundingBox__ctor_m627AE04BA213E57A63AEC13B676D7F5B74DC92FF(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void BoundingBox_tF4DCB9A65CC691059CAC02F205F73759C1A7ADAC_CustomAttributesCacheGenerator_BoundingBox_Clone_m00AE731CD8CDFCDC53DA43A8065BEEC74E9DD5B0(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void BoundingBox_tF4DCB9A65CC691059CAC02F205F73759C1A7ADAC_CustomAttributesCacheGenerator_BoundingBox_ClearXmin_m17EB77B29A2B29A9B182986F8C45D7C0DDE4238D(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void BoundingBox_tF4DCB9A65CC691059CAC02F205F73759C1A7ADAC_CustomAttributesCacheGenerator_BoundingBox_ClearYmin_m786E94CB3B96169321D4E023816E36B3E111D402(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void BoundingBox_tF4DCB9A65CC691059CAC02F205F73759C1A7ADAC_CustomAttributesCacheGenerator_BoundingBox_ClearWidth_mF1280F4300C6409B29F083612CC43AE23EF27BBD(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void BoundingBox_tF4DCB9A65CC691059CAC02F205F73759C1A7ADAC_CustomAttributesCacheGenerator_BoundingBox_ClearHeight_m39635CDC84989A610ECA78395F36D44D5E752E24(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void BoundingBox_tF4DCB9A65CC691059CAC02F205F73759C1A7ADAC_CustomAttributesCacheGenerator_BoundingBox_Equals_m59EADA8FA336A51CAA7BDAC92C9659F352E0EA82(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void BoundingBox_tF4DCB9A65CC691059CAC02F205F73759C1A7ADAC_CustomAttributesCacheGenerator_BoundingBox_Equals_mA301D6545DCCF5CDDB551F3F5D3E0EA14BE4FDB9(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void BoundingBox_tF4DCB9A65CC691059CAC02F205F73759C1A7ADAC_CustomAttributesCacheGenerator_BoundingBox_GetHashCode_mA2E1A26961E791956B155392365ABE0CFD32C9F4(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void BoundingBox_tF4DCB9A65CC691059CAC02F205F73759C1A7ADAC_CustomAttributesCacheGenerator_BoundingBox_ToString_m59B089B9CD4F5D49F89881F869DC857AFC3B78A8(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void BoundingBox_tF4DCB9A65CC691059CAC02F205F73759C1A7ADAC_CustomAttributesCacheGenerator_BoundingBox_WriteTo_m5893434F89C8673816C4FA7884F8BD8CEE7B8087(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void BoundingBox_tF4DCB9A65CC691059CAC02F205F73759C1A7ADAC_CustomAttributesCacheGenerator_BoundingBox_CalculateSize_m3FF05A537A0E9A624B5FABFE60EBC6FFB10F5A23(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void BoundingBox_tF4DCB9A65CC691059CAC02F205F73759C1A7ADAC_CustomAttributesCacheGenerator_BoundingBox_MergeFrom_m8FE73B1C9B61C5307C66B1C3D706836388AC89FC(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void BoundingBox_tF4DCB9A65CC691059CAC02F205F73759C1A7ADAC_CustomAttributesCacheGenerator_BoundingBox_MergeFrom_mC6EFA4D2E99DEC21D436A2A6D90362394A47BFFD(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void BoundingBox_tF4DCB9A65CC691059CAC02F205F73759C1A7ADAC_CustomAttributesCacheGenerator_BoundingBox_tF4DCB9A65CC691059CAC02F205F73759C1A7ADAC____Parser_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void BoundingBox_tF4DCB9A65CC691059CAC02F205F73759C1A7ADAC_CustomAttributesCacheGenerator_BoundingBox_tF4DCB9A65CC691059CAC02F205F73759C1A7ADAC____Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void BoundingBox_tF4DCB9A65CC691059CAC02F205F73759C1A7ADAC_CustomAttributesCacheGenerator_BoundingBox_tF4DCB9A65CC691059CAC02F205F73759C1A7ADAC____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void BoundingBox_tF4DCB9A65CC691059CAC02F205F73759C1A7ADAC_CustomAttributesCacheGenerator_BoundingBox_tF4DCB9A65CC691059CAC02F205F73759C1A7ADAC____Xmin_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void BoundingBox_tF4DCB9A65CC691059CAC02F205F73759C1A7ADAC_CustomAttributesCacheGenerator_BoundingBox_tF4DCB9A65CC691059CAC02F205F73759C1A7ADAC____HasXmin_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void BoundingBox_tF4DCB9A65CC691059CAC02F205F73759C1A7ADAC_CustomAttributesCacheGenerator_BoundingBox_tF4DCB9A65CC691059CAC02F205F73759C1A7ADAC____Ymin_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void BoundingBox_tF4DCB9A65CC691059CAC02F205F73759C1A7ADAC_CustomAttributesCacheGenerator_BoundingBox_tF4DCB9A65CC691059CAC02F205F73759C1A7ADAC____HasYmin_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void BoundingBox_tF4DCB9A65CC691059CAC02F205F73759C1A7ADAC_CustomAttributesCacheGenerator_BoundingBox_tF4DCB9A65CC691059CAC02F205F73759C1A7ADAC____Width_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void BoundingBox_tF4DCB9A65CC691059CAC02F205F73759C1A7ADAC_CustomAttributesCacheGenerator_BoundingBox_tF4DCB9A65CC691059CAC02F205F73759C1A7ADAC____HasWidth_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void BoundingBox_tF4DCB9A65CC691059CAC02F205F73759C1A7ADAC_CustomAttributesCacheGenerator_BoundingBox_tF4DCB9A65CC691059CAC02F205F73759C1A7ADAC____Height_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void BoundingBox_tF4DCB9A65CC691059CAC02F205F73759C1A7ADAC_CustomAttributesCacheGenerator_BoundingBox_tF4DCB9A65CC691059CAC02F205F73759C1A7ADAC____HasHeight_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void U3CU3Ec_t1FD492E3283975CCB4673F6C1C57F94D9E8E25AA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RelativeBoundingBox_tC31BDE3CCCD2009BC2562071BDB4B5DB13F6752E_CustomAttributesCacheGenerator_RelativeBoundingBox__ctor_mA8BC438A7E04136689A5998E0D7EE0CD98C66B6A(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void RelativeBoundingBox_tC31BDE3CCCD2009BC2562071BDB4B5DB13F6752E_CustomAttributesCacheGenerator_RelativeBoundingBox__ctor_m4944F2FED20D1A230D2BF9E5D6A32F725CFCD6EF(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void RelativeBoundingBox_tC31BDE3CCCD2009BC2562071BDB4B5DB13F6752E_CustomAttributesCacheGenerator_RelativeBoundingBox_Clone_m0841DF2E2BFB704DFDCE1809E145778315BE1633(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void RelativeBoundingBox_tC31BDE3CCCD2009BC2562071BDB4B5DB13F6752E_CustomAttributesCacheGenerator_RelativeBoundingBox_ClearXmin_m28CEF281DAEC7AF7C8ED9F0E471553717368B8C3(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void RelativeBoundingBox_tC31BDE3CCCD2009BC2562071BDB4B5DB13F6752E_CustomAttributesCacheGenerator_RelativeBoundingBox_ClearYmin_mD759F2E5D785928C94DA4D2A89BD34A34BA7F11B(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void RelativeBoundingBox_tC31BDE3CCCD2009BC2562071BDB4B5DB13F6752E_CustomAttributesCacheGenerator_RelativeBoundingBox_ClearWidth_m72C70182A243C7353F98E8899B82EF42648B98E1(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void RelativeBoundingBox_tC31BDE3CCCD2009BC2562071BDB4B5DB13F6752E_CustomAttributesCacheGenerator_RelativeBoundingBox_ClearHeight_mD70EE677AB7F7D7C9573EF087BD90D08794B41D7(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void RelativeBoundingBox_tC31BDE3CCCD2009BC2562071BDB4B5DB13F6752E_CustomAttributesCacheGenerator_RelativeBoundingBox_Equals_m4A16727F878D1A1CF29E042D8088470E058DD7DB(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void RelativeBoundingBox_tC31BDE3CCCD2009BC2562071BDB4B5DB13F6752E_CustomAttributesCacheGenerator_RelativeBoundingBox_Equals_m45535830904520641BA7D02C53DCBE8A548359B4(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void RelativeBoundingBox_tC31BDE3CCCD2009BC2562071BDB4B5DB13F6752E_CustomAttributesCacheGenerator_RelativeBoundingBox_GetHashCode_m6E36E0BA9CABBBBB75860C51EB1C253702E47E62(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void RelativeBoundingBox_tC31BDE3CCCD2009BC2562071BDB4B5DB13F6752E_CustomAttributesCacheGenerator_RelativeBoundingBox_ToString_mD096BEBCC911A0831D49B8ADB2393668CB1E07AA(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void RelativeBoundingBox_tC31BDE3CCCD2009BC2562071BDB4B5DB13F6752E_CustomAttributesCacheGenerator_RelativeBoundingBox_WriteTo_mB80D4F45F77E83F7B6A170F6A3BC48001C0FACF0(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void RelativeBoundingBox_tC31BDE3CCCD2009BC2562071BDB4B5DB13F6752E_CustomAttributesCacheGenerator_RelativeBoundingBox_CalculateSize_mCE3CB30EDFEAE4B677F7EF03C94E74FD0F39566E(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void RelativeBoundingBox_tC31BDE3CCCD2009BC2562071BDB4B5DB13F6752E_CustomAttributesCacheGenerator_RelativeBoundingBox_MergeFrom_m0E9CE082728E5F9669448DDC651D3B424F8E444F(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void RelativeBoundingBox_tC31BDE3CCCD2009BC2562071BDB4B5DB13F6752E_CustomAttributesCacheGenerator_RelativeBoundingBox_MergeFrom_m47BCC3409807A67E5FAEBF440FDD9155DFE0DA20(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void RelativeBoundingBox_tC31BDE3CCCD2009BC2562071BDB4B5DB13F6752E_CustomAttributesCacheGenerator_RelativeBoundingBox_tC31BDE3CCCD2009BC2562071BDB4B5DB13F6752E____Parser_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void RelativeBoundingBox_tC31BDE3CCCD2009BC2562071BDB4B5DB13F6752E_CustomAttributesCacheGenerator_RelativeBoundingBox_tC31BDE3CCCD2009BC2562071BDB4B5DB13F6752E____Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void RelativeBoundingBox_tC31BDE3CCCD2009BC2562071BDB4B5DB13F6752E_CustomAttributesCacheGenerator_RelativeBoundingBox_tC31BDE3CCCD2009BC2562071BDB4B5DB13F6752E____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void RelativeBoundingBox_tC31BDE3CCCD2009BC2562071BDB4B5DB13F6752E_CustomAttributesCacheGenerator_RelativeBoundingBox_tC31BDE3CCCD2009BC2562071BDB4B5DB13F6752E____Xmin_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void RelativeBoundingBox_tC31BDE3CCCD2009BC2562071BDB4B5DB13F6752E_CustomAttributesCacheGenerator_RelativeBoundingBox_tC31BDE3CCCD2009BC2562071BDB4B5DB13F6752E____HasXmin_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void RelativeBoundingBox_tC31BDE3CCCD2009BC2562071BDB4B5DB13F6752E_CustomAttributesCacheGenerator_RelativeBoundingBox_tC31BDE3CCCD2009BC2562071BDB4B5DB13F6752E____Ymin_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void RelativeBoundingBox_tC31BDE3CCCD2009BC2562071BDB4B5DB13F6752E_CustomAttributesCacheGenerator_RelativeBoundingBox_tC31BDE3CCCD2009BC2562071BDB4B5DB13F6752E____HasYmin_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void RelativeBoundingBox_tC31BDE3CCCD2009BC2562071BDB4B5DB13F6752E_CustomAttributesCacheGenerator_RelativeBoundingBox_tC31BDE3CCCD2009BC2562071BDB4B5DB13F6752E____Width_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void RelativeBoundingBox_tC31BDE3CCCD2009BC2562071BDB4B5DB13F6752E_CustomAttributesCacheGenerator_RelativeBoundingBox_tC31BDE3CCCD2009BC2562071BDB4B5DB13F6752E____HasWidth_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void RelativeBoundingBox_tC31BDE3CCCD2009BC2562071BDB4B5DB13F6752E_CustomAttributesCacheGenerator_RelativeBoundingBox_tC31BDE3CCCD2009BC2562071BDB4B5DB13F6752E____Height_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void RelativeBoundingBox_tC31BDE3CCCD2009BC2562071BDB4B5DB13F6752E_CustomAttributesCacheGenerator_RelativeBoundingBox_tC31BDE3CCCD2009BC2562071BDB4B5DB13F6752E____HasHeight_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void U3CU3Ec_t7113280FE900CC249CB34C5F691D6B120D0A61C4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BinaryMask_t0F9C5C2054ED5487FD9D2297F6800F66AC695DBE_CustomAttributesCacheGenerator_BinaryMask__ctor_m8F90D268C87AA0A2998B869A679E486B6784B069(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void BinaryMask_t0F9C5C2054ED5487FD9D2297F6800F66AC695DBE_CustomAttributesCacheGenerator_BinaryMask__ctor_m03BA73D9ACA551B4101934F7A24C4AFB1061B712(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void BinaryMask_t0F9C5C2054ED5487FD9D2297F6800F66AC695DBE_CustomAttributesCacheGenerator_BinaryMask_Clone_m686B51B7DC6D0BAB62F6C639B6ADE99AC8A9AA99(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void BinaryMask_t0F9C5C2054ED5487FD9D2297F6800F66AC695DBE_CustomAttributesCacheGenerator_BinaryMask_ClearWidth_m7C972B069D508D1FE0EA7991A4C5362201988D9D(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void BinaryMask_t0F9C5C2054ED5487FD9D2297F6800F66AC695DBE_CustomAttributesCacheGenerator_BinaryMask_ClearHeight_m0CB9F998280EEB1A27CA8EF7AD9776ECB07A5DD6(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void BinaryMask_t0F9C5C2054ED5487FD9D2297F6800F66AC695DBE_CustomAttributesCacheGenerator_BinaryMask_ClearRasterization_m652D34C3A5C6168C317903658D48ED049EF71B9A(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void BinaryMask_t0F9C5C2054ED5487FD9D2297F6800F66AC695DBE_CustomAttributesCacheGenerator_BinaryMask_Equals_mBF78A6B0C1E5516AD385E4C4D22851C5BA57A5F1(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void BinaryMask_t0F9C5C2054ED5487FD9D2297F6800F66AC695DBE_CustomAttributesCacheGenerator_BinaryMask_Equals_m49D94332523281DBAC6789DA4D6C2179E28EF83A(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void BinaryMask_t0F9C5C2054ED5487FD9D2297F6800F66AC695DBE_CustomAttributesCacheGenerator_BinaryMask_GetHashCode_m9E4FFB7CE411C64CAD98DBA291AF0E05C61A8FE5(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void BinaryMask_t0F9C5C2054ED5487FD9D2297F6800F66AC695DBE_CustomAttributesCacheGenerator_BinaryMask_ToString_mF795F2557EF8ED863BBD2F1C459778A61115700B(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void BinaryMask_t0F9C5C2054ED5487FD9D2297F6800F66AC695DBE_CustomAttributesCacheGenerator_BinaryMask_WriteTo_mA261BA797008FF73839EBA7E0592F0DEBD415564(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void BinaryMask_t0F9C5C2054ED5487FD9D2297F6800F66AC695DBE_CustomAttributesCacheGenerator_BinaryMask_CalculateSize_mC22E19FAE984002D7163988734767B26FC5823D1(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void BinaryMask_t0F9C5C2054ED5487FD9D2297F6800F66AC695DBE_CustomAttributesCacheGenerator_BinaryMask_MergeFrom_m6BE953E5A769220962214F9F05E0FE319C1B652B(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void BinaryMask_t0F9C5C2054ED5487FD9D2297F6800F66AC695DBE_CustomAttributesCacheGenerator_BinaryMask_MergeFrom_m5DA0857C4C0936C91F2EB06AE3F299518918D776(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void BinaryMask_t0F9C5C2054ED5487FD9D2297F6800F66AC695DBE_CustomAttributesCacheGenerator_BinaryMask_t0F9C5C2054ED5487FD9D2297F6800F66AC695DBE____Parser_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void BinaryMask_t0F9C5C2054ED5487FD9D2297F6800F66AC695DBE_CustomAttributesCacheGenerator_BinaryMask_t0F9C5C2054ED5487FD9D2297F6800F66AC695DBE____Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void BinaryMask_t0F9C5C2054ED5487FD9D2297F6800F66AC695DBE_CustomAttributesCacheGenerator_BinaryMask_t0F9C5C2054ED5487FD9D2297F6800F66AC695DBE____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void BinaryMask_t0F9C5C2054ED5487FD9D2297F6800F66AC695DBE_CustomAttributesCacheGenerator_BinaryMask_t0F9C5C2054ED5487FD9D2297F6800F66AC695DBE____Width_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void BinaryMask_t0F9C5C2054ED5487FD9D2297F6800F66AC695DBE_CustomAttributesCacheGenerator_BinaryMask_t0F9C5C2054ED5487FD9D2297F6800F66AC695DBE____HasWidth_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void BinaryMask_t0F9C5C2054ED5487FD9D2297F6800F66AC695DBE_CustomAttributesCacheGenerator_BinaryMask_t0F9C5C2054ED5487FD9D2297F6800F66AC695DBE____Height_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void BinaryMask_t0F9C5C2054ED5487FD9D2297F6800F66AC695DBE_CustomAttributesCacheGenerator_BinaryMask_t0F9C5C2054ED5487FD9D2297F6800F66AC695DBE____HasHeight_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void BinaryMask_t0F9C5C2054ED5487FD9D2297F6800F66AC695DBE_CustomAttributesCacheGenerator_BinaryMask_t0F9C5C2054ED5487FD9D2297F6800F66AC695DBE____Rasterization_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void BinaryMask_t0F9C5C2054ED5487FD9D2297F6800F66AC695DBE_CustomAttributesCacheGenerator_BinaryMask_t0F9C5C2054ED5487FD9D2297F6800F66AC695DBE____HasRasterization_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void U3CU3Ec_t476BEC4D64811E70D44EDA4B9A3881257E53AB15_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RelativeKeypoint_tB0867DC1F9743FC97A924CB29A42ADB6A8E7DEA7_CustomAttributesCacheGenerator_RelativeKeypoint__ctor_m206C6B4CC8BFC83247B3CF0E9B6C1DB3EF9FD239(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void RelativeKeypoint_tB0867DC1F9743FC97A924CB29A42ADB6A8E7DEA7_CustomAttributesCacheGenerator_RelativeKeypoint__ctor_mEAE05077643B9B02D325976BFEA6673DEB47ACB9(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void RelativeKeypoint_tB0867DC1F9743FC97A924CB29A42ADB6A8E7DEA7_CustomAttributesCacheGenerator_RelativeKeypoint_Clone_mB4A646D4224167F8867687AA163FFDB1772B344B(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void RelativeKeypoint_tB0867DC1F9743FC97A924CB29A42ADB6A8E7DEA7_CustomAttributesCacheGenerator_RelativeKeypoint_ClearX_mA536C8F3890EAC060547009BFB0E7159C702549E(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void RelativeKeypoint_tB0867DC1F9743FC97A924CB29A42ADB6A8E7DEA7_CustomAttributesCacheGenerator_RelativeKeypoint_ClearY_mBDEF3488FCD528959DEA9E1D490013226E8C3ADC(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void RelativeKeypoint_tB0867DC1F9743FC97A924CB29A42ADB6A8E7DEA7_CustomAttributesCacheGenerator_RelativeKeypoint_ClearKeypointLabel_m3706CBCDA0D55D054051AFFF760EF0590F75698F(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void RelativeKeypoint_tB0867DC1F9743FC97A924CB29A42ADB6A8E7DEA7_CustomAttributesCacheGenerator_RelativeKeypoint_ClearScore_m05253CED8244B4BEE218FD9F8F1299A536A68F34(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void RelativeKeypoint_tB0867DC1F9743FC97A924CB29A42ADB6A8E7DEA7_CustomAttributesCacheGenerator_RelativeKeypoint_Equals_m3F57F2FDEDA1A3E18FB46700957235704010CA1C(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void RelativeKeypoint_tB0867DC1F9743FC97A924CB29A42ADB6A8E7DEA7_CustomAttributesCacheGenerator_RelativeKeypoint_Equals_m704069F17BBCA172946EC75767DE0558DE5796C5(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void RelativeKeypoint_tB0867DC1F9743FC97A924CB29A42ADB6A8E7DEA7_CustomAttributesCacheGenerator_RelativeKeypoint_GetHashCode_m52ACCE197F999EFC3DC86317A382DD805532C5B9(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void RelativeKeypoint_tB0867DC1F9743FC97A924CB29A42ADB6A8E7DEA7_CustomAttributesCacheGenerator_RelativeKeypoint_ToString_mF649593D1A01012190C83FCAC793FE30E0A2ADB7(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void RelativeKeypoint_tB0867DC1F9743FC97A924CB29A42ADB6A8E7DEA7_CustomAttributesCacheGenerator_RelativeKeypoint_WriteTo_mBF98F261D95B71894CAA982839EC4F8A6D99ABC1(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void RelativeKeypoint_tB0867DC1F9743FC97A924CB29A42ADB6A8E7DEA7_CustomAttributesCacheGenerator_RelativeKeypoint_CalculateSize_m5B1B5C66064B9BF331664023DBEEBAC75DFD9169(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void RelativeKeypoint_tB0867DC1F9743FC97A924CB29A42ADB6A8E7DEA7_CustomAttributesCacheGenerator_RelativeKeypoint_MergeFrom_m198362F71ABDF0359BA9874F2E7AF59F52B14C30(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void RelativeKeypoint_tB0867DC1F9743FC97A924CB29A42ADB6A8E7DEA7_CustomAttributesCacheGenerator_RelativeKeypoint_MergeFrom_mE832CEDC0F6D893DA89014206624FDC0F0009AA9(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void RelativeKeypoint_tB0867DC1F9743FC97A924CB29A42ADB6A8E7DEA7_CustomAttributesCacheGenerator_RelativeKeypoint_tB0867DC1F9743FC97A924CB29A42ADB6A8E7DEA7____Parser_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void RelativeKeypoint_tB0867DC1F9743FC97A924CB29A42ADB6A8E7DEA7_CustomAttributesCacheGenerator_RelativeKeypoint_tB0867DC1F9743FC97A924CB29A42ADB6A8E7DEA7____Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void RelativeKeypoint_tB0867DC1F9743FC97A924CB29A42ADB6A8E7DEA7_CustomAttributesCacheGenerator_RelativeKeypoint_tB0867DC1F9743FC97A924CB29A42ADB6A8E7DEA7____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void RelativeKeypoint_tB0867DC1F9743FC97A924CB29A42ADB6A8E7DEA7_CustomAttributesCacheGenerator_RelativeKeypoint_tB0867DC1F9743FC97A924CB29A42ADB6A8E7DEA7____X_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void RelativeKeypoint_tB0867DC1F9743FC97A924CB29A42ADB6A8E7DEA7_CustomAttributesCacheGenerator_RelativeKeypoint_tB0867DC1F9743FC97A924CB29A42ADB6A8E7DEA7____HasX_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void RelativeKeypoint_tB0867DC1F9743FC97A924CB29A42ADB6A8E7DEA7_CustomAttributesCacheGenerator_RelativeKeypoint_tB0867DC1F9743FC97A924CB29A42ADB6A8E7DEA7____Y_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void RelativeKeypoint_tB0867DC1F9743FC97A924CB29A42ADB6A8E7DEA7_CustomAttributesCacheGenerator_RelativeKeypoint_tB0867DC1F9743FC97A924CB29A42ADB6A8E7DEA7____HasY_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void RelativeKeypoint_tB0867DC1F9743FC97A924CB29A42ADB6A8E7DEA7_CustomAttributesCacheGenerator_RelativeKeypoint_tB0867DC1F9743FC97A924CB29A42ADB6A8E7DEA7____KeypointLabel_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void RelativeKeypoint_tB0867DC1F9743FC97A924CB29A42ADB6A8E7DEA7_CustomAttributesCacheGenerator_RelativeKeypoint_tB0867DC1F9743FC97A924CB29A42ADB6A8E7DEA7____HasKeypointLabel_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void RelativeKeypoint_tB0867DC1F9743FC97A924CB29A42ADB6A8E7DEA7_CustomAttributesCacheGenerator_RelativeKeypoint_tB0867DC1F9743FC97A924CB29A42ADB6A8E7DEA7____Score_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void RelativeKeypoint_tB0867DC1F9743FC97A924CB29A42ADB6A8E7DEA7_CustomAttributesCacheGenerator_RelativeKeypoint_tB0867DC1F9743FC97A924CB29A42ADB6A8E7DEA7____HasScore_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void U3CU3Ec_t433A9FEB53731C105E43E59259348CC37F31B113_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_t0F7D4D63D876240F046F147802C3F30B87725A5C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MatrixData_tA28091B9C8107B5FED6284DDF6D1AEFE2969E0C4_CustomAttributesCacheGenerator_MatrixData__ctor_m08760B404A31B2AE0141BB152C5765B20FE3D999(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MatrixData_tA28091B9C8107B5FED6284DDF6D1AEFE2969E0C4_CustomAttributesCacheGenerator_MatrixData__ctor_m190F97D56239F0C19F454C92501EABC70460EF0D(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MatrixData_tA28091B9C8107B5FED6284DDF6D1AEFE2969E0C4_CustomAttributesCacheGenerator_MatrixData_Clone_mCE53C6458C28502B3440957A054F15E1555A2906(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MatrixData_tA28091B9C8107B5FED6284DDF6D1AEFE2969E0C4_CustomAttributesCacheGenerator_MatrixData_ClearRows_mE0E54CD921FE4772B8F80BBDB6B3026AA9077887(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MatrixData_tA28091B9C8107B5FED6284DDF6D1AEFE2969E0C4_CustomAttributesCacheGenerator_MatrixData_ClearCols_m05C85B672E3F314D316EAFF6C74CFC81278869DF(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MatrixData_tA28091B9C8107B5FED6284DDF6D1AEFE2969E0C4_CustomAttributesCacheGenerator_MatrixData_ClearLayout_m2140EFC522A01F79894555FBD578D33E12C684B1(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MatrixData_tA28091B9C8107B5FED6284DDF6D1AEFE2969E0C4_CustomAttributesCacheGenerator_MatrixData_Equals_mAB317070368E9C8FAC655E625805350B1FB0C314(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MatrixData_tA28091B9C8107B5FED6284DDF6D1AEFE2969E0C4_CustomAttributesCacheGenerator_MatrixData_Equals_mFF2B5393EA1A1F13B59FE28679F4FD1946BA8D42(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MatrixData_tA28091B9C8107B5FED6284DDF6D1AEFE2969E0C4_CustomAttributesCacheGenerator_MatrixData_GetHashCode_m43DDD0B77ECB6989C34D865D720E8E4F60AB8113(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MatrixData_tA28091B9C8107B5FED6284DDF6D1AEFE2969E0C4_CustomAttributesCacheGenerator_MatrixData_ToString_m14D4258EAB83BC97E759D73DAD0B8BBB2B78DEB3(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MatrixData_tA28091B9C8107B5FED6284DDF6D1AEFE2969E0C4_CustomAttributesCacheGenerator_MatrixData_WriteTo_m3B87386A2E13C7A8B682CCA7020DF01A57860798(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MatrixData_tA28091B9C8107B5FED6284DDF6D1AEFE2969E0C4_CustomAttributesCacheGenerator_MatrixData_CalculateSize_mB7488B8687D6F10921E34FCB8CCBA3C565BE5CD5(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MatrixData_tA28091B9C8107B5FED6284DDF6D1AEFE2969E0C4_CustomAttributesCacheGenerator_MatrixData_MergeFrom_m0AB33B94558F79353F9AE8E471A759FA65E828FB(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MatrixData_tA28091B9C8107B5FED6284DDF6D1AEFE2969E0C4_CustomAttributesCacheGenerator_MatrixData_MergeFrom_m547674A1C5CD7A86BF22973FA6FDF51A6DD056F5(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MatrixData_tA28091B9C8107B5FED6284DDF6D1AEFE2969E0C4_CustomAttributesCacheGenerator_MatrixData_tA28091B9C8107B5FED6284DDF6D1AEFE2969E0C4____Parser_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MatrixData_tA28091B9C8107B5FED6284DDF6D1AEFE2969E0C4_CustomAttributesCacheGenerator_MatrixData_tA28091B9C8107B5FED6284DDF6D1AEFE2969E0C4____Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MatrixData_tA28091B9C8107B5FED6284DDF6D1AEFE2969E0C4_CustomAttributesCacheGenerator_MatrixData_tA28091B9C8107B5FED6284DDF6D1AEFE2969E0C4____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MatrixData_tA28091B9C8107B5FED6284DDF6D1AEFE2969E0C4_CustomAttributesCacheGenerator_MatrixData_tA28091B9C8107B5FED6284DDF6D1AEFE2969E0C4____Rows_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MatrixData_tA28091B9C8107B5FED6284DDF6D1AEFE2969E0C4_CustomAttributesCacheGenerator_MatrixData_tA28091B9C8107B5FED6284DDF6D1AEFE2969E0C4____HasRows_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MatrixData_tA28091B9C8107B5FED6284DDF6D1AEFE2969E0C4_CustomAttributesCacheGenerator_MatrixData_tA28091B9C8107B5FED6284DDF6D1AEFE2969E0C4____Cols_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MatrixData_tA28091B9C8107B5FED6284DDF6D1AEFE2969E0C4_CustomAttributesCacheGenerator_MatrixData_tA28091B9C8107B5FED6284DDF6D1AEFE2969E0C4____HasCols_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MatrixData_tA28091B9C8107B5FED6284DDF6D1AEFE2969E0C4_CustomAttributesCacheGenerator_MatrixData_tA28091B9C8107B5FED6284DDF6D1AEFE2969E0C4____PackedData_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MatrixData_tA28091B9C8107B5FED6284DDF6D1AEFE2969E0C4_CustomAttributesCacheGenerator_MatrixData_tA28091B9C8107B5FED6284DDF6D1AEFE2969E0C4____Layout_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MatrixData_tA28091B9C8107B5FED6284DDF6D1AEFE2969E0C4_CustomAttributesCacheGenerator_MatrixData_tA28091B9C8107B5FED6284DDF6D1AEFE2969E0C4____HasLayout_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Types_t41118EF0E36EA11B08B5B30ECA357999DAFD8242_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Layout_t19C106ABE5F32E78A70B3F998158682CEA644D62_CustomAttributesCacheGenerator_ColumnMajor(CustomAttributesCache* cache)
{
	{
		OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * tmp = (OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 *)cache->attributes[0];
		OriginalNameAttribute__ctor_mB0C6C38C49B9261D2DC423D692E80B8EBC657BF9(tmp, il2cpp_codegen_string_new_wrapper("\x43\x4F\x4C\x55\x4D\x4E\x5F\x4D\x41\x4A\x4F\x52"), NULL);
	}
}
static void Layout_t19C106ABE5F32E78A70B3F998158682CEA644D62_CustomAttributesCacheGenerator_RowMajor(CustomAttributesCache* cache)
{
	{
		OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * tmp = (OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 *)cache->attributes[0];
		OriginalNameAttribute__ctor_mB0C6C38C49B9261D2DC423D692E80B8EBC657BF9(tmp, il2cpp_codegen_string_new_wrapper("\x52\x4F\x57\x5F\x4D\x41\x4A\x4F\x52"), NULL);
	}
}
static void U3CU3Ec_t315CB76305CAB55D659B19E611FCFA811CA0F2B1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MediaPipeOptions_t9D271404AA6309644B1A0AEDF6116DD119F4D4D8_CustomAttributesCacheGenerator_MediaPipeOptions__ctor_m94ECAD8AA33546C341D7764097D40351B2E4AB54(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MediaPipeOptions_t9D271404AA6309644B1A0AEDF6116DD119F4D4D8_CustomAttributesCacheGenerator_MediaPipeOptions__ctor_m54624D466F21E59CB280A7DB9FA5BA1B99ED1C1C(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MediaPipeOptions_t9D271404AA6309644B1A0AEDF6116DD119F4D4D8_CustomAttributesCacheGenerator_MediaPipeOptions_Clone_m4BDE025E942E472425387DEF8F63146B8C2B6EC4(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MediaPipeOptions_t9D271404AA6309644B1A0AEDF6116DD119F4D4D8_CustomAttributesCacheGenerator_MediaPipeOptions_Equals_m5A11BC1C09E74CC69DB83298DBA5003DD8A65366(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MediaPipeOptions_t9D271404AA6309644B1A0AEDF6116DD119F4D4D8_CustomAttributesCacheGenerator_MediaPipeOptions_Equals_mED7E91CF2EDBAFBCE5E1C2BA2374F0A4E5F48006(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MediaPipeOptions_t9D271404AA6309644B1A0AEDF6116DD119F4D4D8_CustomAttributesCacheGenerator_MediaPipeOptions_GetHashCode_m6C05D27EF2C543C70EC43C7DB712D028BDBE654E(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MediaPipeOptions_t9D271404AA6309644B1A0AEDF6116DD119F4D4D8_CustomAttributesCacheGenerator_MediaPipeOptions_ToString_m46611F2B164EAE51886F328CCC4213104A41C208(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MediaPipeOptions_t9D271404AA6309644B1A0AEDF6116DD119F4D4D8_CustomAttributesCacheGenerator_MediaPipeOptions_WriteTo_mB5CF6A852AD743614BD0B5CC247564DF783C1AC0(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MediaPipeOptions_t9D271404AA6309644B1A0AEDF6116DD119F4D4D8_CustomAttributesCacheGenerator_MediaPipeOptions_CalculateSize_m161CBE1D9EF8D1A82B6D3EFADF0533D1541F34D2(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MediaPipeOptions_t9D271404AA6309644B1A0AEDF6116DD119F4D4D8_CustomAttributesCacheGenerator_MediaPipeOptions_MergeFrom_m169687073AE0664B8B7070D1578ACB9A006A28FC(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MediaPipeOptions_t9D271404AA6309644B1A0AEDF6116DD119F4D4D8_CustomAttributesCacheGenerator_MediaPipeOptions_MergeFrom_m0F16A641B688B2AACE098CE2DD7ED6AF2A33B500(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MediaPipeOptions_t9D271404AA6309644B1A0AEDF6116DD119F4D4D8_CustomAttributesCacheGenerator_MediaPipeOptions_t9D271404AA6309644B1A0AEDF6116DD119F4D4D8____Parser_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MediaPipeOptions_t9D271404AA6309644B1A0AEDF6116DD119F4D4D8_CustomAttributesCacheGenerator_MediaPipeOptions_t9D271404AA6309644B1A0AEDF6116DD119F4D4D8____Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void MediaPipeOptions_t9D271404AA6309644B1A0AEDF6116DD119F4D4D8_CustomAttributesCacheGenerator_MediaPipeOptions_t9D271404AA6309644B1A0AEDF6116DD119F4D4D8____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void U3CU3Ec_t7AD00D32F4CAD5CFD23A067EBB2ADB3953C65232_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TimedModelMatrixProto_t1FEBAF26F6D2794B20DF340436C746FB747382B0_CustomAttributesCacheGenerator_TimedModelMatrixProto__ctor_mD36B8BF1A3255A20D213DB8179CE15F8B90FF922(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void TimedModelMatrixProto_t1FEBAF26F6D2794B20DF340436C746FB747382B0_CustomAttributesCacheGenerator_TimedModelMatrixProto__ctor_m4DFAF5A291060B008DC81DDEB764F5FFC7501617(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void TimedModelMatrixProto_t1FEBAF26F6D2794B20DF340436C746FB747382B0_CustomAttributesCacheGenerator_TimedModelMatrixProto_Clone_m000B8BFEEAA7E9C9B5F816E65A2E524BC67AAE48(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void TimedModelMatrixProto_t1FEBAF26F6D2794B20DF340436C746FB747382B0_CustomAttributesCacheGenerator_TimedModelMatrixProto_ClearTimeMsec_mD9625E2C729D54656E3F68610553639CADF865B0(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void TimedModelMatrixProto_t1FEBAF26F6D2794B20DF340436C746FB747382B0_CustomAttributesCacheGenerator_TimedModelMatrixProto_ClearId_m2380F94A5FB0693F86B1C7856B0ED842B1C29156(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void TimedModelMatrixProto_t1FEBAF26F6D2794B20DF340436C746FB747382B0_CustomAttributesCacheGenerator_TimedModelMatrixProto_Equals_m10573AEDA812F98D2A73FD3D99A8C7FBF788F49D(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void TimedModelMatrixProto_t1FEBAF26F6D2794B20DF340436C746FB747382B0_CustomAttributesCacheGenerator_TimedModelMatrixProto_Equals_m8EAAFDB34A86DB247D89A253C9B5A9F969D6AF72(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void TimedModelMatrixProto_t1FEBAF26F6D2794B20DF340436C746FB747382B0_CustomAttributesCacheGenerator_TimedModelMatrixProto_GetHashCode_m97FD870375066873755CDB0CD8C667D72EF2E26B(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void TimedModelMatrixProto_t1FEBAF26F6D2794B20DF340436C746FB747382B0_CustomAttributesCacheGenerator_TimedModelMatrixProto_ToString_mF73FC8377B34FB364A3471DF34952AEDE0CCFAFB(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void TimedModelMatrixProto_t1FEBAF26F6D2794B20DF340436C746FB747382B0_CustomAttributesCacheGenerator_TimedModelMatrixProto_WriteTo_mE3FC56B10FDEA6B63530855BADD6886B006346AC(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void TimedModelMatrixProto_t1FEBAF26F6D2794B20DF340436C746FB747382B0_CustomAttributesCacheGenerator_TimedModelMatrixProto_CalculateSize_mD7343FF3B98B11A38CB8AD302EBFE05EE5E5B118(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void TimedModelMatrixProto_t1FEBAF26F6D2794B20DF340436C746FB747382B0_CustomAttributesCacheGenerator_TimedModelMatrixProto_MergeFrom_m19C51BFF2F65BC61EC38D1EAA2E1C6212A57874E(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void TimedModelMatrixProto_t1FEBAF26F6D2794B20DF340436C746FB747382B0_CustomAttributesCacheGenerator_TimedModelMatrixProto_MergeFrom_m6CFBC87CE67964F667F449E67BE3488D319F373C(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void TimedModelMatrixProto_t1FEBAF26F6D2794B20DF340436C746FB747382B0_CustomAttributesCacheGenerator_TimedModelMatrixProto_t1FEBAF26F6D2794B20DF340436C746FB747382B0____Parser_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void TimedModelMatrixProto_t1FEBAF26F6D2794B20DF340436C746FB747382B0_CustomAttributesCacheGenerator_TimedModelMatrixProto_t1FEBAF26F6D2794B20DF340436C746FB747382B0____Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void TimedModelMatrixProto_t1FEBAF26F6D2794B20DF340436C746FB747382B0_CustomAttributesCacheGenerator_TimedModelMatrixProto_t1FEBAF26F6D2794B20DF340436C746FB747382B0____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void TimedModelMatrixProto_t1FEBAF26F6D2794B20DF340436C746FB747382B0_CustomAttributesCacheGenerator_TimedModelMatrixProto_t1FEBAF26F6D2794B20DF340436C746FB747382B0____MatrixEntries_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void TimedModelMatrixProto_t1FEBAF26F6D2794B20DF340436C746FB747382B0_CustomAttributesCacheGenerator_TimedModelMatrixProto_t1FEBAF26F6D2794B20DF340436C746FB747382B0____TimeMsec_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void TimedModelMatrixProto_t1FEBAF26F6D2794B20DF340436C746FB747382B0_CustomAttributesCacheGenerator_TimedModelMatrixProto_t1FEBAF26F6D2794B20DF340436C746FB747382B0____HasTimeMsec_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void TimedModelMatrixProto_t1FEBAF26F6D2794B20DF340436C746FB747382B0_CustomAttributesCacheGenerator_TimedModelMatrixProto_t1FEBAF26F6D2794B20DF340436C746FB747382B0____Id_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void TimedModelMatrixProto_t1FEBAF26F6D2794B20DF340436C746FB747382B0_CustomAttributesCacheGenerator_TimedModelMatrixProto_t1FEBAF26F6D2794B20DF340436C746FB747382B0____HasId_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void U3CU3Ec_t552C0FA1438471E2E3EB8F77546E51FC0171BFD6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TimedModelMatrixProtoList_t3A75AC882CCC8C5C7CE330E62547C87220B1A7A7_CustomAttributesCacheGenerator_TimedModelMatrixProtoList__ctor_mAFE5CBDBDF8ABDE5ED930F230AE053920BA6E6E3(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void TimedModelMatrixProtoList_t3A75AC882CCC8C5C7CE330E62547C87220B1A7A7_CustomAttributesCacheGenerator_TimedModelMatrixProtoList__ctor_mFEE41524C860DD57FF001AC92B9BAAA89B731A2A(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void TimedModelMatrixProtoList_t3A75AC882CCC8C5C7CE330E62547C87220B1A7A7_CustomAttributesCacheGenerator_TimedModelMatrixProtoList_Clone_m78CD67CD3A408D9D83041DE3F507EE53552BAA05(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void TimedModelMatrixProtoList_t3A75AC882CCC8C5C7CE330E62547C87220B1A7A7_CustomAttributesCacheGenerator_TimedModelMatrixProtoList_Equals_mE02E4E54D0605023A5A9B0D54412D1CCBBAC674B(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void TimedModelMatrixProtoList_t3A75AC882CCC8C5C7CE330E62547C87220B1A7A7_CustomAttributesCacheGenerator_TimedModelMatrixProtoList_Equals_m4B7E6B649A3541ACE89D9FF48E789EC4EF7E9871(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void TimedModelMatrixProtoList_t3A75AC882CCC8C5C7CE330E62547C87220B1A7A7_CustomAttributesCacheGenerator_TimedModelMatrixProtoList_GetHashCode_m39E976CEB249D20888704E62BE2A62F7AF48717B(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void TimedModelMatrixProtoList_t3A75AC882CCC8C5C7CE330E62547C87220B1A7A7_CustomAttributesCacheGenerator_TimedModelMatrixProtoList_ToString_m8A8860A75EBC20B5EECB9BD5D546DD26FA31AF23(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void TimedModelMatrixProtoList_t3A75AC882CCC8C5C7CE330E62547C87220B1A7A7_CustomAttributesCacheGenerator_TimedModelMatrixProtoList_WriteTo_mF9E2D1D5B4B98D4ECCD1CCD3B66353DC9FC0C4A7(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void TimedModelMatrixProtoList_t3A75AC882CCC8C5C7CE330E62547C87220B1A7A7_CustomAttributesCacheGenerator_TimedModelMatrixProtoList_CalculateSize_mDFA55794734AFC296355D7F42C8E1DEF5DA7F5FB(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void TimedModelMatrixProtoList_t3A75AC882CCC8C5C7CE330E62547C87220B1A7A7_CustomAttributesCacheGenerator_TimedModelMatrixProtoList_MergeFrom_m92F8B84288012603724555016046AEB0B943A1A5(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void TimedModelMatrixProtoList_t3A75AC882CCC8C5C7CE330E62547C87220B1A7A7_CustomAttributesCacheGenerator_TimedModelMatrixProtoList_MergeFrom_m4DF276D53054577C044EB5E3C85EC49FA2A4A248(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void TimedModelMatrixProtoList_t3A75AC882CCC8C5C7CE330E62547C87220B1A7A7_CustomAttributesCacheGenerator_TimedModelMatrixProtoList_t3A75AC882CCC8C5C7CE330E62547C87220B1A7A7____Parser_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void TimedModelMatrixProtoList_t3A75AC882CCC8C5C7CE330E62547C87220B1A7A7_CustomAttributesCacheGenerator_TimedModelMatrixProtoList_t3A75AC882CCC8C5C7CE330E62547C87220B1A7A7____Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void TimedModelMatrixProtoList_t3A75AC882CCC8C5C7CE330E62547C87220B1A7A7_CustomAttributesCacheGenerator_TimedModelMatrixProtoList_t3A75AC882CCC8C5C7CE330E62547C87220B1A7A7____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void TimedModelMatrixProtoList_t3A75AC882CCC8C5C7CE330E62547C87220B1A7A7_CustomAttributesCacheGenerator_TimedModelMatrixProtoList_t3A75AC882CCC8C5C7CE330E62547C87220B1A7A7____ModelMatrix_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void U3CU3Ec_t897629C0EB8099BA4C18CDC8B2661E9BB4E27DB1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TimedVectorProto_t922BD38BFDF681636210E2476928157FE2C69DE7_CustomAttributesCacheGenerator_TimedVectorProto__ctor_m94A7FB7ED78DA1A40E402F3BB017C3E5AE052F23(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void TimedVectorProto_t922BD38BFDF681636210E2476928157FE2C69DE7_CustomAttributesCacheGenerator_TimedVectorProto__ctor_m7A2E61E4FE4C04BFDB2103BC80DA8857BE16A6AF(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void TimedVectorProto_t922BD38BFDF681636210E2476928157FE2C69DE7_CustomAttributesCacheGenerator_TimedVectorProto_Clone_mAA2BCF6CF36B58DD176F497A2D68C2EC34915E49(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void TimedVectorProto_t922BD38BFDF681636210E2476928157FE2C69DE7_CustomAttributesCacheGenerator_TimedVectorProto_ClearTimeMsec_m1B1C0E25E3372C399F5D14B10F2638788BC4F348(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void TimedVectorProto_t922BD38BFDF681636210E2476928157FE2C69DE7_CustomAttributesCacheGenerator_TimedVectorProto_ClearId_m40D90F7DE18B8A1340531B1C587E9FA67FBA3014(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void TimedVectorProto_t922BD38BFDF681636210E2476928157FE2C69DE7_CustomAttributesCacheGenerator_TimedVectorProto_Equals_m32738DC0662B6DD5CF4D2E5E97F07CD4D8137803(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void TimedVectorProto_t922BD38BFDF681636210E2476928157FE2C69DE7_CustomAttributesCacheGenerator_TimedVectorProto_Equals_m699DE31F04C68A804A9C34511BD3F83CC63BF990(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void TimedVectorProto_t922BD38BFDF681636210E2476928157FE2C69DE7_CustomAttributesCacheGenerator_TimedVectorProto_GetHashCode_mB2A72327CB3A9C28014E0DCCCCD9916B8184296C(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void TimedVectorProto_t922BD38BFDF681636210E2476928157FE2C69DE7_CustomAttributesCacheGenerator_TimedVectorProto_ToString_mD6CC9AC2C547209D3590CC7ED638AFAD4D4E1785(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void TimedVectorProto_t922BD38BFDF681636210E2476928157FE2C69DE7_CustomAttributesCacheGenerator_TimedVectorProto_WriteTo_mA00DD38C663E0821F0CF37CA65F2619F7B15396E(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void TimedVectorProto_t922BD38BFDF681636210E2476928157FE2C69DE7_CustomAttributesCacheGenerator_TimedVectorProto_CalculateSize_mCA0C76E99A476908DA0E9C2FA1CD7E00EE1314D5(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void TimedVectorProto_t922BD38BFDF681636210E2476928157FE2C69DE7_CustomAttributesCacheGenerator_TimedVectorProto_MergeFrom_m210826B18001527082F4BD2726C8E7B1755A5387(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void TimedVectorProto_t922BD38BFDF681636210E2476928157FE2C69DE7_CustomAttributesCacheGenerator_TimedVectorProto_MergeFrom_m3497545D1E9A10B36DBE67104C293FB1B37FAB18(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void TimedVectorProto_t922BD38BFDF681636210E2476928157FE2C69DE7_CustomAttributesCacheGenerator_TimedVectorProto_t922BD38BFDF681636210E2476928157FE2C69DE7____Parser_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void TimedVectorProto_t922BD38BFDF681636210E2476928157FE2C69DE7_CustomAttributesCacheGenerator_TimedVectorProto_t922BD38BFDF681636210E2476928157FE2C69DE7____Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void TimedVectorProto_t922BD38BFDF681636210E2476928157FE2C69DE7_CustomAttributesCacheGenerator_TimedVectorProto_t922BD38BFDF681636210E2476928157FE2C69DE7____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void TimedVectorProto_t922BD38BFDF681636210E2476928157FE2C69DE7_CustomAttributesCacheGenerator_TimedVectorProto_t922BD38BFDF681636210E2476928157FE2C69DE7____VectorEntries_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void TimedVectorProto_t922BD38BFDF681636210E2476928157FE2C69DE7_CustomAttributesCacheGenerator_TimedVectorProto_t922BD38BFDF681636210E2476928157FE2C69DE7____TimeMsec_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void TimedVectorProto_t922BD38BFDF681636210E2476928157FE2C69DE7_CustomAttributesCacheGenerator_TimedVectorProto_t922BD38BFDF681636210E2476928157FE2C69DE7____HasTimeMsec_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void TimedVectorProto_t922BD38BFDF681636210E2476928157FE2C69DE7_CustomAttributesCacheGenerator_TimedVectorProto_t922BD38BFDF681636210E2476928157FE2C69DE7____Id_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void TimedVectorProto_t922BD38BFDF681636210E2476928157FE2C69DE7_CustomAttributesCacheGenerator_TimedVectorProto_t922BD38BFDF681636210E2476928157FE2C69DE7____HasId_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void U3CU3Ec_tBA5B02F40AEC74B652F459E12D3554F2272E84E7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TimedVectorProtoList_t628C533965DB55ADC9A8993A87569E9EA7B6033F_CustomAttributesCacheGenerator_TimedVectorProtoList__ctor_m7AFAE896482EDF3711A6DA99A412D64487EF2F91(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void TimedVectorProtoList_t628C533965DB55ADC9A8993A87569E9EA7B6033F_CustomAttributesCacheGenerator_TimedVectorProtoList__ctor_mBB786126903E375611CE37FB8B7ADB4030EA0C1E(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void TimedVectorProtoList_t628C533965DB55ADC9A8993A87569E9EA7B6033F_CustomAttributesCacheGenerator_TimedVectorProtoList_Clone_mA692C92F4F46F28CF480F28E95932FEE2130FDA8(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void TimedVectorProtoList_t628C533965DB55ADC9A8993A87569E9EA7B6033F_CustomAttributesCacheGenerator_TimedVectorProtoList_Equals_m70165213BF6F474B1105E05389301339CF9CA6ED(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void TimedVectorProtoList_t628C533965DB55ADC9A8993A87569E9EA7B6033F_CustomAttributesCacheGenerator_TimedVectorProtoList_Equals_m090C5D733BA5CA3D26DCFC59F13DD2C413025B4A(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void TimedVectorProtoList_t628C533965DB55ADC9A8993A87569E9EA7B6033F_CustomAttributesCacheGenerator_TimedVectorProtoList_GetHashCode_mBE5776B4DEF84AD23EAE943A16440C264D819347(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void TimedVectorProtoList_t628C533965DB55ADC9A8993A87569E9EA7B6033F_CustomAttributesCacheGenerator_TimedVectorProtoList_ToString_m89D6D023EF065EB7214BD2F6C4A5B793AEC47BBA(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void TimedVectorProtoList_t628C533965DB55ADC9A8993A87569E9EA7B6033F_CustomAttributesCacheGenerator_TimedVectorProtoList_WriteTo_m7883C3162842BB6B2D11C97A03E61ED8B0625074(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void TimedVectorProtoList_t628C533965DB55ADC9A8993A87569E9EA7B6033F_CustomAttributesCacheGenerator_TimedVectorProtoList_CalculateSize_mC4270F8CBA1A98987570383F71806D85E13AC42F(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void TimedVectorProtoList_t628C533965DB55ADC9A8993A87569E9EA7B6033F_CustomAttributesCacheGenerator_TimedVectorProtoList_MergeFrom_m1C123548C738E2DEE42C6BDF207533DD92EE55D5(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void TimedVectorProtoList_t628C533965DB55ADC9A8993A87569E9EA7B6033F_CustomAttributesCacheGenerator_TimedVectorProtoList_MergeFrom_m5F9AFAA0BE3F80732ED106238D19067945C8E53A(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void TimedVectorProtoList_t628C533965DB55ADC9A8993A87569E9EA7B6033F_CustomAttributesCacheGenerator_TimedVectorProtoList_t628C533965DB55ADC9A8993A87569E9EA7B6033F____Parser_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void TimedVectorProtoList_t628C533965DB55ADC9A8993A87569E9EA7B6033F_CustomAttributesCacheGenerator_TimedVectorProtoList_t628C533965DB55ADC9A8993A87569E9EA7B6033F____Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void TimedVectorProtoList_t628C533965DB55ADC9A8993A87569E9EA7B6033F_CustomAttributesCacheGenerator_TimedVectorProtoList_t628C533965DB55ADC9A8993A87569E9EA7B6033F____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void TimedVectorProtoList_t628C533965DB55ADC9A8993A87569E9EA7B6033F_CustomAttributesCacheGenerator_TimedVectorProtoList_t628C533965DB55ADC9A8993A87569E9EA7B6033F____VectorList_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void U3CU3Ec_t9C4AE8D4C53A22FD81C8E9F5681FD9F93C191257_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PacketFactoryOptions_tEA685DCB184FF4693C4C66A20AA649B6D0214850_CustomAttributesCacheGenerator_PacketFactoryOptions__ctor_mC8536EB222928EED726DC5B86DDECD96B930DAB0(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketFactoryOptions_tEA685DCB184FF4693C4C66A20AA649B6D0214850_CustomAttributesCacheGenerator_PacketFactoryOptions__ctor_m2D36055064EC0015518ED210217D3E3E04CB050F(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketFactoryOptions_tEA685DCB184FF4693C4C66A20AA649B6D0214850_CustomAttributesCacheGenerator_PacketFactoryOptions_Clone_m81FDE11474A9B170510320D3F0F5D581E367B408(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketFactoryOptions_tEA685DCB184FF4693C4C66A20AA649B6D0214850_CustomAttributesCacheGenerator_PacketFactoryOptions_Equals_mE75139A5773D01442C993D3D8C76EF65159320E9(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketFactoryOptions_tEA685DCB184FF4693C4C66A20AA649B6D0214850_CustomAttributesCacheGenerator_PacketFactoryOptions_Equals_mB3693914B1867B7CB7DE1969EA290CEA81D43E79(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketFactoryOptions_tEA685DCB184FF4693C4C66A20AA649B6D0214850_CustomAttributesCacheGenerator_PacketFactoryOptions_GetHashCode_m21D7D747BBA561125D4372391A037A706D72734E(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketFactoryOptions_tEA685DCB184FF4693C4C66A20AA649B6D0214850_CustomAttributesCacheGenerator_PacketFactoryOptions_ToString_m55FF16DB4F9E721D5593642FE08504579D0F4D53(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketFactoryOptions_tEA685DCB184FF4693C4C66A20AA649B6D0214850_CustomAttributesCacheGenerator_PacketFactoryOptions_WriteTo_m9D660E8331CCC27AAE98146FD46C62C3E702FECD(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketFactoryOptions_tEA685DCB184FF4693C4C66A20AA649B6D0214850_CustomAttributesCacheGenerator_PacketFactoryOptions_CalculateSize_m269B700BA480E8E38BF52DBF256336CF709EBD72(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketFactoryOptions_tEA685DCB184FF4693C4C66A20AA649B6D0214850_CustomAttributesCacheGenerator_PacketFactoryOptions_MergeFrom_m0D0FDA7834EBF6390A437209C82B5E59A32E1843(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketFactoryOptions_tEA685DCB184FF4693C4C66A20AA649B6D0214850_CustomAttributesCacheGenerator_PacketFactoryOptions_MergeFrom_m6AB31546F93E43834CD182777A573B5FD0F0B284(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketFactoryOptions_tEA685DCB184FF4693C4C66A20AA649B6D0214850_CustomAttributesCacheGenerator_PacketFactoryOptions_tEA685DCB184FF4693C4C66A20AA649B6D0214850____Parser_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketFactoryOptions_tEA685DCB184FF4693C4C66A20AA649B6D0214850_CustomAttributesCacheGenerator_PacketFactoryOptions_tEA685DCB184FF4693C4C66A20AA649B6D0214850____Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketFactoryOptions_tEA685DCB184FF4693C4C66A20AA649B6D0214850_CustomAttributesCacheGenerator_PacketFactoryOptions_tEA685DCB184FF4693C4C66A20AA649B6D0214850____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void U3CU3Ec_t7DC7111FDD47602F0B2A651B90589895FF76461E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PacketFactoryConfig_t5BA5CF02AEB213652B28644DDE0D5D5A21703E12_CustomAttributesCacheGenerator_PacketFactoryConfig__ctor_mB19EA1662F94AA4F76B3E366F68C4BEF5DE34E8A(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketFactoryConfig_t5BA5CF02AEB213652B28644DDE0D5D5A21703E12_CustomAttributesCacheGenerator_PacketFactoryConfig__ctor_m16A584150178AEF55F2E4F659D408B01ABACB481(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketFactoryConfig_t5BA5CF02AEB213652B28644DDE0D5D5A21703E12_CustomAttributesCacheGenerator_PacketFactoryConfig_Clone_m27D51A4834DC6F5343FF317372508AC2FA7B08EF(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketFactoryConfig_t5BA5CF02AEB213652B28644DDE0D5D5A21703E12_CustomAttributesCacheGenerator_PacketFactoryConfig_ClearPacketFactory_m82423E2A65876B507F2260501E7B5FE4871B5FD4(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketFactoryConfig_t5BA5CF02AEB213652B28644DDE0D5D5A21703E12_CustomAttributesCacheGenerator_PacketFactoryConfig_ClearOutputSidePacket_mF630C0F005F394FA52F5FEEAC784C45B2A9F400D(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketFactoryConfig_t5BA5CF02AEB213652B28644DDE0D5D5A21703E12_CustomAttributesCacheGenerator_PacketFactoryConfig_ClearExternalOutput_mDFEABBD4FE8E94CE783946760A19F73B23B7099D(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketFactoryConfig_t5BA5CF02AEB213652B28644DDE0D5D5A21703E12_CustomAttributesCacheGenerator_PacketFactoryConfig_ClearOptions_mFF0B8AD05101A7B34DEB196164800C630203DF72(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketFactoryConfig_t5BA5CF02AEB213652B28644DDE0D5D5A21703E12_CustomAttributesCacheGenerator_PacketFactoryConfig_Equals_m2F80DE3C163CE19D35574287766C1E85FEBA44A0(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketFactoryConfig_t5BA5CF02AEB213652B28644DDE0D5D5A21703E12_CustomAttributesCacheGenerator_PacketFactoryConfig_Equals_m0FEDD1353DBB452259CACE6FFF69AAC41D5EDC3D(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketFactoryConfig_t5BA5CF02AEB213652B28644DDE0D5D5A21703E12_CustomAttributesCacheGenerator_PacketFactoryConfig_GetHashCode_m037E8FB9D77E8B663BB9490F53F7746B5954FD01(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketFactoryConfig_t5BA5CF02AEB213652B28644DDE0D5D5A21703E12_CustomAttributesCacheGenerator_PacketFactoryConfig_ToString_m081F5FBD927673C09D7F63168C6A45C50CBBF37C(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketFactoryConfig_t5BA5CF02AEB213652B28644DDE0D5D5A21703E12_CustomAttributesCacheGenerator_PacketFactoryConfig_WriteTo_m182894E780B31E210F4E177DAE8F2438CEC90D56(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketFactoryConfig_t5BA5CF02AEB213652B28644DDE0D5D5A21703E12_CustomAttributesCacheGenerator_PacketFactoryConfig_CalculateSize_m989B4B743104D755C0E33BBFD98C7F2B734AC4D5(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketFactoryConfig_t5BA5CF02AEB213652B28644DDE0D5D5A21703E12_CustomAttributesCacheGenerator_PacketFactoryConfig_MergeFrom_mE285F8F83524EF28E8C0F1B20CCC431D5E9D61A8(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketFactoryConfig_t5BA5CF02AEB213652B28644DDE0D5D5A21703E12_CustomAttributesCacheGenerator_PacketFactoryConfig_MergeFrom_mABEF4433CD66253E3B53FE07C3937BC2E9A8513D(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketFactoryConfig_t5BA5CF02AEB213652B28644DDE0D5D5A21703E12_CustomAttributesCacheGenerator_PacketFactoryConfig_t5BA5CF02AEB213652B28644DDE0D5D5A21703E12____Parser_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketFactoryConfig_t5BA5CF02AEB213652B28644DDE0D5D5A21703E12_CustomAttributesCacheGenerator_PacketFactoryConfig_t5BA5CF02AEB213652B28644DDE0D5D5A21703E12____Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketFactoryConfig_t5BA5CF02AEB213652B28644DDE0D5D5A21703E12_CustomAttributesCacheGenerator_PacketFactoryConfig_t5BA5CF02AEB213652B28644DDE0D5D5A21703E12____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketFactoryConfig_t5BA5CF02AEB213652B28644DDE0D5D5A21703E12_CustomAttributesCacheGenerator_PacketFactoryConfig_t5BA5CF02AEB213652B28644DDE0D5D5A21703E12____PacketFactory_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketFactoryConfig_t5BA5CF02AEB213652B28644DDE0D5D5A21703E12_CustomAttributesCacheGenerator_PacketFactoryConfig_t5BA5CF02AEB213652B28644DDE0D5D5A21703E12____HasPacketFactory_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketFactoryConfig_t5BA5CF02AEB213652B28644DDE0D5D5A21703E12_CustomAttributesCacheGenerator_PacketFactoryConfig_t5BA5CF02AEB213652B28644DDE0D5D5A21703E12____OutputSidePacket_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketFactoryConfig_t5BA5CF02AEB213652B28644DDE0D5D5A21703E12_CustomAttributesCacheGenerator_PacketFactoryConfig_t5BA5CF02AEB213652B28644DDE0D5D5A21703E12____HasOutputSidePacket_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketFactoryConfig_t5BA5CF02AEB213652B28644DDE0D5D5A21703E12_CustomAttributesCacheGenerator_PacketFactoryConfig_t5BA5CF02AEB213652B28644DDE0D5D5A21703E12____ExternalOutput_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketFactoryConfig_t5BA5CF02AEB213652B28644DDE0D5D5A21703E12_CustomAttributesCacheGenerator_PacketFactoryConfig_t5BA5CF02AEB213652B28644DDE0D5D5A21703E12____HasExternalOutput_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketFactoryConfig_t5BA5CF02AEB213652B28644DDE0D5D5A21703E12_CustomAttributesCacheGenerator_PacketFactoryConfig_t5BA5CF02AEB213652B28644DDE0D5D5A21703E12____Options_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketFactoryConfig_t5BA5CF02AEB213652B28644DDE0D5D5A21703E12_CustomAttributesCacheGenerator_PacketFactoryConfig_t5BA5CF02AEB213652B28644DDE0D5D5A21703E12____HasOptions_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void U3CU3Ec_t69C8F92901AD650DAF4D59DA40081E7AF96A0076_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PacketManagerConfig_t324C1B93AB9471F4658F17482A26B89C0BFAB519_CustomAttributesCacheGenerator_PacketManagerConfig__ctor_m882F227FF8DBA0BF8E8743CE05BDC22CE76E7644(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketManagerConfig_t324C1B93AB9471F4658F17482A26B89C0BFAB519_CustomAttributesCacheGenerator_PacketManagerConfig__ctor_m9878FDFCDCEC6AF84637F38F1CBEA9DD5F5EE570(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketManagerConfig_t324C1B93AB9471F4658F17482A26B89C0BFAB519_CustomAttributesCacheGenerator_PacketManagerConfig_Clone_m4E2F5485BEF8CE62F49391651D7448EDC63F3093(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketManagerConfig_t324C1B93AB9471F4658F17482A26B89C0BFAB519_CustomAttributesCacheGenerator_PacketManagerConfig_Equals_m11D2FCCE2BC53BE6796B761C6DF983D05199371B(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketManagerConfig_t324C1B93AB9471F4658F17482A26B89C0BFAB519_CustomAttributesCacheGenerator_PacketManagerConfig_Equals_m448C26325B5F0F63AD1511314AD36AE88FF75B53(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketManagerConfig_t324C1B93AB9471F4658F17482A26B89C0BFAB519_CustomAttributesCacheGenerator_PacketManagerConfig_GetHashCode_mF97E5240FC7AC7599CF9878408E09169FD99F992(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketManagerConfig_t324C1B93AB9471F4658F17482A26B89C0BFAB519_CustomAttributesCacheGenerator_PacketManagerConfig_ToString_mB91FC8BD3FD4FA215BB48D84E60A40ECD8FE301A(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketManagerConfig_t324C1B93AB9471F4658F17482A26B89C0BFAB519_CustomAttributesCacheGenerator_PacketManagerConfig_WriteTo_mC1BB483BA2A79844AAC30C9902ADD2178C765B9A(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketManagerConfig_t324C1B93AB9471F4658F17482A26B89C0BFAB519_CustomAttributesCacheGenerator_PacketManagerConfig_CalculateSize_m9B17003F4E07D2FBA15072AA9FA611E9808C9CEE(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketManagerConfig_t324C1B93AB9471F4658F17482A26B89C0BFAB519_CustomAttributesCacheGenerator_PacketManagerConfig_MergeFrom_mFCFF46E5C7B7FF00042DB64CA4117549B4DA6379(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketManagerConfig_t324C1B93AB9471F4658F17482A26B89C0BFAB519_CustomAttributesCacheGenerator_PacketManagerConfig_MergeFrom_m47C7CA9A0C39ABF84078F6AE1383CD0D40816174(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketManagerConfig_t324C1B93AB9471F4658F17482A26B89C0BFAB519_CustomAttributesCacheGenerator_PacketManagerConfig_t324C1B93AB9471F4658F17482A26B89C0BFAB519____Parser_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketManagerConfig_t324C1B93AB9471F4658F17482A26B89C0BFAB519_CustomAttributesCacheGenerator_PacketManagerConfig_t324C1B93AB9471F4658F17482A26B89C0BFAB519____Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketManagerConfig_t324C1B93AB9471F4658F17482A26B89C0BFAB519_CustomAttributesCacheGenerator_PacketManagerConfig_t324C1B93AB9471F4658F17482A26B89C0BFAB519____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketManagerConfig_t324C1B93AB9471F4658F17482A26B89C0BFAB519_CustomAttributesCacheGenerator_PacketManagerConfig_t324C1B93AB9471F4658F17482A26B89C0BFAB519____Packet_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void U3CU3Ec_t641B3DD9BE9C53B9CC228EF94DBB115BEA013BFF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PacketGeneratorOptions_t103747F5D200028AFE532E63B3E64BE05BF8523A_CustomAttributesCacheGenerator_PacketGeneratorOptions__ctor_mC663EB5FC659CA75449E64A002B4CC10C15360AF(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketGeneratorOptions_t103747F5D200028AFE532E63B3E64BE05BF8523A_CustomAttributesCacheGenerator_PacketGeneratorOptions__ctor_m74543604138A39F4D112E514CE0C9DD4B53BCF39(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketGeneratorOptions_t103747F5D200028AFE532E63B3E64BE05BF8523A_CustomAttributesCacheGenerator_PacketGeneratorOptions_Clone_m058684255081A07E04B4D5EBB30D264D95C2C2BB(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketGeneratorOptions_t103747F5D200028AFE532E63B3E64BE05BF8523A_CustomAttributesCacheGenerator_PacketGeneratorOptions_Equals_m7695E0C7DB1C89DF8F186CB8265472DA3469D9F7(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketGeneratorOptions_t103747F5D200028AFE532E63B3E64BE05BF8523A_CustomAttributesCacheGenerator_PacketGeneratorOptions_Equals_mB3A3471F6EE5C8A3374CBE6DDAD98102A07FBF37(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketGeneratorOptions_t103747F5D200028AFE532E63B3E64BE05BF8523A_CustomAttributesCacheGenerator_PacketGeneratorOptions_GetHashCode_m23D64E3A72F45C6FF8E682520689880DDC187C9F(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketGeneratorOptions_t103747F5D200028AFE532E63B3E64BE05BF8523A_CustomAttributesCacheGenerator_PacketGeneratorOptions_ToString_mE964D3F9F6EC1FEA51DD82143A1F65695F253A18(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketGeneratorOptions_t103747F5D200028AFE532E63B3E64BE05BF8523A_CustomAttributesCacheGenerator_PacketGeneratorOptions_WriteTo_mA2C4C9ABD31A321D10AFA8D9841EEAAA230FE944(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketGeneratorOptions_t103747F5D200028AFE532E63B3E64BE05BF8523A_CustomAttributesCacheGenerator_PacketGeneratorOptions_CalculateSize_m52382B6749DB349EF633CC5ECFC63AF2A00C82CE(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketGeneratorOptions_t103747F5D200028AFE532E63B3E64BE05BF8523A_CustomAttributesCacheGenerator_PacketGeneratorOptions_MergeFrom_m554F607119A2A19278375B13E27CD2AF9B9D9AC5(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketGeneratorOptions_t103747F5D200028AFE532E63B3E64BE05BF8523A_CustomAttributesCacheGenerator_PacketGeneratorOptions_MergeFrom_m0CADE6A187BDA996B2480109B50A87D215468C2C(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketGeneratorOptions_t103747F5D200028AFE532E63B3E64BE05BF8523A_CustomAttributesCacheGenerator_PacketGeneratorOptions_t103747F5D200028AFE532E63B3E64BE05BF8523A____Parser_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketGeneratorOptions_t103747F5D200028AFE532E63B3E64BE05BF8523A_CustomAttributesCacheGenerator_PacketGeneratorOptions_t103747F5D200028AFE532E63B3E64BE05BF8523A____Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketGeneratorOptions_t103747F5D200028AFE532E63B3E64BE05BF8523A_CustomAttributesCacheGenerator_PacketGeneratorOptions_t103747F5D200028AFE532E63B3E64BE05BF8523A____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void U3CU3Ec_t94ED63BBD9E83B387631119D44A2F2BF2ED8CF85_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PacketGeneratorConfig_tB75C9DFBB65CAE892E81E36B9BEFB10CBC226A4F_CustomAttributesCacheGenerator_PacketGeneratorConfig__ctor_m3DD06F90644406E25B61D2CD6105787D7A59DC2E(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketGeneratorConfig_tB75C9DFBB65CAE892E81E36B9BEFB10CBC226A4F_CustomAttributesCacheGenerator_PacketGeneratorConfig__ctor_m6380447A5817E1DB9BA8031BC40A897E3EC7FADB(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketGeneratorConfig_tB75C9DFBB65CAE892E81E36B9BEFB10CBC226A4F_CustomAttributesCacheGenerator_PacketGeneratorConfig_Clone_m7862B8297F86CA31E1A69BC23BBA6819771AB397(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketGeneratorConfig_tB75C9DFBB65CAE892E81E36B9BEFB10CBC226A4F_CustomAttributesCacheGenerator_PacketGeneratorConfig_ClearPacketGenerator_m8F3C91BC62A1B11E3AFAB4AC8CDB798997E4927A(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketGeneratorConfig_tB75C9DFBB65CAE892E81E36B9BEFB10CBC226A4F_CustomAttributesCacheGenerator_PacketGeneratorConfig_ClearOptions_m397FE10C8A8D3BC5CF70E4D76CA66579F88946AF(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketGeneratorConfig_tB75C9DFBB65CAE892E81E36B9BEFB10CBC226A4F_CustomAttributesCacheGenerator_PacketGeneratorConfig_Equals_mFF71F177C5357C6ADF18AE90B67B1B030D2B230D(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketGeneratorConfig_tB75C9DFBB65CAE892E81E36B9BEFB10CBC226A4F_CustomAttributesCacheGenerator_PacketGeneratorConfig_Equals_mAFF8CA1140FFA35F3A7F59B55F34C582BB5BECC7(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketGeneratorConfig_tB75C9DFBB65CAE892E81E36B9BEFB10CBC226A4F_CustomAttributesCacheGenerator_PacketGeneratorConfig_GetHashCode_m67C2EE0089FDA8A17452E8789A9226BF7497F2EB(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketGeneratorConfig_tB75C9DFBB65CAE892E81E36B9BEFB10CBC226A4F_CustomAttributesCacheGenerator_PacketGeneratorConfig_ToString_mF8E0F155AB39F730DC8D028AEBF3F122AC9CF76C(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketGeneratorConfig_tB75C9DFBB65CAE892E81E36B9BEFB10CBC226A4F_CustomAttributesCacheGenerator_PacketGeneratorConfig_WriteTo_mAD2E8F441993B1349F73DB355982F3018A9826DB(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketGeneratorConfig_tB75C9DFBB65CAE892E81E36B9BEFB10CBC226A4F_CustomAttributesCacheGenerator_PacketGeneratorConfig_CalculateSize_m42D473EA3CB305950C349CC960667081855F7408(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketGeneratorConfig_tB75C9DFBB65CAE892E81E36B9BEFB10CBC226A4F_CustomAttributesCacheGenerator_PacketGeneratorConfig_MergeFrom_m56A617696F27E0E76700262130434884884C864A(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketGeneratorConfig_tB75C9DFBB65CAE892E81E36B9BEFB10CBC226A4F_CustomAttributesCacheGenerator_PacketGeneratorConfig_MergeFrom_m972AAB330709CD60D6DF3A296350D7FE9154B6D9(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketGeneratorConfig_tB75C9DFBB65CAE892E81E36B9BEFB10CBC226A4F_CustomAttributesCacheGenerator_PacketGeneratorConfig_tB75C9DFBB65CAE892E81E36B9BEFB10CBC226A4F____Parser_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketGeneratorConfig_tB75C9DFBB65CAE892E81E36B9BEFB10CBC226A4F_CustomAttributesCacheGenerator_PacketGeneratorConfig_tB75C9DFBB65CAE892E81E36B9BEFB10CBC226A4F____Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketGeneratorConfig_tB75C9DFBB65CAE892E81E36B9BEFB10CBC226A4F_CustomAttributesCacheGenerator_PacketGeneratorConfig_tB75C9DFBB65CAE892E81E36B9BEFB10CBC226A4F____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketGeneratorConfig_tB75C9DFBB65CAE892E81E36B9BEFB10CBC226A4F_CustomAttributesCacheGenerator_PacketGeneratorConfig_tB75C9DFBB65CAE892E81E36B9BEFB10CBC226A4F____PacketGenerator_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketGeneratorConfig_tB75C9DFBB65CAE892E81E36B9BEFB10CBC226A4F_CustomAttributesCacheGenerator_PacketGeneratorConfig_tB75C9DFBB65CAE892E81E36B9BEFB10CBC226A4F____HasPacketGenerator_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketGeneratorConfig_tB75C9DFBB65CAE892E81E36B9BEFB10CBC226A4F_CustomAttributesCacheGenerator_PacketGeneratorConfig_tB75C9DFBB65CAE892E81E36B9BEFB10CBC226A4F____InputSidePacket_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketGeneratorConfig_tB75C9DFBB65CAE892E81E36B9BEFB10CBC226A4F_CustomAttributesCacheGenerator_PacketGeneratorConfig_tB75C9DFBB65CAE892E81E36B9BEFB10CBC226A4F____ExternalInput_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketGeneratorConfig_tB75C9DFBB65CAE892E81E36B9BEFB10CBC226A4F_CustomAttributesCacheGenerator_PacketGeneratorConfig_tB75C9DFBB65CAE892E81E36B9BEFB10CBC226A4F____OutputSidePacket_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketGeneratorConfig_tB75C9DFBB65CAE892E81E36B9BEFB10CBC226A4F_CustomAttributesCacheGenerator_PacketGeneratorConfig_tB75C9DFBB65CAE892E81E36B9BEFB10CBC226A4F____ExternalOutput_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketGeneratorConfig_tB75C9DFBB65CAE892E81E36B9BEFB10CBC226A4F_CustomAttributesCacheGenerator_PacketGeneratorConfig_tB75C9DFBB65CAE892E81E36B9BEFB10CBC226A4F____Options_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void PacketGeneratorConfig_tB75C9DFBB65CAE892E81E36B9BEFB10CBC226A4F_CustomAttributesCacheGenerator_PacketGeneratorConfig_tB75C9DFBB65CAE892E81E36B9BEFB10CBC226A4F____HasOptions_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void U3CU3Ec_t3EA88371F6F338476D12B97179D74101FDC1EFF1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Rasterization_tF4ACD4173A2965257259025AB28357C2B065D6A3_CustomAttributesCacheGenerator_Rasterization__ctor_m55FC6BC6C94655B9C8CE0385CB44C1E81716DBB7(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Rasterization_tF4ACD4173A2965257259025AB28357C2B065D6A3_CustomAttributesCacheGenerator_Rasterization__ctor_mD3697404E5C77CE873F448E92932C6CC0C18681C(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Rasterization_tF4ACD4173A2965257259025AB28357C2B065D6A3_CustomAttributesCacheGenerator_Rasterization_Clone_m72998D69821C34E51500317806C6B8EF5C86EC6C(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Rasterization_tF4ACD4173A2965257259025AB28357C2B065D6A3_CustomAttributesCacheGenerator_Rasterization_Equals_mA13E8A4D5D7B8915FF57D3B9C72C364DB37A2F1A(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Rasterization_tF4ACD4173A2965257259025AB28357C2B065D6A3_CustomAttributesCacheGenerator_Rasterization_Equals_m7DAE4E1872E2285E1981E16DC001030FC42A1EFB(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Rasterization_tF4ACD4173A2965257259025AB28357C2B065D6A3_CustomAttributesCacheGenerator_Rasterization_GetHashCode_m7C644190483001AC74DD0288F66F99E3993351F5(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Rasterization_tF4ACD4173A2965257259025AB28357C2B065D6A3_CustomAttributesCacheGenerator_Rasterization_ToString_m0020CC92D76AC7E76D9770A619E69E0C9E22B2B3(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Rasterization_tF4ACD4173A2965257259025AB28357C2B065D6A3_CustomAttributesCacheGenerator_Rasterization_WriteTo_mA8B9BAFEBF0E6AAC7056B5922700F05AAFDFD6E4(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Rasterization_tF4ACD4173A2965257259025AB28357C2B065D6A3_CustomAttributesCacheGenerator_Rasterization_CalculateSize_m136A9B8A9D2FBF3A682F7BF2053C1588FA35AB5D(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Rasterization_tF4ACD4173A2965257259025AB28357C2B065D6A3_CustomAttributesCacheGenerator_Rasterization_MergeFrom_m70BDB2F744263C31C8E738B4122D7FDE11E0225C(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Rasterization_tF4ACD4173A2965257259025AB28357C2B065D6A3_CustomAttributesCacheGenerator_Rasterization_MergeFrom_mF5B9FC0CBCF3484AD858AC847AB6712619FADE13(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Rasterization_tF4ACD4173A2965257259025AB28357C2B065D6A3_CustomAttributesCacheGenerator_Rasterization_tF4ACD4173A2965257259025AB28357C2B065D6A3____Parser_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Rasterization_tF4ACD4173A2965257259025AB28357C2B065D6A3_CustomAttributesCacheGenerator_Rasterization_tF4ACD4173A2965257259025AB28357C2B065D6A3____Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Rasterization_tF4ACD4173A2965257259025AB28357C2B065D6A3_CustomAttributesCacheGenerator_Rasterization_tF4ACD4173A2965257259025AB28357C2B065D6A3____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Rasterization_tF4ACD4173A2965257259025AB28357C2B065D6A3_CustomAttributesCacheGenerator_Rasterization_tF4ACD4173A2965257259025AB28357C2B065D6A3____Interval_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Types_tB9721F5EF014CDBFC226E47769A872B2F647AA57_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Interval_t800C77AD1BB854B8EA764973820554E84A27A8B5_CustomAttributesCacheGenerator_Interval__ctor_m32A3957EBFFE3B98D584CE5835A00D77CEA1F3BE(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Interval_t800C77AD1BB854B8EA764973820554E84A27A8B5_CustomAttributesCacheGenerator_Interval__ctor_m5E2DC10FD5A9373055DC59F91A2DF4AE47E32644(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Interval_t800C77AD1BB854B8EA764973820554E84A27A8B5_CustomAttributesCacheGenerator_Interval_Clone_m07446964FC63DB770BDDB2850439BF01C88B3E53(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Interval_t800C77AD1BB854B8EA764973820554E84A27A8B5_CustomAttributesCacheGenerator_Interval_ClearY_m9A1D81264BB6A7171F645F141D26B16B11FF59A1(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Interval_t800C77AD1BB854B8EA764973820554E84A27A8B5_CustomAttributesCacheGenerator_Interval_ClearLeftX_m18139A501E8C9A583BEA41E83544720908938204(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Interval_t800C77AD1BB854B8EA764973820554E84A27A8B5_CustomAttributesCacheGenerator_Interval_ClearRightX_m6EF10A9D5EA463F0C7E06C15932A66A8A7974662(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Interval_t800C77AD1BB854B8EA764973820554E84A27A8B5_CustomAttributesCacheGenerator_Interval_Equals_m694D5F474327657E1C30F07C2AF9F10C9B24F43F(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Interval_t800C77AD1BB854B8EA764973820554E84A27A8B5_CustomAttributesCacheGenerator_Interval_Equals_mFE99EB6740C0735C445F4AADA222DD792A4E98E4(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Interval_t800C77AD1BB854B8EA764973820554E84A27A8B5_CustomAttributesCacheGenerator_Interval_GetHashCode_mC8E3DB33E588E151786C01B8160CE01B2898412A(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Interval_t800C77AD1BB854B8EA764973820554E84A27A8B5_CustomAttributesCacheGenerator_Interval_ToString_m01323C62EB2CA2032A43994675B1F80BC6868CCC(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Interval_t800C77AD1BB854B8EA764973820554E84A27A8B5_CustomAttributesCacheGenerator_Interval_WriteTo_m501236942A2D0E72E74799CD107E6C04E780731B(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Interval_t800C77AD1BB854B8EA764973820554E84A27A8B5_CustomAttributesCacheGenerator_Interval_CalculateSize_m1A75A81A7A6EB5AB4EDF015348B58F3CC4C7DE95(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Interval_t800C77AD1BB854B8EA764973820554E84A27A8B5_CustomAttributesCacheGenerator_Interval_MergeFrom_m8E8A02B6433AD6798D67E8F86A48B73039E311C1(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Interval_t800C77AD1BB854B8EA764973820554E84A27A8B5_CustomAttributesCacheGenerator_Interval_MergeFrom_m08CA5B392DC43CB8FC5959A8E28146884E6C7376(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Interval_t800C77AD1BB854B8EA764973820554E84A27A8B5_CustomAttributesCacheGenerator_Interval_t800C77AD1BB854B8EA764973820554E84A27A8B5____Parser_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Interval_t800C77AD1BB854B8EA764973820554E84A27A8B5_CustomAttributesCacheGenerator_Interval_t800C77AD1BB854B8EA764973820554E84A27A8B5____Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Interval_t800C77AD1BB854B8EA764973820554E84A27A8B5_CustomAttributesCacheGenerator_Interval_t800C77AD1BB854B8EA764973820554E84A27A8B5____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Interval_t800C77AD1BB854B8EA764973820554E84A27A8B5_CustomAttributesCacheGenerator_Interval_t800C77AD1BB854B8EA764973820554E84A27A8B5____Y_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Interval_t800C77AD1BB854B8EA764973820554E84A27A8B5_CustomAttributesCacheGenerator_Interval_t800C77AD1BB854B8EA764973820554E84A27A8B5____HasY_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Interval_t800C77AD1BB854B8EA764973820554E84A27A8B5_CustomAttributesCacheGenerator_Interval_t800C77AD1BB854B8EA764973820554E84A27A8B5____LeftX_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Interval_t800C77AD1BB854B8EA764973820554E84A27A8B5_CustomAttributesCacheGenerator_Interval_t800C77AD1BB854B8EA764973820554E84A27A8B5____HasLeftX_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Interval_t800C77AD1BB854B8EA764973820554E84A27A8B5_CustomAttributesCacheGenerator_Interval_t800C77AD1BB854B8EA764973820554E84A27A8B5____RightX_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Interval_t800C77AD1BB854B8EA764973820554E84A27A8B5_CustomAttributesCacheGenerator_Interval_t800C77AD1BB854B8EA764973820554E84A27A8B5____HasRightX_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void U3CU3Ec_t02AB85E0260AE1954E4CC6A2A5821F35406E5537_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_tD985A5315A493FBEBE7939DDDC973BC03EB68AC3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF_CustomAttributesCacheGenerator_Rect__ctor_mA1A953C00E9D1E93F5FE705F28869BF03281E90E(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF_CustomAttributesCacheGenerator_Rect__ctor_m3ABE7AB6875A28451F7D68FCD2316A5FFD6A33CF(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF_CustomAttributesCacheGenerator_Rect_Clone_m32E8FD503464BCE245D992DC44220A17FCD192CF(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF_CustomAttributesCacheGenerator_Rect_ClearXCenter_m907A2A14A98D9BE8337AB11E1E11BA6D82CA38C9(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF_CustomAttributesCacheGenerator_Rect_ClearYCenter_mFC560214642EE24645202364FB8544833F84C9D9(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF_CustomAttributesCacheGenerator_Rect_ClearHeight_m5311A4542887AA3987EAEB56C1EE174BBC46EF90(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF_CustomAttributesCacheGenerator_Rect_ClearWidth_m2A0EF58EE901101AFA25FA5ED9C7EE9F7990E0D3(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF_CustomAttributesCacheGenerator_Rect_ClearRotation_m935A6E03D8225670E9D90057502E99B479893CBA(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF_CustomAttributesCacheGenerator_Rect_ClearRectId_m2582A0FD1E0F7B073ACCB7C566AD4CD67810589B(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF_CustomAttributesCacheGenerator_Rect_Equals_m8D8318A274A804C8F7EC5995536C2C5AA121DAE4(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF_CustomAttributesCacheGenerator_Rect_Equals_m8DDA30D9B6EFF7387D1E42982812D783565B971A(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF_CustomAttributesCacheGenerator_Rect_GetHashCode_m894D11D8F145F7800CD41C46D4923F6701EA73BF(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF_CustomAttributesCacheGenerator_Rect_ToString_m9026BF1BE9688FBA3BBAA49AACA2F0624BE20546(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF_CustomAttributesCacheGenerator_Rect_WriteTo_m91BA7F768E68CEA159FE04DC30808C0E98FDBBF2(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF_CustomAttributesCacheGenerator_Rect_CalculateSize_m458355AAEB1961AA3E49528C669FC23E89FF06F0(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF_CustomAttributesCacheGenerator_Rect_MergeFrom_m04A5B2CFE20DC31FF14B0EB09212D6B01842C229(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF_CustomAttributesCacheGenerator_Rect_MergeFrom_m7AF35F7AF17270258910584DC176688CC509CA98(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF_CustomAttributesCacheGenerator_Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF____Parser_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF_CustomAttributesCacheGenerator_Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF____Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF_CustomAttributesCacheGenerator_Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF_CustomAttributesCacheGenerator_Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF____XCenter_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF_CustomAttributesCacheGenerator_Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF____HasXCenter_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF_CustomAttributesCacheGenerator_Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF____YCenter_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF_CustomAttributesCacheGenerator_Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF____HasYCenter_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF_CustomAttributesCacheGenerator_Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF____Height_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF_CustomAttributesCacheGenerator_Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF____HasHeight_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF_CustomAttributesCacheGenerator_Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF____Width_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF_CustomAttributesCacheGenerator_Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF____HasWidth_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF_CustomAttributesCacheGenerator_Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF____Rotation_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF_CustomAttributesCacheGenerator_Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF____HasRotation_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF_CustomAttributesCacheGenerator_Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF____RectId_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF_CustomAttributesCacheGenerator_Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF____HasRectId_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void U3CU3Ec_t9D156C4D9D79C37329D73ECCA6307890C18699AB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F_CustomAttributesCacheGenerator_NormalizedRect__ctor_m3BDEDD59629DCC814DE948B03777DC7E9919548F(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F_CustomAttributesCacheGenerator_NormalizedRect__ctor_m8EA938ADE44DBD9BA74A4EA26BFF929BA398F502(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F_CustomAttributesCacheGenerator_NormalizedRect_Clone_m8B1DF6560577979C5C4AD7B9924DE8A9682BDA7D(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F_CustomAttributesCacheGenerator_NormalizedRect_ClearXCenter_m6BB5D3B7A5D3DEE0964F2ADB0EFB015498C971D3(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F_CustomAttributesCacheGenerator_NormalizedRect_ClearYCenter_m964FB56A39DF73D2EBABDF9B3AB8E4B525BF68BB(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F_CustomAttributesCacheGenerator_NormalizedRect_ClearHeight_m53F519FBB27E41F1015BBF0F9B4F943CB99D521E(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F_CustomAttributesCacheGenerator_NormalizedRect_ClearWidth_m57AD3453E31C13D518C962CB8E89D347AC9A05F5(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F_CustomAttributesCacheGenerator_NormalizedRect_ClearRotation_m7743803A9EE445B91B7637283BAC2D3783CC3323(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F_CustomAttributesCacheGenerator_NormalizedRect_ClearRectId_m1C7E71F564CAE806DEC00BFA1C8EC5E9A7C76006(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F_CustomAttributesCacheGenerator_NormalizedRect_Equals_m5A0389CD1BF8AFEEBA109FE3BC72A84F2B69E2BE(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F_CustomAttributesCacheGenerator_NormalizedRect_Equals_m4650183705C1C20A3686FCE49AA990E2CCFA9167(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F_CustomAttributesCacheGenerator_NormalizedRect_GetHashCode_m2835A32974C581C5AACF9EA8640D3499FAE29B5A(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F_CustomAttributesCacheGenerator_NormalizedRect_ToString_mD9680EE1C02BBFA4E938610290B3E2AAA4AAA896(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F_CustomAttributesCacheGenerator_NormalizedRect_WriteTo_m223F03100F382A13B791A585F64ABB578CB91BF3(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F_CustomAttributesCacheGenerator_NormalizedRect_CalculateSize_mBE22B1A36E88C86ED0DB31FBEE65AB6B6A35CB63(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F_CustomAttributesCacheGenerator_NormalizedRect_MergeFrom_mDBA715172C2C850F99074679573E1BB7FF7589E1(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F_CustomAttributesCacheGenerator_NormalizedRect_MergeFrom_m0469B3573A2B64594C4690D3E91CD1C41EFD382E(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F_CustomAttributesCacheGenerator_NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F____Parser_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F_CustomAttributesCacheGenerator_NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F____Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F_CustomAttributesCacheGenerator_NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F_CustomAttributesCacheGenerator_NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F____XCenter_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F_CustomAttributesCacheGenerator_NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F____HasXCenter_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F_CustomAttributesCacheGenerator_NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F____YCenter_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F_CustomAttributesCacheGenerator_NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F____HasYCenter_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F_CustomAttributesCacheGenerator_NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F____Height_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F_CustomAttributesCacheGenerator_NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F____HasHeight_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F_CustomAttributesCacheGenerator_NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F____Width_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F_CustomAttributesCacheGenerator_NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F____HasWidth_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F_CustomAttributesCacheGenerator_NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F____Rotation_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F_CustomAttributesCacheGenerator_NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F____HasRotation_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F_CustomAttributesCacheGenerator_NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F____RectId_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F_CustomAttributesCacheGenerator_NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F____HasRectId_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void U3CU3Ec_t2583760C8CF53E7795A5CE053961AB9C7E50E775_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void StatusHandlerConfig_t91FFA23EB8EE9D668FCF2A8D0E8E308308A81985_CustomAttributesCacheGenerator_StatusHandlerConfig__ctor_m0C87DFFEF9B52D562AD68762DD83C895E084F1EC(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void StatusHandlerConfig_t91FFA23EB8EE9D668FCF2A8D0E8E308308A81985_CustomAttributesCacheGenerator_StatusHandlerConfig__ctor_mC337F4A5270315F9DF7FE4372468EDE59F5D9B1B(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void StatusHandlerConfig_t91FFA23EB8EE9D668FCF2A8D0E8E308308A81985_CustomAttributesCacheGenerator_StatusHandlerConfig_Clone_mD7BF122D61DDC2B08ACA4521444F692937EB82C1(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void StatusHandlerConfig_t91FFA23EB8EE9D668FCF2A8D0E8E308308A81985_CustomAttributesCacheGenerator_StatusHandlerConfig_ClearStatusHandler_m07CC6662A252212369F7D57B3E3A518A8F8DCA2B(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void StatusHandlerConfig_t91FFA23EB8EE9D668FCF2A8D0E8E308308A81985_CustomAttributesCacheGenerator_StatusHandlerConfig_ClearOptions_mE947E969C224326EA97B7903F732FEC89BFF1176(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void StatusHandlerConfig_t91FFA23EB8EE9D668FCF2A8D0E8E308308A81985_CustomAttributesCacheGenerator_StatusHandlerConfig_Equals_mE15F141F7DD9A8E50CE22E98E94843557421C569(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void StatusHandlerConfig_t91FFA23EB8EE9D668FCF2A8D0E8E308308A81985_CustomAttributesCacheGenerator_StatusHandlerConfig_Equals_mD1A6F9828313594E5C7EF144F5119F54EEE0435A(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void StatusHandlerConfig_t91FFA23EB8EE9D668FCF2A8D0E8E308308A81985_CustomAttributesCacheGenerator_StatusHandlerConfig_GetHashCode_mF6A6AD649A9387349F6B83D7844CDAECC44AA4C4(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void StatusHandlerConfig_t91FFA23EB8EE9D668FCF2A8D0E8E308308A81985_CustomAttributesCacheGenerator_StatusHandlerConfig_ToString_m128817CC910667012E7587B84888E8EB925DE190(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void StatusHandlerConfig_t91FFA23EB8EE9D668FCF2A8D0E8E308308A81985_CustomAttributesCacheGenerator_StatusHandlerConfig_WriteTo_m160C08E410413EC1615505EF407B79F1557ACE34(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void StatusHandlerConfig_t91FFA23EB8EE9D668FCF2A8D0E8E308308A81985_CustomAttributesCacheGenerator_StatusHandlerConfig_CalculateSize_mA82A950CEEDBF1E241EFEF0B855551966114782D(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void StatusHandlerConfig_t91FFA23EB8EE9D668FCF2A8D0E8E308308A81985_CustomAttributesCacheGenerator_StatusHandlerConfig_MergeFrom_m6F38CBD207080A9DE0CF68449CCC1AA881877239(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void StatusHandlerConfig_t91FFA23EB8EE9D668FCF2A8D0E8E308308A81985_CustomAttributesCacheGenerator_StatusHandlerConfig_MergeFrom_mAAE549448B55E008DBF0395D6BB87C183EF0ACCE(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void StatusHandlerConfig_t91FFA23EB8EE9D668FCF2A8D0E8E308308A81985_CustomAttributesCacheGenerator_StatusHandlerConfig_t91FFA23EB8EE9D668FCF2A8D0E8E308308A81985____Parser_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void StatusHandlerConfig_t91FFA23EB8EE9D668FCF2A8D0E8E308308A81985_CustomAttributesCacheGenerator_StatusHandlerConfig_t91FFA23EB8EE9D668FCF2A8D0E8E308308A81985____Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void StatusHandlerConfig_t91FFA23EB8EE9D668FCF2A8D0E8E308308A81985_CustomAttributesCacheGenerator_StatusHandlerConfig_t91FFA23EB8EE9D668FCF2A8D0E8E308308A81985____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void StatusHandlerConfig_t91FFA23EB8EE9D668FCF2A8D0E8E308308A81985_CustomAttributesCacheGenerator_StatusHandlerConfig_t91FFA23EB8EE9D668FCF2A8D0E8E308308A81985____StatusHandler_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void StatusHandlerConfig_t91FFA23EB8EE9D668FCF2A8D0E8E308308A81985_CustomAttributesCacheGenerator_StatusHandlerConfig_t91FFA23EB8EE9D668FCF2A8D0E8E308308A81985____HasStatusHandler_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void StatusHandlerConfig_t91FFA23EB8EE9D668FCF2A8D0E8E308308A81985_CustomAttributesCacheGenerator_StatusHandlerConfig_t91FFA23EB8EE9D668FCF2A8D0E8E308308A81985____InputSidePacket_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void StatusHandlerConfig_t91FFA23EB8EE9D668FCF2A8D0E8E308308A81985_CustomAttributesCacheGenerator_StatusHandlerConfig_t91FFA23EB8EE9D668FCF2A8D0E8E308308A81985____ExternalInput_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void StatusHandlerConfig_t91FFA23EB8EE9D668FCF2A8D0E8E308308A81985_CustomAttributesCacheGenerator_StatusHandlerConfig_t91FFA23EB8EE9D668FCF2A8D0E8E308308A81985____Options_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void StatusHandlerConfig_t91FFA23EB8EE9D668FCF2A8D0E8E308308A81985_CustomAttributesCacheGenerator_StatusHandlerConfig_t91FFA23EB8EE9D668FCF2A8D0E8E308308A81985____HasOptions_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void U3CU3Ec_t14F6C0C7A294A4C44A951230DC6D1A7D3E5FD87D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435_CustomAttributesCacheGenerator_Sticker__ctor_m9F07EA56D9484EC587B82322B0E0718DF88BF800(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435_CustomAttributesCacheGenerator_Sticker__ctor_m2E998EAE290A49D1067301693237681798D172D9(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435_CustomAttributesCacheGenerator_Sticker_Clone_mC616C2DA4B6978BDCE602EC25D2AEBEAC58CC7B2(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435_CustomAttributesCacheGenerator_Sticker_ClearId_m9A821A6CA49F2B6488490BC5012C1A372AA225B2(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435_CustomAttributesCacheGenerator_Sticker_ClearX_m81A2073EC1E6E28D522BA83A09D25B59054C10B4(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435_CustomAttributesCacheGenerator_Sticker_ClearY_mCCDB6ED5BBCC5470C536E9308EEDBF03EBC5A1FC(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435_CustomAttributesCacheGenerator_Sticker_ClearRotation_m706F6BE276F0822E52C34373C1A3A5DB0C7B3D9E(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435_CustomAttributesCacheGenerator_Sticker_ClearScale_m66C7A603A0A374BFD1C1973F4F3C30399BD6260A(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435_CustomAttributesCacheGenerator_Sticker_ClearRenderId_mEA835CE5755D482612166621EAC9FF4C05607150(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435_CustomAttributesCacheGenerator_Sticker_Equals_m35D29CDEEB1B41EC384FEC46477C4042D40B05AF(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435_CustomAttributesCacheGenerator_Sticker_Equals_m337E02F0A28B1447BA8B24631C35FF1C286E60C9(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435_CustomAttributesCacheGenerator_Sticker_GetHashCode_m24AFE04034BF649FC23E1E49AD29DEAEEE7C04AB(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435_CustomAttributesCacheGenerator_Sticker_ToString_m044B37DFDBABC8E510CCDB2944D1A7819B5149D0(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435_CustomAttributesCacheGenerator_Sticker_WriteTo_m0BAF019B7204C3399E1FA485E981DC36F94648DB(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435_CustomAttributesCacheGenerator_Sticker_CalculateSize_mB443FFC825293F7F0ED3AD2A415E4621EF3EA59B(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435_CustomAttributesCacheGenerator_Sticker_MergeFrom_mCF73E0750E5110F3AF4A75B6A09824D7B4484C9B(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435_CustomAttributesCacheGenerator_Sticker_MergeFrom_m9CADCC2E1344332FBCAC86D3FA7488F908175131(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435_CustomAttributesCacheGenerator_Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435____Parser_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435_CustomAttributesCacheGenerator_Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435____Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435_CustomAttributesCacheGenerator_Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435_CustomAttributesCacheGenerator_Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435____Id_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435_CustomAttributesCacheGenerator_Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435____HasId_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435_CustomAttributesCacheGenerator_Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435____X_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435_CustomAttributesCacheGenerator_Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435____HasX_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435_CustomAttributesCacheGenerator_Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435____Y_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435_CustomAttributesCacheGenerator_Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435____HasY_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435_CustomAttributesCacheGenerator_Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435____Rotation_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435_CustomAttributesCacheGenerator_Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435____HasRotation_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435_CustomAttributesCacheGenerator_Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435____Scale_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435_CustomAttributesCacheGenerator_Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435____HasScale_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435_CustomAttributesCacheGenerator_Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435____RenderId_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435_CustomAttributesCacheGenerator_Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435____HasRenderId_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void U3CU3Ec_tF5CF319F7865D36D0728338038A89565E9B43406_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void StickerRoll_t5CCFC6B2292625CF867C2C935AB4749B8FA97D59_CustomAttributesCacheGenerator_StickerRoll__ctor_m7D0E0B4B49A31F27819E3777EDFB6B58A11174DB(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void StickerRoll_t5CCFC6B2292625CF867C2C935AB4749B8FA97D59_CustomAttributesCacheGenerator_StickerRoll__ctor_mC0E9A98013535B1F5E13F0E01117B200EC09E1C7(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void StickerRoll_t5CCFC6B2292625CF867C2C935AB4749B8FA97D59_CustomAttributesCacheGenerator_StickerRoll_Clone_m096072A72723DB3BF8F7311D5E14236BAEC77850(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void StickerRoll_t5CCFC6B2292625CF867C2C935AB4749B8FA97D59_CustomAttributesCacheGenerator_StickerRoll_Equals_m3E5A8335E874E955AA1A7EB2D2400B4BC6A36F93(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void StickerRoll_t5CCFC6B2292625CF867C2C935AB4749B8FA97D59_CustomAttributesCacheGenerator_StickerRoll_Equals_m1583815F5C84B1A33349A25E7C1D10D9B454B786(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void StickerRoll_t5CCFC6B2292625CF867C2C935AB4749B8FA97D59_CustomAttributesCacheGenerator_StickerRoll_GetHashCode_mB2561E014D54E102F9091D03556DCF6D1A678F56(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void StickerRoll_t5CCFC6B2292625CF867C2C935AB4749B8FA97D59_CustomAttributesCacheGenerator_StickerRoll_ToString_m18198D57FDEDBBC074B162CC75414E505B20431E(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void StickerRoll_t5CCFC6B2292625CF867C2C935AB4749B8FA97D59_CustomAttributesCacheGenerator_StickerRoll_WriteTo_m152931EE67A3A849500DCF04913BBB3CEB5B549E(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void StickerRoll_t5CCFC6B2292625CF867C2C935AB4749B8FA97D59_CustomAttributesCacheGenerator_StickerRoll_CalculateSize_m57AA5A87B5891693324E260A333D8B70D013EC0C(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void StickerRoll_t5CCFC6B2292625CF867C2C935AB4749B8FA97D59_CustomAttributesCacheGenerator_StickerRoll_MergeFrom_m7339523DC45A8AF327B08EEC2E9BC55C83E0381A(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void StickerRoll_t5CCFC6B2292625CF867C2C935AB4749B8FA97D59_CustomAttributesCacheGenerator_StickerRoll_MergeFrom_m46C03127E58A827A900A8602C569D8E8343441F5(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void StickerRoll_t5CCFC6B2292625CF867C2C935AB4749B8FA97D59_CustomAttributesCacheGenerator_StickerRoll_t5CCFC6B2292625CF867C2C935AB4749B8FA97D59____Parser_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void StickerRoll_t5CCFC6B2292625CF867C2C935AB4749B8FA97D59_CustomAttributesCacheGenerator_StickerRoll_t5CCFC6B2292625CF867C2C935AB4749B8FA97D59____Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void StickerRoll_t5CCFC6B2292625CF867C2C935AB4749B8FA97D59_CustomAttributesCacheGenerator_StickerRoll_t5CCFC6B2292625CF867C2C935AB4749B8FA97D59____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void StickerRoll_t5CCFC6B2292625CF867C2C935AB4749B8FA97D59_CustomAttributesCacheGenerator_StickerRoll_t5CCFC6B2292625CF867C2C935AB4749B8FA97D59____Sticker_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void U3CU3Ec_t40AABDAE2D3CAAB5B72505754976026BA96DC51E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InputStreamHandlerConfig_t1BAFAC8382A20E901CDE2D3D7E86E757DD5E9D16_CustomAttributesCacheGenerator_InputStreamHandlerConfig__ctor_mDA2A0ADCFBE563186C6F1E56BDBDC3F19522EF52(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void InputStreamHandlerConfig_t1BAFAC8382A20E901CDE2D3D7E86E757DD5E9D16_CustomAttributesCacheGenerator_InputStreamHandlerConfig__ctor_m2A5888DCE80A2B2DB059CB563795F28C050114F9(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void InputStreamHandlerConfig_t1BAFAC8382A20E901CDE2D3D7E86E757DD5E9D16_CustomAttributesCacheGenerator_InputStreamHandlerConfig_Clone_m1C7D8601234B62154567A71C893796A3DD5BCFB8(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void InputStreamHandlerConfig_t1BAFAC8382A20E901CDE2D3D7E86E757DD5E9D16_CustomAttributesCacheGenerator_InputStreamHandlerConfig_ClearInputStreamHandler_m778DA275FBA99054E9B2367B45EE2DA982BE3CB3(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void InputStreamHandlerConfig_t1BAFAC8382A20E901CDE2D3D7E86E757DD5E9D16_CustomAttributesCacheGenerator_InputStreamHandlerConfig_ClearOptions_mC4B0ECD6F0DA723730F3E45FFDBE9D5AC42DFED4(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void InputStreamHandlerConfig_t1BAFAC8382A20E901CDE2D3D7E86E757DD5E9D16_CustomAttributesCacheGenerator_InputStreamHandlerConfig_Equals_m920E22E1FFCB71B026DD2541AE7343A1DC541032(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void InputStreamHandlerConfig_t1BAFAC8382A20E901CDE2D3D7E86E757DD5E9D16_CustomAttributesCacheGenerator_InputStreamHandlerConfig_Equals_mE3FF7A087C37572FAEDD71033B5856E49AA4BAE7(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void InputStreamHandlerConfig_t1BAFAC8382A20E901CDE2D3D7E86E757DD5E9D16_CustomAttributesCacheGenerator_InputStreamHandlerConfig_GetHashCode_m58B4E8D647A7C52BAC4306EBEBE35E370DA58F4B(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void InputStreamHandlerConfig_t1BAFAC8382A20E901CDE2D3D7E86E757DD5E9D16_CustomAttributesCacheGenerator_InputStreamHandlerConfig_ToString_m5A51A1B49D862152FB108FDD9E7EC26DBDD754C7(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void InputStreamHandlerConfig_t1BAFAC8382A20E901CDE2D3D7E86E757DD5E9D16_CustomAttributesCacheGenerator_InputStreamHandlerConfig_WriteTo_mCE51105C65479A27CC3078A2C761EAA386676EF1(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void InputStreamHandlerConfig_t1BAFAC8382A20E901CDE2D3D7E86E757DD5E9D16_CustomAttributesCacheGenerator_InputStreamHandlerConfig_CalculateSize_m4825B60F49C6987E66E0EA4124F5371E7517963C(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void InputStreamHandlerConfig_t1BAFAC8382A20E901CDE2D3D7E86E757DD5E9D16_CustomAttributesCacheGenerator_InputStreamHandlerConfig_MergeFrom_m5313E6C7EFF4B95CD73FCE7ED2B19FE92CA5B1A2(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void InputStreamHandlerConfig_t1BAFAC8382A20E901CDE2D3D7E86E757DD5E9D16_CustomAttributesCacheGenerator_InputStreamHandlerConfig_MergeFrom_mB0FF550FD90B38F91C9A89A027AF419A29D5B3E8(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void InputStreamHandlerConfig_t1BAFAC8382A20E901CDE2D3D7E86E757DD5E9D16_CustomAttributesCacheGenerator_InputStreamHandlerConfig_t1BAFAC8382A20E901CDE2D3D7E86E757DD5E9D16____Parser_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void InputStreamHandlerConfig_t1BAFAC8382A20E901CDE2D3D7E86E757DD5E9D16_CustomAttributesCacheGenerator_InputStreamHandlerConfig_t1BAFAC8382A20E901CDE2D3D7E86E757DD5E9D16____Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void InputStreamHandlerConfig_t1BAFAC8382A20E901CDE2D3D7E86E757DD5E9D16_CustomAttributesCacheGenerator_InputStreamHandlerConfig_t1BAFAC8382A20E901CDE2D3D7E86E757DD5E9D16____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void InputStreamHandlerConfig_t1BAFAC8382A20E901CDE2D3D7E86E757DD5E9D16_CustomAttributesCacheGenerator_InputStreamHandlerConfig_t1BAFAC8382A20E901CDE2D3D7E86E757DD5E9D16____InputStreamHandler_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void InputStreamHandlerConfig_t1BAFAC8382A20E901CDE2D3D7E86E757DD5E9D16_CustomAttributesCacheGenerator_InputStreamHandlerConfig_t1BAFAC8382A20E901CDE2D3D7E86E757DD5E9D16____HasInputStreamHandler_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void InputStreamHandlerConfig_t1BAFAC8382A20E901CDE2D3D7E86E757DD5E9D16_CustomAttributesCacheGenerator_InputStreamHandlerConfig_t1BAFAC8382A20E901CDE2D3D7E86E757DD5E9D16____Options_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void InputStreamHandlerConfig_t1BAFAC8382A20E901CDE2D3D7E86E757DD5E9D16_CustomAttributesCacheGenerator_InputStreamHandlerConfig_t1BAFAC8382A20E901CDE2D3D7E86E757DD5E9D16____HasOptions_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void U3CU3Ec_tC3B2DD5BBC15198CC3282D932BDC2EB383575443_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OutputStreamHandlerConfig_t472506B1EB46A165DEDFD74B41B42728C003617D_CustomAttributesCacheGenerator_OutputStreamHandlerConfig__ctor_m05C62D4D22FF9FF66AF9BB724990B2FFEF12520F(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void OutputStreamHandlerConfig_t472506B1EB46A165DEDFD74B41B42728C003617D_CustomAttributesCacheGenerator_OutputStreamHandlerConfig__ctor_m2C42D8AEFEC3AEA97E86245C7244C6186B1EC37B(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void OutputStreamHandlerConfig_t472506B1EB46A165DEDFD74B41B42728C003617D_CustomAttributesCacheGenerator_OutputStreamHandlerConfig_Clone_mC359C6B6F92C04CD25AA67A7F2E95F90D5837248(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void OutputStreamHandlerConfig_t472506B1EB46A165DEDFD74B41B42728C003617D_CustomAttributesCacheGenerator_OutputStreamHandlerConfig_ClearOutputStreamHandler_m1B66407BF083E4781A6BB19D8C0502845610C3E0(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void OutputStreamHandlerConfig_t472506B1EB46A165DEDFD74B41B42728C003617D_CustomAttributesCacheGenerator_OutputStreamHandlerConfig_ClearOptions_mE6629E3A15B7C6E8DA899E244B1473B6F533849C(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void OutputStreamHandlerConfig_t472506B1EB46A165DEDFD74B41B42728C003617D_CustomAttributesCacheGenerator_OutputStreamHandlerConfig_Equals_m6C32151DC752E25C8DEDB017643B7FA8BF3F356C(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void OutputStreamHandlerConfig_t472506B1EB46A165DEDFD74B41B42728C003617D_CustomAttributesCacheGenerator_OutputStreamHandlerConfig_Equals_m82C75029D3E419F810CAD772CCDE87705D433C2B(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void OutputStreamHandlerConfig_t472506B1EB46A165DEDFD74B41B42728C003617D_CustomAttributesCacheGenerator_OutputStreamHandlerConfig_GetHashCode_m858E4EF857A0EBEA85DA7F18FC322F4B7CF519C1(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void OutputStreamHandlerConfig_t472506B1EB46A165DEDFD74B41B42728C003617D_CustomAttributesCacheGenerator_OutputStreamHandlerConfig_ToString_mD615009A9DBB0AF31B5EC2F952482129EC268201(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void OutputStreamHandlerConfig_t472506B1EB46A165DEDFD74B41B42728C003617D_CustomAttributesCacheGenerator_OutputStreamHandlerConfig_WriteTo_m97B3BB527940366F4AD1983486997DF660920C14(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void OutputStreamHandlerConfig_t472506B1EB46A165DEDFD74B41B42728C003617D_CustomAttributesCacheGenerator_OutputStreamHandlerConfig_CalculateSize_mF73E0648CDAF78E335B21A903AEAB5223BB210BC(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void OutputStreamHandlerConfig_t472506B1EB46A165DEDFD74B41B42728C003617D_CustomAttributesCacheGenerator_OutputStreamHandlerConfig_MergeFrom_mBB90784471D1BF4F6D21792DA33C740B5AE4489F(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void OutputStreamHandlerConfig_t472506B1EB46A165DEDFD74B41B42728C003617D_CustomAttributesCacheGenerator_OutputStreamHandlerConfig_MergeFrom_mEBEB8A78D238C7BCBA267A3982A7B82F4AB8CE39(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void OutputStreamHandlerConfig_t472506B1EB46A165DEDFD74B41B42728C003617D_CustomAttributesCacheGenerator_OutputStreamHandlerConfig_t472506B1EB46A165DEDFD74B41B42728C003617D____Parser_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void OutputStreamHandlerConfig_t472506B1EB46A165DEDFD74B41B42728C003617D_CustomAttributesCacheGenerator_OutputStreamHandlerConfig_t472506B1EB46A165DEDFD74B41B42728C003617D____Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void OutputStreamHandlerConfig_t472506B1EB46A165DEDFD74B41B42728C003617D_CustomAttributesCacheGenerator_OutputStreamHandlerConfig_t472506B1EB46A165DEDFD74B41B42728C003617D____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void OutputStreamHandlerConfig_t472506B1EB46A165DEDFD74B41B42728C003617D_CustomAttributesCacheGenerator_OutputStreamHandlerConfig_t472506B1EB46A165DEDFD74B41B42728C003617D____OutputStreamHandler_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void OutputStreamHandlerConfig_t472506B1EB46A165DEDFD74B41B42728C003617D_CustomAttributesCacheGenerator_OutputStreamHandlerConfig_t472506B1EB46A165DEDFD74B41B42728C003617D____HasOutputStreamHandler_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void OutputStreamHandlerConfig_t472506B1EB46A165DEDFD74B41B42728C003617D_CustomAttributesCacheGenerator_OutputStreamHandlerConfig_t472506B1EB46A165DEDFD74B41B42728C003617D____InputSidePacket_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void OutputStreamHandlerConfig_t472506B1EB46A165DEDFD74B41B42728C003617D_CustomAttributesCacheGenerator_OutputStreamHandlerConfig_t472506B1EB46A165DEDFD74B41B42728C003617D____Options_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void OutputStreamHandlerConfig_t472506B1EB46A165DEDFD74B41B42728C003617D_CustomAttributesCacheGenerator_OutputStreamHandlerConfig_t472506B1EB46A165DEDFD74B41B42728C003617D____HasOptions_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void U3CU3Ec_t837B3FD340DECF1DCD9695DEAA4D9C68D6BA3A51_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AssetBundleManager_tF3C2B5AFF59FB819D342441D3BEBBD5C7B54BACD_CustomAttributesCacheGenerator_AssetBundleManager_LoadAssetBundleAsync_mE25419B8260B8C411CED0EDF65F03336E5A25DBD(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CLoadAssetBundleAsyncU3Ed__21_t580563DF049EEAABF939C11D81845B846D3B687D_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CLoadAssetBundleAsyncU3Ed__21_t580563DF049EEAABF939C11D81845B846D3B687D_0_0_0_var), NULL);
	}
}
static void AssetBundleManager_tF3C2B5AFF59FB819D342441D3BEBBD5C7B54BACD_CustomAttributesCacheGenerator_AssetBundleManager_PrepareAssetAsync_m0CED483ECF346567C5E1328E28E459262DD0131D(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPrepareAssetAsyncU3Ed__23_tE0D0088355DC75E2373DA3E87446BEC021802D5A_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CPrepareAssetAsyncU3Ed__23_tE0D0088355DC75E2373DA3E87446BEC021802D5A_0_0_0_var), NULL);
	}
}
static void AssetBundleManager_tF3C2B5AFF59FB819D342441D3BEBBD5C7B54BACD_CustomAttributesCacheGenerator_AssetBundleManager_PathToResourceAsFile_mE4EA5782FC953FF16031FD0EB659BD758E0EDC7E(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PathResolver_t704DEBCFBC148702F09DD0F438E150616748594A_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E * tmp = (MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E *)cache->attributes[0];
		MonoPInvokeCallbackAttribute__ctor_mA23FC56D16F76D90C3F76B1C41D0765BB21FE344(tmp, il2cpp_codegen_type_get_object(PathResolver_t704DEBCFBC148702F09DD0F438E150616748594A_0_0_0_var), NULL);
	}
}
static void AssetBundleManager_tF3C2B5AFF59FB819D342441D3BEBBD5C7B54BACD_CustomAttributesCacheGenerator_AssetBundleManager_GetResourceContents_mDFE89208C34422AEE3F365704F969ADFA843BA84(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ResourceProvider_t729C6AE767A25D3EF21A51FC68F578F164DC6FCF_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E * tmp = (MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E *)cache->attributes[0];
		MonoPInvokeCallbackAttribute__ctor_mA23FC56D16F76D90C3F76B1C41D0765BB21FE344(tmp, il2cpp_codegen_type_get_object(ResourceProvider_t729C6AE767A25D3EF21A51FC68F578F164DC6FCF_0_0_0_var), NULL);
	}
}
static void AssetBundleManager_tF3C2B5AFF59FB819D342441D3BEBBD5C7B54BACD_CustomAttributesCacheGenerator_AssetBundleManager_WriteCacheFileAsync_m5F88F8554EB84D0743BA2BD9B7D0E75A26F0E316(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CWriteCacheFileAsyncU3Ed__28_tAD75A60D5B46B633653D85972A00F53DAF9D62C1_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CWriteCacheFileAsyncU3Ed__28_tAD75A60D5B46B633653D85972A00F53DAF9D62C1_0_0_0_var), NULL);
	}
}
static void U3CLoadAssetBundleAsyncU3Ed__21_t580563DF049EEAABF939C11D81845B846D3B687D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CLoadAssetBundleAsyncU3Ed__21_t580563DF049EEAABF939C11D81845B846D3B687D_CustomAttributesCacheGenerator_U3CLoadAssetBundleAsyncU3Ed__21_SetStateMachine_mB1E3AA674C432E165404B70B653A6052F97B5070(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrepareAssetAsyncU3Ed__23_tE0D0088355DC75E2373DA3E87446BEC021802D5A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CPrepareAssetAsyncU3Ed__23_tE0D0088355DC75E2373DA3E87446BEC021802D5A_CustomAttributesCacheGenerator_U3CPrepareAssetAsyncU3Ed__23_SetStateMachine_m290CC6603F5D2FC97EC17298E510722817A5A414(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWriteCacheFileAsyncU3Ed__28_tAD75A60D5B46B633653D85972A00F53DAF9D62C1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWriteCacheFileAsyncU3Ed__28_tAD75A60D5B46B633653D85972A00F53DAF9D62C1_CustomAttributesCacheGenerator_U3CWriteCacheFileAsyncU3Ed__28_SetStateMachine_m4078D3327DCD2BBFBF231DE1709C9CD3B7EAD5C4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass5_0_t8CCBD6F7450BECB8E4DBEBAEAA72AAEB94113E73_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AsyncOperationExtension_t9E461E9BF1A1DD60860A4835F5747291425E8EE1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void AsyncOperationExtension_t9E461E9BF1A1DD60860A4835F5747291425E8EE1_CustomAttributesCacheGenerator_AsyncOperationExtension_GetAwaiter_mB80919BDBE42E14B2F7B3A4ADB12EB308F0B8A9C(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LocalAssetManager_t9373BED16CEBAE58B7FE4A595684AC703D75483C_CustomAttributesCacheGenerator_LocalAssetManager_PrepareAssetAsync_mB0FE9494A0DA547BA1823704C614DADD52EB629F(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPrepareAssetAsyncU3Ed__9_t6EC072023AF74799B02659540A1E75A0260F55C6_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CPrepareAssetAsyncU3Ed__9_t6EC072023AF74799B02659540A1E75A0260F55C6_0_0_0_var), NULL);
	}
}
static void LocalAssetManager_t9373BED16CEBAE58B7FE4A595684AC703D75483C_CustomAttributesCacheGenerator_LocalAssetManager_PathToResourceAsFile_mD93EA6538E2E77CCD4ACB981201294DFFDF0C3A6(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PathResolver_t704DEBCFBC148702F09DD0F438E150616748594A_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E * tmp = (MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E *)cache->attributes[0];
		MonoPInvokeCallbackAttribute__ctor_mA23FC56D16F76D90C3F76B1C41D0765BB21FE344(tmp, il2cpp_codegen_type_get_object(PathResolver_t704DEBCFBC148702F09DD0F438E150616748594A_0_0_0_var), NULL);
	}
}
static void LocalAssetManager_t9373BED16CEBAE58B7FE4A595684AC703D75483C_CustomAttributesCacheGenerator_LocalAssetManager_GetResourceContents_m36D0D7964026D770978C9BECA4F18D510A46648A(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ResourceProvider_t729C6AE767A25D3EF21A51FC68F578F164DC6FCF_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E * tmp = (MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E *)cache->attributes[0];
		MonoPInvokeCallbackAttribute__ctor_mA23FC56D16F76D90C3F76B1C41D0765BB21FE344(tmp, il2cpp_codegen_type_get_object(ResourceProvider_t729C6AE767A25D3EF21A51FC68F578F164DC6FCF_0_0_0_var), NULL);
	}
}
static void U3CPrepareAssetAsyncU3Ed__9_t6EC072023AF74799B02659540A1E75A0260F55C6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CPrepareAssetAsyncU3Ed__9_t6EC072023AF74799B02659540A1E75A0260F55C6_CustomAttributesCacheGenerator_U3CPrepareAssetAsyncU3Ed__9_SetStateMachine_mC7A20B6945A994F96677055B1D8DE081F0F89849(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void FaceGeometry_t7B246BEE497814C69CD0E81B2116D2C9746BF305_CustomAttributesCacheGenerator_FaceGeometry__ctor_m3C8727C2045277EA8A051205D4FC9A5CE3D5E89A(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FaceGeometry_t7B246BEE497814C69CD0E81B2116D2C9746BF305_CustomAttributesCacheGenerator_FaceGeometry__ctor_m2C60845DDA8A847E19706B9BD1CCF8064A1C5124(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FaceGeometry_t7B246BEE497814C69CD0E81B2116D2C9746BF305_CustomAttributesCacheGenerator_FaceGeometry_Clone_m8489E2E228E407688DA6FEDFFC665B20256CC9BE(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FaceGeometry_t7B246BEE497814C69CD0E81B2116D2C9746BF305_CustomAttributesCacheGenerator_FaceGeometry_ClearMesh_m7CD19E757E9FD9D40EEE8C790A17A34F9EB32C86(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FaceGeometry_t7B246BEE497814C69CD0E81B2116D2C9746BF305_CustomAttributesCacheGenerator_FaceGeometry_ClearPoseTransformMatrix_m5E342DF34AA1B1C802830EE2BEB6C53AD40BAD4F(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FaceGeometry_t7B246BEE497814C69CD0E81B2116D2C9746BF305_CustomAttributesCacheGenerator_FaceGeometry_Equals_m24D19204C5A3E02D27CB6F1EF569A1D60B36AD7F(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FaceGeometry_t7B246BEE497814C69CD0E81B2116D2C9746BF305_CustomAttributesCacheGenerator_FaceGeometry_Equals_m4AB38A52827525069D398725064F885308DF8426(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FaceGeometry_t7B246BEE497814C69CD0E81B2116D2C9746BF305_CustomAttributesCacheGenerator_FaceGeometry_GetHashCode_m9A30D9E1C207E32C0035016651B8292E1AC0978D(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FaceGeometry_t7B246BEE497814C69CD0E81B2116D2C9746BF305_CustomAttributesCacheGenerator_FaceGeometry_ToString_m8A03508AD29218B0490EEC9FCC3090B9FA68C618(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FaceGeometry_t7B246BEE497814C69CD0E81B2116D2C9746BF305_CustomAttributesCacheGenerator_FaceGeometry_WriteTo_mA3D5E866007FE45435FD2F0C241F5BFF9000DA46(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FaceGeometry_t7B246BEE497814C69CD0E81B2116D2C9746BF305_CustomAttributesCacheGenerator_FaceGeometry_CalculateSize_m8244CFFD3C4A8DA2ABB011819E670C9B6002EC6D(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FaceGeometry_t7B246BEE497814C69CD0E81B2116D2C9746BF305_CustomAttributesCacheGenerator_FaceGeometry_MergeFrom_mE6D2821557D75E5A220BBAD1AF0533C69708A99B(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FaceGeometry_t7B246BEE497814C69CD0E81B2116D2C9746BF305_CustomAttributesCacheGenerator_FaceGeometry_MergeFrom_m635F8DC14C15F10F7ADD712FBE7899E7DF8B918C(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FaceGeometry_t7B246BEE497814C69CD0E81B2116D2C9746BF305_CustomAttributesCacheGenerator_FaceGeometry_t7B246BEE497814C69CD0E81B2116D2C9746BF305____Parser_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FaceGeometry_t7B246BEE497814C69CD0E81B2116D2C9746BF305_CustomAttributesCacheGenerator_FaceGeometry_t7B246BEE497814C69CD0E81B2116D2C9746BF305____Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FaceGeometry_t7B246BEE497814C69CD0E81B2116D2C9746BF305_CustomAttributesCacheGenerator_FaceGeometry_t7B246BEE497814C69CD0E81B2116D2C9746BF305____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FaceGeometry_t7B246BEE497814C69CD0E81B2116D2C9746BF305_CustomAttributesCacheGenerator_FaceGeometry_t7B246BEE497814C69CD0E81B2116D2C9746BF305____Mesh_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FaceGeometry_t7B246BEE497814C69CD0E81B2116D2C9746BF305_CustomAttributesCacheGenerator_FaceGeometry_t7B246BEE497814C69CD0E81B2116D2C9746BF305____HasMesh_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FaceGeometry_t7B246BEE497814C69CD0E81B2116D2C9746BF305_CustomAttributesCacheGenerator_FaceGeometry_t7B246BEE497814C69CD0E81B2116D2C9746BF305____PoseTransformMatrix_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void FaceGeometry_t7B246BEE497814C69CD0E81B2116D2C9746BF305_CustomAttributesCacheGenerator_FaceGeometry_t7B246BEE497814C69CD0E81B2116D2C9746BF305____HasPoseTransformMatrix_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void U3CU3Ec_tC5334BD4B37B017C10987E14331E9FF1E8B7E993_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Mesh3d_t038DF3815E8BB8AC98EE930DFAA9C27D5A3AE2FA_CustomAttributesCacheGenerator_Mesh3d__ctor_m9F9250640B8B59C63500CA172405221839AC8659(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Mesh3d_t038DF3815E8BB8AC98EE930DFAA9C27D5A3AE2FA_CustomAttributesCacheGenerator_Mesh3d__ctor_m3DA3D0DF3B76850845F5492101B043CDFA74891B(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Mesh3d_t038DF3815E8BB8AC98EE930DFAA9C27D5A3AE2FA_CustomAttributesCacheGenerator_Mesh3d_Clone_m1480A74D40F2C4A9F767C9BADE298FA2C929E2FE(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Mesh3d_t038DF3815E8BB8AC98EE930DFAA9C27D5A3AE2FA_CustomAttributesCacheGenerator_Mesh3d_ClearVertexType_m0636B3AF901073C2526A37C1F2A09715A3AF9EDE(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Mesh3d_t038DF3815E8BB8AC98EE930DFAA9C27D5A3AE2FA_CustomAttributesCacheGenerator_Mesh3d_ClearPrimitiveType_mDA655E945A9E7C5224BC7B466BEB4DC2C0237280(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Mesh3d_t038DF3815E8BB8AC98EE930DFAA9C27D5A3AE2FA_CustomAttributesCacheGenerator_Mesh3d_Equals_m8608984036EA7B53D8E397986337302F7A26916D(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Mesh3d_t038DF3815E8BB8AC98EE930DFAA9C27D5A3AE2FA_CustomAttributesCacheGenerator_Mesh3d_Equals_m1A13E2324B6A21935A44DF3AF37B6F090083DFC2(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Mesh3d_t038DF3815E8BB8AC98EE930DFAA9C27D5A3AE2FA_CustomAttributesCacheGenerator_Mesh3d_GetHashCode_m385ABBC9D72FB51065C89DB3616E642F47066A0A(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Mesh3d_t038DF3815E8BB8AC98EE930DFAA9C27D5A3AE2FA_CustomAttributesCacheGenerator_Mesh3d_ToString_m2628330180A999E8A61BC16A9DC80C69E4B9C03C(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Mesh3d_t038DF3815E8BB8AC98EE930DFAA9C27D5A3AE2FA_CustomAttributesCacheGenerator_Mesh3d_WriteTo_m5CBA34FA8D18DFAB07AC9AC358FEFB2626FDE216(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Mesh3d_t038DF3815E8BB8AC98EE930DFAA9C27D5A3AE2FA_CustomAttributesCacheGenerator_Mesh3d_CalculateSize_m1AC813F566F4AAE361BA291AC9FEB73457651D91(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Mesh3d_t038DF3815E8BB8AC98EE930DFAA9C27D5A3AE2FA_CustomAttributesCacheGenerator_Mesh3d_MergeFrom_mA837C24529468595959D161C916756CA53E4BFDD(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Mesh3d_t038DF3815E8BB8AC98EE930DFAA9C27D5A3AE2FA_CustomAttributesCacheGenerator_Mesh3d_MergeFrom_mDBAB758798822F0692FBBADAFDABE64BE546153E(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Mesh3d_t038DF3815E8BB8AC98EE930DFAA9C27D5A3AE2FA_CustomAttributesCacheGenerator_Mesh3d_t038DF3815E8BB8AC98EE930DFAA9C27D5A3AE2FA____Parser_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Mesh3d_t038DF3815E8BB8AC98EE930DFAA9C27D5A3AE2FA_CustomAttributesCacheGenerator_Mesh3d_t038DF3815E8BB8AC98EE930DFAA9C27D5A3AE2FA____Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Mesh3d_t038DF3815E8BB8AC98EE930DFAA9C27D5A3AE2FA_CustomAttributesCacheGenerator_Mesh3d_t038DF3815E8BB8AC98EE930DFAA9C27D5A3AE2FA____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Mesh3d_t038DF3815E8BB8AC98EE930DFAA9C27D5A3AE2FA_CustomAttributesCacheGenerator_Mesh3d_t038DF3815E8BB8AC98EE930DFAA9C27D5A3AE2FA____VertexType_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Mesh3d_t038DF3815E8BB8AC98EE930DFAA9C27D5A3AE2FA_CustomAttributesCacheGenerator_Mesh3d_t038DF3815E8BB8AC98EE930DFAA9C27D5A3AE2FA____HasVertexType_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Mesh3d_t038DF3815E8BB8AC98EE930DFAA9C27D5A3AE2FA_CustomAttributesCacheGenerator_Mesh3d_t038DF3815E8BB8AC98EE930DFAA9C27D5A3AE2FA____PrimitiveType_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Mesh3d_t038DF3815E8BB8AC98EE930DFAA9C27D5A3AE2FA_CustomAttributesCacheGenerator_Mesh3d_t038DF3815E8BB8AC98EE930DFAA9C27D5A3AE2FA____HasPrimitiveType_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Mesh3d_t038DF3815E8BB8AC98EE930DFAA9C27D5A3AE2FA_CustomAttributesCacheGenerator_Mesh3d_t038DF3815E8BB8AC98EE930DFAA9C27D5A3AE2FA____VertexBuffer_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Mesh3d_t038DF3815E8BB8AC98EE930DFAA9C27D5A3AE2FA_CustomAttributesCacheGenerator_Mesh3d_t038DF3815E8BB8AC98EE930DFAA9C27D5A3AE2FA____IndexBuffer_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void Types_t23BF37EC433CF5AEB007313A8F0735E2FB07823E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 * tmp = (DebuggerNonUserCodeAttribute_t47FE9BBE8F4A377B2EDD62B769D2AF2392ED7D41 *)cache->attributes[0];
		DebuggerNonUserCodeAttribute__ctor_m91A90A6224EE679EBFC714DA3C5D2B006974D8AF(tmp, NULL);
	}
}
static void VertexType_t40A963961D76478EA83AB0FBD3F9DEA24AAD9C41_CustomAttributesCacheGenerator_VertexPt(CustomAttributesCache* cache)
{
	{
		OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * tmp = (OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 *)cache->attributes[0];
		OriginalNameAttribute__ctor_mB0C6C38C49B9261D2DC423D692E80B8EBC657BF9(tmp, il2cpp_codegen_string_new_wrapper("\x56\x45\x52\x54\x45\x58\x5F\x50\x54"), NULL);
	}
}
static void PrimitiveType_t71E6471CCFF8B6A6869F5AF4355BC6800DBD3ABF_CustomAttributesCacheGenerator_Triangle(CustomAttributesCache* cache)
{
	{
		OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 * tmp = (OriginalNameAttribute_tE7A7CE04F1D8E94F778629F55BCC2EDDB03D8664 *)cache->attributes[0];
		OriginalNameAttribute__ctor_mB0C6C38C49B9261D2DC423D692E80B8EBC657BF9(tmp, il2cpp_codegen_string_new_wrapper("\x54\x52\x49\x41\x4E\x47\x4C\x45"), NULL);
	}
}
static void U3CU3Ec_t281613EC39006B07E2A6A6EC1065025B81A6888A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_MediaPipeSDK_AttributeGenerators[];
const CustomAttributesCacheGenerator g_MediaPipeSDK_AttributeGenerators[1089] = 
{
	EmbeddedAttribute_t152A226BB4E86AF918B0AAB3CEA83EFB90DE388E_CustomAttributesCacheGenerator,
	IsUnmanagedAttribute_tCC30ED83A257B813A2D28B603A345F0CB5F04BD9_CustomAttributesCacheGenerator,
	U3CU3Ec_t2BC66AEDA6FFAE79441F6EF68ADDDCB204959396_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass11_0_2_t0505406910875832174BED162EC70EE7FA4B580A_CustomAttributesCacheGenerator,
	CalculatorGraphConfigExtension_t9558861CC274C820B31F5D69FAEE6E0ACE45B6A3_CustomAttributesCacheGenerator,
	U3CU3Ec_t3C5228983E3519B9CCEEE7A8822E6205037E999A_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass2_0_t82048CC1732D57D29FE279E3CAF06974BD411658_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass6_0_tBBF7376F93226F24C90D9B6763B3512BA708B4C4_CustomAttributesCacheGenerator,
	GpuBufferFormatExtension_t6AC277E4C201DDBC6B17F4E43B1DBF3C5B3E863C_CustomAttributesCacheGenerator,
	MpReturnCodeExtension_t303D50EB4156CB6B1A7C14DA771FA5F563F2D81C_CustomAttributesCacheGenerator,
	SafeNativeMethods_t8AAF6F1747D3D43EA2E96E312112591EE6944018_CustomAttributesCacheGenerator,
	UnsafeNativeMethods_t5286816A1FB919D621EEB37AD9EC79D205B1E44D_CustomAttributesCacheGenerator,
	U3CU3Ec_t1107DB4858A6272FD61890EF04733CE45CDFCC56_CustomAttributesCacheGenerator,
	Types_t17F11D4BF4592BCA38E72F4CC148526F3CE91E82_CustomAttributesCacheGenerator,
	U3CU3Ec_t9F13509DBCE5115FCDC74F9839F12A056BE8153C_CustomAttributesCacheGenerator,
	U3CU3Ec_t7F9B2B6B274545FC6060B8F50F7BA4A00CD73954_CustomAttributesCacheGenerator,
	U3CU3Ec_t6244C7E01E3E624B6801BF78FB9959E508B7D113_CustomAttributesCacheGenerator,
	U3CU3Ec_t88399BB3483FBD7BEF6C78CA1C86B500A1BB64BF_CustomAttributesCacheGenerator,
	Types_tAA56A21CDC0818B0260ABE6A9B9C933266ACBCBE_CustomAttributesCacheGenerator,
	U3CU3Ec_tD2B580CCD6FBAFA17B247C8F673ACF51461DBBF1_CustomAttributesCacheGenerator,
	U3CU3Ec_t4B2AB6DB1BF7690B634459FC9E4703BD9FE44B7A_CustomAttributesCacheGenerator,
	U3CU3Ec_t720FE2495FF33A1525C1241449A7F483543FD4E5_CustomAttributesCacheGenerator,
	U3CU3Ec_tC84E0C10849A469DF73B913A742DF92D8FA1D0DE_CustomAttributesCacheGenerator,
	U3CU3Ec_tD243BB7950BD2A816658CA48C18E7379AE136A1D_CustomAttributesCacheGenerator,
	Types_tF264D77BD89891DC8875C3BB0C62675FF604E5C3_CustomAttributesCacheGenerator,
	U3CU3Ec_t76319172D0AD039F5C4D0408B5BCC93F3F9B096E_CustomAttributesCacheGenerator,
	U3CU3Ec_t581A289842F8AC72AF35AC7151889D7FC2C87931_CustomAttributesCacheGenerator,
	U3CU3Ec_t89CE3936129D9A47D63F4C5B6855F334D9F62DF4_CustomAttributesCacheGenerator,
	U3CU3Ec_tEAAF53EEB29ABDAD486D2E4FFBFA931A57AA1973_CustomAttributesCacheGenerator,
	U3CU3Ec_t436CC23D732B78B12BA7AF69DAA40088A6033D7C_CustomAttributesCacheGenerator,
	U3CU3Ec_tA8DAFFD20122CFA86C1D826C0831F521C3D73021_CustomAttributesCacheGenerator,
	U3CU3Ec_tBFAE5CA9FD4858EB6B7C4622EAB68A31A61E0B9B_CustomAttributesCacheGenerator,
	Types_t331A01D2554D11320784F4555E3035B794969908_CustomAttributesCacheGenerator,
	U3CU3Ec_t1FD492E3283975CCB4673F6C1C57F94D9E8E25AA_CustomAttributesCacheGenerator,
	U3CU3Ec_t7113280FE900CC249CB34C5F691D6B120D0A61C4_CustomAttributesCacheGenerator,
	U3CU3Ec_t476BEC4D64811E70D44EDA4B9A3881257E53AB15_CustomAttributesCacheGenerator,
	U3CU3Ec_t433A9FEB53731C105E43E59259348CC37F31B113_CustomAttributesCacheGenerator,
	U3CU3Ec_t0F7D4D63D876240F046F147802C3F30B87725A5C_CustomAttributesCacheGenerator,
	Types_t41118EF0E36EA11B08B5B30ECA357999DAFD8242_CustomAttributesCacheGenerator,
	U3CU3Ec_t315CB76305CAB55D659B19E611FCFA811CA0F2B1_CustomAttributesCacheGenerator,
	U3CU3Ec_t7AD00D32F4CAD5CFD23A067EBB2ADB3953C65232_CustomAttributesCacheGenerator,
	U3CU3Ec_t552C0FA1438471E2E3EB8F77546E51FC0171BFD6_CustomAttributesCacheGenerator,
	U3CU3Ec_t897629C0EB8099BA4C18CDC8B2661E9BB4E27DB1_CustomAttributesCacheGenerator,
	U3CU3Ec_tBA5B02F40AEC74B652F459E12D3554F2272E84E7_CustomAttributesCacheGenerator,
	U3CU3Ec_t9C4AE8D4C53A22FD81C8E9F5681FD9F93C191257_CustomAttributesCacheGenerator,
	U3CU3Ec_t7DC7111FDD47602F0B2A651B90589895FF76461E_CustomAttributesCacheGenerator,
	U3CU3Ec_t69C8F92901AD650DAF4D59DA40081E7AF96A0076_CustomAttributesCacheGenerator,
	U3CU3Ec_t641B3DD9BE9C53B9CC228EF94DBB115BEA013BFF_CustomAttributesCacheGenerator,
	U3CU3Ec_t94ED63BBD9E83B387631119D44A2F2BF2ED8CF85_CustomAttributesCacheGenerator,
	U3CU3Ec_t3EA88371F6F338476D12B97179D74101FDC1EFF1_CustomAttributesCacheGenerator,
	Types_tB9721F5EF014CDBFC226E47769A872B2F647AA57_CustomAttributesCacheGenerator,
	U3CU3Ec_t02AB85E0260AE1954E4CC6A2A5821F35406E5537_CustomAttributesCacheGenerator,
	U3CU3Ec_tD985A5315A493FBEBE7939DDDC973BC03EB68AC3_CustomAttributesCacheGenerator,
	U3CU3Ec_t9D156C4D9D79C37329D73ECCA6307890C18699AB_CustomAttributesCacheGenerator,
	U3CU3Ec_t2583760C8CF53E7795A5CE053961AB9C7E50E775_CustomAttributesCacheGenerator,
	U3CU3Ec_t14F6C0C7A294A4C44A951230DC6D1A7D3E5FD87D_CustomAttributesCacheGenerator,
	U3CU3Ec_tF5CF319F7865D36D0728338038A89565E9B43406_CustomAttributesCacheGenerator,
	U3CU3Ec_t40AABDAE2D3CAAB5B72505754976026BA96DC51E_CustomAttributesCacheGenerator,
	U3CU3Ec_tC3B2DD5BBC15198CC3282D932BDC2EB383575443_CustomAttributesCacheGenerator,
	U3CU3Ec_t837B3FD340DECF1DCD9695DEAA4D9C68D6BA3A51_CustomAttributesCacheGenerator,
	U3CLoadAssetBundleAsyncU3Ed__21_t580563DF049EEAABF939C11D81845B846D3B687D_CustomAttributesCacheGenerator,
	U3CPrepareAssetAsyncU3Ed__23_tE0D0088355DC75E2373DA3E87446BEC021802D5A_CustomAttributesCacheGenerator,
	U3CWriteCacheFileAsyncU3Ed__28_tAD75A60D5B46B633653D85972A00F53DAF9D62C1_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass5_0_t8CCBD6F7450BECB8E4DBEBAEAA72AAEB94113E73_CustomAttributesCacheGenerator,
	AsyncOperationExtension_t9E461E9BF1A1DD60860A4835F5747291425E8EE1_CustomAttributesCacheGenerator,
	U3CPrepareAssetAsyncU3Ed__9_t6EC072023AF74799B02659540A1E75A0260F55C6_CustomAttributesCacheGenerator,
	U3CU3Ec_tC5334BD4B37B017C10987E14331E9FF1E8B7E993_CustomAttributesCacheGenerator,
	Types_t23BF37EC433CF5AEB007313A8F0735E2FB07823E_CustomAttributesCacheGenerator,
	U3CU3Ec_t281613EC39006B07E2A6A6EC1065025B81A6888A_CustomAttributesCacheGenerator,
	CircleAnnotationController_tA7923999378E3E1A95DF49FEBF82EBB07219AE54_CustomAttributesCacheGenerator_PositionSize,
	DetectionAnnotationController_tE65DA01B36F319F72CEA4AA803EDBCB20C838EA2_CustomAttributesCacheGenerator_relativeKeypointPrefab,
	LandmarkListAnnotationController_t5DFFA52714162A66D82D024FD4894E8407E51BC6_CustomAttributesCacheGenerator_nodePrefab,
	LandmarkListAnnotationController_t5DFFA52714162A66D82D024FD4894E8407E51BC6_CustomAttributesCacheGenerator_edgePrefab,
	LandmarkListAnnotationController_t5DFFA52714162A66D82D024FD4894E8407E51BC6_CustomAttributesCacheGenerator_NodeScale,
	ListAnnotationController_1_t28493CB41E2E27D515F31137D65D48A80F4A9D27_CustomAttributesCacheGenerator_annotationPrefab,
	ListAnnotationController_1_t28493CB41E2E27D515F31137D65D48A80F4A9D27_CustomAttributesCacheGenerator_DefaultMaxSize,
	ListAnnotationController_1_t28493CB41E2E27D515F31137D65D48A80F4A9D27_CustomAttributesCacheGenerator_U3CMaxSizeU3Ek__BackingField,
	DisposableObject_tEF59059AABEEFCC912FE2A5C8BE0CA5D1AE5F09D_CustomAttributesCacheGenerator_U3CisDisposedU3Ek__BackingField,
	DisposableObject_tEF59059AABEEFCC912FE2A5C8BE0CA5D1AE5F09D_CustomAttributesCacheGenerator_U3CisOwnerU3Ek__BackingField,
	U3CU3Ec_t3C5228983E3519B9CCEEE7A8822E6205037E999A_CustomAttributesCacheGenerator_U3CU3E9__2_0,
	InputType_t06CD6E3FD08548CD0BB0B2462CED0C0A11F1E9AE_CustomAttributesCacheGenerator_Unknown,
	InputType_t06CD6E3FD08548CD0BB0B2462CED0C0A11F1E9AE_CustomAttributesCacheGenerator_Recordio,
	InputType_t06CD6E3FD08548CD0BB0B2462CED0C0A11F1E9AE_CustomAttributesCacheGenerator_ForeignRecordio,
	InputType_t06CD6E3FD08548CD0BB0B2462CED0C0A11F1E9AE_CustomAttributesCacheGenerator_ForeignCsvText,
	InputType_t06CD6E3FD08548CD0BB0B2462CED0C0A11F1E9AE_CustomAttributesCacheGenerator_InvalidUpperBound,
	Format_t899CE640F3FB09E97A6F7796F8A0AFB346CFC49E_CustomAttributesCacheGenerator_Global,
	Format_t899CE640F3FB09E97A6F7796F8A0AFB346CFC49E_CustomAttributesCacheGenerator_BoundingBox,
	Format_t899CE640F3FB09E97A6F7796F8A0AFB346CFC49E_CustomAttributesCacheGenerator_RelativeBoundingBox,
	Format_t899CE640F3FB09E97A6F7796F8A0AFB346CFC49E_CustomAttributesCacheGenerator_Mask,
	Layout_t19C106ABE5F32E78A70B3F998158682CEA644D62_CustomAttributesCacheGenerator_ColumnMajor,
	Layout_t19C106ABE5F32E78A70B3F998158682CEA644D62_CustomAttributesCacheGenerator_RowMajor,
	VertexType_t40A963961D76478EA83AB0FBD3F9DEA24AAD9C41_CustomAttributesCacheGenerator_VertexPt,
	PrimitiveType_t71E6471CCFF8B6A6869F5AF4355BC6800DBD3ABF_CustomAttributesCacheGenerator_Triangle,
	ListAnnotationController_1_t28493CB41E2E27D515F31137D65D48A80F4A9D27_CustomAttributesCacheGenerator_ListAnnotationController_1_get_MaxSize_mAC2AD4B6BCBE8DBE5E0126FFB2159C6332D3024E,
	ListAnnotationController_1_t28493CB41E2E27D515F31137D65D48A80F4A9D27_CustomAttributesCacheGenerator_ListAnnotationController_1_set_MaxSize_m51FD63C4AA84FE9EDD55A66C6FCD8F89B662D115,
	DisposableObject_tEF59059AABEEFCC912FE2A5C8BE0CA5D1AE5F09D_CustomAttributesCacheGenerator_DisposableObject_get_isDisposed_mBAC3411868F2F3866C93C0FD29569C5C6A589E4E,
	DisposableObject_tEF59059AABEEFCC912FE2A5C8BE0CA5D1AE5F09D_CustomAttributesCacheGenerator_DisposableObject_set_isDisposed_m4810231F19F321DDBB7ACEB59D310DA3AF8689FA,
	DisposableObject_tEF59059AABEEFCC912FE2A5C8BE0CA5D1AE5F09D_CustomAttributesCacheGenerator_DisposableObject_get_isOwner_m05D0D142AD0928EFB4EBF5C9BE83A8C4D003B7C5,
	DisposableObject_tEF59059AABEEFCC912FE2A5C8BE0CA5D1AE5F09D_CustomAttributesCacheGenerator_DisposableObject_set_isOwner_m14F37B0D67E76791FAD35719A01F7924A796DE42,
	Protobuf_t8CE0334167D95EA75FFD886A6585A57E9E73A585_CustomAttributesCacheGenerator_Protobuf_LogProtobufMessage_m1B59C21023D21E2AB5CA458EC71B9F43DA4E587D,
	Format_tF31D9B5F4DBE60F77A8FA0B17EEADF7CB2872FBA_CustomAttributesCacheGenerator_Format_FromPixels32_mBA75BC96B4A07BD34189075F273B236DEC47F7D6,
	Format_tF31D9B5F4DBE60F77A8FA0B17EEADF7CB2872FBA_CustomAttributesCacheGenerator_Format_FromBytePtr_m241B5F9B3CA63A791E94B49E80FD4EAE6453619A,
	CalculatorGraphConfigExtension_t9558861CC274C820B31F5D69FAEE6E0ACE45B6A3_CustomAttributesCacheGenerator_CalculatorGraphConfigExtension_ParseFromTextFormat_mC08DFC40A3661668ABA0670D41E52A2DD78F74D6,
	ImageFrame_t8089742C09B5D80723C4FF7BEF27781AE14A450D_CustomAttributesCacheGenerator_ImageFrame_ReleasePixelData_mDF81706134475903E835D1F61193281D8FB96A78,
	ImageFrame_t8089742C09B5D80723C4FF7BEF27781AE14A450D_CustomAttributesCacheGenerator_ImageFrame_GetColor32s_mD2096829CCF07C9F53B7A33C849225B33E3B56F3,
	ImageFrame_t8089742C09B5D80723C4FF7BEF27781AE14A450D_CustomAttributesCacheGenerator_ImageFrame_FromPixels32_m9120673B8A0650BD04B61D67A06C46308ED5F1C8,
	Status_t08160DCBEC7A0170340AC75F91703B1E1EAC1267_CustomAttributesCacheGenerator_Status_IsOk_m1AB7D6193A62F662ECA6115FE48501605923C4BF,
	Status_t08160DCBEC7A0170340AC75F91703B1E1EAC1267_CustomAttributesCacheGenerator_Status_GetPtr_mB5ACB15D7C5332303206547CF6AF7E1AFF3DC267,
	GpuBufferFormatExtension_t6AC277E4C201DDBC6B17F4E43B1DBF3C5B3E863C_CustomAttributesCacheGenerator_GpuBufferFormatExtension_ImageFormatFor_m0DC71BAD40318374571505A1C1A3DA9009A74CE1,
	GpuBufferFormatExtension_t6AC277E4C201DDBC6B17F4E43B1DBF3C5B3E863C_CustomAttributesCacheGenerator_GpuBufferFormatExtension_GlTextureInfoFor_mF1C0CB8FEABA534B7FECD9FF2A531F6B027F2FFF,
	MpReturnCodeExtension_t303D50EB4156CB6B1A7C14DA771FA5F563F2D81C_CustomAttributesCacheGenerator_MpReturnCodeExtension_Assert_mD720CAA4396143E0113F15BBE369066AB04044EC,
	ExecutorConfig_t2755CF63D93D5DC1B5534655FCC554960354EE7F_CustomAttributesCacheGenerator_ExecutorConfig__ctor_m3100C42DF28C84401E6BEF2F84829F2EBBC6A79C,
	ExecutorConfig_t2755CF63D93D5DC1B5534655FCC554960354EE7F_CustomAttributesCacheGenerator_ExecutorConfig__ctor_mD8F55C2743A66A7A21FF05F95AC46741582F3F16,
	ExecutorConfig_t2755CF63D93D5DC1B5534655FCC554960354EE7F_CustomAttributesCacheGenerator_ExecutorConfig_Clone_mBCF44B0A1985F000BBEC12B6FF68B32D57D3AB29,
	ExecutorConfig_t2755CF63D93D5DC1B5534655FCC554960354EE7F_CustomAttributesCacheGenerator_ExecutorConfig_Equals_m3951D8E5FFF9897153DCBB93E5DE556807BBE51D,
	ExecutorConfig_t2755CF63D93D5DC1B5534655FCC554960354EE7F_CustomAttributesCacheGenerator_ExecutorConfig_Equals_mFD6B3AC46F2086E4F1F526630E167EC0C8543318,
	ExecutorConfig_t2755CF63D93D5DC1B5534655FCC554960354EE7F_CustomAttributesCacheGenerator_ExecutorConfig_GetHashCode_m7BDD5B881BD685E1ED277BBBCFD80C4986FFE164,
	ExecutorConfig_t2755CF63D93D5DC1B5534655FCC554960354EE7F_CustomAttributesCacheGenerator_ExecutorConfig_ToString_mF0A8963B6D15984AFB1E8BFF76FABB4F9F173917,
	ExecutorConfig_t2755CF63D93D5DC1B5534655FCC554960354EE7F_CustomAttributesCacheGenerator_ExecutorConfig_WriteTo_mD1274E15A379E9C0DD0C28CE7EB63EAAA74C67D4,
	ExecutorConfig_t2755CF63D93D5DC1B5534655FCC554960354EE7F_CustomAttributesCacheGenerator_ExecutorConfig_CalculateSize_mA55EE68840B8DD1CC6F4AE0974D39CFC0AA61E9C,
	ExecutorConfig_t2755CF63D93D5DC1B5534655FCC554960354EE7F_CustomAttributesCacheGenerator_ExecutorConfig_MergeFrom_m5C5312A118CD794BA85F9895A164F3B9D27864D8,
	ExecutorConfig_t2755CF63D93D5DC1B5534655FCC554960354EE7F_CustomAttributesCacheGenerator_ExecutorConfig_MergeFrom_mB9D3A9DC69128998567149232D6167740C1D86B0,
	InputCollection_tA4C4583EB805BFBA48936774F9E2EF02712021D2_CustomAttributesCacheGenerator_InputCollection__ctor_m6C6B1C4710314CCF5B8B7924C90E035C6FAB2DD8,
	InputCollection_tA4C4583EB805BFBA48936774F9E2EF02712021D2_CustomAttributesCacheGenerator_InputCollection__ctor_m0D27217EBD9D8F66693BFD6643F490520429AA4D,
	InputCollection_tA4C4583EB805BFBA48936774F9E2EF02712021D2_CustomAttributesCacheGenerator_InputCollection_Clone_m1B45CE9CCB12659381144DB30162D6975759F471,
	InputCollection_tA4C4583EB805BFBA48936774F9E2EF02712021D2_CustomAttributesCacheGenerator_InputCollection_Equals_mFED987F83BABE5D3B89DDA35313DD18A2746CEBB,
	InputCollection_tA4C4583EB805BFBA48936774F9E2EF02712021D2_CustomAttributesCacheGenerator_InputCollection_Equals_m240639A5CB435A8E02FD15D41B30E9AFB7C9D2FB,
	InputCollection_tA4C4583EB805BFBA48936774F9E2EF02712021D2_CustomAttributesCacheGenerator_InputCollection_GetHashCode_mAECDBE9B9032BA48A73EC8D59C4ED5F1D473E5B7,
	InputCollection_tA4C4583EB805BFBA48936774F9E2EF02712021D2_CustomAttributesCacheGenerator_InputCollection_ToString_m7FA0413087B5A0AD62F527DD3F15ECDCB9981DEF,
	InputCollection_tA4C4583EB805BFBA48936774F9E2EF02712021D2_CustomAttributesCacheGenerator_InputCollection_WriteTo_m606B5E61E0DD7AC8D05F5C64A3EDD68625ACA63C,
	InputCollection_tA4C4583EB805BFBA48936774F9E2EF02712021D2_CustomAttributesCacheGenerator_InputCollection_CalculateSize_mB74CC608EB1D546473A2A7D93F6037CFA54E60FD,
	InputCollection_tA4C4583EB805BFBA48936774F9E2EF02712021D2_CustomAttributesCacheGenerator_InputCollection_MergeFrom_mD3C06AAE9303FC3CD361361748B7CD0BE5C46D38,
	InputCollection_tA4C4583EB805BFBA48936774F9E2EF02712021D2_CustomAttributesCacheGenerator_InputCollection_MergeFrom_mFADA258A81187638466BFEF0AFC6C8351A94374B,
	InputCollectionSet_tAF58E12C5E427F540512C06F0E46D278E52D89AA_CustomAttributesCacheGenerator_InputCollectionSet__ctor_m106104988333DB6E965B4966687686D1085762F5,
	InputCollectionSet_tAF58E12C5E427F540512C06F0E46D278E52D89AA_CustomAttributesCacheGenerator_InputCollectionSet__ctor_m5B651C10C9348EA4E9BAAEED0053D0D7A17CD82C,
	InputCollectionSet_tAF58E12C5E427F540512C06F0E46D278E52D89AA_CustomAttributesCacheGenerator_InputCollectionSet_Clone_mE409D46CDF2E45AFCF1BCCFA1245ED50361A972D,
	InputCollectionSet_tAF58E12C5E427F540512C06F0E46D278E52D89AA_CustomAttributesCacheGenerator_InputCollectionSet_Equals_mD0F5B94B7B317360CA85154981CC2B5193459D97,
	InputCollectionSet_tAF58E12C5E427F540512C06F0E46D278E52D89AA_CustomAttributesCacheGenerator_InputCollectionSet_Equals_mD2DF8C4AB1E38FE7EB5F3AE5991D7C2E7B686019,
	InputCollectionSet_tAF58E12C5E427F540512C06F0E46D278E52D89AA_CustomAttributesCacheGenerator_InputCollectionSet_GetHashCode_m6AC079DDE2E44C5212E7E62F1FF43C86B2B37D5E,
	InputCollectionSet_tAF58E12C5E427F540512C06F0E46D278E52D89AA_CustomAttributesCacheGenerator_InputCollectionSet_ToString_m9CABAE3D201DE47B317FA7DC708336FFF7E16607,
	InputCollectionSet_tAF58E12C5E427F540512C06F0E46D278E52D89AA_CustomAttributesCacheGenerator_InputCollectionSet_WriteTo_m929290F3104395CB0869AADAC6EAC99A02107476,
	InputCollectionSet_tAF58E12C5E427F540512C06F0E46D278E52D89AA_CustomAttributesCacheGenerator_InputCollectionSet_CalculateSize_m3035110C513A4AC4869481684000D6E163838F20,
	InputCollectionSet_tAF58E12C5E427F540512C06F0E46D278E52D89AA_CustomAttributesCacheGenerator_InputCollectionSet_MergeFrom_m57652BBC66362FB0B8E52A4C24A972069DCA12A3,
	InputCollectionSet_tAF58E12C5E427F540512C06F0E46D278E52D89AA_CustomAttributesCacheGenerator_InputCollectionSet_MergeFrom_m3ECEAC1E1ED1FF19AE13930016EE8AF9744C6C32,
	InputStreamInfo_t1D0174BE2B5EB4CF4B87459D2B634D2C17CB6E0B_CustomAttributesCacheGenerator_InputStreamInfo__ctor_mD1115B096AA44CBE3BAE01F934CB1DC0E2C8AA78,
	InputStreamInfo_t1D0174BE2B5EB4CF4B87459D2B634D2C17CB6E0B_CustomAttributesCacheGenerator_InputStreamInfo__ctor_m1B40C1CDA73DFEAD87E29B99BCEA761AE4C7A703,
	InputStreamInfo_t1D0174BE2B5EB4CF4B87459D2B634D2C17CB6E0B_CustomAttributesCacheGenerator_InputStreamInfo_Clone_mDFE3D66297DD000E2BA4F70D7856F18919BB7D44,
	InputStreamInfo_t1D0174BE2B5EB4CF4B87459D2B634D2C17CB6E0B_CustomAttributesCacheGenerator_InputStreamInfo_Equals_mD3672C103FBD9DF61B4EB0D8DD7B63EF8863A7C3,
	InputStreamInfo_t1D0174BE2B5EB4CF4B87459D2B634D2C17CB6E0B_CustomAttributesCacheGenerator_InputStreamInfo_Equals_m936804A131005237445BABB5F4EDFC06C448B018,
	InputStreamInfo_t1D0174BE2B5EB4CF4B87459D2B634D2C17CB6E0B_CustomAttributesCacheGenerator_InputStreamInfo_GetHashCode_m3AE4587C347B5910CAF5CC12E662B2064136D676,
	InputStreamInfo_t1D0174BE2B5EB4CF4B87459D2B634D2C17CB6E0B_CustomAttributesCacheGenerator_InputStreamInfo_ToString_m7F4E5F79EAC6EFBC730259B570B113870ACEA553,
	InputStreamInfo_t1D0174BE2B5EB4CF4B87459D2B634D2C17CB6E0B_CustomAttributesCacheGenerator_InputStreamInfo_WriteTo_mD01FACD335355F68B1D530EA10BF2E346EB45567,
	InputStreamInfo_t1D0174BE2B5EB4CF4B87459D2B634D2C17CB6E0B_CustomAttributesCacheGenerator_InputStreamInfo_CalculateSize_mD3CDD8A026DEA91C9FE050645038DD708196E41E,
	InputStreamInfo_t1D0174BE2B5EB4CF4B87459D2B634D2C17CB6E0B_CustomAttributesCacheGenerator_InputStreamInfo_MergeFrom_m9AE0F0C70340F4B1399211E4364CC883F752D325,
	InputStreamInfo_t1D0174BE2B5EB4CF4B87459D2B634D2C17CB6E0B_CustomAttributesCacheGenerator_InputStreamInfo_MergeFrom_mD104C197A6E64A40CB866B1ADC51E93F42761B5E,
	ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968_CustomAttributesCacheGenerator_ProfilerConfig__ctor_m023B7A29789ACE07320D0B31C8CA499115617D79,
	ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968_CustomAttributesCacheGenerator_ProfilerConfig__ctor_m262E9327CC6CA09F9622D776162028033D1BE963,
	ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968_CustomAttributesCacheGenerator_ProfilerConfig_Clone_m9412AE0F8B8C1AF838F6267D697F1ED44148B3D5,
	ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968_CustomAttributesCacheGenerator_ProfilerConfig_Equals_mB3F233DC0F6A2B5D3A0A3E9E77F192F684323BD3,
	ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968_CustomAttributesCacheGenerator_ProfilerConfig_Equals_m8F7A20EE6CD5D39FCC8B178E93A994BE8E2E49B2,
	ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968_CustomAttributesCacheGenerator_ProfilerConfig_GetHashCode_m54E95CD9692BE7C31B86676C97728DD0E633F982,
	ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968_CustomAttributesCacheGenerator_ProfilerConfig_ToString_m3B748ED748B01176BCB4494E2D750DAD217AC2A9,
	ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968_CustomAttributesCacheGenerator_ProfilerConfig_WriteTo_m358D63D2EC966F259D61B184EFE314BD91EFC003,
	ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968_CustomAttributesCacheGenerator_ProfilerConfig_CalculateSize_mAD4E24F9EC25953D971CBFDCA5B9E122B8D29EED,
	ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968_CustomAttributesCacheGenerator_ProfilerConfig_MergeFrom_m832DCC37551A1090377D636FBF3741DD738508C8,
	ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968_CustomAttributesCacheGenerator_ProfilerConfig_MergeFrom_m83749E2EB94F44989249949BF89412B0BE6EEF1B,
	CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348_CustomAttributesCacheGenerator_CalculatorGraphConfig__ctor_m2B2B9C3D725A811963E37BFACE3320D1B01AAD6B,
	CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348_CustomAttributesCacheGenerator_CalculatorGraphConfig__ctor_mC7561D31FBB0998EA48446E954D238BB32F6D258,
	CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348_CustomAttributesCacheGenerator_CalculatorGraphConfig_Clone_m17FBA506A44FF42CB08BBA16B66D753A23FD7F8E,
	CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348_CustomAttributesCacheGenerator_CalculatorGraphConfig_Equals_m64171AE8626030825A893CF84181F3DB88686A78,
	CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348_CustomAttributesCacheGenerator_CalculatorGraphConfig_Equals_m34E76CADA083771B042327F2E6A05098D82BC128,
	CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348_CustomAttributesCacheGenerator_CalculatorGraphConfig_GetHashCode_mB45EBF4282A3719E6557603FD57749C19E2315C2,
	CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348_CustomAttributesCacheGenerator_CalculatorGraphConfig_ToString_mA48B2E2730D472DDD57D0D36B0A0350E05BEE497,
	CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348_CustomAttributesCacheGenerator_CalculatorGraphConfig_WriteTo_m0937B7E7FB01FA089C7EABB73DF1ACCCDB08D5EC,
	CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348_CustomAttributesCacheGenerator_CalculatorGraphConfig_CalculateSize_m47318B281CC424A1787F9864750F2A6056BA8EEC,
	CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348_CustomAttributesCacheGenerator_CalculatorGraphConfig_MergeFrom_m6A80FA1E5F6CD141888C842AD93B8979616CDB2F,
	CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348_CustomAttributesCacheGenerator_CalculatorGraphConfig_MergeFrom_m75F230C7A43154402BAF91F2D7837F2D0C79BA4E,
	Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483_CustomAttributesCacheGenerator_Node__ctor_m5A45C5EA9496703F28618D3FDCD3D9603F43A1C4,
	Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483_CustomAttributesCacheGenerator_Node__ctor_mEF6F5AF4FEF24FF0B17E590D1A99D89ED393FFD4,
	Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483_CustomAttributesCacheGenerator_Node_Clone_m4A14B5A6E780D1C69310984C336E4E8A91214084,
	Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483_CustomAttributesCacheGenerator_Node_Equals_m63B25BF70104CFF3B8CB2C3A3E9DFCAF0D225024,
	Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483_CustomAttributesCacheGenerator_Node_Equals_mCAA8CD4AAA40FC247C882742F39D72EE8EF7C59B,
	Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483_CustomAttributesCacheGenerator_Node_GetHashCode_m426A32AEA408311AC8B43458F2470D78F00F996D,
	Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483_CustomAttributesCacheGenerator_Node_ToString_m6C22CB185110B11818FDB6AE1D71D1DF73DD72F7,
	Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483_CustomAttributesCacheGenerator_Node_WriteTo_m2EAD9127103E77DC06897434BD2F8E070F00E0B8,
	Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483_CustomAttributesCacheGenerator_Node_CalculateSize_m2DC063FE5B6CBB1980AA72F0AA9D7564C541C6DE,
	Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483_CustomAttributesCacheGenerator_Node_MergeFrom_m1D3E147794EB001C4E41FECDBD9EDA04D1803378,
	Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483_CustomAttributesCacheGenerator_Node_MergeFrom_mD3BB28988C717504CE6F43642FF64E4EAF34D512,
	CalculatorOptions_t258E073EB6EBC7E44A9C100A180A4F40D5F89857_CustomAttributesCacheGenerator_CalculatorOptions__ctor_m9A66335BA5FAB6F4F057182BAF253B3A586C55FA,
	CalculatorOptions_t258E073EB6EBC7E44A9C100A180A4F40D5F89857_CustomAttributesCacheGenerator_CalculatorOptions__ctor_m022E05735F6CE2036FC8135812D6AFAF853F2D16,
	CalculatorOptions_t258E073EB6EBC7E44A9C100A180A4F40D5F89857_CustomAttributesCacheGenerator_CalculatorOptions_Clone_m717403D7365C851A4F8558834B83CD542CEE0571,
	CalculatorOptions_t258E073EB6EBC7E44A9C100A180A4F40D5F89857_CustomAttributesCacheGenerator_CalculatorOptions_ClearMergeFields_mFD3A53D320B135AFE511B684F9919729200C090D,
	CalculatorOptions_t258E073EB6EBC7E44A9C100A180A4F40D5F89857_CustomAttributesCacheGenerator_CalculatorOptions_Equals_m1A6E5C0DB2FEB400FA0D5F76665FA4F37BDE8B03,
	CalculatorOptions_t258E073EB6EBC7E44A9C100A180A4F40D5F89857_CustomAttributesCacheGenerator_CalculatorOptions_Equals_m1511C43D99252F232CAF2CE903A223A9B7924B87,
	CalculatorOptions_t258E073EB6EBC7E44A9C100A180A4F40D5F89857_CustomAttributesCacheGenerator_CalculatorOptions_GetHashCode_m5879C42E02EDE756A47DFDE711951052D2BBF4D1,
	CalculatorOptions_t258E073EB6EBC7E44A9C100A180A4F40D5F89857_CustomAttributesCacheGenerator_CalculatorOptions_ToString_mB5C0791D4DB12DAAC8A21F528A1F1DA7BFC06ADF,
	CalculatorOptions_t258E073EB6EBC7E44A9C100A180A4F40D5F89857_CustomAttributesCacheGenerator_CalculatorOptions_WriteTo_m55AAD179414854C4BD37ED1895396F9EBBFEB1E7,
	CalculatorOptions_t258E073EB6EBC7E44A9C100A180A4F40D5F89857_CustomAttributesCacheGenerator_CalculatorOptions_CalculateSize_m9612BCA7552905D230E60EEAAFBAF60DA3767572,
	CalculatorOptions_t258E073EB6EBC7E44A9C100A180A4F40D5F89857_CustomAttributesCacheGenerator_CalculatorOptions_MergeFrom_mB9BBBF0230136216E2CB33D75343BC9CBD5EDE97,
	CalculatorOptions_t258E073EB6EBC7E44A9C100A180A4F40D5F89857_CustomAttributesCacheGenerator_CalculatorOptions_MergeFrom_m8F46A283DBD6012F2F985C795D06856AD6CD0E72,
	Classification_tE9A954065398D143A5B6EEA01FAED5BCD7D81BB9_CustomAttributesCacheGenerator_Classification__ctor_mA0FD3F6BC90023C990B4B88F09F9CA77B8DBF669,
	Classification_tE9A954065398D143A5B6EEA01FAED5BCD7D81BB9_CustomAttributesCacheGenerator_Classification__ctor_mEA745239FD6942EA8EBFCC2C66AA5C12645FD828,
	Classification_tE9A954065398D143A5B6EEA01FAED5BCD7D81BB9_CustomAttributesCacheGenerator_Classification_Clone_m75EB64A47676D4684C1EF7F86F3053F9360ECD5B,
	Classification_tE9A954065398D143A5B6EEA01FAED5BCD7D81BB9_CustomAttributesCacheGenerator_Classification_ClearIndex_m19867CF6E45A80A42275612C11C9AAAE7B684CA6,
	Classification_tE9A954065398D143A5B6EEA01FAED5BCD7D81BB9_CustomAttributesCacheGenerator_Classification_ClearScore_m16E3BC42F4227EB6B6ECADA43520EF7363DA1278,
	Classification_tE9A954065398D143A5B6EEA01FAED5BCD7D81BB9_CustomAttributesCacheGenerator_Classification_ClearLabel_m6F8D5E7CF22D2A405DA815F24248399CDCA66134,
	Classification_tE9A954065398D143A5B6EEA01FAED5BCD7D81BB9_CustomAttributesCacheGenerator_Classification_ClearDisplayName_mAA08FE13D01E327D673D03547E9AAB60250C59C4,
	Classification_tE9A954065398D143A5B6EEA01FAED5BCD7D81BB9_CustomAttributesCacheGenerator_Classification_Equals_mA50D3D642CB1069D2540F7FECA301BCB9E7ABF8F,
	Classification_tE9A954065398D143A5B6EEA01FAED5BCD7D81BB9_CustomAttributesCacheGenerator_Classification_Equals_m46491A3E7C368EAD299737A771C47E78F8744E88,
	Classification_tE9A954065398D143A5B6EEA01FAED5BCD7D81BB9_CustomAttributesCacheGenerator_Classification_GetHashCode_mC63AA2FB66A9CB60B0969C8410E40986ED657247,
	Classification_tE9A954065398D143A5B6EEA01FAED5BCD7D81BB9_CustomAttributesCacheGenerator_Classification_ToString_m6D17450C980C2A8E9082FC523C0A78943D27E6F0,
	Classification_tE9A954065398D143A5B6EEA01FAED5BCD7D81BB9_CustomAttributesCacheGenerator_Classification_WriteTo_m53061790010FE08E18DB482140160B2063EB583C,
	Classification_tE9A954065398D143A5B6EEA01FAED5BCD7D81BB9_CustomAttributesCacheGenerator_Classification_CalculateSize_mFF773292996625892D0F77C07A43D0B65C7C931D,
	Classification_tE9A954065398D143A5B6EEA01FAED5BCD7D81BB9_CustomAttributesCacheGenerator_Classification_MergeFrom_m8D981270B6DB8836F391140E0597189E1FCC74E3,
	Classification_tE9A954065398D143A5B6EEA01FAED5BCD7D81BB9_CustomAttributesCacheGenerator_Classification_MergeFrom_m928242B2CE6F22D7577F5ACB8A01B507B34995C2,
	ClassificationList_tC58BDD049F1D6A853B63B07F07DCFA52DC02A8B5_CustomAttributesCacheGenerator_ClassificationList__ctor_m99E71A343606FDE18A16A30431F3FB10C8E85400,
	ClassificationList_tC58BDD049F1D6A853B63B07F07DCFA52DC02A8B5_CustomAttributesCacheGenerator_ClassificationList__ctor_mCF0BED5C2EF4578C32BAF890A31DF4548E394480,
	ClassificationList_tC58BDD049F1D6A853B63B07F07DCFA52DC02A8B5_CustomAttributesCacheGenerator_ClassificationList_Clone_m1BEC3137BD0BD0ADC62E511E6D6C00FC616B30A3,
	ClassificationList_tC58BDD049F1D6A853B63B07F07DCFA52DC02A8B5_CustomAttributesCacheGenerator_ClassificationList_Equals_m2EBF80429667EEC0655AFE46B55349A6A4ECAB79,
	ClassificationList_tC58BDD049F1D6A853B63B07F07DCFA52DC02A8B5_CustomAttributesCacheGenerator_ClassificationList_Equals_mC9CB38807A2EC3B87251459B9C540C3DB2D8AA91,
	ClassificationList_tC58BDD049F1D6A853B63B07F07DCFA52DC02A8B5_CustomAttributesCacheGenerator_ClassificationList_GetHashCode_mA2375D105268BBA492A629CB6D74F776ED246C01,
	ClassificationList_tC58BDD049F1D6A853B63B07F07DCFA52DC02A8B5_CustomAttributesCacheGenerator_ClassificationList_ToString_m8A6C7A594C9DA6CFAEB291FAF1C4419350870DE2,
	ClassificationList_tC58BDD049F1D6A853B63B07F07DCFA52DC02A8B5_CustomAttributesCacheGenerator_ClassificationList_WriteTo_m5C28B1914E8CBA3CDD0CC26E009A49FDACE1E1B1,
	ClassificationList_tC58BDD049F1D6A853B63B07F07DCFA52DC02A8B5_CustomAttributesCacheGenerator_ClassificationList_CalculateSize_m06E066709F39339F2F9880D3EB7A9CCDD1950F88,
	ClassificationList_tC58BDD049F1D6A853B63B07F07DCFA52DC02A8B5_CustomAttributesCacheGenerator_ClassificationList_MergeFrom_m771CE3B97011B6A02527AA5F8AB056CC280A4759,
	ClassificationList_tC58BDD049F1D6A853B63B07F07DCFA52DC02A8B5_CustomAttributesCacheGenerator_ClassificationList_MergeFrom_m11F8284D369551729C93A32BC16394DA6990280C,
	Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E_CustomAttributesCacheGenerator_Detection__ctor_m55A7113BDFA8EF7BE08FA4B349F34A3565313C2C,
	Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E_CustomAttributesCacheGenerator_Detection__ctor_m9A9FD199DC21FB37D14C03361D20D3D8E727FA25,
	Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E_CustomAttributesCacheGenerator_Detection_Clone_mD7CBC5BAEBC3C6C75A793B8F3C68C9302369C26B,
	Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E_CustomAttributesCacheGenerator_Detection_ClearLocationData_m1F6F3AB12BA2553C34D4438B8DAA691413B2F900,
	Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E_CustomAttributesCacheGenerator_Detection_ClearFeatureTag_m2E2491436E69DB5B53F661B3E472B28FAEA25EFE,
	Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E_CustomAttributesCacheGenerator_Detection_ClearTrackId_m966839C219ACA6DCC0636BFD8EDFF70048B82DC9,
	Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E_CustomAttributesCacheGenerator_Detection_ClearDetectionId_m9CB64FD41C0C973CFD6AFAAA30170431AEC808F7,
	Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E_CustomAttributesCacheGenerator_Detection_ClearTimestampUsec_m5E7F1296F66D3F6C41227404FD63C067189A97D9,
	Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E_CustomAttributesCacheGenerator_Detection_Equals_mF155165DC1D42CDC67B1CA09A89384560162D4B0,
	Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E_CustomAttributesCacheGenerator_Detection_Equals_mD313C1A5318C95D4E59E243292D9C147B7A49460,
	Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E_CustomAttributesCacheGenerator_Detection_GetHashCode_mB7B8FB777B8621E0928027D44910CC0971BFA294,
	Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E_CustomAttributesCacheGenerator_Detection_ToString_mE695F914BD60F8E148D00D86E4FBB5C060007026,
	Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E_CustomAttributesCacheGenerator_Detection_WriteTo_m871A5B247BDFCCA600B3C8FE18949C639DB691B9,
	Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E_CustomAttributesCacheGenerator_Detection_CalculateSize_m729B385BE443B130004A0016D33D88914B391E01,
	Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E_CustomAttributesCacheGenerator_Detection_MergeFrom_m6FD510B8AA365A2AB532AF3FA7AC12E7CA62B444,
	Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E_CustomAttributesCacheGenerator_Detection_MergeFrom_m677B85361862396FF00F51831D73EFF565CA7532,
	AssociatedDetection_t24212822C1761635604505177F1691F542A4DF88_CustomAttributesCacheGenerator_AssociatedDetection__ctor_mA166F2C623931F25847860332A520ABAA5CC9DB4,
	AssociatedDetection_t24212822C1761635604505177F1691F542A4DF88_CustomAttributesCacheGenerator_AssociatedDetection__ctor_m1BFE2C623E6B59BB029246429752139FFA5455BC,
	AssociatedDetection_t24212822C1761635604505177F1691F542A4DF88_CustomAttributesCacheGenerator_AssociatedDetection_Clone_m11C498A5BFF91B15128640FE1FF6C186DC791115,
	AssociatedDetection_t24212822C1761635604505177F1691F542A4DF88_CustomAttributesCacheGenerator_AssociatedDetection_ClearId_m1329E13167EA8CDBCFD82765534017CF7CCBC2F4,
	AssociatedDetection_t24212822C1761635604505177F1691F542A4DF88_CustomAttributesCacheGenerator_AssociatedDetection_ClearConfidence_m552D9205076452F3657B3C6A145064F6FE0CEEAD,
	AssociatedDetection_t24212822C1761635604505177F1691F542A4DF88_CustomAttributesCacheGenerator_AssociatedDetection_Equals_mD472A75917A686CB5371B84ADC4B8667091541FF,
	AssociatedDetection_t24212822C1761635604505177F1691F542A4DF88_CustomAttributesCacheGenerator_AssociatedDetection_Equals_m1069816F34AFE7A08AD3953CFC280417427FDA02,
	AssociatedDetection_t24212822C1761635604505177F1691F542A4DF88_CustomAttributesCacheGenerator_AssociatedDetection_GetHashCode_mE8B82EDD364192C96CFECF67D9C44ACD43128F64,
	AssociatedDetection_t24212822C1761635604505177F1691F542A4DF88_CustomAttributesCacheGenerator_AssociatedDetection_ToString_m7AF4B716FBE2D34C584115309FD4DC10F3B1D396,
	AssociatedDetection_t24212822C1761635604505177F1691F542A4DF88_CustomAttributesCacheGenerator_AssociatedDetection_WriteTo_m6CF6A142DD499EA8E2564F71291BE2FCD068DF87,
	AssociatedDetection_t24212822C1761635604505177F1691F542A4DF88_CustomAttributesCacheGenerator_AssociatedDetection_CalculateSize_m31A05FE7461704430FFD30B726F2EFE701EA7F05,
	AssociatedDetection_t24212822C1761635604505177F1691F542A4DF88_CustomAttributesCacheGenerator_AssociatedDetection_MergeFrom_m1C3C649269388963B9FA33ED78AA4FF2FA0DB27D,
	AssociatedDetection_t24212822C1761635604505177F1691F542A4DF88_CustomAttributesCacheGenerator_AssociatedDetection_MergeFrom_m2BB477AACB83B0CF3CDF2C0161D97898E2152612,
	DetectionList_tA50FF81C129AF30DDF87A1F025CD9B8B4A10EF29_CustomAttributesCacheGenerator_DetectionList__ctor_m06FDB5957ED0F6F0B39F5738E06C2F1159B37183,
	DetectionList_tA50FF81C129AF30DDF87A1F025CD9B8B4A10EF29_CustomAttributesCacheGenerator_DetectionList__ctor_mAE98482B38CBE076985F9128D7281E33546913D1,
	DetectionList_tA50FF81C129AF30DDF87A1F025CD9B8B4A10EF29_CustomAttributesCacheGenerator_DetectionList_Clone_m5331EB486F6B61CFB67E0BF4ECD4BE8AB966B807,
	DetectionList_tA50FF81C129AF30DDF87A1F025CD9B8B4A10EF29_CustomAttributesCacheGenerator_DetectionList_Equals_m298DE49B1E060D9C24B467E5A55A8C1D145923D4,
	DetectionList_tA50FF81C129AF30DDF87A1F025CD9B8B4A10EF29_CustomAttributesCacheGenerator_DetectionList_Equals_m6E7B500E4EADA19BAFBE795DFDCFBA9A8B44596F,
	DetectionList_tA50FF81C129AF30DDF87A1F025CD9B8B4A10EF29_CustomAttributesCacheGenerator_DetectionList_GetHashCode_m807A6737A246CB6010A539204908F6F27215C8EE,
	DetectionList_tA50FF81C129AF30DDF87A1F025CD9B8B4A10EF29_CustomAttributesCacheGenerator_DetectionList_ToString_mB5437A519C9DDF1885D468B7324AADDE901CB12D,
	DetectionList_tA50FF81C129AF30DDF87A1F025CD9B8B4A10EF29_CustomAttributesCacheGenerator_DetectionList_WriteTo_m7BA51D678FE5CAD8ADD3A9FDEFE9211D36CB8420,
	DetectionList_tA50FF81C129AF30DDF87A1F025CD9B8B4A10EF29_CustomAttributesCacheGenerator_DetectionList_CalculateSize_mD096AE8E4FAC18AAA69EFAE4CACF35BDA4A4AC58,
	DetectionList_tA50FF81C129AF30DDF87A1F025CD9B8B4A10EF29_CustomAttributesCacheGenerator_DetectionList_MergeFrom_mDF65E3B58421A40462E72B81742D574CC1C9458B,
	DetectionList_tA50FF81C129AF30DDF87A1F025CD9B8B4A10EF29_CustomAttributesCacheGenerator_DetectionList_MergeFrom_m630B2FE189B963D229CE290BA57F9AA276645AEC,
	Landmark_t593F12E6EAABFA00011E82F8D4408C3F75E4EFE3_CustomAttributesCacheGenerator_Landmark__ctor_m32AB2F02006F1DA904DB9B95FBA6F54438CB7E09,
	Landmark_t593F12E6EAABFA00011E82F8D4408C3F75E4EFE3_CustomAttributesCacheGenerator_Landmark__ctor_mB5954D418050FA757FEEAA123D3DB388279E0D66,
	Landmark_t593F12E6EAABFA00011E82F8D4408C3F75E4EFE3_CustomAttributesCacheGenerator_Landmark_Clone_m2F263E193E5C29A90092AE5EE1AC5B3880CAAAEC,
	Landmark_t593F12E6EAABFA00011E82F8D4408C3F75E4EFE3_CustomAttributesCacheGenerator_Landmark_ClearX_m75472AA9F683DEC71B30978EA0A088121462A016,
	Landmark_t593F12E6EAABFA00011E82F8D4408C3F75E4EFE3_CustomAttributesCacheGenerator_Landmark_ClearY_mFE69B3F9E85D3947683E92A28E5F6D43F0FC2BAA,
	Landmark_t593F12E6EAABFA00011E82F8D4408C3F75E4EFE3_CustomAttributesCacheGenerator_Landmark_ClearZ_m77CF2955E6B9A2F237E810067B15CD756B9B3A3A,
	Landmark_t593F12E6EAABFA00011E82F8D4408C3F75E4EFE3_CustomAttributesCacheGenerator_Landmark_ClearVisibility_m63565970A485B48F1E5A549D2A9468B6CB05EF7D,
	Landmark_t593F12E6EAABFA00011E82F8D4408C3F75E4EFE3_CustomAttributesCacheGenerator_Landmark_ClearPresence_m04EEA91E043A0665A4B35DCC26206F73D5643763,
	Landmark_t593F12E6EAABFA00011E82F8D4408C3F75E4EFE3_CustomAttributesCacheGenerator_Landmark_Equals_m5DF27AE33F5ADCA0F166401F6EF682568172E3CD,
	Landmark_t593F12E6EAABFA00011E82F8D4408C3F75E4EFE3_CustomAttributesCacheGenerator_Landmark_Equals_m5DCEFC736DAAB541ECAF07682E0CBA342E06F8C5,
	Landmark_t593F12E6EAABFA00011E82F8D4408C3F75E4EFE3_CustomAttributesCacheGenerator_Landmark_GetHashCode_m1D05AEC667DFD74EF7BD565F23D18E9F2D9EBABF,
	Landmark_t593F12E6EAABFA00011E82F8D4408C3F75E4EFE3_CustomAttributesCacheGenerator_Landmark_ToString_m8A1ADCAF99F8EAB736244D2D64180F410747E9B6,
	Landmark_t593F12E6EAABFA00011E82F8D4408C3F75E4EFE3_CustomAttributesCacheGenerator_Landmark_WriteTo_m2426178F2F1BE3E779166512562C66D5CE55E2DB,
	Landmark_t593F12E6EAABFA00011E82F8D4408C3F75E4EFE3_CustomAttributesCacheGenerator_Landmark_CalculateSize_m57A5261BCE0E7D5FAD4404AB917F2F7D5709CA8A,
	Landmark_t593F12E6EAABFA00011E82F8D4408C3F75E4EFE3_CustomAttributesCacheGenerator_Landmark_MergeFrom_m601FEE1790CD75767288FECA657CEEC9516EB0AB,
	Landmark_t593F12E6EAABFA00011E82F8D4408C3F75E4EFE3_CustomAttributesCacheGenerator_Landmark_MergeFrom_mA2E18E48D58A71220844E0C26DBA09D4E2DED0BB,
	LandmarkList_t81090E798AEC4F7C446243CBAD3BDB14C16525EA_CustomAttributesCacheGenerator_LandmarkList__ctor_mB39DEFB418B9FB30F8E826F7398A5D2F2993154B,
	LandmarkList_t81090E798AEC4F7C446243CBAD3BDB14C16525EA_CustomAttributesCacheGenerator_LandmarkList__ctor_mAB0C124B8A91ECD95FEEA961A5B7AB35704CBF9A,
	LandmarkList_t81090E798AEC4F7C446243CBAD3BDB14C16525EA_CustomAttributesCacheGenerator_LandmarkList_Clone_m162BFB1581722DA392C42B14FF4453FE63924E8A,
	LandmarkList_t81090E798AEC4F7C446243CBAD3BDB14C16525EA_CustomAttributesCacheGenerator_LandmarkList_Equals_m67DF4B25C4F479352B3A65E7BE42E3FC87122CAA,
	LandmarkList_t81090E798AEC4F7C446243CBAD3BDB14C16525EA_CustomAttributesCacheGenerator_LandmarkList_Equals_mEF4EFD2347D65884B176A40B3F9C6A316D41C8B4,
	LandmarkList_t81090E798AEC4F7C446243CBAD3BDB14C16525EA_CustomAttributesCacheGenerator_LandmarkList_GetHashCode_m9BD8E4A4309FF6C0CEE9C524C47140FB34A2565E,
	LandmarkList_t81090E798AEC4F7C446243CBAD3BDB14C16525EA_CustomAttributesCacheGenerator_LandmarkList_ToString_m8B21B5CB0BA7CB7F08FED7BA6D77FC136CF2DF3C,
	LandmarkList_t81090E798AEC4F7C446243CBAD3BDB14C16525EA_CustomAttributesCacheGenerator_LandmarkList_WriteTo_m31B2E7A45E2503C6D1600AB5EF7902F9B5245D07,
	LandmarkList_t81090E798AEC4F7C446243CBAD3BDB14C16525EA_CustomAttributesCacheGenerator_LandmarkList_CalculateSize_m53AFAD8AED8C89539F906D99555FDAFF55CC0A5D,
	LandmarkList_t81090E798AEC4F7C446243CBAD3BDB14C16525EA_CustomAttributesCacheGenerator_LandmarkList_MergeFrom_m8FAB382380EFEDB9FDC963AB56EE75A624A26283,
	LandmarkList_t81090E798AEC4F7C446243CBAD3BDB14C16525EA_CustomAttributesCacheGenerator_LandmarkList_MergeFrom_m84474E94FCBB33DCEFCAFE1F005BACDF7D381A67,
	NormalizedLandmark_t5FAF8FE108D8B6740A93A860AF7786612D36A48C_CustomAttributesCacheGenerator_NormalizedLandmark__ctor_m749D7C799F92DEC13A50FBF151B384E803BFFF52,
	NormalizedLandmark_t5FAF8FE108D8B6740A93A860AF7786612D36A48C_CustomAttributesCacheGenerator_NormalizedLandmark__ctor_mB9A73BE195E17D391BC26DC9C4A9FE7CD3FCF784,
	NormalizedLandmark_t5FAF8FE108D8B6740A93A860AF7786612D36A48C_CustomAttributesCacheGenerator_NormalizedLandmark_Clone_mEA928C357031BFF03D7B3463A38391F589530EFA,
	NormalizedLandmark_t5FAF8FE108D8B6740A93A860AF7786612D36A48C_CustomAttributesCacheGenerator_NormalizedLandmark_ClearX_mF00D31AC4974CD5DFC81875A4A6FC3143DE11626,
	NormalizedLandmark_t5FAF8FE108D8B6740A93A860AF7786612D36A48C_CustomAttributesCacheGenerator_NormalizedLandmark_ClearY_mBA1143EE451962D9CCD7D6490DEE4B109E402477,
	NormalizedLandmark_t5FAF8FE108D8B6740A93A860AF7786612D36A48C_CustomAttributesCacheGenerator_NormalizedLandmark_ClearZ_m43166BF7D6C2B8CC8455116E3ED711FFAD77E03B,
	NormalizedLandmark_t5FAF8FE108D8B6740A93A860AF7786612D36A48C_CustomAttributesCacheGenerator_NormalizedLandmark_ClearVisibility_m462FB2CAAF3A6987E414033FC435E9EEA260EC45,
	NormalizedLandmark_t5FAF8FE108D8B6740A93A860AF7786612D36A48C_CustomAttributesCacheGenerator_NormalizedLandmark_ClearPresence_m3BA6187034736183F1237A19186AB4F7C5D98B68,
	NormalizedLandmark_t5FAF8FE108D8B6740A93A860AF7786612D36A48C_CustomAttributesCacheGenerator_NormalizedLandmark_Equals_m7212B45EE8C95C27A16E61219CF0BC7B63508158,
	NormalizedLandmark_t5FAF8FE108D8B6740A93A860AF7786612D36A48C_CustomAttributesCacheGenerator_NormalizedLandmark_Equals_m694303789E20659F4755195A665597050EA7E661,
	NormalizedLandmark_t5FAF8FE108D8B6740A93A860AF7786612D36A48C_CustomAttributesCacheGenerator_NormalizedLandmark_GetHashCode_m2ABE8B45C235972D5D5E33F4E688B9B6014840F5,
	NormalizedLandmark_t5FAF8FE108D8B6740A93A860AF7786612D36A48C_CustomAttributesCacheGenerator_NormalizedLandmark_ToString_m2071AEC6E2374C9F11165F7CDFFE5720E273362E,
	NormalizedLandmark_t5FAF8FE108D8B6740A93A860AF7786612D36A48C_CustomAttributesCacheGenerator_NormalizedLandmark_WriteTo_m33870DE137A9B7ED976F9A092FFA7B9F0A1930B5,
	NormalizedLandmark_t5FAF8FE108D8B6740A93A860AF7786612D36A48C_CustomAttributesCacheGenerator_NormalizedLandmark_CalculateSize_mE3D06A71DF8BE59866DE7837D6A848673525A73A,
	NormalizedLandmark_t5FAF8FE108D8B6740A93A860AF7786612D36A48C_CustomAttributesCacheGenerator_NormalizedLandmark_MergeFrom_m903A7C27238CE6DC37619530278D38EC5E7124B1,
	NormalizedLandmark_t5FAF8FE108D8B6740A93A860AF7786612D36A48C_CustomAttributesCacheGenerator_NormalizedLandmark_MergeFrom_m0348956EE9DEA0CDEE4760CCBBE0B275EE0DB93A,
	NormalizedLandmarkList_tD96817E5BBC04C5051186DE23E72CA9FB2E8E11E_CustomAttributesCacheGenerator_NormalizedLandmarkList__ctor_m01D41F6D909C6F3B3B7EEE22709861C7B29D61FA,
	NormalizedLandmarkList_tD96817E5BBC04C5051186DE23E72CA9FB2E8E11E_CustomAttributesCacheGenerator_NormalizedLandmarkList__ctor_m75C1E44F5F089A869ED0E929367C75160499360C,
	NormalizedLandmarkList_tD96817E5BBC04C5051186DE23E72CA9FB2E8E11E_CustomAttributesCacheGenerator_NormalizedLandmarkList_Clone_m0858F45BAAB39546DBF5853A5FCB0E2395F34E54,
	NormalizedLandmarkList_tD96817E5BBC04C5051186DE23E72CA9FB2E8E11E_CustomAttributesCacheGenerator_NormalizedLandmarkList_Equals_m0B854C7B06A98D47E68E15B965FE8EDEE4445CDD,
	NormalizedLandmarkList_tD96817E5BBC04C5051186DE23E72CA9FB2E8E11E_CustomAttributesCacheGenerator_NormalizedLandmarkList_Equals_mF6220EBEBE45837C5C7ABE8A60139A168CFD6555,
	NormalizedLandmarkList_tD96817E5BBC04C5051186DE23E72CA9FB2E8E11E_CustomAttributesCacheGenerator_NormalizedLandmarkList_GetHashCode_m8815B70BC600FCAC3B59DFF49C681B3351F4C493,
	NormalizedLandmarkList_tD96817E5BBC04C5051186DE23E72CA9FB2E8E11E_CustomAttributesCacheGenerator_NormalizedLandmarkList_ToString_mE0E29E7405F94D7B9B37CC75B53A64A473B142E9,
	NormalizedLandmarkList_tD96817E5BBC04C5051186DE23E72CA9FB2E8E11E_CustomAttributesCacheGenerator_NormalizedLandmarkList_WriteTo_m7030BCD0F34648E08815E519717EF288531D0070,
	NormalizedLandmarkList_tD96817E5BBC04C5051186DE23E72CA9FB2E8E11E_CustomAttributesCacheGenerator_NormalizedLandmarkList_CalculateSize_mDBDAE3F1AA8E835537862295EC26955E3ECBAB71,
	NormalizedLandmarkList_tD96817E5BBC04C5051186DE23E72CA9FB2E8E11E_CustomAttributesCacheGenerator_NormalizedLandmarkList_MergeFrom_m9E3B0324A50B637AEC4F25E875C6061F5FF0231F,
	NormalizedLandmarkList_tD96817E5BBC04C5051186DE23E72CA9FB2E8E11E_CustomAttributesCacheGenerator_NormalizedLandmarkList_MergeFrom_mC7D95F8D50AF1080D9B19AAC9DD77E7376766312,
	LocationData_t137C5EE37EACB9E28B218F2FF28B3BFFF27BC81A_CustomAttributesCacheGenerator_LocationData__ctor_mB604C634DB577934AE64462669C6C3764CB2000D,
	LocationData_t137C5EE37EACB9E28B218F2FF28B3BFFF27BC81A_CustomAttributesCacheGenerator_LocationData__ctor_m6D1AEDF1838BB6FC4150FD00A021962307D60372,
	LocationData_t137C5EE37EACB9E28B218F2FF28B3BFFF27BC81A_CustomAttributesCacheGenerator_LocationData_Clone_m79CB2770CCAA00A4C9BB99459225994D1291D8F9,
	LocationData_t137C5EE37EACB9E28B218F2FF28B3BFFF27BC81A_CustomAttributesCacheGenerator_LocationData_ClearFormat_mC84F4F1946BB3CE469972A09C41E2008F4B0A7CB,
	LocationData_t137C5EE37EACB9E28B218F2FF28B3BFFF27BC81A_CustomAttributesCacheGenerator_LocationData_ClearBoundingBox_m5EDCE0CB5DC3978B1728178FDBDBA170265FC6F5,
	LocationData_t137C5EE37EACB9E28B218F2FF28B3BFFF27BC81A_CustomAttributesCacheGenerator_LocationData_ClearRelativeBoundingBox_m30E69638BD31A7ABFF6EA8607F0B8716ED807B99,
	LocationData_t137C5EE37EACB9E28B218F2FF28B3BFFF27BC81A_CustomAttributesCacheGenerator_LocationData_ClearMask_m76CE8607652C20FFBAEAE510127E861DDB3677CC,
	LocationData_t137C5EE37EACB9E28B218F2FF28B3BFFF27BC81A_CustomAttributesCacheGenerator_LocationData_Equals_m170A452AFFA5AD61A8E91F3A2C414E43B776262E,
	LocationData_t137C5EE37EACB9E28B218F2FF28B3BFFF27BC81A_CustomAttributesCacheGenerator_LocationData_Equals_m898CD12FC26B6B0589CB403E9C6B29550877A69D,
	LocationData_t137C5EE37EACB9E28B218F2FF28B3BFFF27BC81A_CustomAttributesCacheGenerator_LocationData_GetHashCode_m307B9D8787153A4DFD1C54C01FE4D9BD05672255,
	LocationData_t137C5EE37EACB9E28B218F2FF28B3BFFF27BC81A_CustomAttributesCacheGenerator_LocationData_ToString_m29E48F542440D30B1734C7DB05DFB9CCE5202A05,
	LocationData_t137C5EE37EACB9E28B218F2FF28B3BFFF27BC81A_CustomAttributesCacheGenerator_LocationData_WriteTo_mA5DA809FB04E3707BF837E4352734EC94D6B2B95,
	LocationData_t137C5EE37EACB9E28B218F2FF28B3BFFF27BC81A_CustomAttributesCacheGenerator_LocationData_CalculateSize_m0E812979AB5E6E5CCEA276AEA562C84BB021E4CB,
	LocationData_t137C5EE37EACB9E28B218F2FF28B3BFFF27BC81A_CustomAttributesCacheGenerator_LocationData_MergeFrom_mCAA4BC513485F808DD7A20C6281A4B546A237D05,
	LocationData_t137C5EE37EACB9E28B218F2FF28B3BFFF27BC81A_CustomAttributesCacheGenerator_LocationData_MergeFrom_m388838B258FB88CE12864E45E68DD5F22274BE5C,
	BoundingBox_tF4DCB9A65CC691059CAC02F205F73759C1A7ADAC_CustomAttributesCacheGenerator_BoundingBox__ctor_m593B06506BFD3A2B0775E81312CF30B4E63C4D65,
	BoundingBox_tF4DCB9A65CC691059CAC02F205F73759C1A7ADAC_CustomAttributesCacheGenerator_BoundingBox__ctor_m627AE04BA213E57A63AEC13B676D7F5B74DC92FF,
	BoundingBox_tF4DCB9A65CC691059CAC02F205F73759C1A7ADAC_CustomAttributesCacheGenerator_BoundingBox_Clone_m00AE731CD8CDFCDC53DA43A8065BEEC74E9DD5B0,
	BoundingBox_tF4DCB9A65CC691059CAC02F205F73759C1A7ADAC_CustomAttributesCacheGenerator_BoundingBox_ClearXmin_m17EB77B29A2B29A9B182986F8C45D7C0DDE4238D,
	BoundingBox_tF4DCB9A65CC691059CAC02F205F73759C1A7ADAC_CustomAttributesCacheGenerator_BoundingBox_ClearYmin_m786E94CB3B96169321D4E023816E36B3E111D402,
	BoundingBox_tF4DCB9A65CC691059CAC02F205F73759C1A7ADAC_CustomAttributesCacheGenerator_BoundingBox_ClearWidth_mF1280F4300C6409B29F083612CC43AE23EF27BBD,
	BoundingBox_tF4DCB9A65CC691059CAC02F205F73759C1A7ADAC_CustomAttributesCacheGenerator_BoundingBox_ClearHeight_m39635CDC84989A610ECA78395F36D44D5E752E24,
	BoundingBox_tF4DCB9A65CC691059CAC02F205F73759C1A7ADAC_CustomAttributesCacheGenerator_BoundingBox_Equals_m59EADA8FA336A51CAA7BDAC92C9659F352E0EA82,
	BoundingBox_tF4DCB9A65CC691059CAC02F205F73759C1A7ADAC_CustomAttributesCacheGenerator_BoundingBox_Equals_mA301D6545DCCF5CDDB551F3F5D3E0EA14BE4FDB9,
	BoundingBox_tF4DCB9A65CC691059CAC02F205F73759C1A7ADAC_CustomAttributesCacheGenerator_BoundingBox_GetHashCode_mA2E1A26961E791956B155392365ABE0CFD32C9F4,
	BoundingBox_tF4DCB9A65CC691059CAC02F205F73759C1A7ADAC_CustomAttributesCacheGenerator_BoundingBox_ToString_m59B089B9CD4F5D49F89881F869DC857AFC3B78A8,
	BoundingBox_tF4DCB9A65CC691059CAC02F205F73759C1A7ADAC_CustomAttributesCacheGenerator_BoundingBox_WriteTo_m5893434F89C8673816C4FA7884F8BD8CEE7B8087,
	BoundingBox_tF4DCB9A65CC691059CAC02F205F73759C1A7ADAC_CustomAttributesCacheGenerator_BoundingBox_CalculateSize_m3FF05A537A0E9A624B5FABFE60EBC6FFB10F5A23,
	BoundingBox_tF4DCB9A65CC691059CAC02F205F73759C1A7ADAC_CustomAttributesCacheGenerator_BoundingBox_MergeFrom_m8FE73B1C9B61C5307C66B1C3D706836388AC89FC,
	BoundingBox_tF4DCB9A65CC691059CAC02F205F73759C1A7ADAC_CustomAttributesCacheGenerator_BoundingBox_MergeFrom_mC6EFA4D2E99DEC21D436A2A6D90362394A47BFFD,
	RelativeBoundingBox_tC31BDE3CCCD2009BC2562071BDB4B5DB13F6752E_CustomAttributesCacheGenerator_RelativeBoundingBox__ctor_mA8BC438A7E04136689A5998E0D7EE0CD98C66B6A,
	RelativeBoundingBox_tC31BDE3CCCD2009BC2562071BDB4B5DB13F6752E_CustomAttributesCacheGenerator_RelativeBoundingBox__ctor_m4944F2FED20D1A230D2BF9E5D6A32F725CFCD6EF,
	RelativeBoundingBox_tC31BDE3CCCD2009BC2562071BDB4B5DB13F6752E_CustomAttributesCacheGenerator_RelativeBoundingBox_Clone_m0841DF2E2BFB704DFDCE1809E145778315BE1633,
	RelativeBoundingBox_tC31BDE3CCCD2009BC2562071BDB4B5DB13F6752E_CustomAttributesCacheGenerator_RelativeBoundingBox_ClearXmin_m28CEF281DAEC7AF7C8ED9F0E471553717368B8C3,
	RelativeBoundingBox_tC31BDE3CCCD2009BC2562071BDB4B5DB13F6752E_CustomAttributesCacheGenerator_RelativeBoundingBox_ClearYmin_mD759F2E5D785928C94DA4D2A89BD34A34BA7F11B,
	RelativeBoundingBox_tC31BDE3CCCD2009BC2562071BDB4B5DB13F6752E_CustomAttributesCacheGenerator_RelativeBoundingBox_ClearWidth_m72C70182A243C7353F98E8899B82EF42648B98E1,
	RelativeBoundingBox_tC31BDE3CCCD2009BC2562071BDB4B5DB13F6752E_CustomAttributesCacheGenerator_RelativeBoundingBox_ClearHeight_mD70EE677AB7F7D7C9573EF087BD90D08794B41D7,
	RelativeBoundingBox_tC31BDE3CCCD2009BC2562071BDB4B5DB13F6752E_CustomAttributesCacheGenerator_RelativeBoundingBox_Equals_m4A16727F878D1A1CF29E042D8088470E058DD7DB,
	RelativeBoundingBox_tC31BDE3CCCD2009BC2562071BDB4B5DB13F6752E_CustomAttributesCacheGenerator_RelativeBoundingBox_Equals_m45535830904520641BA7D02C53DCBE8A548359B4,
	RelativeBoundingBox_tC31BDE3CCCD2009BC2562071BDB4B5DB13F6752E_CustomAttributesCacheGenerator_RelativeBoundingBox_GetHashCode_m6E36E0BA9CABBBBB75860C51EB1C253702E47E62,
	RelativeBoundingBox_tC31BDE3CCCD2009BC2562071BDB4B5DB13F6752E_CustomAttributesCacheGenerator_RelativeBoundingBox_ToString_mD096BEBCC911A0831D49B8ADB2393668CB1E07AA,
	RelativeBoundingBox_tC31BDE3CCCD2009BC2562071BDB4B5DB13F6752E_CustomAttributesCacheGenerator_RelativeBoundingBox_WriteTo_mB80D4F45F77E83F7B6A170F6A3BC48001C0FACF0,
	RelativeBoundingBox_tC31BDE3CCCD2009BC2562071BDB4B5DB13F6752E_CustomAttributesCacheGenerator_RelativeBoundingBox_CalculateSize_mCE3CB30EDFEAE4B677F7EF03C94E74FD0F39566E,
	RelativeBoundingBox_tC31BDE3CCCD2009BC2562071BDB4B5DB13F6752E_CustomAttributesCacheGenerator_RelativeBoundingBox_MergeFrom_m0E9CE082728E5F9669448DDC651D3B424F8E444F,
	RelativeBoundingBox_tC31BDE3CCCD2009BC2562071BDB4B5DB13F6752E_CustomAttributesCacheGenerator_RelativeBoundingBox_MergeFrom_m47BCC3409807A67E5FAEBF440FDD9155DFE0DA20,
	BinaryMask_t0F9C5C2054ED5487FD9D2297F6800F66AC695DBE_CustomAttributesCacheGenerator_BinaryMask__ctor_m8F90D268C87AA0A2998B869A679E486B6784B069,
	BinaryMask_t0F9C5C2054ED5487FD9D2297F6800F66AC695DBE_CustomAttributesCacheGenerator_BinaryMask__ctor_m03BA73D9ACA551B4101934F7A24C4AFB1061B712,
	BinaryMask_t0F9C5C2054ED5487FD9D2297F6800F66AC695DBE_CustomAttributesCacheGenerator_BinaryMask_Clone_m686B51B7DC6D0BAB62F6C639B6ADE99AC8A9AA99,
	BinaryMask_t0F9C5C2054ED5487FD9D2297F6800F66AC695DBE_CustomAttributesCacheGenerator_BinaryMask_ClearWidth_m7C972B069D508D1FE0EA7991A4C5362201988D9D,
	BinaryMask_t0F9C5C2054ED5487FD9D2297F6800F66AC695DBE_CustomAttributesCacheGenerator_BinaryMask_ClearHeight_m0CB9F998280EEB1A27CA8EF7AD9776ECB07A5DD6,
	BinaryMask_t0F9C5C2054ED5487FD9D2297F6800F66AC695DBE_CustomAttributesCacheGenerator_BinaryMask_ClearRasterization_m652D34C3A5C6168C317903658D48ED049EF71B9A,
	BinaryMask_t0F9C5C2054ED5487FD9D2297F6800F66AC695DBE_CustomAttributesCacheGenerator_BinaryMask_Equals_mBF78A6B0C1E5516AD385E4C4D22851C5BA57A5F1,
	BinaryMask_t0F9C5C2054ED5487FD9D2297F6800F66AC695DBE_CustomAttributesCacheGenerator_BinaryMask_Equals_m49D94332523281DBAC6789DA4D6C2179E28EF83A,
	BinaryMask_t0F9C5C2054ED5487FD9D2297F6800F66AC695DBE_CustomAttributesCacheGenerator_BinaryMask_GetHashCode_m9E4FFB7CE411C64CAD98DBA291AF0E05C61A8FE5,
	BinaryMask_t0F9C5C2054ED5487FD9D2297F6800F66AC695DBE_CustomAttributesCacheGenerator_BinaryMask_ToString_mF795F2557EF8ED863BBD2F1C459778A61115700B,
	BinaryMask_t0F9C5C2054ED5487FD9D2297F6800F66AC695DBE_CustomAttributesCacheGenerator_BinaryMask_WriteTo_mA261BA797008FF73839EBA7E0592F0DEBD415564,
	BinaryMask_t0F9C5C2054ED5487FD9D2297F6800F66AC695DBE_CustomAttributesCacheGenerator_BinaryMask_CalculateSize_mC22E19FAE984002D7163988734767B26FC5823D1,
	BinaryMask_t0F9C5C2054ED5487FD9D2297F6800F66AC695DBE_CustomAttributesCacheGenerator_BinaryMask_MergeFrom_m6BE953E5A769220962214F9F05E0FE319C1B652B,
	BinaryMask_t0F9C5C2054ED5487FD9D2297F6800F66AC695DBE_CustomAttributesCacheGenerator_BinaryMask_MergeFrom_m5DA0857C4C0936C91F2EB06AE3F299518918D776,
	RelativeKeypoint_tB0867DC1F9743FC97A924CB29A42ADB6A8E7DEA7_CustomAttributesCacheGenerator_RelativeKeypoint__ctor_m206C6B4CC8BFC83247B3CF0E9B6C1DB3EF9FD239,
	RelativeKeypoint_tB0867DC1F9743FC97A924CB29A42ADB6A8E7DEA7_CustomAttributesCacheGenerator_RelativeKeypoint__ctor_mEAE05077643B9B02D325976BFEA6673DEB47ACB9,
	RelativeKeypoint_tB0867DC1F9743FC97A924CB29A42ADB6A8E7DEA7_CustomAttributesCacheGenerator_RelativeKeypoint_Clone_mB4A646D4224167F8867687AA163FFDB1772B344B,
	RelativeKeypoint_tB0867DC1F9743FC97A924CB29A42ADB6A8E7DEA7_CustomAttributesCacheGenerator_RelativeKeypoint_ClearX_mA536C8F3890EAC060547009BFB0E7159C702549E,
	RelativeKeypoint_tB0867DC1F9743FC97A924CB29A42ADB6A8E7DEA7_CustomAttributesCacheGenerator_RelativeKeypoint_ClearY_mBDEF3488FCD528959DEA9E1D490013226E8C3ADC,
	RelativeKeypoint_tB0867DC1F9743FC97A924CB29A42ADB6A8E7DEA7_CustomAttributesCacheGenerator_RelativeKeypoint_ClearKeypointLabel_m3706CBCDA0D55D054051AFFF760EF0590F75698F,
	RelativeKeypoint_tB0867DC1F9743FC97A924CB29A42ADB6A8E7DEA7_CustomAttributesCacheGenerator_RelativeKeypoint_ClearScore_m05253CED8244B4BEE218FD9F8F1299A536A68F34,
	RelativeKeypoint_tB0867DC1F9743FC97A924CB29A42ADB6A8E7DEA7_CustomAttributesCacheGenerator_RelativeKeypoint_Equals_m3F57F2FDEDA1A3E18FB46700957235704010CA1C,
	RelativeKeypoint_tB0867DC1F9743FC97A924CB29A42ADB6A8E7DEA7_CustomAttributesCacheGenerator_RelativeKeypoint_Equals_m704069F17BBCA172946EC75767DE0558DE5796C5,
	RelativeKeypoint_tB0867DC1F9743FC97A924CB29A42ADB6A8E7DEA7_CustomAttributesCacheGenerator_RelativeKeypoint_GetHashCode_m52ACCE197F999EFC3DC86317A382DD805532C5B9,
	RelativeKeypoint_tB0867DC1F9743FC97A924CB29A42ADB6A8E7DEA7_CustomAttributesCacheGenerator_RelativeKeypoint_ToString_mF649593D1A01012190C83FCAC793FE30E0A2ADB7,
	RelativeKeypoint_tB0867DC1F9743FC97A924CB29A42ADB6A8E7DEA7_CustomAttributesCacheGenerator_RelativeKeypoint_WriteTo_mBF98F261D95B71894CAA982839EC4F8A6D99ABC1,
	RelativeKeypoint_tB0867DC1F9743FC97A924CB29A42ADB6A8E7DEA7_CustomAttributesCacheGenerator_RelativeKeypoint_CalculateSize_m5B1B5C66064B9BF331664023DBEEBAC75DFD9169,
	RelativeKeypoint_tB0867DC1F9743FC97A924CB29A42ADB6A8E7DEA7_CustomAttributesCacheGenerator_RelativeKeypoint_MergeFrom_m198362F71ABDF0359BA9874F2E7AF59F52B14C30,
	RelativeKeypoint_tB0867DC1F9743FC97A924CB29A42ADB6A8E7DEA7_CustomAttributesCacheGenerator_RelativeKeypoint_MergeFrom_mE832CEDC0F6D893DA89014206624FDC0F0009AA9,
	MatrixData_tA28091B9C8107B5FED6284DDF6D1AEFE2969E0C4_CustomAttributesCacheGenerator_MatrixData__ctor_m08760B404A31B2AE0141BB152C5765B20FE3D999,
	MatrixData_tA28091B9C8107B5FED6284DDF6D1AEFE2969E0C4_CustomAttributesCacheGenerator_MatrixData__ctor_m190F97D56239F0C19F454C92501EABC70460EF0D,
	MatrixData_tA28091B9C8107B5FED6284DDF6D1AEFE2969E0C4_CustomAttributesCacheGenerator_MatrixData_Clone_mCE53C6458C28502B3440957A054F15E1555A2906,
	MatrixData_tA28091B9C8107B5FED6284DDF6D1AEFE2969E0C4_CustomAttributesCacheGenerator_MatrixData_ClearRows_mE0E54CD921FE4772B8F80BBDB6B3026AA9077887,
	MatrixData_tA28091B9C8107B5FED6284DDF6D1AEFE2969E0C4_CustomAttributesCacheGenerator_MatrixData_ClearCols_m05C85B672E3F314D316EAFF6C74CFC81278869DF,
	MatrixData_tA28091B9C8107B5FED6284DDF6D1AEFE2969E0C4_CustomAttributesCacheGenerator_MatrixData_ClearLayout_m2140EFC522A01F79894555FBD578D33E12C684B1,
	MatrixData_tA28091B9C8107B5FED6284DDF6D1AEFE2969E0C4_CustomAttributesCacheGenerator_MatrixData_Equals_mAB317070368E9C8FAC655E625805350B1FB0C314,
	MatrixData_tA28091B9C8107B5FED6284DDF6D1AEFE2969E0C4_CustomAttributesCacheGenerator_MatrixData_Equals_mFF2B5393EA1A1F13B59FE28679F4FD1946BA8D42,
	MatrixData_tA28091B9C8107B5FED6284DDF6D1AEFE2969E0C4_CustomAttributesCacheGenerator_MatrixData_GetHashCode_m43DDD0B77ECB6989C34D865D720E8E4F60AB8113,
	MatrixData_tA28091B9C8107B5FED6284DDF6D1AEFE2969E0C4_CustomAttributesCacheGenerator_MatrixData_ToString_m14D4258EAB83BC97E759D73DAD0B8BBB2B78DEB3,
	MatrixData_tA28091B9C8107B5FED6284DDF6D1AEFE2969E0C4_CustomAttributesCacheGenerator_MatrixData_WriteTo_m3B87386A2E13C7A8B682CCA7020DF01A57860798,
	MatrixData_tA28091B9C8107B5FED6284DDF6D1AEFE2969E0C4_CustomAttributesCacheGenerator_MatrixData_CalculateSize_mB7488B8687D6F10921E34FCB8CCBA3C565BE5CD5,
	MatrixData_tA28091B9C8107B5FED6284DDF6D1AEFE2969E0C4_CustomAttributesCacheGenerator_MatrixData_MergeFrom_m0AB33B94558F79353F9AE8E471A759FA65E828FB,
	MatrixData_tA28091B9C8107B5FED6284DDF6D1AEFE2969E0C4_CustomAttributesCacheGenerator_MatrixData_MergeFrom_m547674A1C5CD7A86BF22973FA6FDF51A6DD056F5,
	MediaPipeOptions_t9D271404AA6309644B1A0AEDF6116DD119F4D4D8_CustomAttributesCacheGenerator_MediaPipeOptions__ctor_m94ECAD8AA33546C341D7764097D40351B2E4AB54,
	MediaPipeOptions_t9D271404AA6309644B1A0AEDF6116DD119F4D4D8_CustomAttributesCacheGenerator_MediaPipeOptions__ctor_m54624D466F21E59CB280A7DB9FA5BA1B99ED1C1C,
	MediaPipeOptions_t9D271404AA6309644B1A0AEDF6116DD119F4D4D8_CustomAttributesCacheGenerator_MediaPipeOptions_Clone_m4BDE025E942E472425387DEF8F63146B8C2B6EC4,
	MediaPipeOptions_t9D271404AA6309644B1A0AEDF6116DD119F4D4D8_CustomAttributesCacheGenerator_MediaPipeOptions_Equals_m5A11BC1C09E74CC69DB83298DBA5003DD8A65366,
	MediaPipeOptions_t9D271404AA6309644B1A0AEDF6116DD119F4D4D8_CustomAttributesCacheGenerator_MediaPipeOptions_Equals_mED7E91CF2EDBAFBCE5E1C2BA2374F0A4E5F48006,
	MediaPipeOptions_t9D271404AA6309644B1A0AEDF6116DD119F4D4D8_CustomAttributesCacheGenerator_MediaPipeOptions_GetHashCode_m6C05D27EF2C543C70EC43C7DB712D028BDBE654E,
	MediaPipeOptions_t9D271404AA6309644B1A0AEDF6116DD119F4D4D8_CustomAttributesCacheGenerator_MediaPipeOptions_ToString_m46611F2B164EAE51886F328CCC4213104A41C208,
	MediaPipeOptions_t9D271404AA6309644B1A0AEDF6116DD119F4D4D8_CustomAttributesCacheGenerator_MediaPipeOptions_WriteTo_mB5CF6A852AD743614BD0B5CC247564DF783C1AC0,
	MediaPipeOptions_t9D271404AA6309644B1A0AEDF6116DD119F4D4D8_CustomAttributesCacheGenerator_MediaPipeOptions_CalculateSize_m161CBE1D9EF8D1A82B6D3EFADF0533D1541F34D2,
	MediaPipeOptions_t9D271404AA6309644B1A0AEDF6116DD119F4D4D8_CustomAttributesCacheGenerator_MediaPipeOptions_MergeFrom_m169687073AE0664B8B7070D1578ACB9A006A28FC,
	MediaPipeOptions_t9D271404AA6309644B1A0AEDF6116DD119F4D4D8_CustomAttributesCacheGenerator_MediaPipeOptions_MergeFrom_m0F16A641B688B2AACE098CE2DD7ED6AF2A33B500,
	TimedModelMatrixProto_t1FEBAF26F6D2794B20DF340436C746FB747382B0_CustomAttributesCacheGenerator_TimedModelMatrixProto__ctor_mD36B8BF1A3255A20D213DB8179CE15F8B90FF922,
	TimedModelMatrixProto_t1FEBAF26F6D2794B20DF340436C746FB747382B0_CustomAttributesCacheGenerator_TimedModelMatrixProto__ctor_m4DFAF5A291060B008DC81DDEB764F5FFC7501617,
	TimedModelMatrixProto_t1FEBAF26F6D2794B20DF340436C746FB747382B0_CustomAttributesCacheGenerator_TimedModelMatrixProto_Clone_m000B8BFEEAA7E9C9B5F816E65A2E524BC67AAE48,
	TimedModelMatrixProto_t1FEBAF26F6D2794B20DF340436C746FB747382B0_CustomAttributesCacheGenerator_TimedModelMatrixProto_ClearTimeMsec_mD9625E2C729D54656E3F68610553639CADF865B0,
	TimedModelMatrixProto_t1FEBAF26F6D2794B20DF340436C746FB747382B0_CustomAttributesCacheGenerator_TimedModelMatrixProto_ClearId_m2380F94A5FB0693F86B1C7856B0ED842B1C29156,
	TimedModelMatrixProto_t1FEBAF26F6D2794B20DF340436C746FB747382B0_CustomAttributesCacheGenerator_TimedModelMatrixProto_Equals_m10573AEDA812F98D2A73FD3D99A8C7FBF788F49D,
	TimedModelMatrixProto_t1FEBAF26F6D2794B20DF340436C746FB747382B0_CustomAttributesCacheGenerator_TimedModelMatrixProto_Equals_m8EAAFDB34A86DB247D89A253C9B5A9F969D6AF72,
	TimedModelMatrixProto_t1FEBAF26F6D2794B20DF340436C746FB747382B0_CustomAttributesCacheGenerator_TimedModelMatrixProto_GetHashCode_m97FD870375066873755CDB0CD8C667D72EF2E26B,
	TimedModelMatrixProto_t1FEBAF26F6D2794B20DF340436C746FB747382B0_CustomAttributesCacheGenerator_TimedModelMatrixProto_ToString_mF73FC8377B34FB364A3471DF34952AEDE0CCFAFB,
	TimedModelMatrixProto_t1FEBAF26F6D2794B20DF340436C746FB747382B0_CustomAttributesCacheGenerator_TimedModelMatrixProto_WriteTo_mE3FC56B10FDEA6B63530855BADD6886B006346AC,
	TimedModelMatrixProto_t1FEBAF26F6D2794B20DF340436C746FB747382B0_CustomAttributesCacheGenerator_TimedModelMatrixProto_CalculateSize_mD7343FF3B98B11A38CB8AD302EBFE05EE5E5B118,
	TimedModelMatrixProto_t1FEBAF26F6D2794B20DF340436C746FB747382B0_CustomAttributesCacheGenerator_TimedModelMatrixProto_MergeFrom_m19C51BFF2F65BC61EC38D1EAA2E1C6212A57874E,
	TimedModelMatrixProto_t1FEBAF26F6D2794B20DF340436C746FB747382B0_CustomAttributesCacheGenerator_TimedModelMatrixProto_MergeFrom_m6CFBC87CE67964F667F449E67BE3488D319F373C,
	TimedModelMatrixProtoList_t3A75AC882CCC8C5C7CE330E62547C87220B1A7A7_CustomAttributesCacheGenerator_TimedModelMatrixProtoList__ctor_mAFE5CBDBDF8ABDE5ED930F230AE053920BA6E6E3,
	TimedModelMatrixProtoList_t3A75AC882CCC8C5C7CE330E62547C87220B1A7A7_CustomAttributesCacheGenerator_TimedModelMatrixProtoList__ctor_mFEE41524C860DD57FF001AC92B9BAAA89B731A2A,
	TimedModelMatrixProtoList_t3A75AC882CCC8C5C7CE330E62547C87220B1A7A7_CustomAttributesCacheGenerator_TimedModelMatrixProtoList_Clone_m78CD67CD3A408D9D83041DE3F507EE53552BAA05,
	TimedModelMatrixProtoList_t3A75AC882CCC8C5C7CE330E62547C87220B1A7A7_CustomAttributesCacheGenerator_TimedModelMatrixProtoList_Equals_mE02E4E54D0605023A5A9B0D54412D1CCBBAC674B,
	TimedModelMatrixProtoList_t3A75AC882CCC8C5C7CE330E62547C87220B1A7A7_CustomAttributesCacheGenerator_TimedModelMatrixProtoList_Equals_m4B7E6B649A3541ACE89D9FF48E789EC4EF7E9871,
	TimedModelMatrixProtoList_t3A75AC882CCC8C5C7CE330E62547C87220B1A7A7_CustomAttributesCacheGenerator_TimedModelMatrixProtoList_GetHashCode_m39E976CEB249D20888704E62BE2A62F7AF48717B,
	TimedModelMatrixProtoList_t3A75AC882CCC8C5C7CE330E62547C87220B1A7A7_CustomAttributesCacheGenerator_TimedModelMatrixProtoList_ToString_m8A8860A75EBC20B5EECB9BD5D546DD26FA31AF23,
	TimedModelMatrixProtoList_t3A75AC882CCC8C5C7CE330E62547C87220B1A7A7_CustomAttributesCacheGenerator_TimedModelMatrixProtoList_WriteTo_mF9E2D1D5B4B98D4ECCD1CCD3B66353DC9FC0C4A7,
	TimedModelMatrixProtoList_t3A75AC882CCC8C5C7CE330E62547C87220B1A7A7_CustomAttributesCacheGenerator_TimedModelMatrixProtoList_CalculateSize_mDFA55794734AFC296355D7F42C8E1DEF5DA7F5FB,
	TimedModelMatrixProtoList_t3A75AC882CCC8C5C7CE330E62547C87220B1A7A7_CustomAttributesCacheGenerator_TimedModelMatrixProtoList_MergeFrom_m92F8B84288012603724555016046AEB0B943A1A5,
	TimedModelMatrixProtoList_t3A75AC882CCC8C5C7CE330E62547C87220B1A7A7_CustomAttributesCacheGenerator_TimedModelMatrixProtoList_MergeFrom_m4DF276D53054577C044EB5E3C85EC49FA2A4A248,
	TimedVectorProto_t922BD38BFDF681636210E2476928157FE2C69DE7_CustomAttributesCacheGenerator_TimedVectorProto__ctor_m94A7FB7ED78DA1A40E402F3BB017C3E5AE052F23,
	TimedVectorProto_t922BD38BFDF681636210E2476928157FE2C69DE7_CustomAttributesCacheGenerator_TimedVectorProto__ctor_m7A2E61E4FE4C04BFDB2103BC80DA8857BE16A6AF,
	TimedVectorProto_t922BD38BFDF681636210E2476928157FE2C69DE7_CustomAttributesCacheGenerator_TimedVectorProto_Clone_mAA2BCF6CF36B58DD176F497A2D68C2EC34915E49,
	TimedVectorProto_t922BD38BFDF681636210E2476928157FE2C69DE7_CustomAttributesCacheGenerator_TimedVectorProto_ClearTimeMsec_m1B1C0E25E3372C399F5D14B10F2638788BC4F348,
	TimedVectorProto_t922BD38BFDF681636210E2476928157FE2C69DE7_CustomAttributesCacheGenerator_TimedVectorProto_ClearId_m40D90F7DE18B8A1340531B1C587E9FA67FBA3014,
	TimedVectorProto_t922BD38BFDF681636210E2476928157FE2C69DE7_CustomAttributesCacheGenerator_TimedVectorProto_Equals_m32738DC0662B6DD5CF4D2E5E97F07CD4D8137803,
	TimedVectorProto_t922BD38BFDF681636210E2476928157FE2C69DE7_CustomAttributesCacheGenerator_TimedVectorProto_Equals_m699DE31F04C68A804A9C34511BD3F83CC63BF990,
	TimedVectorProto_t922BD38BFDF681636210E2476928157FE2C69DE7_CustomAttributesCacheGenerator_TimedVectorProto_GetHashCode_mB2A72327CB3A9C28014E0DCCCCD9916B8184296C,
	TimedVectorProto_t922BD38BFDF681636210E2476928157FE2C69DE7_CustomAttributesCacheGenerator_TimedVectorProto_ToString_mD6CC9AC2C547209D3590CC7ED638AFAD4D4E1785,
	TimedVectorProto_t922BD38BFDF681636210E2476928157FE2C69DE7_CustomAttributesCacheGenerator_TimedVectorProto_WriteTo_mA00DD38C663E0821F0CF37CA65F2619F7B15396E,
	TimedVectorProto_t922BD38BFDF681636210E2476928157FE2C69DE7_CustomAttributesCacheGenerator_TimedVectorProto_CalculateSize_mCA0C76E99A476908DA0E9C2FA1CD7E00EE1314D5,
	TimedVectorProto_t922BD38BFDF681636210E2476928157FE2C69DE7_CustomAttributesCacheGenerator_TimedVectorProto_MergeFrom_m210826B18001527082F4BD2726C8E7B1755A5387,
	TimedVectorProto_t922BD38BFDF681636210E2476928157FE2C69DE7_CustomAttributesCacheGenerator_TimedVectorProto_MergeFrom_m3497545D1E9A10B36DBE67104C293FB1B37FAB18,
	TimedVectorProtoList_t628C533965DB55ADC9A8993A87569E9EA7B6033F_CustomAttributesCacheGenerator_TimedVectorProtoList__ctor_m7AFAE896482EDF3711A6DA99A412D64487EF2F91,
	TimedVectorProtoList_t628C533965DB55ADC9A8993A87569E9EA7B6033F_CustomAttributesCacheGenerator_TimedVectorProtoList__ctor_mBB786126903E375611CE37FB8B7ADB4030EA0C1E,
	TimedVectorProtoList_t628C533965DB55ADC9A8993A87569E9EA7B6033F_CustomAttributesCacheGenerator_TimedVectorProtoList_Clone_mA692C92F4F46F28CF480F28E95932FEE2130FDA8,
	TimedVectorProtoList_t628C533965DB55ADC9A8993A87569E9EA7B6033F_CustomAttributesCacheGenerator_TimedVectorProtoList_Equals_m70165213BF6F474B1105E05389301339CF9CA6ED,
	TimedVectorProtoList_t628C533965DB55ADC9A8993A87569E9EA7B6033F_CustomAttributesCacheGenerator_TimedVectorProtoList_Equals_m090C5D733BA5CA3D26DCFC59F13DD2C413025B4A,
	TimedVectorProtoList_t628C533965DB55ADC9A8993A87569E9EA7B6033F_CustomAttributesCacheGenerator_TimedVectorProtoList_GetHashCode_mBE5776B4DEF84AD23EAE943A16440C264D819347,
	TimedVectorProtoList_t628C533965DB55ADC9A8993A87569E9EA7B6033F_CustomAttributesCacheGenerator_TimedVectorProtoList_ToString_m89D6D023EF065EB7214BD2F6C4A5B793AEC47BBA,
	TimedVectorProtoList_t628C533965DB55ADC9A8993A87569E9EA7B6033F_CustomAttributesCacheGenerator_TimedVectorProtoList_WriteTo_m7883C3162842BB6B2D11C97A03E61ED8B0625074,
	TimedVectorProtoList_t628C533965DB55ADC9A8993A87569E9EA7B6033F_CustomAttributesCacheGenerator_TimedVectorProtoList_CalculateSize_mC4270F8CBA1A98987570383F71806D85E13AC42F,
	TimedVectorProtoList_t628C533965DB55ADC9A8993A87569E9EA7B6033F_CustomAttributesCacheGenerator_TimedVectorProtoList_MergeFrom_m1C123548C738E2DEE42C6BDF207533DD92EE55D5,
	TimedVectorProtoList_t628C533965DB55ADC9A8993A87569E9EA7B6033F_CustomAttributesCacheGenerator_TimedVectorProtoList_MergeFrom_m5F9AFAA0BE3F80732ED106238D19067945C8E53A,
	PacketFactoryOptions_tEA685DCB184FF4693C4C66A20AA649B6D0214850_CustomAttributesCacheGenerator_PacketFactoryOptions__ctor_mC8536EB222928EED726DC5B86DDECD96B930DAB0,
	PacketFactoryOptions_tEA685DCB184FF4693C4C66A20AA649B6D0214850_CustomAttributesCacheGenerator_PacketFactoryOptions__ctor_m2D36055064EC0015518ED210217D3E3E04CB050F,
	PacketFactoryOptions_tEA685DCB184FF4693C4C66A20AA649B6D0214850_CustomAttributesCacheGenerator_PacketFactoryOptions_Clone_m81FDE11474A9B170510320D3F0F5D581E367B408,
	PacketFactoryOptions_tEA685DCB184FF4693C4C66A20AA649B6D0214850_CustomAttributesCacheGenerator_PacketFactoryOptions_Equals_mE75139A5773D01442C993D3D8C76EF65159320E9,
	PacketFactoryOptions_tEA685DCB184FF4693C4C66A20AA649B6D0214850_CustomAttributesCacheGenerator_PacketFactoryOptions_Equals_mB3693914B1867B7CB7DE1969EA290CEA81D43E79,
	PacketFactoryOptions_tEA685DCB184FF4693C4C66A20AA649B6D0214850_CustomAttributesCacheGenerator_PacketFactoryOptions_GetHashCode_m21D7D747BBA561125D4372391A037A706D72734E,
	PacketFactoryOptions_tEA685DCB184FF4693C4C66A20AA649B6D0214850_CustomAttributesCacheGenerator_PacketFactoryOptions_ToString_m55FF16DB4F9E721D5593642FE08504579D0F4D53,
	PacketFactoryOptions_tEA685DCB184FF4693C4C66A20AA649B6D0214850_CustomAttributesCacheGenerator_PacketFactoryOptions_WriteTo_m9D660E8331CCC27AAE98146FD46C62C3E702FECD,
	PacketFactoryOptions_tEA685DCB184FF4693C4C66A20AA649B6D0214850_CustomAttributesCacheGenerator_PacketFactoryOptions_CalculateSize_m269B700BA480E8E38BF52DBF256336CF709EBD72,
	PacketFactoryOptions_tEA685DCB184FF4693C4C66A20AA649B6D0214850_CustomAttributesCacheGenerator_PacketFactoryOptions_MergeFrom_m0D0FDA7834EBF6390A437209C82B5E59A32E1843,
	PacketFactoryOptions_tEA685DCB184FF4693C4C66A20AA649B6D0214850_CustomAttributesCacheGenerator_PacketFactoryOptions_MergeFrom_m6AB31546F93E43834CD182777A573B5FD0F0B284,
	PacketFactoryConfig_t5BA5CF02AEB213652B28644DDE0D5D5A21703E12_CustomAttributesCacheGenerator_PacketFactoryConfig__ctor_mB19EA1662F94AA4F76B3E366F68C4BEF5DE34E8A,
	PacketFactoryConfig_t5BA5CF02AEB213652B28644DDE0D5D5A21703E12_CustomAttributesCacheGenerator_PacketFactoryConfig__ctor_m16A584150178AEF55F2E4F659D408B01ABACB481,
	PacketFactoryConfig_t5BA5CF02AEB213652B28644DDE0D5D5A21703E12_CustomAttributesCacheGenerator_PacketFactoryConfig_Clone_m27D51A4834DC6F5343FF317372508AC2FA7B08EF,
	PacketFactoryConfig_t5BA5CF02AEB213652B28644DDE0D5D5A21703E12_CustomAttributesCacheGenerator_PacketFactoryConfig_ClearPacketFactory_m82423E2A65876B507F2260501E7B5FE4871B5FD4,
	PacketFactoryConfig_t5BA5CF02AEB213652B28644DDE0D5D5A21703E12_CustomAttributesCacheGenerator_PacketFactoryConfig_ClearOutputSidePacket_mF630C0F005F394FA52F5FEEAC784C45B2A9F400D,
	PacketFactoryConfig_t5BA5CF02AEB213652B28644DDE0D5D5A21703E12_CustomAttributesCacheGenerator_PacketFactoryConfig_ClearExternalOutput_mDFEABBD4FE8E94CE783946760A19F73B23B7099D,
	PacketFactoryConfig_t5BA5CF02AEB213652B28644DDE0D5D5A21703E12_CustomAttributesCacheGenerator_PacketFactoryConfig_ClearOptions_mFF0B8AD05101A7B34DEB196164800C630203DF72,
	PacketFactoryConfig_t5BA5CF02AEB213652B28644DDE0D5D5A21703E12_CustomAttributesCacheGenerator_PacketFactoryConfig_Equals_m2F80DE3C163CE19D35574287766C1E85FEBA44A0,
	PacketFactoryConfig_t5BA5CF02AEB213652B28644DDE0D5D5A21703E12_CustomAttributesCacheGenerator_PacketFactoryConfig_Equals_m0FEDD1353DBB452259CACE6FFF69AAC41D5EDC3D,
	PacketFactoryConfig_t5BA5CF02AEB213652B28644DDE0D5D5A21703E12_CustomAttributesCacheGenerator_PacketFactoryConfig_GetHashCode_m037E8FB9D77E8B663BB9490F53F7746B5954FD01,
	PacketFactoryConfig_t5BA5CF02AEB213652B28644DDE0D5D5A21703E12_CustomAttributesCacheGenerator_PacketFactoryConfig_ToString_m081F5FBD927673C09D7F63168C6A45C50CBBF37C,
	PacketFactoryConfig_t5BA5CF02AEB213652B28644DDE0D5D5A21703E12_CustomAttributesCacheGenerator_PacketFactoryConfig_WriteTo_m182894E780B31E210F4E177DAE8F2438CEC90D56,
	PacketFactoryConfig_t5BA5CF02AEB213652B28644DDE0D5D5A21703E12_CustomAttributesCacheGenerator_PacketFactoryConfig_CalculateSize_m989B4B743104D755C0E33BBFD98C7F2B734AC4D5,
	PacketFactoryConfig_t5BA5CF02AEB213652B28644DDE0D5D5A21703E12_CustomAttributesCacheGenerator_PacketFactoryConfig_MergeFrom_mE285F8F83524EF28E8C0F1B20CCC431D5E9D61A8,
	PacketFactoryConfig_t5BA5CF02AEB213652B28644DDE0D5D5A21703E12_CustomAttributesCacheGenerator_PacketFactoryConfig_MergeFrom_mABEF4433CD66253E3B53FE07C3937BC2E9A8513D,
	PacketManagerConfig_t324C1B93AB9471F4658F17482A26B89C0BFAB519_CustomAttributesCacheGenerator_PacketManagerConfig__ctor_m882F227FF8DBA0BF8E8743CE05BDC22CE76E7644,
	PacketManagerConfig_t324C1B93AB9471F4658F17482A26B89C0BFAB519_CustomAttributesCacheGenerator_PacketManagerConfig__ctor_m9878FDFCDCEC6AF84637F38F1CBEA9DD5F5EE570,
	PacketManagerConfig_t324C1B93AB9471F4658F17482A26B89C0BFAB519_CustomAttributesCacheGenerator_PacketManagerConfig_Clone_m4E2F5485BEF8CE62F49391651D7448EDC63F3093,
	PacketManagerConfig_t324C1B93AB9471F4658F17482A26B89C0BFAB519_CustomAttributesCacheGenerator_PacketManagerConfig_Equals_m11D2FCCE2BC53BE6796B761C6DF983D05199371B,
	PacketManagerConfig_t324C1B93AB9471F4658F17482A26B89C0BFAB519_CustomAttributesCacheGenerator_PacketManagerConfig_Equals_m448C26325B5F0F63AD1511314AD36AE88FF75B53,
	PacketManagerConfig_t324C1B93AB9471F4658F17482A26B89C0BFAB519_CustomAttributesCacheGenerator_PacketManagerConfig_GetHashCode_mF97E5240FC7AC7599CF9878408E09169FD99F992,
	PacketManagerConfig_t324C1B93AB9471F4658F17482A26B89C0BFAB519_CustomAttributesCacheGenerator_PacketManagerConfig_ToString_mB91FC8BD3FD4FA215BB48D84E60A40ECD8FE301A,
	PacketManagerConfig_t324C1B93AB9471F4658F17482A26B89C0BFAB519_CustomAttributesCacheGenerator_PacketManagerConfig_WriteTo_mC1BB483BA2A79844AAC30C9902ADD2178C765B9A,
	PacketManagerConfig_t324C1B93AB9471F4658F17482A26B89C0BFAB519_CustomAttributesCacheGenerator_PacketManagerConfig_CalculateSize_m9B17003F4E07D2FBA15072AA9FA611E9808C9CEE,
	PacketManagerConfig_t324C1B93AB9471F4658F17482A26B89C0BFAB519_CustomAttributesCacheGenerator_PacketManagerConfig_MergeFrom_mFCFF46E5C7B7FF00042DB64CA4117549B4DA6379,
	PacketManagerConfig_t324C1B93AB9471F4658F17482A26B89C0BFAB519_CustomAttributesCacheGenerator_PacketManagerConfig_MergeFrom_m47C7CA9A0C39ABF84078F6AE1383CD0D40816174,
	PacketGeneratorOptions_t103747F5D200028AFE532E63B3E64BE05BF8523A_CustomAttributesCacheGenerator_PacketGeneratorOptions__ctor_mC663EB5FC659CA75449E64A002B4CC10C15360AF,
	PacketGeneratorOptions_t103747F5D200028AFE532E63B3E64BE05BF8523A_CustomAttributesCacheGenerator_PacketGeneratorOptions__ctor_m74543604138A39F4D112E514CE0C9DD4B53BCF39,
	PacketGeneratorOptions_t103747F5D200028AFE532E63B3E64BE05BF8523A_CustomAttributesCacheGenerator_PacketGeneratorOptions_Clone_m058684255081A07E04B4D5EBB30D264D95C2C2BB,
	PacketGeneratorOptions_t103747F5D200028AFE532E63B3E64BE05BF8523A_CustomAttributesCacheGenerator_PacketGeneratorOptions_Equals_m7695E0C7DB1C89DF8F186CB8265472DA3469D9F7,
	PacketGeneratorOptions_t103747F5D200028AFE532E63B3E64BE05BF8523A_CustomAttributesCacheGenerator_PacketGeneratorOptions_Equals_mB3A3471F6EE5C8A3374CBE6DDAD98102A07FBF37,
	PacketGeneratorOptions_t103747F5D200028AFE532E63B3E64BE05BF8523A_CustomAttributesCacheGenerator_PacketGeneratorOptions_GetHashCode_m23D64E3A72F45C6FF8E682520689880DDC187C9F,
	PacketGeneratorOptions_t103747F5D200028AFE532E63B3E64BE05BF8523A_CustomAttributesCacheGenerator_PacketGeneratorOptions_ToString_mE964D3F9F6EC1FEA51DD82143A1F65695F253A18,
	PacketGeneratorOptions_t103747F5D200028AFE532E63B3E64BE05BF8523A_CustomAttributesCacheGenerator_PacketGeneratorOptions_WriteTo_mA2C4C9ABD31A321D10AFA8D9841EEAAA230FE944,
	PacketGeneratorOptions_t103747F5D200028AFE532E63B3E64BE05BF8523A_CustomAttributesCacheGenerator_PacketGeneratorOptions_CalculateSize_m52382B6749DB349EF633CC5ECFC63AF2A00C82CE,
	PacketGeneratorOptions_t103747F5D200028AFE532E63B3E64BE05BF8523A_CustomAttributesCacheGenerator_PacketGeneratorOptions_MergeFrom_m554F607119A2A19278375B13E27CD2AF9B9D9AC5,
	PacketGeneratorOptions_t103747F5D200028AFE532E63B3E64BE05BF8523A_CustomAttributesCacheGenerator_PacketGeneratorOptions_MergeFrom_m0CADE6A187BDA996B2480109B50A87D215468C2C,
	PacketGeneratorConfig_tB75C9DFBB65CAE892E81E36B9BEFB10CBC226A4F_CustomAttributesCacheGenerator_PacketGeneratorConfig__ctor_m3DD06F90644406E25B61D2CD6105787D7A59DC2E,
	PacketGeneratorConfig_tB75C9DFBB65CAE892E81E36B9BEFB10CBC226A4F_CustomAttributesCacheGenerator_PacketGeneratorConfig__ctor_m6380447A5817E1DB9BA8031BC40A897E3EC7FADB,
	PacketGeneratorConfig_tB75C9DFBB65CAE892E81E36B9BEFB10CBC226A4F_CustomAttributesCacheGenerator_PacketGeneratorConfig_Clone_m7862B8297F86CA31E1A69BC23BBA6819771AB397,
	PacketGeneratorConfig_tB75C9DFBB65CAE892E81E36B9BEFB10CBC226A4F_CustomAttributesCacheGenerator_PacketGeneratorConfig_ClearPacketGenerator_m8F3C91BC62A1B11E3AFAB4AC8CDB798997E4927A,
	PacketGeneratorConfig_tB75C9DFBB65CAE892E81E36B9BEFB10CBC226A4F_CustomAttributesCacheGenerator_PacketGeneratorConfig_ClearOptions_m397FE10C8A8D3BC5CF70E4D76CA66579F88946AF,
	PacketGeneratorConfig_tB75C9DFBB65CAE892E81E36B9BEFB10CBC226A4F_CustomAttributesCacheGenerator_PacketGeneratorConfig_Equals_mFF71F177C5357C6ADF18AE90B67B1B030D2B230D,
	PacketGeneratorConfig_tB75C9DFBB65CAE892E81E36B9BEFB10CBC226A4F_CustomAttributesCacheGenerator_PacketGeneratorConfig_Equals_mAFF8CA1140FFA35F3A7F59B55F34C582BB5BECC7,
	PacketGeneratorConfig_tB75C9DFBB65CAE892E81E36B9BEFB10CBC226A4F_CustomAttributesCacheGenerator_PacketGeneratorConfig_GetHashCode_m67C2EE0089FDA8A17452E8789A9226BF7497F2EB,
	PacketGeneratorConfig_tB75C9DFBB65CAE892E81E36B9BEFB10CBC226A4F_CustomAttributesCacheGenerator_PacketGeneratorConfig_ToString_mF8E0F155AB39F730DC8D028AEBF3F122AC9CF76C,
	PacketGeneratorConfig_tB75C9DFBB65CAE892E81E36B9BEFB10CBC226A4F_CustomAttributesCacheGenerator_PacketGeneratorConfig_WriteTo_mAD2E8F441993B1349F73DB355982F3018A9826DB,
	PacketGeneratorConfig_tB75C9DFBB65CAE892E81E36B9BEFB10CBC226A4F_CustomAttributesCacheGenerator_PacketGeneratorConfig_CalculateSize_m42D473EA3CB305950C349CC960667081855F7408,
	PacketGeneratorConfig_tB75C9DFBB65CAE892E81E36B9BEFB10CBC226A4F_CustomAttributesCacheGenerator_PacketGeneratorConfig_MergeFrom_m56A617696F27E0E76700262130434884884C864A,
	PacketGeneratorConfig_tB75C9DFBB65CAE892E81E36B9BEFB10CBC226A4F_CustomAttributesCacheGenerator_PacketGeneratorConfig_MergeFrom_m972AAB330709CD60D6DF3A296350D7FE9154B6D9,
	Rasterization_tF4ACD4173A2965257259025AB28357C2B065D6A3_CustomAttributesCacheGenerator_Rasterization__ctor_m55FC6BC6C94655B9C8CE0385CB44C1E81716DBB7,
	Rasterization_tF4ACD4173A2965257259025AB28357C2B065D6A3_CustomAttributesCacheGenerator_Rasterization__ctor_mD3697404E5C77CE873F448E92932C6CC0C18681C,
	Rasterization_tF4ACD4173A2965257259025AB28357C2B065D6A3_CustomAttributesCacheGenerator_Rasterization_Clone_m72998D69821C34E51500317806C6B8EF5C86EC6C,
	Rasterization_tF4ACD4173A2965257259025AB28357C2B065D6A3_CustomAttributesCacheGenerator_Rasterization_Equals_mA13E8A4D5D7B8915FF57D3B9C72C364DB37A2F1A,
	Rasterization_tF4ACD4173A2965257259025AB28357C2B065D6A3_CustomAttributesCacheGenerator_Rasterization_Equals_m7DAE4E1872E2285E1981E16DC001030FC42A1EFB,
	Rasterization_tF4ACD4173A2965257259025AB28357C2B065D6A3_CustomAttributesCacheGenerator_Rasterization_GetHashCode_m7C644190483001AC74DD0288F66F99E3993351F5,
	Rasterization_tF4ACD4173A2965257259025AB28357C2B065D6A3_CustomAttributesCacheGenerator_Rasterization_ToString_m0020CC92D76AC7E76D9770A619E69E0C9E22B2B3,
	Rasterization_tF4ACD4173A2965257259025AB28357C2B065D6A3_CustomAttributesCacheGenerator_Rasterization_WriteTo_mA8B9BAFEBF0E6AAC7056B5922700F05AAFDFD6E4,
	Rasterization_tF4ACD4173A2965257259025AB28357C2B065D6A3_CustomAttributesCacheGenerator_Rasterization_CalculateSize_m136A9B8A9D2FBF3A682F7BF2053C1588FA35AB5D,
	Rasterization_tF4ACD4173A2965257259025AB28357C2B065D6A3_CustomAttributesCacheGenerator_Rasterization_MergeFrom_m70BDB2F744263C31C8E738B4122D7FDE11E0225C,
	Rasterization_tF4ACD4173A2965257259025AB28357C2B065D6A3_CustomAttributesCacheGenerator_Rasterization_MergeFrom_mF5B9FC0CBCF3484AD858AC847AB6712619FADE13,
	Interval_t800C77AD1BB854B8EA764973820554E84A27A8B5_CustomAttributesCacheGenerator_Interval__ctor_m32A3957EBFFE3B98D584CE5835A00D77CEA1F3BE,
	Interval_t800C77AD1BB854B8EA764973820554E84A27A8B5_CustomAttributesCacheGenerator_Interval__ctor_m5E2DC10FD5A9373055DC59F91A2DF4AE47E32644,
	Interval_t800C77AD1BB854B8EA764973820554E84A27A8B5_CustomAttributesCacheGenerator_Interval_Clone_m07446964FC63DB770BDDB2850439BF01C88B3E53,
	Interval_t800C77AD1BB854B8EA764973820554E84A27A8B5_CustomAttributesCacheGenerator_Interval_ClearY_m9A1D81264BB6A7171F645F141D26B16B11FF59A1,
	Interval_t800C77AD1BB854B8EA764973820554E84A27A8B5_CustomAttributesCacheGenerator_Interval_ClearLeftX_m18139A501E8C9A583BEA41E83544720908938204,
	Interval_t800C77AD1BB854B8EA764973820554E84A27A8B5_CustomAttributesCacheGenerator_Interval_ClearRightX_m6EF10A9D5EA463F0C7E06C15932A66A8A7974662,
	Interval_t800C77AD1BB854B8EA764973820554E84A27A8B5_CustomAttributesCacheGenerator_Interval_Equals_m694D5F474327657E1C30F07C2AF9F10C9B24F43F,
	Interval_t800C77AD1BB854B8EA764973820554E84A27A8B5_CustomAttributesCacheGenerator_Interval_Equals_mFE99EB6740C0735C445F4AADA222DD792A4E98E4,
	Interval_t800C77AD1BB854B8EA764973820554E84A27A8B5_CustomAttributesCacheGenerator_Interval_GetHashCode_mC8E3DB33E588E151786C01B8160CE01B2898412A,
	Interval_t800C77AD1BB854B8EA764973820554E84A27A8B5_CustomAttributesCacheGenerator_Interval_ToString_m01323C62EB2CA2032A43994675B1F80BC6868CCC,
	Interval_t800C77AD1BB854B8EA764973820554E84A27A8B5_CustomAttributesCacheGenerator_Interval_WriteTo_m501236942A2D0E72E74799CD107E6C04E780731B,
	Interval_t800C77AD1BB854B8EA764973820554E84A27A8B5_CustomAttributesCacheGenerator_Interval_CalculateSize_m1A75A81A7A6EB5AB4EDF015348B58F3CC4C7DE95,
	Interval_t800C77AD1BB854B8EA764973820554E84A27A8B5_CustomAttributesCacheGenerator_Interval_MergeFrom_m8E8A02B6433AD6798D67E8F86A48B73039E311C1,
	Interval_t800C77AD1BB854B8EA764973820554E84A27A8B5_CustomAttributesCacheGenerator_Interval_MergeFrom_m08CA5B392DC43CB8FC5959A8E28146884E6C7376,
	Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF_CustomAttributesCacheGenerator_Rect__ctor_mA1A953C00E9D1E93F5FE705F28869BF03281E90E,
	Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF_CustomAttributesCacheGenerator_Rect__ctor_m3ABE7AB6875A28451F7D68FCD2316A5FFD6A33CF,
	Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF_CustomAttributesCacheGenerator_Rect_Clone_m32E8FD503464BCE245D992DC44220A17FCD192CF,
	Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF_CustomAttributesCacheGenerator_Rect_ClearXCenter_m907A2A14A98D9BE8337AB11E1E11BA6D82CA38C9,
	Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF_CustomAttributesCacheGenerator_Rect_ClearYCenter_mFC560214642EE24645202364FB8544833F84C9D9,
	Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF_CustomAttributesCacheGenerator_Rect_ClearHeight_m5311A4542887AA3987EAEB56C1EE174BBC46EF90,
	Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF_CustomAttributesCacheGenerator_Rect_ClearWidth_m2A0EF58EE901101AFA25FA5ED9C7EE9F7990E0D3,
	Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF_CustomAttributesCacheGenerator_Rect_ClearRotation_m935A6E03D8225670E9D90057502E99B479893CBA,
	Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF_CustomAttributesCacheGenerator_Rect_ClearRectId_m2582A0FD1E0F7B073ACCB7C566AD4CD67810589B,
	Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF_CustomAttributesCacheGenerator_Rect_Equals_m8D8318A274A804C8F7EC5995536C2C5AA121DAE4,
	Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF_CustomAttributesCacheGenerator_Rect_Equals_m8DDA30D9B6EFF7387D1E42982812D783565B971A,
	Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF_CustomAttributesCacheGenerator_Rect_GetHashCode_m894D11D8F145F7800CD41C46D4923F6701EA73BF,
	Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF_CustomAttributesCacheGenerator_Rect_ToString_m9026BF1BE9688FBA3BBAA49AACA2F0624BE20546,
	Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF_CustomAttributesCacheGenerator_Rect_WriteTo_m91BA7F768E68CEA159FE04DC30808C0E98FDBBF2,
	Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF_CustomAttributesCacheGenerator_Rect_CalculateSize_m458355AAEB1961AA3E49528C669FC23E89FF06F0,
	Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF_CustomAttributesCacheGenerator_Rect_MergeFrom_m04A5B2CFE20DC31FF14B0EB09212D6B01842C229,
	Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF_CustomAttributesCacheGenerator_Rect_MergeFrom_m7AF35F7AF17270258910584DC176688CC509CA98,
	NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F_CustomAttributesCacheGenerator_NormalizedRect__ctor_m3BDEDD59629DCC814DE948B03777DC7E9919548F,
	NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F_CustomAttributesCacheGenerator_NormalizedRect__ctor_m8EA938ADE44DBD9BA74A4EA26BFF929BA398F502,
	NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F_CustomAttributesCacheGenerator_NormalizedRect_Clone_m8B1DF6560577979C5C4AD7B9924DE8A9682BDA7D,
	NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F_CustomAttributesCacheGenerator_NormalizedRect_ClearXCenter_m6BB5D3B7A5D3DEE0964F2ADB0EFB015498C971D3,
	NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F_CustomAttributesCacheGenerator_NormalizedRect_ClearYCenter_m964FB56A39DF73D2EBABDF9B3AB8E4B525BF68BB,
	NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F_CustomAttributesCacheGenerator_NormalizedRect_ClearHeight_m53F519FBB27E41F1015BBF0F9B4F943CB99D521E,
	NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F_CustomAttributesCacheGenerator_NormalizedRect_ClearWidth_m57AD3453E31C13D518C962CB8E89D347AC9A05F5,
	NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F_CustomAttributesCacheGenerator_NormalizedRect_ClearRotation_m7743803A9EE445B91B7637283BAC2D3783CC3323,
	NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F_CustomAttributesCacheGenerator_NormalizedRect_ClearRectId_m1C7E71F564CAE806DEC00BFA1C8EC5E9A7C76006,
	NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F_CustomAttributesCacheGenerator_NormalizedRect_Equals_m5A0389CD1BF8AFEEBA109FE3BC72A84F2B69E2BE,
	NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F_CustomAttributesCacheGenerator_NormalizedRect_Equals_m4650183705C1C20A3686FCE49AA990E2CCFA9167,
	NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F_CustomAttributesCacheGenerator_NormalizedRect_GetHashCode_m2835A32974C581C5AACF9EA8640D3499FAE29B5A,
	NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F_CustomAttributesCacheGenerator_NormalizedRect_ToString_mD9680EE1C02BBFA4E938610290B3E2AAA4AAA896,
	NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F_CustomAttributesCacheGenerator_NormalizedRect_WriteTo_m223F03100F382A13B791A585F64ABB578CB91BF3,
	NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F_CustomAttributesCacheGenerator_NormalizedRect_CalculateSize_mBE22B1A36E88C86ED0DB31FBEE65AB6B6A35CB63,
	NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F_CustomAttributesCacheGenerator_NormalizedRect_MergeFrom_mDBA715172C2C850F99074679573E1BB7FF7589E1,
	NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F_CustomAttributesCacheGenerator_NormalizedRect_MergeFrom_m0469B3573A2B64594C4690D3E91CD1C41EFD382E,
	StatusHandlerConfig_t91FFA23EB8EE9D668FCF2A8D0E8E308308A81985_CustomAttributesCacheGenerator_StatusHandlerConfig__ctor_m0C87DFFEF9B52D562AD68762DD83C895E084F1EC,
	StatusHandlerConfig_t91FFA23EB8EE9D668FCF2A8D0E8E308308A81985_CustomAttributesCacheGenerator_StatusHandlerConfig__ctor_mC337F4A5270315F9DF7FE4372468EDE59F5D9B1B,
	StatusHandlerConfig_t91FFA23EB8EE9D668FCF2A8D0E8E308308A81985_CustomAttributesCacheGenerator_StatusHandlerConfig_Clone_mD7BF122D61DDC2B08ACA4521444F692937EB82C1,
	StatusHandlerConfig_t91FFA23EB8EE9D668FCF2A8D0E8E308308A81985_CustomAttributesCacheGenerator_StatusHandlerConfig_ClearStatusHandler_m07CC6662A252212369F7D57B3E3A518A8F8DCA2B,
	StatusHandlerConfig_t91FFA23EB8EE9D668FCF2A8D0E8E308308A81985_CustomAttributesCacheGenerator_StatusHandlerConfig_ClearOptions_mE947E969C224326EA97B7903F732FEC89BFF1176,
	StatusHandlerConfig_t91FFA23EB8EE9D668FCF2A8D0E8E308308A81985_CustomAttributesCacheGenerator_StatusHandlerConfig_Equals_mE15F141F7DD9A8E50CE22E98E94843557421C569,
	StatusHandlerConfig_t91FFA23EB8EE9D668FCF2A8D0E8E308308A81985_CustomAttributesCacheGenerator_StatusHandlerConfig_Equals_mD1A6F9828313594E5C7EF144F5119F54EEE0435A,
	StatusHandlerConfig_t91FFA23EB8EE9D668FCF2A8D0E8E308308A81985_CustomAttributesCacheGenerator_StatusHandlerConfig_GetHashCode_mF6A6AD649A9387349F6B83D7844CDAECC44AA4C4,
	StatusHandlerConfig_t91FFA23EB8EE9D668FCF2A8D0E8E308308A81985_CustomAttributesCacheGenerator_StatusHandlerConfig_ToString_m128817CC910667012E7587B84888E8EB925DE190,
	StatusHandlerConfig_t91FFA23EB8EE9D668FCF2A8D0E8E308308A81985_CustomAttributesCacheGenerator_StatusHandlerConfig_WriteTo_m160C08E410413EC1615505EF407B79F1557ACE34,
	StatusHandlerConfig_t91FFA23EB8EE9D668FCF2A8D0E8E308308A81985_CustomAttributesCacheGenerator_StatusHandlerConfig_CalculateSize_mA82A950CEEDBF1E241EFEF0B855551966114782D,
	StatusHandlerConfig_t91FFA23EB8EE9D668FCF2A8D0E8E308308A81985_CustomAttributesCacheGenerator_StatusHandlerConfig_MergeFrom_m6F38CBD207080A9DE0CF68449CCC1AA881877239,
	StatusHandlerConfig_t91FFA23EB8EE9D668FCF2A8D0E8E308308A81985_CustomAttributesCacheGenerator_StatusHandlerConfig_MergeFrom_mAAE549448B55E008DBF0395D6BB87C183EF0ACCE,
	Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435_CustomAttributesCacheGenerator_Sticker__ctor_m9F07EA56D9484EC587B82322B0E0718DF88BF800,
	Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435_CustomAttributesCacheGenerator_Sticker__ctor_m2E998EAE290A49D1067301693237681798D172D9,
	Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435_CustomAttributesCacheGenerator_Sticker_Clone_mC616C2DA4B6978BDCE602EC25D2AEBEAC58CC7B2,
	Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435_CustomAttributesCacheGenerator_Sticker_ClearId_m9A821A6CA49F2B6488490BC5012C1A372AA225B2,
	Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435_CustomAttributesCacheGenerator_Sticker_ClearX_m81A2073EC1E6E28D522BA83A09D25B59054C10B4,
	Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435_CustomAttributesCacheGenerator_Sticker_ClearY_mCCDB6ED5BBCC5470C536E9308EEDBF03EBC5A1FC,
	Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435_CustomAttributesCacheGenerator_Sticker_ClearRotation_m706F6BE276F0822E52C34373C1A3A5DB0C7B3D9E,
	Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435_CustomAttributesCacheGenerator_Sticker_ClearScale_m66C7A603A0A374BFD1C1973F4F3C30399BD6260A,
	Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435_CustomAttributesCacheGenerator_Sticker_ClearRenderId_mEA835CE5755D482612166621EAC9FF4C05607150,
	Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435_CustomAttributesCacheGenerator_Sticker_Equals_m35D29CDEEB1B41EC384FEC46477C4042D40B05AF,
	Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435_CustomAttributesCacheGenerator_Sticker_Equals_m337E02F0A28B1447BA8B24631C35FF1C286E60C9,
	Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435_CustomAttributesCacheGenerator_Sticker_GetHashCode_m24AFE04034BF649FC23E1E49AD29DEAEEE7C04AB,
	Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435_CustomAttributesCacheGenerator_Sticker_ToString_m044B37DFDBABC8E510CCDB2944D1A7819B5149D0,
	Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435_CustomAttributesCacheGenerator_Sticker_WriteTo_m0BAF019B7204C3399E1FA485E981DC36F94648DB,
	Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435_CustomAttributesCacheGenerator_Sticker_CalculateSize_mB443FFC825293F7F0ED3AD2A415E4621EF3EA59B,
	Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435_CustomAttributesCacheGenerator_Sticker_MergeFrom_mCF73E0750E5110F3AF4A75B6A09824D7B4484C9B,
	Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435_CustomAttributesCacheGenerator_Sticker_MergeFrom_m9CADCC2E1344332FBCAC86D3FA7488F908175131,
	StickerRoll_t5CCFC6B2292625CF867C2C935AB4749B8FA97D59_CustomAttributesCacheGenerator_StickerRoll__ctor_m7D0E0B4B49A31F27819E3777EDFB6B58A11174DB,
	StickerRoll_t5CCFC6B2292625CF867C2C935AB4749B8FA97D59_CustomAttributesCacheGenerator_StickerRoll__ctor_mC0E9A98013535B1F5E13F0E01117B200EC09E1C7,
	StickerRoll_t5CCFC6B2292625CF867C2C935AB4749B8FA97D59_CustomAttributesCacheGenerator_StickerRoll_Clone_m096072A72723DB3BF8F7311D5E14236BAEC77850,
	StickerRoll_t5CCFC6B2292625CF867C2C935AB4749B8FA97D59_CustomAttributesCacheGenerator_StickerRoll_Equals_m3E5A8335E874E955AA1A7EB2D2400B4BC6A36F93,
	StickerRoll_t5CCFC6B2292625CF867C2C935AB4749B8FA97D59_CustomAttributesCacheGenerator_StickerRoll_Equals_m1583815F5C84B1A33349A25E7C1D10D9B454B786,
	StickerRoll_t5CCFC6B2292625CF867C2C935AB4749B8FA97D59_CustomAttributesCacheGenerator_StickerRoll_GetHashCode_mB2561E014D54E102F9091D03556DCF6D1A678F56,
	StickerRoll_t5CCFC6B2292625CF867C2C935AB4749B8FA97D59_CustomAttributesCacheGenerator_StickerRoll_ToString_m18198D57FDEDBBC074B162CC75414E505B20431E,
	StickerRoll_t5CCFC6B2292625CF867C2C935AB4749B8FA97D59_CustomAttributesCacheGenerator_StickerRoll_WriteTo_m152931EE67A3A849500DCF04913BBB3CEB5B549E,
	StickerRoll_t5CCFC6B2292625CF867C2C935AB4749B8FA97D59_CustomAttributesCacheGenerator_StickerRoll_CalculateSize_m57AA5A87B5891693324E260A333D8B70D013EC0C,
	StickerRoll_t5CCFC6B2292625CF867C2C935AB4749B8FA97D59_CustomAttributesCacheGenerator_StickerRoll_MergeFrom_m7339523DC45A8AF327B08EEC2E9BC55C83E0381A,
	StickerRoll_t5CCFC6B2292625CF867C2C935AB4749B8FA97D59_CustomAttributesCacheGenerator_StickerRoll_MergeFrom_m46C03127E58A827A900A8602C569D8E8343441F5,
	InputStreamHandlerConfig_t1BAFAC8382A20E901CDE2D3D7E86E757DD5E9D16_CustomAttributesCacheGenerator_InputStreamHandlerConfig__ctor_mDA2A0ADCFBE563186C6F1E56BDBDC3F19522EF52,
	InputStreamHandlerConfig_t1BAFAC8382A20E901CDE2D3D7E86E757DD5E9D16_CustomAttributesCacheGenerator_InputStreamHandlerConfig__ctor_m2A5888DCE80A2B2DB059CB563795F28C050114F9,
	InputStreamHandlerConfig_t1BAFAC8382A20E901CDE2D3D7E86E757DD5E9D16_CustomAttributesCacheGenerator_InputStreamHandlerConfig_Clone_m1C7D8601234B62154567A71C893796A3DD5BCFB8,
	InputStreamHandlerConfig_t1BAFAC8382A20E901CDE2D3D7E86E757DD5E9D16_CustomAttributesCacheGenerator_InputStreamHandlerConfig_ClearInputStreamHandler_m778DA275FBA99054E9B2367B45EE2DA982BE3CB3,
	InputStreamHandlerConfig_t1BAFAC8382A20E901CDE2D3D7E86E757DD5E9D16_CustomAttributesCacheGenerator_InputStreamHandlerConfig_ClearOptions_mC4B0ECD6F0DA723730F3E45FFDBE9D5AC42DFED4,
	InputStreamHandlerConfig_t1BAFAC8382A20E901CDE2D3D7E86E757DD5E9D16_CustomAttributesCacheGenerator_InputStreamHandlerConfig_Equals_m920E22E1FFCB71B026DD2541AE7343A1DC541032,
	InputStreamHandlerConfig_t1BAFAC8382A20E901CDE2D3D7E86E757DD5E9D16_CustomAttributesCacheGenerator_InputStreamHandlerConfig_Equals_mE3FF7A087C37572FAEDD71033B5856E49AA4BAE7,
	InputStreamHandlerConfig_t1BAFAC8382A20E901CDE2D3D7E86E757DD5E9D16_CustomAttributesCacheGenerator_InputStreamHandlerConfig_GetHashCode_m58B4E8D647A7C52BAC4306EBEBE35E370DA58F4B,
	InputStreamHandlerConfig_t1BAFAC8382A20E901CDE2D3D7E86E757DD5E9D16_CustomAttributesCacheGenerator_InputStreamHandlerConfig_ToString_m5A51A1B49D862152FB108FDD9E7EC26DBDD754C7,
	InputStreamHandlerConfig_t1BAFAC8382A20E901CDE2D3D7E86E757DD5E9D16_CustomAttributesCacheGenerator_InputStreamHandlerConfig_WriteTo_mCE51105C65479A27CC3078A2C761EAA386676EF1,
	InputStreamHandlerConfig_t1BAFAC8382A20E901CDE2D3D7E86E757DD5E9D16_CustomAttributesCacheGenerator_InputStreamHandlerConfig_CalculateSize_m4825B60F49C6987E66E0EA4124F5371E7517963C,
	InputStreamHandlerConfig_t1BAFAC8382A20E901CDE2D3D7E86E757DD5E9D16_CustomAttributesCacheGenerator_InputStreamHandlerConfig_MergeFrom_m5313E6C7EFF4B95CD73FCE7ED2B19FE92CA5B1A2,
	InputStreamHandlerConfig_t1BAFAC8382A20E901CDE2D3D7E86E757DD5E9D16_CustomAttributesCacheGenerator_InputStreamHandlerConfig_MergeFrom_mB0FF550FD90B38F91C9A89A027AF419A29D5B3E8,
	OutputStreamHandlerConfig_t472506B1EB46A165DEDFD74B41B42728C003617D_CustomAttributesCacheGenerator_OutputStreamHandlerConfig__ctor_m05C62D4D22FF9FF66AF9BB724990B2FFEF12520F,
	OutputStreamHandlerConfig_t472506B1EB46A165DEDFD74B41B42728C003617D_CustomAttributesCacheGenerator_OutputStreamHandlerConfig__ctor_m2C42D8AEFEC3AEA97E86245C7244C6186B1EC37B,
	OutputStreamHandlerConfig_t472506B1EB46A165DEDFD74B41B42728C003617D_CustomAttributesCacheGenerator_OutputStreamHandlerConfig_Clone_mC359C6B6F92C04CD25AA67A7F2E95F90D5837248,
	OutputStreamHandlerConfig_t472506B1EB46A165DEDFD74B41B42728C003617D_CustomAttributesCacheGenerator_OutputStreamHandlerConfig_ClearOutputStreamHandler_m1B66407BF083E4781A6BB19D8C0502845610C3E0,
	OutputStreamHandlerConfig_t472506B1EB46A165DEDFD74B41B42728C003617D_CustomAttributesCacheGenerator_OutputStreamHandlerConfig_ClearOptions_mE6629E3A15B7C6E8DA899E244B1473B6F533849C,
	OutputStreamHandlerConfig_t472506B1EB46A165DEDFD74B41B42728C003617D_CustomAttributesCacheGenerator_OutputStreamHandlerConfig_Equals_m6C32151DC752E25C8DEDB017643B7FA8BF3F356C,
	OutputStreamHandlerConfig_t472506B1EB46A165DEDFD74B41B42728C003617D_CustomAttributesCacheGenerator_OutputStreamHandlerConfig_Equals_m82C75029D3E419F810CAD772CCDE87705D433C2B,
	OutputStreamHandlerConfig_t472506B1EB46A165DEDFD74B41B42728C003617D_CustomAttributesCacheGenerator_OutputStreamHandlerConfig_GetHashCode_m858E4EF857A0EBEA85DA7F18FC322F4B7CF519C1,
	OutputStreamHandlerConfig_t472506B1EB46A165DEDFD74B41B42728C003617D_CustomAttributesCacheGenerator_OutputStreamHandlerConfig_ToString_mD615009A9DBB0AF31B5EC2F952482129EC268201,
	OutputStreamHandlerConfig_t472506B1EB46A165DEDFD74B41B42728C003617D_CustomAttributesCacheGenerator_OutputStreamHandlerConfig_WriteTo_m97B3BB527940366F4AD1983486997DF660920C14,
	OutputStreamHandlerConfig_t472506B1EB46A165DEDFD74B41B42728C003617D_CustomAttributesCacheGenerator_OutputStreamHandlerConfig_CalculateSize_mF73E0648CDAF78E335B21A903AEAB5223BB210BC,
	OutputStreamHandlerConfig_t472506B1EB46A165DEDFD74B41B42728C003617D_CustomAttributesCacheGenerator_OutputStreamHandlerConfig_MergeFrom_mBB90784471D1BF4F6D21792DA33C740B5AE4489F,
	OutputStreamHandlerConfig_t472506B1EB46A165DEDFD74B41B42728C003617D_CustomAttributesCacheGenerator_OutputStreamHandlerConfig_MergeFrom_mEBEB8A78D238C7BCBA267A3982A7B82F4AB8CE39,
	AssetBundleManager_tF3C2B5AFF59FB819D342441D3BEBBD5C7B54BACD_CustomAttributesCacheGenerator_AssetBundleManager_LoadAssetBundleAsync_mE25419B8260B8C411CED0EDF65F03336E5A25DBD,
	AssetBundleManager_tF3C2B5AFF59FB819D342441D3BEBBD5C7B54BACD_CustomAttributesCacheGenerator_AssetBundleManager_PrepareAssetAsync_m0CED483ECF346567C5E1328E28E459262DD0131D,
	AssetBundleManager_tF3C2B5AFF59FB819D342441D3BEBBD5C7B54BACD_CustomAttributesCacheGenerator_AssetBundleManager_PathToResourceAsFile_mE4EA5782FC953FF16031FD0EB659BD758E0EDC7E,
	AssetBundleManager_tF3C2B5AFF59FB819D342441D3BEBBD5C7B54BACD_CustomAttributesCacheGenerator_AssetBundleManager_GetResourceContents_mDFE89208C34422AEE3F365704F969ADFA843BA84,
	AssetBundleManager_tF3C2B5AFF59FB819D342441D3BEBBD5C7B54BACD_CustomAttributesCacheGenerator_AssetBundleManager_WriteCacheFileAsync_m5F88F8554EB84D0743BA2BD9B7D0E75A26F0E316,
	U3CLoadAssetBundleAsyncU3Ed__21_t580563DF049EEAABF939C11D81845B846D3B687D_CustomAttributesCacheGenerator_U3CLoadAssetBundleAsyncU3Ed__21_SetStateMachine_mB1E3AA674C432E165404B70B653A6052F97B5070,
	U3CPrepareAssetAsyncU3Ed__23_tE0D0088355DC75E2373DA3E87446BEC021802D5A_CustomAttributesCacheGenerator_U3CPrepareAssetAsyncU3Ed__23_SetStateMachine_m290CC6603F5D2FC97EC17298E510722817A5A414,
	U3CWriteCacheFileAsyncU3Ed__28_tAD75A60D5B46B633653D85972A00F53DAF9D62C1_CustomAttributesCacheGenerator_U3CWriteCacheFileAsyncU3Ed__28_SetStateMachine_m4078D3327DCD2BBFBF231DE1709C9CD3B7EAD5C4,
	AsyncOperationExtension_t9E461E9BF1A1DD60860A4835F5747291425E8EE1_CustomAttributesCacheGenerator_AsyncOperationExtension_GetAwaiter_mB80919BDBE42E14B2F7B3A4ADB12EB308F0B8A9C,
	LocalAssetManager_t9373BED16CEBAE58B7FE4A595684AC703D75483C_CustomAttributesCacheGenerator_LocalAssetManager_PrepareAssetAsync_mB0FE9494A0DA547BA1823704C614DADD52EB629F,
	LocalAssetManager_t9373BED16CEBAE58B7FE4A595684AC703D75483C_CustomAttributesCacheGenerator_LocalAssetManager_PathToResourceAsFile_mD93EA6538E2E77CCD4ACB981201294DFFDF0C3A6,
	LocalAssetManager_t9373BED16CEBAE58B7FE4A595684AC703D75483C_CustomAttributesCacheGenerator_LocalAssetManager_GetResourceContents_m36D0D7964026D770978C9BECA4F18D510A46648A,
	U3CPrepareAssetAsyncU3Ed__9_t6EC072023AF74799B02659540A1E75A0260F55C6_CustomAttributesCacheGenerator_U3CPrepareAssetAsyncU3Ed__9_SetStateMachine_mC7A20B6945A994F96677055B1D8DE081F0F89849,
	FaceGeometry_t7B246BEE497814C69CD0E81B2116D2C9746BF305_CustomAttributesCacheGenerator_FaceGeometry__ctor_m3C8727C2045277EA8A051205D4FC9A5CE3D5E89A,
	FaceGeometry_t7B246BEE497814C69CD0E81B2116D2C9746BF305_CustomAttributesCacheGenerator_FaceGeometry__ctor_m2C60845DDA8A847E19706B9BD1CCF8064A1C5124,
	FaceGeometry_t7B246BEE497814C69CD0E81B2116D2C9746BF305_CustomAttributesCacheGenerator_FaceGeometry_Clone_m8489E2E228E407688DA6FEDFFC665B20256CC9BE,
	FaceGeometry_t7B246BEE497814C69CD0E81B2116D2C9746BF305_CustomAttributesCacheGenerator_FaceGeometry_ClearMesh_m7CD19E757E9FD9D40EEE8C790A17A34F9EB32C86,
	FaceGeometry_t7B246BEE497814C69CD0E81B2116D2C9746BF305_CustomAttributesCacheGenerator_FaceGeometry_ClearPoseTransformMatrix_m5E342DF34AA1B1C802830EE2BEB6C53AD40BAD4F,
	FaceGeometry_t7B246BEE497814C69CD0E81B2116D2C9746BF305_CustomAttributesCacheGenerator_FaceGeometry_Equals_m24D19204C5A3E02D27CB6F1EF569A1D60B36AD7F,
	FaceGeometry_t7B246BEE497814C69CD0E81B2116D2C9746BF305_CustomAttributesCacheGenerator_FaceGeometry_Equals_m4AB38A52827525069D398725064F885308DF8426,
	FaceGeometry_t7B246BEE497814C69CD0E81B2116D2C9746BF305_CustomAttributesCacheGenerator_FaceGeometry_GetHashCode_m9A30D9E1C207E32C0035016651B8292E1AC0978D,
	FaceGeometry_t7B246BEE497814C69CD0E81B2116D2C9746BF305_CustomAttributesCacheGenerator_FaceGeometry_ToString_m8A03508AD29218B0490EEC9FCC3090B9FA68C618,
	FaceGeometry_t7B246BEE497814C69CD0E81B2116D2C9746BF305_CustomAttributesCacheGenerator_FaceGeometry_WriteTo_mA3D5E866007FE45435FD2F0C241F5BFF9000DA46,
	FaceGeometry_t7B246BEE497814C69CD0E81B2116D2C9746BF305_CustomAttributesCacheGenerator_FaceGeometry_CalculateSize_m8244CFFD3C4A8DA2ABB011819E670C9B6002EC6D,
	FaceGeometry_t7B246BEE497814C69CD0E81B2116D2C9746BF305_CustomAttributesCacheGenerator_FaceGeometry_MergeFrom_mE6D2821557D75E5A220BBAD1AF0533C69708A99B,
	FaceGeometry_t7B246BEE497814C69CD0E81B2116D2C9746BF305_CustomAttributesCacheGenerator_FaceGeometry_MergeFrom_m635F8DC14C15F10F7ADD712FBE7899E7DF8B918C,
	Mesh3d_t038DF3815E8BB8AC98EE930DFAA9C27D5A3AE2FA_CustomAttributesCacheGenerator_Mesh3d__ctor_m9F9250640B8B59C63500CA172405221839AC8659,
	Mesh3d_t038DF3815E8BB8AC98EE930DFAA9C27D5A3AE2FA_CustomAttributesCacheGenerator_Mesh3d__ctor_m3DA3D0DF3B76850845F5492101B043CDFA74891B,
	Mesh3d_t038DF3815E8BB8AC98EE930DFAA9C27D5A3AE2FA_CustomAttributesCacheGenerator_Mesh3d_Clone_m1480A74D40F2C4A9F767C9BADE298FA2C929E2FE,
	Mesh3d_t038DF3815E8BB8AC98EE930DFAA9C27D5A3AE2FA_CustomAttributesCacheGenerator_Mesh3d_ClearVertexType_m0636B3AF901073C2526A37C1F2A09715A3AF9EDE,
	Mesh3d_t038DF3815E8BB8AC98EE930DFAA9C27D5A3AE2FA_CustomAttributesCacheGenerator_Mesh3d_ClearPrimitiveType_mDA655E945A9E7C5224BC7B466BEB4DC2C0237280,
	Mesh3d_t038DF3815E8BB8AC98EE930DFAA9C27D5A3AE2FA_CustomAttributesCacheGenerator_Mesh3d_Equals_m8608984036EA7B53D8E397986337302F7A26916D,
	Mesh3d_t038DF3815E8BB8AC98EE930DFAA9C27D5A3AE2FA_CustomAttributesCacheGenerator_Mesh3d_Equals_m1A13E2324B6A21935A44DF3AF37B6F090083DFC2,
	Mesh3d_t038DF3815E8BB8AC98EE930DFAA9C27D5A3AE2FA_CustomAttributesCacheGenerator_Mesh3d_GetHashCode_m385ABBC9D72FB51065C89DB3616E642F47066A0A,
	Mesh3d_t038DF3815E8BB8AC98EE930DFAA9C27D5A3AE2FA_CustomAttributesCacheGenerator_Mesh3d_ToString_m2628330180A999E8A61BC16A9DC80C69E4B9C03C,
	Mesh3d_t038DF3815E8BB8AC98EE930DFAA9C27D5A3AE2FA_CustomAttributesCacheGenerator_Mesh3d_WriteTo_m5CBA34FA8D18DFAB07AC9AC358FEFB2626FDE216,
	Mesh3d_t038DF3815E8BB8AC98EE930DFAA9C27D5A3AE2FA_CustomAttributesCacheGenerator_Mesh3d_CalculateSize_m1AC813F566F4AAE361BA291AC9FEB73457651D91,
	Mesh3d_t038DF3815E8BB8AC98EE930DFAA9C27D5A3AE2FA_CustomAttributesCacheGenerator_Mesh3d_MergeFrom_mA837C24529468595959D161C916756CA53E4BFDD,
	Mesh3d_t038DF3815E8BB8AC98EE930DFAA9C27D5A3AE2FA_CustomAttributesCacheGenerator_Mesh3d_MergeFrom_mDBAB758798822F0692FBBADAFDABE64BE546153E,
	U3CU3Ec__DisplayClass2_0_t82048CC1732D57D29FE279E3CAF06974BD411658_CustomAttributesCacheGenerator_U3CU3Ec__DisplayClass2_0_U3CCanonicalNodeNameU3Eb__1_mFF877148EC50CBF017DFA3AC2C7B076B8592640C____pair0,
	U3CU3Ec__DisplayClass2_0_t82048CC1732D57D29FE279E3CAF06974BD411658_CustomAttributesCacheGenerator_U3CU3Ec__DisplayClass2_0_U3CCanonicalNodeNameU3Eb__2_m3D71E7837D418B4AE8BED40F3FBF79EEB1090097____pair0,
	ExecutorConfig_t2755CF63D93D5DC1B5534655FCC554960354EE7F_CustomAttributesCacheGenerator_ExecutorConfig_t2755CF63D93D5DC1B5534655FCC554960354EE7F____Parser_PropertyInfo,
	ExecutorConfig_t2755CF63D93D5DC1B5534655FCC554960354EE7F_CustomAttributesCacheGenerator_ExecutorConfig_t2755CF63D93D5DC1B5534655FCC554960354EE7F____Descriptor_PropertyInfo,
	ExecutorConfig_t2755CF63D93D5DC1B5534655FCC554960354EE7F_CustomAttributesCacheGenerator_ExecutorConfig_t2755CF63D93D5DC1B5534655FCC554960354EE7F____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo,
	ExecutorConfig_t2755CF63D93D5DC1B5534655FCC554960354EE7F_CustomAttributesCacheGenerator_ExecutorConfig_t2755CF63D93D5DC1B5534655FCC554960354EE7F____Name_PropertyInfo,
	ExecutorConfig_t2755CF63D93D5DC1B5534655FCC554960354EE7F_CustomAttributesCacheGenerator_ExecutorConfig_t2755CF63D93D5DC1B5534655FCC554960354EE7F____Type_PropertyInfo,
	ExecutorConfig_t2755CF63D93D5DC1B5534655FCC554960354EE7F_CustomAttributesCacheGenerator_ExecutorConfig_t2755CF63D93D5DC1B5534655FCC554960354EE7F____Options_PropertyInfo,
	InputCollection_tA4C4583EB805BFBA48936774F9E2EF02712021D2_CustomAttributesCacheGenerator_InputCollection_tA4C4583EB805BFBA48936774F9E2EF02712021D2____Parser_PropertyInfo,
	InputCollection_tA4C4583EB805BFBA48936774F9E2EF02712021D2_CustomAttributesCacheGenerator_InputCollection_tA4C4583EB805BFBA48936774F9E2EF02712021D2____Descriptor_PropertyInfo,
	InputCollection_tA4C4583EB805BFBA48936774F9E2EF02712021D2_CustomAttributesCacheGenerator_InputCollection_tA4C4583EB805BFBA48936774F9E2EF02712021D2____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo,
	InputCollection_tA4C4583EB805BFBA48936774F9E2EF02712021D2_CustomAttributesCacheGenerator_InputCollection_tA4C4583EB805BFBA48936774F9E2EF02712021D2____Name_PropertyInfo,
	InputCollection_tA4C4583EB805BFBA48936774F9E2EF02712021D2_CustomAttributesCacheGenerator_InputCollection_tA4C4583EB805BFBA48936774F9E2EF02712021D2____SidePacketName_PropertyInfo,
	InputCollection_tA4C4583EB805BFBA48936774F9E2EF02712021D2_CustomAttributesCacheGenerator_InputCollection_tA4C4583EB805BFBA48936774F9E2EF02712021D2____ExternalInputName_PropertyInfo,
	InputCollection_tA4C4583EB805BFBA48936774F9E2EF02712021D2_CustomAttributesCacheGenerator_InputCollection_tA4C4583EB805BFBA48936774F9E2EF02712021D2____InputType_PropertyInfo,
	InputCollection_tA4C4583EB805BFBA48936774F9E2EF02712021D2_CustomAttributesCacheGenerator_InputCollection_tA4C4583EB805BFBA48936774F9E2EF02712021D2____FileName_PropertyInfo,
	InputCollectionSet_tAF58E12C5E427F540512C06F0E46D278E52D89AA_CustomAttributesCacheGenerator_InputCollectionSet_tAF58E12C5E427F540512C06F0E46D278E52D89AA____Parser_PropertyInfo,
	InputCollectionSet_tAF58E12C5E427F540512C06F0E46D278E52D89AA_CustomAttributesCacheGenerator_InputCollectionSet_tAF58E12C5E427F540512C06F0E46D278E52D89AA____Descriptor_PropertyInfo,
	InputCollectionSet_tAF58E12C5E427F540512C06F0E46D278E52D89AA_CustomAttributesCacheGenerator_InputCollectionSet_tAF58E12C5E427F540512C06F0E46D278E52D89AA____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo,
	InputCollectionSet_tAF58E12C5E427F540512C06F0E46D278E52D89AA_CustomAttributesCacheGenerator_InputCollectionSet_tAF58E12C5E427F540512C06F0E46D278E52D89AA____InputCollection_PropertyInfo,
	InputStreamInfo_t1D0174BE2B5EB4CF4B87459D2B634D2C17CB6E0B_CustomAttributesCacheGenerator_InputStreamInfo_t1D0174BE2B5EB4CF4B87459D2B634D2C17CB6E0B____Parser_PropertyInfo,
	InputStreamInfo_t1D0174BE2B5EB4CF4B87459D2B634D2C17CB6E0B_CustomAttributesCacheGenerator_InputStreamInfo_t1D0174BE2B5EB4CF4B87459D2B634D2C17CB6E0B____Descriptor_PropertyInfo,
	InputStreamInfo_t1D0174BE2B5EB4CF4B87459D2B634D2C17CB6E0B_CustomAttributesCacheGenerator_InputStreamInfo_t1D0174BE2B5EB4CF4B87459D2B634D2C17CB6E0B____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo,
	InputStreamInfo_t1D0174BE2B5EB4CF4B87459D2B634D2C17CB6E0B_CustomAttributesCacheGenerator_InputStreamInfo_t1D0174BE2B5EB4CF4B87459D2B634D2C17CB6E0B____TagIndex_PropertyInfo,
	InputStreamInfo_t1D0174BE2B5EB4CF4B87459D2B634D2C17CB6E0B_CustomAttributesCacheGenerator_InputStreamInfo_t1D0174BE2B5EB4CF4B87459D2B634D2C17CB6E0B____BackEdge_PropertyInfo,
	ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968_CustomAttributesCacheGenerator_ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968____Parser_PropertyInfo,
	ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968_CustomAttributesCacheGenerator_ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968____Descriptor_PropertyInfo,
	ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968_CustomAttributesCacheGenerator_ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo,
	ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968_CustomAttributesCacheGenerator_ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968____HistogramIntervalSizeUsec_PropertyInfo,
	ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968_CustomAttributesCacheGenerator_ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968____NumHistogramIntervals_PropertyInfo,
	ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968_CustomAttributesCacheGenerator_ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968____EnableInputOutputLatency_PropertyInfo,
	ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968_CustomAttributesCacheGenerator_ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968____EnableProfiler_PropertyInfo,
	ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968_CustomAttributesCacheGenerator_ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968____EnableStreamLatency_PropertyInfo,
	ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968_CustomAttributesCacheGenerator_ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968____UsePacketTimestampForAddedPacket_PropertyInfo,
	ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968_CustomAttributesCacheGenerator_ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968____TraceLogCapacity_PropertyInfo,
	ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968_CustomAttributesCacheGenerator_ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968____TraceEventTypesDisabled_PropertyInfo,
	ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968_CustomAttributesCacheGenerator_ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968____TraceLogPath_PropertyInfo,
	ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968_CustomAttributesCacheGenerator_ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968____TraceLogCount_PropertyInfo,
	ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968_CustomAttributesCacheGenerator_ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968____TraceLogIntervalUsec_PropertyInfo,
	ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968_CustomAttributesCacheGenerator_ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968____TraceLogMarginUsec_PropertyInfo,
	ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968_CustomAttributesCacheGenerator_ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968____TraceLogDurationEvents_PropertyInfo,
	ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968_CustomAttributesCacheGenerator_ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968____TraceLogIntervalCount_PropertyInfo,
	ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968_CustomAttributesCacheGenerator_ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968____TraceLogDisabled_PropertyInfo,
	ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968_CustomAttributesCacheGenerator_ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968____TraceEnabled_PropertyInfo,
	ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968_CustomAttributesCacheGenerator_ProfilerConfig_tB95B69E4B2D0C39E94FC8FDA4DDE382936ACD968____TraceLogInstantEvents_PropertyInfo,
	CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348_CustomAttributesCacheGenerator_CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348____Parser_PropertyInfo,
	CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348_CustomAttributesCacheGenerator_CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348____Descriptor_PropertyInfo,
	CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348_CustomAttributesCacheGenerator_CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo,
	CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348_CustomAttributesCacheGenerator_CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348____Node_PropertyInfo,
	CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348_CustomAttributesCacheGenerator_CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348____PacketFactory_PropertyInfo,
	CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348_CustomAttributesCacheGenerator_CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348____PacketGenerator_PropertyInfo,
	CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348_CustomAttributesCacheGenerator_CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348____NumThreads_PropertyInfo,
	CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348_CustomAttributesCacheGenerator_CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348____StatusHandler_PropertyInfo,
	CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348_CustomAttributesCacheGenerator_CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348____InputStream_PropertyInfo,
	CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348_CustomAttributesCacheGenerator_CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348____OutputStream_PropertyInfo,
	CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348_CustomAttributesCacheGenerator_CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348____InputSidePacket_PropertyInfo,
	CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348_CustomAttributesCacheGenerator_CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348____OutputSidePacket_PropertyInfo,
	CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348_CustomAttributesCacheGenerator_CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348____MaxQueueSize_PropertyInfo,
	CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348_CustomAttributesCacheGenerator_CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348____ReportDeadlock_PropertyInfo,
	CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348_CustomAttributesCacheGenerator_CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348____InputStreamHandler_PropertyInfo,
	CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348_CustomAttributesCacheGenerator_CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348____OutputStreamHandler_PropertyInfo,
	CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348_CustomAttributesCacheGenerator_CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348____Executor_PropertyInfo,
	CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348_CustomAttributesCacheGenerator_CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348____ProfilerConfig_PropertyInfo,
	CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348_CustomAttributesCacheGenerator_CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348____Package_PropertyInfo,
	CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348_CustomAttributesCacheGenerator_CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348____Type_PropertyInfo,
	CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348_CustomAttributesCacheGenerator_CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348____Options_PropertyInfo,
	CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348_CustomAttributesCacheGenerator_CalculatorGraphConfig_t67FA59B5AD97E96B31E0D0A31F11BB1DD6D20348____GraphOptions_PropertyInfo,
	Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483_CustomAttributesCacheGenerator_Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483____Parser_PropertyInfo,
	Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483_CustomAttributesCacheGenerator_Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483____Descriptor_PropertyInfo,
	Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483_CustomAttributesCacheGenerator_Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo,
	Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483_CustomAttributesCacheGenerator_Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483____Name_PropertyInfo,
	Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483_CustomAttributesCacheGenerator_Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483____Calculator_PropertyInfo,
	Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483_CustomAttributesCacheGenerator_Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483____InputStream_PropertyInfo,
	Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483_CustomAttributesCacheGenerator_Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483____OutputStream_PropertyInfo,
	Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483_CustomAttributesCacheGenerator_Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483____InputSidePacket_PropertyInfo,
	Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483_CustomAttributesCacheGenerator_Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483____OutputSidePacket_PropertyInfo,
	Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483_CustomAttributesCacheGenerator_Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483____Options_PropertyInfo,
	Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483_CustomAttributesCacheGenerator_Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483____NodeOptions_PropertyInfo,
	Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483_CustomAttributesCacheGenerator_Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483____SourceLayer_PropertyInfo,
	Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483_CustomAttributesCacheGenerator_Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483____BufferSizeHint_PropertyInfo,
	Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483_CustomAttributesCacheGenerator_Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483____InputStreamHandler_PropertyInfo,
	Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483_CustomAttributesCacheGenerator_Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483____OutputStreamHandler_PropertyInfo,
	Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483_CustomAttributesCacheGenerator_Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483____InputStreamInfo_PropertyInfo,
	Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483_CustomAttributesCacheGenerator_Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483____Executor_PropertyInfo,
	Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483_CustomAttributesCacheGenerator_Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483____ProfilerConfig_PropertyInfo,
	Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483_CustomAttributesCacheGenerator_Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483____MaxInFlight_PropertyInfo,
	Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483_CustomAttributesCacheGenerator_Node_tDC9B0B485FB882C46CF088968C637E67A8B3B483____ExternalInput_PropertyInfo,
	CalculatorOptions_t258E073EB6EBC7E44A9C100A180A4F40D5F89857_CustomAttributesCacheGenerator_CalculatorOptions_t258E073EB6EBC7E44A9C100A180A4F40D5F89857____Parser_PropertyInfo,
	CalculatorOptions_t258E073EB6EBC7E44A9C100A180A4F40D5F89857_CustomAttributesCacheGenerator_CalculatorOptions_t258E073EB6EBC7E44A9C100A180A4F40D5F89857____Descriptor_PropertyInfo,
	CalculatorOptions_t258E073EB6EBC7E44A9C100A180A4F40D5F89857_CustomAttributesCacheGenerator_CalculatorOptions_t258E073EB6EBC7E44A9C100A180A4F40D5F89857____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo,
	CalculatorOptions_t258E073EB6EBC7E44A9C100A180A4F40D5F89857_CustomAttributesCacheGenerator_CalculatorOptions_t258E073EB6EBC7E44A9C100A180A4F40D5F89857____MergeFields_PropertyInfo,
	CalculatorOptions_t258E073EB6EBC7E44A9C100A180A4F40D5F89857_CustomAttributesCacheGenerator_CalculatorOptions_t258E073EB6EBC7E44A9C100A180A4F40D5F89857____HasMergeFields_PropertyInfo,
	Classification_tE9A954065398D143A5B6EEA01FAED5BCD7D81BB9_CustomAttributesCacheGenerator_Classification_tE9A954065398D143A5B6EEA01FAED5BCD7D81BB9____Parser_PropertyInfo,
	Classification_tE9A954065398D143A5B6EEA01FAED5BCD7D81BB9_CustomAttributesCacheGenerator_Classification_tE9A954065398D143A5B6EEA01FAED5BCD7D81BB9____Descriptor_PropertyInfo,
	Classification_tE9A954065398D143A5B6EEA01FAED5BCD7D81BB9_CustomAttributesCacheGenerator_Classification_tE9A954065398D143A5B6EEA01FAED5BCD7D81BB9____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo,
	Classification_tE9A954065398D143A5B6EEA01FAED5BCD7D81BB9_CustomAttributesCacheGenerator_Classification_tE9A954065398D143A5B6EEA01FAED5BCD7D81BB9____Index_PropertyInfo,
	Classification_tE9A954065398D143A5B6EEA01FAED5BCD7D81BB9_CustomAttributesCacheGenerator_Classification_tE9A954065398D143A5B6EEA01FAED5BCD7D81BB9____HasIndex_PropertyInfo,
	Classification_tE9A954065398D143A5B6EEA01FAED5BCD7D81BB9_CustomAttributesCacheGenerator_Classification_tE9A954065398D143A5B6EEA01FAED5BCD7D81BB9____Score_PropertyInfo,
	Classification_tE9A954065398D143A5B6EEA01FAED5BCD7D81BB9_CustomAttributesCacheGenerator_Classification_tE9A954065398D143A5B6EEA01FAED5BCD7D81BB9____HasScore_PropertyInfo,
	Classification_tE9A954065398D143A5B6EEA01FAED5BCD7D81BB9_CustomAttributesCacheGenerator_Classification_tE9A954065398D143A5B6EEA01FAED5BCD7D81BB9____Label_PropertyInfo,
	Classification_tE9A954065398D143A5B6EEA01FAED5BCD7D81BB9_CustomAttributesCacheGenerator_Classification_tE9A954065398D143A5B6EEA01FAED5BCD7D81BB9____HasLabel_PropertyInfo,
	Classification_tE9A954065398D143A5B6EEA01FAED5BCD7D81BB9_CustomAttributesCacheGenerator_Classification_tE9A954065398D143A5B6EEA01FAED5BCD7D81BB9____DisplayName_PropertyInfo,
	Classification_tE9A954065398D143A5B6EEA01FAED5BCD7D81BB9_CustomAttributesCacheGenerator_Classification_tE9A954065398D143A5B6EEA01FAED5BCD7D81BB9____HasDisplayName_PropertyInfo,
	ClassificationList_tC58BDD049F1D6A853B63B07F07DCFA52DC02A8B5_CustomAttributesCacheGenerator_ClassificationList_tC58BDD049F1D6A853B63B07F07DCFA52DC02A8B5____Parser_PropertyInfo,
	ClassificationList_tC58BDD049F1D6A853B63B07F07DCFA52DC02A8B5_CustomAttributesCacheGenerator_ClassificationList_tC58BDD049F1D6A853B63B07F07DCFA52DC02A8B5____Descriptor_PropertyInfo,
	ClassificationList_tC58BDD049F1D6A853B63B07F07DCFA52DC02A8B5_CustomAttributesCacheGenerator_ClassificationList_tC58BDD049F1D6A853B63B07F07DCFA52DC02A8B5____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo,
	ClassificationList_tC58BDD049F1D6A853B63B07F07DCFA52DC02A8B5_CustomAttributesCacheGenerator_ClassificationList_tC58BDD049F1D6A853B63B07F07DCFA52DC02A8B5____Classification_PropertyInfo,
	Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E_CustomAttributesCacheGenerator_Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E____Parser_PropertyInfo,
	Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E_CustomAttributesCacheGenerator_Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E____Descriptor_PropertyInfo,
	Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E_CustomAttributesCacheGenerator_Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo,
	Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E_CustomAttributesCacheGenerator_Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E____Label_PropertyInfo,
	Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E_CustomAttributesCacheGenerator_Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E____LabelId_PropertyInfo,
	Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E_CustomAttributesCacheGenerator_Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E____Score_PropertyInfo,
	Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E_CustomAttributesCacheGenerator_Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E____LocationData_PropertyInfo,
	Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E_CustomAttributesCacheGenerator_Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E____HasLocationData_PropertyInfo,
	Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E_CustomAttributesCacheGenerator_Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E____FeatureTag_PropertyInfo,
	Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E_CustomAttributesCacheGenerator_Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E____HasFeatureTag_PropertyInfo,
	Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E_CustomAttributesCacheGenerator_Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E____TrackId_PropertyInfo,
	Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E_CustomAttributesCacheGenerator_Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E____HasTrackId_PropertyInfo,
	Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E_CustomAttributesCacheGenerator_Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E____DetectionId_PropertyInfo,
	Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E_CustomAttributesCacheGenerator_Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E____HasDetectionId_PropertyInfo,
	Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E_CustomAttributesCacheGenerator_Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E____AssociatedDetections_PropertyInfo,
	Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E_CustomAttributesCacheGenerator_Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E____DisplayName_PropertyInfo,
	Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E_CustomAttributesCacheGenerator_Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E____TimestampUsec_PropertyInfo,
	Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E_CustomAttributesCacheGenerator_Detection_t0EC502E241B708197BCC09A480A9940EE1BFB84E____HasTimestampUsec_PropertyInfo,
	AssociatedDetection_t24212822C1761635604505177F1691F542A4DF88_CustomAttributesCacheGenerator_AssociatedDetection_t24212822C1761635604505177F1691F542A4DF88____Parser_PropertyInfo,
	AssociatedDetection_t24212822C1761635604505177F1691F542A4DF88_CustomAttributesCacheGenerator_AssociatedDetection_t24212822C1761635604505177F1691F542A4DF88____Descriptor_PropertyInfo,
	AssociatedDetection_t24212822C1761635604505177F1691F542A4DF88_CustomAttributesCacheGenerator_AssociatedDetection_t24212822C1761635604505177F1691F542A4DF88____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo,
	AssociatedDetection_t24212822C1761635604505177F1691F542A4DF88_CustomAttributesCacheGenerator_AssociatedDetection_t24212822C1761635604505177F1691F542A4DF88____Id_PropertyInfo,
	AssociatedDetection_t24212822C1761635604505177F1691F542A4DF88_CustomAttributesCacheGenerator_AssociatedDetection_t24212822C1761635604505177F1691F542A4DF88____HasId_PropertyInfo,
	AssociatedDetection_t24212822C1761635604505177F1691F542A4DF88_CustomAttributesCacheGenerator_AssociatedDetection_t24212822C1761635604505177F1691F542A4DF88____Confidence_PropertyInfo,
	AssociatedDetection_t24212822C1761635604505177F1691F542A4DF88_CustomAttributesCacheGenerator_AssociatedDetection_t24212822C1761635604505177F1691F542A4DF88____HasConfidence_PropertyInfo,
	DetectionList_tA50FF81C129AF30DDF87A1F025CD9B8B4A10EF29_CustomAttributesCacheGenerator_DetectionList_tA50FF81C129AF30DDF87A1F025CD9B8B4A10EF29____Parser_PropertyInfo,
	DetectionList_tA50FF81C129AF30DDF87A1F025CD9B8B4A10EF29_CustomAttributesCacheGenerator_DetectionList_tA50FF81C129AF30DDF87A1F025CD9B8B4A10EF29____Descriptor_PropertyInfo,
	DetectionList_tA50FF81C129AF30DDF87A1F025CD9B8B4A10EF29_CustomAttributesCacheGenerator_DetectionList_tA50FF81C129AF30DDF87A1F025CD9B8B4A10EF29____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo,
	DetectionList_tA50FF81C129AF30DDF87A1F025CD9B8B4A10EF29_CustomAttributesCacheGenerator_DetectionList_tA50FF81C129AF30DDF87A1F025CD9B8B4A10EF29____Detection_PropertyInfo,
	Landmark_t593F12E6EAABFA00011E82F8D4408C3F75E4EFE3_CustomAttributesCacheGenerator_Landmark_t593F12E6EAABFA00011E82F8D4408C3F75E4EFE3____Parser_PropertyInfo,
	Landmark_t593F12E6EAABFA00011E82F8D4408C3F75E4EFE3_CustomAttributesCacheGenerator_Landmark_t593F12E6EAABFA00011E82F8D4408C3F75E4EFE3____Descriptor_PropertyInfo,
	Landmark_t593F12E6EAABFA00011E82F8D4408C3F75E4EFE3_CustomAttributesCacheGenerator_Landmark_t593F12E6EAABFA00011E82F8D4408C3F75E4EFE3____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo,
	Landmark_t593F12E6EAABFA00011E82F8D4408C3F75E4EFE3_CustomAttributesCacheGenerator_Landmark_t593F12E6EAABFA00011E82F8D4408C3F75E4EFE3____X_PropertyInfo,
	Landmark_t593F12E6EAABFA00011E82F8D4408C3F75E4EFE3_CustomAttributesCacheGenerator_Landmark_t593F12E6EAABFA00011E82F8D4408C3F75E4EFE3____HasX_PropertyInfo,
	Landmark_t593F12E6EAABFA00011E82F8D4408C3F75E4EFE3_CustomAttributesCacheGenerator_Landmark_t593F12E6EAABFA00011E82F8D4408C3F75E4EFE3____Y_PropertyInfo,
	Landmark_t593F12E6EAABFA00011E82F8D4408C3F75E4EFE3_CustomAttributesCacheGenerator_Landmark_t593F12E6EAABFA00011E82F8D4408C3F75E4EFE3____HasY_PropertyInfo,
	Landmark_t593F12E6EAABFA00011E82F8D4408C3F75E4EFE3_CustomAttributesCacheGenerator_Landmark_t593F12E6EAABFA00011E82F8D4408C3F75E4EFE3____Z_PropertyInfo,
	Landmark_t593F12E6EAABFA00011E82F8D4408C3F75E4EFE3_CustomAttributesCacheGenerator_Landmark_t593F12E6EAABFA00011E82F8D4408C3F75E4EFE3____HasZ_PropertyInfo,
	Landmark_t593F12E6EAABFA00011E82F8D4408C3F75E4EFE3_CustomAttributesCacheGenerator_Landmark_t593F12E6EAABFA00011E82F8D4408C3F75E4EFE3____Visibility_PropertyInfo,
	Landmark_t593F12E6EAABFA00011E82F8D4408C3F75E4EFE3_CustomAttributesCacheGenerator_Landmark_t593F12E6EAABFA00011E82F8D4408C3F75E4EFE3____HasVisibility_PropertyInfo,
	Landmark_t593F12E6EAABFA00011E82F8D4408C3F75E4EFE3_CustomAttributesCacheGenerator_Landmark_t593F12E6EAABFA00011E82F8D4408C3F75E4EFE3____Presence_PropertyInfo,
	Landmark_t593F12E6EAABFA00011E82F8D4408C3F75E4EFE3_CustomAttributesCacheGenerator_Landmark_t593F12E6EAABFA00011E82F8D4408C3F75E4EFE3____HasPresence_PropertyInfo,
	LandmarkList_t81090E798AEC4F7C446243CBAD3BDB14C16525EA_CustomAttributesCacheGenerator_LandmarkList_t81090E798AEC4F7C446243CBAD3BDB14C16525EA____Parser_PropertyInfo,
	LandmarkList_t81090E798AEC4F7C446243CBAD3BDB14C16525EA_CustomAttributesCacheGenerator_LandmarkList_t81090E798AEC4F7C446243CBAD3BDB14C16525EA____Descriptor_PropertyInfo,
	LandmarkList_t81090E798AEC4F7C446243CBAD3BDB14C16525EA_CustomAttributesCacheGenerator_LandmarkList_t81090E798AEC4F7C446243CBAD3BDB14C16525EA____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo,
	LandmarkList_t81090E798AEC4F7C446243CBAD3BDB14C16525EA_CustomAttributesCacheGenerator_LandmarkList_t81090E798AEC4F7C446243CBAD3BDB14C16525EA____Landmark_PropertyInfo,
	NormalizedLandmark_t5FAF8FE108D8B6740A93A860AF7786612D36A48C_CustomAttributesCacheGenerator_NormalizedLandmark_t5FAF8FE108D8B6740A93A860AF7786612D36A48C____Parser_PropertyInfo,
	NormalizedLandmark_t5FAF8FE108D8B6740A93A860AF7786612D36A48C_CustomAttributesCacheGenerator_NormalizedLandmark_t5FAF8FE108D8B6740A93A860AF7786612D36A48C____Descriptor_PropertyInfo,
	NormalizedLandmark_t5FAF8FE108D8B6740A93A860AF7786612D36A48C_CustomAttributesCacheGenerator_NormalizedLandmark_t5FAF8FE108D8B6740A93A860AF7786612D36A48C____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo,
	NormalizedLandmark_t5FAF8FE108D8B6740A93A860AF7786612D36A48C_CustomAttributesCacheGenerator_NormalizedLandmark_t5FAF8FE108D8B6740A93A860AF7786612D36A48C____X_PropertyInfo,
	NormalizedLandmark_t5FAF8FE108D8B6740A93A860AF7786612D36A48C_CustomAttributesCacheGenerator_NormalizedLandmark_t5FAF8FE108D8B6740A93A860AF7786612D36A48C____HasX_PropertyInfo,
	NormalizedLandmark_t5FAF8FE108D8B6740A93A860AF7786612D36A48C_CustomAttributesCacheGenerator_NormalizedLandmark_t5FAF8FE108D8B6740A93A860AF7786612D36A48C____Y_PropertyInfo,
	NormalizedLandmark_t5FAF8FE108D8B6740A93A860AF7786612D36A48C_CustomAttributesCacheGenerator_NormalizedLandmark_t5FAF8FE108D8B6740A93A860AF7786612D36A48C____HasY_PropertyInfo,
	NormalizedLandmark_t5FAF8FE108D8B6740A93A860AF7786612D36A48C_CustomAttributesCacheGenerator_NormalizedLandmark_t5FAF8FE108D8B6740A93A860AF7786612D36A48C____Z_PropertyInfo,
	NormalizedLandmark_t5FAF8FE108D8B6740A93A860AF7786612D36A48C_CustomAttributesCacheGenerator_NormalizedLandmark_t5FAF8FE108D8B6740A93A860AF7786612D36A48C____HasZ_PropertyInfo,
	NormalizedLandmark_t5FAF8FE108D8B6740A93A860AF7786612D36A48C_CustomAttributesCacheGenerator_NormalizedLandmark_t5FAF8FE108D8B6740A93A860AF7786612D36A48C____Visibility_PropertyInfo,
	NormalizedLandmark_t5FAF8FE108D8B6740A93A860AF7786612D36A48C_CustomAttributesCacheGenerator_NormalizedLandmark_t5FAF8FE108D8B6740A93A860AF7786612D36A48C____HasVisibility_PropertyInfo,
	NormalizedLandmark_t5FAF8FE108D8B6740A93A860AF7786612D36A48C_CustomAttributesCacheGenerator_NormalizedLandmark_t5FAF8FE108D8B6740A93A860AF7786612D36A48C____Presence_PropertyInfo,
	NormalizedLandmark_t5FAF8FE108D8B6740A93A860AF7786612D36A48C_CustomAttributesCacheGenerator_NormalizedLandmark_t5FAF8FE108D8B6740A93A860AF7786612D36A48C____HasPresence_PropertyInfo,
	NormalizedLandmarkList_tD96817E5BBC04C5051186DE23E72CA9FB2E8E11E_CustomAttributesCacheGenerator_NormalizedLandmarkList_tD96817E5BBC04C5051186DE23E72CA9FB2E8E11E____Parser_PropertyInfo,
	NormalizedLandmarkList_tD96817E5BBC04C5051186DE23E72CA9FB2E8E11E_CustomAttributesCacheGenerator_NormalizedLandmarkList_tD96817E5BBC04C5051186DE23E72CA9FB2E8E11E____Descriptor_PropertyInfo,
	NormalizedLandmarkList_tD96817E5BBC04C5051186DE23E72CA9FB2E8E11E_CustomAttributesCacheGenerator_NormalizedLandmarkList_tD96817E5BBC04C5051186DE23E72CA9FB2E8E11E____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo,
	NormalizedLandmarkList_tD96817E5BBC04C5051186DE23E72CA9FB2E8E11E_CustomAttributesCacheGenerator_NormalizedLandmarkList_tD96817E5BBC04C5051186DE23E72CA9FB2E8E11E____Landmark_PropertyInfo,
	LocationData_t137C5EE37EACB9E28B218F2FF28B3BFFF27BC81A_CustomAttributesCacheGenerator_LocationData_t137C5EE37EACB9E28B218F2FF28B3BFFF27BC81A____Parser_PropertyInfo,
	LocationData_t137C5EE37EACB9E28B218F2FF28B3BFFF27BC81A_CustomAttributesCacheGenerator_LocationData_t137C5EE37EACB9E28B218F2FF28B3BFFF27BC81A____Descriptor_PropertyInfo,
	LocationData_t137C5EE37EACB9E28B218F2FF28B3BFFF27BC81A_CustomAttributesCacheGenerator_LocationData_t137C5EE37EACB9E28B218F2FF28B3BFFF27BC81A____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo,
	LocationData_t137C5EE37EACB9E28B218F2FF28B3BFFF27BC81A_CustomAttributesCacheGenerator_LocationData_t137C5EE37EACB9E28B218F2FF28B3BFFF27BC81A____Format_PropertyInfo,
	LocationData_t137C5EE37EACB9E28B218F2FF28B3BFFF27BC81A_CustomAttributesCacheGenerator_LocationData_t137C5EE37EACB9E28B218F2FF28B3BFFF27BC81A____HasFormat_PropertyInfo,
	LocationData_t137C5EE37EACB9E28B218F2FF28B3BFFF27BC81A_CustomAttributesCacheGenerator_LocationData_t137C5EE37EACB9E28B218F2FF28B3BFFF27BC81A____BoundingBox_PropertyInfo,
	LocationData_t137C5EE37EACB9E28B218F2FF28B3BFFF27BC81A_CustomAttributesCacheGenerator_LocationData_t137C5EE37EACB9E28B218F2FF28B3BFFF27BC81A____HasBoundingBox_PropertyInfo,
	LocationData_t137C5EE37EACB9E28B218F2FF28B3BFFF27BC81A_CustomAttributesCacheGenerator_LocationData_t137C5EE37EACB9E28B218F2FF28B3BFFF27BC81A____RelativeBoundingBox_PropertyInfo,
	LocationData_t137C5EE37EACB9E28B218F2FF28B3BFFF27BC81A_CustomAttributesCacheGenerator_LocationData_t137C5EE37EACB9E28B218F2FF28B3BFFF27BC81A____HasRelativeBoundingBox_PropertyInfo,
	LocationData_t137C5EE37EACB9E28B218F2FF28B3BFFF27BC81A_CustomAttributesCacheGenerator_LocationData_t137C5EE37EACB9E28B218F2FF28B3BFFF27BC81A____Mask_PropertyInfo,
	LocationData_t137C5EE37EACB9E28B218F2FF28B3BFFF27BC81A_CustomAttributesCacheGenerator_LocationData_t137C5EE37EACB9E28B218F2FF28B3BFFF27BC81A____HasMask_PropertyInfo,
	LocationData_t137C5EE37EACB9E28B218F2FF28B3BFFF27BC81A_CustomAttributesCacheGenerator_LocationData_t137C5EE37EACB9E28B218F2FF28B3BFFF27BC81A____RelativeKeypoints_PropertyInfo,
	BoundingBox_tF4DCB9A65CC691059CAC02F205F73759C1A7ADAC_CustomAttributesCacheGenerator_BoundingBox_tF4DCB9A65CC691059CAC02F205F73759C1A7ADAC____Parser_PropertyInfo,
	BoundingBox_tF4DCB9A65CC691059CAC02F205F73759C1A7ADAC_CustomAttributesCacheGenerator_BoundingBox_tF4DCB9A65CC691059CAC02F205F73759C1A7ADAC____Descriptor_PropertyInfo,
	BoundingBox_tF4DCB9A65CC691059CAC02F205F73759C1A7ADAC_CustomAttributesCacheGenerator_BoundingBox_tF4DCB9A65CC691059CAC02F205F73759C1A7ADAC____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo,
	BoundingBox_tF4DCB9A65CC691059CAC02F205F73759C1A7ADAC_CustomAttributesCacheGenerator_BoundingBox_tF4DCB9A65CC691059CAC02F205F73759C1A7ADAC____Xmin_PropertyInfo,
	BoundingBox_tF4DCB9A65CC691059CAC02F205F73759C1A7ADAC_CustomAttributesCacheGenerator_BoundingBox_tF4DCB9A65CC691059CAC02F205F73759C1A7ADAC____HasXmin_PropertyInfo,
	BoundingBox_tF4DCB9A65CC691059CAC02F205F73759C1A7ADAC_CustomAttributesCacheGenerator_BoundingBox_tF4DCB9A65CC691059CAC02F205F73759C1A7ADAC____Ymin_PropertyInfo,
	BoundingBox_tF4DCB9A65CC691059CAC02F205F73759C1A7ADAC_CustomAttributesCacheGenerator_BoundingBox_tF4DCB9A65CC691059CAC02F205F73759C1A7ADAC____HasYmin_PropertyInfo,
	BoundingBox_tF4DCB9A65CC691059CAC02F205F73759C1A7ADAC_CustomAttributesCacheGenerator_BoundingBox_tF4DCB9A65CC691059CAC02F205F73759C1A7ADAC____Width_PropertyInfo,
	BoundingBox_tF4DCB9A65CC691059CAC02F205F73759C1A7ADAC_CustomAttributesCacheGenerator_BoundingBox_tF4DCB9A65CC691059CAC02F205F73759C1A7ADAC____HasWidth_PropertyInfo,
	BoundingBox_tF4DCB9A65CC691059CAC02F205F73759C1A7ADAC_CustomAttributesCacheGenerator_BoundingBox_tF4DCB9A65CC691059CAC02F205F73759C1A7ADAC____Height_PropertyInfo,
	BoundingBox_tF4DCB9A65CC691059CAC02F205F73759C1A7ADAC_CustomAttributesCacheGenerator_BoundingBox_tF4DCB9A65CC691059CAC02F205F73759C1A7ADAC____HasHeight_PropertyInfo,
	RelativeBoundingBox_tC31BDE3CCCD2009BC2562071BDB4B5DB13F6752E_CustomAttributesCacheGenerator_RelativeBoundingBox_tC31BDE3CCCD2009BC2562071BDB4B5DB13F6752E____Parser_PropertyInfo,
	RelativeBoundingBox_tC31BDE3CCCD2009BC2562071BDB4B5DB13F6752E_CustomAttributesCacheGenerator_RelativeBoundingBox_tC31BDE3CCCD2009BC2562071BDB4B5DB13F6752E____Descriptor_PropertyInfo,
	RelativeBoundingBox_tC31BDE3CCCD2009BC2562071BDB4B5DB13F6752E_CustomAttributesCacheGenerator_RelativeBoundingBox_tC31BDE3CCCD2009BC2562071BDB4B5DB13F6752E____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo,
	RelativeBoundingBox_tC31BDE3CCCD2009BC2562071BDB4B5DB13F6752E_CustomAttributesCacheGenerator_RelativeBoundingBox_tC31BDE3CCCD2009BC2562071BDB4B5DB13F6752E____Xmin_PropertyInfo,
	RelativeBoundingBox_tC31BDE3CCCD2009BC2562071BDB4B5DB13F6752E_CustomAttributesCacheGenerator_RelativeBoundingBox_tC31BDE3CCCD2009BC2562071BDB4B5DB13F6752E____HasXmin_PropertyInfo,
	RelativeBoundingBox_tC31BDE3CCCD2009BC2562071BDB4B5DB13F6752E_CustomAttributesCacheGenerator_RelativeBoundingBox_tC31BDE3CCCD2009BC2562071BDB4B5DB13F6752E____Ymin_PropertyInfo,
	RelativeBoundingBox_tC31BDE3CCCD2009BC2562071BDB4B5DB13F6752E_CustomAttributesCacheGenerator_RelativeBoundingBox_tC31BDE3CCCD2009BC2562071BDB4B5DB13F6752E____HasYmin_PropertyInfo,
	RelativeBoundingBox_tC31BDE3CCCD2009BC2562071BDB4B5DB13F6752E_CustomAttributesCacheGenerator_RelativeBoundingBox_tC31BDE3CCCD2009BC2562071BDB4B5DB13F6752E____Width_PropertyInfo,
	RelativeBoundingBox_tC31BDE3CCCD2009BC2562071BDB4B5DB13F6752E_CustomAttributesCacheGenerator_RelativeBoundingBox_tC31BDE3CCCD2009BC2562071BDB4B5DB13F6752E____HasWidth_PropertyInfo,
	RelativeBoundingBox_tC31BDE3CCCD2009BC2562071BDB4B5DB13F6752E_CustomAttributesCacheGenerator_RelativeBoundingBox_tC31BDE3CCCD2009BC2562071BDB4B5DB13F6752E____Height_PropertyInfo,
	RelativeBoundingBox_tC31BDE3CCCD2009BC2562071BDB4B5DB13F6752E_CustomAttributesCacheGenerator_RelativeBoundingBox_tC31BDE3CCCD2009BC2562071BDB4B5DB13F6752E____HasHeight_PropertyInfo,
	BinaryMask_t0F9C5C2054ED5487FD9D2297F6800F66AC695DBE_CustomAttributesCacheGenerator_BinaryMask_t0F9C5C2054ED5487FD9D2297F6800F66AC695DBE____Parser_PropertyInfo,
	BinaryMask_t0F9C5C2054ED5487FD9D2297F6800F66AC695DBE_CustomAttributesCacheGenerator_BinaryMask_t0F9C5C2054ED5487FD9D2297F6800F66AC695DBE____Descriptor_PropertyInfo,
	BinaryMask_t0F9C5C2054ED5487FD9D2297F6800F66AC695DBE_CustomAttributesCacheGenerator_BinaryMask_t0F9C5C2054ED5487FD9D2297F6800F66AC695DBE____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo,
	BinaryMask_t0F9C5C2054ED5487FD9D2297F6800F66AC695DBE_CustomAttributesCacheGenerator_BinaryMask_t0F9C5C2054ED5487FD9D2297F6800F66AC695DBE____Width_PropertyInfo,
	BinaryMask_t0F9C5C2054ED5487FD9D2297F6800F66AC695DBE_CustomAttributesCacheGenerator_BinaryMask_t0F9C5C2054ED5487FD9D2297F6800F66AC695DBE____HasWidth_PropertyInfo,
	BinaryMask_t0F9C5C2054ED5487FD9D2297F6800F66AC695DBE_CustomAttributesCacheGenerator_BinaryMask_t0F9C5C2054ED5487FD9D2297F6800F66AC695DBE____Height_PropertyInfo,
	BinaryMask_t0F9C5C2054ED5487FD9D2297F6800F66AC695DBE_CustomAttributesCacheGenerator_BinaryMask_t0F9C5C2054ED5487FD9D2297F6800F66AC695DBE____HasHeight_PropertyInfo,
	BinaryMask_t0F9C5C2054ED5487FD9D2297F6800F66AC695DBE_CustomAttributesCacheGenerator_BinaryMask_t0F9C5C2054ED5487FD9D2297F6800F66AC695DBE____Rasterization_PropertyInfo,
	BinaryMask_t0F9C5C2054ED5487FD9D2297F6800F66AC695DBE_CustomAttributesCacheGenerator_BinaryMask_t0F9C5C2054ED5487FD9D2297F6800F66AC695DBE____HasRasterization_PropertyInfo,
	RelativeKeypoint_tB0867DC1F9743FC97A924CB29A42ADB6A8E7DEA7_CustomAttributesCacheGenerator_RelativeKeypoint_tB0867DC1F9743FC97A924CB29A42ADB6A8E7DEA7____Parser_PropertyInfo,
	RelativeKeypoint_tB0867DC1F9743FC97A924CB29A42ADB6A8E7DEA7_CustomAttributesCacheGenerator_RelativeKeypoint_tB0867DC1F9743FC97A924CB29A42ADB6A8E7DEA7____Descriptor_PropertyInfo,
	RelativeKeypoint_tB0867DC1F9743FC97A924CB29A42ADB6A8E7DEA7_CustomAttributesCacheGenerator_RelativeKeypoint_tB0867DC1F9743FC97A924CB29A42ADB6A8E7DEA7____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo,
	RelativeKeypoint_tB0867DC1F9743FC97A924CB29A42ADB6A8E7DEA7_CustomAttributesCacheGenerator_RelativeKeypoint_tB0867DC1F9743FC97A924CB29A42ADB6A8E7DEA7____X_PropertyInfo,
	RelativeKeypoint_tB0867DC1F9743FC97A924CB29A42ADB6A8E7DEA7_CustomAttributesCacheGenerator_RelativeKeypoint_tB0867DC1F9743FC97A924CB29A42ADB6A8E7DEA7____HasX_PropertyInfo,
	RelativeKeypoint_tB0867DC1F9743FC97A924CB29A42ADB6A8E7DEA7_CustomAttributesCacheGenerator_RelativeKeypoint_tB0867DC1F9743FC97A924CB29A42ADB6A8E7DEA7____Y_PropertyInfo,
	RelativeKeypoint_tB0867DC1F9743FC97A924CB29A42ADB6A8E7DEA7_CustomAttributesCacheGenerator_RelativeKeypoint_tB0867DC1F9743FC97A924CB29A42ADB6A8E7DEA7____HasY_PropertyInfo,
	RelativeKeypoint_tB0867DC1F9743FC97A924CB29A42ADB6A8E7DEA7_CustomAttributesCacheGenerator_RelativeKeypoint_tB0867DC1F9743FC97A924CB29A42ADB6A8E7DEA7____KeypointLabel_PropertyInfo,
	RelativeKeypoint_tB0867DC1F9743FC97A924CB29A42ADB6A8E7DEA7_CustomAttributesCacheGenerator_RelativeKeypoint_tB0867DC1F9743FC97A924CB29A42ADB6A8E7DEA7____HasKeypointLabel_PropertyInfo,
	RelativeKeypoint_tB0867DC1F9743FC97A924CB29A42ADB6A8E7DEA7_CustomAttributesCacheGenerator_RelativeKeypoint_tB0867DC1F9743FC97A924CB29A42ADB6A8E7DEA7____Score_PropertyInfo,
	RelativeKeypoint_tB0867DC1F9743FC97A924CB29A42ADB6A8E7DEA7_CustomAttributesCacheGenerator_RelativeKeypoint_tB0867DC1F9743FC97A924CB29A42ADB6A8E7DEA7____HasScore_PropertyInfo,
	MatrixData_tA28091B9C8107B5FED6284DDF6D1AEFE2969E0C4_CustomAttributesCacheGenerator_MatrixData_tA28091B9C8107B5FED6284DDF6D1AEFE2969E0C4____Parser_PropertyInfo,
	MatrixData_tA28091B9C8107B5FED6284DDF6D1AEFE2969E0C4_CustomAttributesCacheGenerator_MatrixData_tA28091B9C8107B5FED6284DDF6D1AEFE2969E0C4____Descriptor_PropertyInfo,
	MatrixData_tA28091B9C8107B5FED6284DDF6D1AEFE2969E0C4_CustomAttributesCacheGenerator_MatrixData_tA28091B9C8107B5FED6284DDF6D1AEFE2969E0C4____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo,
	MatrixData_tA28091B9C8107B5FED6284DDF6D1AEFE2969E0C4_CustomAttributesCacheGenerator_MatrixData_tA28091B9C8107B5FED6284DDF6D1AEFE2969E0C4____Rows_PropertyInfo,
	MatrixData_tA28091B9C8107B5FED6284DDF6D1AEFE2969E0C4_CustomAttributesCacheGenerator_MatrixData_tA28091B9C8107B5FED6284DDF6D1AEFE2969E0C4____HasRows_PropertyInfo,
	MatrixData_tA28091B9C8107B5FED6284DDF6D1AEFE2969E0C4_CustomAttributesCacheGenerator_MatrixData_tA28091B9C8107B5FED6284DDF6D1AEFE2969E0C4____Cols_PropertyInfo,
	MatrixData_tA28091B9C8107B5FED6284DDF6D1AEFE2969E0C4_CustomAttributesCacheGenerator_MatrixData_tA28091B9C8107B5FED6284DDF6D1AEFE2969E0C4____HasCols_PropertyInfo,
	MatrixData_tA28091B9C8107B5FED6284DDF6D1AEFE2969E0C4_CustomAttributesCacheGenerator_MatrixData_tA28091B9C8107B5FED6284DDF6D1AEFE2969E0C4____PackedData_PropertyInfo,
	MatrixData_tA28091B9C8107B5FED6284DDF6D1AEFE2969E0C4_CustomAttributesCacheGenerator_MatrixData_tA28091B9C8107B5FED6284DDF6D1AEFE2969E0C4____Layout_PropertyInfo,
	MatrixData_tA28091B9C8107B5FED6284DDF6D1AEFE2969E0C4_CustomAttributesCacheGenerator_MatrixData_tA28091B9C8107B5FED6284DDF6D1AEFE2969E0C4____HasLayout_PropertyInfo,
	MediaPipeOptions_t9D271404AA6309644B1A0AEDF6116DD119F4D4D8_CustomAttributesCacheGenerator_MediaPipeOptions_t9D271404AA6309644B1A0AEDF6116DD119F4D4D8____Parser_PropertyInfo,
	MediaPipeOptions_t9D271404AA6309644B1A0AEDF6116DD119F4D4D8_CustomAttributesCacheGenerator_MediaPipeOptions_t9D271404AA6309644B1A0AEDF6116DD119F4D4D8____Descriptor_PropertyInfo,
	MediaPipeOptions_t9D271404AA6309644B1A0AEDF6116DD119F4D4D8_CustomAttributesCacheGenerator_MediaPipeOptions_t9D271404AA6309644B1A0AEDF6116DD119F4D4D8____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo,
	TimedModelMatrixProto_t1FEBAF26F6D2794B20DF340436C746FB747382B0_CustomAttributesCacheGenerator_TimedModelMatrixProto_t1FEBAF26F6D2794B20DF340436C746FB747382B0____Parser_PropertyInfo,
	TimedModelMatrixProto_t1FEBAF26F6D2794B20DF340436C746FB747382B0_CustomAttributesCacheGenerator_TimedModelMatrixProto_t1FEBAF26F6D2794B20DF340436C746FB747382B0____Descriptor_PropertyInfo,
	TimedModelMatrixProto_t1FEBAF26F6D2794B20DF340436C746FB747382B0_CustomAttributesCacheGenerator_TimedModelMatrixProto_t1FEBAF26F6D2794B20DF340436C746FB747382B0____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo,
	TimedModelMatrixProto_t1FEBAF26F6D2794B20DF340436C746FB747382B0_CustomAttributesCacheGenerator_TimedModelMatrixProto_t1FEBAF26F6D2794B20DF340436C746FB747382B0____MatrixEntries_PropertyInfo,
	TimedModelMatrixProto_t1FEBAF26F6D2794B20DF340436C746FB747382B0_CustomAttributesCacheGenerator_TimedModelMatrixProto_t1FEBAF26F6D2794B20DF340436C746FB747382B0____TimeMsec_PropertyInfo,
	TimedModelMatrixProto_t1FEBAF26F6D2794B20DF340436C746FB747382B0_CustomAttributesCacheGenerator_TimedModelMatrixProto_t1FEBAF26F6D2794B20DF340436C746FB747382B0____HasTimeMsec_PropertyInfo,
	TimedModelMatrixProto_t1FEBAF26F6D2794B20DF340436C746FB747382B0_CustomAttributesCacheGenerator_TimedModelMatrixProto_t1FEBAF26F6D2794B20DF340436C746FB747382B0____Id_PropertyInfo,
	TimedModelMatrixProto_t1FEBAF26F6D2794B20DF340436C746FB747382B0_CustomAttributesCacheGenerator_TimedModelMatrixProto_t1FEBAF26F6D2794B20DF340436C746FB747382B0____HasId_PropertyInfo,
	TimedModelMatrixProtoList_t3A75AC882CCC8C5C7CE330E62547C87220B1A7A7_CustomAttributesCacheGenerator_TimedModelMatrixProtoList_t3A75AC882CCC8C5C7CE330E62547C87220B1A7A7____Parser_PropertyInfo,
	TimedModelMatrixProtoList_t3A75AC882CCC8C5C7CE330E62547C87220B1A7A7_CustomAttributesCacheGenerator_TimedModelMatrixProtoList_t3A75AC882CCC8C5C7CE330E62547C87220B1A7A7____Descriptor_PropertyInfo,
	TimedModelMatrixProtoList_t3A75AC882CCC8C5C7CE330E62547C87220B1A7A7_CustomAttributesCacheGenerator_TimedModelMatrixProtoList_t3A75AC882CCC8C5C7CE330E62547C87220B1A7A7____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo,
	TimedModelMatrixProtoList_t3A75AC882CCC8C5C7CE330E62547C87220B1A7A7_CustomAttributesCacheGenerator_TimedModelMatrixProtoList_t3A75AC882CCC8C5C7CE330E62547C87220B1A7A7____ModelMatrix_PropertyInfo,
	TimedVectorProto_t922BD38BFDF681636210E2476928157FE2C69DE7_CustomAttributesCacheGenerator_TimedVectorProto_t922BD38BFDF681636210E2476928157FE2C69DE7____Parser_PropertyInfo,
	TimedVectorProto_t922BD38BFDF681636210E2476928157FE2C69DE7_CustomAttributesCacheGenerator_TimedVectorProto_t922BD38BFDF681636210E2476928157FE2C69DE7____Descriptor_PropertyInfo,
	TimedVectorProto_t922BD38BFDF681636210E2476928157FE2C69DE7_CustomAttributesCacheGenerator_TimedVectorProto_t922BD38BFDF681636210E2476928157FE2C69DE7____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo,
	TimedVectorProto_t922BD38BFDF681636210E2476928157FE2C69DE7_CustomAttributesCacheGenerator_TimedVectorProto_t922BD38BFDF681636210E2476928157FE2C69DE7____VectorEntries_PropertyInfo,
	TimedVectorProto_t922BD38BFDF681636210E2476928157FE2C69DE7_CustomAttributesCacheGenerator_TimedVectorProto_t922BD38BFDF681636210E2476928157FE2C69DE7____TimeMsec_PropertyInfo,
	TimedVectorProto_t922BD38BFDF681636210E2476928157FE2C69DE7_CustomAttributesCacheGenerator_TimedVectorProto_t922BD38BFDF681636210E2476928157FE2C69DE7____HasTimeMsec_PropertyInfo,
	TimedVectorProto_t922BD38BFDF681636210E2476928157FE2C69DE7_CustomAttributesCacheGenerator_TimedVectorProto_t922BD38BFDF681636210E2476928157FE2C69DE7____Id_PropertyInfo,
	TimedVectorProto_t922BD38BFDF681636210E2476928157FE2C69DE7_CustomAttributesCacheGenerator_TimedVectorProto_t922BD38BFDF681636210E2476928157FE2C69DE7____HasId_PropertyInfo,
	TimedVectorProtoList_t628C533965DB55ADC9A8993A87569E9EA7B6033F_CustomAttributesCacheGenerator_TimedVectorProtoList_t628C533965DB55ADC9A8993A87569E9EA7B6033F____Parser_PropertyInfo,
	TimedVectorProtoList_t628C533965DB55ADC9A8993A87569E9EA7B6033F_CustomAttributesCacheGenerator_TimedVectorProtoList_t628C533965DB55ADC9A8993A87569E9EA7B6033F____Descriptor_PropertyInfo,
	TimedVectorProtoList_t628C533965DB55ADC9A8993A87569E9EA7B6033F_CustomAttributesCacheGenerator_TimedVectorProtoList_t628C533965DB55ADC9A8993A87569E9EA7B6033F____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo,
	TimedVectorProtoList_t628C533965DB55ADC9A8993A87569E9EA7B6033F_CustomAttributesCacheGenerator_TimedVectorProtoList_t628C533965DB55ADC9A8993A87569E9EA7B6033F____VectorList_PropertyInfo,
	PacketFactoryOptions_tEA685DCB184FF4693C4C66A20AA649B6D0214850_CustomAttributesCacheGenerator_PacketFactoryOptions_tEA685DCB184FF4693C4C66A20AA649B6D0214850____Parser_PropertyInfo,
	PacketFactoryOptions_tEA685DCB184FF4693C4C66A20AA649B6D0214850_CustomAttributesCacheGenerator_PacketFactoryOptions_tEA685DCB184FF4693C4C66A20AA649B6D0214850____Descriptor_PropertyInfo,
	PacketFactoryOptions_tEA685DCB184FF4693C4C66A20AA649B6D0214850_CustomAttributesCacheGenerator_PacketFactoryOptions_tEA685DCB184FF4693C4C66A20AA649B6D0214850____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo,
	PacketFactoryConfig_t5BA5CF02AEB213652B28644DDE0D5D5A21703E12_CustomAttributesCacheGenerator_PacketFactoryConfig_t5BA5CF02AEB213652B28644DDE0D5D5A21703E12____Parser_PropertyInfo,
	PacketFactoryConfig_t5BA5CF02AEB213652B28644DDE0D5D5A21703E12_CustomAttributesCacheGenerator_PacketFactoryConfig_t5BA5CF02AEB213652B28644DDE0D5D5A21703E12____Descriptor_PropertyInfo,
	PacketFactoryConfig_t5BA5CF02AEB213652B28644DDE0D5D5A21703E12_CustomAttributesCacheGenerator_PacketFactoryConfig_t5BA5CF02AEB213652B28644DDE0D5D5A21703E12____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo,
	PacketFactoryConfig_t5BA5CF02AEB213652B28644DDE0D5D5A21703E12_CustomAttributesCacheGenerator_PacketFactoryConfig_t5BA5CF02AEB213652B28644DDE0D5D5A21703E12____PacketFactory_PropertyInfo,
	PacketFactoryConfig_t5BA5CF02AEB213652B28644DDE0D5D5A21703E12_CustomAttributesCacheGenerator_PacketFactoryConfig_t5BA5CF02AEB213652B28644DDE0D5D5A21703E12____HasPacketFactory_PropertyInfo,
	PacketFactoryConfig_t5BA5CF02AEB213652B28644DDE0D5D5A21703E12_CustomAttributesCacheGenerator_PacketFactoryConfig_t5BA5CF02AEB213652B28644DDE0D5D5A21703E12____OutputSidePacket_PropertyInfo,
	PacketFactoryConfig_t5BA5CF02AEB213652B28644DDE0D5D5A21703E12_CustomAttributesCacheGenerator_PacketFactoryConfig_t5BA5CF02AEB213652B28644DDE0D5D5A21703E12____HasOutputSidePacket_PropertyInfo,
	PacketFactoryConfig_t5BA5CF02AEB213652B28644DDE0D5D5A21703E12_CustomAttributesCacheGenerator_PacketFactoryConfig_t5BA5CF02AEB213652B28644DDE0D5D5A21703E12____ExternalOutput_PropertyInfo,
	PacketFactoryConfig_t5BA5CF02AEB213652B28644DDE0D5D5A21703E12_CustomAttributesCacheGenerator_PacketFactoryConfig_t5BA5CF02AEB213652B28644DDE0D5D5A21703E12____HasExternalOutput_PropertyInfo,
	PacketFactoryConfig_t5BA5CF02AEB213652B28644DDE0D5D5A21703E12_CustomAttributesCacheGenerator_PacketFactoryConfig_t5BA5CF02AEB213652B28644DDE0D5D5A21703E12____Options_PropertyInfo,
	PacketFactoryConfig_t5BA5CF02AEB213652B28644DDE0D5D5A21703E12_CustomAttributesCacheGenerator_PacketFactoryConfig_t5BA5CF02AEB213652B28644DDE0D5D5A21703E12____HasOptions_PropertyInfo,
	PacketManagerConfig_t324C1B93AB9471F4658F17482A26B89C0BFAB519_CustomAttributesCacheGenerator_PacketManagerConfig_t324C1B93AB9471F4658F17482A26B89C0BFAB519____Parser_PropertyInfo,
	PacketManagerConfig_t324C1B93AB9471F4658F17482A26B89C0BFAB519_CustomAttributesCacheGenerator_PacketManagerConfig_t324C1B93AB9471F4658F17482A26B89C0BFAB519____Descriptor_PropertyInfo,
	PacketManagerConfig_t324C1B93AB9471F4658F17482A26B89C0BFAB519_CustomAttributesCacheGenerator_PacketManagerConfig_t324C1B93AB9471F4658F17482A26B89C0BFAB519____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo,
	PacketManagerConfig_t324C1B93AB9471F4658F17482A26B89C0BFAB519_CustomAttributesCacheGenerator_PacketManagerConfig_t324C1B93AB9471F4658F17482A26B89C0BFAB519____Packet_PropertyInfo,
	PacketGeneratorOptions_t103747F5D200028AFE532E63B3E64BE05BF8523A_CustomAttributesCacheGenerator_PacketGeneratorOptions_t103747F5D200028AFE532E63B3E64BE05BF8523A____Parser_PropertyInfo,
	PacketGeneratorOptions_t103747F5D200028AFE532E63B3E64BE05BF8523A_CustomAttributesCacheGenerator_PacketGeneratorOptions_t103747F5D200028AFE532E63B3E64BE05BF8523A____Descriptor_PropertyInfo,
	PacketGeneratorOptions_t103747F5D200028AFE532E63B3E64BE05BF8523A_CustomAttributesCacheGenerator_PacketGeneratorOptions_t103747F5D200028AFE532E63B3E64BE05BF8523A____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo,
	PacketGeneratorConfig_tB75C9DFBB65CAE892E81E36B9BEFB10CBC226A4F_CustomAttributesCacheGenerator_PacketGeneratorConfig_tB75C9DFBB65CAE892E81E36B9BEFB10CBC226A4F____Parser_PropertyInfo,
	PacketGeneratorConfig_tB75C9DFBB65CAE892E81E36B9BEFB10CBC226A4F_CustomAttributesCacheGenerator_PacketGeneratorConfig_tB75C9DFBB65CAE892E81E36B9BEFB10CBC226A4F____Descriptor_PropertyInfo,
	PacketGeneratorConfig_tB75C9DFBB65CAE892E81E36B9BEFB10CBC226A4F_CustomAttributesCacheGenerator_PacketGeneratorConfig_tB75C9DFBB65CAE892E81E36B9BEFB10CBC226A4F____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo,
	PacketGeneratorConfig_tB75C9DFBB65CAE892E81E36B9BEFB10CBC226A4F_CustomAttributesCacheGenerator_PacketGeneratorConfig_tB75C9DFBB65CAE892E81E36B9BEFB10CBC226A4F____PacketGenerator_PropertyInfo,
	PacketGeneratorConfig_tB75C9DFBB65CAE892E81E36B9BEFB10CBC226A4F_CustomAttributesCacheGenerator_PacketGeneratorConfig_tB75C9DFBB65CAE892E81E36B9BEFB10CBC226A4F____HasPacketGenerator_PropertyInfo,
	PacketGeneratorConfig_tB75C9DFBB65CAE892E81E36B9BEFB10CBC226A4F_CustomAttributesCacheGenerator_PacketGeneratorConfig_tB75C9DFBB65CAE892E81E36B9BEFB10CBC226A4F____InputSidePacket_PropertyInfo,
	PacketGeneratorConfig_tB75C9DFBB65CAE892E81E36B9BEFB10CBC226A4F_CustomAttributesCacheGenerator_PacketGeneratorConfig_tB75C9DFBB65CAE892E81E36B9BEFB10CBC226A4F____ExternalInput_PropertyInfo,
	PacketGeneratorConfig_tB75C9DFBB65CAE892E81E36B9BEFB10CBC226A4F_CustomAttributesCacheGenerator_PacketGeneratorConfig_tB75C9DFBB65CAE892E81E36B9BEFB10CBC226A4F____OutputSidePacket_PropertyInfo,
	PacketGeneratorConfig_tB75C9DFBB65CAE892E81E36B9BEFB10CBC226A4F_CustomAttributesCacheGenerator_PacketGeneratorConfig_tB75C9DFBB65CAE892E81E36B9BEFB10CBC226A4F____ExternalOutput_PropertyInfo,
	PacketGeneratorConfig_tB75C9DFBB65CAE892E81E36B9BEFB10CBC226A4F_CustomAttributesCacheGenerator_PacketGeneratorConfig_tB75C9DFBB65CAE892E81E36B9BEFB10CBC226A4F____Options_PropertyInfo,
	PacketGeneratorConfig_tB75C9DFBB65CAE892E81E36B9BEFB10CBC226A4F_CustomAttributesCacheGenerator_PacketGeneratorConfig_tB75C9DFBB65CAE892E81E36B9BEFB10CBC226A4F____HasOptions_PropertyInfo,
	Rasterization_tF4ACD4173A2965257259025AB28357C2B065D6A3_CustomAttributesCacheGenerator_Rasterization_tF4ACD4173A2965257259025AB28357C2B065D6A3____Parser_PropertyInfo,
	Rasterization_tF4ACD4173A2965257259025AB28357C2B065D6A3_CustomAttributesCacheGenerator_Rasterization_tF4ACD4173A2965257259025AB28357C2B065D6A3____Descriptor_PropertyInfo,
	Rasterization_tF4ACD4173A2965257259025AB28357C2B065D6A3_CustomAttributesCacheGenerator_Rasterization_tF4ACD4173A2965257259025AB28357C2B065D6A3____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo,
	Rasterization_tF4ACD4173A2965257259025AB28357C2B065D6A3_CustomAttributesCacheGenerator_Rasterization_tF4ACD4173A2965257259025AB28357C2B065D6A3____Interval_PropertyInfo,
	Interval_t800C77AD1BB854B8EA764973820554E84A27A8B5_CustomAttributesCacheGenerator_Interval_t800C77AD1BB854B8EA764973820554E84A27A8B5____Parser_PropertyInfo,
	Interval_t800C77AD1BB854B8EA764973820554E84A27A8B5_CustomAttributesCacheGenerator_Interval_t800C77AD1BB854B8EA764973820554E84A27A8B5____Descriptor_PropertyInfo,
	Interval_t800C77AD1BB854B8EA764973820554E84A27A8B5_CustomAttributesCacheGenerator_Interval_t800C77AD1BB854B8EA764973820554E84A27A8B5____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo,
	Interval_t800C77AD1BB854B8EA764973820554E84A27A8B5_CustomAttributesCacheGenerator_Interval_t800C77AD1BB854B8EA764973820554E84A27A8B5____Y_PropertyInfo,
	Interval_t800C77AD1BB854B8EA764973820554E84A27A8B5_CustomAttributesCacheGenerator_Interval_t800C77AD1BB854B8EA764973820554E84A27A8B5____HasY_PropertyInfo,
	Interval_t800C77AD1BB854B8EA764973820554E84A27A8B5_CustomAttributesCacheGenerator_Interval_t800C77AD1BB854B8EA764973820554E84A27A8B5____LeftX_PropertyInfo,
	Interval_t800C77AD1BB854B8EA764973820554E84A27A8B5_CustomAttributesCacheGenerator_Interval_t800C77AD1BB854B8EA764973820554E84A27A8B5____HasLeftX_PropertyInfo,
	Interval_t800C77AD1BB854B8EA764973820554E84A27A8B5_CustomAttributesCacheGenerator_Interval_t800C77AD1BB854B8EA764973820554E84A27A8B5____RightX_PropertyInfo,
	Interval_t800C77AD1BB854B8EA764973820554E84A27A8B5_CustomAttributesCacheGenerator_Interval_t800C77AD1BB854B8EA764973820554E84A27A8B5____HasRightX_PropertyInfo,
	Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF_CustomAttributesCacheGenerator_Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF____Parser_PropertyInfo,
	Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF_CustomAttributesCacheGenerator_Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF____Descriptor_PropertyInfo,
	Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF_CustomAttributesCacheGenerator_Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo,
	Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF_CustomAttributesCacheGenerator_Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF____XCenter_PropertyInfo,
	Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF_CustomAttributesCacheGenerator_Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF____HasXCenter_PropertyInfo,
	Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF_CustomAttributesCacheGenerator_Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF____YCenter_PropertyInfo,
	Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF_CustomAttributesCacheGenerator_Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF____HasYCenter_PropertyInfo,
	Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF_CustomAttributesCacheGenerator_Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF____Height_PropertyInfo,
	Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF_CustomAttributesCacheGenerator_Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF____HasHeight_PropertyInfo,
	Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF_CustomAttributesCacheGenerator_Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF____Width_PropertyInfo,
	Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF_CustomAttributesCacheGenerator_Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF____HasWidth_PropertyInfo,
	Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF_CustomAttributesCacheGenerator_Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF____Rotation_PropertyInfo,
	Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF_CustomAttributesCacheGenerator_Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF____HasRotation_PropertyInfo,
	Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF_CustomAttributesCacheGenerator_Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF____RectId_PropertyInfo,
	Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF_CustomAttributesCacheGenerator_Rect_t50A75BBF6E86E9916AB6DC6CAE61BCDCB1C3C0CF____HasRectId_PropertyInfo,
	NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F_CustomAttributesCacheGenerator_NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F____Parser_PropertyInfo,
	NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F_CustomAttributesCacheGenerator_NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F____Descriptor_PropertyInfo,
	NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F_CustomAttributesCacheGenerator_NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo,
	NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F_CustomAttributesCacheGenerator_NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F____XCenter_PropertyInfo,
	NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F_CustomAttributesCacheGenerator_NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F____HasXCenter_PropertyInfo,
	NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F_CustomAttributesCacheGenerator_NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F____YCenter_PropertyInfo,
	NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F_CustomAttributesCacheGenerator_NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F____HasYCenter_PropertyInfo,
	NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F_CustomAttributesCacheGenerator_NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F____Height_PropertyInfo,
	NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F_CustomAttributesCacheGenerator_NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F____HasHeight_PropertyInfo,
	NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F_CustomAttributesCacheGenerator_NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F____Width_PropertyInfo,
	NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F_CustomAttributesCacheGenerator_NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F____HasWidth_PropertyInfo,
	NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F_CustomAttributesCacheGenerator_NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F____Rotation_PropertyInfo,
	NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F_CustomAttributesCacheGenerator_NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F____HasRotation_PropertyInfo,
	NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F_CustomAttributesCacheGenerator_NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F____RectId_PropertyInfo,
	NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F_CustomAttributesCacheGenerator_NormalizedRect_t4FA849F4D896964D7F60C4D49DD8CD73668E713F____HasRectId_PropertyInfo,
	StatusHandlerConfig_t91FFA23EB8EE9D668FCF2A8D0E8E308308A81985_CustomAttributesCacheGenerator_StatusHandlerConfig_t91FFA23EB8EE9D668FCF2A8D0E8E308308A81985____Parser_PropertyInfo,
	StatusHandlerConfig_t91FFA23EB8EE9D668FCF2A8D0E8E308308A81985_CustomAttributesCacheGenerator_StatusHandlerConfig_t91FFA23EB8EE9D668FCF2A8D0E8E308308A81985____Descriptor_PropertyInfo,
	StatusHandlerConfig_t91FFA23EB8EE9D668FCF2A8D0E8E308308A81985_CustomAttributesCacheGenerator_StatusHandlerConfig_t91FFA23EB8EE9D668FCF2A8D0E8E308308A81985____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo,
	StatusHandlerConfig_t91FFA23EB8EE9D668FCF2A8D0E8E308308A81985_CustomAttributesCacheGenerator_StatusHandlerConfig_t91FFA23EB8EE9D668FCF2A8D0E8E308308A81985____StatusHandler_PropertyInfo,
	StatusHandlerConfig_t91FFA23EB8EE9D668FCF2A8D0E8E308308A81985_CustomAttributesCacheGenerator_StatusHandlerConfig_t91FFA23EB8EE9D668FCF2A8D0E8E308308A81985____HasStatusHandler_PropertyInfo,
	StatusHandlerConfig_t91FFA23EB8EE9D668FCF2A8D0E8E308308A81985_CustomAttributesCacheGenerator_StatusHandlerConfig_t91FFA23EB8EE9D668FCF2A8D0E8E308308A81985____InputSidePacket_PropertyInfo,
	StatusHandlerConfig_t91FFA23EB8EE9D668FCF2A8D0E8E308308A81985_CustomAttributesCacheGenerator_StatusHandlerConfig_t91FFA23EB8EE9D668FCF2A8D0E8E308308A81985____ExternalInput_PropertyInfo,
	StatusHandlerConfig_t91FFA23EB8EE9D668FCF2A8D0E8E308308A81985_CustomAttributesCacheGenerator_StatusHandlerConfig_t91FFA23EB8EE9D668FCF2A8D0E8E308308A81985____Options_PropertyInfo,
	StatusHandlerConfig_t91FFA23EB8EE9D668FCF2A8D0E8E308308A81985_CustomAttributesCacheGenerator_StatusHandlerConfig_t91FFA23EB8EE9D668FCF2A8D0E8E308308A81985____HasOptions_PropertyInfo,
	Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435_CustomAttributesCacheGenerator_Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435____Parser_PropertyInfo,
	Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435_CustomAttributesCacheGenerator_Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435____Descriptor_PropertyInfo,
	Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435_CustomAttributesCacheGenerator_Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo,
	Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435_CustomAttributesCacheGenerator_Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435____Id_PropertyInfo,
	Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435_CustomAttributesCacheGenerator_Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435____HasId_PropertyInfo,
	Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435_CustomAttributesCacheGenerator_Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435____X_PropertyInfo,
	Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435_CustomAttributesCacheGenerator_Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435____HasX_PropertyInfo,
	Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435_CustomAttributesCacheGenerator_Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435____Y_PropertyInfo,
	Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435_CustomAttributesCacheGenerator_Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435____HasY_PropertyInfo,
	Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435_CustomAttributesCacheGenerator_Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435____Rotation_PropertyInfo,
	Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435_CustomAttributesCacheGenerator_Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435____HasRotation_PropertyInfo,
	Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435_CustomAttributesCacheGenerator_Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435____Scale_PropertyInfo,
	Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435_CustomAttributesCacheGenerator_Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435____HasScale_PropertyInfo,
	Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435_CustomAttributesCacheGenerator_Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435____RenderId_PropertyInfo,
	Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435_CustomAttributesCacheGenerator_Sticker_t69F9AEC365D4FA58649256A9814DDAF8FFBD2435____HasRenderId_PropertyInfo,
	StickerRoll_t5CCFC6B2292625CF867C2C935AB4749B8FA97D59_CustomAttributesCacheGenerator_StickerRoll_t5CCFC6B2292625CF867C2C935AB4749B8FA97D59____Parser_PropertyInfo,
	StickerRoll_t5CCFC6B2292625CF867C2C935AB4749B8FA97D59_CustomAttributesCacheGenerator_StickerRoll_t5CCFC6B2292625CF867C2C935AB4749B8FA97D59____Descriptor_PropertyInfo,
	StickerRoll_t5CCFC6B2292625CF867C2C935AB4749B8FA97D59_CustomAttributesCacheGenerator_StickerRoll_t5CCFC6B2292625CF867C2C935AB4749B8FA97D59____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo,
	StickerRoll_t5CCFC6B2292625CF867C2C935AB4749B8FA97D59_CustomAttributesCacheGenerator_StickerRoll_t5CCFC6B2292625CF867C2C935AB4749B8FA97D59____Sticker_PropertyInfo,
	InputStreamHandlerConfig_t1BAFAC8382A20E901CDE2D3D7E86E757DD5E9D16_CustomAttributesCacheGenerator_InputStreamHandlerConfig_t1BAFAC8382A20E901CDE2D3D7E86E757DD5E9D16____Parser_PropertyInfo,
	InputStreamHandlerConfig_t1BAFAC8382A20E901CDE2D3D7E86E757DD5E9D16_CustomAttributesCacheGenerator_InputStreamHandlerConfig_t1BAFAC8382A20E901CDE2D3D7E86E757DD5E9D16____Descriptor_PropertyInfo,
	InputStreamHandlerConfig_t1BAFAC8382A20E901CDE2D3D7E86E757DD5E9D16_CustomAttributesCacheGenerator_InputStreamHandlerConfig_t1BAFAC8382A20E901CDE2D3D7E86E757DD5E9D16____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo,
	InputStreamHandlerConfig_t1BAFAC8382A20E901CDE2D3D7E86E757DD5E9D16_CustomAttributesCacheGenerator_InputStreamHandlerConfig_t1BAFAC8382A20E901CDE2D3D7E86E757DD5E9D16____InputStreamHandler_PropertyInfo,
	InputStreamHandlerConfig_t1BAFAC8382A20E901CDE2D3D7E86E757DD5E9D16_CustomAttributesCacheGenerator_InputStreamHandlerConfig_t1BAFAC8382A20E901CDE2D3D7E86E757DD5E9D16____HasInputStreamHandler_PropertyInfo,
	InputStreamHandlerConfig_t1BAFAC8382A20E901CDE2D3D7E86E757DD5E9D16_CustomAttributesCacheGenerator_InputStreamHandlerConfig_t1BAFAC8382A20E901CDE2D3D7E86E757DD5E9D16____Options_PropertyInfo,
	InputStreamHandlerConfig_t1BAFAC8382A20E901CDE2D3D7E86E757DD5E9D16_CustomAttributesCacheGenerator_InputStreamHandlerConfig_t1BAFAC8382A20E901CDE2D3D7E86E757DD5E9D16____HasOptions_PropertyInfo,
	OutputStreamHandlerConfig_t472506B1EB46A165DEDFD74B41B42728C003617D_CustomAttributesCacheGenerator_OutputStreamHandlerConfig_t472506B1EB46A165DEDFD74B41B42728C003617D____Parser_PropertyInfo,
	OutputStreamHandlerConfig_t472506B1EB46A165DEDFD74B41B42728C003617D_CustomAttributesCacheGenerator_OutputStreamHandlerConfig_t472506B1EB46A165DEDFD74B41B42728C003617D____Descriptor_PropertyInfo,
	OutputStreamHandlerConfig_t472506B1EB46A165DEDFD74B41B42728C003617D_CustomAttributesCacheGenerator_OutputStreamHandlerConfig_t472506B1EB46A165DEDFD74B41B42728C003617D____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo,
	OutputStreamHandlerConfig_t472506B1EB46A165DEDFD74B41B42728C003617D_CustomAttributesCacheGenerator_OutputStreamHandlerConfig_t472506B1EB46A165DEDFD74B41B42728C003617D____OutputStreamHandler_PropertyInfo,
	OutputStreamHandlerConfig_t472506B1EB46A165DEDFD74B41B42728C003617D_CustomAttributesCacheGenerator_OutputStreamHandlerConfig_t472506B1EB46A165DEDFD74B41B42728C003617D____HasOutputStreamHandler_PropertyInfo,
	OutputStreamHandlerConfig_t472506B1EB46A165DEDFD74B41B42728C003617D_CustomAttributesCacheGenerator_OutputStreamHandlerConfig_t472506B1EB46A165DEDFD74B41B42728C003617D____InputSidePacket_PropertyInfo,
	OutputStreamHandlerConfig_t472506B1EB46A165DEDFD74B41B42728C003617D_CustomAttributesCacheGenerator_OutputStreamHandlerConfig_t472506B1EB46A165DEDFD74B41B42728C003617D____Options_PropertyInfo,
	OutputStreamHandlerConfig_t472506B1EB46A165DEDFD74B41B42728C003617D_CustomAttributesCacheGenerator_OutputStreamHandlerConfig_t472506B1EB46A165DEDFD74B41B42728C003617D____HasOptions_PropertyInfo,
	FaceGeometry_t7B246BEE497814C69CD0E81B2116D2C9746BF305_CustomAttributesCacheGenerator_FaceGeometry_t7B246BEE497814C69CD0E81B2116D2C9746BF305____Parser_PropertyInfo,
	FaceGeometry_t7B246BEE497814C69CD0E81B2116D2C9746BF305_CustomAttributesCacheGenerator_FaceGeometry_t7B246BEE497814C69CD0E81B2116D2C9746BF305____Descriptor_PropertyInfo,
	FaceGeometry_t7B246BEE497814C69CD0E81B2116D2C9746BF305_CustomAttributesCacheGenerator_FaceGeometry_t7B246BEE497814C69CD0E81B2116D2C9746BF305____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo,
	FaceGeometry_t7B246BEE497814C69CD0E81B2116D2C9746BF305_CustomAttributesCacheGenerator_FaceGeometry_t7B246BEE497814C69CD0E81B2116D2C9746BF305____Mesh_PropertyInfo,
	FaceGeometry_t7B246BEE497814C69CD0E81B2116D2C9746BF305_CustomAttributesCacheGenerator_FaceGeometry_t7B246BEE497814C69CD0E81B2116D2C9746BF305____HasMesh_PropertyInfo,
	FaceGeometry_t7B246BEE497814C69CD0E81B2116D2C9746BF305_CustomAttributesCacheGenerator_FaceGeometry_t7B246BEE497814C69CD0E81B2116D2C9746BF305____PoseTransformMatrix_PropertyInfo,
	FaceGeometry_t7B246BEE497814C69CD0E81B2116D2C9746BF305_CustomAttributesCacheGenerator_FaceGeometry_t7B246BEE497814C69CD0E81B2116D2C9746BF305____HasPoseTransformMatrix_PropertyInfo,
	Mesh3d_t038DF3815E8BB8AC98EE930DFAA9C27D5A3AE2FA_CustomAttributesCacheGenerator_Mesh3d_t038DF3815E8BB8AC98EE930DFAA9C27D5A3AE2FA____Parser_PropertyInfo,
	Mesh3d_t038DF3815E8BB8AC98EE930DFAA9C27D5A3AE2FA_CustomAttributesCacheGenerator_Mesh3d_t038DF3815E8BB8AC98EE930DFAA9C27D5A3AE2FA____Descriptor_PropertyInfo,
	Mesh3d_t038DF3815E8BB8AC98EE930DFAA9C27D5A3AE2FA_CustomAttributesCacheGenerator_Mesh3d_t038DF3815E8BB8AC98EE930DFAA9C27D5A3AE2FA____pbU3AU3AGoogle_Protobuf_IMessage_Descriptor_PropertyInfo,
	Mesh3d_t038DF3815E8BB8AC98EE930DFAA9C27D5A3AE2FA_CustomAttributesCacheGenerator_Mesh3d_t038DF3815E8BB8AC98EE930DFAA9C27D5A3AE2FA____VertexType_PropertyInfo,
	Mesh3d_t038DF3815E8BB8AC98EE930DFAA9C27D5A3AE2FA_CustomAttributesCacheGenerator_Mesh3d_t038DF3815E8BB8AC98EE930DFAA9C27D5A3AE2FA____HasVertexType_PropertyInfo,
	Mesh3d_t038DF3815E8BB8AC98EE930DFAA9C27D5A3AE2FA_CustomAttributesCacheGenerator_Mesh3d_t038DF3815E8BB8AC98EE930DFAA9C27D5A3AE2FA____PrimitiveType_PropertyInfo,
	Mesh3d_t038DF3815E8BB8AC98EE930DFAA9C27D5A3AE2FA_CustomAttributesCacheGenerator_Mesh3d_t038DF3815E8BB8AC98EE930DFAA9C27D5A3AE2FA____HasPrimitiveType_PropertyInfo,
	Mesh3d_t038DF3815E8BB8AC98EE930DFAA9C27D5A3AE2FA_CustomAttributesCacheGenerator_Mesh3d_t038DF3815E8BB8AC98EE930DFAA9C27D5A3AE2FA____VertexBuffer_PropertyInfo,
	Mesh3d_t038DF3815E8BB8AC98EE930DFAA9C27D5A3AE2FA_CustomAttributesCacheGenerator_Mesh3d_t038DF3815E8BB8AC98EE930DFAA9C27D5A3AE2FA____IndexBuffer_PropertyInfo,
	MediaPipeSDK_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}
