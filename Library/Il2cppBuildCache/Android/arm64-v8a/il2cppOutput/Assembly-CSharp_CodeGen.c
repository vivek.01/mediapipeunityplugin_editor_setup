﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"


extern const RuntimeMethod* DemoGraph_PushInputInGlContext_m2000278CAE8C8F4281A45E55684BD7498AEE66E7_RuntimeMethod_var;
extern const RuntimeMethod* InstantMotionTrackingGraph_MatrixCallback_mC846E6B8E080F5495EE9AB511DA6C35EFBC65C6D_RuntimeMethod_var;
extern const RuntimeMethod* OfficialDemoGraph_BuildDestination_m8A2E396648876205DF379CDB3B2C198AB6A5E70D_RuntimeMethod_var;
extern const RuntimeMethod* OfficialDemoGraph_OnReleaseDestinationTexture_mC9040C05CB2B2BC7C3D8126BCCFD6FF9B24B199D_RuntimeMethod_var;
extern const RuntimeMethod* SceneDirectors_GetCurrentContext_mCCE71B73479FC8A1F8D65066ED794FE54AF5401C_RuntimeMethod_var;
extern const RuntimeMethod* TextureFramePool_OnTextureFrameRelease_m3F46346F6E43543DD9BE828528E788F0066D9B6D_RuntimeMethod_var;



// 0x00000001 System.Void BoxTrackingGraph::PrepareDependentAssets()
extern void BoxTrackingGraph_PrepareDependentAssets_m05EE28DA9847DD92864711C314FA859BC1604D8B (void);
// 0x00000002 System.Void BoxTrackingGraph::.ctor()
extern void BoxTrackingGraph__ctor_m1F924AEA97048C003343847B748ECE447B0D5C14 (void);
// 0x00000003 Mediapipe.Status FaceDetectionGraph::StartRun()
extern void FaceDetectionGraph_StartRun_m4C9EC328DA283D7DF4166322D028FB90F56C8F0F (void);
// 0x00000004 System.Void FaceDetectionGraph::RenderOutput(WebCamScreenController,TextureFrame)
extern void FaceDetectionGraph_RenderOutput_m636FCE4A0542B4EB15A8006D050C83A876C8F02A (void);
// 0x00000005 System.Boolean FaceDetectionGraph::FetchNextFaceDetectionsPresence()
extern void FaceDetectionGraph_FetchNextFaceDetectionsPresence_mB8F34BF749B84CBE3FB6B1646C7395E3578EDBD8 (void);
// 0x00000006 System.Collections.Generic.List`1<Mediapipe.Detection> FaceDetectionGraph::FetchNextFaceDetections()
extern void FaceDetectionGraph_FetchNextFaceDetections_m28848AE7CE7FC056EA9A892D1EE5E6BDF602A29F (void);
// 0x00000007 System.Void FaceDetectionGraph::RenderAnnotation(WebCamScreenController,System.Collections.Generic.List`1<Mediapipe.Detection>)
extern void FaceDetectionGraph_RenderAnnotation_mAD9D6B8CD827468770C21825CE71220648566F3F (void);
// 0x00000008 System.Void FaceDetectionGraph::PrepareDependentAssets()
extern void FaceDetectionGraph_PrepareDependentAssets_mD50A5EAD1B3D925B360DB5900D720EFD04D5DA53 (void);
// 0x00000009 System.Void FaceDetectionGraph::.ctor()
extern void FaceDetectionGraph__ctor_m4AD69744309E2963FE3472DA01BAACC7925F8D2B (void);
// 0x0000000A Mediapipe.Status FaceMeshGraph::StartRun()
extern void FaceMeshGraph_StartRun_m8FBF2282FBFDB0CAB4958F9760CFF6DB7C727676 (void);
// 0x0000000B System.Void FaceMeshGraph::RenderOutput(WebCamScreenController,TextureFrame)
extern void FaceMeshGraph_RenderOutput_m4254F5F13CB92C5973D3A3368930A03690002088 (void);
// 0x0000000C FaceMeshValue FaceMeshGraph::FetchNextFaceMeshValue()
extern void FaceMeshGraph_FetchNextFaceMeshValue_m2D19113DFFAEA3959DF3D81D74BC80F28DC9B7FF (void);
// 0x0000000D System.Boolean FaceMeshGraph::FetchNextMultiFaceLandmarksPresence()
extern void FaceMeshGraph_FetchNextMultiFaceLandmarksPresence_m40F6730E413758E20C88E4966B5C3F59F7DC5826 (void);
// 0x0000000E System.Collections.Generic.List`1<Mediapipe.NormalizedLandmarkList> FaceMeshGraph::FetchNextMultiFaceLandmarks()
extern void FaceMeshGraph_FetchNextMultiFaceLandmarks_m19A423812E005DBA773429DB7D6F9E5335E63F30 (void);
// 0x0000000F System.Collections.Generic.List`1<Mediapipe.NormalizedRect> FaceMeshGraph::FetchNextFaceRectsFromLandmarks()
extern void FaceMeshGraph_FetchNextFaceRectsFromLandmarks_mE1C04E004DCA2E89C961B115BACD5342F68DFF94 (void);
// 0x00000010 System.Boolean FaceMeshGraph::FetchNextFaceDetectionsPresence()
extern void FaceMeshGraph_FetchNextFaceDetectionsPresence_m4C4B5B85FA8EA8556A3DF8B97C3FAA9C644D9D76 (void);
// 0x00000011 System.Collections.Generic.List`1<Mediapipe.Detection> FaceMeshGraph::FetchNextFaceDetections()
extern void FaceMeshGraph_FetchNextFaceDetections_mB6BF818DC0FB6592D475BB639CB094B6701DB387 (void);
// 0x00000012 System.Void FaceMeshGraph::RenderAnnotation(WebCamScreenController,FaceMeshValue)
extern void FaceMeshGraph_RenderAnnotation_m1DB0A81D2A9CFBA8066CC1C3D8202EA75FA52205 (void);
// 0x00000013 System.Void FaceMeshGraph::PrepareDependentAssets()
extern void FaceMeshGraph_PrepareDependentAssets_m294A4DA923760F74783AED263C03A78BFC949F13 (void);
// 0x00000014 System.Void FaceMeshGraph::.ctor()
extern void FaceMeshGraph__ctor_m43E53DD64DE931567D66B9AD018909A4D71ABC65 (void);
// 0x00000015 System.Void FaceMeshValue::.ctor(System.Collections.Generic.List`1<Mediapipe.NormalizedLandmarkList>,System.Collections.Generic.List`1<Mediapipe.NormalizedRect>,System.Collections.Generic.List`1<Mediapipe.Detection>)
extern void FaceMeshValue__ctor_mFC39E40B2619885A26F478916AA27649B8662A54 (void);
// 0x00000016 System.Void FaceMeshValue::.ctor(System.Collections.Generic.List`1<Mediapipe.NormalizedLandmarkList>,System.Collections.Generic.List`1<Mediapipe.NormalizedRect>)
extern void FaceMeshValue__ctor_m95AE9C33402D7A75EB7E2DB5EA77CD8AE20B5441 (void);
// 0x00000017 System.Void FaceMeshValue::.ctor()
extern void FaceMeshValue__ctor_m6A233558A47A78EB07E2357962C5BAF4FED097AB (void);
// 0x00000018 System.Void MultiFaceLandmarkListAnnotationController::Draw(UnityEngine.Transform,System.Collections.Generic.List`1<Mediapipe.NormalizedLandmarkList>,System.Boolean)
extern void MultiFaceLandmarkListAnnotationController_Draw_m6BCC424733D3B71D37BF2DB3EDCF7DEE911A5449 (void);
// 0x00000019 System.Void MultiFaceLandmarkListAnnotationController::.ctor()
extern void MultiFaceLandmarkListAnnotationController__ctor_m85BEE397C269389D174E5B4F0D58E37CB6C81CFC (void);
// 0x0000001A Mediapipe.Status HairSegmentationGraph::StartRun()
extern void HairSegmentationGraph_StartRun_mCE6B8980E63427A47520897D2CD62F2F4A924C1A (void);
// 0x0000001B System.Void HairSegmentationGraph::RenderOutput(WebCamScreenController,TextureFrame)
extern void HairSegmentationGraph_RenderOutput_m5A9647CA4ED55B4C926F09C15B15D35B60783F7D (void);
// 0x0000001C Mediapipe.ImageFrame HairSegmentationGraph::FetchNextHairMask()
extern void HairSegmentationGraph_FetchNextHairMask_mEA29737479B86679C7A2C5027C36A3D0706E6417 (void);
// 0x0000001D System.Void HairSegmentationGraph::RenderAnnotation(WebCamScreenController,Mediapipe.ImageFrame)
extern void HairSegmentationGraph_RenderAnnotation_m42157BA41DEDC645EE8E4A283919842E62BDA594 (void);
// 0x0000001E System.Void HairSegmentationGraph::PrepareDependentAssets()
extern void HairSegmentationGraph_PrepareDependentAssets_m23CB676C5758F2EB911D0AB2DF526367871719C2 (void);
// 0x0000001F System.Void HairSegmentationGraph::.ctor()
extern void HairSegmentationGraph__ctor_m30D9B4BD759E5F9F4DC712DF01B0211E3B8F7F6E (void);
// 0x00000020 System.Void HandTrackingAnnotationController::Awake()
extern void HandTrackingAnnotationController_Awake_m198BAFA00284FB41F83344F3E5176047C7F7F8C9 (void);
// 0x00000021 System.Void HandTrackingAnnotationController::OnDestroy()
extern void HandTrackingAnnotationController_OnDestroy_m9B27802FEF8EF883F894737BD0F58AF20ACA1704 (void);
// 0x00000022 System.Void HandTrackingAnnotationController::Clear()
extern void HandTrackingAnnotationController_Clear_m9C0901D913DA0AE6675954ECC4A2FBC8C0C2D4D5 (void);
// 0x00000023 System.Void HandTrackingAnnotationController::Draw(UnityEngine.Transform,System.Collections.Generic.List`1<Mediapipe.NormalizedLandmarkList>,System.Collections.Generic.List`1<Mediapipe.ClassificationList>,System.Collections.Generic.List`1<Mediapipe.Detection>,System.Collections.Generic.List`1<Mediapipe.NormalizedRect>,System.Boolean)
extern void HandTrackingAnnotationController_Draw_m13C67CED8F296622F9D365B7298B501882465AB9 (void);
// 0x00000024 System.Void HandTrackingAnnotationController::.ctor()
extern void HandTrackingAnnotationController__ctor_mC980D10DF166E35BD4C931BA554D3EA9D2941D9D (void);
// 0x00000025 Mediapipe.Status HandTrackingGraph::StartRun()
extern void HandTrackingGraph_StartRun_m27E20BEE05795279C04D220E23B37361CD331B76 (void);
// 0x00000026 System.Void HandTrackingGraph::RenderOutput(WebCamScreenController,TextureFrame)
extern void HandTrackingGraph_RenderOutput_mEA7097A99F5189AB98693FEEC137DE53777E1DAB (void);
// 0x00000027 HandTrackingValue HandTrackingGraph::FetchNextHandTrackingValue()
extern void HandTrackingGraph_FetchNextHandTrackingValue_mADAAA040A00929AC0CA0F5D7F7FC7CFE2511D977 (void);
// 0x00000028 System.Collections.Generic.List`1<Mediapipe.ClassificationList> HandTrackingGraph::FetchNextHandednesses()
extern void HandTrackingGraph_FetchNextHandednesses_m4A5063894719F0077B194EA593AEA6F8C5B479D5 (void);
// 0x00000029 System.Collections.Generic.List`1<Mediapipe.NormalizedRect> HandTrackingGraph::FetchNextPalmRects()
extern void HandTrackingGraph_FetchNextPalmRects_m4915CDCD124B7257542AFFA078EEAC2DDC9EDB75 (void);
// 0x0000002A System.Collections.Generic.List`1<Mediapipe.NormalizedLandmarkList> HandTrackingGraph::FetchNextHandLandmarks()
extern void HandTrackingGraph_FetchNextHandLandmarks_mC8FFBB34EE4F7EB57765EF44B4BCAFC260E49721 (void);
// 0x0000002B System.Boolean HandTrackingGraph::FetchNextHandLandmarksPresence()
extern void HandTrackingGraph_FetchNextHandLandmarksPresence_m64DEC8EA11773720C40261C1DCB9BCE6263A514E (void);
// 0x0000002C System.Boolean HandTrackingGraph::FetchNextPalmDetectionsPresence()
extern void HandTrackingGraph_FetchNextPalmDetectionsPresence_mF39C54AC1FA0DD1F00D35CAA52EF5895F46FC63E (void);
// 0x0000002D System.Collections.Generic.List`1<Mediapipe.Detection> HandTrackingGraph::FetchNextPalmDetections()
extern void HandTrackingGraph_FetchNextPalmDetections_m1B4F2746E00A46A7FC9F0982F11B7A241D5F7955 (void);
// 0x0000002E System.Void HandTrackingGraph::RenderAnnotation(WebCamScreenController,HandTrackingValue)
extern void HandTrackingGraph_RenderAnnotation_mEB8A66095D376FD0E2990A00029DD658132A3B12 (void);
// 0x0000002F System.Void HandTrackingGraph::PrepareDependentAssets()
extern void HandTrackingGraph_PrepareDependentAssets_m62CDE28125E1FCDC09EAB929D638DDE775F54E23 (void);
// 0x00000030 System.Void HandTrackingGraph::.ctor()
extern void HandTrackingGraph__ctor_m348829BB1A1F463C698A924BC45EF6366F2FEC35 (void);
// 0x00000031 System.Void HandTrackingValue::.ctor(System.Collections.Generic.List`1<Mediapipe.NormalizedLandmarkList>,System.Collections.Generic.List`1<Mediapipe.ClassificationList>,System.Collections.Generic.List`1<Mediapipe.Detection>,System.Collections.Generic.List`1<Mediapipe.NormalizedRect>)
extern void HandTrackingValue__ctor_mE27A4A78AAFFACB7445F7A85544F1702C450A6AB (void);
// 0x00000032 System.Void MultiHandLandmarkListAnnotationController::Draw(UnityEngine.Transform,System.Collections.Generic.List`1<Mediapipe.NormalizedLandmarkList>,System.Boolean)
extern void MultiHandLandmarkListAnnotationController_Draw_m227CE67F22F1D0DB0E5F71505259DD47640E95F1 (void);
// 0x00000033 System.Void MultiHandLandmarkListAnnotationController::.ctor()
extern void MultiHandLandmarkListAnnotationController__ctor_m0D3902B0E52D175719DF844CA50A3B1A2E01588D (void);
// 0x00000034 System.Void HelloWorld::OnEnable()
extern void HelloWorld_OnEnable_m6AFEE56E7F9980EE178909D98C0354F657772FBF (void);
// 0x00000035 System.Void HelloWorld::OnDestroy()
extern void HelloWorld_OnDestroy_m67F8DDC62429D3CADD2140FF2BF75136EBB7149E (void);
// 0x00000036 System.Void HelloWorld::OnDisable()
extern void HelloWorld_OnDisable_mA1048B3556E33611481F597AAE2FC06B2EC3AE6D (void);
// 0x00000037 System.Void HelloWorld::Start()
extern void HelloWorld_Start_m5BDA2C486D048B975ACDD0F4F1402960153FA64E (void);
// 0x00000038 System.Collections.IEnumerator HelloWorld::RunGraph()
extern void HelloWorld_RunGraph_mEC050C49115FEC8EDB2DA7CEC4CDC6786D220FE9 (void);
// 0x00000039 System.Void HelloWorld::.ctor()
extern void HelloWorld__ctor_mBA31A819FA7B1D67B696BD1C8B7339E6BB053723 (void);
// 0x0000003A System.Void HelloWorld/<RunGraph>d__7::.ctor(System.Int32)
extern void U3CRunGraphU3Ed__7__ctor_m3DFA86D230949C52467903E06DD7147BEC7CD009 (void);
// 0x0000003B System.Void HelloWorld/<RunGraph>d__7::System.IDisposable.Dispose()
extern void U3CRunGraphU3Ed__7_System_IDisposable_Dispose_m274C2E9415C732F3D596F53C22C7F0FA82F94094 (void);
// 0x0000003C System.Boolean HelloWorld/<RunGraph>d__7::MoveNext()
extern void U3CRunGraphU3Ed__7_MoveNext_m3BB5CB4B3D84C4D1C373E35A0B3CD8D50D4719E0 (void);
// 0x0000003D System.Object HelloWorld/<RunGraph>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRunGraphU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC95A3572B36E5FA9CAB03A0B7412AA63218F2CB2 (void);
// 0x0000003E System.Void HelloWorld/<RunGraph>d__7::System.Collections.IEnumerator.Reset()
extern void U3CRunGraphU3Ed__7_System_Collections_IEnumerator_Reset_m301ADA817F03B03DA7898E44710ACCA8220EC79F (void);
// 0x0000003F System.Object HelloWorld/<RunGraph>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CRunGraphU3Ed__7_System_Collections_IEnumerator_get_Current_mFCF11A73BC49AE0753C24A27BABB665A8E2171D3 (void);
// 0x00000040 System.Void HelloWorldGraph::Initialize()
extern void HelloWorldGraph_Initialize_mB8C04A91B7F773DF6C059DEC5470D4486B43FB56 (void);
// 0x00000041 System.Void HelloWorldGraph::Initialize(Mediapipe.GpuResources,Mediapipe.GlCalculatorHelper)
extern void HelloWorldGraph_Initialize_m595AAC24B1E935F3E9BA1BB658E9B8724B72C322 (void);
// 0x00000042 Mediapipe.Status HelloWorldGraph::StartRun()
extern void HelloWorldGraph_StartRun_m19CB956409000DEFD9B3110D542330A6C6C058DF (void);
// 0x00000043 Mediapipe.Status HelloWorldGraph::StartRun(UnityEngine.Texture)
extern void HelloWorldGraph_StartRun_m8A3AF566B0070B654900EC06F09BE18DBCD8ECD6 (void);
// 0x00000044 Mediapipe.Status HelloWorldGraph::PushInput(System.String)
extern void HelloWorldGraph_PushInput_m04D33B0645CF73E91664A52C399DC06727A100E0 (void);
// 0x00000045 System.Void HelloWorldGraph::RenderOutput(WebCamScreenController,System.String)
extern void HelloWorldGraph_RenderOutput_m7B30F29BD10D048671EF5AB746A8B27DC3AA269B (void);
// 0x00000046 System.Void HelloWorldGraph::Stop()
extern void HelloWorldGraph_Stop_mADFE3EA726C4653EACD1B0FD932A063EC21A4904 (void);
// 0x00000047 System.Void HelloWorldGraph::.ctor()
extern void HelloWorldGraph__ctor_m44F353825068CA222F325603F7CB9458AF7F5E4B (void);
// 0x00000048 System.Void HolisticAnnotationController::Awake()
extern void HolisticAnnotationController_Awake_mC365D079C7EBB9EB1C506626BDDC1BE3B8A88C3E (void);
// 0x00000049 System.Void HolisticAnnotationController::OnDestroy()
extern void HolisticAnnotationController_OnDestroy_mD506D081F43376BC9001162882F48BA4E4C1BA03 (void);
// 0x0000004A System.Void HolisticAnnotationController::ClearIrisAnnotations()
extern void HolisticAnnotationController_ClearIrisAnnotations_mBE35D2226E99BB831AF3B8731C6C90FB412E7AC3 (void);
// 0x0000004B System.Void HolisticAnnotationController::Clear()
extern void HolisticAnnotationController_Clear_mE09E7EB6A1FD6B53379478032668D28442F9938F (void);
// 0x0000004C System.Void HolisticAnnotationController::Draw(UnityEngine.Transform,Mediapipe.NormalizedLandmarkList,Mediapipe.NormalizedRect,Mediapipe.Detection,Mediapipe.NormalizedLandmarkList,Mediapipe.NormalizedLandmarkList,Mediapipe.NormalizedLandmarkList,System.Boolean)
extern void HolisticAnnotationController_Draw_mDFECD1E821A5798CAB4FC454B191315D6F15A18A (void);
// 0x0000004D System.Collections.Generic.IList`1<Mediapipe.NormalizedLandmark> HolisticAnnotationController::GetIrisLandmarks(Mediapipe.NormalizedLandmarkList,HolisticAnnotationController/Side)
extern void HolisticAnnotationController_GetIrisLandmarks_mC27C93139318881288CDCCC11B3CC3A7E517ED39 (void);
// 0x0000004E System.Void HolisticAnnotationController::.ctor()
extern void HolisticAnnotationController__ctor_m8135EB93782202E61F0D9EC353E6E63E2EECF18F (void);
// 0x0000004F Mediapipe.Status HolisticGraph::StartRun()
extern void HolisticGraph_StartRun_m3AF465C9862A27EE543518BE2033E9D8B3C6E861 (void);
// 0x00000050 System.Void HolisticGraph::RenderOutput(WebCamScreenController,TextureFrame)
extern void HolisticGraph_RenderOutput_m5BDFCEE30E82A17726B67FE21E16BD7DF839D4BD (void);
// 0x00000051 HolisticValue HolisticGraph::FetchNextHolisticValue()
extern void HolisticGraph_FetchNextHolisticValue_m88A3C8C7803BCF1659EDB015875B7BEFC6253EC0 (void);
// 0x00000052 Mediapipe.NormalizedLandmarkList HolisticGraph::FetchNextPoseLandmarks()
extern void HolisticGraph_FetchNextPoseLandmarks_m389992D029187AFB332AC67E9D42AE4FD9893D31 (void);
// 0x00000053 Mediapipe.LandmarkList HolisticGraph::FetchNextPoseWorldLandmarks()
extern void HolisticGraph_FetchNextPoseWorldLandmarks_m4BAAE4DDDF5BEF3FFB6EA04CD0231E78DE99EB4D (void);
// 0x00000054 Mediapipe.NormalizedRect HolisticGraph::FetchNextPoseRoi()
extern void HolisticGraph_FetchNextPoseRoi_m677C49433FC941F6494EBE0F63AA77D14217BE7D (void);
// 0x00000055 Mediapipe.Detection HolisticGraph::FetchNextPoseDetection()
extern void HolisticGraph_FetchNextPoseDetection_m2FDB0F8D7B727DBDCA64A4C089956FF480A58884 (void);
// 0x00000056 Mediapipe.NormalizedLandmarkList HolisticGraph::FetchNextFaceLandmarks()
extern void HolisticGraph_FetchNextFaceLandmarks_m1A3BD8D3D170058A91A79C0234BCC47A24874B6B (void);
// 0x00000057 Mediapipe.NormalizedLandmarkList HolisticGraph::FetchNextLeftHandLandmarks()
extern void HolisticGraph_FetchNextLeftHandLandmarks_mBBE573B692D7E1E73E6FFAF31F1534F7CE9AB2B1 (void);
// 0x00000058 Mediapipe.NormalizedLandmarkList HolisticGraph::FetchNextRightHandLandmarks()
extern void HolisticGraph_FetchNextRightHandLandmarks_mFCFFCA84C646DF19222C31F471400CE403D60769 (void);
// 0x00000059 System.Boolean HolisticGraph::FetchNextPoseLandmarksPresence()
extern void HolisticGraph_FetchNextPoseLandmarksPresence_mCF41735F44F6FC34EBEFF72E4BB1BF1B9CAFB90B (void);
// 0x0000005A System.Boolean HolisticGraph::FetchNextPoseRoiPresence()
extern void HolisticGraph_FetchNextPoseRoiPresence_m6B70B0D47FD067C229F2E6C8C30ED3AF5646E793 (void);
// 0x0000005B System.Boolean HolisticGraph::FetchNextPoseDetectionPresence()
extern void HolisticGraph_FetchNextPoseDetectionPresence_m58B111E2433FA4470DDA21DC1CBF033142CD0340 (void);
// 0x0000005C System.Boolean HolisticGraph::FetchNextFaceLandmarksPresence()
extern void HolisticGraph_FetchNextFaceLandmarksPresence_mA1FE2E3A30E8113B6E327863D253FC56034A9E42 (void);
// 0x0000005D System.Boolean HolisticGraph::FetchNextLeftHandLandmarksPresence()
extern void HolisticGraph_FetchNextLeftHandLandmarksPresence_m7ADDB03B13B1448BF73C99A82F2958C24E941666 (void);
// 0x0000005E System.Boolean HolisticGraph::FetchNextRightHandLandmarksPresence()
extern void HolisticGraph_FetchNextRightHandLandmarksPresence_m22E186A2AE47B8ED02C316BA9B14038BF656A33D (void);
// 0x0000005F System.Void HolisticGraph::RenderAnnotation(WebCamScreenController,HolisticValue)
extern void HolisticGraph_RenderAnnotation_m4974D09C2EDC6FD5314148FF29C154332403B1BA (void);
// 0x00000060 System.Void HolisticGraph::PrepareDependentAssets()
extern void HolisticGraph_PrepareDependentAssets_m23E3BC5356EBFF85A0B683359665CD8D752992F0 (void);
// 0x00000061 System.Void HolisticGraph::.ctor()
extern void HolisticGraph__ctor_m42CB0A1C84ED44455B407D0F8E0AB7364664D99B (void);
// 0x00000062 System.Void HolisticValue::.ctor(Mediapipe.NormalizedLandmarkList,Mediapipe.NormalizedRect,Mediapipe.Detection,Mediapipe.NormalizedLandmarkList,Mediapipe.NormalizedLandmarkList,Mediapipe.NormalizedLandmarkList)
extern void HolisticValue__ctor_mC75FD6F6CC3584DB53A691941CE65B3DC585C42F (void);
// 0x00000063 System.Void InstantMotionTrackingGraph::Start()
extern void InstantMotionTrackingGraph_Start_m6B7D35C57B7A83FC0E599F187B87DB77DCD07F72 (void);
// 0x00000064 System.Void InstantMotionTrackingGraph::Update()
extern void InstantMotionTrackingGraph_Update_m100CC554C0AB2FDB6B6756C254D67D8D17C1CEEF (void);
// 0x00000065 Mediapipe.Status InstantMotionTrackingGraph::StartRun(UnityEngine.Texture)
extern void InstantMotionTrackingGraph_StartRun_mD52137BFAB5A216A5ADE1C62225C98A6A864460F (void);
// 0x00000066 Mediapipe.Status InstantMotionTrackingGraph::PushInput(TextureFrame)
extern void InstantMotionTrackingGraph_PushInput_mD405B0097BE5CBCB5AEEADE00061AF4D425718B6 (void);
// 0x00000067 System.IntPtr InstantMotionTrackingGraph::MatrixCallback(System.IntPtr)
extern void InstantMotionTrackingGraph_MatrixCallback_mC846E6B8E080F5495EE9AB511DA6C35EFBC65C6D (void);
// 0x00000068 UnityEngine.Matrix4x4 InstantMotionTrackingGraph::Matrix4x4FromBytes(System.Collections.Generic.IList`1<System.Single>)
extern void InstantMotionTrackingGraph_Matrix4x4FromBytes_mE0EB0340C7ECB48AAC14F50959DF348826CD7881 (void);
// 0x00000069 System.Single InstantMotionTrackingGraph::GetVerticalFovRadians()
extern void InstantMotionTrackingGraph_GetVerticalFovRadians_m85820F4FBFCD635331401289BBD9FD1B920C8AC2 (void);
// 0x0000006A Mediapipe.ImageFrame InstantMotionTrackingGraph::GetImageFrameFromImage(UnityEngine.TextAsset)
extern void InstantMotionTrackingGraph_GetImageFrameFromImage_m3A12662C545CCC06DC90AA1118D7F27E26CE0011 (void);
// 0x0000006B System.Void InstantMotionTrackingGraph::ResetSticker(System.Single,System.Single)
extern void InstantMotionTrackingGraph_ResetSticker_m8A515FF66BB7217424902E847EBA816EE5C7205E (void);
// 0x0000006C System.Void InstantMotionTrackingGraph::UpdateImuRotationMatrix(UnityEngine.Gyroscope)
extern void InstantMotionTrackingGraph_UpdateImuRotationMatrix_m74BC0FBA4310E421A1CA1AF6A5FADD098ABFBEEE (void);
// 0x0000006D System.Void InstantMotionTrackingGraph::FlipTexture2D(UnityEngine.Texture2D)
extern void InstantMotionTrackingGraph_FlipTexture2D_mEA2FF99F078D2F0804278E3C12E2ACB0B6AA22EA (void);
// 0x0000006E System.Void InstantMotionTrackingGraph::PrepareDependentAssets()
extern void InstantMotionTrackingGraph_PrepareDependentAssets_m6B9EAFFA09A77A90CAB4BDD76CD0D3FD7F19E52F (void);
// 0x0000006F System.Void InstantMotionTrackingGraph::.ctor()
extern void InstantMotionTrackingGraph__ctor_m60F455C983471F8512D9CCE338BF2E036F1F5BD3 (void);
// 0x00000070 System.Void IrisAnnotationController::Awake()
extern void IrisAnnotationController_Awake_mCBD4F8DB103291BE473D069AB0FE1FF953673268 (void);
// 0x00000071 System.Void IrisAnnotationController::OnDestroy()
extern void IrisAnnotationController_OnDestroy_mC0CA37E3CE952A8341D6C5402856A92321B6F2CC (void);
// 0x00000072 System.Void IrisAnnotationController::Clear()
extern void IrisAnnotationController_Clear_m5600D82164EC6B7A72AE5EAB45B20ACF8659D9AA (void);
// 0x00000073 System.Void IrisAnnotationController::Draw(UnityEngine.Transform,System.Collections.Generic.IList`1<Mediapipe.NormalizedLandmark>,System.Boolean)
extern void IrisAnnotationController_Draw_m1771EED6E49D9678FC19BB91A7B0CF7686B830FC (void);
// 0x00000074 System.Void IrisAnnotationController::DrawIrisCircle(UnityEngine.Transform,System.Collections.Generic.IList`1<Mediapipe.NormalizedLandmark>,System.Boolean)
extern void IrisAnnotationController_DrawIrisCircle_m54EDDA39C32AEF180611BCDED064C168DEA80F72 (void);
// 0x00000075 System.Single IrisAnnotationController::GetIrisRadius(UnityEngine.Transform,Mediapipe.NormalizedLandmark,Mediapipe.NormalizedLandmark,Mediapipe.NormalizedLandmark,Mediapipe.NormalizedLandmark)
extern void IrisAnnotationController_GetIrisRadius_m77C90A814AE7B701899E390194467780A0277CF2 (void);
// 0x00000076 System.Void IrisAnnotationController::.ctor()
extern void IrisAnnotationController__ctor_m6864D88B80AB6E1F0C52B789AA8D8EEC2934F07E (void);
// 0x00000077 Mediapipe.Status IrisTrackingGraph::StartRun()
extern void IrisTrackingGraph_StartRun_mDDF5E8379106CE3CA11149CCE6B018C47A42C91F (void);
// 0x00000078 System.Void IrisTrackingGraph::RenderOutput(WebCamScreenController,TextureFrame)
extern void IrisTrackingGraph_RenderOutput_m7F1F9396964E548BCE27FE22A694427A334E08DE (void);
// 0x00000079 IrisTrackingValue IrisTrackingGraph::FetchNextIrisTrackingValue()
extern void IrisTrackingGraph_FetchNextIrisTrackingValue_mA4209A848C61EB8EC6DE9EDA608DFC997CA14E2B (void);
// 0x0000007A System.Boolean IrisTrackingGraph::FetchNextFaceLandmarksWithIrisPresence()
extern void IrisTrackingGraph_FetchNextFaceLandmarksWithIrisPresence_m567378ED3111EDC748166CA79F4892F75A49AEB1 (void);
// 0x0000007B Mediapipe.NormalizedLandmarkList IrisTrackingGraph::FetchNextFaceLandmarksWithIris()
extern void IrisTrackingGraph_FetchNextFaceLandmarksWithIris_m86D179E7D566A2FE154DC1521EC8BCC60BC974EF (void);
// 0x0000007C Mediapipe.NormalizedRect IrisTrackingGraph::FetchNextFaceRect()
extern void IrisTrackingGraph_FetchNextFaceRect_m595446762254469BB7310C03F1A17DF92760DA66 (void);
// 0x0000007D System.Boolean IrisTrackingGraph::FetchNextFaceDetectionsPresence()
extern void IrisTrackingGraph_FetchNextFaceDetectionsPresence_mEFCCE102F66A0B4F9023DC6139353C3534ECE06F (void);
// 0x0000007E System.Collections.Generic.List`1<Mediapipe.Detection> IrisTrackingGraph::FetchNextFaceDetections()
extern void IrisTrackingGraph_FetchNextFaceDetections_m23B455C0231140E6E32D86791170A6EF907C7295 (void);
// 0x0000007F System.Void IrisTrackingGraph::RenderAnnotation(WebCamScreenController,IrisTrackingValue)
extern void IrisTrackingGraph_RenderAnnotation_m99CA3919854118FCA929F73170BBDBB5FDD97152 (void);
// 0x00000080 System.Void IrisTrackingGraph::PrepareDependentAssets()
extern void IrisTrackingGraph_PrepareDependentAssets_mAF007E19150FE5B76FC8870B815AFB34DAE4B6A1 (void);
// 0x00000081 System.Void IrisTrackingGraph::.ctor()
extern void IrisTrackingGraph__ctor_mF86F5938345EB035FFE594C472D641D282DFD639 (void);
// 0x00000082 System.Void IrisTrackingValue::.ctor(Mediapipe.NormalizedLandmarkList,Mediapipe.NormalizedRect,System.Collections.Generic.List`1<Mediapipe.Detection>)
extern void IrisTrackingValue__ctor_m3F78E2CDF7AC15CD061B0FB735C959574721A745 (void);
// 0x00000083 System.Void IrisTrackingValue::.ctor(Mediapipe.NormalizedLandmarkList,Mediapipe.NormalizedRect)
extern void IrisTrackingValue__ctor_m196EDBB60F4DCA0C4910EC449B9E0F9AC778F15C (void);
// 0x00000084 System.Void IrisTrackingValue::.ctor()
extern void IrisTrackingValue__ctor_m2B8DC9C88267D37FF3EA199563F70903EA770EC5 (void);
// 0x00000085 Mediapipe.Status ObjectDetectionGraph::StartRun()
extern void ObjectDetectionGraph_StartRun_m62A7E7900F4A5B8D2DCA0877D2FDEC65D7E06F47 (void);
// 0x00000086 System.Void ObjectDetectionGraph::RenderOutput(WebCamScreenController,TextureFrame)
extern void ObjectDetectionGraph_RenderOutput_m51C5EF12F378984A6E8D2FDEC77D97ECCF012E03 (void);
// 0x00000087 System.Collections.Generic.List`1<Mediapipe.Detection> ObjectDetectionGraph::FetchNextOutputDetections()
extern void ObjectDetectionGraph_FetchNextOutputDetections_m46FA59531138924694BA2F3A75245036C212E9EC (void);
// 0x00000088 System.Void ObjectDetectionGraph::RenderAnnotation(WebCamScreenController,System.Collections.Generic.List`1<Mediapipe.Detection>)
extern void ObjectDetectionGraph_RenderAnnotation_m0098604F5FB04C68EC148E303BB4097FD03672B6 (void);
// 0x00000089 System.Void ObjectDetectionGraph::PrepareDependentAssets()
extern void ObjectDetectionGraph_PrepareDependentAssets_m7C97A18E1C173DCCF361360CAC622380618CD818 (void);
// 0x0000008A System.Void ObjectDetectionGraph::.ctor()
extern void ObjectDetectionGraph__ctor_m16EFCDA346D904589A632E5477F0A76119F3AE18 (void);
// 0x0000008B Mediapipe.Status ObjectDetection3dGraph::StartRun(UnityEngine.Texture)
extern void ObjectDetection3dGraph_StartRun_mA240ADFC153ED97C5A74AA93C2E78456DC3769B2 (void);
// 0x0000008C Mediapipe.Status ObjectDetection3dGraph::PushInput(TextureFrame)
extern void ObjectDetection3dGraph_PushInput_m6008766FAD2785E931B497F83512E6AC177918CD (void);
// 0x0000008D Mediapipe.ImageFrame ObjectDetection3dGraph::GetImageFrameFromImage(UnityEngine.TextAsset)
extern void ObjectDetection3dGraph_GetImageFrameFromImage_m19CF9F35EDC3E5EF78069194E6129C93F82742F7 (void);
// 0x0000008E System.Void ObjectDetection3dGraph::FlipTexture2D(UnityEngine.Texture2D)
extern void ObjectDetection3dGraph_FlipTexture2D_m089C56342484C7F31E83EEABF41152523DDD3950 (void);
// 0x0000008F System.String ObjectDetection3dGraph::GetAllowedLabels(ObjectDetection3dGraph/Category)
extern void ObjectDetection3dGraph_GetAllowedLabels_m14ED25AF19BFE27CE3DD6AD03CDA6D15845106C0 (void);
// 0x00000090 System.Single[] ObjectDetection3dGraph::GetModelScale(ObjectDetection3dGraph/Category)
extern void ObjectDetection3dGraph_GetModelScale_m80A123D7684EF97F9BB27E5E25411BCC667ACFBE (void);
// 0x00000091 System.Single[] ObjectDetection3dGraph::GetModelTransformation(ObjectDetection3dGraph/Category)
extern void ObjectDetection3dGraph_GetModelTransformation_m2AA0F65B970FA94B34AAFC106142D98181D210B9 (void);
// 0x00000092 System.String ObjectDetection3dGraph::GetObjAssetName(ObjectDetection3dGraph/Category)
extern void ObjectDetection3dGraph_GetObjAssetName_m40BEAB5BBC10E9F1CB8F84C6C8818C7EDEA9B169 (void);
// 0x00000093 System.String ObjectDetection3dGraph::GetModelAssetName(ObjectDetection3dGraph/Category)
extern void ObjectDetection3dGraph_GetModelAssetName_mBFA528CCC8BF9525D69A85B0EC683573FBA4F065 (void);
// 0x00000094 System.Void ObjectDetection3dGraph::PrepareDependentAssets()
extern void ObjectDetection3dGraph_PrepareDependentAssets_m8391FDEF049B0D2346E3EB64D2C23EBC0F37D4B9 (void);
// 0x00000095 System.Void ObjectDetection3dGraph::.ctor()
extern void ObjectDetection3dGraph__ctor_m9F8E2D15BCA161A56B159E644FEDA256566CD364 (void);
// 0x00000096 System.Void OfficialDemoGraph::Initialize()
extern void OfficialDemoGraph_Initialize_mCE1E30992E53B89B3134809E6F435745DFE85A9F (void);
// 0x00000097 Mediapipe.Status OfficialDemoGraph::StartRun()
extern void OfficialDemoGraph_StartRun_m58FEDDCFFC06D4C1C36746EAE6FB1F91C206A826 (void);
// 0x00000098 Mediapipe.Status OfficialDemoGraph::StartRun(UnityEngine.Texture)
extern void OfficialDemoGraph_StartRun_mC9EF60EDC6E347A0EA52462B2902B4B747576263 (void);
// 0x00000099 System.Void OfficialDemoGraph::RenderOutput(WebCamScreenController,TextureFrame)
extern void OfficialDemoGraph_RenderOutput_mD0D09C4A260679E602F50E0CF3E876C1A69EB0B9 (void);
// 0x0000009A System.Void OfficialDemoGraph::SetupOutputPacket(UnityEngine.Texture)
extern void OfficialDemoGraph_SetupOutputPacket_mE4CF689CE4B6663118F56AF719520197E8680E20 (void);
// 0x0000009B System.IntPtr OfficialDemoGraph::BuildDestination()
extern void OfficialDemoGraph_BuildDestination_m8A2E396648876205DF379CDB3B2C198AB6A5E70D (void);
// 0x0000009C System.Void OfficialDemoGraph::OnReleaseDestinationTexture(System.UInt64,System.IntPtr)
extern void OfficialDemoGraph_OnReleaseDestinationTexture_mC9040C05CB2B2BC7C3D8126BCCFD6FF9B24B199D (void);
// 0x0000009D System.Void OfficialDemoGraph::PrepareDependentAssets()
extern void OfficialDemoGraph_PrepareDependentAssets_m01839BFC5F2185A99A54C059AA21A95552FE6F6F (void);
// 0x0000009E System.Void OfficialDemoGraph::.ctor()
extern void OfficialDemoGraph__ctor_m1F421EBB594E8107461FA2687A2EEC44516F6E16 (void);
// 0x0000009F System.Void OfficialDemoGraph/<>c::.cctor()
extern void U3CU3Ec__cctor_m33E315325F655BADAD28838760E49C791F583EAD (void);
// 0x000000A0 System.Void OfficialDemoGraph/<>c::.ctor()
extern void U3CU3Ec__ctor_m91E14F7FC3F9E20F9473106B54EECA4EB72F6CC4 (void);
// 0x000000A1 System.Boolean OfficialDemoGraph/<>c::<Initialize>b__7_0(Mediapipe.CalculatorGraphConfig/Types/Node)
extern void U3CU3Ec_U3CInitializeU3Eb__7_0_m7B0DB7824359E516D3D061B8D2F9A5E6629EB1B4 (void);
// 0x000000A2 System.Void PoseTrackingAnnotationController::Awake()
extern void PoseTrackingAnnotationController_Awake_m711CCDD327DA4DC434A4482B875DFEC9B47833DF (void);
// 0x000000A3 System.Void PoseTrackingAnnotationController::OnDestroy()
extern void PoseTrackingAnnotationController_OnDestroy_mA647A0C518036FD1C95C3923552213ADEB587BC3 (void);
// 0x000000A4 System.Void PoseTrackingAnnotationController::Clear()
extern void PoseTrackingAnnotationController_Clear_mC0D72A4DE44BB85139FBFF42F8EAEE1023ACDA8E (void);
// 0x000000A5 System.Void PoseTrackingAnnotationController::Draw(UnityEngine.Transform,Mediapipe.NormalizedLandmarkList,Mediapipe.Detection,System.Boolean)
extern void PoseTrackingAnnotationController_Draw_m672A79018C521DC19EF92E91925ED33470A16A29 (void);
// 0x000000A6 System.Void PoseTrackingAnnotationController::.ctor()
extern void PoseTrackingAnnotationController__ctor_m63EF25498045010947C00A1A16E24F00406199D8 (void);
// 0x000000A7 Mediapipe.Status PoseTrackingGraph::StartRun()
extern void PoseTrackingGraph_StartRun_m14C2DD7B3A0A8AAF4E011F7C3AA4A12A93E78E32 (void);
// 0x000000A8 System.Void PoseTrackingGraph::RenderOutput(WebCamScreenController,TextureFrame)
extern void PoseTrackingGraph_RenderOutput_mECB1FD783B4954D4B65C6E16CB974CCAB2E9D1F8 (void);
// 0x000000A9 PoseTrackingValue PoseTrackingGraph::FetchNextPoseTrackingValue()
extern void PoseTrackingGraph_FetchNextPoseTrackingValue_m90BCC9EE17D3B5EC08B751C17C678906DD762CAE (void);
// 0x000000AA Mediapipe.NormalizedLandmarkList PoseTrackingGraph::FetchNextPoseLandmarks()
extern void PoseTrackingGraph_FetchNextPoseLandmarks_mD8F03F2C450DEBDFB98E1DDC55BEDA0F5B7B65F7 (void);
// 0x000000AB Mediapipe.LandmarkList PoseTrackingGraph::FetchNextPoseWorldLandmarks()
extern void PoseTrackingGraph_FetchNextPoseWorldLandmarks_m5783D9CA08442AA68308EA76788234936E09ECE5 (void);
// 0x000000AC Mediapipe.Detection PoseTrackingGraph::FetchNextPoseDetection()
extern void PoseTrackingGraph_FetchNextPoseDetection_m582B8CF649AD78EE0C2B7447C309FB188ECA3225 (void);
// 0x000000AD System.Boolean PoseTrackingGraph::FetchNextPoseLandmarksPresence()
extern void PoseTrackingGraph_FetchNextPoseLandmarksPresence_m38BDBD7699857C953E2BAF7A8C399247D40E0AF6 (void);
// 0x000000AE System.Boolean PoseTrackingGraph::FetchNextPoseDetectionPresence()
extern void PoseTrackingGraph_FetchNextPoseDetectionPresence_m1D354F04B2BC51A4D12FCCC55B50D6CCFB70CD14 (void);
// 0x000000AF System.Void PoseTrackingGraph::RenderAnnotation(WebCamScreenController,PoseTrackingValue)
extern void PoseTrackingGraph_RenderAnnotation_m3DA6AA32F70F1ED93F8F28E976B8FEF9B5DE9590 (void);
// 0x000000B0 System.Void PoseTrackingGraph::PrepareDependentAssets()
extern void PoseTrackingGraph_PrepareDependentAssets_mC049BB7935FED0C3E5D84FB509630ACCCB79757D (void);
// 0x000000B1 System.Void PoseTrackingGraph::.ctor()
extern void PoseTrackingGraph__ctor_mC5CC427988CA38D2CD3A28C6C46D47B62A8EE96D (void);
// 0x000000B2 System.Void PoseTrackingValue::.ctor(Mediapipe.NormalizedLandmarkList,Mediapipe.Detection)
extern void PoseTrackingValue__ctor_mA6D1C4B0308B8E4A72D22EF8C983F4316EFEFADD (void);
// 0x000000B3 System.Void PoseTrackingValue::.ctor(Mediapipe.NormalizedLandmarkList)
extern void PoseTrackingValue__ctor_mD3CC38B61D481FBD54EFF3D9B4021DE0BB88BBFA (void);
// 0x000000B4 System.Void PoseTrackingValue::.ctor()
extern void PoseTrackingValue__ctor_mF14BE4257DE1256E556DC4569E78F0FC9EBF9E7C (void);
// 0x000000B5 System.Void DemoGraph::OnEnable()
extern void DemoGraph_OnEnable_mE8A25FFDE70FDB756A52F120C51AB08E6E9167C9 (void);
// 0x000000B6 System.Void DemoGraph::OnDestroy()
extern void DemoGraph_OnDestroy_m81C1A93A5C931FD23902C75A0A6DD841408C2064 (void);
// 0x000000B7 System.Void DemoGraph::Initialize()
extern void DemoGraph_Initialize_m5AE4999C25882FA45DFADA977C00A90780983158 (void);
// 0x000000B8 System.Void DemoGraph::Initialize(Mediapipe.GpuResources,Mediapipe.GlCalculatorHelper)
extern void DemoGraph_Initialize_mEB929274E6A01E61198C30005AAB6A6C1EDF8496 (void);
// 0x000000B9 Mediapipe.Status DemoGraph::StartRun()
// 0x000000BA Mediapipe.Status DemoGraph::StartRun(UnityEngine.Texture)
extern void DemoGraph_StartRun_mDF368E565F1CA198E168B3024368498201D1840D (void);
// 0x000000BB Mediapipe.Status DemoGraph::PushInput(TextureFrame)
extern void DemoGraph_PushInput_mC9D77CEB41EEEFBDF54716F84494D42BB20DA54A (void);
// 0x000000BC System.IntPtr DemoGraph::PushInputInGlContext()
extern void DemoGraph_PushInputInGlContext_m2000278CAE8C8F4281A45E55684BD7498AEE66E7 (void);
// 0x000000BD System.Void DemoGraph::RenderOutput(WebCamScreenController,TextureFrame)
// 0x000000BE System.Void DemoGraph::Stop()
extern void DemoGraph_Stop_mBAE75F98D62479F19617DE292F318831786CE3CF (void);
// 0x000000BF T DemoGraph::FetchNext(Mediapipe.OutputStreamPoller`1<T>,Mediapipe.Packet`1<T>,System.String,T)
// 0x000000C0 System.Collections.Generic.List`1<T> DemoGraph::FetchNextVector(Mediapipe.OutputStreamPoller`1<System.Collections.Generic.List`1<T>>,Mediapipe.Packet`1<System.Collections.Generic.List`1<T>>,System.String)
// 0x000000C1 System.Boolean DemoGraph::IsGpuEnabled()
extern void DemoGraph_IsGpuEnabled_mD652E3C58A96FC837513E32821CC81E3B53E6E3B (void);
// 0x000000C2 UnityEngine.TextAsset DemoGraph::GetConfig()
extern void DemoGraph_GetConfig_m11C8089683027904D506742D9F1BD707312A2B1D (void);
// 0x000000C3 Mediapipe.Timestamp DemoGraph::GetCurrentTimestamp()
extern void DemoGraph_GetCurrentTimestamp_mDF2B222E428D6B20BA51AF5575A2DE521442C55E (void);
// 0x000000C4 System.Void DemoGraph::PrepareDependentAssets()
extern void DemoGraph_PrepareDependentAssets_mBB767E839F2DDB4174EC0B4792AB5315E69AC451 (void);
// 0x000000C5 System.Void DemoGraph::PrepareDependentAsset(System.String,System.String,System.Boolean)
extern void DemoGraph_PrepareDependentAsset_m34E9D4AB58D3F93104CBCF31BC8EAC22E48A8A6A (void);
// 0x000000C6 System.Void DemoGraph::PrepareDependentAsset(System.String,System.Boolean)
extern void DemoGraph_PrepareDependentAsset_m47F83CC86EF233C5A71909610C8B49F2D817314A (void);
// 0x000000C7 System.Void DemoGraph::.ctor()
extern void DemoGraph__ctor_mA23100AF178FD3650B48A9D7EA24328A9D323010 (void);
// 0x000000C8 System.Void DemoGraph::.cctor()
extern void DemoGraph__cctor_mE957BEE2AF55339F6D009A10A78F0C7B85701389 (void);
// 0x000000C9 System.Void GraphSelectorController::Start()
extern void GraphSelectorController_Start_mD91D0BBB84C06B0C21E1CE9F6130034FB7FA3385 (void);
// 0x000000CA System.Void GraphSelectorController::InitializeOptions()
extern void GraphSelectorController_InitializeOptions_m4609C975D121612A7D83A7B38F2734379309D8C2 (void);
// 0x000000CB System.Void GraphSelectorController::AddGraph(System.String,UnityEngine.GameObject)
extern void GraphSelectorController_AddGraph_m57AE5AAF597BEEC95D131C6ABA994261AA165F93 (void);
// 0x000000CC System.Void GraphSelectorController::OnValueChanged(UnityEngine.UI.Dropdown)
extern void GraphSelectorController_OnValueChanged_m9E1AB5ACF9B106EB7B4BD40E70AACAC566700075 (void);
// 0x000000CD System.Void GraphSelectorController::.ctor()
extern void GraphSelectorController__ctor_m50F2A7F3E49332E5FCB69883BADED34C5BD6516A (void);
// 0x000000CE System.Void GraphSelectorController/<>c__DisplayClass14_0::.ctor()
extern void U3CU3Ec__DisplayClass14_0__ctor_mDA752D5F05BE71E0CE0721C76EEC75AADF18BBAB (void);
// 0x000000CF System.Void GraphSelectorController/<>c__DisplayClass14_0::<Start>b__0(System.Int32)
extern void U3CU3Ec__DisplayClass14_0_U3CStartU3Eb__0_mE03A849834B76FA788F35CFB00240F4F8466B3CD (void);
// 0x000000D0 System.Void GraphSelectorController/<>c::.cctor()
extern void U3CU3Ec__cctor_m074B0DD6EB3F0513707C18EB8CC36BDC3E9AD660 (void);
// 0x000000D1 System.Void GraphSelectorController/<>c::.ctor()
extern void U3CU3Ec__ctor_m38A174A8D48807AFBD2B08B32FF893708FF80E81 (void);
// 0x000000D2 System.String GraphSelectorController/<>c::<InitializeOptions>b__15_0(System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GameObject>)
extern void U3CU3Ec_U3CInitializeOptionsU3Eb__15_0_mA9BCB57D32A8943FDF2D0E7B00FE5C7AA79F4F9A (void);
// 0x000000D3 System.Void IDemoGraph`1::Initialize()
// 0x000000D4 System.Void IDemoGraph`1::Initialize(Mediapipe.GpuResources,Mediapipe.GlCalculatorHelper)
// 0x000000D5 Mediapipe.Status IDemoGraph`1::StartRun()
// 0x000000D6 Mediapipe.Status IDemoGraph`1::StartRun(UnityEngine.Texture)
// 0x000000D7 Mediapipe.Status IDemoGraph`1::PushInput(T)
// 0x000000D8 System.Void IDemoGraph`1::RenderOutput(WebCamScreenController,T)
// 0x000000D9 System.Void IDemoGraph`1::Stop()
// 0x000000DA T MonoSingleton`1::get_Instance()
// 0x000000DB System.Void MonoSingleton`1::OnApplicationQuit()
// 0x000000DC System.Void MonoSingleton`1::OnDestroy()
// 0x000000DD System.Void MonoSingleton`1::.ctor()
// 0x000000DE System.Void MonoSingleton`1::.cctor()
// 0x000000DF System.Void AssetBundleLoader::Start()
extern void AssetBundleLoader_Start_m314A89ADD54444061951121B8A92B3BF6FC0716C (void);
// 0x000000E0 System.Void AssetBundleLoader::OnDestroy()
extern void AssetBundleLoader_OnDestroy_m8923F1ED479883407A3CC3A4CF70F2A1E8484154 (void);
// 0x000000E1 System.Void AssetBundleLoader::PrepareAsset(System.String,System.String,System.Boolean)
extern void AssetBundleLoader_PrepareAsset_m16D0132427E3FEF28A4662727BFF8894B0D87CA6 (void);
// 0x000000E2 System.Void AssetBundleLoader::PrepareAsset(System.String,System.Boolean)
extern void AssetBundleLoader_PrepareAsset_m173898ED2C7C6E68E9231E7A06FEE991C678189F (void);
// 0x000000E3 System.Void AssetBundleLoader::.ctor()
extern void AssetBundleLoader__ctor_mB970FD154AC04446AB9A5A3E2E37E6B21C9E9E4A (void);
// 0x000000E4 System.Void AssetLoader::PrepareAsset(System.String,System.String,System.Boolean)
// 0x000000E5 System.Void AssetLoader::PrepareAsset(System.String,System.Boolean)
// 0x000000E6 System.Void AssetLoader::.ctor()
extern void AssetLoader__ctor_mE56652CAAC8D47BEE3322D066093FC684FF1AFD9 (void);
// 0x000000E7 System.Void LocalAssetLoader::Start()
extern void LocalAssetLoader_Start_m3D815416B246A7EA5F38412A5251A2FAA9A16ED4 (void);
// 0x000000E8 System.Void LocalAssetLoader::PrepareAsset(System.String,System.String,System.Boolean)
extern void LocalAssetLoader_PrepareAsset_mF2E45803F3FD224B727AEC994B6B297178DA08AC (void);
// 0x000000E9 System.Void LocalAssetLoader::PrepareAsset(System.String,System.Boolean)
extern void LocalAssetLoader_PrepareAsset_m4EDFAF906DDC74A9BE113E7EEA6CCE0D39A153D3 (void);
// 0x000000EA System.Void LocalAssetLoader::.ctor()
extern void LocalAssetLoader__ctor_m03F5DF97295A2CA25DE6576B19E89640251B12E3 (void);
// 0x000000EB System.Int32 TextureFrame::get_width()
extern void TextureFrame_get_width_mB17FE2BFBE4D72D820BE811F28970E4DF2152286 (void);
// 0x000000EC System.Void TextureFrame::set_width(System.Int32)
extern void TextureFrame_set_width_m8341215AAEEE53D7BF045E837978BB36DC498509 (void);
// 0x000000ED System.Int32 TextureFrame::get_height()
extern void TextureFrame_get_height_mE5C87876C38DEFA9AC9E5CBC26F1FEF36F4D3C01 (void);
// 0x000000EE System.Void TextureFrame::set_height(System.Int32)
extern void TextureFrame_set_height_m2B6DC0460751E6E07DDA94C4469DA7EFCA6559F3 (void);
// 0x000000EF System.Void TextureFrame::.ctor(System.Int32,System.Int32,Mediapipe.GlTextureBuffer/DeletionCallback)
extern void TextureFrame__ctor_m618693D401DF10E2BD5AC1FDB7EAA3F5E2A12B9B (void);
// 0x000000F0 System.Void TextureFrame::CopyTexture(UnityEngine.Texture)
extern void TextureFrame_CopyTexture_mB5838078D4D4ABE454CE4376EF15FED5DBDDD483 (void);
// 0x000000F1 System.Void TextureFrame::CopyTextureFrom(UnityEngine.WebCamTexture)
extern void TextureFrame_CopyTextureFrom_m81118EA2BE3568462D439C38A880A0E280B5DFCA (void);
// 0x000000F2 UnityEngine.Color32[] TextureFrame::GetPixels32()
extern void TextureFrame_GetPixels32_mD2BC248BE24C1D5D80C197B37D4E1F72B60291B4 (void);
// 0x000000F3 Unity.Collections.NativeArray`1<System.Byte> TextureFrame::GetRawNativeByteArray()
extern void TextureFrame_GetRawNativeByteArray_m0A487F97A83BDCF4B6E99156169E5095787F0999 (void);
// 0x000000F4 System.IntPtr TextureFrame::GetNativeTexturePtr(System.Boolean)
extern void TextureFrame_GetNativeTexturePtr_mEEACFA81137F86A70792ABA8DAB274BB5D1BAF7A (void);
// 0x000000F5 Mediapipe.GpuBufferFormat TextureFrame::get_gpuBufferformat()
extern void TextureFrame_get_gpuBufferformat_m0A151608C3DE107C27B74B1A91EEFB87D42C0162 (void);
// 0x000000F6 System.Void TextureFrame::Release()
extern void TextureFrame_Release_m5FD86ED849C0A725FD81BA1571EB42FBEB6EC443 (void);
// 0x000000F7 System.Int32 TextureFramePool::get_frameCount()
extern void TextureFramePool_get_frameCount_m1EEE73F9CE3F37B8BFA654D190DF8643B3EA80C6 (void);
// 0x000000F8 System.Void TextureFramePool::Start()
extern void TextureFramePool_Start_m9E7FE1C938B1EC3AE6F1F27391A8C19D3A736901 (void);
// 0x000000F9 System.Void TextureFramePool::SetDimension(System.Int32,System.Int32)
extern void TextureFramePool_SetDimension_m6EA1E9F5A7FBCF369406C0471642ACFEFE49B366 (void);
// 0x000000FA TextureFramePool/TextureFrameRequest TextureFramePool::RequestNextTextureFrame(System.Action`1<TextureFrame>)
extern void TextureFramePool_RequestNextTextureFrame_m650F62C47E3514D5B40477D93D28C5B77B7D5D42 (void);
// 0x000000FB System.Void TextureFramePool::OnTextureFrameRelease(System.UInt64,System.IntPtr)
extern void TextureFramePool_OnTextureFrameRelease_m3F46346F6E43543DD9BE828528E788F0066D9B6D (void);
// 0x000000FC System.Boolean TextureFramePool::IsStale(TextureFrame)
extern void TextureFramePool_IsStale_m813BCC0E0D81350A198B851F3D1BFEE67EEA4559 (void);
// 0x000000FD TextureFrame TextureFramePool::CreateNewTextureFrame()
extern void TextureFramePool_CreateNewTextureFrame_m32BFEE26C9DF9D680B7D231E8BF59303671A4F51 (void);
// 0x000000FE System.Collections.IEnumerator TextureFramePool::WaitForTextureFrame(System.Action`1<TextureFrame>)
extern void TextureFramePool_WaitForTextureFrame_m529EA3FF190A1A49B5FBAA2C223A9FAABA01CA64 (void);
// 0x000000FF System.Void TextureFramePool::.ctor()
extern void TextureFramePool__ctor_m7E948FF5CE6609BB9D01BC41532BFEB818368649 (void);
// 0x00000100 System.Boolean TextureFramePool::<WaitForTextureFrame>b__14_0()
extern void TextureFramePool_U3CWaitForTextureFrameU3Eb__14_0_m67C6BA48945B02E3183AB2EF7B18D3062D1A388A (void);
// 0x00000101 TextureFrame TextureFramePool/TextureFrameRequest::get_textureFrame()
extern void TextureFrameRequest_get_textureFrame_m16903FC7DAE1826ADC2FA455E6FC22164C9A3B6A (void);
// 0x00000102 System.Void TextureFramePool/TextureFrameRequest::set_textureFrame(TextureFrame)
extern void TextureFrameRequest_set_textureFrame_m9144A4180722C034BC62972B4782BA63D4932740 (void);
// 0x00000103 System.Boolean TextureFramePool/TextureFrameRequest::get_keepWaiting()
extern void TextureFrameRequest_get_keepWaiting_mAE570C818C78FC40B06D2C9952B123BB82D95B31 (void);
// 0x00000104 System.Void TextureFramePool/TextureFrameRequest::.ctor(TextureFramePool,System.Action`1<TextureFrame>)
extern void TextureFrameRequest__ctor_m3D4B7B289DB257150776374D6FD34A1BBC47ADFE (void);
// 0x00000105 System.Void TextureFramePool/TextureFrameRequest/<>c__DisplayClass7_0::.ctor()
extern void U3CU3Ec__DisplayClass7_0__ctor_mA4E7764618D2B8580AA1984002395165B65BD0B0 (void);
// 0x00000106 System.Void TextureFramePool/TextureFrameRequest/<>c__DisplayClass7_0::<.ctor>b__0(TextureFrame)
extern void U3CU3Ec__DisplayClass7_0_U3C_ctorU3Eb__0_mF5C499D1FF07B38DE384EA7D6D2F987AEFD03BEA (void);
// 0x00000107 System.Void TextureFramePool/<WaitForTextureFrame>d__14::.ctor(System.Int32)
extern void U3CWaitForTextureFrameU3Ed__14__ctor_mF771738CC4E23FBA80292BA09F52FD4F8D6B9D28 (void);
// 0x00000108 System.Void TextureFramePool/<WaitForTextureFrame>d__14::System.IDisposable.Dispose()
extern void U3CWaitForTextureFrameU3Ed__14_System_IDisposable_Dispose_m0856FDFBE77BC309EFB4E259C2C1F5C449608973 (void);
// 0x00000109 System.Boolean TextureFramePool/<WaitForTextureFrame>d__14::MoveNext()
extern void U3CWaitForTextureFrameU3Ed__14_MoveNext_m714F156265E3DF6D1B6C7A9E668828021494862E (void);
// 0x0000010A System.Void TextureFramePool/<WaitForTextureFrame>d__14::<>m__Finally1()
extern void U3CWaitForTextureFrameU3Ed__14_U3CU3Em__Finally1_mD0E118D793FECA85E0146D2D2F3880164CD0DFC6 (void);
// 0x0000010B System.Object TextureFramePool/<WaitForTextureFrame>d__14::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitForTextureFrameU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m53532F7067493675A7491BEBD5F356D352EFD9D6 (void);
// 0x0000010C System.Void TextureFramePool/<WaitForTextureFrame>d__14::System.Collections.IEnumerator.Reset()
extern void U3CWaitForTextureFrameU3Ed__14_System_Collections_IEnumerator_Reset_m46689A7498CC85E7622FE1AD0C2735D6CEF2AC20 (void);
// 0x0000010D System.Object TextureFramePool/<WaitForTextureFrame>d__14::System.Collections.IEnumerator.get_Current()
extern void U3CWaitForTextureFrameU3Ed__14_System_Collections_IEnumerator_get_Current_m1D796BDA08361D499F44C847EF0197DE55C3D817 (void);
// 0x0000010E System.Void SceneDirectors::OnEnable()
extern void SceneDirectors_OnEnable_mCBD92A542FF77CB1B4309276BA24ACC84607E8D2 (void);
// 0x0000010F System.Void SceneDirectors::GetCurrentContext(System.Int32)
extern void SceneDirectors_GetCurrentContext_mCCE71B73479FC8A1F8D65066ED794FE54AF5401C (void);
// 0x00000110 System.Void SceneDirectors::Start()
extern void SceneDirectors_Start_m38CF084668B0A46498DC296BF25AFF997CC8CA8E (void);
// 0x00000111 System.Void SceneDirectors::OnDisable()
extern void SceneDirectors_OnDisable_m963B803D2C7E4B5905504CD2B0D7B4A443174E78 (void);
// 0x00000112 System.Void SceneDirectors::ChangeWebCamDevice(System.Nullable`1<UnityEngine.WebCamDevice>)
extern void SceneDirectors_ChangeWebCamDevice_mFCE77CD4BDEEBF3C5367A3CA155F8A92A8842D89 (void);
// 0x00000113 System.Void SceneDirectors::ResetCamera(System.Nullable`1<UnityEngine.WebCamDevice>)
extern void SceneDirectors_ResetCamera_m6ABD1DBF5D0E3C6AABFB71728CB51B3CEFAC6E9E (void);
// 0x00000114 System.Void SceneDirectors::StopCamera()
extern void SceneDirectors_StopCamera_m5AB26AFF4AFA1287CC8BCFB177EF3E3398B0F883 (void);
// 0x00000115 System.Void SceneDirectors::ChangeGraph(UnityEngine.GameObject)
extern void SceneDirectors_ChangeGraph_m37E90B817E010C6B07DE7FB78E726FB46B57CC44 (void);
// 0x00000116 System.Void SceneDirectors::StartGraph()
extern void SceneDirectors_StartGraph_m7EFFEBA32E6BEDDC3533DF445CA96D9EC7BDE000 (void);
// 0x00000117 System.Void SceneDirectors::StopGraph()
extern void SceneDirectors_StopGraph_m491AECC533ACAD2F35910BBAC23520A69C8204EF (void);
// 0x00000118 System.Void SceneDirectors::SetupGpuResources()
extern void SceneDirectors_SetupGpuResources_mD56D11263A6E7302B82A16BA173601BEA7E4CC38 (void);
// 0x00000119 System.Collections.IEnumerator SceneDirectors::RunGraph()
extern void SceneDirectors_RunGraph_m538FB86082B4D6CC9D2871496FD8F79ECF0E7DFC (void);
// 0x0000011A System.Collections.IEnumerator SceneDirectors::WaitForGraph()
extern void SceneDirectors_WaitForGraph_m336B2A27DAC594C63F79DBC26A63439A4E1908A2 (void);
// 0x0000011B System.Collections.IEnumerator SceneDirectors::WaitForCamera(WebCamScreenController)
extern void SceneDirectors_WaitForCamera_m275D49CD83631A2DCB68FB4B48C8E7F6CB350E44 (void);
// 0x0000011C System.Boolean SceneDirectors::IsGpuEnabled()
extern void SceneDirectors_IsGpuEnabled_m143E1BD1CED99A16BE648101D9A85F3079A8DD8A (void);
// 0x0000011D System.Void SceneDirectors::.ctor()
extern void SceneDirectors__ctor_mF12F0D145BBDF0D0019647DA938A5D183383E3C3 (void);
// 0x0000011E System.Void SceneDirectors::.cctor()
extern void SceneDirectors__cctor_mFE88EF865916DD4BB9BB603DE50C013440231745 (void);
// 0x0000011F System.Void SceneDirectors/PluginCallback::.ctor(System.Object,System.IntPtr)
extern void PluginCallback__ctor_m61BF52D1A0D7864A3261FD73EA7717DD28A381F3 (void);
// 0x00000120 System.Void SceneDirectors/PluginCallback::Invoke(System.Int32)
extern void PluginCallback_Invoke_m6EF75B25BBE9D45490735A69014EA486DE6A07B5 (void);
// 0x00000121 System.IAsyncResult SceneDirectors/PluginCallback::BeginInvoke(System.Int32,System.AsyncCallback,System.Object)
extern void PluginCallback_BeginInvoke_m8A935FAE202242AF0447F3EE1AA3DBA51EF3085A (void);
// 0x00000122 System.Void SceneDirectors/PluginCallback::EndInvoke(System.IAsyncResult)
extern void PluginCallback_EndInvoke_mB86BE68FBF7572990FCEEB018C3F66BDFD54D57B (void);
// 0x00000123 System.Void SceneDirectors/<RunGraph>d__24::.ctor(System.Int32)
extern void U3CRunGraphU3Ed__24__ctor_mD8E548AF383C56A2B2A8AEF57581758506147835 (void);
// 0x00000124 System.Void SceneDirectors/<RunGraph>d__24::System.IDisposable.Dispose()
extern void U3CRunGraphU3Ed__24_System_IDisposable_Dispose_m1A65857C24F53A20E78ACA865CEF9BC40FA2FCF8 (void);
// 0x00000125 System.Boolean SceneDirectors/<RunGraph>d__24::MoveNext()
extern void U3CRunGraphU3Ed__24_MoveNext_mA48529878CC074107801B30928AB6A835A869470 (void);
// 0x00000126 System.Object SceneDirectors/<RunGraph>d__24::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRunGraphU3Ed__24_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD2AD458E8533A88202382ACC60A91B838B2DDBAB (void);
// 0x00000127 System.Void SceneDirectors/<RunGraph>d__24::System.Collections.IEnumerator.Reset()
extern void U3CRunGraphU3Ed__24_System_Collections_IEnumerator_Reset_mF598EC361ACE423F3AAA808AE02A3FFD0799AB53 (void);
// 0x00000128 System.Object SceneDirectors/<RunGraph>d__24::System.Collections.IEnumerator.get_Current()
extern void U3CRunGraphU3Ed__24_System_Collections_IEnumerator_get_Current_m0F69806467AF047BC3C6FB4F9EBD727FD3E1E9A8 (void);
// 0x00000129 System.Void SceneDirectors/<>c__DisplayClass25_0::.ctor()
extern void U3CU3Ec__DisplayClass25_0__ctor_m1BE01B678D510B8DCBB9967532F507E1BC902BFD (void);
// 0x0000012A System.Boolean SceneDirectors/<>c__DisplayClass25_0::<WaitForGraph>b__0()
extern void U3CU3Ec__DisplayClass25_0_U3CWaitForGraphU3Eb__0_m165D9F8B6F3FF9F1280CD5551693A0BB897224EB (void);
// 0x0000012B System.Void SceneDirectors/<WaitForGraph>d__25::.ctor(System.Int32)
extern void U3CWaitForGraphU3Ed__25__ctor_m5CA156DE5E91E041B8EE6C3B206858ED6B2FDE62 (void);
// 0x0000012C System.Void SceneDirectors/<WaitForGraph>d__25::System.IDisposable.Dispose()
extern void U3CWaitForGraphU3Ed__25_System_IDisposable_Dispose_m4AA842376297CE56575BA85E691DD6D60B254E78 (void);
// 0x0000012D System.Boolean SceneDirectors/<WaitForGraph>d__25::MoveNext()
extern void U3CWaitForGraphU3Ed__25_MoveNext_mB0726717B2E9F8348367DA00A1A98B5A281695C9 (void);
// 0x0000012E System.Object SceneDirectors/<WaitForGraph>d__25::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitForGraphU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF0E9DF6BE3D9F085FA4BB436AAC9E15332D245EC (void);
// 0x0000012F System.Void SceneDirectors/<WaitForGraph>d__25::System.Collections.IEnumerator.Reset()
extern void U3CWaitForGraphU3Ed__25_System_Collections_IEnumerator_Reset_m04AE7F2FE9D453891514E4C6FED67FFD1876A5A5 (void);
// 0x00000130 System.Object SceneDirectors/<WaitForGraph>d__25::System.Collections.IEnumerator.get_Current()
extern void U3CWaitForGraphU3Ed__25_System_Collections_IEnumerator_get_Current_m49604FA593536760B6C891E6A0A1D2CBAEA241E9 (void);
// 0x00000131 System.Void SceneDirectors/<>c__DisplayClass26_0::.ctor()
extern void U3CU3Ec__DisplayClass26_0__ctor_m5D09D9E181E00F6F2D9EE7BE9DE1345B95CB6156 (void);
// 0x00000132 System.Boolean SceneDirectors/<>c__DisplayClass26_0::<WaitForCamera>b__0()
extern void U3CU3Ec__DisplayClass26_0_U3CWaitForCameraU3Eb__0_m34CFB2A116353E776417F2F5C61902DB0D22C4BD (void);
// 0x00000133 System.Void SceneDirectors/<WaitForCamera>d__26::.ctor(System.Int32)
extern void U3CWaitForCameraU3Ed__26__ctor_mEF0BA9234B5326F8D1DA18B5BC3D54C2EDB621A8 (void);
// 0x00000134 System.Void SceneDirectors/<WaitForCamera>d__26::System.IDisposable.Dispose()
extern void U3CWaitForCameraU3Ed__26_System_IDisposable_Dispose_m798B59916A7F7B04C2473B999C112FAA56D2E800 (void);
// 0x00000135 System.Boolean SceneDirectors/<WaitForCamera>d__26::MoveNext()
extern void U3CWaitForCameraU3Ed__26_MoveNext_m35011ADE6AC38B7DF2212CE5384222ADCB119599 (void);
// 0x00000136 System.Object SceneDirectors/<WaitForCamera>d__26::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitForCameraU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF9C3E046BC69EEED73E7E43B6788A781A09AFC3F (void);
// 0x00000137 System.Void SceneDirectors/<WaitForCamera>d__26::System.Collections.IEnumerator.Reset()
extern void U3CWaitForCameraU3Ed__26_System_Collections_IEnumerator_Reset_m3CB2E113834F80E3427C87910C5CA706FA5DDA4B (void);
// 0x00000138 System.Object SceneDirectors/<WaitForCamera>d__26::System.Collections.IEnumerator.get_Current()
extern void U3CWaitForCameraU3Ed__26_System_Collections_IEnumerator_get_Current_m6590C064728CBBE508A79AF2027ABFBBF28B93B2 (void);
// 0x00000139 System.Collections.IEnumerator WebCamDeviceSelectorController::Start()
extern void WebCamDeviceSelectorController_Start_m519A2B7FD0AA40F570C8EC6EF6A0AEEB057560F2 (void);
// 0x0000013A System.Void WebCamDeviceSelectorController::ResetOptions(UnityEngine.WebCamDevice[])
extern void WebCamDeviceSelectorController_ResetOptions_mB4740F2C8BAF7C189219F39D5F986D76615597AC (void);
// 0x0000013B System.Void WebCamDeviceSelectorController::OnValueChanged(UnityEngine.UI.Dropdown)
extern void WebCamDeviceSelectorController_OnValueChanged_m80AE65850AD7B8966C390B2572F447AAAA698EC4 (void);
// 0x0000013C System.Void WebCamDeviceSelectorController::.ctor()
extern void WebCamDeviceSelectorController__ctor_mFF483BA8241138EA65FDC1E8ABD012D2D27FB3FF (void);
// 0x0000013D System.Void WebCamDeviceSelectorController/<>c__DisplayClass2_0::.ctor()
extern void U3CU3Ec__DisplayClass2_0__ctor_mF3418C9C0D34BAD9890879D9D68E5978D8217583 (void);
// 0x0000013E System.Void WebCamDeviceSelectorController/<>c__DisplayClass2_0::<Start>b__0(System.Int32)
extern void U3CU3Ec__DisplayClass2_0_U3CStartU3Eb__0_mEB2BFFCFAC5100A1ABAA8717E506DE12FC048F29 (void);
// 0x0000013F System.Void WebCamDeviceSelectorController/<Start>d__2::.ctor(System.Int32)
extern void U3CStartU3Ed__2__ctor_m906CABD8C45330E13A122514A7C09E2EC5FC0039 (void);
// 0x00000140 System.Void WebCamDeviceSelectorController/<Start>d__2::System.IDisposable.Dispose()
extern void U3CStartU3Ed__2_System_IDisposable_Dispose_m0139BD6575B1E2FA511461685412C8259DCA96E1 (void);
// 0x00000141 System.Boolean WebCamDeviceSelectorController/<Start>d__2::MoveNext()
extern void U3CStartU3Ed__2_MoveNext_m136CCB629E85380FC4C25CFDB823994C8B179176 (void);
// 0x00000142 System.Object WebCamDeviceSelectorController/<Start>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC9597C9A7823BD016D2A3C49B00789A8CB933A45 (void);
// 0x00000143 System.Void WebCamDeviceSelectorController/<Start>d__2::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__2_System_Collections_IEnumerator_Reset_m3B48B6C9ABBB439192150CDDCD2EFF1DE7DFC150 (void);
// 0x00000144 System.Object WebCamDeviceSelectorController/<Start>d__2::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__2_System_Collections_IEnumerator_get_Current_mD2E5B1382AFEAA92756EEC8CEF1A480E6EE3F82D (void);
// 0x00000145 System.Void WebCamDeviceSelectorController/<>c::.cctor()
extern void U3CU3Ec__cctor_m3E75619C4AE56C7338911687731CB5F9DA67388C (void);
// 0x00000146 System.Void WebCamDeviceSelectorController/<>c::.ctor()
extern void U3CU3Ec__ctor_m4CC6FB973E54D9EC805CB8585256E36041A9CD0D (void);
// 0x00000147 System.String WebCamDeviceSelectorController/<>c::<ResetOptions>b__3_0(UnityEngine.WebCamDevice)
extern void U3CU3Ec_U3CResetOptionsU3Eb__3_0_mD0F6B7F5AE75B9B5AA967D46F06869613F4FE7BA (void);
// 0x00000148 System.Boolean WebCamScreenController::get_isPlaying()
extern void WebCamScreenController_get_isPlaying_m0E549AEE2590F3C44B0A7024F6B9935A8D030DB8 (void);
// 0x00000149 System.Boolean WebCamScreenController::get_isWebCamTextureInitialized()
extern void WebCamScreenController_get_isWebCamTextureInitialized_m1F8E924BE952001D20EB6C33FEB263B89FD26A43 (void);
// 0x0000014A System.Boolean WebCamScreenController::get_isWebCamReady()
extern void WebCamScreenController_get_isWebCamReady_m3E7B97B3CC805F75101BD695CD71B5C8805E10AB (void);
// 0x0000014B System.Collections.IEnumerator WebCamScreenController::ResetScreen(System.Nullable`1<UnityEngine.WebCamDevice>)
extern void WebCamScreenController_ResetScreen_mAE3830893EA75A8259087B49F90DE6FC58162FE6 (void);
// 0x0000014C System.Single WebCamScreenController::GetFocalLengthPx()
extern void WebCamScreenController_GetFocalLengthPx_mC17048586BA498F9E296F5D9E31FAECE4E2BDA81 (void);
// 0x0000014D UnityEngine.Color32[] WebCamScreenController::GetPixels32()
extern void WebCamScreenController_GetPixels32_mC2ABD47CAA36A512745F08C845F811A763930347 (void);
// 0x0000014E System.IntPtr WebCamScreenController::GetNativeTexturePtr()
extern void WebCamScreenController_GetNativeTexturePtr_m2D55BC26BAEF943A94E34F609F956683E939456A (void);
// 0x0000014F UnityEngine.Texture2D WebCamScreenController::GetScreen()
extern void WebCamScreenController_GetScreen_mE2672C011854115B4405400233271228C9501F2B (void);
// 0x00000150 System.Void WebCamScreenController::DrawScreen(UnityEngine.Color32[])
extern void WebCamScreenController_DrawScreen_m81777A74A970CCF3B5B1E937CC17B73983B2EA14 (void);
// 0x00000151 System.Void WebCamScreenController::DrawScreen(TextureFrame)
extern void WebCamScreenController_DrawScreen_m66665EB5E695F86D3B1412C6A41758524756B95D (void);
// 0x00000152 System.Void WebCamScreenController::DrawScreen(Mediapipe.ImageFrame)
extern void WebCamScreenController_DrawScreen_m9CAD950BEA882BD8BED018A3A45269CC508AE3D4 (void);
// 0x00000153 System.Void WebCamScreenController::DrawScreen(Mediapipe.GpuBuffer)
extern void WebCamScreenController_DrawScreen_m60FD0893746CB0EBA14FB886F24A384B2D0F9933 (void);
// 0x00000154 TextureFramePool/TextureFrameRequest WebCamScreenController::RequestNextFrame()
extern void WebCamScreenController_RequestNextFrame_mFCB56335FA67C76752A1473A0E421A5D1EBA0711 (void);
// 0x00000155 System.Void WebCamScreenController::.ctor()
extern void WebCamScreenController__ctor_m564A79DFC42C3283DC7C8B5195A1BFE8AB46B283 (void);
// 0x00000156 System.Void WebCamScreenController::<RequestNextFrame>b__25_0(TextureFrame)
extern void WebCamScreenController_U3CRequestNextFrameU3Eb__25_0_m8AB50133A528C658E544CB7ACE354C0728F7971D (void);
// 0x00000157 System.Void WebCamScreenController/WebCamTextureFramePool::.ctor()
extern void WebCamTextureFramePool__ctor_m3DC7CC1F2FB9290A51CF95A97F527C765E6C2138 (void);
// 0x00000158 System.Void WebCamScreenController/<>c__DisplayClass16_0::.ctor()
extern void U3CU3Ec__DisplayClass16_0__ctor_m2CF670D61D38970A791AA2A0EB39265CD742F921 (void);
// 0x00000159 System.Boolean WebCamScreenController/<>c__DisplayClass16_0::<ResetScreen>b__0()
extern void U3CU3Ec__DisplayClass16_0_U3CResetScreenU3Eb__0_m885265E8AFDFCA117465873E143DF4338AAE43F3 (void);
// 0x0000015A System.Void WebCamScreenController/<ResetScreen>d__16::.ctor(System.Int32)
extern void U3CResetScreenU3Ed__16__ctor_mDAD1FEE5686D6597DCB7579537D2EB62649F79D9 (void);
// 0x0000015B System.Void WebCamScreenController/<ResetScreen>d__16::System.IDisposable.Dispose()
extern void U3CResetScreenU3Ed__16_System_IDisposable_Dispose_m7C7966A9BC91BB8B9411C59299BE4A64AD4F3F73 (void);
// 0x0000015C System.Boolean WebCamScreenController/<ResetScreen>d__16::MoveNext()
extern void U3CResetScreenU3Ed__16_MoveNext_mB725E61B4C070637F15D48DF6752994D0C0C4D51 (void);
// 0x0000015D System.Object WebCamScreenController/<ResetScreen>d__16::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CResetScreenU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4832D1E17D71F3ECAF696FCCCF39237400C816C3 (void);
// 0x0000015E System.Void WebCamScreenController/<ResetScreen>d__16::System.Collections.IEnumerator.Reset()
extern void U3CResetScreenU3Ed__16_System_Collections_IEnumerator_Reset_m60226C6644A0E904B746230543DABF1C5BCD4336 (void);
// 0x0000015F System.Object WebCamScreenController/<ResetScreen>d__16::System.Collections.IEnumerator.get_Current()
extern void U3CResetScreenU3Ed__16_System_Collections_IEnumerator_get_Current_mB8C7F8DC2FAE2DD8A27D76DF2D91AB5F7B2AA38A (void);
// 0x00000160 System.Void PoseEstimation0fBody::Start()
extern void PoseEstimation0fBody_Start_m91FA846628A342754D6E87BAE1F5D0441E120AFF (void);
// 0x00000161 System.Void PoseEstimation0fBody::InitializeOptions()
extern void PoseEstimation0fBody_InitializeOptions_m66749367C2607A165DE4EA0C1E67671E12646798 (void);
// 0x00000162 System.Void PoseEstimation0fBody::AddGraph(System.String,UnityEngine.GameObject)
extern void PoseEstimation0fBody_AddGraph_m9CF7DEB41674F5E4A7FFD2598E6911DF0D9715B0 (void);
// 0x00000163 System.Void PoseEstimation0fBody::OnValueChanged(UnityEngine.UI.Dropdown)
extern void PoseEstimation0fBody_OnValueChanged_m248B29D7F3B1B20199C11115CA5AE9F335283A80 (void);
// 0x00000164 System.Void PoseEstimation0fBody::.ctor()
extern void PoseEstimation0fBody__ctor_m97DA44F36B6C41703D5F9247B12EC0F88AC0441B (void);
// 0x00000165 System.Void PoseEstimation0fBody/<>c__DisplayClass3_0::.ctor()
extern void U3CU3Ec__DisplayClass3_0__ctor_m4D9074C547A5900BBD37FC9B1640020C85D6DDE9 (void);
// 0x00000166 System.Void PoseEstimation0fBody/<>c__DisplayClass3_0::<Start>b__0(System.Int32)
extern void U3CU3Ec__DisplayClass3_0_U3CStartU3Eb__0_m36BA8EA8C57247BEBF8E2509096254804786DBCE (void);
// 0x00000167 System.Void PoseEstimation0fBody/<>c::.cctor()
extern void U3CU3Ec__cctor_mA277C683B4DCAA5C52C178D2AEEF38A185087D55 (void);
// 0x00000168 System.Void PoseEstimation0fBody/<>c::.ctor()
extern void U3CU3Ec__ctor_m245D725199C98168448F7AF2C845415ABD16C300 (void);
// 0x00000169 System.String PoseEstimation0fBody/<>c::<InitializeOptions>b__4_0(System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GameObject>)
extern void U3CU3Ec_U3CInitializeOptionsU3Eb__4_0_m70691D0D9209490C781B535F269985B3F22F338C (void);
// 0x0000016A System.Void PoseToCharacter::Start()
extern void PoseToCharacter_Start_m94450E96DA400CF785CC05ECB5C74D3D7197982F (void);
// 0x0000016B System.Void PoseToCharacter::Update()
extern void PoseToCharacter_Update_mA8E1D20A3013884FDB7458131F6A93E659CCA3EE (void);
// 0x0000016C System.Void PoseToCharacter::.ctor()
extern void PoseToCharacter__ctor_m3B9F9F88F1289CBCACBBB7F27C0E820D803605BD (void);
// 0x0000016D System.Void Mediapipe.FaceMeshAnnotationController::OnDestroy()
extern void FaceMeshAnnotationController_OnDestroy_mCAF09B0DDD5183D7F7035B9B3642EB44206181D8 (void);
// 0x0000016E System.Void Mediapipe.FaceMeshAnnotationController::Awake()
extern void FaceMeshAnnotationController_Awake_m6FED787F540E20CA63DEAE2B717F06795EFD7051 (void);
// 0x0000016F System.Void Mediapipe.FaceMeshAnnotationController::Clear()
extern void FaceMeshAnnotationController_Clear_m4E71BF85112EBF5AE41CF06E24E6C7D29578A1AC (void);
// 0x00000170 System.Void Mediapipe.FaceMeshAnnotationController::Draw(UnityEngine.Transform,System.Collections.Generic.List`1<Mediapipe.NormalizedLandmarkList>,System.Collections.Generic.List`1<Mediapipe.NormalizedRect>,System.Collections.Generic.List`1<Mediapipe.Detection>,System.Boolean)
extern void FaceMeshAnnotationController_Draw_m3E16B0F8AA1C73F609976EA805F455379AE71DD6 (void);
// 0x00000171 System.Void Mediapipe.FaceMeshAnnotationController::.ctor()
extern void FaceMeshAnnotationController__ctor_mE67164411DC0053C7A15E2B46F244E83095EC01F (void);
// 0x00000172 System.Void Mediapipe.IrisTrackingAnnotationController::Awake()
extern void IrisTrackingAnnotationController_Awake_mDE918CD348065F148A3DC255C060E10A483F31C4 (void);
// 0x00000173 System.Void Mediapipe.IrisTrackingAnnotationController::OnDestroy()
extern void IrisTrackingAnnotationController_OnDestroy_mA14BA87DDFA8B39BA605758A79BE21D74A399C9E (void);
// 0x00000174 System.Void Mediapipe.IrisTrackingAnnotationController::Clear()
extern void IrisTrackingAnnotationController_Clear_m35985209DC5C93D0C92276804464BECD6C446915 (void);
// 0x00000175 System.Void Mediapipe.IrisTrackingAnnotationController::Draw(UnityEngine.Transform,Mediapipe.NormalizedLandmarkList,Mediapipe.NormalizedRect,System.Collections.Generic.List`1<Mediapipe.Detection>,System.Boolean)
extern void IrisTrackingAnnotationController_Draw_m942952474ECADBEA9F7133E011FBC00E6EA18C9D (void);
// 0x00000176 System.Collections.Generic.IList`1<Mediapipe.NormalizedLandmark> Mediapipe.IrisTrackingAnnotationController::GetIrisLandmarks(Mediapipe.NormalizedLandmarkList,Mediapipe.IrisTrackingAnnotationController/Side)
extern void IrisTrackingAnnotationController_GetIrisLandmarks_m9514410F016CF22279C0FF620120227916377F7E (void);
// 0x00000177 System.Void Mediapipe.IrisTrackingAnnotationController::.ctor()
extern void IrisTrackingAnnotationController__ctor_m0C8345BCC991BA7985138F9AA3D32CDD2E20E4FA (void);
static Il2CppMethodPointer s_methodPointers[375] = 
{
	BoxTrackingGraph_PrepareDependentAssets_m05EE28DA9847DD92864711C314FA859BC1604D8B,
	BoxTrackingGraph__ctor_m1F924AEA97048C003343847B748ECE447B0D5C14,
	FaceDetectionGraph_StartRun_m4C9EC328DA283D7DF4166322D028FB90F56C8F0F,
	FaceDetectionGraph_RenderOutput_m636FCE4A0542B4EB15A8006D050C83A876C8F02A,
	FaceDetectionGraph_FetchNextFaceDetectionsPresence_mB8F34BF749B84CBE3FB6B1646C7395E3578EDBD8,
	FaceDetectionGraph_FetchNextFaceDetections_m28848AE7CE7FC056EA9A892D1EE5E6BDF602A29F,
	FaceDetectionGraph_RenderAnnotation_mAD9D6B8CD827468770C21825CE71220648566F3F,
	FaceDetectionGraph_PrepareDependentAssets_mD50A5EAD1B3D925B360DB5900D720EFD04D5DA53,
	FaceDetectionGraph__ctor_m4AD69744309E2963FE3472DA01BAACC7925F8D2B,
	FaceMeshGraph_StartRun_m8FBF2282FBFDB0CAB4958F9760CFF6DB7C727676,
	FaceMeshGraph_RenderOutput_m4254F5F13CB92C5973D3A3368930A03690002088,
	FaceMeshGraph_FetchNextFaceMeshValue_m2D19113DFFAEA3959DF3D81D74BC80F28DC9B7FF,
	FaceMeshGraph_FetchNextMultiFaceLandmarksPresence_m40F6730E413758E20C88E4966B5C3F59F7DC5826,
	FaceMeshGraph_FetchNextMultiFaceLandmarks_m19A423812E005DBA773429DB7D6F9E5335E63F30,
	FaceMeshGraph_FetchNextFaceRectsFromLandmarks_mE1C04E004DCA2E89C961B115BACD5342F68DFF94,
	FaceMeshGraph_FetchNextFaceDetectionsPresence_m4C4B5B85FA8EA8556A3DF8B97C3FAA9C644D9D76,
	FaceMeshGraph_FetchNextFaceDetections_mB6BF818DC0FB6592D475BB639CB094B6701DB387,
	FaceMeshGraph_RenderAnnotation_m1DB0A81D2A9CFBA8066CC1C3D8202EA75FA52205,
	FaceMeshGraph_PrepareDependentAssets_m294A4DA923760F74783AED263C03A78BFC949F13,
	FaceMeshGraph__ctor_m43E53DD64DE931567D66B9AD018909A4D71ABC65,
	FaceMeshValue__ctor_mFC39E40B2619885A26F478916AA27649B8662A54,
	FaceMeshValue__ctor_m95AE9C33402D7A75EB7E2DB5EA77CD8AE20B5441,
	FaceMeshValue__ctor_m6A233558A47A78EB07E2357962C5BAF4FED097AB,
	MultiFaceLandmarkListAnnotationController_Draw_m6BCC424733D3B71D37BF2DB3EDCF7DEE911A5449,
	MultiFaceLandmarkListAnnotationController__ctor_m85BEE397C269389D174E5B4F0D58E37CB6C81CFC,
	HairSegmentationGraph_StartRun_mCE6B8980E63427A47520897D2CD62F2F4A924C1A,
	HairSegmentationGraph_RenderOutput_m5A9647CA4ED55B4C926F09C15B15D35B60783F7D,
	HairSegmentationGraph_FetchNextHairMask_mEA29737479B86679C7A2C5027C36A3D0706E6417,
	HairSegmentationGraph_RenderAnnotation_m42157BA41DEDC645EE8E4A283919842E62BDA594,
	HairSegmentationGraph_PrepareDependentAssets_m23CB676C5758F2EB911D0AB2DF526367871719C2,
	HairSegmentationGraph__ctor_m30D9B4BD759E5F9F4DC712DF01B0211E3B8F7F6E,
	HandTrackingAnnotationController_Awake_m198BAFA00284FB41F83344F3E5176047C7F7F8C9,
	HandTrackingAnnotationController_OnDestroy_m9B27802FEF8EF883F894737BD0F58AF20ACA1704,
	HandTrackingAnnotationController_Clear_m9C0901D913DA0AE6675954ECC4A2FBC8C0C2D4D5,
	HandTrackingAnnotationController_Draw_m13C67CED8F296622F9D365B7298B501882465AB9,
	HandTrackingAnnotationController__ctor_mC980D10DF166E35BD4C931BA554D3EA9D2941D9D,
	HandTrackingGraph_StartRun_m27E20BEE05795279C04D220E23B37361CD331B76,
	HandTrackingGraph_RenderOutput_mEA7097A99F5189AB98693FEEC137DE53777E1DAB,
	HandTrackingGraph_FetchNextHandTrackingValue_mADAAA040A00929AC0CA0F5D7F7FC7CFE2511D977,
	HandTrackingGraph_FetchNextHandednesses_m4A5063894719F0077B194EA593AEA6F8C5B479D5,
	HandTrackingGraph_FetchNextPalmRects_m4915CDCD124B7257542AFFA078EEAC2DDC9EDB75,
	HandTrackingGraph_FetchNextHandLandmarks_mC8FFBB34EE4F7EB57765EF44B4BCAFC260E49721,
	HandTrackingGraph_FetchNextHandLandmarksPresence_m64DEC8EA11773720C40261C1DCB9BCE6263A514E,
	HandTrackingGraph_FetchNextPalmDetectionsPresence_mF39C54AC1FA0DD1F00D35CAA52EF5895F46FC63E,
	HandTrackingGraph_FetchNextPalmDetections_m1B4F2746E00A46A7FC9F0982F11B7A241D5F7955,
	HandTrackingGraph_RenderAnnotation_mEB8A66095D376FD0E2990A00029DD658132A3B12,
	HandTrackingGraph_PrepareDependentAssets_m62CDE28125E1FCDC09EAB929D638DDE775F54E23,
	HandTrackingGraph__ctor_m348829BB1A1F463C698A924BC45EF6366F2FEC35,
	HandTrackingValue__ctor_mE27A4A78AAFFACB7445F7A85544F1702C450A6AB,
	MultiHandLandmarkListAnnotationController_Draw_m227CE67F22F1D0DB0E5F71505259DD47640E95F1,
	MultiHandLandmarkListAnnotationController__ctor_m0D3902B0E52D175719DF844CA50A3B1A2E01588D,
	HelloWorld_OnEnable_m6AFEE56E7F9980EE178909D98C0354F657772FBF,
	HelloWorld_OnDestroy_m67F8DDC62429D3CADD2140FF2BF75136EBB7149E,
	HelloWorld_OnDisable_mA1048B3556E33611481F597AAE2FC06B2EC3AE6D,
	HelloWorld_Start_m5BDA2C486D048B975ACDD0F4F1402960153FA64E,
	HelloWorld_RunGraph_mEC050C49115FEC8EDB2DA7CEC4CDC6786D220FE9,
	HelloWorld__ctor_mBA31A819FA7B1D67B696BD1C8B7339E6BB053723,
	U3CRunGraphU3Ed__7__ctor_m3DFA86D230949C52467903E06DD7147BEC7CD009,
	U3CRunGraphU3Ed__7_System_IDisposable_Dispose_m274C2E9415C732F3D596F53C22C7F0FA82F94094,
	U3CRunGraphU3Ed__7_MoveNext_m3BB5CB4B3D84C4D1C373E35A0B3CD8D50D4719E0,
	U3CRunGraphU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC95A3572B36E5FA9CAB03A0B7412AA63218F2CB2,
	U3CRunGraphU3Ed__7_System_Collections_IEnumerator_Reset_m301ADA817F03B03DA7898E44710ACCA8220EC79F,
	U3CRunGraphU3Ed__7_System_Collections_IEnumerator_get_Current_mFCF11A73BC49AE0753C24A27BABB665A8E2171D3,
	HelloWorldGraph_Initialize_mB8C04A91B7F773DF6C059DEC5470D4486B43FB56,
	HelloWorldGraph_Initialize_m595AAC24B1E935F3E9BA1BB658E9B8724B72C322,
	HelloWorldGraph_StartRun_m19CB956409000DEFD9B3110D542330A6C6C058DF,
	HelloWorldGraph_StartRun_m8A3AF566B0070B654900EC06F09BE18DBCD8ECD6,
	HelloWorldGraph_PushInput_m04D33B0645CF73E91664A52C399DC06727A100E0,
	HelloWorldGraph_RenderOutput_m7B30F29BD10D048671EF5AB746A8B27DC3AA269B,
	HelloWorldGraph_Stop_mADFE3EA726C4653EACD1B0FD932A063EC21A4904,
	HelloWorldGraph__ctor_m44F353825068CA222F325603F7CB9458AF7F5E4B,
	HolisticAnnotationController_Awake_mC365D079C7EBB9EB1C506626BDDC1BE3B8A88C3E,
	HolisticAnnotationController_OnDestroy_mD506D081F43376BC9001162882F48BA4E4C1BA03,
	HolisticAnnotationController_ClearIrisAnnotations_mBE35D2226E99BB831AF3B8731C6C90FB412E7AC3,
	HolisticAnnotationController_Clear_mE09E7EB6A1FD6B53379478032668D28442F9938F,
	HolisticAnnotationController_Draw_mDFECD1E821A5798CAB4FC454B191315D6F15A18A,
	HolisticAnnotationController_GetIrisLandmarks_mC27C93139318881288CDCCC11B3CC3A7E517ED39,
	HolisticAnnotationController__ctor_m8135EB93782202E61F0D9EC353E6E63E2EECF18F,
	HolisticGraph_StartRun_m3AF465C9862A27EE543518BE2033E9D8B3C6E861,
	HolisticGraph_RenderOutput_m5BDFCEE30E82A17726B67FE21E16BD7DF839D4BD,
	HolisticGraph_FetchNextHolisticValue_m88A3C8C7803BCF1659EDB015875B7BEFC6253EC0,
	HolisticGraph_FetchNextPoseLandmarks_m389992D029187AFB332AC67E9D42AE4FD9893D31,
	HolisticGraph_FetchNextPoseWorldLandmarks_m4BAAE4DDDF5BEF3FFB6EA04CD0231E78DE99EB4D,
	HolisticGraph_FetchNextPoseRoi_m677C49433FC941F6494EBE0F63AA77D14217BE7D,
	HolisticGraph_FetchNextPoseDetection_m2FDB0F8D7B727DBDCA64A4C089956FF480A58884,
	HolisticGraph_FetchNextFaceLandmarks_m1A3BD8D3D170058A91A79C0234BCC47A24874B6B,
	HolisticGraph_FetchNextLeftHandLandmarks_mBBE573B692D7E1E73E6FFAF31F1534F7CE9AB2B1,
	HolisticGraph_FetchNextRightHandLandmarks_mFCFFCA84C646DF19222C31F471400CE403D60769,
	HolisticGraph_FetchNextPoseLandmarksPresence_mCF41735F44F6FC34EBEFF72E4BB1BF1B9CAFB90B,
	HolisticGraph_FetchNextPoseRoiPresence_m6B70B0D47FD067C229F2E6C8C30ED3AF5646E793,
	HolisticGraph_FetchNextPoseDetectionPresence_m58B111E2433FA4470DDA21DC1CBF033142CD0340,
	HolisticGraph_FetchNextFaceLandmarksPresence_mA1FE2E3A30E8113B6E327863D253FC56034A9E42,
	HolisticGraph_FetchNextLeftHandLandmarksPresence_m7ADDB03B13B1448BF73C99A82F2958C24E941666,
	HolisticGraph_FetchNextRightHandLandmarksPresence_m22E186A2AE47B8ED02C316BA9B14038BF656A33D,
	HolisticGraph_RenderAnnotation_m4974D09C2EDC6FD5314148FF29C154332403B1BA,
	HolisticGraph_PrepareDependentAssets_m23E3BC5356EBFF85A0B683359665CD8D752992F0,
	HolisticGraph__ctor_m42CB0A1C84ED44455B407D0F8E0AB7364664D99B,
	HolisticValue__ctor_mC75FD6F6CC3584DB53A691941CE65B3DC585C42F,
	InstantMotionTrackingGraph_Start_m6B7D35C57B7A83FC0E599F187B87DB77DCD07F72,
	InstantMotionTrackingGraph_Update_m100CC554C0AB2FDB6B6756C254D67D8D17C1CEEF,
	InstantMotionTrackingGraph_StartRun_mD52137BFAB5A216A5ADE1C62225C98A6A864460F,
	InstantMotionTrackingGraph_PushInput_mD405B0097BE5CBCB5AEEADE00061AF4D425718B6,
	InstantMotionTrackingGraph_MatrixCallback_mC846E6B8E080F5495EE9AB511DA6C35EFBC65C6D,
	InstantMotionTrackingGraph_Matrix4x4FromBytes_mE0EB0340C7ECB48AAC14F50959DF348826CD7881,
	InstantMotionTrackingGraph_GetVerticalFovRadians_m85820F4FBFCD635331401289BBD9FD1B920C8AC2,
	InstantMotionTrackingGraph_GetImageFrameFromImage_m3A12662C545CCC06DC90AA1118D7F27E26CE0011,
	InstantMotionTrackingGraph_ResetSticker_m8A515FF66BB7217424902E847EBA816EE5C7205E,
	InstantMotionTrackingGraph_UpdateImuRotationMatrix_m74BC0FBA4310E421A1CA1AF6A5FADD098ABFBEEE,
	InstantMotionTrackingGraph_FlipTexture2D_mEA2FF99F078D2F0804278E3C12E2ACB0B6AA22EA,
	InstantMotionTrackingGraph_PrepareDependentAssets_m6B9EAFFA09A77A90CAB4BDD76CD0D3FD7F19E52F,
	InstantMotionTrackingGraph__ctor_m60F455C983471F8512D9CCE338BF2E036F1F5BD3,
	IrisAnnotationController_Awake_mCBD4F8DB103291BE473D069AB0FE1FF953673268,
	IrisAnnotationController_OnDestroy_mC0CA37E3CE952A8341D6C5402856A92321B6F2CC,
	IrisAnnotationController_Clear_m5600D82164EC6B7A72AE5EAB45B20ACF8659D9AA,
	IrisAnnotationController_Draw_m1771EED6E49D9678FC19BB91A7B0CF7686B830FC,
	IrisAnnotationController_DrawIrisCircle_m54EDDA39C32AEF180611BCDED064C168DEA80F72,
	IrisAnnotationController_GetIrisRadius_m77C90A814AE7B701899E390194467780A0277CF2,
	IrisAnnotationController__ctor_m6864D88B80AB6E1F0C52B789AA8D8EEC2934F07E,
	IrisTrackingGraph_StartRun_mDDF5E8379106CE3CA11149CCE6B018C47A42C91F,
	IrisTrackingGraph_RenderOutput_m7F1F9396964E548BCE27FE22A694427A334E08DE,
	IrisTrackingGraph_FetchNextIrisTrackingValue_mA4209A848C61EB8EC6DE9EDA608DFC997CA14E2B,
	IrisTrackingGraph_FetchNextFaceLandmarksWithIrisPresence_m567378ED3111EDC748166CA79F4892F75A49AEB1,
	IrisTrackingGraph_FetchNextFaceLandmarksWithIris_m86D179E7D566A2FE154DC1521EC8BCC60BC974EF,
	IrisTrackingGraph_FetchNextFaceRect_m595446762254469BB7310C03F1A17DF92760DA66,
	IrisTrackingGraph_FetchNextFaceDetectionsPresence_mEFCCE102F66A0B4F9023DC6139353C3534ECE06F,
	IrisTrackingGraph_FetchNextFaceDetections_m23B455C0231140E6E32D86791170A6EF907C7295,
	IrisTrackingGraph_RenderAnnotation_m99CA3919854118FCA929F73170BBDBB5FDD97152,
	IrisTrackingGraph_PrepareDependentAssets_mAF007E19150FE5B76FC8870B815AFB34DAE4B6A1,
	IrisTrackingGraph__ctor_mF86F5938345EB035FFE594C472D641D282DFD639,
	IrisTrackingValue__ctor_m3F78E2CDF7AC15CD061B0FB735C959574721A745,
	IrisTrackingValue__ctor_m196EDBB60F4DCA0C4910EC449B9E0F9AC778F15C,
	IrisTrackingValue__ctor_m2B8DC9C88267D37FF3EA199563F70903EA770EC5,
	ObjectDetectionGraph_StartRun_m62A7E7900F4A5B8D2DCA0877D2FDEC65D7E06F47,
	ObjectDetectionGraph_RenderOutput_m51C5EF12F378984A6E8D2FDEC77D97ECCF012E03,
	ObjectDetectionGraph_FetchNextOutputDetections_m46FA59531138924694BA2F3A75245036C212E9EC,
	ObjectDetectionGraph_RenderAnnotation_m0098604F5FB04C68EC148E303BB4097FD03672B6,
	ObjectDetectionGraph_PrepareDependentAssets_m7C97A18E1C173DCCF361360CAC622380618CD818,
	ObjectDetectionGraph__ctor_m16EFCDA346D904589A632E5477F0A76119F3AE18,
	ObjectDetection3dGraph_StartRun_mA240ADFC153ED97C5A74AA93C2E78456DC3769B2,
	ObjectDetection3dGraph_PushInput_m6008766FAD2785E931B497F83512E6AC177918CD,
	ObjectDetection3dGraph_GetImageFrameFromImage_m19CF9F35EDC3E5EF78069194E6129C93F82742F7,
	ObjectDetection3dGraph_FlipTexture2D_m089C56342484C7F31E83EEABF41152523DDD3950,
	ObjectDetection3dGraph_GetAllowedLabels_m14ED25AF19BFE27CE3DD6AD03CDA6D15845106C0,
	ObjectDetection3dGraph_GetModelScale_m80A123D7684EF97F9BB27E5E25411BCC667ACFBE,
	ObjectDetection3dGraph_GetModelTransformation_m2AA0F65B970FA94B34AAFC106142D98181D210B9,
	ObjectDetection3dGraph_GetObjAssetName_m40BEAB5BBC10E9F1CB8F84C6C8818C7EDEA9B169,
	ObjectDetection3dGraph_GetModelAssetName_mBFA528CCC8BF9525D69A85B0EC683573FBA4F065,
	ObjectDetection3dGraph_PrepareDependentAssets_m8391FDEF049B0D2346E3EB64D2C23EBC0F37D4B9,
	ObjectDetection3dGraph__ctor_m9F8E2D15BCA161A56B159E644FEDA256566CD364,
	OfficialDemoGraph_Initialize_mCE1E30992E53B89B3134809E6F435745DFE85A9F,
	OfficialDemoGraph_StartRun_m58FEDDCFFC06D4C1C36746EAE6FB1F91C206A826,
	OfficialDemoGraph_StartRun_mC9EF60EDC6E347A0EA52462B2902B4B747576263,
	OfficialDemoGraph_RenderOutput_mD0D09C4A260679E602F50E0CF3E876C1A69EB0B9,
	OfficialDemoGraph_SetupOutputPacket_mE4CF689CE4B6663118F56AF719520197E8680E20,
	OfficialDemoGraph_BuildDestination_m8A2E396648876205DF379CDB3B2C198AB6A5E70D,
	OfficialDemoGraph_OnReleaseDestinationTexture_mC9040C05CB2B2BC7C3D8126BCCFD6FF9B24B199D,
	OfficialDemoGraph_PrepareDependentAssets_m01839BFC5F2185A99A54C059AA21A95552FE6F6F,
	OfficialDemoGraph__ctor_m1F421EBB594E8107461FA2687A2EEC44516F6E16,
	U3CU3Ec__cctor_m33E315325F655BADAD28838760E49C791F583EAD,
	U3CU3Ec__ctor_m91E14F7FC3F9E20F9473106B54EECA4EB72F6CC4,
	U3CU3Ec_U3CInitializeU3Eb__7_0_m7B0DB7824359E516D3D061B8D2F9A5E6629EB1B4,
	PoseTrackingAnnotationController_Awake_m711CCDD327DA4DC434A4482B875DFEC9B47833DF,
	PoseTrackingAnnotationController_OnDestroy_mA647A0C518036FD1C95C3923552213ADEB587BC3,
	PoseTrackingAnnotationController_Clear_mC0D72A4DE44BB85139FBFF42F8EAEE1023ACDA8E,
	PoseTrackingAnnotationController_Draw_m672A79018C521DC19EF92E91925ED33470A16A29,
	PoseTrackingAnnotationController__ctor_m63EF25498045010947C00A1A16E24F00406199D8,
	PoseTrackingGraph_StartRun_m14C2DD7B3A0A8AAF4E011F7C3AA4A12A93E78E32,
	PoseTrackingGraph_RenderOutput_mECB1FD783B4954D4B65C6E16CB974CCAB2E9D1F8,
	PoseTrackingGraph_FetchNextPoseTrackingValue_m90BCC9EE17D3B5EC08B751C17C678906DD762CAE,
	PoseTrackingGraph_FetchNextPoseLandmarks_mD8F03F2C450DEBDFB98E1DDC55BEDA0F5B7B65F7,
	PoseTrackingGraph_FetchNextPoseWorldLandmarks_m5783D9CA08442AA68308EA76788234936E09ECE5,
	PoseTrackingGraph_FetchNextPoseDetection_m582B8CF649AD78EE0C2B7447C309FB188ECA3225,
	PoseTrackingGraph_FetchNextPoseLandmarksPresence_m38BDBD7699857C953E2BAF7A8C399247D40E0AF6,
	PoseTrackingGraph_FetchNextPoseDetectionPresence_m1D354F04B2BC51A4D12FCCC55B50D6CCFB70CD14,
	PoseTrackingGraph_RenderAnnotation_m3DA6AA32F70F1ED93F8F28E976B8FEF9B5DE9590,
	PoseTrackingGraph_PrepareDependentAssets_mC049BB7935FED0C3E5D84FB509630ACCCB79757D,
	PoseTrackingGraph__ctor_mC5CC427988CA38D2CD3A28C6C46D47B62A8EE96D,
	PoseTrackingValue__ctor_mA6D1C4B0308B8E4A72D22EF8C983F4316EFEFADD,
	PoseTrackingValue__ctor_mD3CC38B61D481FBD54EFF3D9B4021DE0BB88BBFA,
	PoseTrackingValue__ctor_mF14BE4257DE1256E556DC4569E78F0FC9EBF9E7C,
	DemoGraph_OnEnable_mE8A25FFDE70FDB756A52F120C51AB08E6E9167C9,
	DemoGraph_OnDestroy_m81C1A93A5C931FD23902C75A0A6DD841408C2064,
	DemoGraph_Initialize_m5AE4999C25882FA45DFADA977C00A90780983158,
	DemoGraph_Initialize_mEB929274E6A01E61198C30005AAB6A6C1EDF8496,
	NULL,
	DemoGraph_StartRun_mDF368E565F1CA198E168B3024368498201D1840D,
	DemoGraph_PushInput_mC9D77CEB41EEEFBDF54716F84494D42BB20DA54A,
	DemoGraph_PushInputInGlContext_m2000278CAE8C8F4281A45E55684BD7498AEE66E7,
	NULL,
	DemoGraph_Stop_mBAE75F98D62479F19617DE292F318831786CE3CF,
	NULL,
	NULL,
	DemoGraph_IsGpuEnabled_mD652E3C58A96FC837513E32821CC81E3B53E6E3B,
	DemoGraph_GetConfig_m11C8089683027904D506742D9F1BD707312A2B1D,
	DemoGraph_GetCurrentTimestamp_mDF2B222E428D6B20BA51AF5575A2DE521442C55E,
	DemoGraph_PrepareDependentAssets_mBB767E839F2DDB4174EC0B4792AB5315E69AC451,
	DemoGraph_PrepareDependentAsset_m34E9D4AB58D3F93104CBCF31BC8EAC22E48A8A6A,
	DemoGraph_PrepareDependentAsset_m47F83CC86EF233C5A71909610C8B49F2D817314A,
	DemoGraph__ctor_mA23100AF178FD3650B48A9D7EA24328A9D323010,
	DemoGraph__cctor_mE957BEE2AF55339F6D009A10A78F0C7B85701389,
	GraphSelectorController_Start_mD91D0BBB84C06B0C21E1CE9F6130034FB7FA3385,
	GraphSelectorController_InitializeOptions_m4609C975D121612A7D83A7B38F2734379309D8C2,
	GraphSelectorController_AddGraph_m57AE5AAF597BEEC95D131C6ABA994261AA165F93,
	GraphSelectorController_OnValueChanged_m9E1AB5ACF9B106EB7B4BD40E70AACAC566700075,
	GraphSelectorController__ctor_m50F2A7F3E49332E5FCB69883BADED34C5BD6516A,
	U3CU3Ec__DisplayClass14_0__ctor_mDA752D5F05BE71E0CE0721C76EEC75AADF18BBAB,
	U3CU3Ec__DisplayClass14_0_U3CStartU3Eb__0_mE03A849834B76FA788F35CFB00240F4F8466B3CD,
	U3CU3Ec__cctor_m074B0DD6EB3F0513707C18EB8CC36BDC3E9AD660,
	U3CU3Ec__ctor_m38A174A8D48807AFBD2B08B32FF893708FF80E81,
	U3CU3Ec_U3CInitializeOptionsU3Eb__15_0_mA9BCB57D32A8943FDF2D0E7B00FE5C7AA79F4F9A,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	AssetBundleLoader_Start_m314A89ADD54444061951121B8A92B3BF6FC0716C,
	AssetBundleLoader_OnDestroy_m8923F1ED479883407A3CC3A4CF70F2A1E8484154,
	AssetBundleLoader_PrepareAsset_m16D0132427E3FEF28A4662727BFF8894B0D87CA6,
	AssetBundleLoader_PrepareAsset_m173898ED2C7C6E68E9231E7A06FEE991C678189F,
	AssetBundleLoader__ctor_mB970FD154AC04446AB9A5A3E2E37E6B21C9E9E4A,
	NULL,
	NULL,
	AssetLoader__ctor_mE56652CAAC8D47BEE3322D066093FC684FF1AFD9,
	LocalAssetLoader_Start_m3D815416B246A7EA5F38412A5251A2FAA9A16ED4,
	LocalAssetLoader_PrepareAsset_mF2E45803F3FD224B727AEC994B6B297178DA08AC,
	LocalAssetLoader_PrepareAsset_m4EDFAF906DDC74A9BE113E7EEA6CCE0D39A153D3,
	LocalAssetLoader__ctor_m03F5DF97295A2CA25DE6576B19E89640251B12E3,
	TextureFrame_get_width_mB17FE2BFBE4D72D820BE811F28970E4DF2152286,
	TextureFrame_set_width_m8341215AAEEE53D7BF045E837978BB36DC498509,
	TextureFrame_get_height_mE5C87876C38DEFA9AC9E5CBC26F1FEF36F4D3C01,
	TextureFrame_set_height_m2B6DC0460751E6E07DDA94C4469DA7EFCA6559F3,
	TextureFrame__ctor_m618693D401DF10E2BD5AC1FDB7EAA3F5E2A12B9B,
	TextureFrame_CopyTexture_mB5838078D4D4ABE454CE4376EF15FED5DBDDD483,
	TextureFrame_CopyTextureFrom_m81118EA2BE3568462D439C38A880A0E280B5DFCA,
	TextureFrame_GetPixels32_mD2BC248BE24C1D5D80C197B37D4E1F72B60291B4,
	TextureFrame_GetRawNativeByteArray_m0A487F97A83BDCF4B6E99156169E5095787F0999,
	TextureFrame_GetNativeTexturePtr_mEEACFA81137F86A70792ABA8DAB274BB5D1BAF7A,
	TextureFrame_get_gpuBufferformat_m0A151608C3DE107C27B74B1A91EEFB87D42C0162,
	TextureFrame_Release_m5FD86ED849C0A725FD81BA1571EB42FBEB6EC443,
	TextureFramePool_get_frameCount_m1EEE73F9CE3F37B8BFA654D190DF8643B3EA80C6,
	TextureFramePool_Start_m9E7FE1C938B1EC3AE6F1F27391A8C19D3A736901,
	TextureFramePool_SetDimension_m6EA1E9F5A7FBCF369406C0471642ACFEFE49B366,
	TextureFramePool_RequestNextTextureFrame_m650F62C47E3514D5B40477D93D28C5B77B7D5D42,
	TextureFramePool_OnTextureFrameRelease_m3F46346F6E43543DD9BE828528E788F0066D9B6D,
	TextureFramePool_IsStale_m813BCC0E0D81350A198B851F3D1BFEE67EEA4559,
	TextureFramePool_CreateNewTextureFrame_m32BFEE26C9DF9D680B7D231E8BF59303671A4F51,
	TextureFramePool_WaitForTextureFrame_m529EA3FF190A1A49B5FBAA2C223A9FAABA01CA64,
	TextureFramePool__ctor_m7E948FF5CE6609BB9D01BC41532BFEB818368649,
	TextureFramePool_U3CWaitForTextureFrameU3Eb__14_0_m67C6BA48945B02E3183AB2EF7B18D3062D1A388A,
	TextureFrameRequest_get_textureFrame_m16903FC7DAE1826ADC2FA455E6FC22164C9A3B6A,
	TextureFrameRequest_set_textureFrame_m9144A4180722C034BC62972B4782BA63D4932740,
	TextureFrameRequest_get_keepWaiting_mAE570C818C78FC40B06D2C9952B123BB82D95B31,
	TextureFrameRequest__ctor_m3D4B7B289DB257150776374D6FD34A1BBC47ADFE,
	U3CU3Ec__DisplayClass7_0__ctor_mA4E7764618D2B8580AA1984002395165B65BD0B0,
	U3CU3Ec__DisplayClass7_0_U3C_ctorU3Eb__0_mF5C499D1FF07B38DE384EA7D6D2F987AEFD03BEA,
	U3CWaitForTextureFrameU3Ed__14__ctor_mF771738CC4E23FBA80292BA09F52FD4F8D6B9D28,
	U3CWaitForTextureFrameU3Ed__14_System_IDisposable_Dispose_m0856FDFBE77BC309EFB4E259C2C1F5C449608973,
	U3CWaitForTextureFrameU3Ed__14_MoveNext_m714F156265E3DF6D1B6C7A9E668828021494862E,
	U3CWaitForTextureFrameU3Ed__14_U3CU3Em__Finally1_mD0E118D793FECA85E0146D2D2F3880164CD0DFC6,
	U3CWaitForTextureFrameU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m53532F7067493675A7491BEBD5F356D352EFD9D6,
	U3CWaitForTextureFrameU3Ed__14_System_Collections_IEnumerator_Reset_m46689A7498CC85E7622FE1AD0C2735D6CEF2AC20,
	U3CWaitForTextureFrameU3Ed__14_System_Collections_IEnumerator_get_Current_m1D796BDA08361D499F44C847EF0197DE55C3D817,
	SceneDirectors_OnEnable_mCBD92A542FF77CB1B4309276BA24ACC84607E8D2,
	SceneDirectors_GetCurrentContext_mCCE71B73479FC8A1F8D65066ED794FE54AF5401C,
	SceneDirectors_Start_m38CF084668B0A46498DC296BF25AFF997CC8CA8E,
	SceneDirectors_OnDisable_m963B803D2C7E4B5905504CD2B0D7B4A443174E78,
	SceneDirectors_ChangeWebCamDevice_mFCE77CD4BDEEBF3C5367A3CA155F8A92A8842D89,
	SceneDirectors_ResetCamera_m6ABD1DBF5D0E3C6AABFB71728CB51B3CEFAC6E9E,
	SceneDirectors_StopCamera_m5AB26AFF4AFA1287CC8BCFB177EF3E3398B0F883,
	SceneDirectors_ChangeGraph_m37E90B817E010C6B07DE7FB78E726FB46B57CC44,
	SceneDirectors_StartGraph_m7EFFEBA32E6BEDDC3533DF445CA96D9EC7BDE000,
	SceneDirectors_StopGraph_m491AECC533ACAD2F35910BBAC23520A69C8204EF,
	SceneDirectors_SetupGpuResources_mD56D11263A6E7302B82A16BA173601BEA7E4CC38,
	SceneDirectors_RunGraph_m538FB86082B4D6CC9D2871496FD8F79ECF0E7DFC,
	SceneDirectors_WaitForGraph_m336B2A27DAC594C63F79DBC26A63439A4E1908A2,
	SceneDirectors_WaitForCamera_m275D49CD83631A2DCB68FB4B48C8E7F6CB350E44,
	SceneDirectors_IsGpuEnabled_m143E1BD1CED99A16BE648101D9A85F3079A8DD8A,
	SceneDirectors__ctor_mF12F0D145BBDF0D0019647DA938A5D183383E3C3,
	SceneDirectors__cctor_mFE88EF865916DD4BB9BB603DE50C013440231745,
	PluginCallback__ctor_m61BF52D1A0D7864A3261FD73EA7717DD28A381F3,
	PluginCallback_Invoke_m6EF75B25BBE9D45490735A69014EA486DE6A07B5,
	PluginCallback_BeginInvoke_m8A935FAE202242AF0447F3EE1AA3DBA51EF3085A,
	PluginCallback_EndInvoke_mB86BE68FBF7572990FCEEB018C3F66BDFD54D57B,
	U3CRunGraphU3Ed__24__ctor_mD8E548AF383C56A2B2A8AEF57581758506147835,
	U3CRunGraphU3Ed__24_System_IDisposable_Dispose_m1A65857C24F53A20E78ACA865CEF9BC40FA2FCF8,
	U3CRunGraphU3Ed__24_MoveNext_mA48529878CC074107801B30928AB6A835A869470,
	U3CRunGraphU3Ed__24_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD2AD458E8533A88202382ACC60A91B838B2DDBAB,
	U3CRunGraphU3Ed__24_System_Collections_IEnumerator_Reset_mF598EC361ACE423F3AAA808AE02A3FFD0799AB53,
	U3CRunGraphU3Ed__24_System_Collections_IEnumerator_get_Current_m0F69806467AF047BC3C6FB4F9EBD727FD3E1E9A8,
	U3CU3Ec__DisplayClass25_0__ctor_m1BE01B678D510B8DCBB9967532F507E1BC902BFD,
	U3CU3Ec__DisplayClass25_0_U3CWaitForGraphU3Eb__0_m165D9F8B6F3FF9F1280CD5551693A0BB897224EB,
	U3CWaitForGraphU3Ed__25__ctor_m5CA156DE5E91E041B8EE6C3B206858ED6B2FDE62,
	U3CWaitForGraphU3Ed__25_System_IDisposable_Dispose_m4AA842376297CE56575BA85E691DD6D60B254E78,
	U3CWaitForGraphU3Ed__25_MoveNext_mB0726717B2E9F8348367DA00A1A98B5A281695C9,
	U3CWaitForGraphU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF0E9DF6BE3D9F085FA4BB436AAC9E15332D245EC,
	U3CWaitForGraphU3Ed__25_System_Collections_IEnumerator_Reset_m04AE7F2FE9D453891514E4C6FED67FFD1876A5A5,
	U3CWaitForGraphU3Ed__25_System_Collections_IEnumerator_get_Current_m49604FA593536760B6C891E6A0A1D2CBAEA241E9,
	U3CU3Ec__DisplayClass26_0__ctor_m5D09D9E181E00F6F2D9EE7BE9DE1345B95CB6156,
	U3CU3Ec__DisplayClass26_0_U3CWaitForCameraU3Eb__0_m34CFB2A116353E776417F2F5C61902DB0D22C4BD,
	U3CWaitForCameraU3Ed__26__ctor_mEF0BA9234B5326F8D1DA18B5BC3D54C2EDB621A8,
	U3CWaitForCameraU3Ed__26_System_IDisposable_Dispose_m798B59916A7F7B04C2473B999C112FAA56D2E800,
	U3CWaitForCameraU3Ed__26_MoveNext_m35011ADE6AC38B7DF2212CE5384222ADCB119599,
	U3CWaitForCameraU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF9C3E046BC69EEED73E7E43B6788A781A09AFC3F,
	U3CWaitForCameraU3Ed__26_System_Collections_IEnumerator_Reset_m3CB2E113834F80E3427C87910C5CA706FA5DDA4B,
	U3CWaitForCameraU3Ed__26_System_Collections_IEnumerator_get_Current_m6590C064728CBBE508A79AF2027ABFBBF28B93B2,
	WebCamDeviceSelectorController_Start_m519A2B7FD0AA40F570C8EC6EF6A0AEEB057560F2,
	WebCamDeviceSelectorController_ResetOptions_mB4740F2C8BAF7C189219F39D5F986D76615597AC,
	WebCamDeviceSelectorController_OnValueChanged_m80AE65850AD7B8966C390B2572F447AAAA698EC4,
	WebCamDeviceSelectorController__ctor_mFF483BA8241138EA65FDC1E8ABD012D2D27FB3FF,
	U3CU3Ec__DisplayClass2_0__ctor_mF3418C9C0D34BAD9890879D9D68E5978D8217583,
	U3CU3Ec__DisplayClass2_0_U3CStartU3Eb__0_mEB2BFFCFAC5100A1ABAA8717E506DE12FC048F29,
	U3CStartU3Ed__2__ctor_m906CABD8C45330E13A122514A7C09E2EC5FC0039,
	U3CStartU3Ed__2_System_IDisposable_Dispose_m0139BD6575B1E2FA511461685412C8259DCA96E1,
	U3CStartU3Ed__2_MoveNext_m136CCB629E85380FC4C25CFDB823994C8B179176,
	U3CStartU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC9597C9A7823BD016D2A3C49B00789A8CB933A45,
	U3CStartU3Ed__2_System_Collections_IEnumerator_Reset_m3B48B6C9ABBB439192150CDDCD2EFF1DE7DFC150,
	U3CStartU3Ed__2_System_Collections_IEnumerator_get_Current_mD2E5B1382AFEAA92756EEC8CEF1A480E6EE3F82D,
	U3CU3Ec__cctor_m3E75619C4AE56C7338911687731CB5F9DA67388C,
	U3CU3Ec__ctor_m4CC6FB973E54D9EC805CB8585256E36041A9CD0D,
	U3CU3Ec_U3CResetOptionsU3Eb__3_0_mD0F6B7F5AE75B9B5AA967D46F06869613F4FE7BA,
	WebCamScreenController_get_isPlaying_m0E549AEE2590F3C44B0A7024F6B9935A8D030DB8,
	WebCamScreenController_get_isWebCamTextureInitialized_m1F8E924BE952001D20EB6C33FEB263B89FD26A43,
	WebCamScreenController_get_isWebCamReady_m3E7B97B3CC805F75101BD695CD71B5C8805E10AB,
	WebCamScreenController_ResetScreen_mAE3830893EA75A8259087B49F90DE6FC58162FE6,
	WebCamScreenController_GetFocalLengthPx_mC17048586BA498F9E296F5D9E31FAECE4E2BDA81,
	WebCamScreenController_GetPixels32_mC2ABD47CAA36A512745F08C845F811A763930347,
	WebCamScreenController_GetNativeTexturePtr_m2D55BC26BAEF943A94E34F609F956683E939456A,
	WebCamScreenController_GetScreen_mE2672C011854115B4405400233271228C9501F2B,
	WebCamScreenController_DrawScreen_m81777A74A970CCF3B5B1E937CC17B73983B2EA14,
	WebCamScreenController_DrawScreen_m66665EB5E695F86D3B1412C6A41758524756B95D,
	WebCamScreenController_DrawScreen_m9CAD950BEA882BD8BED018A3A45269CC508AE3D4,
	WebCamScreenController_DrawScreen_m60FD0893746CB0EBA14FB886F24A384B2D0F9933,
	WebCamScreenController_RequestNextFrame_mFCB56335FA67C76752A1473A0E421A5D1EBA0711,
	WebCamScreenController__ctor_m564A79DFC42C3283DC7C8B5195A1BFE8AB46B283,
	WebCamScreenController_U3CRequestNextFrameU3Eb__25_0_m8AB50133A528C658E544CB7ACE354C0728F7971D,
	WebCamTextureFramePool__ctor_m3DC7CC1F2FB9290A51CF95A97F527C765E6C2138,
	U3CU3Ec__DisplayClass16_0__ctor_m2CF670D61D38970A791AA2A0EB39265CD742F921,
	U3CU3Ec__DisplayClass16_0_U3CResetScreenU3Eb__0_m885265E8AFDFCA117465873E143DF4338AAE43F3,
	U3CResetScreenU3Ed__16__ctor_mDAD1FEE5686D6597DCB7579537D2EB62649F79D9,
	U3CResetScreenU3Ed__16_System_IDisposable_Dispose_m7C7966A9BC91BB8B9411C59299BE4A64AD4F3F73,
	U3CResetScreenU3Ed__16_MoveNext_mB725E61B4C070637F15D48DF6752994D0C0C4D51,
	U3CResetScreenU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4832D1E17D71F3ECAF696FCCCF39237400C816C3,
	U3CResetScreenU3Ed__16_System_Collections_IEnumerator_Reset_m60226C6644A0E904B746230543DABF1C5BCD4336,
	U3CResetScreenU3Ed__16_System_Collections_IEnumerator_get_Current_mB8C7F8DC2FAE2DD8A27D76DF2D91AB5F7B2AA38A,
	PoseEstimation0fBody_Start_m91FA846628A342754D6E87BAE1F5D0441E120AFF,
	PoseEstimation0fBody_InitializeOptions_m66749367C2607A165DE4EA0C1E67671E12646798,
	PoseEstimation0fBody_AddGraph_m9CF7DEB41674F5E4A7FFD2598E6911DF0D9715B0,
	PoseEstimation0fBody_OnValueChanged_m248B29D7F3B1B20199C11115CA5AE9F335283A80,
	PoseEstimation0fBody__ctor_m97DA44F36B6C41703D5F9247B12EC0F88AC0441B,
	U3CU3Ec__DisplayClass3_0__ctor_m4D9074C547A5900BBD37FC9B1640020C85D6DDE9,
	U3CU3Ec__DisplayClass3_0_U3CStartU3Eb__0_m36BA8EA8C57247BEBF8E2509096254804786DBCE,
	U3CU3Ec__cctor_mA277C683B4DCAA5C52C178D2AEEF38A185087D55,
	U3CU3Ec__ctor_m245D725199C98168448F7AF2C845415ABD16C300,
	U3CU3Ec_U3CInitializeOptionsU3Eb__4_0_m70691D0D9209490C781B535F269985B3F22F338C,
	PoseToCharacter_Start_m94450E96DA400CF785CC05ECB5C74D3D7197982F,
	PoseToCharacter_Update_mA8E1D20A3013884FDB7458131F6A93E659CCA3EE,
	PoseToCharacter__ctor_m3B9F9F88F1289CBCACBBB7F27C0E820D803605BD,
	FaceMeshAnnotationController_OnDestroy_mCAF09B0DDD5183D7F7035B9B3642EB44206181D8,
	FaceMeshAnnotationController_Awake_m6FED787F540E20CA63DEAE2B717F06795EFD7051,
	FaceMeshAnnotationController_Clear_m4E71BF85112EBF5AE41CF06E24E6C7D29578A1AC,
	FaceMeshAnnotationController_Draw_m3E16B0F8AA1C73F609976EA805F455379AE71DD6,
	FaceMeshAnnotationController__ctor_mE67164411DC0053C7A15E2B46F244E83095EC01F,
	IrisTrackingAnnotationController_Awake_mDE918CD348065F148A3DC255C060E10A483F31C4,
	IrisTrackingAnnotationController_OnDestroy_mA14BA87DDFA8B39BA605758A79BE21D74A399C9E,
	IrisTrackingAnnotationController_Clear_m35985209DC5C93D0C92276804464BECD6C446915,
	IrisTrackingAnnotationController_Draw_m942952474ECADBEA9F7133E011FBC00E6EA18C9D,
	IrisTrackingAnnotationController_GetIrisLandmarks_m9514410F016CF22279C0FF620120227916377F7E,
	IrisTrackingAnnotationController__ctor_m0C8345BCC991BA7985138F9AA3D32CDD2E20E4FA,
};
static const int32_t s_InvokerIndices[375] = 
{
	1450,
	1450,
	1416,
	800,
	1436,
	1416,
	800,
	1450,
	1450,
	1416,
	800,
	1416,
	1436,
	1416,
	1416,
	1436,
	1416,
	800,
	1450,
	1450,
	504,
	800,
	1450,
	505,
	1450,
	1416,
	800,
	1416,
	800,
	1450,
	1450,
	1450,
	1450,
	1450,
	114,
	1450,
	1416,
	800,
	1416,
	1416,
	1416,
	1416,
	1436,
	1436,
	1416,
	800,
	1450,
	1450,
	336,
	505,
	1450,
	1450,
	1450,
	1450,
	1450,
	1416,
	1450,
	1235,
	1450,
	1436,
	1416,
	1450,
	1416,
	1450,
	800,
	1416,
	1002,
	1002,
	800,
	1450,
	1450,
	1450,
	1450,
	1450,
	1450,
	35,
	604,
	1450,
	1416,
	800,
	1416,
	1416,
	1416,
	1416,
	1416,
	1416,
	1416,
	1416,
	1436,
	1436,
	1436,
	1436,
	1436,
	1436,
	800,
	1450,
	1450,
	113,
	1450,
	1450,
	1002,
	1002,
	2330,
	2334,
	1439,
	1002,
	817,
	1244,
	1244,
	1450,
	1450,
	1450,
	1450,
	1450,
	505,
	505,
	143,
	1450,
	1416,
	800,
	1416,
	1436,
	1416,
	1416,
	1436,
	1416,
	800,
	1450,
	1450,
	504,
	800,
	1450,
	1416,
	800,
	1416,
	800,
	1450,
	1450,
	1002,
	1002,
	1002,
	1244,
	999,
	999,
	999,
	999,
	999,
	1450,
	1450,
	1450,
	1416,
	1002,
	800,
	1244,
	2442,
	2212,
	1450,
	1450,
	2458,
	1450,
	1109,
	1450,
	1450,
	1450,
	337,
	1450,
	1416,
	800,
	1416,
	1416,
	1416,
	1416,
	1436,
	1436,
	800,
	1450,
	1450,
	800,
	1244,
	1450,
	1450,
	1450,
	1450,
	800,
	1416,
	1002,
	1002,
	2442,
	800,
	1450,
	-1,
	-1,
	1436,
	1416,
	1416,
	1450,
	505,
	802,
	1450,
	2458,
	1450,
	1450,
	800,
	1244,
	1450,
	1450,
	1235,
	2458,
	1450,
	992,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	1450,
	1450,
	505,
	802,
	1450,
	505,
	802,
	1450,
	1450,
	505,
	802,
	1450,
	1405,
	1235,
	1405,
	1235,
	475,
	1244,
	1244,
	1416,
	1364,
	981,
	1405,
	1450,
	1405,
	1450,
	735,
	1002,
	2212,
	2380,
	1416,
	1002,
	1450,
	1436,
	1416,
	1244,
	1436,
	800,
	1450,
	1244,
	1235,
	1450,
	1436,
	1450,
	1416,
	1450,
	1416,
	1450,
	2418,
	1450,
	1450,
	1202,
	1202,
	1450,
	1244,
	1450,
	1450,
	1450,
	1416,
	1416,
	1002,
	1436,
	1450,
	2458,
	799,
	1235,
	396,
	1244,
	1235,
	1450,
	1436,
	1416,
	1450,
	1416,
	1450,
	1436,
	1235,
	1450,
	1436,
	1416,
	1450,
	1416,
	1450,
	1436,
	1235,
	1450,
	1436,
	1416,
	1450,
	1416,
	1416,
	1244,
	1244,
	1450,
	1450,
	1235,
	1235,
	1450,
	1436,
	1416,
	1450,
	1416,
	2458,
	1450,
	1008,
	1436,
	1436,
	1436,
	993,
	1439,
	1416,
	1407,
	1416,
	1244,
	1244,
	1244,
	1244,
	1416,
	1450,
	1244,
	1450,
	1450,
	1436,
	1235,
	1450,
	1436,
	1416,
	1450,
	1416,
	1450,
	1450,
	800,
	1244,
	1450,
	1450,
	1235,
	2458,
	1450,
	992,
	1450,
	1450,
	1450,
	1450,
	1450,
	1450,
	171,
	1450,
	1450,
	1450,
	1450,
	171,
	604,
	1450,
};
static const Il2CppTokenIndexMethodTuple s_reversePInvokeIndices[6] = 
{
	{ 0x06000067, 4,  (void**)&InstantMotionTrackingGraph_MatrixCallback_mC846E6B8E080F5495EE9AB511DA6C35EFBC65C6D_RuntimeMethod_var, 0 },
	{ 0x0600009B, 8,  (void**)&OfficialDemoGraph_BuildDestination_m8A2E396648876205DF379CDB3B2C198AB6A5E70D_RuntimeMethod_var, 0 },
	{ 0x0600009C, 9,  (void**)&OfficialDemoGraph_OnReleaseDestinationTexture_mC9040C05CB2B2BC7C3D8126BCCFD6FF9B24B199D_RuntimeMethod_var, 0 },
	{ 0x060000BC, 2,  (void**)&DemoGraph_PushInputInGlContext_m2000278CAE8C8F4281A45E55684BD7498AEE66E7_RuntimeMethod_var, 0 },
	{ 0x060000FB, 12,  (void**)&TextureFramePool_OnTextureFrameRelease_m3F46346F6E43543DD9BE828528E788F0066D9B6D_RuntimeMethod_var, 0 },
	{ 0x0600010F, 11,  (void**)&SceneDirectors_GetCurrentContext_mCCE71B73479FC8A1F8D65066ED794FE54AF5401C_RuntimeMethod_var, 0 },
};
static const Il2CppTokenRangePair s_rgctxIndices[3] = 
{
	{ 0x02000027, { 5, 4 } },
	{ 0x060000BF, { 0, 2 } },
	{ 0x060000C0, { 2, 3 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[9] = 
{
	{ (Il2CppRGCTXDataType)3, 7010 },
	{ (Il2CppRGCTXDataType)3, 7024 },
	{ (Il2CppRGCTXDataType)3, 10231 },
	{ (Il2CppRGCTXDataType)2, 2515 },
	{ (Il2CppRGCTXDataType)3, 5046 },
	{ (Il2CppRGCTXDataType)2, 2878 },
	{ (Il2CppRGCTXDataType)1, 440 },
	{ (Il2CppRGCTXDataType)2, 440 },
	{ (Il2CppRGCTXDataType)3, 10602 },
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	375,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	6,
	s_reversePInvokeIndices,
	3,
	s_rgctxIndices,
	9,
	s_rgctxValues,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
