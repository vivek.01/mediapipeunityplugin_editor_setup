﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"


extern const RuntimeMethod* AssetBundleManager_GetResourceContents_mDFE89208C34422AEE3F365704F969ADFA843BA84_RuntimeMethod_var;
extern const RuntimeMethod* AssetBundleManager_PathToResourceAsFile_mE4EA5782FC953FF16031FD0EB659BD758E0EDC7E_RuntimeMethod_var;
extern const RuntimeMethod* ImageFrame_ReleasePixelData_mDF81706134475903E835D1F61193281D8FB96A78_RuntimeMethod_var;
extern const RuntimeMethod* LocalAssetManager_GetResourceContents_m36D0D7964026D770978C9BECA4F18D510A46648A_RuntimeMethod_var;
extern const RuntimeMethod* LocalAssetManager_PathToResourceAsFile_mD93EA6538E2E77CCD4ACB981201294DFFDF0C3A6_RuntimeMethod_var;
extern const RuntimeMethod* Protobuf_LogProtobufMessage_m1B59C21023D21E2AB5CA458EC71B9F43DA4E587D_RuntimeMethod_var;



// 0x00000001 System.Void Microsoft.CodeAnalysis.EmbeddedAttribute::.ctor()
extern void EmbeddedAttribute__ctor_mFD149DC63111CEA4726998614A96442713AD600F (void);
// 0x00000002 System.Void System.Runtime.CompilerServices.IsUnmanagedAttribute::.ctor()
extern void IsUnmanagedAttribute__ctor_m45D6F01B7AD5A98E821F1DA8A4E6B7281B8E8C79 (void);
// 0x00000003 System.Void Mediapipe.AnnotationController::Clear()
// 0x00000004 UnityEngine.Vector3 Mediapipe.AnnotationController::ScaleVector(UnityEngine.Transform)
extern void AnnotationController_ScaleVector_mA6A1B303D73EE79175C490D1101B000AA224B62F (void);
// 0x00000005 UnityEngine.Vector3 Mediapipe.AnnotationController::GetPositionFromNormalizedPoint(UnityEngine.Transform,System.Single,System.Single,System.Boolean)
extern void AnnotationController_GetPositionFromNormalizedPoint_mA8FE8268BAB0015082254E4C206DC6CEB877EEDC (void);
// 0x00000006 UnityEngine.Vector3 Mediapipe.AnnotationController::GetPosition(UnityEngine.Transform,Mediapipe.NormalizedLandmark,System.Boolean)
extern void AnnotationController_GetPosition_m22613159F002193250F9829E14F25AC93FDD0565 (void);
// 0x00000007 UnityEngine.Vector3 Mediapipe.AnnotationController::GetPosition(UnityEngine.Transform,Mediapipe.LocationData/Types/RelativeKeypoint,System.Boolean)
extern void AnnotationController_GetPosition_mB946751E773391FD51AEE00303BB14A2C97C9B8E (void);
// 0x00000008 UnityEngine.Vector3[] Mediapipe.AnnotationController::GetPositionsFromNormalizedRect(UnityEngine.Transform,System.Single,System.Single,System.Single,System.Single,System.Single,System.Boolean)
extern void AnnotationController_GetPositionsFromNormalizedRect_m1880507A736BE35F694432C774FCE21007A68FB6 (void);
// 0x00000009 UnityEngine.Vector3[] Mediapipe.AnnotationController::GetPositions(UnityEngine.Transform,Mediapipe.NormalizedRect,System.Boolean)
extern void AnnotationController_GetPositions_mA87C2E5977845B30D8BD7D92F6EB08AE18554E88 (void);
// 0x0000000A UnityEngine.Vector3[] Mediapipe.AnnotationController::GetPositions(UnityEngine.Transform,Mediapipe.LocationData/Types/RelativeBoundingBox,System.Boolean)
extern void AnnotationController_GetPositions_m002727B3E76D92E2D884A9C7408CF6F5B3A4ED9E (void);
// 0x0000000B System.Single Mediapipe.AnnotationController::GetDistance(UnityEngine.Transform,Mediapipe.NormalizedLandmark,Mediapipe.NormalizedLandmark)
extern void AnnotationController_GetDistance_mB3B606B0082950C0229985887B4065AC14E214A1 (void);
// 0x0000000C System.Void Mediapipe.AnnotationController::.ctor()
extern void AnnotationController__ctor_m91527FA9EB5B41FA7474B9BC2906D391EDCA87AD (void);
// 0x0000000D System.Void Mediapipe.CircleAnnotationController::Awake()
extern void CircleAnnotationController_Awake_mCDCA10265B7B08F36EC318E0C200EBDAE40ED1A3 (void);
// 0x0000000E System.Void Mediapipe.CircleAnnotationController::Clear()
extern void CircleAnnotationController_Clear_m874E57F539654541E673F490F630BB344280FBA8 (void);
// 0x0000000F System.Void Mediapipe.CircleAnnotationController::Draw(UnityEngine.Transform,Mediapipe.NormalizedLandmark,System.Single,System.Boolean)
extern void CircleAnnotationController_Draw_mD6FD8CC9F1B1134FE28A74118462508A7637D8E3 (void);
// 0x00000010 System.Void Mediapipe.CircleAnnotationController::.ctor()
extern void CircleAnnotationController__ctor_m5E7F8FDC9F4586B94447E81F4E5F09DD44C7EC3D (void);
// 0x00000011 System.Void Mediapipe.ClassificationAnnotationController::Clear()
extern void ClassificationAnnotationController_Clear_m6A787687B93BF33ECDF9CDE14516F127F6B05BB1 (void);
// 0x00000012 System.Void Mediapipe.ClassificationAnnotationController::Draw(UnityEngine.Transform,Mediapipe.ClassificationList)
extern void ClassificationAnnotationController_Draw_m39C7805E97E187BCA7DB77F591F370B6AF7F9E1B (void);
// 0x00000013 System.Void Mediapipe.ClassificationAnnotationController::.ctor()
extern void ClassificationAnnotationController__ctor_m199E7565128A0C23B5250703E0B3C638895F00D5 (void);
// 0x00000014 System.Void Mediapipe.DetectionAnnotationController::Awake()
extern void DetectionAnnotationController_Awake_mD5BD0D8D23BE5190BDA473C6E9F414C7B15F8074 (void);
// 0x00000015 System.Void Mediapipe.DetectionAnnotationController::OnDestroy()
extern void DetectionAnnotationController_OnDestroy_mECC90E8F61C02E69302EC82F66CF710E8695D520 (void);
// 0x00000016 System.Void Mediapipe.DetectionAnnotationController::Clear()
extern void DetectionAnnotationController_Clear_m9917051AE6144BE8A36EA25E5CBCB57EC0082A48 (void);
// 0x00000017 System.Void Mediapipe.DetectionAnnotationController::Draw(UnityEngine.Transform,Mediapipe.Detection,System.Boolean)
extern void DetectionAnnotationController_Draw_mC635729B32BFFE0573AC09E0F568F240BDB5B918 (void);
// 0x00000018 System.Void Mediapipe.DetectionAnnotationController::DrawRectAndLabel(UnityEngine.Transform,Mediapipe.Detection,System.Boolean)
extern void DetectionAnnotationController_DrawRectAndLabel_m335B34E8F5111E5CBCCCDC12C46A6F4936E5DDC2 (void);
// 0x00000019 System.Void Mediapipe.DetectionAnnotationController::DrawRelativeKeypoints(UnityEngine.Transform,Google.Protobuf.Collections.RepeatedField`1<Mediapipe.LocationData/Types/RelativeKeypoint>,System.Boolean)
extern void DetectionAnnotationController_DrawRelativeKeypoints_m8C014F231F5A4DCBA767F8819F6EB9DAA66DE554 (void);
// 0x0000001A System.Void Mediapipe.DetectionAnnotationController::.ctor()
extern void DetectionAnnotationController__ctor_m70E884B362DA793AB008F7A44CE3E597EC828764 (void);
// 0x0000001B System.Void Mediapipe.DetectionListAnnotationController::Draw(UnityEngine.Transform,System.Collections.Generic.List`1<Mediapipe.Detection>,System.Boolean)
extern void DetectionListAnnotationController_Draw_m231C288C83D30FD15FD1C87C17AFD9E58033C26F (void);
// 0x0000001C System.Void Mediapipe.DetectionListAnnotationController::.ctor()
extern void DetectionListAnnotationController__ctor_m38E68DF023D6D7144EADBFFA4ED38147A3AB6C60 (void);
// 0x0000001D System.Void Mediapipe.EdgeAnnotationController::Clear()
extern void EdgeAnnotationController_Clear_m35C257ED7D47DF6E266FBD472774E0F26B0ABE4D (void);
// 0x0000001E System.Void Mediapipe.EdgeAnnotationController::Draw(UnityEngine.Transform,Mediapipe.NormalizedLandmark,Mediapipe.NormalizedLandmark,System.Boolean)
extern void EdgeAnnotationController_Draw_mDCCF112E0A995942C044D1B8D17D5AD439A6F080 (void);
// 0x0000001F System.Void Mediapipe.EdgeAnnotationController::Draw(UnityEngine.Transform,UnityEngine.GameObject,UnityEngine.GameObject)
extern void EdgeAnnotationController_Draw_mE4FFDF92737C3CA440C302D4F10A95111B820C41 (void);
// 0x00000020 System.Void Mediapipe.EdgeAnnotationController::Draw(UnityEngine.Transform,UnityEngine.Vector3,UnityEngine.Vector3)
extern void EdgeAnnotationController_Draw_m572DF445739E37D19FF04546A008D71F69F4B737 (void);
// 0x00000021 System.Void Mediapipe.EdgeAnnotationController::.ctor()
extern void EdgeAnnotationController__ctor_mB893C7C2E5FC5BF68C89ABB3323304148A23C0BF (void);
// 0x00000022 System.Void Mediapipe.EdgeAnnotationController::.cctor()
extern void EdgeAnnotationController__cctor_mE76D0BC832C0F3C2A0572399BFB104A983F67973 (void);
// 0x00000023 System.Collections.Generic.IList`1<System.Tuple`2<System.Int32,System.Int32>> Mediapipe.FaceLandmarkListAnnotationController::get_Connections()
extern void FaceLandmarkListAnnotationController_get_Connections_m15A7FEAC348237A285AA44D935DCE9CF5BC86F50 (void);
// 0x00000024 System.Int32 Mediapipe.FaceLandmarkListAnnotationController::get_NodeSize()
extern void FaceLandmarkListAnnotationController_get_NodeSize_m9E30F5E2E0844E89FD3D9A028B8B8DF44507CFCE (void);
// 0x00000025 System.Void Mediapipe.FaceLandmarkListAnnotationController::.ctor()
extern void FaceLandmarkListAnnotationController__ctor_mC199284B683342CCE4F3057699736E57F6B251CE (void);
// 0x00000026 System.Void Mediapipe.FaceLandmarkListAnnotationController::.cctor()
extern void FaceLandmarkListAnnotationController__cctor_mFFF8D3A9B2940E9A693A40E9B83E53B8A1F7EA75 (void);
// 0x00000027 System.Collections.Generic.IList`1<System.Tuple`2<System.Int32,System.Int32>> Mediapipe.FullBodyPoseLandmarkListAnnotationController::get_Connections()
extern void FullBodyPoseLandmarkListAnnotationController_get_Connections_m54A6734258122B53B38819FA9E1FAA196B1E3ADD (void);
// 0x00000028 System.Int32 Mediapipe.FullBodyPoseLandmarkListAnnotationController::get_NodeSize()
extern void FullBodyPoseLandmarkListAnnotationController_get_NodeSize_m32FF5D69EE61A449C9C4EE0275523D71D276A19C (void);
// 0x00000029 System.Void Mediapipe.FullBodyPoseLandmarkListAnnotationController::.ctor()
extern void FullBodyPoseLandmarkListAnnotationController__ctor_m64E26DF784F76F5BAD8834655A470A0F6A463B90 (void);
// 0x0000002A System.Void Mediapipe.FullBodyPoseLandmarkListAnnotationController::.cctor()
extern void FullBodyPoseLandmarkListAnnotationController__cctor_mFA255E10AD607DB63486E637E9FE08B3A70D4735 (void);
// 0x0000002B System.Collections.Generic.IList`1<System.Tuple`2<System.Int32,System.Int32>> Mediapipe.HandLandmarkListAnnotationController::get_Connections()
extern void HandLandmarkListAnnotationController_get_Connections_mF282E0CA0D45FEE4BF7FA08750824207B9351F37 (void);
// 0x0000002C System.Int32 Mediapipe.HandLandmarkListAnnotationController::get_NodeSize()
extern void HandLandmarkListAnnotationController_get_NodeSize_m77725524772C22CD775660855100A13906F1AE0D (void);
// 0x0000002D System.Void Mediapipe.HandLandmarkListAnnotationController::.ctor()
extern void HandLandmarkListAnnotationController__ctor_m1CD1741D3944A10B233676042DA460286A2D8C63 (void);
// 0x0000002E System.Void Mediapipe.HandLandmarkListAnnotationController::.cctor()
extern void HandLandmarkListAnnotationController__cctor_m7992BD4835558328C92E46F00715D49CFE5E09A0 (void);
// 0x0000002F System.Void Mediapipe.LandmarkListAnnotationController::Awake()
extern void LandmarkListAnnotationController_Awake_m26810925771B118CF1FC849EDF5BE47BAF041866 (void);
// 0x00000030 System.Void Mediapipe.LandmarkListAnnotationController::OnDestroy()
extern void LandmarkListAnnotationController_OnDestroy_mC0B69B9D32E4F94378B050A392F099D72414CA72 (void);
// 0x00000031 System.Void Mediapipe.LandmarkListAnnotationController::Clear()
extern void LandmarkListAnnotationController_Clear_m4102D6A3768C843BE73D619F305A471DE1D18D21 (void);
// 0x00000032 System.Void Mediapipe.LandmarkListAnnotationController::Draw(UnityEngine.Transform,Mediapipe.NormalizedLandmarkList,System.Boolean)
extern void LandmarkListAnnotationController_Draw_mCA206AD88A5552F61C67D0DD7CB65718AEFF31E9 (void);
// 0x00000033 System.Collections.Generic.IList`1<System.Tuple`2<System.Int32,System.Int32>> Mediapipe.LandmarkListAnnotationController::get_Connections()
// 0x00000034 System.Int32 Mediapipe.LandmarkListAnnotationController::get_NodeSize()
// 0x00000035 System.Int32 Mediapipe.LandmarkListAnnotationController::get_EdgeSize()
extern void LandmarkListAnnotationController_get_EdgeSize_m5458F9000E0C6912812E34CD4EA14859BC3949C8 (void);
// 0x00000036 System.Boolean Mediapipe.LandmarkListAnnotationController::isEmpty(Mediapipe.NormalizedLandmarkList)
extern void LandmarkListAnnotationController_isEmpty_mDFF2CF713A34ED30E756AD573576BD183B21C7C2 (void);
// 0x00000037 System.Void Mediapipe.LandmarkListAnnotationController::.ctor()
extern void LandmarkListAnnotationController__ctor_m278A21D9E55C456BD6E81F0AF9649C446CD7B012 (void);
// 0x00000038 System.Void Mediapipe.LandmarkListAnnotationController/<>c::.cctor()
extern void U3CU3Ec__cctor_mE58891B03C85CFB11891A9E26D0E6BF20935C5C0 (void);
// 0x00000039 System.Void Mediapipe.LandmarkListAnnotationController/<>c::.ctor()
extern void U3CU3Ec__ctor_mFF3B99818696F77BB36E5CE0E1632EA54B9798D4 (void);
// 0x0000003A System.Boolean Mediapipe.LandmarkListAnnotationController/<>c::<isEmpty>b__15_0(Mediapipe.NormalizedLandmark)
extern void U3CU3Ec_U3CisEmptyU3Eb__15_0_mEC716E744D88196469528450F57828EF688C33E8 (void);
// 0x0000003B System.Int32 Mediapipe.ListAnnotationController`1::get_MaxSize()
// 0x0000003C System.Void Mediapipe.ListAnnotationController`1::set_MaxSize(System.Int32)
// 0x0000003D System.Void Mediapipe.ListAnnotationController`1::OnDestroy()
// 0x0000003E System.Void Mediapipe.ListAnnotationController`1::Clear()
// 0x0000003F System.Void Mediapipe.ListAnnotationController`1::ClearAll(System.Int32)
// 0x00000040 System.Void Mediapipe.ListAnnotationController`1::Awake()
// 0x00000041 System.Void Mediapipe.ListAnnotationController`1::UpdateMaxSize(System.Int32)
// 0x00000042 UnityEngine.GameObject Mediapipe.ListAnnotationController`1::GetAnnotationAt(System.Int32)
// 0x00000043 T Mediapipe.ListAnnotationController`1::GetAnnotationControllerAt(System.Int32)
// 0x00000044 System.Void Mediapipe.ListAnnotationController`1::.ctor()
// 0x00000045 System.Void Mediapipe.MaskAnnotationController::Clear()
extern void MaskAnnotationController_Clear_m0EC8E8F3CFB3CEFCC10FEB8A14038254EC5F5A3C (void);
// 0x00000046 System.Void Mediapipe.MaskAnnotationController::Draw(UnityEngine.Texture2D,Mediapipe.ImageFrame,UnityEngine.Color,System.Boolean,System.Single)
extern void MaskAnnotationController_Draw_m531F4070BF7AA113B8AAC414FCCA0AC556FE08A5 (void);
// 0x00000047 System.Void Mediapipe.MaskAnnotationController::SetMask(UnityEngine.Texture2D,System.Int32,System.Int32,System.Int32,System.Int32,System.Single,UnityEngine.Color)
extern void MaskAnnotationController_SetMask_m95191E76F668E0855000110D97BA0348DD01FA64 (void);
// 0x00000048 System.Tuple`2<System.Int32,System.Int32> Mediapipe.MaskAnnotationController::GetNearestRange(System.Int32,System.Int32,System.Int32)
extern void MaskAnnotationController_GetNearestRange_m7BF3A8665F1D4D756C866D940ABD8659A48E7D14 (void);
// 0x00000049 System.Void Mediapipe.MaskAnnotationController::.ctor()
extern void MaskAnnotationController__ctor_m425C5578FB544FEF95D1C604CFBE529E39B139B0 (void);
// 0x0000004A System.Void Mediapipe.NodeAnnotationController::Clear()
extern void NodeAnnotationController_Clear_m09268CFAD7F9AEEA39B290E176D384EC1B7C09F0 (void);
// 0x0000004B System.Void Mediapipe.NodeAnnotationController::Draw(UnityEngine.Transform,Mediapipe.NormalizedLandmark,System.Boolean,System.Single)
extern void NodeAnnotationController_Draw_mB5824A0C834CEBBF000D9C9B44EA5E59C1E38C5A (void);
// 0x0000004C System.Void Mediapipe.NodeAnnotationController::Draw(UnityEngine.Transform,Mediapipe.LocationData/Types/RelativeKeypoint,System.Boolean,System.Single)
extern void NodeAnnotationController_Draw_m2CFD5978D6F6A64AAC99AF8ADDEB36AE12B38E40 (void);
// 0x0000004D System.Void Mediapipe.NodeAnnotationController::.ctor()
extern void NodeAnnotationController__ctor_mA3FACFE1BEFE9173D43FAC39F1E1B21B25B82768 (void);
// 0x0000004E System.Collections.Generic.IList`1<System.Tuple`2<System.Int32,System.Int32>> Mediapipe.PoseLandmarkListAnnotationController::get_Connections()
extern void PoseLandmarkListAnnotationController_get_Connections_m4632729106C8CD1307DD9445E86C0C93F4AD27E9 (void);
// 0x0000004F System.Int32 Mediapipe.PoseLandmarkListAnnotationController::get_NodeSize()
extern void PoseLandmarkListAnnotationController_get_NodeSize_m5ACFCB49553DDC8C55B0D7912F401E89EDCF348A (void);
// 0x00000050 System.Void Mediapipe.PoseLandmarkListAnnotationController::.ctor()
extern void PoseLandmarkListAnnotationController__ctor_m9621D67A36A00C9583B93F7CAC64033318E8E2D7 (void);
// 0x00000051 System.Void Mediapipe.PoseLandmarkListAnnotationController::.cctor()
extern void PoseLandmarkListAnnotationController__cctor_mCFE6BDC12124548334EEA7056E8E957F3DCC0725 (void);
// 0x00000052 System.Void Mediapipe.RectAnnotationController::Clear()
extern void RectAnnotationController_Clear_mCAAE3F48534569E674C8872069DD036628E045AC (void);
// 0x00000053 System.Void Mediapipe.RectAnnotationController::Draw(UnityEngine.Transform,Mediapipe.NormalizedRect,System.Boolean)
extern void RectAnnotationController_Draw_m94915D81C3FB8600B4B962801BC28C41B2826C28 (void);
// 0x00000054 System.Void Mediapipe.RectAnnotationController::.ctor()
extern void RectAnnotationController__ctor_m07B8A633B5379A1BB3CDE0059F03894A41FBF758 (void);
// 0x00000055 System.Void Mediapipe.RectListAnnotationController::Draw(UnityEngine.Transform,System.Collections.Generic.List`1<Mediapipe.NormalizedRect>,System.Boolean)
extern void RectListAnnotationController_Draw_mD5379AC3DFF9997053512DBEF8F1D019E3BF8E07 (void);
// 0x00000056 System.Void Mediapipe.RectListAnnotationController::.ctor()
extern void RectListAnnotationController__ctor_mEF32CC85C836EF6601E10488CF00C48C60686A1A (void);
// 0x00000057 System.Boolean Mediapipe.DisposableObject::get_isDisposed()
extern void DisposableObject_get_isDisposed_mBAC3411868F2F3866C93C0FD29569C5C6A589E4E (void);
// 0x00000058 System.Void Mediapipe.DisposableObject::set_isDisposed(System.Boolean)
extern void DisposableObject_set_isDisposed_m4810231F19F321DDBB7ACEB59D310DA3AF8689FA (void);
// 0x00000059 System.Boolean Mediapipe.DisposableObject::get_isOwner()
extern void DisposableObject_get_isOwner_m05D0D142AD0928EFB4EBF5C9BE83A8C4D003B7C5 (void);
// 0x0000005A System.Void Mediapipe.DisposableObject::set_isOwner(System.Boolean)
extern void DisposableObject_set_isOwner_m14F37B0D67E76791FAD35719A01F7924A796DE42 (void);
// 0x0000005B System.Void Mediapipe.DisposableObject::.ctor()
extern void DisposableObject__ctor_m3D3FF015DD820E8D044BE2029F67E8769AB3A908 (void);
// 0x0000005C System.Void Mediapipe.DisposableObject::.ctor(System.Boolean)
extern void DisposableObject__ctor_mCC479EC8FD88BF3ADC5D5238D2FD08B7EB77EF28 (void);
// 0x0000005D System.Void Mediapipe.DisposableObject::Dispose()
extern void DisposableObject_Dispose_m6A068C5925EC39A121CCB9881C074EF27C77A84B (void);
// 0x0000005E System.Void Mediapipe.DisposableObject::Dispose(System.Boolean)
extern void DisposableObject_Dispose_m729100105D4D3C69B47D47B52FDB6078E832DC1E (void);
// 0x0000005F System.Void Mediapipe.DisposableObject::Finalize()
extern void DisposableObject_Finalize_m61E5D0FAA3769F9C54F2AF70D87ECA0BC1424F1C (void);
// 0x00000060 System.Void Mediapipe.DisposableObject::DisposeManaged()
extern void DisposableObject_DisposeManaged_mF57755111B4ADFD17F40662B7F5464EB746D1232 (void);
// 0x00000061 System.Void Mediapipe.DisposableObject::DisposeUnmanaged()
extern void DisposableObject_DisposeUnmanaged_m8DF53D7CF53D62539AE5180B87D9C834382E4590 (void);
// 0x00000062 System.Void Mediapipe.DisposableObject::TransferOwnership()
extern void DisposableObject_TransferOwnership_mF63D4787258D48201930A2CF3BEE7A47430A2EFC (void);
// 0x00000063 System.Void Mediapipe.DisposableObject::ThrowIfDisposed()
extern void DisposableObject_ThrowIfDisposed_m44AC87144CFB5BCB096E952AF55DC17B1917E26F (void);
// 0x00000064 System.IntPtr Mediapipe.IMpResourceHandle::get_mpPtr()
// 0x00000065 System.Void Mediapipe.IMpResourceHandle::ReleaseMpResource()
// 0x00000066 System.Void Mediapipe.IMpResourceHandle::TransferOwnership()
// 0x00000067 System.Boolean Mediapipe.IMpResourceHandle::OwnsResource()
// 0x00000068 System.Void Mediapipe.InternalException::.ctor(System.String)
extern void InternalException__ctor_mA799316BDDBC4BEBABD6DDF7EC64148566AA7203 (void);
// 0x00000069 System.Void Mediapipe.MediaPipeException::.ctor(System.String)
extern void MediaPipeException__ctor_m38FF115395F8A5B1926A12CADF964D3FFA4B6462 (void);
// 0x0000006A System.Void Mediapipe.MediaPipePluginException::.ctor(System.String)
extern void MediaPipePluginException__ctor_m9A533AFB6CAC664271755038902A1B978ECD5D44 (void);
// 0x0000006B System.Void Mediapipe.MpResourceHandle::.ctor(System.Boolean)
extern void MpResourceHandle__ctor_m84EC3037371F2F7B1DFC32D8D9370BAC6821705A (void);
// 0x0000006C System.Void Mediapipe.MpResourceHandle::.ctor(System.IntPtr,System.Boolean)
extern void MpResourceHandle__ctor_m86544707DB7A9AA7351DB6D2A2491B6BD97385A0 (void);
// 0x0000006D System.IntPtr Mediapipe.MpResourceHandle::get_mpPtr()
extern void MpResourceHandle_get_mpPtr_mA93C5D418EFA29D6C0FB55B776C4F4FA1416498C (void);
// 0x0000006E System.Void Mediapipe.MpResourceHandle::ReleaseMpResource()
extern void MpResourceHandle_ReleaseMpResource_m1374ACB47C279F5BA463FD3FC8306A9E0E9E78E7 (void);
// 0x0000006F System.Boolean Mediapipe.MpResourceHandle::OwnsResource()
extern void MpResourceHandle_OwnsResource_m5CA80083503C7210737E3FD843AF0324BD6AEEA1 (void);
// 0x00000070 System.Void Mediapipe.MpResourceHandle::DisposeUnmanaged()
extern void MpResourceHandle_DisposeUnmanaged_mE90C9069D41976D29D6022E25A9814E3309DDB1A (void);
// 0x00000071 System.Void Mediapipe.MpResourceHandle::ReleaseMpPtr()
extern void MpResourceHandle_ReleaseMpPtr_mAF6A25D3E070825A6DB44923275C01C45C8CEE5B (void);
// 0x00000072 System.Void Mediapipe.MpResourceHandle::DeleteMpPtr()
// 0x00000073 System.String Mediapipe.MpResourceHandle::MarshalStringFromNative(Mediapipe.MpResourceHandle/StringOutFunc)
extern void MpResourceHandle_MarshalStringFromNative_m9BF8B0C0534BCDC70741E1AD47C8639B527616AF (void);
// 0x00000074 System.Void Mediapipe.MpResourceHandle/StringOutFunc::.ctor(System.Object,System.IntPtr)
extern void StringOutFunc__ctor_mA1CDFE1C87059E6CCA17F75ACFECBF6B05B4693E (void);
// 0x00000075 Mediapipe.MpReturnCode Mediapipe.MpResourceHandle/StringOutFunc::Invoke(System.IntPtr,System.IntPtr&)
extern void StringOutFunc_Invoke_mC2B207EB6C49C01D5589BE4A28CB108639B3C606 (void);
// 0x00000076 System.IAsyncResult Mediapipe.MpResourceHandle/StringOutFunc::BeginInvoke(System.IntPtr,System.IntPtr&,System.AsyncCallback,System.Object)
extern void StringOutFunc_BeginInvoke_m6C5BE29024026EC369CBDF2951F49A766E686299 (void);
// 0x00000077 Mediapipe.MpReturnCode Mediapipe.MpResourceHandle/StringOutFunc::EndInvoke(System.IntPtr&,System.IAsyncResult)
extern void StringOutFunc_EndInvoke_mA2FFD153F9910DB743310669FA6A07215304C498 (void);
// 0x00000078 System.Void Mediapipe.SharedPtrHandle::.ctor(System.IntPtr,System.Boolean)
extern void SharedPtrHandle__ctor_mA38B222513827FF11119BB27C36C05573D2495D1 (void);
// 0x00000079 System.IntPtr Mediapipe.SharedPtrHandle::Get()
// 0x0000007A System.Void Mediapipe.SharedPtrHandle::Reset()
// 0x0000007B System.Void Mediapipe.UniquePtrHandle::.ctor(System.IntPtr,System.Boolean)
extern void UniquePtrHandle__ctor_m422414A0665FCE76EE6CC557727CD1A9523B15EF (void);
// 0x0000007C System.IntPtr Mediapipe.UniquePtrHandle::Get()
// 0x0000007D System.IntPtr Mediapipe.UniquePtrHandle::Release()
// 0x0000007E System.Void Mediapipe.Glog::Initialize(System.String,System.String)
extern void Glog_Initialize_mEA148EF01B31968CE8A1B2B4B3C05887A8E13294 (void);
// 0x0000007F System.Void Mediapipe.Glog::Shutdown()
extern void Glog_Shutdown_m7FEA643AF98D2D93BE2BE591DDCAC0491DDA204F (void);
// 0x00000080 System.Void Mediapipe.Glog::Log(Mediapipe.Glog/Severity,System.String)
extern void Glog_Log_m7EBF9C4A6CE2922A03DF5784E6B16F89A93584AC (void);
// 0x00000081 System.Void Mediapipe.Glog::FlushLogFiles(Mediapipe.Glog/Severity)
extern void Glog_FlushLogFiles_m99F284D2286E6B9365CC355150FE2481032938D1 (void);
// 0x00000082 System.Void Mediapipe.Glog::.ctor()
extern void Glog__ctor_m8FBD33C65B740B811C7858EE8D7373A107E49AB3 (void);
// 0x00000083 System.Void Mediapipe.Protobuf::.cctor()
extern void Protobuf__cctor_m8563A723AE3C71192A61E05DD1604B109DF496A0 (void);
// 0x00000084 T Mediapipe.Protobuf::DeserializeProto(System.IntPtr,Google.Protobuf.MessageParser`1<T>)
// 0x00000085 System.Collections.Generic.List`1<T> Mediapipe.Protobuf::DeserializeProtoVector(System.IntPtr,Google.Protobuf.MessageParser`1<T>)
// 0x00000086 System.Void Mediapipe.Protobuf::LogProtobufMessage(System.Int32,System.String,System.Int32,System.String)
extern void Protobuf_LogProtobufMessage_m1B59C21023D21E2AB5CA458EC71B9F43DA4E587D (void);
// 0x00000087 System.String Mediapipe.Protobuf::FormatProtobufLogLevel(System.Int32)
extern void Protobuf_FormatProtobufLogLevel_mD7C7A738B5069D575A88EB32A862234D6E26CADB (void);
// 0x00000088 System.Void Mediapipe.Protobuf::.ctor()
extern void Protobuf__ctor_m450C7237AC8ABE0A9B482F1B6C550368AF9CD751 (void);
// 0x00000089 System.Void Mediapipe.Protobuf/ProtobufLogHandler::.ctor(System.Object,System.IntPtr)
extern void ProtobufLogHandler__ctor_m9710EFC55EEE9F88A73A00BC3C8C92FFDD59C17B (void);
// 0x0000008A System.Void Mediapipe.Protobuf/ProtobufLogHandler::Invoke(System.Int32,System.String,System.Int32,System.String)
extern void ProtobufLogHandler_Invoke_mA37425C68AA303DA4CF3B7538C68B0ABE51CA648 (void);
// 0x0000008B System.IAsyncResult Mediapipe.Protobuf/ProtobufLogHandler::BeginInvoke(System.Int32,System.String,System.Int32,System.String,System.AsyncCallback,System.Object)
extern void ProtobufLogHandler_BeginInvoke_mD8C54C91E7F9F0BCDB53884070279A724A0AC53F (void);
// 0x0000008C System.Void Mediapipe.Protobuf/ProtobufLogHandler::EndInvoke(System.IAsyncResult)
extern void ProtobufLogHandler_EndInvoke_mB4A85E6157160DC1F0D249E2B7EB52E4E911ACCA (void);
// 0x0000008D System.Void Mediapipe.StdString::.ctor(System.IntPtr,System.Boolean)
extern void StdString__ctor_m05D13A243A1ABD5F541B48B0890087853BF469D9 (void);
// 0x0000008E System.Void Mediapipe.StdString::.ctor(System.Byte[])
extern void StdString__ctor_mFE5502F4BEEF68B901A1526DFB601D4FBC240C30 (void);
// 0x0000008F System.Void Mediapipe.StdString::DeleteMpPtr()
extern void StdString_DeleteMpPtr_mDEAFCE78D7148065C66167238E51FB5589C6F093 (void);
// 0x00000090 System.Void Mediapipe.StdString::Swap(Mediapipe.StdString)
extern void StdString_Swap_m3995E0894B6AAD06C25494E2F795A2D9E05C48B3 (void);
// 0x00000091 Unity.Collections.NativeArray`1<System.Byte> Mediapipe.Format::FromPixels32(UnityEngine.Color32[],System.Int32,System.Int32,System.Boolean,Unity.Collections.Allocator)
extern void Format_FromPixels32_mBA75BC96B4A07BD34189075F273B236DEC47F7D6 (void);
// 0x00000092 UnityEngine.Color32[] Mediapipe.Format::FromBytePtr(System.IntPtr,Mediapipe.ImageFormat/Format,System.Int32,System.Int32,System.Int32,System.Boolean)
extern void Format_FromBytePtr_m241B5F9B3CA63A791E94B49E80FD4EAE6453619A (void);
// 0x00000093 UnityEngine.Color32[] Mediapipe.Format::FromSRGBOrSRGBA(System.IntPtr,Mediapipe.ImageFormat/Format,System.Int32,System.Int32,System.Int32,System.Boolean)
extern void Format_FromSRGBOrSRGBA_m91CFC61B21D1369DC2497631690B0B1BE2A1E761 (void);
// 0x00000094 System.Void Mediapipe.Format::.ctor()
extern void Format__ctor_m23F96F92A6A488E27E4A130F4A7D1D3A8A5668A1 (void);
// 0x00000095 System.Void Mediapipe.CalculatorGraph::.ctor()
extern void CalculatorGraph__ctor_m36D4D86B01B255D9402C869FF30ABB19AE0EDE1C (void);
// 0x00000096 System.Void Mediapipe.CalculatorGraph::.ctor(System.String)
extern void CalculatorGraph__ctor_m22A32EAA02E54FF25DCB234289DBD50A6A8E3FD9 (void);
// 0x00000097 System.Void Mediapipe.CalculatorGraph::.ctor(System.Byte[])
extern void CalculatorGraph__ctor_m2CC73C9FE7F8E19141AACF74C322B482C089C530 (void);
// 0x00000098 System.Void Mediapipe.CalculatorGraph::.ctor(Mediapipe.CalculatorGraphConfig)
extern void CalculatorGraph__ctor_mC912C0B993206DED797465F65749660EF5BB3E7E (void);
// 0x00000099 System.Void Mediapipe.CalculatorGraph::DeleteMpPtr()
extern void CalculatorGraph_DeleteMpPtr_mC89CA3B9EA6119419ACD668538E1DBC1FEA1232B (void);
// 0x0000009A Mediapipe.Status Mediapipe.CalculatorGraph::Initialize(Mediapipe.CalculatorGraphConfig)
extern void CalculatorGraph_Initialize_m8C661AF74FE4395742FBC329C8F513CDD022FA2F (void);
// 0x0000009B Mediapipe.Status Mediapipe.CalculatorGraph::Initialize(Mediapipe.CalculatorGraphConfig,Mediapipe.SidePacket)
extern void CalculatorGraph_Initialize_m898F1A6588B7861E73B60666C8938551BA66DE8A (void);
// 0x0000009C Mediapipe.CalculatorGraphConfig Mediapipe.CalculatorGraph::Config()
extern void CalculatorGraph_Config_mC7AE9FF00C912A256893678836B00E6DF9BF06D1 (void);
// 0x0000009D Mediapipe.Status Mediapipe.CalculatorGraph::ObserveOutputStream(System.String,Mediapipe.CalculatorGraph/NativePacketCallback)
extern void CalculatorGraph_ObserveOutputStream_m829748E3A903D01A0F061EF1CB5B06C911E3D285 (void);
// 0x0000009E Mediapipe.Status Mediapipe.CalculatorGraph::ObserveOutputStream(System.String,Mediapipe.CalculatorGraph/PacketCallback`2<T,U>,System.Runtime.InteropServices.GCHandle&)
// 0x0000009F Mediapipe.StatusOrPoller`1<T> Mediapipe.CalculatorGraph::AddOutputStreamPoller(System.String)
// 0x000000A0 Mediapipe.Status Mediapipe.CalculatorGraph::Run()
extern void CalculatorGraph_Run_m76946CA8F7B01409B68DA01F0ED26FD26CEFC25C (void);
// 0x000000A1 Mediapipe.Status Mediapipe.CalculatorGraph::Run(Mediapipe.SidePacket)
extern void CalculatorGraph_Run_m7DFB3A09CBB32849E1140DC62397EF1D687A9A05 (void);
// 0x000000A2 Mediapipe.Status Mediapipe.CalculatorGraph::StartRun()
extern void CalculatorGraph_StartRun_m03C22F09F5A6C5AB8286ADFAF71A70E942654F20 (void);
// 0x000000A3 Mediapipe.Status Mediapipe.CalculatorGraph::StartRun(Mediapipe.SidePacket)
extern void CalculatorGraph_StartRun_mFC0C313F1EE00E6B807B5F9F38E8D83BA8EB0652 (void);
// 0x000000A4 Mediapipe.Status Mediapipe.CalculatorGraph::WaitUntilIdle()
extern void CalculatorGraph_WaitUntilIdle_m23B76876DD0079A039C5D8884DAF708F02CA0626 (void);
// 0x000000A5 Mediapipe.Status Mediapipe.CalculatorGraph::WaitUntilDone()
extern void CalculatorGraph_WaitUntilDone_mB927ED4B8553C4D9D18A78D7F35F99C8EE296A94 (void);
// 0x000000A6 System.Boolean Mediapipe.CalculatorGraph::HasError()
extern void CalculatorGraph_HasError_m729D4E56E080DDB79B0465E1E3B0BFDDE64077EA (void);
// 0x000000A7 Mediapipe.Status Mediapipe.CalculatorGraph::AddPacketToInputStream(System.String,Mediapipe.Packet`1<T>)
// 0x000000A8 Mediapipe.Status Mediapipe.CalculatorGraph::SetInputStreamMaxQueueSize(System.String,System.Int32)
extern void CalculatorGraph_SetInputStreamMaxQueueSize_m9CE86A65512DA9341B41DCEAF7BF4DC06ED76853 (void);
// 0x000000A9 Mediapipe.Status Mediapipe.CalculatorGraph::CloseInputStream(System.String)
extern void CalculatorGraph_CloseInputStream_m0B1B320656765ACDB380A8CD68A46E5D975646E5 (void);
// 0x000000AA Mediapipe.Status Mediapipe.CalculatorGraph::CloseAllPacketSources()
extern void CalculatorGraph_CloseAllPacketSources_m3C6F94EFFEC33EED48F154E750C7B7C8DF75627E (void);
// 0x000000AB System.Void Mediapipe.CalculatorGraph::Cancel()
extern void CalculatorGraph_Cancel_mB2521E5ACF6E453321D059307B614354D923C995 (void);
// 0x000000AC System.Boolean Mediapipe.CalculatorGraph::GraphInputStreamsClosed()
extern void CalculatorGraph_GraphInputStreamsClosed_m1DB432A0F05381983D76E04902D30311EC35410B (void);
// 0x000000AD System.Boolean Mediapipe.CalculatorGraph::IsNodeThrottled(System.Int32)
extern void CalculatorGraph_IsNodeThrottled_m101F9F7708F3AFA3FA9D5FC62F52C3930A0CF259 (void);
// 0x000000AE System.Boolean Mediapipe.CalculatorGraph::UnthrottleSources()
extern void CalculatorGraph_UnthrottleSources_m3E9FBFB1917AAAC09EA2BDF85239523DF2F1656C (void);
// 0x000000AF Mediapipe.GpuResources Mediapipe.CalculatorGraph::GetGpuResources()
extern void CalculatorGraph_GetGpuResources_m66F59E3EA07F1B06158CEC08E8B008AF3E15BC41 (void);
// 0x000000B0 Mediapipe.Status Mediapipe.CalculatorGraph::SetGpuResources(Mediapipe.GpuResources)
extern void CalculatorGraph_SetGpuResources_m642B43908918A1405EA7CC51249E282AC8A5A153 (void);
// 0x000000B1 System.Void Mediapipe.CalculatorGraph/NativePacketCallback::.ctor(System.Object,System.IntPtr)
extern void NativePacketCallback__ctor_m81ABAA8A6C21D63E15EE423115105AE8D9ADBDA1 (void);
// 0x000000B2 System.IntPtr Mediapipe.CalculatorGraph/NativePacketCallback::Invoke(System.IntPtr)
extern void NativePacketCallback_Invoke_m6F8D1AD548AD6CEEB9673C0AD8B6D12731949760 (void);
// 0x000000B3 System.IAsyncResult Mediapipe.CalculatorGraph/NativePacketCallback::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern void NativePacketCallback_BeginInvoke_mEB623D88F7259AF8A65F0A10C8A4DFABA1870D07 (void);
// 0x000000B4 System.IntPtr Mediapipe.CalculatorGraph/NativePacketCallback::EndInvoke(System.IAsyncResult)
extern void NativePacketCallback_EndInvoke_mA0C8A7CED34494EC24029BAE390F7FB145489A97 (void);
// 0x000000B5 System.Void Mediapipe.CalculatorGraph/PacketCallback`2::.ctor(System.Object,System.IntPtr)
// 0x000000B6 Mediapipe.Status Mediapipe.CalculatorGraph/PacketCallback`2::Invoke(T)
// 0x000000B7 System.IAsyncResult Mediapipe.CalculatorGraph/PacketCallback`2::BeginInvoke(T,System.AsyncCallback,System.Object)
// 0x000000B8 Mediapipe.Status Mediapipe.CalculatorGraph/PacketCallback`2::EndInvoke(System.IAsyncResult)
// 0x000000B9 System.Void Mediapipe.CalculatorGraph/<>c__DisplayClass11_0`2::.ctor()
// 0x000000BA System.IntPtr Mediapipe.CalculatorGraph/<>c__DisplayClass11_0`2::<ObserveOutputStream>b__0(System.IntPtr)
// 0x000000BB Mediapipe.CalculatorGraphConfig Mediapipe.CalculatorGraphConfigExtension::ParseFromTextFormat(Google.Protobuf.MessageParser`1<Mediapipe.CalculatorGraphConfig>,System.String)
extern void CalculatorGraphConfigExtension_ParseFromTextFormat_mC08DFC40A3661668ABA0670D41E52A2DD78F74D6 (void);
// 0x000000BC System.Void Mediapipe.ImageFormat::.ctor()
extern void ImageFormat__ctor_m420FD54FCB00542E8EB1EE191B34605A8DF7C700 (void);
// 0x000000BD System.Void Mediapipe.ImageFrame::.ctor()
extern void ImageFrame__ctor_m29D5F204DF5BA3E608B4A89A37601FB2A3B37BA3 (void);
// 0x000000BE System.Void Mediapipe.ImageFrame::.ctor(System.IntPtr,System.Boolean)
extern void ImageFrame__ctor_m2010F6431940810F02B702E606A183C2D680ADDC (void);
// 0x000000BF System.Void Mediapipe.ImageFrame::.ctor(Mediapipe.ImageFormat/Format,System.Int32,System.Int32)
extern void ImageFrame__ctor_m01A45B52CABA11826A12FC34559CEFCBF12337E3 (void);
// 0x000000C0 System.Void Mediapipe.ImageFrame::.ctor(Mediapipe.ImageFormat/Format,System.Int32,System.Int32,System.UInt32)
extern void ImageFrame__ctor_m93B466136759DF1B0715B77DFF30F203CC854C85 (void);
// 0x000000C1 System.Void Mediapipe.ImageFrame::.ctor(Mediapipe.ImageFormat/Format,System.Int32,System.Int32,System.Int32,Unity.Collections.NativeArray`1<System.Byte>)
extern void ImageFrame__ctor_mC57C2480EABF2293E41108FCA225FA28D61C87DB (void);
// 0x000000C2 System.Void Mediapipe.ImageFrame::DeleteMpPtr()
extern void ImageFrame_DeleteMpPtr_m494FDB3BF93AB7FFE38984E082187397A016A795 (void);
// 0x000000C3 System.Void Mediapipe.ImageFrame::ReleasePixelData(System.IntPtr)
extern void ImageFrame_ReleasePixelData_mDF81706134475903E835D1F61193281D8FB96A78 (void);
// 0x000000C4 System.Boolean Mediapipe.ImageFrame::IsEmpty()
extern void ImageFrame_IsEmpty_m7912CACE26E03C46CB8F83F88C320066A102D3BF (void);
// 0x000000C5 System.Boolean Mediapipe.ImageFrame::IsContiguous()
extern void ImageFrame_IsContiguous_m58B08C6C48E05CB2F01D445A7CF6A04FE8B61E8C (void);
// 0x000000C6 System.Boolean Mediapipe.ImageFrame::IsAligned(System.UInt32)
extern void ImageFrame_IsAligned_m6F6B228BEA320E63BA6BAD80E2A19D00291DC4EB (void);
// 0x000000C7 Mediapipe.ImageFormat/Format Mediapipe.ImageFrame::Format()
extern void ImageFrame_Format_m6B77B6314857CBE754511605C80D1857A945EDCB (void);
// 0x000000C8 System.Int32 Mediapipe.ImageFrame::Width()
extern void ImageFrame_Width_m3304C2896325DA21083D049787DD1099E7D93055 (void);
// 0x000000C9 System.Int32 Mediapipe.ImageFrame::Height()
extern void ImageFrame_Height_m6A48AB4A397EA894CC934B93EEC339404E15A11C (void);
// 0x000000CA System.Int32 Mediapipe.ImageFrame::ChannelSize()
extern void ImageFrame_ChannelSize_m3124500B1632D837AA9E93DD0184079ACAA5E5E6 (void);
// 0x000000CB System.Int32 Mediapipe.ImageFrame::NumberOfChannels()
extern void ImageFrame_NumberOfChannels_m2E5790091664B6745BF9BA2F8CBB71AB9350CC82 (void);
// 0x000000CC System.Int32 Mediapipe.ImageFrame::ByteDepth()
extern void ImageFrame_ByteDepth_m638AE8635AB0A084AA6A129E7C3D2E14F0CBB45C (void);
// 0x000000CD System.Int32 Mediapipe.ImageFrame::WidthStep()
extern void ImageFrame_WidthStep_m909F70841936B4AED0662AF47C6C338CA608AB6E (void);
// 0x000000CE System.IntPtr Mediapipe.ImageFrame::MutablePixelData()
extern void ImageFrame_MutablePixelData_mF01774C70E45D02B97516D6068A3F743BCF2EDBF (void);
// 0x000000CF System.Int32 Mediapipe.ImageFrame::PixelDataSize()
extern void ImageFrame_PixelDataSize_mAFF965177FDBE24FD084798066E749CFE27FFA5D (void);
// 0x000000D0 System.Int32 Mediapipe.ImageFrame::PixelDataSizeStoredContiguously()
extern void ImageFrame_PixelDataSizeStoredContiguously_m68E91BE457D940A9330D008BF27272C369337171 (void);
// 0x000000D1 System.Void Mediapipe.ImageFrame::SetToZero()
extern void ImageFrame_SetToZero_m161454E46ECE6342D6BD5CA522D06975E3B003D0 (void);
// 0x000000D2 System.Void Mediapipe.ImageFrame::SetAlignmentPaddingAreas()
extern void ImageFrame_SetAlignmentPaddingAreas_m5F12CAEDC2D558C3D8DD96E1FB4CF51BFF2A3218 (void);
// 0x000000D3 System.Byte[] Mediapipe.ImageFrame::CopyToByteBuffer(System.Int32)
extern void ImageFrame_CopyToByteBuffer_m9AA396B301E4C66B72243E42EB86049E3AC068EC (void);
// 0x000000D4 System.UInt16[] Mediapipe.ImageFrame::CopyToUshortBuffer(System.Int32)
extern void ImageFrame_CopyToUshortBuffer_m960C7E177961D859DCFCF95D9845864CF2342524 (void);
// 0x000000D5 System.Single[] Mediapipe.ImageFrame::CopyToFloatBuffer(System.Int32)
extern void ImageFrame_CopyToFloatBuffer_m4C3FDC93762FDC679FA0AE44621347FC08ADD11D (void);
// 0x000000D6 UnityEngine.Color32[] Mediapipe.ImageFrame::GetColor32s(System.Boolean)
extern void ImageFrame_GetColor32s_mD2096829CCF07C9F53B7A33C849225B33E3B56F3 (void);
// 0x000000D7 Mediapipe.ImageFrame Mediapipe.ImageFrame::FromPixels32(UnityEngine.Color32[],System.Int32,System.Int32,System.Boolean)
extern void ImageFrame_FromPixels32_m9120673B8A0650BD04B61D67A06C46308ED5F1C8 (void);
// 0x000000D8 T[] Mediapipe.ImageFrame::CopyToBuffer(Mediapipe.ImageFrame/CopyToBufferHandler,System.Int32)
// 0x000000D9 T Mediapipe.ImageFrame::ValueOrFormatException(Mediapipe.MpReturnCode,T)
// 0x000000DA System.Void Mediapipe.ImageFrame::.cctor()
extern void ImageFrame__cctor_m5A12C8FDAC49DA6B2E4C371E598DCA3B73C7171E (void);
// 0x000000DB System.Void Mediapipe.ImageFrame/Deleter::.ctor(System.Object,System.IntPtr)
extern void Deleter__ctor_mBB58A201C41ABF007FA3CEDE78267CABDDC5D4FA (void);
// 0x000000DC System.Void Mediapipe.ImageFrame/Deleter::Invoke(System.IntPtr)
extern void Deleter_Invoke_mC17A2FF230D6866E5DD2D6E540AE5C33CF61D6E0 (void);
// 0x000000DD System.IAsyncResult Mediapipe.ImageFrame/Deleter::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern void Deleter_BeginInvoke_m9C7D93CB27971E6A5FF8501E19796058A67A7A9E (void);
// 0x000000DE System.Void Mediapipe.ImageFrame/Deleter::EndInvoke(System.IAsyncResult)
extern void Deleter_EndInvoke_m32533BA7EA9D551EED7D9D7F03A16C3194C46E20 (void);
// 0x000000DF System.Void Mediapipe.ImageFrame/CopyToBufferHandler::.ctor(System.Object,System.IntPtr)
extern void CopyToBufferHandler__ctor_m594E51AF66793E7829C89EEB20453C8F968537DB (void);
// 0x000000E0 Mediapipe.MpReturnCode Mediapipe.ImageFrame/CopyToBufferHandler::Invoke(System.IntPtr,System.IntPtr,System.Int32)
extern void CopyToBufferHandler_Invoke_mDAD87CAE73D1954DA71BBEFA9855119C09FE841A (void);
// 0x000000E1 System.IAsyncResult Mediapipe.ImageFrame/CopyToBufferHandler::BeginInvoke(System.IntPtr,System.IntPtr,System.Int32,System.AsyncCallback,System.Object)
extern void CopyToBufferHandler_BeginInvoke_mD3B015953708075E3B6178F48ACB01C63C108843 (void);
// 0x000000E2 Mediapipe.MpReturnCode Mediapipe.ImageFrame/CopyToBufferHandler::EndInvoke(System.IAsyncResult)
extern void CopyToBufferHandler_EndInvoke_m099390B5454C2A85CA354C983188E93BE6B20FDF (void);
// 0x000000E3 System.Void Mediapipe.OutputStreamPoller`1::.ctor(System.IntPtr)
// 0x000000E4 System.Void Mediapipe.OutputStreamPoller`1::DeleteMpPtr()
// 0x000000E5 System.Boolean Mediapipe.OutputStreamPoller`1::Next(Mediapipe.Packet`1<T>)
// 0x000000E6 System.Void Mediapipe.BoolPacket::.ctor()
extern void BoolPacket__ctor_mEE22B0E58DF3C687C6080F4BBDFEC9711B37CDC7 (void);
// 0x000000E7 System.Void Mediapipe.BoolPacket::.ctor(System.IntPtr,System.Boolean)
extern void BoolPacket__ctor_m4B7F1FC58A8A9D29A6F3E41EC836BC7F2167D7E9 (void);
// 0x000000E8 System.Void Mediapipe.BoolPacket::.ctor(System.Boolean)
extern void BoolPacket__ctor_m015E8D32154E855F3BB265B46FF981DEF11BF0B1 (void);
// 0x000000E9 System.Void Mediapipe.BoolPacket::.ctor(System.Boolean,Mediapipe.Timestamp)
extern void BoolPacket__ctor_m6E643109D20FB0EF20CE617FCF3FCC2F0B4BB579 (void);
// 0x000000EA System.Boolean Mediapipe.BoolPacket::Get()
extern void BoolPacket_Get_m8573DF1E3D437F3529FAF8F1B61A67BBCDD737EA (void);
// 0x000000EB Mediapipe.StatusOr`1<System.Boolean> Mediapipe.BoolPacket::Consume()
extern void BoolPacket_Consume_mFB8814960CB85F85438EDAB217DC842F1EAF1B19 (void);
// 0x000000EC Mediapipe.Status Mediapipe.BoolPacket::ValidateAsType()
extern void BoolPacket_ValidateAsType_m701BB77A9DC1C68E0A90C14519B9DC0FA4EFEA4B (void);
// 0x000000ED System.Void Mediapipe.ClassificationListPacket::.ctor()
extern void ClassificationListPacket__ctor_m7D8BB7BF96CE4DD6909F0530D057940631992B90 (void);
// 0x000000EE System.Void Mediapipe.ClassificationListPacket::.ctor(System.IntPtr,System.Boolean)
extern void ClassificationListPacket__ctor_m6F5ABE97EF75801E06D7A4386EBEDC772E0ED256 (void);
// 0x000000EF Mediapipe.ClassificationList Mediapipe.ClassificationListPacket::Get()
extern void ClassificationListPacket_Get_mDFE6549E5F24ACBABA44FFF1C1951DB61F065693 (void);
// 0x000000F0 Mediapipe.StatusOr`1<Mediapipe.ClassificationList> Mediapipe.ClassificationListPacket::Consume()
extern void ClassificationListPacket_Consume_mFB138C8195DAFEC6FF40197FAF671488A2DDA3C5 (void);
// 0x000000F1 System.Void Mediapipe.ClassificationListVectorPacket::.ctor()
extern void ClassificationListVectorPacket__ctor_mF0C5750DB5C5F62D1701CD7FCAF5515FE11F2DBD (void);
// 0x000000F2 System.Void Mediapipe.ClassificationListVectorPacket::.ctor(System.IntPtr,System.Boolean)
extern void ClassificationListVectorPacket__ctor_m4EB266F98C0E27EECD91DE06821806D532FE6652 (void);
// 0x000000F3 System.Collections.Generic.List`1<Mediapipe.ClassificationList> Mediapipe.ClassificationListVectorPacket::Get()
extern void ClassificationListVectorPacket_Get_m602FBADF65C1389449E17EBE9942BA2396BE3022 (void);
// 0x000000F4 Mediapipe.StatusOr`1<System.Collections.Generic.List`1<Mediapipe.ClassificationList>> Mediapipe.ClassificationListVectorPacket::Consume()
extern void ClassificationListVectorPacket_Consume_m2401F688ECBFB4B789DE258D12FFD58561E497DB (void);
// 0x000000F5 System.Void Mediapipe.DetectionPacket::.ctor()
extern void DetectionPacket__ctor_m70A631AC1200AA90328E94CA6D3F4A0114AAA712 (void);
// 0x000000F6 System.Void Mediapipe.DetectionPacket::.ctor(System.IntPtr,System.Boolean)
extern void DetectionPacket__ctor_m14188B6220F109A7CBFABAC440E7A7D3C8B3CB0F (void);
// 0x000000F7 Mediapipe.Detection Mediapipe.DetectionPacket::Get()
extern void DetectionPacket_Get_mC7A86B2FB0B47FA4394CED9D64B16FEB93E78A28 (void);
// 0x000000F8 Mediapipe.StatusOr`1<Mediapipe.Detection> Mediapipe.DetectionPacket::Consume()
extern void DetectionPacket_Consume_mD410C3EB7420273D6012988B5879E444F4E574E4 (void);
// 0x000000F9 System.Void Mediapipe.DetectionVectorPacket::.ctor()
extern void DetectionVectorPacket__ctor_m84905BC9D1D2C722F8032AE4ECCC5CCE338A8FAA (void);
// 0x000000FA System.Void Mediapipe.DetectionVectorPacket::.ctor(System.IntPtr,System.Boolean)
extern void DetectionVectorPacket__ctor_m4923465D388C8D4BFA389B0D863E06E184EFE9A8 (void);
// 0x000000FB System.Collections.Generic.List`1<Mediapipe.Detection> Mediapipe.DetectionVectorPacket::Get()
extern void DetectionVectorPacket_Get_m1A52D9A60177DFEB75BAA4994935CC8A3CAE250E (void);
// 0x000000FC Mediapipe.StatusOr`1<System.Collections.Generic.List`1<Mediapipe.Detection>> Mediapipe.DetectionVectorPacket::Consume()
extern void DetectionVectorPacket_Consume_mEE7E7CC4C5AFB803328A2859E8B4543F6FF5A659 (void);
// 0x000000FD System.Void Mediapipe.EglSurfaceHolderPacket::.ctor()
extern void EglSurfaceHolderPacket__ctor_mF78E88A8A6359A8555991B23D381286563B6B6C5 (void);
// 0x000000FE System.Void Mediapipe.EglSurfaceHolderPacket::.ctor(System.IntPtr,System.Boolean)
extern void EglSurfaceHolderPacket__ctor_m98FC5A830FB4C6ADA9046027C1701F0EC8338DF2 (void);
// 0x000000FF System.Void Mediapipe.EglSurfaceHolderPacket::.ctor(Mediapipe.EglSurfaceHolder)
extern void EglSurfaceHolderPacket__ctor_m1DD5B3B240C421D80FBAB771553F6CCE05310E09 (void);
// 0x00000100 Mediapipe.EglSurfaceHolder Mediapipe.EglSurfaceHolderPacket::Get()
extern void EglSurfaceHolderPacket_Get_m5E1464B27335BCC765CAE7E534B3C96A6C07A4BC (void);
// 0x00000101 Mediapipe.StatusOr`1<Mediapipe.EglSurfaceHolder> Mediapipe.EglSurfaceHolderPacket::Consume()
extern void EglSurfaceHolderPacket_Consume_m90583CEE4077F1D4E1C0C50B8CD12FB27F5A56C9 (void);
// 0x00000102 Mediapipe.Status Mediapipe.EglSurfaceHolderPacket::ValidateAsType()
extern void EglSurfaceHolderPacket_ValidateAsType_mFF906C70E7C7D117568FB88CE0927923E0A364FF (void);
// 0x00000103 System.Void Mediapipe.FaceGeometryPacket::.ctor()
extern void FaceGeometryPacket__ctor_mEC53EE49D2C63E4BBF95061B9FB86A46616C29F1 (void);
// 0x00000104 System.Void Mediapipe.FaceGeometryPacket::.ctor(System.IntPtr,System.Boolean)
extern void FaceGeometryPacket__ctor_m18C4AF0AC5FA7EB672A326BB4ED3AF752BA60DB3 (void);
// 0x00000105 Mediapipe.FaceGeometry.FaceGeometry Mediapipe.FaceGeometryPacket::Get()
extern void FaceGeometryPacket_Get_mFC9FE7711C8944067DEB26E2577063A83EAF9FB4 (void);
// 0x00000106 Mediapipe.StatusOr`1<Mediapipe.FaceGeometry.FaceGeometry> Mediapipe.FaceGeometryPacket::Consume()
extern void FaceGeometryPacket_Consume_mFB59B93264C62ED1AE5BC253AE7BBFE24176BA31 (void);
// 0x00000107 System.Void Mediapipe.FaceGeometryVectorPacket::.ctor()
extern void FaceGeometryVectorPacket__ctor_m459EF509CE7172963988C5CBED487B0DD00FE586 (void);
// 0x00000108 System.Void Mediapipe.FaceGeometryVectorPacket::.ctor(System.IntPtr,System.Boolean)
extern void FaceGeometryVectorPacket__ctor_mAF6EBA85D11220797E9D56C0376C1DD1CA057354 (void);
// 0x00000109 System.Collections.Generic.List`1<Mediapipe.FaceGeometry.FaceGeometry> Mediapipe.FaceGeometryVectorPacket::Get()
extern void FaceGeometryVectorPacket_Get_mAFF8E0CB41F0EC5D7AEBB7570708995518689178 (void);
// 0x0000010A Mediapipe.StatusOr`1<System.Collections.Generic.List`1<Mediapipe.FaceGeometry.FaceGeometry>> Mediapipe.FaceGeometryVectorPacket::Consume()
extern void FaceGeometryVectorPacket_Consume_mC1E76CEC7018B01D128568996AE42067B734FC7F (void);
// 0x0000010B System.Int32 Mediapipe.FloatArrayPacket::get_Length()
extern void FloatArrayPacket_get_Length_m68103A1AFD1107D6D687F4ECA1A8E7B6AE758A48 (void);
// 0x0000010C System.Void Mediapipe.FloatArrayPacket::set_Length(System.Int32)
extern void FloatArrayPacket_set_Length_mF1B7847E79DDB1D84031110539EDE34E93D52948 (void);
// 0x0000010D System.Void Mediapipe.FloatArrayPacket::.ctor()
extern void FloatArrayPacket__ctor_m476339A32806070580F3F2CF5219999A1E8606A4 (void);
// 0x0000010E System.Void Mediapipe.FloatArrayPacket::.ctor(System.IntPtr,System.Boolean)
extern void FloatArrayPacket__ctor_m8B32604AA9F13944B583A056BEF28D67DD491538 (void);
// 0x0000010F System.Void Mediapipe.FloatArrayPacket::.ctor(System.Single[])
extern void FloatArrayPacket__ctor_m4744778764B78D9BD731E13059EA1BAEF3EDA1F6 (void);
// 0x00000110 System.Void Mediapipe.FloatArrayPacket::.ctor(System.Single[],Mediapipe.Timestamp)
extern void FloatArrayPacket__ctor_m4EBD038F4964B10FC499B8592CE160B87BE75AE7 (void);
// 0x00000111 System.Single[] Mediapipe.FloatArrayPacket::Get()
extern void FloatArrayPacket_Get_m999ED4EFEFBAB026EECEA379BC1D52DA8F69DC3B (void);
// 0x00000112 System.IntPtr Mediapipe.FloatArrayPacket::GetArrayPtr()
extern void FloatArrayPacket_GetArrayPtr_mD4732D5649A29E5EB6F371204B874C3130F7DE29 (void);
// 0x00000113 Mediapipe.StatusOr`1<System.Single[]> Mediapipe.FloatArrayPacket::Consume()
extern void FloatArrayPacket_Consume_mF5A75E832D4CAF3CE1640E95E7B37E5F7AFB8EA8 (void);
// 0x00000114 Mediapipe.Status Mediapipe.FloatArrayPacket::ValidateAsType()
extern void FloatArrayPacket_ValidateAsType_mFEE6608EB63B92AE9C5D4EB76D312C11456D8326 (void);
// 0x00000115 System.Void Mediapipe.FloatPacket::.ctor()
extern void FloatPacket__ctor_m94D4DD12FE67B6672394A8C29EC5D1B8C926DC15 (void);
// 0x00000116 System.Void Mediapipe.FloatPacket::.ctor(System.IntPtr,System.Boolean)
extern void FloatPacket__ctor_m3EB8CB7BC8960F8FD755711692AB320BA8006E28 (void);
// 0x00000117 System.Void Mediapipe.FloatPacket::.ctor(System.Single)
extern void FloatPacket__ctor_m69CBE1B718A00C9D85C78338A78A62B0AF78E612 (void);
// 0x00000118 System.Void Mediapipe.FloatPacket::.ctor(System.Single,Mediapipe.Timestamp)
extern void FloatPacket__ctor_mE750051038E9A2C9665C8C7BB94ECF78A76703F2 (void);
// 0x00000119 System.Single Mediapipe.FloatPacket::Get()
extern void FloatPacket_Get_m8B69FF4C7DB98F3186125A496545A5F7745B2BB8 (void);
// 0x0000011A Mediapipe.StatusOr`1<System.Single> Mediapipe.FloatPacket::Consume()
extern void FloatPacket_Consume_mC8157E32C5E845CCFDE3C3129A4FEEAAC3D06D55 (void);
// 0x0000011B Mediapipe.Status Mediapipe.FloatPacket::ValidateAsType()
extern void FloatPacket_ValidateAsType_m22D1704DDEF951D702B4232135EEE089028479E4 (void);
// 0x0000011C System.Void Mediapipe.GpuBufferPacket::.ctor()
extern void GpuBufferPacket__ctor_m88A14BF857318BC48B291942A1F9241E3A877E55 (void);
// 0x0000011D System.Void Mediapipe.GpuBufferPacket::.ctor(System.IntPtr,System.Boolean)
extern void GpuBufferPacket__ctor_m41B47DB9422377B8160066B021C6F0D4DA4A8605 (void);
// 0x0000011E System.Void Mediapipe.GpuBufferPacket::.ctor(Mediapipe.GpuBuffer)
extern void GpuBufferPacket__ctor_mCB128ED7406480F17F4C10EB124B01078D8B64E2 (void);
// 0x0000011F System.Void Mediapipe.GpuBufferPacket::.ctor(Mediapipe.GpuBuffer,Mediapipe.Timestamp)
extern void GpuBufferPacket__ctor_m264281472954E7D49DA678A561026D588FA77F34 (void);
// 0x00000120 Mediapipe.GpuBuffer Mediapipe.GpuBufferPacket::Get()
extern void GpuBufferPacket_Get_m8250DCA09877B9706616E7530AC4FD462E884714 (void);
// 0x00000121 Mediapipe.StatusOr`1<Mediapipe.GpuBuffer> Mediapipe.GpuBufferPacket::Consume()
extern void GpuBufferPacket_Consume_m54A8F02F46DAD644282B922D1A0F7F6910B76164 (void);
// 0x00000122 Mediapipe.Status Mediapipe.GpuBufferPacket::ValidateAsType()
extern void GpuBufferPacket_ValidateAsType_mB2969F3867B4DA5E0BB15F7539E6B69397A8DDDE (void);
// 0x00000123 System.Void Mediapipe.ImageFramePacket::.ctor()
extern void ImageFramePacket__ctor_m8C42C8E1ACE52FE6DAE2FC8FF41373D1CBD90B7E (void);
// 0x00000124 System.Void Mediapipe.ImageFramePacket::.ctor(System.IntPtr,System.Boolean)
extern void ImageFramePacket__ctor_mE506EA2475806BD9B8E02FB8DA65B64CCCD4A22D (void);
// 0x00000125 System.Void Mediapipe.ImageFramePacket::.ctor(Mediapipe.ImageFrame)
extern void ImageFramePacket__ctor_m62F88D26F8094EB837D8E940DF278B2F60625F3F (void);
// 0x00000126 System.Void Mediapipe.ImageFramePacket::.ctor(Mediapipe.ImageFrame,Mediapipe.Timestamp)
extern void ImageFramePacket__ctor_mFDDD14887EE56428667A17AC7F93EE832D80DDA3 (void);
// 0x00000127 Mediapipe.ImageFrame Mediapipe.ImageFramePacket::Get()
extern void ImageFramePacket_Get_m5F81B855F3053CCE9F6BE213009E9A7DE9F3C1C0 (void);
// 0x00000128 Mediapipe.StatusOr`1<Mediapipe.ImageFrame> Mediapipe.ImageFramePacket::Consume()
extern void ImageFramePacket_Consume_m005203727D1EBD3C486737CAB25330B2C1369B6C (void);
// 0x00000129 Mediapipe.Status Mediapipe.ImageFramePacket::ValidateAsType()
extern void ImageFramePacket_ValidateAsType_m7510228BF540F55078E09A0BE57F7F4FE5F049F3 (void);
// 0x0000012A System.Void Mediapipe.IntPacket::.ctor()
extern void IntPacket__ctor_m07BF80086BBB1A25E7371F8CB381665EF923ACF0 (void);
// 0x0000012B System.Void Mediapipe.IntPacket::.ctor(System.IntPtr,System.Boolean)
extern void IntPacket__ctor_mE962567B69D3B89693E8C3FC2AA941B57E130E41 (void);
// 0x0000012C System.Void Mediapipe.IntPacket::.ctor(System.Int32)
extern void IntPacket__ctor_m76E92AD15634EC1F08604A6FDA03AA78EB87E7BB (void);
// 0x0000012D System.Void Mediapipe.IntPacket::.ctor(System.Int32,Mediapipe.Timestamp)
extern void IntPacket__ctor_mD7F292657D6386C4F0A424EC6ED5DD5D68728278 (void);
// 0x0000012E System.Int32 Mediapipe.IntPacket::Get()
extern void IntPacket_Get_mBBC1199637935AD3AA6F65FACFF20E908DEA929D (void);
// 0x0000012F Mediapipe.StatusOr`1<System.Int32> Mediapipe.IntPacket::Consume()
extern void IntPacket_Consume_mF8B617979BE7123778BCF55BA71807FC225BB9C5 (void);
// 0x00000130 Mediapipe.Status Mediapipe.IntPacket::ValidateAsType()
extern void IntPacket_ValidateAsType_m239AB41E9DF7DD535579BB42F79D0D00D314238E (void);
// 0x00000131 System.Void Mediapipe.LandmarkListPacket::.ctor()
extern void LandmarkListPacket__ctor_mB519037236737ED36145D0C242A3056941A6DC05 (void);
// 0x00000132 System.Void Mediapipe.LandmarkListPacket::.ctor(System.IntPtr,System.Boolean)
extern void LandmarkListPacket__ctor_m7908377EC47937ADD6B5EB95D3408FC55E142BB5 (void);
// 0x00000133 Mediapipe.LandmarkList Mediapipe.LandmarkListPacket::Get()
extern void LandmarkListPacket_Get_m9CA05B24C36E55F4F7D7A048A2B60C7A60C2DBE7 (void);
// 0x00000134 Mediapipe.StatusOr`1<Mediapipe.LandmarkList> Mediapipe.LandmarkListPacket::Consume()
extern void LandmarkListPacket_Consume_mD2AD293B62BF76437A17145F58F3A94B6F5E8C4C (void);
// 0x00000135 System.Void Mediapipe.LandmarkListVectorPacket::.ctor()
extern void LandmarkListVectorPacket__ctor_mB8751CC00D67D9A53EF23E429AA8E3D3C9149371 (void);
// 0x00000136 System.Void Mediapipe.LandmarkListVectorPacket::.ctor(System.IntPtr,System.Boolean)
extern void LandmarkListVectorPacket__ctor_m5292AE0ACD60A81C934E1DC644231E4FE8F5E1AE (void);
// 0x00000137 System.Collections.Generic.List`1<Mediapipe.LandmarkList> Mediapipe.LandmarkListVectorPacket::Get()
extern void LandmarkListVectorPacket_Get_mB39D04D8428292BB3D3C0988B306FDCE8F9262EB (void);
// 0x00000138 Mediapipe.StatusOr`1<System.Collections.Generic.List`1<Mediapipe.LandmarkList>> Mediapipe.LandmarkListVectorPacket::Consume()
extern void LandmarkListVectorPacket_Consume_m723B97443E78FBA6B1C6951F777165B6A8EC033E (void);
// 0x00000139 System.Void Mediapipe.NormalizedLandmarkListPacket::.ctor()
extern void NormalizedLandmarkListPacket__ctor_m8146FE9538D4496BA288556EAEBE3935B694DDC6 (void);
// 0x0000013A System.Void Mediapipe.NormalizedLandmarkListPacket::.ctor(System.IntPtr,System.Boolean)
extern void NormalizedLandmarkListPacket__ctor_mF7DCC4B6BA9677D972F2E26AB50EC451DFFA266A (void);
// 0x0000013B Mediapipe.NormalizedLandmarkList Mediapipe.NormalizedLandmarkListPacket::Get()
extern void NormalizedLandmarkListPacket_Get_m966758F9DE1713DF2CE377746E3841CEF7D9724B (void);
// 0x0000013C Mediapipe.StatusOr`1<Mediapipe.NormalizedLandmarkList> Mediapipe.NormalizedLandmarkListPacket::Consume()
extern void NormalizedLandmarkListPacket_Consume_m804E7B6D85E27A1F59B17DF57579C6C4EBCB1D12 (void);
// 0x0000013D System.Void Mediapipe.NormalizedLandmarkListVectorPacket::.ctor()
extern void NormalizedLandmarkListVectorPacket__ctor_m8A2AA4FE3890246F4048F9CFEE4A447D665614A2 (void);
// 0x0000013E System.Void Mediapipe.NormalizedLandmarkListVectorPacket::.ctor(System.IntPtr,System.Boolean)
extern void NormalizedLandmarkListVectorPacket__ctor_m2998B1890D97FEEECA1DFCCA4FE6893372398719 (void);
// 0x0000013F System.Collections.Generic.List`1<Mediapipe.NormalizedLandmarkList> Mediapipe.NormalizedLandmarkListVectorPacket::Get()
extern void NormalizedLandmarkListVectorPacket_Get_mFFC22EC67235BEF8E943C80E310437DF9441744B (void);
// 0x00000140 Mediapipe.StatusOr`1<System.Collections.Generic.List`1<Mediapipe.NormalizedLandmarkList>> Mediapipe.NormalizedLandmarkListVectorPacket::Consume()
extern void NormalizedLandmarkListVectorPacket_Consume_m1807E6D5795354CD0B1B2AE28D733EAC91C8A2E7 (void);
// 0x00000141 System.Void Mediapipe.NormalizedRectPacket::.ctor()
extern void NormalizedRectPacket__ctor_m6C1495F3EA126FB7D6BA735EF64BC57E82022F59 (void);
// 0x00000142 System.Void Mediapipe.NormalizedRectPacket::.ctor(System.IntPtr,System.Boolean)
extern void NormalizedRectPacket__ctor_mF4FBD2D256CFF83792D7499056418C9E26AA93AC (void);
// 0x00000143 Mediapipe.NormalizedRect Mediapipe.NormalizedRectPacket::Get()
extern void NormalizedRectPacket_Get_m55E780812C034845667FDD2D3686F47C74CF4707 (void);
// 0x00000144 Mediapipe.StatusOr`1<Mediapipe.NormalizedRect> Mediapipe.NormalizedRectPacket::Consume()
extern void NormalizedRectPacket_Consume_mF3DB38A19AA0DDFE20C1FA218A13E9B53A3B160A (void);
// 0x00000145 System.Void Mediapipe.NormalizedRectVectorPacket::.ctor()
extern void NormalizedRectVectorPacket__ctor_m8866E2D88AE2CF73A4B70F93A8C7D6C5F311FACF (void);
// 0x00000146 System.Void Mediapipe.NormalizedRectVectorPacket::.ctor(System.IntPtr,System.Boolean)
extern void NormalizedRectVectorPacket__ctor_mDA9127A3C79348C5FA80FCAE21C39DAB13AEB5EC (void);
// 0x00000147 System.Collections.Generic.List`1<Mediapipe.NormalizedRect> Mediapipe.NormalizedRectVectorPacket::Get()
extern void NormalizedRectVectorPacket_Get_m435F2E28DF7D9CF437739CAD8349EAEA10B6591A (void);
// 0x00000148 Mediapipe.StatusOr`1<System.Collections.Generic.List`1<Mediapipe.NormalizedRect>> Mediapipe.NormalizedRectVectorPacket::Consume()
extern void NormalizedRectVectorPacket_Consume_m9C00B4AB8B7FB901A474BAB2DDFC725501156704 (void);
// 0x00000149 System.Void Mediapipe.Packet`1::.ctor()
// 0x0000014A System.Void Mediapipe.Packet`1::.ctor(System.IntPtr,System.Boolean)
// 0x0000014B T Mediapipe.Packet`1::Get()
// 0x0000014C Mediapipe.StatusOr`1<T> Mediapipe.Packet`1::Consume()
// 0x0000014D Mediapipe.Packet`1<T> Mediapipe.Packet`1::At(Mediapipe.Timestamp)
// 0x0000014E Mediapipe.Status Mediapipe.Packet`1::ValidateAsProtoMessageLite()
// 0x0000014F Mediapipe.Status Mediapipe.Packet`1::ValidateAsType()
// 0x00000150 Mediapipe.Timestamp Mediapipe.Packet`1::Timestamp()
// 0x00000151 System.String Mediapipe.Packet`1::DebugString()
// 0x00000152 System.String Mediapipe.Packet`1::RegisteredTypeName()
// 0x00000153 System.String Mediapipe.Packet`1::DebugTypeName()
// 0x00000154 System.Void Mediapipe.Packet`1::DeleteMpPtr()
// 0x00000155 System.Void Mediapipe.RectPacket::.ctor()
extern void RectPacket__ctor_m397951D4B853FF046034D31D43F4A6412CE4FAA8 (void);
// 0x00000156 System.Void Mediapipe.RectPacket::.ctor(System.IntPtr,System.Boolean)
extern void RectPacket__ctor_mD63909E2139E69328D20043C87EB456EFDA5708D (void);
// 0x00000157 Mediapipe.Rect Mediapipe.RectPacket::Get()
extern void RectPacket_Get_mCF71D733DA3BADB44C7C308643EB57A13DF7CFE9 (void);
// 0x00000158 Mediapipe.StatusOr`1<Mediapipe.Rect> Mediapipe.RectPacket::Consume()
extern void RectPacket_Consume_mE49B0F253C83AADA2E1618C7E42EB571CBD05038 (void);
// 0x00000159 System.Void Mediapipe.RectVectorPacket::.ctor()
extern void RectVectorPacket__ctor_m20C7C5CDA3D814F26D2CE1B506D54E54E437484C (void);
// 0x0000015A System.Void Mediapipe.RectVectorPacket::.ctor(System.IntPtr,System.Boolean)
extern void RectVectorPacket__ctor_m3EBD3B7353132A2164E58446C744D9DB848AF54B (void);
// 0x0000015B System.Collections.Generic.List`1<Mediapipe.Rect> Mediapipe.RectVectorPacket::Get()
extern void RectVectorPacket_Get_m0558DC78485DC33CD2B83E42995ECF9730CC6546 (void);
// 0x0000015C Mediapipe.StatusOr`1<System.Collections.Generic.List`1<Mediapipe.Rect>> Mediapipe.RectVectorPacket::Consume()
extern void RectVectorPacket_Consume_m10DDE90499EADC09D44FD47F2ADA86F448B42ACD (void);
// 0x0000015D System.Void Mediapipe.SidePacket::.ctor()
extern void SidePacket__ctor_m94BBD634075945D876D1165B0124C48A9F55B60F (void);
// 0x0000015E System.Void Mediapipe.SidePacket::DeleteMpPtr()
extern void SidePacket_DeleteMpPtr_m42E775911F7B60DBCF1080984362B84614054183 (void);
// 0x0000015F System.Int32 Mediapipe.SidePacket::get_size()
extern void SidePacket_get_size_m158F3F0476D058B162A51B385A7AE98FD8D71A4F (void);
// 0x00000160 T Mediapipe.SidePacket::At(System.String)
// 0x00000161 System.Void Mediapipe.SidePacket::Emplace(System.String,Mediapipe.Packet`1<T>)
// 0x00000162 System.Int32 Mediapipe.SidePacket::Erase(System.String)
extern void SidePacket_Erase_m6D37DF450F205FE9E170530996746C3646DF215C (void);
// 0x00000163 System.Void Mediapipe.SidePacket::Clear()
extern void SidePacket_Clear_mE1D68C3A1CBB09B03A8990C3ADDA38F406A60910 (void);
// 0x00000164 System.Void Mediapipe.StringPacket::.ctor()
extern void StringPacket__ctor_mBB141B0BB40EC079E68BEC133596BFE7A050ED8F (void);
// 0x00000165 System.Void Mediapipe.StringPacket::.ctor(System.IntPtr,System.Boolean)
extern void StringPacket__ctor_m0C225D27A264A54D3FD0EBC4B1D87DE740073B45 (void);
// 0x00000166 System.Void Mediapipe.StringPacket::.ctor(System.String)
extern void StringPacket__ctor_m7628BDAA9CD2CAFBBD4F9D101B6FA8C63B8FE9F7 (void);
// 0x00000167 System.Void Mediapipe.StringPacket::.ctor(System.Byte[])
extern void StringPacket__ctor_m471CF1CECC04CD64E658768C22B5D62CFCE447FE (void);
// 0x00000168 System.Void Mediapipe.StringPacket::.ctor(System.String,Mediapipe.Timestamp)
extern void StringPacket__ctor_m5C56F71BEE91B48AFE934A6511B3C8621D7B658B (void);
// 0x00000169 System.Void Mediapipe.StringPacket::.ctor(System.Byte[],Mediapipe.Timestamp)
extern void StringPacket__ctor_mDAD7F17DE476143ABF079FE0DD4A1232D3846869 (void);
// 0x0000016A System.String Mediapipe.StringPacket::Get()
extern void StringPacket_Get_mCB1372C9A02FCE412CFC0707AA28351DD979DC88 (void);
// 0x0000016B System.Byte[] Mediapipe.StringPacket::GetByteArray()
extern void StringPacket_GetByteArray_m8ABA453AEB363023B50B507D9F764921FA93FF31 (void);
// 0x0000016C Mediapipe.StatusOr`1<System.String> Mediapipe.StringPacket::Consume()
extern void StringPacket_Consume_m47C8076F9CF78B27C2D82CF00BAF3E194180FC44 (void);
// 0x0000016D Mediapipe.Status Mediapipe.StringPacket::ValidateAsType()
extern void StringPacket_ValidateAsType_m5B09B9DA8991CF004E238BBEAB7770226FC227D0 (void);
// 0x0000016E System.Void Mediapipe.TimedModelMatrixProtoListPacket::.ctor()
extern void TimedModelMatrixProtoListPacket__ctor_m95CAC2FCEBDBE47EAB99458705A3379863B487A4 (void);
// 0x0000016F System.Void Mediapipe.TimedModelMatrixProtoListPacket::.ctor(System.IntPtr,System.Boolean)
extern void TimedModelMatrixProtoListPacket__ctor_m4E6BFC9894B85C14E95D5BD63C9F5BF8300C180F (void);
// 0x00000170 Mediapipe.TimedModelMatrixProtoList Mediapipe.TimedModelMatrixProtoListPacket::Get()
extern void TimedModelMatrixProtoListPacket_Get_m6C96E21574E7978CDA1C1FCAC67B09BA22182913 (void);
// 0x00000171 Mediapipe.StatusOr`1<Mediapipe.TimedModelMatrixProtoList> Mediapipe.TimedModelMatrixProtoListPacket::Consume()
extern void TimedModelMatrixProtoListPacket_Consume_m22BDBFF6C7A401480A6EA851F01313BF06E3E88D (void);
// 0x00000172 System.Void Mediapipe.Status::.ctor(System.IntPtr,System.Boolean)
extern void Status__ctor_m9C7377654210F203C94885752B242DBE89331CEE (void);
// 0x00000173 System.Void Mediapipe.Status::DeleteMpPtr()
extern void Status_DeleteMpPtr_m419E44E0CD2FB970DA950DDDC296F4E48DED3334 (void);
// 0x00000174 System.Boolean Mediapipe.Status::IsOk()
extern void Status_IsOk_m1AB7D6193A62F662ECA6115FE48501605923C4BF (void);
// 0x00000175 System.Boolean Mediapipe.Status::get_ok()
extern void Status_get_ok_mA24DE1BC9E9B794EF40FC34F7D7DE10A75D9BA89 (void);
// 0x00000176 System.Void Mediapipe.Status::AssertOk()
extern void Status_AssertOk_m5D7CAE9C845DFCF680FA8FA02535448DB73378BA (void);
// 0x00000177 Mediapipe.Status/StatusCode Mediapipe.Status::get_code()
extern void Status_get_code_mE216C5A6ECB9C9C2FBDEEDBD596EE8FB937786A4 (void);
// 0x00000178 System.Int32 Mediapipe.Status::get_rawCode()
extern void Status_get_rawCode_m722ABD74814432559C14E91A51A5F3AB2A831959 (void);
// 0x00000179 System.String Mediapipe.Status::ToString()
extern void Status_ToString_m43D732AB911552FBFE10F1442A4F6EF700EF883C (void);
// 0x0000017A Mediapipe.Status Mediapipe.Status::Build(Mediapipe.Status/StatusCode,System.String,System.Boolean)
extern void Status_Build_m6A3A71C812358028B629FB901002FA5E4FB9CBC6 (void);
// 0x0000017B Mediapipe.Status Mediapipe.Status::Ok(System.Boolean)
extern void Status_Ok_m562FAD5DC80FDFD4E4E08B39C0E551AB38471A5C (void);
// 0x0000017C Mediapipe.Status Mediapipe.Status::FailedPrecondition(System.String,System.Boolean)
extern void Status_FailedPrecondition_m9206B46B17436E94E7C15CDFB5BBD621FF546DE9 (void);
// 0x0000017D System.IntPtr Mediapipe.Status::GetPtr()
extern void Status_GetPtr_mB5ACB15D7C5332303206547CF6AF7E1AFF3DC267 (void);
// 0x0000017E System.Void Mediapipe.StatusOr`1::.ctor(System.IntPtr)
// 0x0000017F System.Boolean Mediapipe.StatusOr`1::get_ok()
// 0x00000180 Mediapipe.Status Mediapipe.StatusOr`1::get_status()
// 0x00000181 T Mediapipe.StatusOr`1::ValueOr(T)
// 0x00000182 T Mediapipe.StatusOr`1::Value()
// 0x00000183 System.Void Mediapipe.StatusOrGpuBuffer::.ctor(System.IntPtr)
extern void StatusOrGpuBuffer__ctor_mE24B41F053AF28D40D7C96CD0FE0FD6A2553127A (void);
// 0x00000184 System.Void Mediapipe.StatusOrGpuBuffer::DeleteMpPtr()
extern void StatusOrGpuBuffer_DeleteMpPtr_m3505E083EC078519ACCA212C5F64D42E6BEA17E0 (void);
// 0x00000185 System.Boolean Mediapipe.StatusOrGpuBuffer::get_ok()
extern void StatusOrGpuBuffer_get_ok_m4FEBB5F31D15FE3BCAE7C459EFE808C37DF3F5A2 (void);
// 0x00000186 Mediapipe.Status Mediapipe.StatusOrGpuBuffer::get_status()
extern void StatusOrGpuBuffer_get_status_m0D1E1EA95E28140596F8A0C44B83646E5C427CCA (void);
// 0x00000187 Mediapipe.GpuBuffer Mediapipe.StatusOrGpuBuffer::Value()
extern void StatusOrGpuBuffer_Value_m2A0536109A5976811746230BD1E97ECF21A8EEFB (void);
// 0x00000188 System.Void Mediapipe.StatusOrGpuResources::.ctor(System.IntPtr)
extern void StatusOrGpuResources__ctor_mCB2C02477107410DAA5414C8A11A91A8097D353F (void);
// 0x00000189 System.Void Mediapipe.StatusOrGpuResources::DeleteMpPtr()
extern void StatusOrGpuResources_DeleteMpPtr_m0D0DE02E8D3FE8B4B015EAD3AA48D341E5AF1013 (void);
// 0x0000018A System.Boolean Mediapipe.StatusOrGpuResources::get_ok()
extern void StatusOrGpuResources_get_ok_mBA8B9F8EBC2098830E59F865216B96CB517DB317 (void);
// 0x0000018B Mediapipe.Status Mediapipe.StatusOrGpuResources::get_status()
extern void StatusOrGpuResources_get_status_m0EF93F6A9D499FACF5692B14F9C7E545AD272802 (void);
// 0x0000018C Mediapipe.GpuResources Mediapipe.StatusOrGpuResources::Value()
extern void StatusOrGpuResources_Value_m00F3851C340D6EE8526D1A4972AE71CDBA1845C4 (void);
// 0x0000018D System.Void Mediapipe.StatusOrImageFrame::.ctor(System.IntPtr)
extern void StatusOrImageFrame__ctor_m3F3BD600C6E96CE4AD3C44AF0C081B0FE38324CE (void);
// 0x0000018E System.Void Mediapipe.StatusOrImageFrame::DeleteMpPtr()
extern void StatusOrImageFrame_DeleteMpPtr_m9D13A0FCD189EBBB32A8A33DE3C064F2AE110709 (void);
// 0x0000018F System.Boolean Mediapipe.StatusOrImageFrame::get_ok()
extern void StatusOrImageFrame_get_ok_mCBCE759B46C402A2F70758C29CEA35C2F0FC1554 (void);
// 0x00000190 Mediapipe.Status Mediapipe.StatusOrImageFrame::get_status()
extern void StatusOrImageFrame_get_status_mC983700682CC7620FDDBC833B1BBD953FA5A8711 (void);
// 0x00000191 Mediapipe.ImageFrame Mediapipe.StatusOrImageFrame::Value()
extern void StatusOrImageFrame_Value_m6757DFC34202610E8539EF99F2348260F22F8367 (void);
// 0x00000192 System.Void Mediapipe.StatusOrPoller`1::.ctor(System.IntPtr)
// 0x00000193 System.Void Mediapipe.StatusOrPoller`1::DeleteMpPtr()
// 0x00000194 System.Boolean Mediapipe.StatusOrPoller`1::get_ok()
// 0x00000195 Mediapipe.Status Mediapipe.StatusOrPoller`1::get_status()
// 0x00000196 Mediapipe.OutputStreamPoller`1<T> Mediapipe.StatusOrPoller`1::Value()
// 0x00000197 System.Void Mediapipe.Timestamp::.ctor(System.IntPtr)
extern void Timestamp__ctor_m8984121477483A8AECBFAA63B42671654ADF76E5 (void);
// 0x00000198 System.Void Mediapipe.Timestamp::.ctor(System.Int64)
extern void Timestamp__ctor_mD6BA157AECDF613EFD3712BC891FFC56C803E382 (void);
// 0x00000199 System.Void Mediapipe.Timestamp::DeleteMpPtr()
extern void Timestamp_DeleteMpPtr_mFF8FC74D2902C2D070B2EEA3BF00D873E1B8841A (void);
// 0x0000019A System.Boolean Mediapipe.Timestamp::Equals(Mediapipe.Timestamp)
extern void Timestamp_Equals_mB921E5812E7C119E66951F63F023AF46F76360EE (void);
// 0x0000019B System.Boolean Mediapipe.Timestamp::Equals(System.Object)
extern void Timestamp_Equals_m09DAA18C300695E041E4478D89F943395F29B095 (void);
// 0x0000019C System.Boolean Mediapipe.Timestamp::op_Equality(Mediapipe.Timestamp,Mediapipe.Timestamp)
extern void Timestamp_op_Equality_m693EB078DE5DFFDCF5D4B4EC21090A2A5BD9A5DF (void);
// 0x0000019D System.Boolean Mediapipe.Timestamp::op_Inequality(Mediapipe.Timestamp,Mediapipe.Timestamp)
extern void Timestamp_op_Inequality_m84514AD05FB1FFBF9B68A722A83D82773A316694 (void);
// 0x0000019E System.Int32 Mediapipe.Timestamp::GetHashCode()
extern void Timestamp_GetHashCode_mB39CB76DB3A664F4327A75F5E6FC33FAA4CEE584 (void);
// 0x0000019F System.Int64 Mediapipe.Timestamp::Value()
extern void Timestamp_Value_m8ED1D5B544ADFBDFE6C22A54A51129354234A1A0 (void);
// 0x000001A0 System.Double Mediapipe.Timestamp::Seconds()
extern void Timestamp_Seconds_mD2208527892D8BBC5B0B0507DFC8C86003DEB0A2 (void);
// 0x000001A1 System.Int64 Mediapipe.Timestamp::Microseconds()
extern void Timestamp_Microseconds_m652EC31FBA7E8DA3D765846C9F2E52B1BB01184C (void);
// 0x000001A2 System.Boolean Mediapipe.Timestamp::IsSpecialValue()
extern void Timestamp_IsSpecialValue_mB59E9DA7BA75120FFC78B38C3932A6CAA5E9408F (void);
// 0x000001A3 System.Boolean Mediapipe.Timestamp::IsRangeValue()
extern void Timestamp_IsRangeValue_mA39BAEAA089FC7EA88A5A6E1F3D3A0D2971AA6F4 (void);
// 0x000001A4 System.Boolean Mediapipe.Timestamp::IsAllowedInStream()
extern void Timestamp_IsAllowedInStream_m28787D306FDFC76500F1CECBAFDD0A2527D5ECF9 (void);
// 0x000001A5 System.String Mediapipe.Timestamp::DebugString()
extern void Timestamp_DebugString_mB94B553296CD865FC8C4C15B8D281D0597D35B3B (void);
// 0x000001A6 Mediapipe.Timestamp Mediapipe.Timestamp::NextAllowedInStream()
extern void Timestamp_NextAllowedInStream_m1CC8BB280F0D28A05D4314FEAAFEE5DEC4CC81B2 (void);
// 0x000001A7 Mediapipe.Timestamp Mediapipe.Timestamp::PreviousAllowedInStream()
extern void Timestamp_PreviousAllowedInStream_m69CB5A37663015FDE11837E5F7019F943FEBB63B (void);
// 0x000001A8 Mediapipe.Timestamp Mediapipe.Timestamp::FromSeconds(System.Double)
extern void Timestamp_FromSeconds_m95F2743A6A8BEF54DF7A735D878F6B94673E7146 (void);
// 0x000001A9 Mediapipe.Timestamp Mediapipe.Timestamp::Unset()
extern void Timestamp_Unset_m73DB62A88E38F14EF156DA76FA6C3471C4699878 (void);
// 0x000001AA Mediapipe.Timestamp Mediapipe.Timestamp::Unstarted()
extern void Timestamp_Unstarted_mDDB33C7B62A030C0907F9BDA2BC7906FD5B80F34 (void);
// 0x000001AB Mediapipe.Timestamp Mediapipe.Timestamp::PreStream()
extern void Timestamp_PreStream_m15B60672CFE0A4243100214754A47C0B16DB8901 (void);
// 0x000001AC Mediapipe.Timestamp Mediapipe.Timestamp::Min()
extern void Timestamp_Min_m7E9A6D79B9FA7C9C969290FE78245318AC5D16EA (void);
// 0x000001AD Mediapipe.Timestamp Mediapipe.Timestamp::Max()
extern void Timestamp_Max_m070F224F1AF233AFE6FBDC66006A9643DD0554E4 (void);
// 0x000001AE Mediapipe.Timestamp Mediapipe.Timestamp::PostStream()
extern void Timestamp_PostStream_m0FD73F66D53B8E005C47FB371073535B644F3B53 (void);
// 0x000001AF Mediapipe.Timestamp Mediapipe.Timestamp::OneOverPostStream()
extern void Timestamp_OneOverPostStream_m7E87501F1887BEF9ECC5D7BFFB47F0708CF693D3 (void);
// 0x000001B0 Mediapipe.Timestamp Mediapipe.Timestamp::Done()
extern void Timestamp_Done_m22451488C91102C475D8C2D015E2EC148AD5A263 (void);
// 0x000001B1 System.String Mediapipe.Tool::GetUnusedNodeName(Mediapipe.CalculatorGraphConfig,System.String)
extern void Tool_GetUnusedNodeName_m0C955B325CBB0F5B42B07737B763A30997BFD14C (void);
// 0x000001B2 System.String Mediapipe.Tool::GetUnusedSidePacketName(Mediapipe.CalculatorGraphConfig,System.String)
extern void Tool_GetUnusedSidePacketName_mF68C95622DA9603667EA97F2C388C430E54AB73B (void);
// 0x000001B3 System.String Mediapipe.Tool::CanonicalNodeName(Mediapipe.CalculatorGraphConfig,System.Int32)
extern void Tool_CanonicalNodeName_mD3E38F7DAA65E9484FF8EB5CE66CD1F24C409FC9 (void);
// 0x000001B4 System.String Mediapipe.Tool::ParseNameFromStream(System.String)
extern void Tool_ParseNameFromStream_mC1D798DD23698FE600D0E04A45906A0F5D774D1D (void);
// 0x000001B5 System.ValueTuple`2<System.String,System.Int32> Mediapipe.Tool::ParseTagIndex(System.String)
extern void Tool_ParseTagIndex_m057EA602EE210C4A8B59B78835444EE7B70CC0FE (void);
// 0x000001B6 System.ValueTuple`2<System.String,System.Int32> Mediapipe.Tool::ParseTagIndexFromStream(System.String)
extern void Tool_ParseTagIndexFromStream_m1861ED05301E17B8823410C368E96FDE62C799CC (void);
// 0x000001B7 System.String Mediapipe.Tool::CatTag(System.String,System.Int32)
extern void Tool_CatTag_mCE0C8D85484E79F321B34BAFC74B6F33D07F6DF1 (void);
// 0x000001B8 System.String Mediapipe.Tool::CatStream(System.ValueTuple`2<System.String,System.Int32>,System.String)
extern void Tool_CatStream_m5ED50ED47178AABD3A742F702E71F7F072FBFEC0 (void);
// 0x000001B9 System.Void Mediapipe.Tool::ValidateName(System.String)
extern void Tool_ValidateName_mE9A665D09188303F3C615AB000E086A6461DA7A3 (void);
// 0x000001BA System.Void Mediapipe.Tool::ValidateNumber(System.String)
extern void Tool_ValidateNumber_m5C9A486C7A5FF2A390FAF79A7E664AE667028EEB (void);
// 0x000001BB System.Void Mediapipe.Tool::ValidateTag(System.String)
extern void Tool_ValidateTag_mB6C5A26CCEDBFF11F24EF88C2BB928560D273D56 (void);
// 0x000001BC System.Void Mediapipe.Tool::ParseTagAndName(System.String,System.String&,System.String&)
extern void Tool_ParseTagAndName_m94B1F5FDCB7815D93AF05CF1BD7B4140D26393D1 (void);
// 0x000001BD System.Void Mediapipe.Tool::ParseTagIndexName(System.String,System.String&,System.Int32&,System.String&)
extern void Tool_ParseTagIndexName_m24DA0B173A21A2445AFE1955C39A1B7DC2F82D27 (void);
// 0x000001BE System.Void Mediapipe.Tool::ParseTagIndex(System.String,System.String&,System.Int32&)
extern void Tool_ParseTagIndex_mBAA2C082D2408EAFA201F0FBD943892E0FF0BDDB (void);
// 0x000001BF System.Void Mediapipe.Tool::.cctor()
extern void Tool__cctor_m74F926ED19E84CAD3BF399CCB7C1062350978DE5 (void);
// 0x000001C0 System.Void Mediapipe.Tool/<>c::.cctor()
extern void U3CU3Ec__cctor_mF3525D366FAF1C69C7DB2BD020B1B2C24881EBF1 (void);
// 0x000001C1 System.Void Mediapipe.Tool/<>c::.ctor()
extern void U3CU3Ec__ctor_m8B39BB6CF13D89A43CA2C80F9099F71F40A683B3 (void);
// 0x000001C2 System.String Mediapipe.Tool/<>c::<GetUnusedNodeName>b__0_0(Mediapipe.CalculatorGraphConfig/Types/Node)
extern void U3CU3Ec_U3CGetUnusedNodeNameU3Eb__0_0_m1573C18DBAFEE5305A01C13A0ADFD7617D56ABA6 (void);
// 0x000001C3 System.Boolean Mediapipe.Tool/<>c::<GetUnusedNodeName>b__0_1(System.String)
extern void U3CU3Ec_U3CGetUnusedNodeNameU3Eb__0_1_m973A43ED4082D09D59069B0CD717E9AA10439EF7 (void);
// 0x000001C4 System.Collections.Generic.IEnumerable`1<System.String> Mediapipe.Tool/<>c::<GetUnusedSidePacketName>b__1_0(Mediapipe.CalculatorGraphConfig/Types/Node)
extern void U3CU3Ec_U3CGetUnusedSidePacketNameU3Eb__1_0_m032B4D03AD11DAFBA1F2305761CBBBFE2BBC52BD (void);
// 0x000001C5 System.String Mediapipe.Tool/<>c::<GetUnusedSidePacketName>b__1_1(System.String)
extern void U3CU3Ec_U3CGetUnusedSidePacketNameU3Eb__1_1_m31C41D012EC747D3B46B661601D6B0D37AA4051A (void);
// 0x000001C6 System.ValueTuple`2<System.String,System.Int32> Mediapipe.Tool/<>c::<CanonicalNodeName>b__2_0(Mediapipe.CalculatorGraphConfig/Types/Node,System.Int32)
extern void U3CU3Ec_U3CCanonicalNodeNameU3Eb__2_0_m09CC4407B697A7EDE5149AF583946FE8D0EB10B5 (void);
// 0x000001C7 System.Void Mediapipe.Tool/<>c__DisplayClass2_0::.ctor()
extern void U3CU3Ec__DisplayClass2_0__ctor_m23CF51FC77B997BE7F8EDBC2930024FE9E0865CF (void);
// 0x000001C8 System.Boolean Mediapipe.Tool/<>c__DisplayClass2_0::<CanonicalNodeName>b__1(System.ValueTuple`2<System.String,System.Int32>)
extern void U3CU3Ec__DisplayClass2_0_U3CCanonicalNodeNameU3Eb__1_mFF877148EC50CBF017DFA3AC2C7B076B8592640C (void);
// 0x000001C9 System.Boolean Mediapipe.Tool/<>c__DisplayClass2_0::<CanonicalNodeName>b__2(System.ValueTuple`2<System.String,System.Int32>)
extern void U3CU3Ec__DisplayClass2_0_U3CCanonicalNodeNameU3Eb__2_m3D71E7837D418B4AE8BED40F3FBF79EEB1090097 (void);
// 0x000001CA System.Void Mediapipe.Internal::.cctor()
extern void Internal__cctor_m2BD3907D1F71437953C5947E0E835AB04BADF8EC (void);
// 0x000001CB System.IntPtr Mediapipe.Egl::getCurrentContext()
extern void Egl_getCurrentContext_mB4CF66E727F4062DD38E8C856F6474C2934C3D6B (void);
// 0x000001CC System.Void Mediapipe.Egl::.ctor()
extern void Egl__ctor_m3BDF96C8F9476CFD7D587C692B4E7954F97FA070 (void);
// 0x000001CD System.Void Mediapipe.EglSurfaceHolder::.ctor(System.IntPtr,System.Boolean)
extern void EglSurfaceHolder__ctor_m7BE3959ACDF959BEA69F2267CED3236502E36447 (void);
// 0x000001CE System.Void Mediapipe.EglSurfaceHolder::.ctor()
extern void EglSurfaceHolder__ctor_mC65B9953DB0EADC85668F3D0B6C3018C6733900F (void);
// 0x000001CF System.Void Mediapipe.EglSurfaceHolder::DisposeManaged()
extern void EglSurfaceHolder_DisposeManaged_mEEB3A6C584E34F4F5825167BF0FD40984291984F (void);
// 0x000001D0 System.Void Mediapipe.EglSurfaceHolder::DeleteMpPtr()
extern void EglSurfaceHolder_DeleteMpPtr_mF106A2DDC3996BFAE265D62C20F3139209F4C341 (void);
// 0x000001D1 System.IntPtr Mediapipe.EglSurfaceHolder::get_uniquePtr()
extern void EglSurfaceHolder_get_uniquePtr_m23F917F234894D1E453FBA278CBDE7DDD8EFFC49 (void);
// 0x000001D2 System.Boolean Mediapipe.EglSurfaceHolder::FlipY()
extern void EglSurfaceHolder_FlipY_m74D0271352027D3A368987992330EA00255441C4 (void);
// 0x000001D3 System.Void Mediapipe.EglSurfaceHolder::SetFlipY(System.Boolean)
extern void EglSurfaceHolder_SetFlipY_m83B3559FF1CB915DCF329C75726FC71531B92ECB (void);
// 0x000001D4 System.Void Mediapipe.EglSurfaceHolder::SetSurface(System.IntPtr,Mediapipe.GlContext)
extern void EglSurfaceHolder_SetSurface_mCCA2BDCB0580B8DB530640DC190D29D8D61E0FA8 (void);
// 0x000001D5 System.Void Mediapipe.EglSurfaceHolder/UniquePtr::.ctor(System.IntPtr,System.Boolean)
extern void UniquePtr__ctor_m711D1B92C36E741A5DBF23A2AA8797A3E399D15A (void);
// 0x000001D6 System.Void Mediapipe.EglSurfaceHolder/UniquePtr::DeleteMpPtr()
extern void UniquePtr_DeleteMpPtr_mEE74CCF03702726FC9E702E9FC3D1E5AD9A93510 (void);
// 0x000001D7 System.IntPtr Mediapipe.EglSurfaceHolder/UniquePtr::Get()
extern void UniquePtr_Get_m9FE36CBFADFCD46B550EAB9A0BE4499FAB6AD63A (void);
// 0x000001D8 System.IntPtr Mediapipe.EglSurfaceHolder/UniquePtr::Release()
extern void UniquePtr_Release_m51B4FAA53C6A85E42FD520D460A786A828C4E937 (void);
// 0x000001D9 System.Void Mediapipe.Gl::Flush()
extern void Gl_Flush_mFFD91F9F1B554478D1FFCDDFCE26F081C1EA762F (void);
// 0x000001DA System.Void Mediapipe.Gl::ReadPixels(System.Int32,System.Int32,System.Int32,System.Int32,System.UInt32,System.UInt32,System.IntPtr)
extern void Gl_ReadPixels_m8A6F20357EFBF3D510DF8B17EE0D3A20185C39DD (void);
// 0x000001DB System.Void Mediapipe.Gl::.ctor()
extern void Gl__ctor_mD8F762B01CC4DA9A9609808494E569FEACB51C8D (void);
// 0x000001DC System.Void Mediapipe.Gl::.cctor()
extern void Gl__cctor_m849DB06EA463BBB4C25700A5D083CABB48992D1F (void);
// 0x000001DD System.Void Mediapipe.GlCalculatorHelper::.ctor()
extern void GlCalculatorHelper__ctor_mED9CE6D90FC19E484067DFC4FFBAF051FA4A7592 (void);
// 0x000001DE System.Void Mediapipe.GlCalculatorHelper::DeleteMpPtr()
extern void GlCalculatorHelper_DeleteMpPtr_m6E2BE53BB044DB3D3014B937AAF80905212B3321 (void);
// 0x000001DF System.Void Mediapipe.GlCalculatorHelper::InitializeForTest(Mediapipe.GpuResources)
extern void GlCalculatorHelper_InitializeForTest_m3D82F897DD6397CF04573B3D55086806220A186A (void);
// 0x000001E0 Mediapipe.Status Mediapipe.GlCalculatorHelper::RunInGlContext(Mediapipe.GlCalculatorHelper/NativeGlStatusFunction)
extern void GlCalculatorHelper_RunInGlContext_m854262F1C00A435A27E0F029A3DA335CC85D6377 (void);
// 0x000001E1 Mediapipe.Status Mediapipe.GlCalculatorHelper::RunInGlContext(Mediapipe.GlCalculatorHelper/GlStatusFunction)
extern void GlCalculatorHelper_RunInGlContext_m14CC71E506057D595A328999456DE5DC480BFEF9 (void);
// 0x000001E2 Mediapipe.GlTexture Mediapipe.GlCalculatorHelper::CreateSourceTexture(Mediapipe.ImageFrame)
extern void GlCalculatorHelper_CreateSourceTexture_m4B7E02C15E08649463AF29A0448FF6B4BC989078 (void);
// 0x000001E3 Mediapipe.GlTexture Mediapipe.GlCalculatorHelper::CreateSourceTexture(Mediapipe.GpuBuffer)
extern void GlCalculatorHelper_CreateSourceTexture_mA608BEEC8A3D3789D432DD612DBDC669E659B215 (void);
// 0x000001E4 Mediapipe.GlTexture Mediapipe.GlCalculatorHelper::CreateDestinationTexture(System.Int32,System.Int32,Mediapipe.GpuBufferFormat)
extern void GlCalculatorHelper_CreateDestinationTexture_m671F45F0B3205A64A7A80E597A9882736337F47A (void);
// 0x000001E5 Mediapipe.GlTexture Mediapipe.GlCalculatorHelper::CreateDestinationTexture(Mediapipe.GpuBuffer)
extern void GlCalculatorHelper_CreateDestinationTexture_m30E9A4995EA72C0D40EF92977855DDF5490DDFC0 (void);
// 0x000001E6 System.UInt32 Mediapipe.GlCalculatorHelper::get_framebuffer()
extern void GlCalculatorHelper_get_framebuffer_mCE417D5C2D12B89AD7E6531F45C6B26831DB1F08 (void);
// 0x000001E7 System.Void Mediapipe.GlCalculatorHelper::BindFramebuffer(Mediapipe.GlTexture)
extern void GlCalculatorHelper_BindFramebuffer_mFB23876074DCA60C97303E4DE2D45E3321772729 (void);
// 0x000001E8 Mediapipe.GlContext Mediapipe.GlCalculatorHelper::GetGlContext()
extern void GlCalculatorHelper_GetGlContext_m8BADA680F11266E543F52D1CCA4047E88090F634 (void);
// 0x000001E9 System.Boolean Mediapipe.GlCalculatorHelper::Initialized()
extern void GlCalculatorHelper_Initialized_mF4D1152677917FB45DE9E4CF255385C7D0EE04F2 (void);
// 0x000001EA System.Void Mediapipe.GlCalculatorHelper/NativeGlStatusFunction::.ctor(System.Object,System.IntPtr)
extern void NativeGlStatusFunction__ctor_m4E4DBC119EA964E1487480F33DF30E248B37ED0D (void);
// 0x000001EB System.IntPtr Mediapipe.GlCalculatorHelper/NativeGlStatusFunction::Invoke()
extern void NativeGlStatusFunction_Invoke_m4903685B192FB0C37BF8BA15A25725489CE9913F (void);
// 0x000001EC System.IAsyncResult Mediapipe.GlCalculatorHelper/NativeGlStatusFunction::BeginInvoke(System.AsyncCallback,System.Object)
extern void NativeGlStatusFunction_BeginInvoke_m916161DD9E8A043DFCBA94AE466D439B734F78C4 (void);
// 0x000001ED System.IntPtr Mediapipe.GlCalculatorHelper/NativeGlStatusFunction::EndInvoke(System.IAsyncResult)
extern void NativeGlStatusFunction_EndInvoke_m9F18E6DA0B6441B5E8457E5D42A314C2EECFE27A (void);
// 0x000001EE System.Void Mediapipe.GlCalculatorHelper/GlStatusFunction::.ctor(System.Object,System.IntPtr)
extern void GlStatusFunction__ctor_m197CC04C90CAE808E7AE290D518447591C3B7DB6 (void);
// 0x000001EF Mediapipe.Status Mediapipe.GlCalculatorHelper/GlStatusFunction::Invoke()
extern void GlStatusFunction_Invoke_m12BC53A91B6D252C89C941F220446CFF06E8C245 (void);
// 0x000001F0 System.IAsyncResult Mediapipe.GlCalculatorHelper/GlStatusFunction::BeginInvoke(System.AsyncCallback,System.Object)
extern void GlStatusFunction_BeginInvoke_m5D5DEC877E7D08432AD8446D197085585CFF7705 (void);
// 0x000001F1 Mediapipe.Status Mediapipe.GlCalculatorHelper/GlStatusFunction::EndInvoke(System.IAsyncResult)
extern void GlStatusFunction_EndInvoke_m38AEFE4208B350E20FDB98ECDBBF4A881320F33D (void);
// 0x000001F2 System.Void Mediapipe.GlCalculatorHelper/<>c__DisplayClass6_0::.ctor()
extern void U3CU3Ec__DisplayClass6_0__ctor_m7C001C7A63DA237AD4C4632470DBD4C776DC46D3 (void);
// 0x000001F3 System.IntPtr Mediapipe.GlCalculatorHelper/<>c__DisplayClass6_0::<RunInGlContext>b__0()
extern void U3CU3Ec__DisplayClass6_0_U3CRunInGlContextU3Eb__0_m1DFECA19C476C7A6BB493FA7CC03EA093EF7410A (void);
// 0x000001F4 Mediapipe.GlContext Mediapipe.GlContext::GetCurrent()
extern void GlContext_GetCurrent_mB9A6B5CDABBDA48C1836129B5AF00F5973F82772 (void);
// 0x000001F5 System.Void Mediapipe.GlContext::.ctor(System.IntPtr,System.Boolean)
extern void GlContext__ctor_m1C07CDE642639B52680E89B12FE36BCC9264F06E (void);
// 0x000001F6 System.Void Mediapipe.GlContext::DisposeManaged()
extern void GlContext_DisposeManaged_m6B35429B3CC257381398D44A37D63D2FDF82DCBC (void);
// 0x000001F7 System.Void Mediapipe.GlContext::DeleteMpPtr()
extern void GlContext_DeleteMpPtr_m40E799D4782C27A8E4C78407C4BEE0B393721BAF (void);
// 0x000001F8 System.IntPtr Mediapipe.GlContext::get_sharedPtr()
extern void GlContext_get_sharedPtr_mBE1A1440C22EA958C65B1304EC7349DA290D0D23 (void);
// 0x000001F9 System.IntPtr Mediapipe.GlContext::get_eglDisplay()
extern void GlContext_get_eglDisplay_m13BE65FAFE297827F6515C99EFB2F5EC3D175F17 (void);
// 0x000001FA System.IntPtr Mediapipe.GlContext::get_eglConfig()
extern void GlContext_get_eglConfig_mA6E26C9E54F1AD4EFB40BBF07C868F8ADD3143DC (void);
// 0x000001FB System.IntPtr Mediapipe.GlContext::get_eglContext()
extern void GlContext_get_eglContext_m8D9B78C2AB3E803AF4884E0C145E5084415D78B9 (void);
// 0x000001FC System.Boolean Mediapipe.GlContext::IsCurrent()
extern void GlContext_IsCurrent_m873729CAEF4CB78A38FD49BA0F88B235B83295A3 (void);
// 0x000001FD System.Int32 Mediapipe.GlContext::get_glMajorVersion()
extern void GlContext_get_glMajorVersion_mC9F14DE57BB0B56BA6385D653B6C5551B1FF5490 (void);
// 0x000001FE System.Int32 Mediapipe.GlContext::get_glMinorVersion()
extern void GlContext_get_glMinorVersion_m2ABD04E5FBE49FEE33603695580B503D32C3615B (void);
// 0x000001FF System.Int64 Mediapipe.GlContext::get_glFinishCount()
extern void GlContext_get_glFinishCount_m216DE26ADF237DDBDB4AFF32899E0B0F81949ACF (void);
// 0x00000200 System.Void Mediapipe.GlContext/SharedPtr::.ctor(System.IntPtr,System.Boolean)
extern void SharedPtr__ctor_m14C3EE05599C49269C8D16730873C3261D653CA2 (void);
// 0x00000201 System.Void Mediapipe.GlContext/SharedPtr::DeleteMpPtr()
extern void SharedPtr_DeleteMpPtr_m2ECD289E992B3D2E2A7382AD9DBD253CDCDF9AE1 (void);
// 0x00000202 System.IntPtr Mediapipe.GlContext/SharedPtr::Get()
extern void SharedPtr_Get_mC3E26F745D81CCC1728BCB1A7FE99AD85AF8CDC4 (void);
// 0x00000203 System.Void Mediapipe.GlContext/SharedPtr::Reset()
extern void SharedPtr_Reset_mA4990583161BE5BF3685740D631C9706AA21E1C1 (void);
// 0x00000204 System.Void Mediapipe.GlSyncPoint::.ctor(System.IntPtr)
extern void GlSyncPoint__ctor_m6456B57D6B4D4ED7021648601CA6BE899AFC904C (void);
// 0x00000205 System.Void Mediapipe.GlSyncPoint::DisposeManaged()
extern void GlSyncPoint_DisposeManaged_m1C6D43F20A6EACCCAADBDAE3356F6C0B41FDE63D (void);
// 0x00000206 System.Void Mediapipe.GlSyncPoint::DeleteMpPtr()
extern void GlSyncPoint_DeleteMpPtr_m30254F0135C6FCB8173C42B2548E4CF7CBD3FFAB (void);
// 0x00000207 System.IntPtr Mediapipe.GlSyncPoint::get_sharedPtr()
extern void GlSyncPoint_get_sharedPtr_mDB32F436A87D83EDB577F21F646321B467CE28BF (void);
// 0x00000208 System.Void Mediapipe.GlSyncPoint::Wait()
extern void GlSyncPoint_Wait_m3232AF4DDE782E1BE174B8C3AEFF50CDC02CD7DB (void);
// 0x00000209 System.Void Mediapipe.GlSyncPoint::WaitOnGpu()
extern void GlSyncPoint_WaitOnGpu_m063A587E38D216FE3EE27962942EC90A7B7FC9C0 (void);
// 0x0000020A System.Boolean Mediapipe.GlSyncPoint::IsReady()
extern void GlSyncPoint_IsReady_m6A324F61F14F7AE4E66477CD17F983B4EE9B4825 (void);
// 0x0000020B Mediapipe.GlContext Mediapipe.GlSyncPoint::GetContext()
extern void GlSyncPoint_GetContext_mF01089C943E1EFC34B59BE388BE77D376E4F78A7 (void);
// 0x0000020C System.Void Mediapipe.GlSyncPoint/SharedPtr::.ctor(System.IntPtr)
extern void SharedPtr__ctor_m5BD3C4AE88D2601F50DEB1C411E9465267CF6E37 (void);
// 0x0000020D System.Void Mediapipe.GlSyncPoint/SharedPtr::DeleteMpPtr()
extern void SharedPtr_DeleteMpPtr_m44ADA2336F2C5CF6303A5D83528EEBD8BB121D72 (void);
// 0x0000020E System.IntPtr Mediapipe.GlSyncPoint/SharedPtr::Get()
extern void SharedPtr_Get_m37B1B746F7047DED6894561A572BD8C6EB101412 (void);
// 0x0000020F System.Void Mediapipe.GlSyncPoint/SharedPtr::Reset()
extern void SharedPtr_Reset_mD72AB969653217D5A30D22E0BF88F4AD69500814 (void);
// 0x00000210 System.Void Mediapipe.GlTexture::.ctor()
extern void GlTexture__ctor_mAA5D2577B3C6E8794DF8D61ECBEFC56C00EAD57F (void);
// 0x00000211 System.Void Mediapipe.GlTexture::.ctor(System.UInt32,System.Int32,System.Int32)
extern void GlTexture__ctor_m7F5DCB7763F6FAE5FCBF5A6E1591AFE1B2B3FC63 (void);
// 0x00000212 System.Void Mediapipe.GlTexture::.ctor(System.IntPtr,System.Boolean)
extern void GlTexture__ctor_m2AEF92C3D88B651E1EF05501A3AD0AD69337C3BA (void);
// 0x00000213 System.Void Mediapipe.GlTexture::DeleteMpPtr()
extern void GlTexture_DeleteMpPtr_m41955E8A8D79B8DA9955E8965E53529F4B212A6C (void);
// 0x00000214 System.Int32 Mediapipe.GlTexture::get_width()
extern void GlTexture_get_width_mD683963793D7AFDBA0D21208045F50F17D3C9F2D (void);
// 0x00000215 System.Int32 Mediapipe.GlTexture::get_height()
extern void GlTexture_get_height_mBD9DA13F674A06BDE9A6B5B3D91F2A7C9F840CCB (void);
// 0x00000216 System.UInt32 Mediapipe.GlTexture::get_target()
extern void GlTexture_get_target_mE253484EB717A8FFA08A0E6C5D9478ED4F6D977F (void);
// 0x00000217 System.UInt32 Mediapipe.GlTexture::get_name()
extern void GlTexture_get_name_m3ECBC06AFCE90777A0C246C37D4DE832CED8BBF4 (void);
// 0x00000218 System.Void Mediapipe.GlTexture::Release()
extern void GlTexture_Release_m21906AC252786712676B7C1317F6B4356755D04C (void);
// 0x00000219 Mediapipe.GpuBuffer Mediapipe.GlTexture::GetGpuBufferFrame()
extern void GlTexture_GetGpuBufferFrame_mA23FBDF0EC29307AADD4238465576290D14596DE (void);
// 0x0000021A System.Void Mediapipe.GlTextureBuffer::.ctor(System.IntPtr,System.Boolean)
extern void GlTextureBuffer__ctor_mBBBA010E5D01643C0BC9E1DD19983E90AF5BA629 (void);
// 0x0000021B System.Void Mediapipe.GlTextureBuffer::.ctor(System.UInt32,System.UInt32,System.Int32,System.Int32,Mediapipe.GpuBufferFormat,Mediapipe.GlTextureBuffer/DeletionCallback,Mediapipe.GlContext)
extern void GlTextureBuffer__ctor_mA401218742C0C229E0369388ABE52BC83793E731 (void);
// 0x0000021C System.Void Mediapipe.GlTextureBuffer::.ctor(System.UInt32,System.Int32,System.Int32,Mediapipe.GpuBufferFormat,Mediapipe.GlTextureBuffer/DeletionCallback,Mediapipe.GlContext)
extern void GlTextureBuffer__ctor_mF3409802CBB5671728673B27E741FD72C0B7F1A3 (void);
// 0x0000021D System.Void Mediapipe.GlTextureBuffer::DisposeManaged()
extern void GlTextureBuffer_DisposeManaged_m6D138C3B48CFFAA645F8B5EDC935A8A8E79CF0AD (void);
// 0x0000021E System.Void Mediapipe.GlTextureBuffer::DeleteMpPtr()
extern void GlTextureBuffer_DeleteMpPtr_m21B7F529A2C68F78AE9653548425E1D8B10A38A3 (void);
// 0x0000021F System.IntPtr Mediapipe.GlTextureBuffer::get_sharedPtr()
extern void GlTextureBuffer_get_sharedPtr_m8A649180BB71728A92A8DC2D89097F1F7803D47E (void);
// 0x00000220 System.UInt32 Mediapipe.GlTextureBuffer::Name()
extern void GlTextureBuffer_Name_mEC35FAD74F98B6AA3DEBF2E743A294023FC4977E (void);
// 0x00000221 System.UInt32 Mediapipe.GlTextureBuffer::Target()
extern void GlTextureBuffer_Target_m472E0209AB400AED6EF4908DAC90822B7AAE2A75 (void);
// 0x00000222 System.Int32 Mediapipe.GlTextureBuffer::Width()
extern void GlTextureBuffer_Width_m96DBFCCD80CE8B28BEDFF56785980B0ED79404A8 (void);
// 0x00000223 System.Int32 Mediapipe.GlTextureBuffer::Height()
extern void GlTextureBuffer_Height_mD5ECD427E4F9A12CB109E7F8A34C092A42CF7A4F (void);
// 0x00000224 Mediapipe.GpuBufferFormat Mediapipe.GlTextureBuffer::Format()
extern void GlTextureBuffer_Format_m3743C4D5D3A1A2373EC70771EB71E0FCD6AC4261 (void);
// 0x00000225 System.Void Mediapipe.GlTextureBuffer::WaitUntilComplete()
extern void GlTextureBuffer_WaitUntilComplete_mCFE1A82B0E7402E29C15949658E77FA261D2700C (void);
// 0x00000226 System.Void Mediapipe.GlTextureBuffer::WaitOnGpu()
extern void GlTextureBuffer_WaitOnGpu_mB01F327D8C651A198325AD2E3C9E6D88B995AD6A (void);
// 0x00000227 System.Void Mediapipe.GlTextureBuffer::Reuse()
extern void GlTextureBuffer_Reuse_mBFB7E77D05EA45583F0A993F4F1C69A97C8FC59D (void);
// 0x00000228 System.Void Mediapipe.GlTextureBuffer::Updated(Mediapipe.GlSyncPoint)
extern void GlTextureBuffer_Updated_m35E5A32106B24F0369F29971FCDB1E5BDA2318AB (void);
// 0x00000229 System.Void Mediapipe.GlTextureBuffer::DidRead(Mediapipe.GlSyncPoint)
extern void GlTextureBuffer_DidRead_mE802E78D5C5D53D1595A1EE8142204074F8C9BB9 (void);
// 0x0000022A System.Void Mediapipe.GlTextureBuffer::WaitForConsumers()
extern void GlTextureBuffer_WaitForConsumers_mAF1873E2A6E07C943119CA7327C661DBB5BA4106 (void);
// 0x0000022B System.Void Mediapipe.GlTextureBuffer::WaitForConsumersOnGpu()
extern void GlTextureBuffer_WaitForConsumersOnGpu_m168E33BD1D767B334D311F9CB530605041DA4528 (void);
// 0x0000022C Mediapipe.GlContext Mediapipe.GlTextureBuffer::GetProducerContext()
extern void GlTextureBuffer_GetProducerContext_m82BACB68186137B343003A742E74378AA8B1B11B (void);
// 0x0000022D System.Void Mediapipe.GlTextureBuffer/DeletionCallback::.ctor(System.Object,System.IntPtr)
extern void DeletionCallback__ctor_mEE936A181548630722C48E8C936D92774AD5B704 (void);
// 0x0000022E System.Void Mediapipe.GlTextureBuffer/DeletionCallback::Invoke(System.UInt64,System.IntPtr)
extern void DeletionCallback_Invoke_m8A004DD58A6F0CFEEF0C779C0D48CB2AF676CE1D (void);
// 0x0000022F System.IAsyncResult Mediapipe.GlTextureBuffer/DeletionCallback::BeginInvoke(System.UInt64,System.IntPtr,System.AsyncCallback,System.Object)
extern void DeletionCallback_BeginInvoke_m71EA016BFDCA213EBDB28D4D4583867D4FB4CC89 (void);
// 0x00000230 System.Void Mediapipe.GlTextureBuffer/DeletionCallback::EndInvoke(System.IAsyncResult)
extern void DeletionCallback_EndInvoke_mE7A9FCABB86464713FDBCFE233BE225351E9D257 (void);
// 0x00000231 System.Void Mediapipe.GlTextureBuffer/SharedPtr::.ctor(System.IntPtr,System.Boolean)
extern void SharedPtr__ctor_m98F6E0D93E8BAAD61638CC2119F3A5D60453FF0C (void);
// 0x00000232 System.Void Mediapipe.GlTextureBuffer/SharedPtr::DeleteMpPtr()
extern void SharedPtr_DeleteMpPtr_m15B99543BE899251E82EB06C391FE369A9A098CB (void);
// 0x00000233 System.IntPtr Mediapipe.GlTextureBuffer/SharedPtr::Get()
extern void SharedPtr_Get_m5F37E1C54DB721F377C4598E5A4D108775DBB1D0 (void);
// 0x00000234 System.Void Mediapipe.GlTextureBuffer/SharedPtr::Reset()
extern void SharedPtr_Reset_m221F2D07E60E20B9762F4A6C1D3414BC20B3818D (void);
// 0x00000235 System.Void Mediapipe.GpuBuffer::.ctor(System.IntPtr,System.Boolean)
extern void GpuBuffer__ctor_mEDFB9403435ECE8E7CB8F155F2C98BE3E96F0533 (void);
// 0x00000236 System.Void Mediapipe.GpuBuffer::.ctor(Mediapipe.GlTextureBuffer)
extern void GpuBuffer__ctor_m2141005B1A7037462F5444ABC626B1A8101561D9 (void);
// 0x00000237 System.Void Mediapipe.GpuBuffer::DeleteMpPtr()
extern void GpuBuffer_DeleteMpPtr_mF427246C893B1357C2EB0BBF92F1B587F47D0C98 (void);
// 0x00000238 Mediapipe.GlTextureBuffer Mediapipe.GpuBuffer::GetGlTextureBuffer()
extern void GpuBuffer_GetGlTextureBuffer_mB806BF061030AE2938A473769CF49B9BCDABB219 (void);
// 0x00000239 Mediapipe.GpuBufferFormat Mediapipe.GpuBuffer::Format()
extern void GpuBuffer_Format_mF0146FDAD099AC813D712C3C74FEE82480FEB05B (void);
// 0x0000023A System.Int32 Mediapipe.GpuBuffer::Width()
extern void GpuBuffer_Width_m3662757DE67BF205D717BA97F5863C9B02FCF204 (void);
// 0x0000023B System.Int32 Mediapipe.GpuBuffer::Height()
extern void GpuBuffer_Height_mF70307C036418D29D6F7402A067F271AE56C886A (void);
// 0x0000023C Mediapipe.ImageFormat/Format Mediapipe.GpuBufferFormatExtension::ImageFormatFor(Mediapipe.GpuBufferFormat)
extern void GpuBufferFormatExtension_ImageFormatFor_m0DC71BAD40318374571505A1C1A3DA9009A74CE1 (void);
// 0x0000023D Mediapipe.GlTextureInfo Mediapipe.GpuBufferFormatExtension::GlTextureInfoFor(Mediapipe.GpuBufferFormat,System.Int32,Mediapipe.GlVersion)
extern void GpuBufferFormatExtension_GlTextureInfoFor_mF1C0CB8FEABA534B7FECD9FF2A531F6B027F2FFF (void);
// 0x0000023E System.Void Mediapipe.GpuResources::.ctor(System.IntPtr)
extern void GpuResources__ctor_m741ACB7606DC6CA9CC05041EEF5C8AF3B1803A35 (void);
// 0x0000023F System.Void Mediapipe.GpuResources::DisposeManaged()
extern void GpuResources_DisposeManaged_m4C220E1A1701B1CD894D4149702ECBFC1EDB9EEF (void);
// 0x00000240 System.Void Mediapipe.GpuResources::DeleteMpPtr()
extern void GpuResources_DeleteMpPtr_m670CBED6D7322E5348FA1A5A162668CF966BFE86 (void);
// 0x00000241 System.IntPtr Mediapipe.GpuResources::get_sharedPtr()
extern void GpuResources_get_sharedPtr_m11E75840CD47CCEC222918BA72FCEC8FE911610E (void);
// 0x00000242 Mediapipe.StatusOrGpuResources Mediapipe.GpuResources::Create()
extern void GpuResources_Create_mA6F2EEBA031CF7A093B228AD50C928269CA17B4A (void);
// 0x00000243 Mediapipe.StatusOrGpuResources Mediapipe.GpuResources::Create(System.IntPtr)
extern void GpuResources_Create_m6D68A86FD785FC29E089FF15F6015EBD3E6FA805 (void);
// 0x00000244 System.Void Mediapipe.GpuResources/SharedPtr::.ctor(System.IntPtr)
extern void SharedPtr__ctor_m0A2FC31F39BC8B901D3EA97233C382620227B590 (void);
// 0x00000245 System.Void Mediapipe.GpuResources/SharedPtr::DeleteMpPtr()
extern void SharedPtr_DeleteMpPtr_mE4D3FEF627D2FF2F766A4B7F7EE0057C6AA4E7AD (void);
// 0x00000246 System.IntPtr Mediapipe.GpuResources/SharedPtr::Get()
extern void SharedPtr_Get_mA20A8481C67F4A1E9D7E40E3E346A2EEED900639 (void);
// 0x00000247 System.Void Mediapipe.GpuResources/SharedPtr::Reset()
extern void SharedPtr_Reset_m3E67A24946A739AC0F99DD184CA993D253E8793B (void);
// 0x00000248 System.Void Mediapipe.MpReturnCodeExtension::Assert(Mediapipe.MpReturnCode)
extern void MpReturnCodeExtension_Assert_mD720CAA4396143E0113F15BBE369066AB04044EC (void);
// 0x00000249 System.Boolean Mediapipe.SafeNativeMethods::absl_Status__ok(System.IntPtr)
extern void SafeNativeMethods_absl_Status__ok_mEC744B480B0D1DBFE29DEEB2F6B3E2505AB267D2 (void);
// 0x0000024A System.Int32 Mediapipe.SafeNativeMethods::absl_Status__raw_code(System.IntPtr)
extern void SafeNativeMethods_absl_Status__raw_code_m2C0BE3BD9F5F3CC78A27750FF59AE0984ECB0ADF (void);
// 0x0000024B System.Boolean Mediapipe.SafeNativeMethods::mp_CalculatorGraph__HasError(System.IntPtr)
extern void SafeNativeMethods_mp_CalculatorGraph__HasError_mAEF2E518E58094A550DC430EC3DF6CF766E4747E (void);
// 0x0000024C System.Boolean Mediapipe.SafeNativeMethods::mp_CalculatorGraph__HasInputStream__PKc(System.IntPtr,System.String)
extern void SafeNativeMethods_mp_CalculatorGraph__HasInputStream__PKc_m96D2D15C893403E4B1C0E39BFAB0EC612BE4401E (void);
// 0x0000024D System.Boolean Mediapipe.SafeNativeMethods::mp_CalculatorGraph__GraphInputStreamsClosed(System.IntPtr)
extern void SafeNativeMethods_mp_CalculatorGraph__GraphInputStreamsClosed_m95C8D9340C9890B856AF401D82BF7A036E6C051B (void);
// 0x0000024E System.Boolean Mediapipe.SafeNativeMethods::mp_CalculatorGraph__IsNodeThrottled__i(System.IntPtr,System.Int32)
extern void SafeNativeMethods_mp_CalculatorGraph__IsNodeThrottled__i_m5F78ED89AF3A67340AFD7B2E6455E1E762B5624B (void);
// 0x0000024F System.Boolean Mediapipe.SafeNativeMethods::mp_CalculatorGraph__UnthrottleSources(System.IntPtr)
extern void SafeNativeMethods_mp_CalculatorGraph__UnthrottleSources_m85801679DD5D2FCA1D2E08343B21159B7139DCE4 (void);
// 0x00000250 System.Boolean Mediapipe.SafeNativeMethods::mp_ImageFrame__IsEmpty(System.IntPtr)
extern void SafeNativeMethods_mp_ImageFrame__IsEmpty_m3B0B75877931357EC87CBFAAD90B872D961AAE95 (void);
// 0x00000251 System.Boolean Mediapipe.SafeNativeMethods::mp_ImageFrame__IsContiguous(System.IntPtr)
extern void SafeNativeMethods_mp_ImageFrame__IsContiguous_mA9B0642B4CAB465D52FE42516282F230740FADE6 (void);
// 0x00000252 Mediapipe.MpReturnCode Mediapipe.SafeNativeMethods::mp_ImageFrame__IsAligned__ui(System.IntPtr,System.UInt32,System.Boolean&)
extern void SafeNativeMethods_mp_ImageFrame__IsAligned__ui_m43CD5220D418D6B7BE55768C3110E97C166F14B0 (void);
// 0x00000253 Mediapipe.ImageFormat/Format Mediapipe.SafeNativeMethods::mp_ImageFrame__Format(System.IntPtr)
extern void SafeNativeMethods_mp_ImageFrame__Format_m20A45FAAEDDA6825FF189F1EABBC5B28BDFE8C3D (void);
// 0x00000254 System.Int32 Mediapipe.SafeNativeMethods::mp_ImageFrame__Width(System.IntPtr)
extern void SafeNativeMethods_mp_ImageFrame__Width_m4CAB7749017189207C979B206F8658296D2B2836 (void);
// 0x00000255 System.Int32 Mediapipe.SafeNativeMethods::mp_ImageFrame__Height(System.IntPtr)
extern void SafeNativeMethods_mp_ImageFrame__Height_mEA6B4E9A8FC6737B62F44C45D58090085FA9CAFF (void);
// 0x00000256 Mediapipe.MpReturnCode Mediapipe.SafeNativeMethods::mp_ImageFrame__ChannelSize(System.IntPtr,System.Int32&)
extern void SafeNativeMethods_mp_ImageFrame__ChannelSize_m30EE73317EF93BD9090A7EFC9A2B1FAECD4AE9A8 (void);
// 0x00000257 Mediapipe.MpReturnCode Mediapipe.SafeNativeMethods::mp_ImageFrame__NumberOfChannels(System.IntPtr,System.Int32&)
extern void SafeNativeMethods_mp_ImageFrame__NumberOfChannels_mEFF318A9468A0E2DA8C68017E7D0E215EBD78308 (void);
// 0x00000258 Mediapipe.MpReturnCode Mediapipe.SafeNativeMethods::mp_ImageFrame__ByteDepth(System.IntPtr,System.Int32&)
extern void SafeNativeMethods_mp_ImageFrame__ByteDepth_m892DDB59B17B0BD2927C32801E0E69C341C0DEE9 (void);
// 0x00000259 System.Int32 Mediapipe.SafeNativeMethods::mp_ImageFrame__WidthStep(System.IntPtr)
extern void SafeNativeMethods_mp_ImageFrame__WidthStep_m871665FBF157028D03B465834FE21593D5821A94 (void);
// 0x0000025A System.IntPtr Mediapipe.SafeNativeMethods::mp_ImageFrame__MutablePixelData(System.IntPtr)
extern void SafeNativeMethods_mp_ImageFrame__MutablePixelData_mAFAB8BB5635D6966958D11B8959F5B8BA44926A9 (void);
// 0x0000025B System.Int32 Mediapipe.SafeNativeMethods::mp_ImageFrame__PixelDataSize(System.IntPtr)
extern void SafeNativeMethods_mp_ImageFrame__PixelDataSize_mFD9AF92BD01CA7EB665BD7BF85B6E7365C143522 (void);
// 0x0000025C Mediapipe.MpReturnCode Mediapipe.SafeNativeMethods::mp_ImageFrame__PixelDataSizeStoredContiguously(System.IntPtr,System.Int32&)
extern void SafeNativeMethods_mp_ImageFrame__PixelDataSizeStoredContiguously_m863D9349265A0D3ED2BBE719B24F68DE905ABE76 (void);
// 0x0000025D System.Boolean Mediapipe.SafeNativeMethods::mp_StatusOrImageFrame__ok(System.IntPtr)
extern void SafeNativeMethods_mp_StatusOrImageFrame__ok_m35BBD386B1234436B420858C9D3D5D6B327B6148 (void);
// 0x0000025E System.Boolean Mediapipe.SafeNativeMethods::mp_StatusOrPoller__ok(System.IntPtr)
extern void SafeNativeMethods_mp_StatusOrPoller__ok_m83A6EBC921C0A01BD97B741C036D07F85617F22E (void);
// 0x0000025F System.Void Mediapipe.SafeNativeMethods::mp_SidePacket__clear(System.IntPtr)
extern void SafeNativeMethods_mp_SidePacket__clear_m27EDF042F43DBEF61B67D83530C54985CA3B64B7 (void);
// 0x00000260 System.Int32 Mediapipe.SafeNativeMethods::mp_SidePacket__size(System.IntPtr)
extern void SafeNativeMethods_mp_SidePacket__size_m586B60EBBD006C99029D9AF619D576A06D08D429 (void);
// 0x00000261 System.Int64 Mediapipe.SafeNativeMethods::mp_Timestamp__Value(System.IntPtr)
extern void SafeNativeMethods_mp_Timestamp__Value_m5941A9E3BFEE21D0CADF5B08442F665953571F86 (void);
// 0x00000262 System.Double Mediapipe.SafeNativeMethods::mp_Timestamp__Seconds(System.IntPtr)
extern void SafeNativeMethods_mp_Timestamp__Seconds_mB0EDE5B4BDAD18EC5F0CE34ACD5912A257A32E2A (void);
// 0x00000263 System.Int64 Mediapipe.SafeNativeMethods::mp_Timestamp__Microseconds(System.IntPtr)
extern void SafeNativeMethods_mp_Timestamp__Microseconds_mAD701EF0D7E21E16B797ABF5121772B012AE389B (void);
// 0x00000264 System.Boolean Mediapipe.SafeNativeMethods::mp_Timestamp__IsSpecialValue(System.IntPtr)
extern void SafeNativeMethods_mp_Timestamp__IsSpecialValue_m48F93E35FE1EF4CDCF4BF2D27232BF3492470546 (void);
// 0x00000265 System.Boolean Mediapipe.SafeNativeMethods::mp_Timestamp__IsRangeValue(System.IntPtr)
extern void SafeNativeMethods_mp_Timestamp__IsRangeValue_mDBB73A6533348F04CA3CC012BBBB75D512EBF413 (void);
// 0x00000266 System.Boolean Mediapipe.SafeNativeMethods::mp_Timestamp__IsAllowedInStream(System.IntPtr)
extern void SafeNativeMethods_mp_Timestamp__IsAllowedInStream_m4608AF1406219709EAFEDCEFFC32422E45FF4762 (void);
// 0x00000267 System.IntPtr Mediapipe.SafeNativeMethods::mp_EglSurfaceHolderUniquePtr__get(System.IntPtr)
extern void SafeNativeMethods_mp_EglSurfaceHolderUniquePtr__get_m4CDE3F55FDD11B6CD970031672CF95833F0457C4 (void);
// 0x00000268 System.IntPtr Mediapipe.SafeNativeMethods::mp_EglSurfaceHolderUniquePtr__release(System.IntPtr)
extern void SafeNativeMethods_mp_EglSurfaceHolderUniquePtr__release_m8FD831BE5E967AE82ABD6053E513020830AEBFE7 (void);
// 0x00000269 System.Void Mediapipe.SafeNativeMethods::mp_EglSurfaceHolder__SetFlipY__b(System.IntPtr,System.Boolean)
extern void SafeNativeMethods_mp_EglSurfaceHolder__SetFlipY__b_m956C018704280E5DB6E6CE238A534A255E75726B (void);
// 0x0000026A System.Boolean Mediapipe.SafeNativeMethods::mp_EglSurfaceHolder__flip_y(System.IntPtr)
extern void SafeNativeMethods_mp_EglSurfaceHolder__flip_y_mA5D085F3C2DA3AAAA274B503F7214FDC1295BF97 (void);
// 0x0000026B System.UInt32 Mediapipe.SafeNativeMethods::mp_GlCalculatorHelper__framebuffer(System.IntPtr)
extern void SafeNativeMethods_mp_GlCalculatorHelper__framebuffer_mD0F283F9F9E6CDF8BE22307C5BEEBA49A9459836 (void);
// 0x0000026C System.IntPtr Mediapipe.SafeNativeMethods::mp_GlCalculatorHelper__GetGlContext(System.IntPtr)
extern void SafeNativeMethods_mp_GlCalculatorHelper__GetGlContext_mB6E5385FB4F38E276A960C2FDD34D587DA1A564E (void);
// 0x0000026D System.Boolean Mediapipe.SafeNativeMethods::mp_GlCalculatorHelper__Initialized(System.IntPtr)
extern void SafeNativeMethods_mp_GlCalculatorHelper__Initialized_mDBF658A570ED0B9AE41F8B45F0D20AADF03C8972 (void);
// 0x0000026E System.IntPtr Mediapipe.SafeNativeMethods::mp_SharedGlContext__get(System.IntPtr)
extern void SafeNativeMethods_mp_SharedGlContext__get_m77A246A1DD76F2827D8B943E7CD34A126175E78D (void);
// 0x0000026F System.IntPtr Mediapipe.SafeNativeMethods::mp_GlContext__egl_display(System.IntPtr)
extern void SafeNativeMethods_mp_GlContext__egl_display_mDFD0D5C5B325AEBFA773B9E5C3ABF5AA820B87F3 (void);
// 0x00000270 System.IntPtr Mediapipe.SafeNativeMethods::mp_GlContext__egl_config(System.IntPtr)
extern void SafeNativeMethods_mp_GlContext__egl_config_mB1C4067B8E251256C9496B33C5A7F6A769C6655F (void);
// 0x00000271 System.IntPtr Mediapipe.SafeNativeMethods::mp_GlContext__egl_context(System.IntPtr)
extern void SafeNativeMethods_mp_GlContext__egl_context_mCF562A477272774F36F248EC01A2F6064BED010D (void);
// 0x00000272 System.Boolean Mediapipe.SafeNativeMethods::mp_GlContext__IsCurrent(System.IntPtr)
extern void SafeNativeMethods_mp_GlContext__IsCurrent_m0FC507CDDA5C4664FE126E841D8F0722B0C53140 (void);
// 0x00000273 System.Int32 Mediapipe.SafeNativeMethods::mp_GlContext__gl_major_version(System.IntPtr)
extern void SafeNativeMethods_mp_GlContext__gl_major_version_m58A4039D6307CA75B143E2870B38F5B5FBC7F75E (void);
// 0x00000274 System.Int32 Mediapipe.SafeNativeMethods::mp_GlContext__gl_minor_version(System.IntPtr)
extern void SafeNativeMethods_mp_GlContext__gl_minor_version_mA60ED176CF817E57BF86A0159B08CBF2C2A53A7F (void);
// 0x00000275 System.Int64 Mediapipe.SafeNativeMethods::mp_GlContext__gl_finish_count(System.IntPtr)
extern void SafeNativeMethods_mp_GlContext__gl_finish_count_mB59F2811EEEBA1D9081BE3B121B1498755FC4345 (void);
// 0x00000276 System.IntPtr Mediapipe.SafeNativeMethods::mp_GlSyncToken__get(System.IntPtr)
extern void SafeNativeMethods_mp_GlSyncToken__get_mD56895E3208FF0DA701F53B1466B46F8692FA7BD (void);
// 0x00000277 System.UInt32 Mediapipe.SafeNativeMethods::mp_GlTextureBuffer__name(System.IntPtr)
extern void SafeNativeMethods_mp_GlTextureBuffer__name_m479C783C3F41F317595CC2E7B2DB7A3AF9FA8E1D (void);
// 0x00000278 System.UInt32 Mediapipe.SafeNativeMethods::mp_GlTextureBuffer__target(System.IntPtr)
extern void SafeNativeMethods_mp_GlTextureBuffer__target_m8C6D07CAF145D1A5B247CA83A2FE0DCAA5B5D20F (void);
// 0x00000279 System.Int32 Mediapipe.SafeNativeMethods::mp_GlTextureBuffer__width(System.IntPtr)
extern void SafeNativeMethods_mp_GlTextureBuffer__width_m6091187038D09CF89A8A2AF1131D2976521273AD (void);
// 0x0000027A System.Int32 Mediapipe.SafeNativeMethods::mp_GlTextureBuffer__height(System.IntPtr)
extern void SafeNativeMethods_mp_GlTextureBuffer__height_mD60419D574FF25DCC3129D5FDE07E723528057D8 (void);
// 0x0000027B Mediapipe.GpuBufferFormat Mediapipe.SafeNativeMethods::mp_GlTextureBuffer__format(System.IntPtr)
extern void SafeNativeMethods_mp_GlTextureBuffer__format_mFDEBCD3289458EAA2A2FDE199F91881080664091 (void);
// 0x0000027C System.IntPtr Mediapipe.SafeNativeMethods::mp_GlTextureBuffer__GetProducerContext(System.IntPtr)
extern void SafeNativeMethods_mp_GlTextureBuffer__GetProducerContext_m9DD4394D5FE1BAB9A3F68C647BD1D447B650D567 (void);
// 0x0000027D System.IntPtr Mediapipe.SafeNativeMethods::mp_SharedGlTextureBuffer__get(System.IntPtr)
extern void SafeNativeMethods_mp_SharedGlTextureBuffer__get_m685ABA5BC586433E9EE5A45A4493E792B016091A (void);
// 0x0000027E System.Int32 Mediapipe.SafeNativeMethods::mp_GlTexture__width(System.IntPtr)
extern void SafeNativeMethods_mp_GlTexture__width_mBA5184A37E978EF66138FD29C6650414AE70C4ED (void);
// 0x0000027F System.Int32 Mediapipe.SafeNativeMethods::mp_GlTexture__height(System.IntPtr)
extern void SafeNativeMethods_mp_GlTexture__height_m53E669E4DEF2FDFCA16B8BF1FACB7D8A11C82498 (void);
// 0x00000280 System.UInt32 Mediapipe.SafeNativeMethods::mp_GlTexture__target(System.IntPtr)
extern void SafeNativeMethods_mp_GlTexture__target_m4F2BBDF563DAAD3D406BB3EAB8BCDEC80F2689AD (void);
// 0x00000281 System.UInt32 Mediapipe.SafeNativeMethods::mp_GlTexture__name(System.IntPtr)
extern void SafeNativeMethods_mp_GlTexture__name_m474E34F1F07E7912813EB4C81ED3592A9B161798 (void);
// 0x00000282 System.IntPtr Mediapipe.SafeNativeMethods::eglGetCurrentContext()
extern void SafeNativeMethods_eglGetCurrentContext_mD72A32A6D2BDDA00FB1349C41203B8555F0C9D96 (void);
// 0x00000283 Mediapipe.ImageFormat/Format Mediapipe.SafeNativeMethods::mp__ImageFormatForGpuBufferFormat__ui(Mediapipe.GpuBufferFormat)
extern void SafeNativeMethods_mp__ImageFormatForGpuBufferFormat__ui_mCC9CC4A423B2816E2CE1D6B73E64093C1EEEFC84 (void);
// 0x00000284 Mediapipe.ImageFormat/Format Mediapipe.SafeNativeMethods::mp__GpuBufferFormatForImageFormat__ui(Mediapipe.ImageFormat/Format)
extern void SafeNativeMethods_mp__GpuBufferFormatForImageFormat__ui_m75341A8F99649E1037C95A7A8330159F7B2548E8 (void);
// 0x00000285 System.IntPtr Mediapipe.SafeNativeMethods::mp_GpuBuffer__GetGlTextureBufferSharedPtr(System.IntPtr)
extern void SafeNativeMethods_mp_GpuBuffer__GetGlTextureBufferSharedPtr_m98B37FA49D421C1A905EDD5A99C782375B1024F9 (void);
// 0x00000286 System.Int32 Mediapipe.SafeNativeMethods::mp_GpuBuffer__width(System.IntPtr)
extern void SafeNativeMethods_mp_GpuBuffer__width_m35DD1BFB04D5A3630332B5D88A386B2DBD43A590 (void);
// 0x00000287 System.Int32 Mediapipe.SafeNativeMethods::mp_GpuBuffer__height(System.IntPtr)
extern void SafeNativeMethods_mp_GpuBuffer__height_m022ABAE0EC201C187B9F7B6C1579A3268718EAC5 (void);
// 0x00000288 Mediapipe.GpuBufferFormat Mediapipe.SafeNativeMethods::mp_GpuBuffer__format(System.IntPtr)
extern void SafeNativeMethods_mp_GpuBuffer__format_mB81DF79DCDEECE226598CB31499B471DAAD9F1D3 (void);
// 0x00000289 System.Boolean Mediapipe.SafeNativeMethods::mp_StatusOrGpuBuffer__ok(System.IntPtr)
extern void SafeNativeMethods_mp_StatusOrGpuBuffer__ok_m9B5B4651065A8B7C4BFC89B0A2B8DFC56FA29DD3 (void);
// 0x0000028A System.IntPtr Mediapipe.SafeNativeMethods::mp_SharedGpuResources__get(System.IntPtr)
extern void SafeNativeMethods_mp_SharedGpuResources__get_m57FBD88F3363794B30704C7872639B40D0A31F1C (void);
// 0x0000028B System.Boolean Mediapipe.SafeNativeMethods::mp_StatusOrGpuResources__ok(System.IntPtr)
extern void SafeNativeMethods_mp_StatusOrGpuResources__ok_mCBB427A2A37D0C223EF03087F10344B4DE79A43B (void);
// 0x0000028C System.Void Mediapipe.SafeNativeMethods::mp__SetCustomGlobalResourceProvider__P(Mediapipe.ResourceManager/ResourceProvider)
extern void SafeNativeMethods_mp__SetCustomGlobalResourceProvider__P_mB87C4A8C9CF2BE4E84286CB3259095A473F6BE8F (void);
// 0x0000028D System.Void Mediapipe.SafeNativeMethods::mp__SetCustomGlobalPathResolver__P(Mediapipe.ResourceManager/PathResolver)
extern void SafeNativeMethods_mp__SetCustomGlobalPathResolver__P_mE4C2A9785019DF67D40E2D5F0F984EAE0AD77DDC (void);
// 0x0000028E Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::absl_Status__i_PKc(System.Int32,System.String,System.IntPtr&)
extern void UnsafeNativeMethods_absl_Status__i_PKc_mB82E4A27B452A24BC4372A917C898EBF5D6D99C0 (void);
// 0x0000028F System.Void Mediapipe.UnsafeNativeMethods::absl_Status__delete(System.IntPtr)
extern void UnsafeNativeMethods_absl_Status__delete_m2714B82E6E5767C3ABE751D42445E7D46675327B (void);
// 0x00000290 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::absl_Status__ToString(System.IntPtr,System.IntPtr&)
extern void UnsafeNativeMethods_absl_Status__ToString_m8951B57C56E90D52EC1A777539F4A2125CEF3D1E (void);
// 0x00000291 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::google_InitGoogleLogging__PKc(System.String,System.String)
extern void UnsafeNativeMethods_google_InitGoogleLogging__PKc_m7247C0BF7BCCA6C8768CE87F5EB4F0DC2177CCC8 (void);
// 0x00000292 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::google_ShutdownGoogleLogging()
extern void UnsafeNativeMethods_google_ShutdownGoogleLogging_m713B45D64FA6740EA54746169BB39B940EC42DE9 (void);
// 0x00000293 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::glog_LOG_INFO__PKc(System.String)
extern void UnsafeNativeMethods_glog_LOG_INFO__PKc_m80B39329DB19DC3D4FF4AA04D76F3E4440BE3D0A (void);
// 0x00000294 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::glog_LOG_WARNING__PKc(System.String)
extern void UnsafeNativeMethods_glog_LOG_WARNING__PKc_m9575584BC9A435A4ECA4ED13F73E3BE35027A187 (void);
// 0x00000295 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::glog_LOG_ERROR__PKc(System.String)
extern void UnsafeNativeMethods_glog_LOG_ERROR__PKc_m2B1BCDD27FDE5BED60F513FC9B34E2A1A9ABC93E (void);
// 0x00000296 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::glog_LOG_FATAL__PKc(System.String)
extern void UnsafeNativeMethods_glog_LOG_FATAL__PKc_m16E265733740AC679C4A4BE789014FD2368061D5 (void);
// 0x00000297 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::google_FlushLogFiles(Mediapipe.Glog/Severity)
extern void UnsafeNativeMethods_google_FlushLogFiles_m00DB44E58A14845083702AFACC409F087B05A6C9 (void);
// 0x00000298 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::google_protobuf__SetLogHandler__PF(Mediapipe.Protobuf/ProtobufLogHandler)
extern void UnsafeNativeMethods_google_protobuf__SetLogHandler__PF_m96F435E92258861055A5641775CF6265129FA9D9 (void);
// 0x00000299 System.Void Mediapipe.UnsafeNativeMethods::mp_api_SerializedProto__delete(System.IntPtr)
extern void UnsafeNativeMethods_mp_api_SerializedProto__delete_mC22C4B39E8D7514D85038932FE42184A8A44F18A (void);
// 0x0000029A System.Void Mediapipe.UnsafeNativeMethods::mp_api_SerializedProtoVector__delete(System.IntPtr)
extern void UnsafeNativeMethods_mp_api_SerializedProtoVector__delete_mB1009B2243E779FB9DA5DE805DBFEC575E20AFEA (void);
// 0x0000029B Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_Packet__GetClassificationList(System.IntPtr,System.IntPtr&)
extern void UnsafeNativeMethods_mp_Packet__GetClassificationList_m5D5B5B259BA062A8428477492A3C96BB3643A69C (void);
// 0x0000029C Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_Packet__GetClassificationListVector(System.IntPtr,System.IntPtr&)
extern void UnsafeNativeMethods_mp_Packet__GetClassificationListVector_mBC4F2A6BE4F04333EA316763EA522EB6A128F187 (void);
// 0x0000029D Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_Packet__GetDetection(System.IntPtr,System.IntPtr&)
extern void UnsafeNativeMethods_mp_Packet__GetDetection_mCA5167FA259DE1FA1BA36991C515D0A661C98A73 (void);
// 0x0000029E Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_Packet__GetDetectionVector(System.IntPtr,System.IntPtr&)
extern void UnsafeNativeMethods_mp_Packet__GetDetectionVector_mBA6AF4461617DB866FEF4C4CCE067CD8DAB778CA (void);
// 0x0000029F Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_Packet__GetLandmarkList(System.IntPtr,System.IntPtr&)
extern void UnsafeNativeMethods_mp_Packet__GetLandmarkList_mC1C750C9718BB6E9AB90FE514367F98C3E88016E (void);
// 0x000002A0 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_Packet__GetLandmarkListVector(System.IntPtr,System.IntPtr&)
extern void UnsafeNativeMethods_mp_Packet__GetLandmarkListVector_mC6F29366A62516AEB8617EBA337E88258112EFCD (void);
// 0x000002A1 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_Packet__GetNormalizedLandmarkList(System.IntPtr,System.IntPtr&)
extern void UnsafeNativeMethods_mp_Packet__GetNormalizedLandmarkList_m5EF611C26E7162F20FD2AEB51DF01558308B88E1 (void);
// 0x000002A2 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_Packet__GetNormalizedLandmarkListVector(System.IntPtr,System.IntPtr&)
extern void UnsafeNativeMethods_mp_Packet__GetNormalizedLandmarkListVector_mE680E5DB73904DFAB7B3545C50112033A7E2ED67 (void);
// 0x000002A3 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_Packet__GetRect(System.IntPtr,System.IntPtr&)
extern void UnsafeNativeMethods_mp_Packet__GetRect_mA049CCB1123F24642E514987AFC72A3F355A931A (void);
// 0x000002A4 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_Packet__GetRectVector(System.IntPtr,System.IntPtr&)
extern void UnsafeNativeMethods_mp_Packet__GetRectVector_mA647B744426461E0170868EF986FF01793DFF02E (void);
// 0x000002A5 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_Packet__GetNormalizedRect(System.IntPtr,System.IntPtr&)
extern void UnsafeNativeMethods_mp_Packet__GetNormalizedRect_m5E23A7F00AAA100FD73AA55D5AA08989CC881D5A (void);
// 0x000002A6 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_Packet__GetNormalizedRectVector(System.IntPtr,System.IntPtr&)
extern void UnsafeNativeMethods_mp_Packet__GetNormalizedRectVector_mC34B528BB4B4519ABC6CC9B3594AC8794C93DAC8 (void);
// 0x000002A7 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_Packet__GetTimedModelMatrixProtoList(System.IntPtr,System.IntPtr&)
extern void UnsafeNativeMethods_mp_Packet__GetTimedModelMatrixProtoList_mF0BB4C96FAB580F5C37788678A7EE0811A20CB7F (void);
// 0x000002A8 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_Packet__GetFaceGeometry(System.IntPtr,System.IntPtr&)
extern void UnsafeNativeMethods_mp_Packet__GetFaceGeometry_m93386E3813301804D984AF23AEC6B712DC448ECE (void);
// 0x000002A9 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_Packet__GetFaceGeometryVector(System.IntPtr,System.IntPtr&)
extern void UnsafeNativeMethods_mp_Packet__GetFaceGeometryVector_mD84B81BAD735BD5A64A2A705800424841FF21E53 (void);
// 0x000002AA System.Void Mediapipe.UnsafeNativeMethods::delete_array__PKc(System.IntPtr)
extern void UnsafeNativeMethods_delete_array__PKc_mB7F674A345E1C9862F94699A91B4965A13E801FE (void);
// 0x000002AB System.Void Mediapipe.UnsafeNativeMethods::std_string__delete(System.IntPtr)
extern void UnsafeNativeMethods_std_string__delete_m6B1FB2B41291BEB1CE151B1A1B90D23657491B4D (void);
// 0x000002AC Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::std_string__PKc_i(System.Byte[],System.Int32,System.IntPtr&)
extern void UnsafeNativeMethods_std_string__PKc_i_m3F75B67B76097D20533314BE303411E4749D1DEB (void);
// 0x000002AD System.Void Mediapipe.UnsafeNativeMethods::std_string__swap__Rstr(System.IntPtr,System.IntPtr)
extern void UnsafeNativeMethods_std_string__swap__Rstr_mE68CFEE2B4838EE29B606D0E38B0086A06C3B8F8 (void);
// 0x000002AE Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_CalculatorGraph__(System.IntPtr&)
extern void UnsafeNativeMethods_mp_CalculatorGraph___m7FFC20DF00A0E3D5583A0989E91858A69DBFC673 (void);
// 0x000002AF Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_CalculatorGraph__PKc(System.String,System.IntPtr&)
extern void UnsafeNativeMethods_mp_CalculatorGraph__PKc_m329C0B20CD5E38149E5AC76B025827B0D925C777 (void);
// 0x000002B0 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_CalculatorGraph__PKc_i(System.Byte[],System.Int32,System.IntPtr&)
extern void UnsafeNativeMethods_mp_CalculatorGraph__PKc_i_m1C2E1FE59994A24961B38D3961F5C16005E4C9CB (void);
// 0x000002B1 System.Void Mediapipe.UnsafeNativeMethods::mp_CalculatorGraph__delete(System.IntPtr)
extern void UnsafeNativeMethods_mp_CalculatorGraph__delete_m4F90E4052AEB8AC600A1EE995DE948B638583172 (void);
// 0x000002B2 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_CalculatorGraph__Initialize__PKc_i(System.IntPtr,System.Byte[],System.Int32,System.IntPtr&)
extern void UnsafeNativeMethods_mp_CalculatorGraph__Initialize__PKc_i_m78A6F16896B73BAC036E293A9E30A66370E14683 (void);
// 0x000002B3 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_CalculatorGraph__Initialize__PKc_i_Rsp(System.IntPtr,System.Byte[],System.Int32,System.IntPtr,System.IntPtr&)
extern void UnsafeNativeMethods_mp_CalculatorGraph__Initialize__PKc_i_Rsp_m11B851A7306D03C2F01C4687AD14EB2AD1AEE212 (void);
// 0x000002B4 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_CalculatorGraph__Config(System.IntPtr,System.IntPtr&)
extern void UnsafeNativeMethods_mp_CalculatorGraph__Config_m6A9EAAADF8F36D3AEF4838D18A5B97F505CA4D66 (void);
// 0x000002B5 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_CalculatorGraph__ObserveOutputStream__PKc_PF(System.IntPtr,System.String,Mediapipe.CalculatorGraph/NativePacketCallback,System.IntPtr&)
extern void UnsafeNativeMethods_mp_CalculatorGraph__ObserveOutputStream__PKc_PF_m609221A7AAA13C9FBDD87CECA529658FC865A24B (void);
// 0x000002B6 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_CalculatorGraph__AddOutputStreamPoller__PKc(System.IntPtr,System.String,System.IntPtr&)
extern void UnsafeNativeMethods_mp_CalculatorGraph__AddOutputStreamPoller__PKc_m3B501FE83F370CCFA39FF12049AA9B8260A320B7 (void);
// 0x000002B7 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_CalculatorGraph__Run__Rsp(System.IntPtr,System.IntPtr,System.IntPtr&)
extern void UnsafeNativeMethods_mp_CalculatorGraph__Run__Rsp_mA406BF45E78BC89D290CE6EDA1D0EAC294B8987C (void);
// 0x000002B8 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_CalculatorGraph__StartRun__Rsp(System.IntPtr,System.IntPtr,System.IntPtr&)
extern void UnsafeNativeMethods_mp_CalculatorGraph__StartRun__Rsp_m6EFEC73224583BF5FD5B7A1C9562D94D1D4C8B19 (void);
// 0x000002B9 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_CalculatorGraph__WaitUntilIdle(System.IntPtr,System.IntPtr&)
extern void UnsafeNativeMethods_mp_CalculatorGraph__WaitUntilIdle_m5836C5B6885E9E0D5FA95257337273895E1B216A (void);
// 0x000002BA Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_CalculatorGraph__WaitUntilDone(System.IntPtr,System.IntPtr&)
extern void UnsafeNativeMethods_mp_CalculatorGraph__WaitUntilDone_m01DD698F4634390750656E085F47398555310280 (void);
// 0x000002BB Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_CalculatorGraph__AddPacketToInputStream__PKc_Ppacket(System.IntPtr,System.String,System.IntPtr,System.IntPtr&)
extern void UnsafeNativeMethods_mp_CalculatorGraph__AddPacketToInputStream__PKc_Ppacket_mD46F59A711F3260B006E08516C0A87D78AF4CCC3 (void);
// 0x000002BC Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_CalculatorGraph__SetInputStreamMaxQueueSize__PKc_i(System.IntPtr,System.String,System.Int32,System.IntPtr&)
extern void UnsafeNativeMethods_mp_CalculatorGraph__SetInputStreamMaxQueueSize__PKc_i_mD9A2A728135B91D91148BB502E27F6A99CC75272 (void);
// 0x000002BD Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_CalculatorGraph__CloseInputStream__PKc(System.IntPtr,System.String,System.IntPtr&)
extern void UnsafeNativeMethods_mp_CalculatorGraph__CloseInputStream__PKc_m72329320798825CF335CCA402B3A4BE94CE659BE (void);
// 0x000002BE Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_CalculatorGraph__CloseAllPacketSources(System.IntPtr,System.IntPtr&)
extern void UnsafeNativeMethods_mp_CalculatorGraph__CloseAllPacketSources_m4B78C1B5E16004B23D9D3BC97A666241446CF33B (void);
// 0x000002BF Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_CalculatorGraph__Cancel(System.IntPtr)
extern void UnsafeNativeMethods_mp_CalculatorGraph__Cancel_m31D80E21BEE4DA0CA352FD5D3288099ECB9DFAA1 (void);
// 0x000002C0 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_CalculatorGraph__GetGpuResources(System.IntPtr,System.IntPtr&)
extern void UnsafeNativeMethods_mp_CalculatorGraph__GetGpuResources_m6AB83B12E22D06B4542FDDF59E3849B48EF32009 (void);
// 0x000002C1 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_CalculatorGraph__SetGpuResources__SPgpu(System.IntPtr,System.IntPtr,System.IntPtr&)
extern void UnsafeNativeMethods_mp_CalculatorGraph__SetGpuResources__SPgpu_mD550E6BD978D9EE8C9FCB458CB8C18E519DD77C5 (void);
// 0x000002C2 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_api__ConvertFromCalculatorGraphConfigTextFormat(System.String,System.IntPtr&)
extern void UnsafeNativeMethods_mp_api__ConvertFromCalculatorGraphConfigTextFormat_mEC8C2CA29B20E95D6F69B5D01E7A25FEE1306050 (void);
// 0x000002C3 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_ImageFrame__(System.IntPtr&)
extern void UnsafeNativeMethods_mp_ImageFrame___mA06D1D408059353336E7DADD697E12AEE592B9C9 (void);
// 0x000002C4 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_ImageFrame__ui_i_i_ui(Mediapipe.ImageFormat/Format,System.Int32,System.Int32,System.UInt32,System.IntPtr&)
extern void UnsafeNativeMethods_mp_ImageFrame__ui_i_i_ui_m80132AFA17D9384B6314144D18126A205B6CE629 (void);
// 0x000002C5 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_ImageFrame__ui_i_i_i_Pui8_PF(Mediapipe.ImageFormat/Format,System.Int32,System.Int32,System.Int32,System.IntPtr,Mediapipe.ImageFrame/Deleter,System.IntPtr&)
extern void UnsafeNativeMethods_mp_ImageFrame__ui_i_i_i_Pui8_PF_m951B34241067B94DD839F7245FB354B8CAEE2B33 (void);
// 0x000002C6 System.Void Mediapipe.UnsafeNativeMethods::mp_ImageFrame__delete(System.IntPtr)
extern void UnsafeNativeMethods_mp_ImageFrame__delete_m633ABD20C43834CC5CFF3BB5AB772F49126A7C3E (void);
// 0x000002C7 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_ImageFrame__SetToZero(System.IntPtr)
extern void UnsafeNativeMethods_mp_ImageFrame__SetToZero_m57776266CD9A1126948AB95987EEF125667BE2F9 (void);
// 0x000002C8 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_ImageFrame__SetAlignmentPaddingAreas(System.IntPtr)
extern void UnsafeNativeMethods_mp_ImageFrame__SetAlignmentPaddingAreas_m9B581E7702727EFF945566989EAB34CA36029E73 (void);
// 0x000002C9 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_ImageFrame__CopyToBuffer__Pui8_i(System.IntPtr,System.IntPtr,System.Int32)
extern void UnsafeNativeMethods_mp_ImageFrame__CopyToBuffer__Pui8_i_m04B90E082C813A3D32C91D98F466DE61DBE09C9E (void);
// 0x000002CA Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_ImageFrame__CopyToBuffer__Pui16_i(System.IntPtr,System.IntPtr,System.Int32)
extern void UnsafeNativeMethods_mp_ImageFrame__CopyToBuffer__Pui16_i_mF73153B91A88324107CAA664D57ECC18DA8A4929 (void);
// 0x000002CB Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_ImageFrame__CopyToBuffer__Pf_i(System.IntPtr,System.IntPtr,System.Int32)
extern void UnsafeNativeMethods_mp_ImageFrame__CopyToBuffer__Pf_i_m5B626C3F783B9714F178EFD0560B2CAADCE82387 (void);
// 0x000002CC System.Void Mediapipe.UnsafeNativeMethods::mp_StatusOrImageFrame__delete(System.IntPtr)
extern void UnsafeNativeMethods_mp_StatusOrImageFrame__delete_mC4D146E8E63A9E69BE226B36EDBD0C7B0E257CF7 (void);
// 0x000002CD Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_StatusOrImageFrame__status(System.IntPtr,System.IntPtr&)
extern void UnsafeNativeMethods_mp_StatusOrImageFrame__status_m8814246D65E05FEB29751A9228737687FDA2F43A (void);
// 0x000002CE Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_StatusOrImageFrame__value(System.IntPtr,System.IntPtr&)
extern void UnsafeNativeMethods_mp_StatusOrImageFrame__value_mBB34AC5304F76D102DA54AA9324D41386A850759 (void);
// 0x000002CF Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp__MakeImageFramePacket__Pif(System.IntPtr,System.IntPtr&)
extern void UnsafeNativeMethods_mp__MakeImageFramePacket__Pif_mE778FC600DFBAB5FEEA64D23594176FD404DB5DD (void);
// 0x000002D0 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp__MakeImageFramePacket_At__Pif_Rt(System.IntPtr,System.IntPtr,System.IntPtr&)
extern void UnsafeNativeMethods_mp__MakeImageFramePacket_At__Pif_Rt_mEDE9F3DC30DD60B1F1B5D43029628162BC25E618 (void);
// 0x000002D1 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_Packet__ConsumeImageFrame(System.IntPtr,System.IntPtr&)
extern void UnsafeNativeMethods_mp_Packet__ConsumeImageFrame_mFE66F0BB2E99E16A494E589FE93930D4B4F3EC2B (void);
// 0x000002D2 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_Packet__GetImageFrame(System.IntPtr,System.IntPtr&)
extern void UnsafeNativeMethods_mp_Packet__GetImageFrame_mAFFEEEC5E9AE7853BCDBEF383C1E5773FF2A36B9 (void);
// 0x000002D3 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_Packet__ValidateAsImageFrame(System.IntPtr,System.IntPtr&)
extern void UnsafeNativeMethods_mp_Packet__ValidateAsImageFrame_mBEA2DC0EAC6996B6CF44F64ED525F7946F477997 (void);
// 0x000002D4 System.Void Mediapipe.UnsafeNativeMethods::mp_OutputStreamPoller__delete(System.IntPtr)
extern void UnsafeNativeMethods_mp_OutputStreamPoller__delete_mF80A88EB6F8AEEF3601FC8A9BC993B47CD0D8EDE (void);
// 0x000002D5 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_OutputStreamPoller__Reset(System.IntPtr)
extern void UnsafeNativeMethods_mp_OutputStreamPoller__Reset_m24569A5627802636296D36F6700ACCDB684E4C8A (void);
// 0x000002D6 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_OutputStreamPoller__Next_Ppacket(System.IntPtr,System.IntPtr,System.Boolean&)
extern void UnsafeNativeMethods_mp_OutputStreamPoller__Next_Ppacket_mD2F54CDCB8BD9834FEA9E5CFD3CC8281CA4AECD8 (void);
// 0x000002D7 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_OutputStreamPoller__SetMaxQueueSize(System.IntPtr,System.Int32)
extern void UnsafeNativeMethods_mp_OutputStreamPoller__SetMaxQueueSize_m5E4C00650A5637BD5B58C6B1C6B20669C2C12F8C (void);
// 0x000002D8 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_OutputStreamPoller__QueueSize(System.IntPtr,System.Int32&)
extern void UnsafeNativeMethods_mp_OutputStreamPoller__QueueSize_mB59D18B9A8C93F800BDB95705B0A7B7B5C5C7373 (void);
// 0x000002D9 System.Void Mediapipe.UnsafeNativeMethods::mp_StatusOrPoller__delete(System.IntPtr)
extern void UnsafeNativeMethods_mp_StatusOrPoller__delete_m783FE25074696766E55842007A3DD8D5B3151D03 (void);
// 0x000002DA Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_StatusOrPoller__status(System.IntPtr,System.IntPtr&)
extern void UnsafeNativeMethods_mp_StatusOrPoller__status_mB98DDA33E18CBF2A8918ACFD99E34D556EFFA8D1 (void);
// 0x000002DB Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_StatusOrPoller__value(System.IntPtr,System.IntPtr&)
extern void UnsafeNativeMethods_mp_StatusOrPoller__value_mD44249D0D2D220280F8DAEC85AFEE3210DACECFC (void);
// 0x000002DC Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_Packet__(System.IntPtr&)
extern void UnsafeNativeMethods_mp_Packet___m4B27218CA7EAE8669B1445DBF18849A654B18914 (void);
// 0x000002DD System.Void Mediapipe.UnsafeNativeMethods::mp_Packet__delete(System.IntPtr)
extern void UnsafeNativeMethods_mp_Packet__delete_m4259B2C7C37715CA2C47B439DCC152F4985D17EB (void);
// 0x000002DE Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_Packet__At__Rt(System.IntPtr,System.IntPtr,System.IntPtr&)
extern void UnsafeNativeMethods_mp_Packet__At__Rt_m2CA92FA4A50BAA10DE9F8C740666E19B30F4FE96 (void);
// 0x000002DF Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_Packet__ValidateAsProtoMessageLite(System.IntPtr,System.IntPtr&)
extern void UnsafeNativeMethods_mp_Packet__ValidateAsProtoMessageLite_m2B98615C611E32D9D9A74379B6B910936B0D99F4 (void);
// 0x000002E0 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_Packet__Timestamp(System.IntPtr,System.IntPtr&)
extern void UnsafeNativeMethods_mp_Packet__Timestamp_m689EF52DE8336D6936127AB4FB2F172241C504A3 (void);
// 0x000002E1 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_Packet__DebugString(System.IntPtr,System.IntPtr&)
extern void UnsafeNativeMethods_mp_Packet__DebugString_m5453F6427CC9F8E19279E4EA657D0954F58D13FD (void);
// 0x000002E2 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_Packet__RegisteredTypeName(System.IntPtr,System.IntPtr&)
extern void UnsafeNativeMethods_mp_Packet__RegisteredTypeName_mD801D6208E8FF92A78BC25FDD3F08B38C0221A42 (void);
// 0x000002E3 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_Packet__DebugTypeName(System.IntPtr,System.IntPtr&)
extern void UnsafeNativeMethods_mp_Packet__DebugTypeName_m3CF6B3782848CF204F773730E55C99A878A773EB (void);
// 0x000002E4 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp__MakeBoolPacket__b(System.Boolean,System.IntPtr&)
extern void UnsafeNativeMethods_mp__MakeBoolPacket__b_mC3D4996305DDE3045E015C65FE8144E3B1DBA22E (void);
// 0x000002E5 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp__MakeBoolPacket_At__b_Rt(System.Boolean,System.IntPtr,System.IntPtr&)
extern void UnsafeNativeMethods_mp__MakeBoolPacket_At__b_Rt_mFAC5E3C58B53F3E16C9190BADE565D6017608850 (void);
// 0x000002E6 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_Packet__GetBool(System.IntPtr,System.Boolean&)
extern void UnsafeNativeMethods_mp_Packet__GetBool_m2EC3BF6749448AA1575613423B66076567BCCEE7 (void);
// 0x000002E7 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_Packet__ValidateAsBool(System.IntPtr,System.IntPtr&)
extern void UnsafeNativeMethods_mp_Packet__ValidateAsBool_m172217551337C215F886BFEE43BF596D9ADDE840 (void);
// 0x000002E8 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp__MakeFloatPacket__f(System.Single,System.IntPtr&)
extern void UnsafeNativeMethods_mp__MakeFloatPacket__f_m25C2E700AE041259CD826F002DF694B21A1CDA73 (void);
// 0x000002E9 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp__MakeFloatPacket_At__f_Rt(System.Single,System.IntPtr,System.IntPtr&)
extern void UnsafeNativeMethods_mp__MakeFloatPacket_At__f_Rt_m398C11302C7E534056394D2C0793D06643FE65F1 (void);
// 0x000002EA Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_Packet__GetFloat(System.IntPtr,System.Single&)
extern void UnsafeNativeMethods_mp_Packet__GetFloat_m3DF61A639355E715A082DE0139AF4EC96FF00FD7 (void);
// 0x000002EB Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_Packet__ValidateAsFloat(System.IntPtr,System.IntPtr&)
extern void UnsafeNativeMethods_mp_Packet__ValidateAsFloat_m4E211787F621A579F30BEC34C7A22BD819A9DE6A (void);
// 0x000002EC Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp__MakeIntPacket__i(System.Int32,System.IntPtr&)
extern void UnsafeNativeMethods_mp__MakeIntPacket__i_mD539128EF054E5532E7940C8FB4642262915B3F6 (void);
// 0x000002ED Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp__MakeIntPacket_At__i_Rt(System.Int32,System.IntPtr,System.IntPtr&)
extern void UnsafeNativeMethods_mp__MakeIntPacket_At__i_Rt_m5315ECFC5DDB21123C2AD6505C8C7BD04946113C (void);
// 0x000002EE Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_Packet__GetInt(System.IntPtr,System.Int32&)
extern void UnsafeNativeMethods_mp_Packet__GetInt_m279BC5C3E5B49E90E524E9BF7682FAF427D3C74C (void);
// 0x000002EF Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_Packet__ValidateAsInt(System.IntPtr,System.IntPtr&)
extern void UnsafeNativeMethods_mp_Packet__ValidateAsInt_mDFBB7CAF5C4460501B979651F006807B8FAD4B5D (void);
// 0x000002F0 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp__MakeFloatArrayPacket__Pf_i(System.Single[],System.Int32,System.IntPtr&)
extern void UnsafeNativeMethods_mp__MakeFloatArrayPacket__Pf_i_mEAA64058B9C860B8011985E7C7A2A70B7D330C9E (void);
// 0x000002F1 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp__MakeFloatArrayPacket_At__Pf_i_Rt(System.Single[],System.Int32,System.IntPtr,System.IntPtr&)
extern void UnsafeNativeMethods_mp__MakeFloatArrayPacket_At__Pf_i_Rt_mA760CF1527D55C13EFCF9D81A70D7DFFF5A68427 (void);
// 0x000002F2 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_Packet__GetFloatArray(System.IntPtr,System.IntPtr&)
extern void UnsafeNativeMethods_mp_Packet__GetFloatArray_m6FCAFB32B0AB5682A91C38FDB9113660BFC9D045 (void);
// 0x000002F3 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_Packet__ValidateAsFloatArray(System.IntPtr,System.IntPtr&)
extern void UnsafeNativeMethods_mp_Packet__ValidateAsFloatArray_m68959F5932769B7D9C01A3B339287059EF5F7051 (void);
// 0x000002F4 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp__MakeStringPacket__PKc(System.String,System.IntPtr&)
extern void UnsafeNativeMethods_mp__MakeStringPacket__PKc_m9CB04B7CC2B5980591C6DD4681E67931F0BCEBC3 (void);
// 0x000002F5 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp__MakeStringPacket_At__PKc_Rt(System.String,System.IntPtr,System.IntPtr&)
extern void UnsafeNativeMethods_mp__MakeStringPacket_At__PKc_Rt_mA35B8CB6736C0E66156E45128375581B0F83AADA (void);
// 0x000002F6 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp__MakeStringPacket__PKc_i(System.Byte[],System.Int32,System.IntPtr&)
extern void UnsafeNativeMethods_mp__MakeStringPacket__PKc_i_mD2991AB834F1A47CD5EB185B64EE75AF8651EA82 (void);
// 0x000002F7 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp__MakeStringPacket_At__PKc_i_Rt(System.Byte[],System.Int32,System.IntPtr,System.IntPtr&)
extern void UnsafeNativeMethods_mp__MakeStringPacket_At__PKc_i_Rt_m32DDCC8982FFADEF18FE28C0CEAB7FA203598089 (void);
// 0x000002F8 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_Packet__GetString(System.IntPtr,System.IntPtr&)
extern void UnsafeNativeMethods_mp_Packet__GetString_m6A01C86A275F3A0DFE5A623DBA21D6C959B42D98 (void);
// 0x000002F9 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_Packet__GetByteString(System.IntPtr,System.IntPtr&,System.Int32&)
extern void UnsafeNativeMethods_mp_Packet__GetByteString_m03B679BA78DCCE37F60AF59BC9EE7FF4BE1F1652 (void);
// 0x000002FA Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_Packet__ValidateAsString(System.IntPtr,System.IntPtr&)
extern void UnsafeNativeMethods_mp_Packet__ValidateAsString_m41ECBB9C8AF0ED93C63FC6DC390ECE79F7D1BA33 (void);
// 0x000002FB Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_SidePacket__(System.IntPtr&)
extern void UnsafeNativeMethods_mp_SidePacket___m105DEB8D0482F8BEFE6B74880846081FCD914C30 (void);
// 0x000002FC System.Void Mediapipe.UnsafeNativeMethods::mp_SidePacket__delete(System.IntPtr)
extern void UnsafeNativeMethods_mp_SidePacket__delete_mB0AB6C5D00E85998CE0781C3E5E774184652FE50 (void);
// 0x000002FD Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_SidePacket__emplace__PKc_Rp(System.IntPtr,System.String,System.IntPtr)
extern void UnsafeNativeMethods_mp_SidePacket__emplace__PKc_Rp_m65F3E6A67163EABD3CC8534A1662D6AE1EADA1BE (void);
// 0x000002FE Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_SidePacket__at__PKc(System.IntPtr,System.String,System.IntPtr&)
extern void UnsafeNativeMethods_mp_SidePacket__at__PKc_m44BE563F3160D472928CDE52BF897D28C4702331 (void);
// 0x000002FF Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_SidePacket__erase__PKc(System.IntPtr,System.String,System.Int32&)
extern void UnsafeNativeMethods_mp_SidePacket__erase__PKc_m1EB44CE19489868A1DA2DE63C2157C96E3CD8FEF (void);
// 0x00000300 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_Timestamp__l(System.Int64,System.IntPtr&)
extern void UnsafeNativeMethods_mp_Timestamp__l_m05D27598D3A65F6F974231A155EE46E678070DBA (void);
// 0x00000301 System.Void Mediapipe.UnsafeNativeMethods::mp_Timestamp__delete(System.IntPtr)
extern void UnsafeNativeMethods_mp_Timestamp__delete_mEB3830A001AF05B5D8319E20471716AA7774D4EB (void);
// 0x00000302 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_Timestamp__DebugString(System.IntPtr,System.IntPtr&)
extern void UnsafeNativeMethods_mp_Timestamp__DebugString_mFAD408E42CA854250F504DB2D4294AE8F3F94FF2 (void);
// 0x00000303 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_Timestamp__NextAllowedInStream(System.IntPtr,System.IntPtr&)
extern void UnsafeNativeMethods_mp_Timestamp__NextAllowedInStream_mC7E9A0F316FFDFFDFD6D04890BFECB9DC55BB22A (void);
// 0x00000304 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_Timestamp__PreviousAllowedInStream(System.IntPtr,System.IntPtr&)
extern void UnsafeNativeMethods_mp_Timestamp__PreviousAllowedInStream_m7B02969BE69F1114CD9631E2236B743417C23179 (void);
// 0x00000305 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_Timestamp_FromSeconds__d(System.Double,System.IntPtr&)
extern void UnsafeNativeMethods_mp_Timestamp_FromSeconds__d_m5804EC6D41E66A6DB34CBEFD8DFDA7BFF40104E8 (void);
// 0x00000306 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_Timestamp_Unset(System.IntPtr&)
extern void UnsafeNativeMethods_mp_Timestamp_Unset_mD15DA5F84FC6FE6A1649313F714C63EB97985EE3 (void);
// 0x00000307 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_Timestamp_Unstarted(System.IntPtr&)
extern void UnsafeNativeMethods_mp_Timestamp_Unstarted_m629139CD13BD3C6004CE0B30AF045B321610E65D (void);
// 0x00000308 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_Timestamp_PreStream(System.IntPtr&)
extern void UnsafeNativeMethods_mp_Timestamp_PreStream_m8B9544DC5A11273F31DC7D4F0E0908BA15530EFC (void);
// 0x00000309 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_Timestamp_Min(System.IntPtr&)
extern void UnsafeNativeMethods_mp_Timestamp_Min_m79F65301A63D19211BA106BB0364539DF07AD123 (void);
// 0x0000030A Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_Timestamp_Max(System.IntPtr&)
extern void UnsafeNativeMethods_mp_Timestamp_Max_m7151A86A5B0D6CA573BB0E58C4F7AB15BB1D9679 (void);
// 0x0000030B Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_Timestamp_PostStream(System.IntPtr&)
extern void UnsafeNativeMethods_mp_Timestamp_PostStream_mE373877C8728C18F15D7FC2EA3478374F882A4DB (void);
// 0x0000030C Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_Timestamp_OneOverPostStream(System.IntPtr&)
extern void UnsafeNativeMethods_mp_Timestamp_OneOverPostStream_m198726C8309D7E9DFD81CFD5A924109CBE638798 (void);
// 0x0000030D Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_Timestamp_Done(System.IntPtr&)
extern void UnsafeNativeMethods_mp_Timestamp_Done_m0E665BE4D0BD4A62195A021E7575EFFE4028C649 (void);
// 0x0000030E Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_EglSurfaceHolderUniquePtr__(System.IntPtr&)
extern void UnsafeNativeMethods_mp_EglSurfaceHolderUniquePtr___mFA1960DBD060B9DD0714F9DFAE51C4D924541EEC (void);
// 0x0000030F System.Void Mediapipe.UnsafeNativeMethods::mp_EglSurfaceHolderUniquePtr__delete(System.IntPtr)
extern void UnsafeNativeMethods_mp_EglSurfaceHolderUniquePtr__delete_mA3702C0E386123FD244B6B6779D532DDCBA56FED (void);
// 0x00000310 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_EglSurfaceHolder__SetSurface__P_Pgc(System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr&)
extern void UnsafeNativeMethods_mp_EglSurfaceHolder__SetSurface__P_Pgc_m7D9ADF0C33B15FD538BFE52B0C9E1D12335C42FD (void);
// 0x00000311 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp__MakeEglSurfaceHolderUniquePtrPacket__Reshup(System.IntPtr,System.IntPtr&)
extern void UnsafeNativeMethods_mp__MakeEglSurfaceHolderUniquePtrPacket__Reshup_m61EA050A5C212CC77AF491C042B3EB21584B78A6 (void);
// 0x00000312 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_Packet__GetEglSurfaceHolderUniquePtr(System.IntPtr,System.IntPtr&)
extern void UnsafeNativeMethods_mp_Packet__GetEglSurfaceHolderUniquePtr_mCA5F9F06A0B3D1C8E16C458D3A6E90A95DC9CBF4 (void);
// 0x00000313 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_Packet__ValidateAsEglSurfaceHolderUniquePtr(System.IntPtr,System.IntPtr&)
extern void UnsafeNativeMethods_mp_Packet__ValidateAsEglSurfaceHolderUniquePtr_mF81CDC357FE5EA028BA317FCAF78B28516FC139C (void);
// 0x00000314 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_GlCalculatorHelper__(System.IntPtr&)
extern void UnsafeNativeMethods_mp_GlCalculatorHelper___mD9794B062CC2A2861E91913A68EBA24797832503 (void);
// 0x00000315 System.Void Mediapipe.UnsafeNativeMethods::mp_GlCalculatorHelper__delete(System.IntPtr)
extern void UnsafeNativeMethods_mp_GlCalculatorHelper__delete_mC36BC1ADF60A447B21C608334F90DAF714BDBB50 (void);
// 0x00000316 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_GlCalculatorHelper__InitializeForTest__Pgr(System.IntPtr,System.IntPtr)
extern void UnsafeNativeMethods_mp_GlCalculatorHelper__InitializeForTest__Pgr_m4E62C0E3703796B16C2EE5FE95D84350C0C46C25 (void);
// 0x00000317 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_GlCalculatorHelper__RunInGlContext__PF(System.IntPtr,Mediapipe.GlCalculatorHelper/NativeGlStatusFunction,System.IntPtr&)
extern void UnsafeNativeMethods_mp_GlCalculatorHelper__RunInGlContext__PF_mD7818FFEFEE3F379478842EEBC4AF5AC99FBBA26 (void);
// 0x00000318 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_GlCalculatorHelper__CreateSourceTexture__Rif(System.IntPtr,System.IntPtr,System.IntPtr&)
extern void UnsafeNativeMethods_mp_GlCalculatorHelper__CreateSourceTexture__Rif_m08794BCA2130AF6D4CF01EA0787CEBBD5FB0C5E2 (void);
// 0x00000319 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_GlCalculatorHelper__CreateSourceTexture__Rgb(System.IntPtr,System.IntPtr,System.IntPtr&)
extern void UnsafeNativeMethods_mp_GlCalculatorHelper__CreateSourceTexture__Rgb_mC62659CA1113C1599CE628C1682D568A6646E433 (void);
// 0x0000031A Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_GlCalculatorHelper__CreateDestinationTexture__i_i_ui(System.IntPtr,System.Int32,System.Int32,Mediapipe.GpuBufferFormat,System.IntPtr&)
extern void UnsafeNativeMethods_mp_GlCalculatorHelper__CreateDestinationTexture__i_i_ui_m955037C794329EB4F3C07F7198206F26EBCE8104 (void);
// 0x0000031B Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_GlCalculatorHelper__CreateDestinationTexture__Rgb(System.IntPtr,System.IntPtr,System.IntPtr&)
extern void UnsafeNativeMethods_mp_GlCalculatorHelper__CreateDestinationTexture__Rgb_mA8F4C5AAF049376D6BD5C3D1EEB6C80E4492F420 (void);
// 0x0000031C Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_GlCalculatorHelper__BindFrameBuffer__Rtexture(System.IntPtr,System.IntPtr)
extern void UnsafeNativeMethods_mp_GlCalculatorHelper__BindFrameBuffer__Rtexture_mFF1C7E100E489A7C94A71F8367B2B33220E476A6 (void);
// 0x0000031D System.Void Mediapipe.UnsafeNativeMethods::mp_SharedGlContext__delete(System.IntPtr)
extern void UnsafeNativeMethods_mp_SharedGlContext__delete_m516471F482EE3DCAB00F1AD7AA55FD85A03AA0B5 (void);
// 0x0000031E System.Void Mediapipe.UnsafeNativeMethods::mp_SharedGlContext__reset(System.IntPtr)
extern void UnsafeNativeMethods_mp_SharedGlContext__reset_m982C4B36DF337D091A57B1535F351E82981CD103 (void);
// 0x0000031F Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_GlContext_GetCurrent(System.IntPtr&)
extern void UnsafeNativeMethods_mp_GlContext_GetCurrent_mA74C4F85595901A7D6A900E3AF34F0AE68C3E0D5 (void);
// 0x00000320 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_GlContext_Create__P_b(System.Boolean,System.IntPtr&)
extern void UnsafeNativeMethods_mp_GlContext_Create__P_b_m0163CB3DC08ECA02C9AB2EBBA6B28AB7242457B4 (void);
// 0x00000321 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_GlContext_Create__Rgc_b(System.IntPtr,System.Boolean,System.IntPtr&)
extern void UnsafeNativeMethods_mp_GlContext_Create__Rgc_b_m47970266FC551B1062583F9FDDC0C6725B8C5CF4 (void);
// 0x00000322 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_GlContext_Create__ui_b(System.UInt32,System.Boolean,System.IntPtr&)
extern void UnsafeNativeMethods_mp_GlContext_Create__ui_b_mA82830BB2C8B7ECFFA412AD51528FCBE33C88E35 (void);
// 0x00000323 System.Void Mediapipe.UnsafeNativeMethods::mp_GlSyncToken__delete(System.IntPtr)
extern void UnsafeNativeMethods_mp_GlSyncToken__delete_m134132AEBC805F6AF3957668AD15F9909C100DEF (void);
// 0x00000324 System.Void Mediapipe.UnsafeNativeMethods::mp_GlSyncToken__reset(System.IntPtr)
extern void UnsafeNativeMethods_mp_GlSyncToken__reset_mD8A9583F43E7F7D8F5CC3696450C605DC02C33A8 (void);
// 0x00000325 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_GlSyncPoint__Wait(System.IntPtr)
extern void UnsafeNativeMethods_mp_GlSyncPoint__Wait_m31E26352E617FB81B42288C708295D0AB95F5BD8 (void);
// 0x00000326 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_GlSyncPoint__WaitOnGpu(System.IntPtr)
extern void UnsafeNativeMethods_mp_GlSyncPoint__WaitOnGpu_mD6A37C1C2ED9A7E1C23F1FFFF08F8A4D91BC79CC (void);
// 0x00000327 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_GlSyncPoint__IsReady(System.IntPtr,System.Boolean&)
extern void UnsafeNativeMethods_mp_GlSyncPoint__IsReady_m4C8F9A53EF337E3BAAAF12B00B6EB747F78CE1DC (void);
// 0x00000328 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_GlSyncPoint__GetContext(System.IntPtr,System.IntPtr&)
extern void UnsafeNativeMethods_mp_GlSyncPoint__GetContext_m5136BE6D034EC4AC9EA35BEFFAD0F3541110998B (void);
// 0x00000329 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_GlTextureBuffer__WaitUntilComplete(System.IntPtr)
extern void UnsafeNativeMethods_mp_GlTextureBuffer__WaitUntilComplete_m4233E6A8A9400BD9362A697949ECE362AD9A3DAC (void);
// 0x0000032A Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_GlTextureBuffer__WaitOnGpu(System.IntPtr)
extern void UnsafeNativeMethods_mp_GlTextureBuffer__WaitOnGpu_m79EFB9905F183C292E68C34D93BA48867A3DFE52 (void);
// 0x0000032B Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_GlTextureBuffer__Reuse(System.IntPtr)
extern void UnsafeNativeMethods_mp_GlTextureBuffer__Reuse_m0EBB634379283DD74A401E5695298FD2DBBEA8C4 (void);
// 0x0000032C Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_GlTextureBuffer__Updated__Pgst(System.IntPtr,System.IntPtr)
extern void UnsafeNativeMethods_mp_GlTextureBuffer__Updated__Pgst_m4415F8C0D5D254DF566E81E7E17D65F6611BBE71 (void);
// 0x0000032D Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_GlTextureBuffer__DidRead__Pgst(System.IntPtr,System.IntPtr)
extern void UnsafeNativeMethods_mp_GlTextureBuffer__DidRead__Pgst_m26D4FE5DD8591985820F7DDC07D8243FC05F6CBD (void);
// 0x0000032E Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_GlTextureBuffer__WaitForConsumers(System.IntPtr)
extern void UnsafeNativeMethods_mp_GlTextureBuffer__WaitForConsumers_m158EF85652DCC48F4B34A3B23F7730CCBB54FD15 (void);
// 0x0000032F Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_GlTextureBuffer__WaitForConsumersOnGpu(System.IntPtr)
extern void UnsafeNativeMethods_mp_GlTextureBuffer__WaitForConsumersOnGpu_mED94677CBF0899C734031EE6503B752124878927 (void);
// 0x00000330 System.Void Mediapipe.UnsafeNativeMethods::mp_SharedGlTextureBuffer__delete(System.IntPtr)
extern void UnsafeNativeMethods_mp_SharedGlTextureBuffer__delete_mBB962EAC919950600E9A4904B58333CAF4500442 (void);
// 0x00000331 System.Void Mediapipe.UnsafeNativeMethods::mp_SharedGlTextureBuffer__reset(System.IntPtr)
extern void UnsafeNativeMethods_mp_SharedGlTextureBuffer__reset_mA0236B1D07EE119BF809E6F8EDBE8CF978F02E1E (void);
// 0x00000332 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_SharedGlTextureBuffer__ui_ui_i_i_ui_PF_PSgc(System.UInt32,System.UInt32,System.Int32,System.Int32,Mediapipe.GpuBufferFormat,Mediapipe.GlTextureBuffer/DeletionCallback,System.IntPtr,System.IntPtr&)
extern void UnsafeNativeMethods_mp_SharedGlTextureBuffer__ui_ui_i_i_ui_PF_PSgc_mD32CEEB07332C225968A2E51ADE24F31EF0436FF (void);
// 0x00000333 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_GlTexture__(System.IntPtr&)
extern void UnsafeNativeMethods_mp_GlTexture___m20BE7519E2CF13EA0206A14894C4432B6CEF1F23 (void);
// 0x00000334 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_GlTexture__ui_i_i(System.UInt32,System.Int32,System.Int32,System.IntPtr&)
extern void UnsafeNativeMethods_mp_GlTexture__ui_i_i_mB10E1E2EA1848D47D096D9FCFD7AC2B84E5BB209 (void);
// 0x00000335 System.Void Mediapipe.UnsafeNativeMethods::mp_GlTexture__delete(System.IntPtr)
extern void UnsafeNativeMethods_mp_GlTexture__delete_mE7BE0DD7AED9B9BFB8C9B80C295A71824ADAA3B0 (void);
// 0x00000336 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_GlTexture__Release(System.IntPtr)
extern void UnsafeNativeMethods_mp_GlTexture__Release_m9D098B1E50A054D2EF6331D23AC28A2EAAD3A4CE (void);
// 0x00000337 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_GlTexture__GetGpuBufferFrame(System.IntPtr,System.IntPtr&)
extern void UnsafeNativeMethods_mp_GlTexture__GetGpuBufferFrame_mC9CAE089643F63706F7B9FB64FE9D56A425AFAC1 (void);
// 0x00000338 System.Void Mediapipe.UnsafeNativeMethods::glFlush()
extern void UnsafeNativeMethods_glFlush_m31511D22964C1C47F83A18E543AD163B2EEE80CD (void);
// 0x00000339 System.Void Mediapipe.UnsafeNativeMethods::glReadPixels(System.Int32,System.Int32,System.Int32,System.Int32,System.UInt32,System.UInt32,System.IntPtr)
extern void UnsafeNativeMethods_glReadPixels_mB6CD520FF4A77019A700CDEB6646E61DED73EC85 (void);
// 0x0000033A Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp__GlTextureInfoForGpuBufferFormat__ui_i_ui(Mediapipe.GpuBufferFormat,System.Int32,Mediapipe.GlVersion,System.IntPtr&)
extern void UnsafeNativeMethods_mp__GlTextureInfoForGpuBufferFormat__ui_i_ui_m702EC5C6A8CDE05A30017A55B335D8F9814AAD20 (void);
// 0x0000033B System.Void Mediapipe.UnsafeNativeMethods::mp_GlTextureInfo__delete(System.IntPtr)
extern void UnsafeNativeMethods_mp_GlTextureInfo__delete_mD15DB3B56AA9D273779773AD51CD4C6B09D9272F (void);
// 0x0000033C Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_GpuBuffer__PSgtb(System.IntPtr,System.IntPtr&)
extern void UnsafeNativeMethods_mp_GpuBuffer__PSgtb_mB8E0C0C78133F955B567785F8DF9E55A74748DFC (void);
// 0x0000033D System.Void Mediapipe.UnsafeNativeMethods::mp_GpuBuffer__delete(System.IntPtr)
extern void UnsafeNativeMethods_mp_GpuBuffer__delete_m074BA5E059620B6D6A4DF0724B7A9FB4F66A46CE (void);
// 0x0000033E System.Void Mediapipe.UnsafeNativeMethods::mp_StatusOrGpuBuffer__delete(System.IntPtr)
extern void UnsafeNativeMethods_mp_StatusOrGpuBuffer__delete_m6A3EC340F49BEAA2300BDEB0C03317291DF0876B (void);
// 0x0000033F Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_StatusOrGpuBuffer__status(System.IntPtr,System.IntPtr&)
extern void UnsafeNativeMethods_mp_StatusOrGpuBuffer__status_m7C19FE633B1262D3116585E157D55B4AB19A4D25 (void);
// 0x00000340 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_StatusOrGpuBuffer__value(System.IntPtr,System.IntPtr&)
extern void UnsafeNativeMethods_mp_StatusOrGpuBuffer__value_m76C20DD7EC1309D1390043ACDDE80BF5B158996E (void);
// 0x00000341 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp__MakeGpuBufferPacket__Rgb(System.IntPtr,System.IntPtr&)
extern void UnsafeNativeMethods_mp__MakeGpuBufferPacket__Rgb_mCC7FC6DB7296AFDB4D47AFEFD64C62311E4E1603 (void);
// 0x00000342 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp__MakeGpuBufferPacket_At__Rgb_Rts(System.IntPtr,System.IntPtr,System.IntPtr&)
extern void UnsafeNativeMethods_mp__MakeGpuBufferPacket_At__Rgb_Rts_mF6EEF02FF77BBE000B28E57FFC0D09A6C597AAFC (void);
// 0x00000343 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_Packet__ConsumeGpuBuffer(System.IntPtr,System.IntPtr&)
extern void UnsafeNativeMethods_mp_Packet__ConsumeGpuBuffer_m89C334399B51256A2F44D1796606990AF4CA8C1D (void);
// 0x00000344 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_Packet__GetGpuBuffer(System.IntPtr,System.IntPtr&)
extern void UnsafeNativeMethods_mp_Packet__GetGpuBuffer_mF8BC0FC62B3F8B2EEF660D2ABDA9F03F728D711B (void);
// 0x00000345 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_Packet__ValidateAsGpuBuffer(System.IntPtr,System.IntPtr&)
extern void UnsafeNativeMethods_mp_Packet__ValidateAsGpuBuffer_m1D904C4703C863F78041212A3F652058EAB5968B (void);
// 0x00000346 System.Void Mediapipe.UnsafeNativeMethods::mp_SharedGpuResources__delete(System.IntPtr)
extern void UnsafeNativeMethods_mp_SharedGpuResources__delete_m0BF26B12CB62E8B39C2BED656B9EE6E9CA52B7EF (void);
// 0x00000347 System.Void Mediapipe.UnsafeNativeMethods::mp_SharedGpuResources__reset(System.IntPtr)
extern void UnsafeNativeMethods_mp_SharedGpuResources__reset_m77D76C52F21147E6E907A5D33EA785FE41BDBB9A (void);
// 0x00000348 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_GpuResources_Create(System.IntPtr&)
extern void UnsafeNativeMethods_mp_GpuResources_Create_m81993A00DBB989A6CA6318779B2032FFC31A45F6 (void);
// 0x00000349 Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_GpuResources_Create__Pv(System.IntPtr,System.IntPtr&)
extern void UnsafeNativeMethods_mp_GpuResources_Create__Pv_m72245C30025BEB85BF129709A9D8EF44142DE4BD (void);
// 0x0000034A System.Void Mediapipe.UnsafeNativeMethods::mp_StatusOrGpuResources__delete(System.IntPtr)
extern void UnsafeNativeMethods_mp_StatusOrGpuResources__delete_mFC0AF350ACBA574EB87AB173FFFB45499E8B3D4B (void);
// 0x0000034B Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_StatusOrGpuResources__status(System.IntPtr,System.IntPtr&)
extern void UnsafeNativeMethods_mp_StatusOrGpuResources__status_m11FFFF3B4A64584B2A6AFDC566946EAA1A7C5FB8 (void);
// 0x0000034C Mediapipe.MpReturnCode Mediapipe.UnsafeNativeMethods::mp_StatusOrGpuResources__value(System.IntPtr,System.IntPtr&)
extern void UnsafeNativeMethods_mp_StatusOrGpuResources__value_m11B00CBAC6CBDC555D3EA34EC21289985BE45A5A (void);
// 0x0000034D Google.Protobuf.Reflection.FileDescriptor Mediapipe.CalculatorReflection::get_Descriptor()
extern void CalculatorReflection_get_Descriptor_mCDC5E5411464321E441FCA5E702FEB9B91F2053A (void);
// 0x0000034E System.Void Mediapipe.CalculatorReflection::.cctor()
extern void CalculatorReflection__cctor_mA59ED6EB26C0896EFFC8B607E5953F24EE892579 (void);
// 0x0000034F Google.Protobuf.MessageParser`1<Mediapipe.ExecutorConfig> Mediapipe.ExecutorConfig::get_Parser()
extern void ExecutorConfig_get_Parser_m4061A739D47DCBF6051EC347E17AB3824F0309EA (void);
// 0x00000350 Google.Protobuf.Reflection.MessageDescriptor Mediapipe.ExecutorConfig::get_Descriptor()
extern void ExecutorConfig_get_Descriptor_mBDD68DADEAA889FE416B0F65B6ACCEC4AE81DB73 (void);
// 0x00000351 Google.Protobuf.Reflection.MessageDescriptor Mediapipe.ExecutorConfig::pb::Google.Protobuf.IMessage.get_Descriptor()
extern void ExecutorConfig_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_mBEC46DF521D3037149CD6112E6E58C12E1FD3FBE (void);
// 0x00000352 System.Void Mediapipe.ExecutorConfig::.ctor()
extern void ExecutorConfig__ctor_m3100C42DF28C84401E6BEF2F84829F2EBBC6A79C (void);
// 0x00000353 System.Void Mediapipe.ExecutorConfig::.ctor(Mediapipe.ExecutorConfig)
extern void ExecutorConfig__ctor_mD8F55C2743A66A7A21FF05F95AC46741582F3F16 (void);
// 0x00000354 Mediapipe.ExecutorConfig Mediapipe.ExecutorConfig::Clone()
extern void ExecutorConfig_Clone_mBCF44B0A1985F000BBEC12B6FF68B32D57D3AB29 (void);
// 0x00000355 System.String Mediapipe.ExecutorConfig::get_Name()
extern void ExecutorConfig_get_Name_mCDD3B5CE8F632D79AEFE09086A0C5E1A6C1CD4AB (void);
// 0x00000356 System.Void Mediapipe.ExecutorConfig::set_Name(System.String)
extern void ExecutorConfig_set_Name_m030BE0B5394956C7D0BA0A7D0BA2623DAF4AB0AF (void);
// 0x00000357 System.String Mediapipe.ExecutorConfig::get_Type()
extern void ExecutorConfig_get_Type_m9D4C633477ECD18E4CCAB745D9090547DBB11748 (void);
// 0x00000358 System.Void Mediapipe.ExecutorConfig::set_Type(System.String)
extern void ExecutorConfig_set_Type_mD55EFA000FE493B28505A7CB9B256722B1154F24 (void);
// 0x00000359 Mediapipe.MediaPipeOptions Mediapipe.ExecutorConfig::get_Options()
extern void ExecutorConfig_get_Options_m47FB86097FA6BE4FA3F16E2C9548046A5A08760A (void);
// 0x0000035A System.Void Mediapipe.ExecutorConfig::set_Options(Mediapipe.MediaPipeOptions)
extern void ExecutorConfig_set_Options_m2F020AF03F033A468C9788B097981EDBB54DE2F1 (void);
// 0x0000035B System.Boolean Mediapipe.ExecutorConfig::Equals(System.Object)
extern void ExecutorConfig_Equals_m3951D8E5FFF9897153DCBB93E5DE556807BBE51D (void);
// 0x0000035C System.Boolean Mediapipe.ExecutorConfig::Equals(Mediapipe.ExecutorConfig)
extern void ExecutorConfig_Equals_mFD6B3AC46F2086E4F1F526630E167EC0C8543318 (void);
// 0x0000035D System.Int32 Mediapipe.ExecutorConfig::GetHashCode()
extern void ExecutorConfig_GetHashCode_m7BDD5B881BD685E1ED277BBBCFD80C4986FFE164 (void);
// 0x0000035E System.String Mediapipe.ExecutorConfig::ToString()
extern void ExecutorConfig_ToString_mF0A8963B6D15984AFB1E8BFF76FABB4F9F173917 (void);
// 0x0000035F System.Void Mediapipe.ExecutorConfig::WriteTo(Google.Protobuf.CodedOutputStream)
extern void ExecutorConfig_WriteTo_mD1274E15A379E9C0DD0C28CE7EB63EAAA74C67D4 (void);
// 0x00000360 System.Int32 Mediapipe.ExecutorConfig::CalculateSize()
extern void ExecutorConfig_CalculateSize_mA55EE68840B8DD1CC6F4AE0974D39CFC0AA61E9C (void);
// 0x00000361 System.Void Mediapipe.ExecutorConfig::MergeFrom(Mediapipe.ExecutorConfig)
extern void ExecutorConfig_MergeFrom_m5C5312A118CD794BA85F9895A164F3B9D27864D8 (void);
// 0x00000362 System.Void Mediapipe.ExecutorConfig::MergeFrom(Google.Protobuf.CodedInputStream)
extern void ExecutorConfig_MergeFrom_mB9D3A9DC69128998567149232D6167740C1D86B0 (void);
// 0x00000363 System.Void Mediapipe.ExecutorConfig::.cctor()
extern void ExecutorConfig__cctor_mB6A2622F6DA7E9C26FD5784793F08474FA116C8F (void);
// 0x00000364 System.Void Mediapipe.ExecutorConfig/<>c::.cctor()
extern void U3CU3Ec__cctor_mF55D6DAFEBD23B5E894241270D8ED12D312F6AC7 (void);
// 0x00000365 System.Void Mediapipe.ExecutorConfig/<>c::.ctor()
extern void U3CU3Ec__ctor_mC2141556671D9CE21521511489272A6D16177989 (void);
// 0x00000366 Mediapipe.ExecutorConfig Mediapipe.ExecutorConfig/<>c::<.cctor>b__35_0()
extern void U3CU3Ec_U3C_cctorU3Eb__35_0_mB2CC60AD96D63C4FE75FD13C750E41EBEE101412 (void);
// 0x00000367 Google.Protobuf.MessageParser`1<Mediapipe.InputCollection> Mediapipe.InputCollection::get_Parser()
extern void InputCollection_get_Parser_mE378D9F9332EFCF03788A8188CA64ED6FB1F7F30 (void);
// 0x00000368 Google.Protobuf.Reflection.MessageDescriptor Mediapipe.InputCollection::get_Descriptor()
extern void InputCollection_get_Descriptor_m53F257A5D4B3C8390B661C52C9BDB22198A32FCE (void);
// 0x00000369 Google.Protobuf.Reflection.MessageDescriptor Mediapipe.InputCollection::pb::Google.Protobuf.IMessage.get_Descriptor()
extern void InputCollection_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m5B17F58E47F5B31C88D78D46FEB89225FDF83680 (void);
// 0x0000036A System.Void Mediapipe.InputCollection::.ctor()
extern void InputCollection__ctor_m6C6B1C4710314CCF5B8B7924C90E035C6FAB2DD8 (void);
// 0x0000036B System.Void Mediapipe.InputCollection::.ctor(Mediapipe.InputCollection)
extern void InputCollection__ctor_m0D27217EBD9D8F66693BFD6643F490520429AA4D (void);
// 0x0000036C Mediapipe.InputCollection Mediapipe.InputCollection::Clone()
extern void InputCollection_Clone_m1B45CE9CCB12659381144DB30162D6975759F471 (void);
// 0x0000036D System.String Mediapipe.InputCollection::get_Name()
extern void InputCollection_get_Name_mDE372674C6096FF2DCCA764D10D98D2FAA5BC3F6 (void);
// 0x0000036E System.Void Mediapipe.InputCollection::set_Name(System.String)
extern void InputCollection_set_Name_mAF13645B09F23DA5A9BB95AE01316CA53A9954EF (void);
// 0x0000036F Google.Protobuf.Collections.RepeatedField`1<System.String> Mediapipe.InputCollection::get_SidePacketName()
extern void InputCollection_get_SidePacketName_m6F5B9464B72B54C2786C5EB8DD78FECC3D5CF2A9 (void);
// 0x00000370 Google.Protobuf.Collections.RepeatedField`1<System.String> Mediapipe.InputCollection::get_ExternalInputName()
extern void InputCollection_get_ExternalInputName_mBF5636DF6433CB626FA7B198305E9A315AF0A540 (void);
// 0x00000371 Mediapipe.InputCollection/Types/InputType Mediapipe.InputCollection::get_InputType()
extern void InputCollection_get_InputType_m01D40EBAC557D3A3440C1E8FC084866F8CC2E7A7 (void);
// 0x00000372 System.Void Mediapipe.InputCollection::set_InputType(Mediapipe.InputCollection/Types/InputType)
extern void InputCollection_set_InputType_m046259B7E53CF8711B283C6B32EF40644A50F54C (void);
// 0x00000373 System.String Mediapipe.InputCollection::get_FileName()
extern void InputCollection_get_FileName_m37189943C7703CC0BDEFA64D296DC0AD416E0B82 (void);
// 0x00000374 System.Void Mediapipe.InputCollection::set_FileName(System.String)
extern void InputCollection_set_FileName_m90482CDBF38046F894E3EE6816DF83DDDAFC3F85 (void);
// 0x00000375 System.Boolean Mediapipe.InputCollection::Equals(System.Object)
extern void InputCollection_Equals_mFED987F83BABE5D3B89DDA35313DD18A2746CEBB (void);
// 0x00000376 System.Boolean Mediapipe.InputCollection::Equals(Mediapipe.InputCollection)
extern void InputCollection_Equals_m240639A5CB435A8E02FD15D41B30E9AFB7C9D2FB (void);
// 0x00000377 System.Int32 Mediapipe.InputCollection::GetHashCode()
extern void InputCollection_GetHashCode_mAECDBE9B9032BA48A73EC8D59C4ED5F1D473E5B7 (void);
// 0x00000378 System.String Mediapipe.InputCollection::ToString()
extern void InputCollection_ToString_m7FA0413087B5A0AD62F527DD3F15ECDCB9981DEF (void);
// 0x00000379 System.Void Mediapipe.InputCollection::WriteTo(Google.Protobuf.CodedOutputStream)
extern void InputCollection_WriteTo_m606B5E61E0DD7AC8D05F5C64A3EDD68625ACA63C (void);
// 0x0000037A System.Int32 Mediapipe.InputCollection::CalculateSize()
extern void InputCollection_CalculateSize_mB74CC608EB1D546473A2A7D93F6037CFA54E60FD (void);
// 0x0000037B System.Void Mediapipe.InputCollection::MergeFrom(Mediapipe.InputCollection)
extern void InputCollection_MergeFrom_mD3C06AAE9303FC3CD361361748B7CD0BE5C46D38 (void);
// 0x0000037C System.Void Mediapipe.InputCollection::MergeFrom(Google.Protobuf.CodedInputStream)
extern void InputCollection_MergeFrom_mFADA258A81187638466BFEF0AFC6C8351A94374B (void);
// 0x0000037D System.Void Mediapipe.InputCollection::.cctor()
extern void InputCollection__cctor_mBBE11626BE60F7C2C8892719A0DACC27BE20143D (void);
// 0x0000037E System.Void Mediapipe.InputCollection/<>c::.cctor()
extern void U3CU3Ec__cctor_m775626A98B13AA2C3AF7D4B66B3BBE5B35960637 (void);
// 0x0000037F System.Void Mediapipe.InputCollection/<>c::.ctor()
extern void U3CU3Ec__ctor_m4519A711962554F775A3B093EABF5B4C3AC7DB6E (void);
// 0x00000380 Mediapipe.InputCollection Mediapipe.InputCollection/<>c::<.cctor>b__46_0()
extern void U3CU3Ec_U3C_cctorU3Eb__46_0_mE37B8A95569CD0FC845A9D68DDBD2612B13205DC (void);
// 0x00000381 Google.Protobuf.MessageParser`1<Mediapipe.InputCollectionSet> Mediapipe.InputCollectionSet::get_Parser()
extern void InputCollectionSet_get_Parser_m3DEC565028824E563F99C174DB57A62670D0A5BD (void);
// 0x00000382 Google.Protobuf.Reflection.MessageDescriptor Mediapipe.InputCollectionSet::get_Descriptor()
extern void InputCollectionSet_get_Descriptor_m234566FE3F857EA8CF17D175DA30CCA9D7BA74DA (void);
// 0x00000383 Google.Protobuf.Reflection.MessageDescriptor Mediapipe.InputCollectionSet::pb::Google.Protobuf.IMessage.get_Descriptor()
extern void InputCollectionSet_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_mECA590FBA2BD56B8274FCB46219A1B321EACB1B6 (void);
// 0x00000384 System.Void Mediapipe.InputCollectionSet::.ctor()
extern void InputCollectionSet__ctor_m106104988333DB6E965B4966687686D1085762F5 (void);
// 0x00000385 System.Void Mediapipe.InputCollectionSet::.ctor(Mediapipe.InputCollectionSet)
extern void InputCollectionSet__ctor_m5B651C10C9348EA4E9BAAEED0053D0D7A17CD82C (void);
// 0x00000386 Mediapipe.InputCollectionSet Mediapipe.InputCollectionSet::Clone()
extern void InputCollectionSet_Clone_mE409D46CDF2E45AFCF1BCCFA1245ED50361A972D (void);
// 0x00000387 Google.Protobuf.Collections.RepeatedField`1<Mediapipe.InputCollection> Mediapipe.InputCollectionSet::get_InputCollection()
extern void InputCollectionSet_get_InputCollection_mC05A051CC8BCA271CFE62488B2B8C053955619DD (void);
// 0x00000388 System.Boolean Mediapipe.InputCollectionSet::Equals(System.Object)
extern void InputCollectionSet_Equals_mD0F5B94B7B317360CA85154981CC2B5193459D97 (void);
// 0x00000389 System.Boolean Mediapipe.InputCollectionSet::Equals(Mediapipe.InputCollectionSet)
extern void InputCollectionSet_Equals_mD2DF8C4AB1E38FE7EB5F3AE5991D7C2E7B686019 (void);
// 0x0000038A System.Int32 Mediapipe.InputCollectionSet::GetHashCode()
extern void InputCollectionSet_GetHashCode_m6AC079DDE2E44C5212E7E62F1FF43C86B2B37D5E (void);
// 0x0000038B System.String Mediapipe.InputCollectionSet::ToString()
extern void InputCollectionSet_ToString_m9CABAE3D201DE47B317FA7DC708336FFF7E16607 (void);
// 0x0000038C System.Void Mediapipe.InputCollectionSet::WriteTo(Google.Protobuf.CodedOutputStream)
extern void InputCollectionSet_WriteTo_m929290F3104395CB0869AADAC6EAC99A02107476 (void);
// 0x0000038D System.Int32 Mediapipe.InputCollectionSet::CalculateSize()
extern void InputCollectionSet_CalculateSize_m3035110C513A4AC4869481684000D6E163838F20 (void);
// 0x0000038E System.Void Mediapipe.InputCollectionSet::MergeFrom(Mediapipe.InputCollectionSet)
extern void InputCollectionSet_MergeFrom_m57652BBC66362FB0B8E52A4C24A972069DCA12A3 (void);
// 0x0000038F System.Void Mediapipe.InputCollectionSet::MergeFrom(Google.Protobuf.CodedInputStream)
extern void InputCollectionSet_MergeFrom_m3ECEAC1E1ED1FF19AE13930016EE8AF9744C6C32 (void);
// 0x00000390 System.Void Mediapipe.InputCollectionSet::.cctor()
extern void InputCollectionSet__cctor_mC175F4CC4961FE477BC6F94B31F6777BB2B564A0 (void);
// 0x00000391 System.Void Mediapipe.InputCollectionSet/<>c::.cctor()
extern void U3CU3Ec__cctor_mAC32A0CDE2B86354240EF13EA670CD7AE3FB5F67 (void);
// 0x00000392 System.Void Mediapipe.InputCollectionSet/<>c::.ctor()
extern void U3CU3Ec__ctor_m0757AF253EDE03DEEA8E6285493C7BB08EC02170 (void);
// 0x00000393 Mediapipe.InputCollectionSet Mediapipe.InputCollectionSet/<>c::<.cctor>b__25_0()
extern void U3CU3Ec_U3C_cctorU3Eb__25_0_m5AC5917E1E50A6FB568D1FA34C9EFD309AD37692 (void);
// 0x00000394 Google.Protobuf.MessageParser`1<Mediapipe.InputStreamInfo> Mediapipe.InputStreamInfo::get_Parser()
extern void InputStreamInfo_get_Parser_m29EEEEE0106399720E60958EFE1DE729B252297B (void);
// 0x00000395 Google.Protobuf.Reflection.MessageDescriptor Mediapipe.InputStreamInfo::get_Descriptor()
extern void InputStreamInfo_get_Descriptor_m522BCB77CE097CFA0B2926952ECAC0B025EF1257 (void);
// 0x00000396 Google.Protobuf.Reflection.MessageDescriptor Mediapipe.InputStreamInfo::pb::Google.Protobuf.IMessage.get_Descriptor()
extern void InputStreamInfo_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m1C7871CD59A7B9AC8F5FEEA72AAF44A9C903584C (void);
// 0x00000397 System.Void Mediapipe.InputStreamInfo::.ctor()
extern void InputStreamInfo__ctor_mD1115B096AA44CBE3BAE01F934CB1DC0E2C8AA78 (void);
// 0x00000398 System.Void Mediapipe.InputStreamInfo::.ctor(Mediapipe.InputStreamInfo)
extern void InputStreamInfo__ctor_m1B40C1CDA73DFEAD87E29B99BCEA761AE4C7A703 (void);
// 0x00000399 Mediapipe.InputStreamInfo Mediapipe.InputStreamInfo::Clone()
extern void InputStreamInfo_Clone_mDFE3D66297DD000E2BA4F70D7856F18919BB7D44 (void);
// 0x0000039A System.String Mediapipe.InputStreamInfo::get_TagIndex()
extern void InputStreamInfo_get_TagIndex_m2DFDC9D8A61379DD1F3DF69E0412B7CBE5DEF833 (void);
// 0x0000039B System.Void Mediapipe.InputStreamInfo::set_TagIndex(System.String)
extern void InputStreamInfo_set_TagIndex_m499BCC10AE86C87A0E24617B6CEABD0CFB18FF2D (void);
// 0x0000039C System.Boolean Mediapipe.InputStreamInfo::get_BackEdge()
extern void InputStreamInfo_get_BackEdge_m7116667E66A9D5F7E01BE058DAF8D6D0875B75E2 (void);
// 0x0000039D System.Void Mediapipe.InputStreamInfo::set_BackEdge(System.Boolean)
extern void InputStreamInfo_set_BackEdge_m619978F90472531E677D24A0DBE7A2578DA1D868 (void);
// 0x0000039E System.Boolean Mediapipe.InputStreamInfo::Equals(System.Object)
extern void InputStreamInfo_Equals_mD3672C103FBD9DF61B4EB0D8DD7B63EF8863A7C3 (void);
// 0x0000039F System.Boolean Mediapipe.InputStreamInfo::Equals(Mediapipe.InputStreamInfo)
extern void InputStreamInfo_Equals_m936804A131005237445BABB5F4EDFC06C448B018 (void);
// 0x000003A0 System.Int32 Mediapipe.InputStreamInfo::GetHashCode()
extern void InputStreamInfo_GetHashCode_m3AE4587C347B5910CAF5CC12E662B2064136D676 (void);
// 0x000003A1 System.String Mediapipe.InputStreamInfo::ToString()
extern void InputStreamInfo_ToString_m7F4E5F79EAC6EFBC730259B570B113870ACEA553 (void);
// 0x000003A2 System.Void Mediapipe.InputStreamInfo::WriteTo(Google.Protobuf.CodedOutputStream)
extern void InputStreamInfo_WriteTo_mD01FACD335355F68B1D530EA10BF2E346EB45567 (void);
// 0x000003A3 System.Int32 Mediapipe.InputStreamInfo::CalculateSize()
extern void InputStreamInfo_CalculateSize_mD3CDD8A026DEA91C9FE050645038DD708196E41E (void);
// 0x000003A4 System.Void Mediapipe.InputStreamInfo::MergeFrom(Mediapipe.InputStreamInfo)
extern void InputStreamInfo_MergeFrom_m9AE0F0C70340F4B1399211E4364CC883F752D325 (void);
// 0x000003A5 System.Void Mediapipe.InputStreamInfo::MergeFrom(Google.Protobuf.CodedInputStream)
extern void InputStreamInfo_MergeFrom_mD104C197A6E64A40CB866B1ADC51E93F42761B5E (void);
// 0x000003A6 System.Void Mediapipe.InputStreamInfo::.cctor()
extern void InputStreamInfo__cctor_m49F60A4A40008C1C7DAEEC941126B8D134974888 (void);
// 0x000003A7 System.Void Mediapipe.InputStreamInfo/<>c::.cctor()
extern void U3CU3Ec__cctor_mAA9349BC4F34FC226908D2CFAE77AC9D0D85E723 (void);
// 0x000003A8 System.Void Mediapipe.InputStreamInfo/<>c::.ctor()
extern void U3CU3Ec__ctor_m6068A300BACB163E7D442C7FD0FD6A809D9E7A78 (void);
// 0x000003A9 Mediapipe.InputStreamInfo Mediapipe.InputStreamInfo/<>c::<.cctor>b__30_0()
extern void U3CU3Ec_U3C_cctorU3Eb__30_0_m6DFD9BB113EC492435F9DA60F2655AFAB899D070 (void);
// 0x000003AA Google.Protobuf.MessageParser`1<Mediapipe.ProfilerConfig> Mediapipe.ProfilerConfig::get_Parser()
extern void ProfilerConfig_get_Parser_mDC3680A806A01DC409BA9CB9CB0E0F0E82FD098C (void);
// 0x000003AB Google.Protobuf.Reflection.MessageDescriptor Mediapipe.ProfilerConfig::get_Descriptor()
extern void ProfilerConfig_get_Descriptor_m45D4BC75A233D5863820E0519FBD549ACB4A5C8B (void);
// 0x000003AC Google.Protobuf.Reflection.MessageDescriptor Mediapipe.ProfilerConfig::pb::Google.Protobuf.IMessage.get_Descriptor()
extern void ProfilerConfig_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m354E93AA3D4D9902C2302405BA82693B9C0E80C5 (void);
// 0x000003AD System.Void Mediapipe.ProfilerConfig::.ctor()
extern void ProfilerConfig__ctor_m023B7A29789ACE07320D0B31C8CA499115617D79 (void);
// 0x000003AE System.Void Mediapipe.ProfilerConfig::.ctor(Mediapipe.ProfilerConfig)
extern void ProfilerConfig__ctor_m262E9327CC6CA09F9622D776162028033D1BE963 (void);
// 0x000003AF Mediapipe.ProfilerConfig Mediapipe.ProfilerConfig::Clone()
extern void ProfilerConfig_Clone_m9412AE0F8B8C1AF838F6267D697F1ED44148B3D5 (void);
// 0x000003B0 System.Int64 Mediapipe.ProfilerConfig::get_HistogramIntervalSizeUsec()
extern void ProfilerConfig_get_HistogramIntervalSizeUsec_m6D91E433129C5662A8BFDC98BE3E1E12FD2B0CF0 (void);
// 0x000003B1 System.Void Mediapipe.ProfilerConfig::set_HistogramIntervalSizeUsec(System.Int64)
extern void ProfilerConfig_set_HistogramIntervalSizeUsec_mD21DBA09EE8366CEF3D89A419A85EEFEF5355C9A (void);
// 0x000003B2 System.Int64 Mediapipe.ProfilerConfig::get_NumHistogramIntervals()
extern void ProfilerConfig_get_NumHistogramIntervals_mB094148194DDAD421C145C61F8AAC4BE518EAF0B (void);
// 0x000003B3 System.Void Mediapipe.ProfilerConfig::set_NumHistogramIntervals(System.Int64)
extern void ProfilerConfig_set_NumHistogramIntervals_m63112C698F0F95060925ADBC162C0D3D1D3C10C4 (void);
// 0x000003B4 System.Boolean Mediapipe.ProfilerConfig::get_EnableInputOutputLatency()
extern void ProfilerConfig_get_EnableInputOutputLatency_mEBD9F1BB647CC19BCBEC981FA54C22CECDC9536F (void);
// 0x000003B5 System.Void Mediapipe.ProfilerConfig::set_EnableInputOutputLatency(System.Boolean)
extern void ProfilerConfig_set_EnableInputOutputLatency_m0D0C6D17E5901859534BC1C76E9ED8C87FF00A1B (void);
// 0x000003B6 System.Boolean Mediapipe.ProfilerConfig::get_EnableProfiler()
extern void ProfilerConfig_get_EnableProfiler_m6370DE235049E6426DA0950488C35C4A2C199949 (void);
// 0x000003B7 System.Void Mediapipe.ProfilerConfig::set_EnableProfiler(System.Boolean)
extern void ProfilerConfig_set_EnableProfiler_m3D3EAB2ABB953A6A48D76EAFFD5407F7D7B27408 (void);
// 0x000003B8 System.Boolean Mediapipe.ProfilerConfig::get_EnableStreamLatency()
extern void ProfilerConfig_get_EnableStreamLatency_m98BF49C9827E6A20CD87B8A6224C671FC9EDE895 (void);
// 0x000003B9 System.Void Mediapipe.ProfilerConfig::set_EnableStreamLatency(System.Boolean)
extern void ProfilerConfig_set_EnableStreamLatency_m8ECB2C951B166BE71D746D1B04B4ADF63DBA2151 (void);
// 0x000003BA System.Boolean Mediapipe.ProfilerConfig::get_UsePacketTimestampForAddedPacket()
extern void ProfilerConfig_get_UsePacketTimestampForAddedPacket_m18282A570D68BD141087F3F2D3FDDD7DA2F580D4 (void);
// 0x000003BB System.Void Mediapipe.ProfilerConfig::set_UsePacketTimestampForAddedPacket(System.Boolean)
extern void ProfilerConfig_set_UsePacketTimestampForAddedPacket_m0CCCCBDDB2434D8B2A8DC7AE3462289C12F3D0CD (void);
// 0x000003BC System.Int64 Mediapipe.ProfilerConfig::get_TraceLogCapacity()
extern void ProfilerConfig_get_TraceLogCapacity_m33F9AE0545DF9C0E5CE9BBC60A6A6C5AF744FB65 (void);
// 0x000003BD System.Void Mediapipe.ProfilerConfig::set_TraceLogCapacity(System.Int64)
extern void ProfilerConfig_set_TraceLogCapacity_mFDE761A2BFB9A850CAFA2FFC9B229B7F7C130612 (void);
// 0x000003BE Google.Protobuf.Collections.RepeatedField`1<System.Int32> Mediapipe.ProfilerConfig::get_TraceEventTypesDisabled()
extern void ProfilerConfig_get_TraceEventTypesDisabled_m138212D8581E0D2A4F1AE7D5554CB319107D2437 (void);
// 0x000003BF System.String Mediapipe.ProfilerConfig::get_TraceLogPath()
extern void ProfilerConfig_get_TraceLogPath_m8A2EE82AAC47E9EC2BC0343DA658940E9BA6D1AE (void);
// 0x000003C0 System.Void Mediapipe.ProfilerConfig::set_TraceLogPath(System.String)
extern void ProfilerConfig_set_TraceLogPath_m38F963E88EBE4BF9CC9FC459BA314A056AB3082B (void);
// 0x000003C1 System.Int32 Mediapipe.ProfilerConfig::get_TraceLogCount()
extern void ProfilerConfig_get_TraceLogCount_m05C36326E91640453E292A0F8F2AB5437026B071 (void);
// 0x000003C2 System.Void Mediapipe.ProfilerConfig::set_TraceLogCount(System.Int32)
extern void ProfilerConfig_set_TraceLogCount_m239C502EB594621DCB8B0384DED343B7C9761B73 (void);
// 0x000003C3 System.Int64 Mediapipe.ProfilerConfig::get_TraceLogIntervalUsec()
extern void ProfilerConfig_get_TraceLogIntervalUsec_m9793B4062D176C4063A08813D4914FE6DC1BB943 (void);
// 0x000003C4 System.Void Mediapipe.ProfilerConfig::set_TraceLogIntervalUsec(System.Int64)
extern void ProfilerConfig_set_TraceLogIntervalUsec_mA68DC25CF8409BE740B33AAAD986DA96E5CFE961 (void);
// 0x000003C5 System.Int64 Mediapipe.ProfilerConfig::get_TraceLogMarginUsec()
extern void ProfilerConfig_get_TraceLogMarginUsec_mF9B6487ADDF8B789026A27F3CE43BCB86B3A6695 (void);
// 0x000003C6 System.Void Mediapipe.ProfilerConfig::set_TraceLogMarginUsec(System.Int64)
extern void ProfilerConfig_set_TraceLogMarginUsec_m54122925CADA5CEF944B50BD8EDD09F5B8B09A95 (void);
// 0x000003C7 System.Boolean Mediapipe.ProfilerConfig::get_TraceLogDurationEvents()
extern void ProfilerConfig_get_TraceLogDurationEvents_m8C35A76E9947A90E9469ED45AA523DC5E5D54BE9 (void);
// 0x000003C8 System.Void Mediapipe.ProfilerConfig::set_TraceLogDurationEvents(System.Boolean)
extern void ProfilerConfig_set_TraceLogDurationEvents_m0D3E0D43649B7C7F2CF18B6FFF2D520EE7DD9347 (void);
// 0x000003C9 System.Int32 Mediapipe.ProfilerConfig::get_TraceLogIntervalCount()
extern void ProfilerConfig_get_TraceLogIntervalCount_mCDAD806631E8824097362B69D313800F629D81FF (void);
// 0x000003CA System.Void Mediapipe.ProfilerConfig::set_TraceLogIntervalCount(System.Int32)
extern void ProfilerConfig_set_TraceLogIntervalCount_mBA8BC1B5CCF34D31AF7503DC9426317E3CB68F27 (void);
// 0x000003CB System.Boolean Mediapipe.ProfilerConfig::get_TraceLogDisabled()
extern void ProfilerConfig_get_TraceLogDisabled_m04F92FC63CCC927214FEA89C9CE5A07F6B353E54 (void);
// 0x000003CC System.Void Mediapipe.ProfilerConfig::set_TraceLogDisabled(System.Boolean)
extern void ProfilerConfig_set_TraceLogDisabled_m1A04F77DA8620C6AB5C0CFF6A2027EF882B3495B (void);
// 0x000003CD System.Boolean Mediapipe.ProfilerConfig::get_TraceEnabled()
extern void ProfilerConfig_get_TraceEnabled_mA8A9B99D538EF79A8B1DFC93CA2C48A4DEDD251C (void);
// 0x000003CE System.Void Mediapipe.ProfilerConfig::set_TraceEnabled(System.Boolean)
extern void ProfilerConfig_set_TraceEnabled_m8DC1CBC71241FBF4C147B2B9FFE8423A505A0761 (void);
// 0x000003CF System.Boolean Mediapipe.ProfilerConfig::get_TraceLogInstantEvents()
extern void ProfilerConfig_get_TraceLogInstantEvents_mB12FA539BAA5FD750193D355F42FE76E81FB9BDC (void);
// 0x000003D0 System.Void Mediapipe.ProfilerConfig::set_TraceLogInstantEvents(System.Boolean)
extern void ProfilerConfig_set_TraceLogInstantEvents_mEF5BF40D40A904DF92022204AD51FCDE5C06C840 (void);
// 0x000003D1 System.Boolean Mediapipe.ProfilerConfig::Equals(System.Object)
extern void ProfilerConfig_Equals_mB3F233DC0F6A2B5D3A0A3E9E77F192F684323BD3 (void);
// 0x000003D2 System.Boolean Mediapipe.ProfilerConfig::Equals(Mediapipe.ProfilerConfig)
extern void ProfilerConfig_Equals_m8F7A20EE6CD5D39FCC8B178E93A994BE8E2E49B2 (void);
// 0x000003D3 System.Int32 Mediapipe.ProfilerConfig::GetHashCode()
extern void ProfilerConfig_GetHashCode_m54E95CD9692BE7C31B86676C97728DD0E633F982 (void);
// 0x000003D4 System.String Mediapipe.ProfilerConfig::ToString()
extern void ProfilerConfig_ToString_m3B748ED748B01176BCB4494E2D750DAD217AC2A9 (void);
// 0x000003D5 System.Void Mediapipe.ProfilerConfig::WriteTo(Google.Protobuf.CodedOutputStream)
extern void ProfilerConfig_WriteTo_m358D63D2EC966F259D61B184EFE314BD91EFC003 (void);
// 0x000003D6 System.Int32 Mediapipe.ProfilerConfig::CalculateSize()
extern void ProfilerConfig_CalculateSize_mAD4E24F9EC25953D971CBFDCA5B9E122B8D29EED (void);
// 0x000003D7 System.Void Mediapipe.ProfilerConfig::MergeFrom(Mediapipe.ProfilerConfig)
extern void ProfilerConfig_MergeFrom_m832DCC37551A1090377D636FBF3741DD738508C8 (void);
// 0x000003D8 System.Void Mediapipe.ProfilerConfig::MergeFrom(Google.Protobuf.CodedInputStream)
extern void ProfilerConfig_MergeFrom_m83749E2EB94F44989249949BF89412B0BE6EEF1B (void);
// 0x000003D9 System.Void Mediapipe.ProfilerConfig::.cctor()
extern void ProfilerConfig__cctor_m67023CC8F429220EC22D3415EBA8A5F458FA5C78 (void);
// 0x000003DA System.Void Mediapipe.ProfilerConfig/<>c::.cctor()
extern void U3CU3Ec__cctor_m790ED7413C6278199A79F0F96D1EC732307F5A67 (void);
// 0x000003DB System.Void Mediapipe.ProfilerConfig/<>c::.ctor()
extern void U3CU3Ec__ctor_mD6DCE79502F7D20A567B9CC11BAB432934475D42 (void);
// 0x000003DC Mediapipe.ProfilerConfig Mediapipe.ProfilerConfig/<>c::<.cctor>b__105_0()
extern void U3CU3Ec_U3C_cctorU3Eb__105_0_mF0A38142138B6391679CCCD4E7C69A42D23B1403 (void);
// 0x000003DD Google.Protobuf.MessageParser`1<Mediapipe.CalculatorGraphConfig> Mediapipe.CalculatorGraphConfig::get_Parser()
extern void CalculatorGraphConfig_get_Parser_m4A079EC8E8CAAD2D0103348A0B7BCDB227430C32 (void);
// 0x000003DE Google.Protobuf.Reflection.MessageDescriptor Mediapipe.CalculatorGraphConfig::get_Descriptor()
extern void CalculatorGraphConfig_get_Descriptor_m8D7BBE111C92901AE6C097AD989429407C1F665C (void);
// 0x000003DF Google.Protobuf.Reflection.MessageDescriptor Mediapipe.CalculatorGraphConfig::pb::Google.Protobuf.IMessage.get_Descriptor()
extern void CalculatorGraphConfig_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m06B01AB068DC73424EA701B7C22D5F72E1B71561 (void);
// 0x000003E0 System.Void Mediapipe.CalculatorGraphConfig::.ctor()
extern void CalculatorGraphConfig__ctor_m2B2B9C3D725A811963E37BFACE3320D1B01AAD6B (void);
// 0x000003E1 System.Void Mediapipe.CalculatorGraphConfig::.ctor(Mediapipe.CalculatorGraphConfig)
extern void CalculatorGraphConfig__ctor_mC7561D31FBB0998EA48446E954D238BB32F6D258 (void);
// 0x000003E2 Mediapipe.CalculatorGraphConfig Mediapipe.CalculatorGraphConfig::Clone()
extern void CalculatorGraphConfig_Clone_m17FBA506A44FF42CB08BBA16B66D753A23FD7F8E (void);
// 0x000003E3 Google.Protobuf.Collections.RepeatedField`1<Mediapipe.CalculatorGraphConfig/Types/Node> Mediapipe.CalculatorGraphConfig::get_Node()
extern void CalculatorGraphConfig_get_Node_m321554CDAD36FEDC9405A18B6A66EC9BE760ED0B (void);
// 0x000003E4 Google.Protobuf.Collections.RepeatedField`1<Mediapipe.PacketFactoryConfig> Mediapipe.CalculatorGraphConfig::get_PacketFactory()
extern void CalculatorGraphConfig_get_PacketFactory_m273E2DE8AC98B7F5CD1AD128CB48AEE53D72A88B (void);
// 0x000003E5 Google.Protobuf.Collections.RepeatedField`1<Mediapipe.PacketGeneratorConfig> Mediapipe.CalculatorGraphConfig::get_PacketGenerator()
extern void CalculatorGraphConfig_get_PacketGenerator_m69B3E7B6CB9B1493B8767D1C3AC0BB55DF38F977 (void);
// 0x000003E6 System.Int32 Mediapipe.CalculatorGraphConfig::get_NumThreads()
extern void CalculatorGraphConfig_get_NumThreads_m6A39F58BDA93AFAE00A004C84E3493636EAF87F5 (void);
// 0x000003E7 System.Void Mediapipe.CalculatorGraphConfig::set_NumThreads(System.Int32)
extern void CalculatorGraphConfig_set_NumThreads_mD0064766BD1FE929CC790875C973DC449C8C432B (void);
// 0x000003E8 Google.Protobuf.Collections.RepeatedField`1<Mediapipe.StatusHandlerConfig> Mediapipe.CalculatorGraphConfig::get_StatusHandler()
extern void CalculatorGraphConfig_get_StatusHandler_m743365040F37E3C159D6BECE1663E0D6E9F1B9D4 (void);
// 0x000003E9 Google.Protobuf.Collections.RepeatedField`1<System.String> Mediapipe.CalculatorGraphConfig::get_InputStream()
extern void CalculatorGraphConfig_get_InputStream_mBB8EA0317D48A82B675083F8B9051A23AC0A3F44 (void);
// 0x000003EA Google.Protobuf.Collections.RepeatedField`1<System.String> Mediapipe.CalculatorGraphConfig::get_OutputStream()
extern void CalculatorGraphConfig_get_OutputStream_m3BD29DC90D9B7A21DDE753EF4ED1E3838D9472BA (void);
// 0x000003EB Google.Protobuf.Collections.RepeatedField`1<System.String> Mediapipe.CalculatorGraphConfig::get_InputSidePacket()
extern void CalculatorGraphConfig_get_InputSidePacket_mBC81A12B242533F7A6D150F599C3C9D65CDEDEE2 (void);
// 0x000003EC Google.Protobuf.Collections.RepeatedField`1<System.String> Mediapipe.CalculatorGraphConfig::get_OutputSidePacket()
extern void CalculatorGraphConfig_get_OutputSidePacket_mABD36DE826A9956AB94F62ECB0869787802DE7DC (void);
// 0x000003ED System.Int32 Mediapipe.CalculatorGraphConfig::get_MaxQueueSize()
extern void CalculatorGraphConfig_get_MaxQueueSize_mAA37C40F789D6FD921090A133602AA7C623855FA (void);
// 0x000003EE System.Void Mediapipe.CalculatorGraphConfig::set_MaxQueueSize(System.Int32)
extern void CalculatorGraphConfig_set_MaxQueueSize_m4D8AB2D205D8AFC0FCB0372935FF294608D8A3EC (void);
// 0x000003EF System.Boolean Mediapipe.CalculatorGraphConfig::get_ReportDeadlock()
extern void CalculatorGraphConfig_get_ReportDeadlock_mD7E9AA636A64D20F511DCA8DA40722006E60ABCF (void);
// 0x000003F0 System.Void Mediapipe.CalculatorGraphConfig::set_ReportDeadlock(System.Boolean)
extern void CalculatorGraphConfig_set_ReportDeadlock_mC86981168F02A6C2B86CEAD791ADE82F37BB22D4 (void);
// 0x000003F1 Mediapipe.InputStreamHandlerConfig Mediapipe.CalculatorGraphConfig::get_InputStreamHandler()
extern void CalculatorGraphConfig_get_InputStreamHandler_m3D3C8636240AACE7647CCA94C9D7BDCC0311A1D4 (void);
// 0x000003F2 System.Void Mediapipe.CalculatorGraphConfig::set_InputStreamHandler(Mediapipe.InputStreamHandlerConfig)
extern void CalculatorGraphConfig_set_InputStreamHandler_m38F86ED4E5D6A6FD8C6CF0F6AF0C7507BADC137A (void);
// 0x000003F3 Mediapipe.OutputStreamHandlerConfig Mediapipe.CalculatorGraphConfig::get_OutputStreamHandler()
extern void CalculatorGraphConfig_get_OutputStreamHandler_mC802C414AB6F0D4BF2F154FD58814D8BE70948CF (void);
// 0x000003F4 System.Void Mediapipe.CalculatorGraphConfig::set_OutputStreamHandler(Mediapipe.OutputStreamHandlerConfig)
extern void CalculatorGraphConfig_set_OutputStreamHandler_mAA9AAC5111D30D2D0BA230E7D05C6DB295CB2C3A (void);
// 0x000003F5 Google.Protobuf.Collections.RepeatedField`1<Mediapipe.ExecutorConfig> Mediapipe.CalculatorGraphConfig::get_Executor()
extern void CalculatorGraphConfig_get_Executor_mFB5C9FA0B856C97131B7A3C2C0BC8F995D779BF4 (void);
// 0x000003F6 Mediapipe.ProfilerConfig Mediapipe.CalculatorGraphConfig::get_ProfilerConfig()
extern void CalculatorGraphConfig_get_ProfilerConfig_m983F9645276D8632486E927C5429AF98558B4A60 (void);
// 0x000003F7 System.Void Mediapipe.CalculatorGraphConfig::set_ProfilerConfig(Mediapipe.ProfilerConfig)
extern void CalculatorGraphConfig_set_ProfilerConfig_mB1FF19F6A1B86A3D51E5B09663EC1A41555ACDEE (void);
// 0x000003F8 System.String Mediapipe.CalculatorGraphConfig::get_Package()
extern void CalculatorGraphConfig_get_Package_mAB118379EA357AD0EF33CBA05CEBD93657E5BCEB (void);
// 0x000003F9 System.Void Mediapipe.CalculatorGraphConfig::set_Package(System.String)
extern void CalculatorGraphConfig_set_Package_m0D1CA3C3DB1518FECAB2F2FE03F689CAD6C54EA5 (void);
// 0x000003FA System.String Mediapipe.CalculatorGraphConfig::get_Type()
extern void CalculatorGraphConfig_get_Type_m31C5992F7EC58444CD9558F43B966B065959B42C (void);
// 0x000003FB System.Void Mediapipe.CalculatorGraphConfig::set_Type(System.String)
extern void CalculatorGraphConfig_set_Type_m322E27DA217F05E23F477EB05F65EBD02E9481F5 (void);
// 0x000003FC Mediapipe.MediaPipeOptions Mediapipe.CalculatorGraphConfig::get_Options()
extern void CalculatorGraphConfig_get_Options_m71FCD390FB456C70704A92CF88DAEE4E76AD5EB3 (void);
// 0x000003FD System.Void Mediapipe.CalculatorGraphConfig::set_Options(Mediapipe.MediaPipeOptions)
extern void CalculatorGraphConfig_set_Options_m13AD3E5B15DAB480E5A357BA396E1ED9EDDDFE48 (void);
// 0x000003FE Google.Protobuf.Collections.RepeatedField`1<Google.Protobuf.WellKnownTypes.Any> Mediapipe.CalculatorGraphConfig::get_GraphOptions()
extern void CalculatorGraphConfig_get_GraphOptions_mECAFC93F8B1268731A1707A6ED6FFC1884AC8D82 (void);
// 0x000003FF System.Boolean Mediapipe.CalculatorGraphConfig::Equals(System.Object)
extern void CalculatorGraphConfig_Equals_m64171AE8626030825A893CF84181F3DB88686A78 (void);
// 0x00000400 System.Boolean Mediapipe.CalculatorGraphConfig::Equals(Mediapipe.CalculatorGraphConfig)
extern void CalculatorGraphConfig_Equals_m34E76CADA083771B042327F2E6A05098D82BC128 (void);
// 0x00000401 System.Int32 Mediapipe.CalculatorGraphConfig::GetHashCode()
extern void CalculatorGraphConfig_GetHashCode_mB45EBF4282A3719E6557603FD57749C19E2315C2 (void);
// 0x00000402 System.String Mediapipe.CalculatorGraphConfig::ToString()
extern void CalculatorGraphConfig_ToString_mA48B2E2730D472DDD57D0D36B0A0350E05BEE497 (void);
// 0x00000403 System.Void Mediapipe.CalculatorGraphConfig::WriteTo(Google.Protobuf.CodedOutputStream)
extern void CalculatorGraphConfig_WriteTo_m0937B7E7FB01FA089C7EABB73DF1ACCCDB08D5EC (void);
// 0x00000404 System.Int32 Mediapipe.CalculatorGraphConfig::CalculateSize()
extern void CalculatorGraphConfig_CalculateSize_m47318B281CC424A1787F9864750F2A6056BA8EEC (void);
// 0x00000405 System.Void Mediapipe.CalculatorGraphConfig::MergeFrom(Mediapipe.CalculatorGraphConfig)
extern void CalculatorGraphConfig_MergeFrom_m6A80FA1E5F6CD141888C842AD93B8979616CDB2F (void);
// 0x00000406 System.Void Mediapipe.CalculatorGraphConfig::MergeFrom(Google.Protobuf.CodedInputStream)
extern void CalculatorGraphConfig_MergeFrom_m75F230C7A43154402BAF91F2D7837F2D0C79BA4E (void);
// 0x00000407 System.Void Mediapipe.CalculatorGraphConfig::.cctor()
extern void CalculatorGraphConfig__cctor_mD017EEE41436B98F035D39AD96FF27B6712B7DC9 (void);
// 0x00000408 Google.Protobuf.MessageParser`1<Mediapipe.CalculatorGraphConfig/Types/Node> Mediapipe.CalculatorGraphConfig/Types/Node::get_Parser()
extern void Node_get_Parser_mF9303F20AC373AE8D8C58D0154E2DC2CEB82D5E2 (void);
// 0x00000409 Google.Protobuf.Reflection.MessageDescriptor Mediapipe.CalculatorGraphConfig/Types/Node::get_Descriptor()
extern void Node_get_Descriptor_m6CFD9547D6F0F7ADAC34C66B7A492A7D95FB2AB4 (void);
// 0x0000040A Google.Protobuf.Reflection.MessageDescriptor Mediapipe.CalculatorGraphConfig/Types/Node::pb::Google.Protobuf.IMessage.get_Descriptor()
extern void Node_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_mE6DF0D8937057AA16D69FC87BE227924E24249BD (void);
// 0x0000040B System.Void Mediapipe.CalculatorGraphConfig/Types/Node::.ctor()
extern void Node__ctor_m5A45C5EA9496703F28618D3FDCD3D9603F43A1C4 (void);
// 0x0000040C System.Void Mediapipe.CalculatorGraphConfig/Types/Node::.ctor(Mediapipe.CalculatorGraphConfig/Types/Node)
extern void Node__ctor_mEF6F5AF4FEF24FF0B17E590D1A99D89ED393FFD4 (void);
// 0x0000040D Mediapipe.CalculatorGraphConfig/Types/Node Mediapipe.CalculatorGraphConfig/Types/Node::Clone()
extern void Node_Clone_m4A14B5A6E780D1C69310984C336E4E8A91214084 (void);
// 0x0000040E System.String Mediapipe.CalculatorGraphConfig/Types/Node::get_Name()
extern void Node_get_Name_mE894E2493AAADAF47A6ACFC738E7E94394E92030 (void);
// 0x0000040F System.Void Mediapipe.CalculatorGraphConfig/Types/Node::set_Name(System.String)
extern void Node_set_Name_m8DFF5AA51DB6CA486897A1AD8EB12023A5304A19 (void);
// 0x00000410 System.String Mediapipe.CalculatorGraphConfig/Types/Node::get_Calculator()
extern void Node_get_Calculator_m463A3226E13F44A829387F15848B98FC4A16C1C7 (void);
// 0x00000411 System.Void Mediapipe.CalculatorGraphConfig/Types/Node::set_Calculator(System.String)
extern void Node_set_Calculator_mC243D9CA49A3FB38A38C0933167CB9CDB46FC711 (void);
// 0x00000412 Google.Protobuf.Collections.RepeatedField`1<System.String> Mediapipe.CalculatorGraphConfig/Types/Node::get_InputStream()
extern void Node_get_InputStream_m9EB8711512F3633C6BD12A4A0F6112AA8DFDCE27 (void);
// 0x00000413 Google.Protobuf.Collections.RepeatedField`1<System.String> Mediapipe.CalculatorGraphConfig/Types/Node::get_OutputStream()
extern void Node_get_OutputStream_m25F9F90483C30DE82663F105D859B577F6080224 (void);
// 0x00000414 Google.Protobuf.Collections.RepeatedField`1<System.String> Mediapipe.CalculatorGraphConfig/Types/Node::get_InputSidePacket()
extern void Node_get_InputSidePacket_mF7C54227234D3F8C8528FD3EBAD858D13496B22D (void);
// 0x00000415 Google.Protobuf.Collections.RepeatedField`1<System.String> Mediapipe.CalculatorGraphConfig/Types/Node::get_OutputSidePacket()
extern void Node_get_OutputSidePacket_m359AFD1F4BED5CD417EE684572BD56646A435B46 (void);
// 0x00000416 Mediapipe.CalculatorOptions Mediapipe.CalculatorGraphConfig/Types/Node::get_Options()
extern void Node_get_Options_mBB7686196682BE9E2D6AA4FF11935614E986DB7F (void);
// 0x00000417 System.Void Mediapipe.CalculatorGraphConfig/Types/Node::set_Options(Mediapipe.CalculatorOptions)
extern void Node_set_Options_m76F61CD0BA18EAF0EDEF0206766E3A3389F538D3 (void);
// 0x00000418 Google.Protobuf.Collections.RepeatedField`1<Google.Protobuf.WellKnownTypes.Any> Mediapipe.CalculatorGraphConfig/Types/Node::get_NodeOptions()
extern void Node_get_NodeOptions_mB3CF30AF1990E0FC4D3E59FB3202AEBC9F941B48 (void);
// 0x00000419 System.Int32 Mediapipe.CalculatorGraphConfig/Types/Node::get_SourceLayer()
extern void Node_get_SourceLayer_m97D929C5A0664473E9D33926576185747B896168 (void);
// 0x0000041A System.Void Mediapipe.CalculatorGraphConfig/Types/Node::set_SourceLayer(System.Int32)
extern void Node_set_SourceLayer_m4BA33A971551752CC6538799F21C042183F0D21E (void);
// 0x0000041B System.Int32 Mediapipe.CalculatorGraphConfig/Types/Node::get_BufferSizeHint()
extern void Node_get_BufferSizeHint_m6CE45C732EADE72554171AFC981B89AF38B75D8B (void);
// 0x0000041C System.Void Mediapipe.CalculatorGraphConfig/Types/Node::set_BufferSizeHint(System.Int32)
extern void Node_set_BufferSizeHint_mEE66DD42B144153E83E60EADCF0F1F0DF983A47A (void);
// 0x0000041D Mediapipe.InputStreamHandlerConfig Mediapipe.CalculatorGraphConfig/Types/Node::get_InputStreamHandler()
extern void Node_get_InputStreamHandler_m14731A3E0A58CB1461B29F0B3EBC409DF9DD5F68 (void);
// 0x0000041E System.Void Mediapipe.CalculatorGraphConfig/Types/Node::set_InputStreamHandler(Mediapipe.InputStreamHandlerConfig)
extern void Node_set_InputStreamHandler_m64D09F558F91F81782A5880C38AA90C176FABAE9 (void);
// 0x0000041F Mediapipe.OutputStreamHandlerConfig Mediapipe.CalculatorGraphConfig/Types/Node::get_OutputStreamHandler()
extern void Node_get_OutputStreamHandler_m1B9F69D2D3E527A8DF6A9B112E47761326287E5E (void);
// 0x00000420 System.Void Mediapipe.CalculatorGraphConfig/Types/Node::set_OutputStreamHandler(Mediapipe.OutputStreamHandlerConfig)
extern void Node_set_OutputStreamHandler_m68BAFF770395CA21744A219A0C2151210F96FAC8 (void);
// 0x00000421 Google.Protobuf.Collections.RepeatedField`1<Mediapipe.InputStreamInfo> Mediapipe.CalculatorGraphConfig/Types/Node::get_InputStreamInfo()
extern void Node_get_InputStreamInfo_mAA1B70E6EFDED9D84D9974493E3B2EAC855FC811 (void);
// 0x00000422 System.String Mediapipe.CalculatorGraphConfig/Types/Node::get_Executor()
extern void Node_get_Executor_mBA165D27539BF3A41818F70D820AADE881BAAC2F (void);
// 0x00000423 System.Void Mediapipe.CalculatorGraphConfig/Types/Node::set_Executor(System.String)
extern void Node_set_Executor_m2EA39E8873CE41463AAFBAE6D649B7D875982C79 (void);
// 0x00000424 Mediapipe.ProfilerConfig Mediapipe.CalculatorGraphConfig/Types/Node::get_ProfilerConfig()
extern void Node_get_ProfilerConfig_m7B4BB9BC3DEE985FCB1F3136BB3910935463A41B (void);
// 0x00000425 System.Void Mediapipe.CalculatorGraphConfig/Types/Node::set_ProfilerConfig(Mediapipe.ProfilerConfig)
extern void Node_set_ProfilerConfig_mF0C2D1AD53CDDC63846F085DF9CD5E88FD901E51 (void);
// 0x00000426 System.Int32 Mediapipe.CalculatorGraphConfig/Types/Node::get_MaxInFlight()
extern void Node_get_MaxInFlight_m59FAF8A41BCCDA9D0317A51BD8A23EAF250D45B0 (void);
// 0x00000427 System.Void Mediapipe.CalculatorGraphConfig/Types/Node::set_MaxInFlight(System.Int32)
extern void Node_set_MaxInFlight_m1215966893B59017C75BABDFBFD6BB7CA53E5FE8 (void);
// 0x00000428 Google.Protobuf.Collections.RepeatedField`1<System.String> Mediapipe.CalculatorGraphConfig/Types/Node::get_ExternalInput()
extern void Node_get_ExternalInput_m730411592F0348DC416C05E41B00742BEF6CBE86 (void);
// 0x00000429 System.Boolean Mediapipe.CalculatorGraphConfig/Types/Node::Equals(System.Object)
extern void Node_Equals_m63B25BF70104CFF3B8CB2C3A3E9DFCAF0D225024 (void);
// 0x0000042A System.Boolean Mediapipe.CalculatorGraphConfig/Types/Node::Equals(Mediapipe.CalculatorGraphConfig/Types/Node)
extern void Node_Equals_mCAA8CD4AAA40FC247C882742F39D72EE8EF7C59B (void);
// 0x0000042B System.Int32 Mediapipe.CalculatorGraphConfig/Types/Node::GetHashCode()
extern void Node_GetHashCode_m426A32AEA408311AC8B43458F2470D78F00F996D (void);
// 0x0000042C System.String Mediapipe.CalculatorGraphConfig/Types/Node::ToString()
extern void Node_ToString_m6C22CB185110B11818FDB6AE1D71D1DF73DD72F7 (void);
// 0x0000042D System.Void Mediapipe.CalculatorGraphConfig/Types/Node::WriteTo(Google.Protobuf.CodedOutputStream)
extern void Node_WriteTo_m2EAD9127103E77DC06897434BD2F8E070F00E0B8 (void);
// 0x0000042E System.Int32 Mediapipe.CalculatorGraphConfig/Types/Node::CalculateSize()
extern void Node_CalculateSize_m2DC063FE5B6CBB1980AA72F0AA9D7564C541C6DE (void);
// 0x0000042F System.Void Mediapipe.CalculatorGraphConfig/Types/Node::MergeFrom(Mediapipe.CalculatorGraphConfig/Types/Node)
extern void Node_MergeFrom_m1D3E147794EB001C4E41FECDBD9EDA04D1803378 (void);
// 0x00000430 System.Void Mediapipe.CalculatorGraphConfig/Types/Node::MergeFrom(Google.Protobuf.CodedInputStream)
extern void Node_MergeFrom_mD3BB28988C717504CE6F43642FF64E4EAF34D512 (void);
// 0x00000431 System.Void Mediapipe.CalculatorGraphConfig/Types/Node::.cctor()
extern void Node__cctor_m0900A6CC307EEA0D0C17645D288DA0F1A84B1878 (void);
// 0x00000432 System.Void Mediapipe.CalculatorGraphConfig/Types/Node/<>c::.cctor()
extern void U3CU3Ec__cctor_m6691D8B8CB759B58A74F3BE7BDF3C07F45DE956B (void);
// 0x00000433 System.Void Mediapipe.CalculatorGraphConfig/Types/Node/<>c::.ctor()
extern void U3CU3Ec__ctor_m0317D08A4EE3CD97EC4391D557A983CC5CB2BA52 (void);
// 0x00000434 Mediapipe.CalculatorGraphConfig/Types/Node Mediapipe.CalculatorGraphConfig/Types/Node/<>c::<.cctor>b__105_0()
extern void U3CU3Ec_U3C_cctorU3Eb__105_0_m2D64BA391FF1273A1B4191466C9A88B866AD20AC (void);
// 0x00000435 System.Void Mediapipe.CalculatorGraphConfig/<>c::.cctor()
extern void U3CU3Ec__cctor_mE98B01E5F1D1FE7993C97A269FA57D8D15EFDB5E (void);
// 0x00000436 System.Void Mediapipe.CalculatorGraphConfig/<>c::.ctor()
extern void U3CU3Ec__ctor_m6130553500819B4B8B66AD28388D63ED6E86FD16 (void);
// 0x00000437 Mediapipe.CalculatorGraphConfig Mediapipe.CalculatorGraphConfig/<>c::<.cctor>b__116_0()
extern void U3CU3Ec_U3C_cctorU3Eb__116_0_mFF593F9603CDCFB39AC1E7420E4C59FD7FB80B9F (void);
// 0x00000438 Google.Protobuf.Reflection.FileDescriptor Mediapipe.CalculatorOptionsReflection::get_Descriptor()
extern void CalculatorOptionsReflection_get_Descriptor_mF20E4E31A159BA940BC5DE49E78A9E78C49C509E (void);
// 0x00000439 System.Void Mediapipe.CalculatorOptionsReflection::.cctor()
extern void CalculatorOptionsReflection__cctor_mED04E49048569790FA0216FC4F03B95556ADC6DC (void);
// 0x0000043A Google.Protobuf.ExtensionSet`1<Mediapipe.CalculatorOptions> Mediapipe.CalculatorOptions::get__Extensions()
extern void CalculatorOptions_get__Extensions_mE3A439A85F848172CE7F64F8874C64C06B8E3DEA (void);
// 0x0000043B Google.Protobuf.MessageParser`1<Mediapipe.CalculatorOptions> Mediapipe.CalculatorOptions::get_Parser()
extern void CalculatorOptions_get_Parser_mC4CDAC265B77C1107C13C864C75ACFDF5A9FF065 (void);
// 0x0000043C Google.Protobuf.Reflection.MessageDescriptor Mediapipe.CalculatorOptions::get_Descriptor()
extern void CalculatorOptions_get_Descriptor_mA68C7E9135E7BAFC404E03C5EA5D641EFE9B1290 (void);
// 0x0000043D Google.Protobuf.Reflection.MessageDescriptor Mediapipe.CalculatorOptions::pb::Google.Protobuf.IMessage.get_Descriptor()
extern void CalculatorOptions_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m0BFBA8271EF58F841A7563515256852FBE2F4BDB (void);
// 0x0000043E System.Void Mediapipe.CalculatorOptions::.ctor()
extern void CalculatorOptions__ctor_m9A66335BA5FAB6F4F057182BAF253B3A586C55FA (void);
// 0x0000043F System.Void Mediapipe.CalculatorOptions::.ctor(Mediapipe.CalculatorOptions)
extern void CalculatorOptions__ctor_m022E05735F6CE2036FC8135812D6AFAF853F2D16 (void);
// 0x00000440 Mediapipe.CalculatorOptions Mediapipe.CalculatorOptions::Clone()
extern void CalculatorOptions_Clone_m717403D7365C851A4F8558834B83CD542CEE0571 (void);
// 0x00000441 System.Boolean Mediapipe.CalculatorOptions::get_MergeFields()
extern void CalculatorOptions_get_MergeFields_mEFD21DBB2DF9935DB3A43AC109265E0F81CB1A2D (void);
// 0x00000442 System.Void Mediapipe.CalculatorOptions::set_MergeFields(System.Boolean)
extern void CalculatorOptions_set_MergeFields_m1D48D2A31F24665AAD61B8BF1D7366DB1F5368DB (void);
// 0x00000443 System.Boolean Mediapipe.CalculatorOptions::get_HasMergeFields()
extern void CalculatorOptions_get_HasMergeFields_mDB7E473865D6C54CDDB7D25808BDCD53C9830F77 (void);
// 0x00000444 System.Void Mediapipe.CalculatorOptions::ClearMergeFields()
extern void CalculatorOptions_ClearMergeFields_mFD3A53D320B135AFE511B684F9919729200C090D (void);
// 0x00000445 System.Boolean Mediapipe.CalculatorOptions::Equals(System.Object)
extern void CalculatorOptions_Equals_m1A6E5C0DB2FEB400FA0D5F76665FA4F37BDE8B03 (void);
// 0x00000446 System.Boolean Mediapipe.CalculatorOptions::Equals(Mediapipe.CalculatorOptions)
extern void CalculatorOptions_Equals_m1511C43D99252F232CAF2CE903A223A9B7924B87 (void);
// 0x00000447 System.Int32 Mediapipe.CalculatorOptions::GetHashCode()
extern void CalculatorOptions_GetHashCode_m5879C42E02EDE756A47DFDE711951052D2BBF4D1 (void);
// 0x00000448 System.String Mediapipe.CalculatorOptions::ToString()
extern void CalculatorOptions_ToString_mB5C0791D4DB12DAAC8A21F528A1F1DA7BFC06ADF (void);
// 0x00000449 System.Void Mediapipe.CalculatorOptions::WriteTo(Google.Protobuf.CodedOutputStream)
extern void CalculatorOptions_WriteTo_m55AAD179414854C4BD37ED1895396F9EBBFEB1E7 (void);
// 0x0000044A System.Int32 Mediapipe.CalculatorOptions::CalculateSize()
extern void CalculatorOptions_CalculateSize_m9612BCA7552905D230E60EEAAFBAF60DA3767572 (void);
// 0x0000044B System.Void Mediapipe.CalculatorOptions::MergeFrom(Mediapipe.CalculatorOptions)
extern void CalculatorOptions_MergeFrom_mB9BBBF0230136216E2CB33D75343BC9CBD5EDE97 (void);
// 0x0000044C System.Void Mediapipe.CalculatorOptions::MergeFrom(Google.Protobuf.CodedInputStream)
extern void CalculatorOptions_MergeFrom_m8F46A283DBD6012F2F985C795D06856AD6CD0E72 (void);
// 0x0000044D TValue Mediapipe.CalculatorOptions::GetExtension(Google.Protobuf.Extension`2<Mediapipe.CalculatorOptions,TValue>)
// 0x0000044E Google.Protobuf.Collections.RepeatedField`1<TValue> Mediapipe.CalculatorOptions::GetExtension(Google.Protobuf.RepeatedExtension`2<Mediapipe.CalculatorOptions,TValue>)
// 0x0000044F Google.Protobuf.Collections.RepeatedField`1<TValue> Mediapipe.CalculatorOptions::GetOrInitializeExtension(Google.Protobuf.RepeatedExtension`2<Mediapipe.CalculatorOptions,TValue>)
// 0x00000450 System.Void Mediapipe.CalculatorOptions::SetExtension(Google.Protobuf.Extension`2<Mediapipe.CalculatorOptions,TValue>,TValue)
// 0x00000451 System.Boolean Mediapipe.CalculatorOptions::HasExtension(Google.Protobuf.Extension`2<Mediapipe.CalculatorOptions,TValue>)
// 0x00000452 System.Void Mediapipe.CalculatorOptions::ClearExtension(Google.Protobuf.Extension`2<Mediapipe.CalculatorOptions,TValue>)
// 0x00000453 System.Void Mediapipe.CalculatorOptions::ClearExtension(Google.Protobuf.RepeatedExtension`2<Mediapipe.CalculatorOptions,TValue>)
// 0x00000454 System.Void Mediapipe.CalculatorOptions::.cctor()
extern void CalculatorOptions__cctor_mF9865E242081176ED6E2A94CBA6753CD693436E0 (void);
// 0x00000455 System.Void Mediapipe.CalculatorOptions/<>c::.cctor()
extern void U3CU3Ec__cctor_mA0E14D23F686BCB83934F9854B23DCEC99A64551 (void);
// 0x00000456 System.Void Mediapipe.CalculatorOptions/<>c::.ctor()
extern void U3CU3Ec__ctor_mC6FB7BDDD3FE3C4369F030777A5E466797FA4DE0 (void);
// 0x00000457 Mediapipe.CalculatorOptions Mediapipe.CalculatorOptions/<>c::<.cctor>b__40_0()
extern void U3CU3Ec_U3C_cctorU3Eb__40_0_mFD0888555A378F870EF0328048AD9049F02310B5 (void);
// 0x00000458 Google.Protobuf.Reflection.FileDescriptor Mediapipe.ClassificationReflection::get_Descriptor()
extern void ClassificationReflection_get_Descriptor_m78DA35CEFDADB39F032CB4D99EA9793A989B1497 (void);
// 0x00000459 System.Void Mediapipe.ClassificationReflection::.cctor()
extern void ClassificationReflection__cctor_m101442F33A51F897212863098A5C46FE43E6D64D (void);
// 0x0000045A Google.Protobuf.MessageParser`1<Mediapipe.Classification> Mediapipe.Classification::get_Parser()
extern void Classification_get_Parser_m9991AAB25C6135C1B640B62FEDFFE2DA1857227F (void);
// 0x0000045B Google.Protobuf.Reflection.MessageDescriptor Mediapipe.Classification::get_Descriptor()
extern void Classification_get_Descriptor_m4CAC2D180BF16B61FA4C5E9CE3133EB293E521C9 (void);
// 0x0000045C Google.Protobuf.Reflection.MessageDescriptor Mediapipe.Classification::pb::Google.Protobuf.IMessage.get_Descriptor()
extern void Classification_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m305A4A472F71460AB4A2568C30080DB74B512AAF (void);
// 0x0000045D System.Void Mediapipe.Classification::.ctor()
extern void Classification__ctor_mA0FD3F6BC90023C990B4B88F09F9CA77B8DBF669 (void);
// 0x0000045E System.Void Mediapipe.Classification::.ctor(Mediapipe.Classification)
extern void Classification__ctor_mEA745239FD6942EA8EBFCC2C66AA5C12645FD828 (void);
// 0x0000045F Mediapipe.Classification Mediapipe.Classification::Clone()
extern void Classification_Clone_m75EB64A47676D4684C1EF7F86F3053F9360ECD5B (void);
// 0x00000460 System.Int32 Mediapipe.Classification::get_Index()
extern void Classification_get_Index_mFE79FC35324E0D817C664BDD0BE2F28CC1F350E1 (void);
// 0x00000461 System.Void Mediapipe.Classification::set_Index(System.Int32)
extern void Classification_set_Index_m2CD1CA4910F5EF83DA679F897E99D253160239DB (void);
// 0x00000462 System.Boolean Mediapipe.Classification::get_HasIndex()
extern void Classification_get_HasIndex_mB9C7ADCAEBFA0B0F6EFC3E044B66650D4520709E (void);
// 0x00000463 System.Void Mediapipe.Classification::ClearIndex()
extern void Classification_ClearIndex_m19867CF6E45A80A42275612C11C9AAAE7B684CA6 (void);
// 0x00000464 System.Single Mediapipe.Classification::get_Score()
extern void Classification_get_Score_m6CCD9B79AF875C575F96962C002A8566EFD48129 (void);
// 0x00000465 System.Void Mediapipe.Classification::set_Score(System.Single)
extern void Classification_set_Score_m547A53C0E75905C5C480A3FF93AB8C7564FDE6C5 (void);
// 0x00000466 System.Boolean Mediapipe.Classification::get_HasScore()
extern void Classification_get_HasScore_mD6871B3DBB90DCCEA9FCA2B0058C6DA7384AF390 (void);
// 0x00000467 System.Void Mediapipe.Classification::ClearScore()
extern void Classification_ClearScore_m16E3BC42F4227EB6B6ECADA43520EF7363DA1278 (void);
// 0x00000468 System.String Mediapipe.Classification::get_Label()
extern void Classification_get_Label_m969CB2659140B662BC7811109153905CE067E5C6 (void);
// 0x00000469 System.Void Mediapipe.Classification::set_Label(System.String)
extern void Classification_set_Label_mA88854B632D97297FA3F3DA8C3F54278FAA2415D (void);
// 0x0000046A System.Boolean Mediapipe.Classification::get_HasLabel()
extern void Classification_get_HasLabel_m97DB7171CA85E47C60E2976C6D05C28AAEF7C72E (void);
// 0x0000046B System.Void Mediapipe.Classification::ClearLabel()
extern void Classification_ClearLabel_m6F8D5E7CF22D2A405DA815F24248399CDCA66134 (void);
// 0x0000046C System.String Mediapipe.Classification::get_DisplayName()
extern void Classification_get_DisplayName_m664B8F81ADA4E88E59E8ABF475C6531255034C38 (void);
// 0x0000046D System.Void Mediapipe.Classification::set_DisplayName(System.String)
extern void Classification_set_DisplayName_mB976A5F49F03AC4C602AF90287E7826493EA0935 (void);
// 0x0000046E System.Boolean Mediapipe.Classification::get_HasDisplayName()
extern void Classification_get_HasDisplayName_m92482B43EB3E4D20E7A9997C53A69CB92E11FBE5 (void);
// 0x0000046F System.Void Mediapipe.Classification::ClearDisplayName()
extern void Classification_ClearDisplayName_mAA08FE13D01E327D673D03547E9AAB60250C59C4 (void);
// 0x00000470 System.Boolean Mediapipe.Classification::Equals(System.Object)
extern void Classification_Equals_mA50D3D642CB1069D2540F7FECA301BCB9E7ABF8F (void);
// 0x00000471 System.Boolean Mediapipe.Classification::Equals(Mediapipe.Classification)
extern void Classification_Equals_m46491A3E7C368EAD299737A771C47E78F8744E88 (void);
// 0x00000472 System.Int32 Mediapipe.Classification::GetHashCode()
extern void Classification_GetHashCode_mC63AA2FB66A9CB60B0969C8410E40986ED657247 (void);
// 0x00000473 System.String Mediapipe.Classification::ToString()
extern void Classification_ToString_m6D17450C980C2A8E9082FC523C0A78943D27E6F0 (void);
// 0x00000474 System.Void Mediapipe.Classification::WriteTo(Google.Protobuf.CodedOutputStream)
extern void Classification_WriteTo_m53061790010FE08E18DB482140160B2063EB583C (void);
// 0x00000475 System.Int32 Mediapipe.Classification::CalculateSize()
extern void Classification_CalculateSize_mFF773292996625892D0F77C07A43D0B65C7C931D (void);
// 0x00000476 System.Void Mediapipe.Classification::MergeFrom(Mediapipe.Classification)
extern void Classification_MergeFrom_m8D981270B6DB8836F391140E0597189E1FCC74E3 (void);
// 0x00000477 System.Void Mediapipe.Classification::MergeFrom(Google.Protobuf.CodedInputStream)
extern void Classification_MergeFrom_m928242B2CE6F22D7577F5ACB8A01B507B34995C2 (void);
// 0x00000478 System.Void Mediapipe.Classification::.cctor()
extern void Classification__cctor_m529CD66124B6A29D2F21337C4E9441621CFF5338 (void);
// 0x00000479 System.Void Mediapipe.Classification/<>c::.cctor()
extern void U3CU3Ec__cctor_mFAC5FD5F4F89E867E169C84412DDC0A3FC50BB93 (void);
// 0x0000047A System.Void Mediapipe.Classification/<>c::.ctor()
extern void U3CU3Ec__ctor_m71F748620202B31A7FAF60806A2D7B340999BE9B (void);
// 0x0000047B Mediapipe.Classification Mediapipe.Classification/<>c::<.cctor>b__57_0()
extern void U3CU3Ec_U3C_cctorU3Eb__57_0_m28BAE58C6629166341F472CFDAB8D83DAEF7BDD6 (void);
// 0x0000047C Google.Protobuf.MessageParser`1<Mediapipe.ClassificationList> Mediapipe.ClassificationList::get_Parser()
extern void ClassificationList_get_Parser_m1C0072871396300B488BBE724F48C1AE9EF52CBE (void);
// 0x0000047D Google.Protobuf.Reflection.MessageDescriptor Mediapipe.ClassificationList::get_Descriptor()
extern void ClassificationList_get_Descriptor_m6289F12482426E42F5A3784CB4F956036ECF8C16 (void);
// 0x0000047E Google.Protobuf.Reflection.MessageDescriptor Mediapipe.ClassificationList::pb::Google.Protobuf.IMessage.get_Descriptor()
extern void ClassificationList_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m993852CE5EC970083F2B7CC7E1294261153A32B4 (void);
// 0x0000047F System.Void Mediapipe.ClassificationList::.ctor()
extern void ClassificationList__ctor_m99E71A343606FDE18A16A30431F3FB10C8E85400 (void);
// 0x00000480 System.Void Mediapipe.ClassificationList::.ctor(Mediapipe.ClassificationList)
extern void ClassificationList__ctor_mCF0BED5C2EF4578C32BAF890A31DF4548E394480 (void);
// 0x00000481 Mediapipe.ClassificationList Mediapipe.ClassificationList::Clone()
extern void ClassificationList_Clone_m1BEC3137BD0BD0ADC62E511E6D6C00FC616B30A3 (void);
// 0x00000482 Google.Protobuf.Collections.RepeatedField`1<Mediapipe.Classification> Mediapipe.ClassificationList::get_Classification()
extern void ClassificationList_get_Classification_m93E3EFFC59DD0782B82B986DF08D5E6D8B0C765E (void);
// 0x00000483 System.Boolean Mediapipe.ClassificationList::Equals(System.Object)
extern void ClassificationList_Equals_m2EBF80429667EEC0655AFE46B55349A6A4ECAB79 (void);
// 0x00000484 System.Boolean Mediapipe.ClassificationList::Equals(Mediapipe.ClassificationList)
extern void ClassificationList_Equals_mC9CB38807A2EC3B87251459B9C540C3DB2D8AA91 (void);
// 0x00000485 System.Int32 Mediapipe.ClassificationList::GetHashCode()
extern void ClassificationList_GetHashCode_mA2375D105268BBA492A629CB6D74F776ED246C01 (void);
// 0x00000486 System.String Mediapipe.ClassificationList::ToString()
extern void ClassificationList_ToString_m8A6C7A594C9DA6CFAEB291FAF1C4419350870DE2 (void);
// 0x00000487 System.Void Mediapipe.ClassificationList::WriteTo(Google.Protobuf.CodedOutputStream)
extern void ClassificationList_WriteTo_m5C28B1914E8CBA3CDD0CC26E009A49FDACE1E1B1 (void);
// 0x00000488 System.Int32 Mediapipe.ClassificationList::CalculateSize()
extern void ClassificationList_CalculateSize_m06E066709F39339F2F9880D3EB7A9CCDD1950F88 (void);
// 0x00000489 System.Void Mediapipe.ClassificationList::MergeFrom(Mediapipe.ClassificationList)
extern void ClassificationList_MergeFrom_m771CE3B97011B6A02527AA5F8AB056CC280A4759 (void);
// 0x0000048A System.Void Mediapipe.ClassificationList::MergeFrom(Google.Protobuf.CodedInputStream)
extern void ClassificationList_MergeFrom_m11F8284D369551729C93A32BC16394DA6990280C (void);
// 0x0000048B System.Void Mediapipe.ClassificationList::.cctor()
extern void ClassificationList__cctor_m115DB60C39CB0C78C2E3A7D9E9FA3F8185E3688A (void);
// 0x0000048C System.Void Mediapipe.ClassificationList/<>c::.cctor()
extern void U3CU3Ec__cctor_m473E5C6703238A211F37B203C89988DE76245581 (void);
// 0x0000048D System.Void Mediapipe.ClassificationList/<>c::.ctor()
extern void U3CU3Ec__ctor_mB16914BE2CAD61F52D33CD22926EBE47055DCD2A (void);
// 0x0000048E Mediapipe.ClassificationList Mediapipe.ClassificationList/<>c::<.cctor>b__25_0()
extern void U3CU3Ec_U3C_cctorU3Eb__25_0_m5C9E9AEF6312BEE63E13A9513E60921E769A132C (void);
// 0x0000048F Google.Protobuf.Reflection.FileDescriptor Mediapipe.DetectionReflection::get_Descriptor()
extern void DetectionReflection_get_Descriptor_mCB26B9F43F22487C01ECA623532A1729500844A9 (void);
// 0x00000490 System.Void Mediapipe.DetectionReflection::.cctor()
extern void DetectionReflection__cctor_m29247C93D4909A3D8F3CEAD3A22700D443C8D0DD (void);
// 0x00000491 Google.Protobuf.MessageParser`1<Mediapipe.Detection> Mediapipe.Detection::get_Parser()
extern void Detection_get_Parser_m8804F3493EB34E33CB9D3403761A2080CDEE91A8 (void);
// 0x00000492 Google.Protobuf.Reflection.MessageDescriptor Mediapipe.Detection::get_Descriptor()
extern void Detection_get_Descriptor_m960643CC637EA36DD9CC7991397DCB69E3D1C7B4 (void);
// 0x00000493 Google.Protobuf.Reflection.MessageDescriptor Mediapipe.Detection::pb::Google.Protobuf.IMessage.get_Descriptor()
extern void Detection_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_mA9EC20DA8799833311A701FAA77BCC77ADEA0273 (void);
// 0x00000494 System.Void Mediapipe.Detection::.ctor()
extern void Detection__ctor_m55A7113BDFA8EF7BE08FA4B349F34A3565313C2C (void);
// 0x00000495 System.Void Mediapipe.Detection::.ctor(Mediapipe.Detection)
extern void Detection__ctor_m9A9FD199DC21FB37D14C03361D20D3D8E727FA25 (void);
// 0x00000496 Mediapipe.Detection Mediapipe.Detection::Clone()
extern void Detection_Clone_mD7CBC5BAEBC3C6C75A793B8F3C68C9302369C26B (void);
// 0x00000497 Google.Protobuf.Collections.RepeatedField`1<System.String> Mediapipe.Detection::get_Label()
extern void Detection_get_Label_m859E003D211E67426B3E718E37573AB8BC57A0F6 (void);
// 0x00000498 Google.Protobuf.Collections.RepeatedField`1<System.Int32> Mediapipe.Detection::get_LabelId()
extern void Detection_get_LabelId_m94CA1879A1F03B68D28ED95D3A9015C5A86AAE50 (void);
// 0x00000499 Google.Protobuf.Collections.RepeatedField`1<System.Single> Mediapipe.Detection::get_Score()
extern void Detection_get_Score_mA75239AC61ECD75F9C6A8C3BA58205E95795DD34 (void);
// 0x0000049A Mediapipe.LocationData Mediapipe.Detection::get_LocationData()
extern void Detection_get_LocationData_mE9510CB14E4E89A7B989F268825E8B0B28109247 (void);
// 0x0000049B System.Void Mediapipe.Detection::set_LocationData(Mediapipe.LocationData)
extern void Detection_set_LocationData_m7DF88785B38E16CB61F810B2D6B2E1DF294FED01 (void);
// 0x0000049C System.Boolean Mediapipe.Detection::get_HasLocationData()
extern void Detection_get_HasLocationData_mC91AB3C89B4B520E89B47280EA1C02AA1AE97693 (void);
// 0x0000049D System.Void Mediapipe.Detection::ClearLocationData()
extern void Detection_ClearLocationData_m1F6F3AB12BA2553C34D4438B8DAA691413B2F900 (void);
// 0x0000049E System.String Mediapipe.Detection::get_FeatureTag()
extern void Detection_get_FeatureTag_mAD5D24D4F9AFFAEC840EEB8681C65D66CF87C321 (void);
// 0x0000049F System.Void Mediapipe.Detection::set_FeatureTag(System.String)
extern void Detection_set_FeatureTag_m7FD28F6460CEC89731D435C5B244ADD03F61366E (void);
// 0x000004A0 System.Boolean Mediapipe.Detection::get_HasFeatureTag()
extern void Detection_get_HasFeatureTag_m33BE0FB333C13D56A374E18FCE67433FFA6B7D37 (void);
// 0x000004A1 System.Void Mediapipe.Detection::ClearFeatureTag()
extern void Detection_ClearFeatureTag_m2E2491436E69DB5B53F661B3E472B28FAEA25EFE (void);
// 0x000004A2 System.String Mediapipe.Detection::get_TrackId()
extern void Detection_get_TrackId_mA0AEE8F82BBE355D60BB0A82E88F898C5EDFD1A8 (void);
// 0x000004A3 System.Void Mediapipe.Detection::set_TrackId(System.String)
extern void Detection_set_TrackId_m2B5BB3099BA7A0265DB14ECF2916DC6ACBAE94E8 (void);
// 0x000004A4 System.Boolean Mediapipe.Detection::get_HasTrackId()
extern void Detection_get_HasTrackId_mB016B407BBFBE43EC46B4C5DAEDE7D0AF914D620 (void);
// 0x000004A5 System.Void Mediapipe.Detection::ClearTrackId()
extern void Detection_ClearTrackId_m966839C219ACA6DCC0636BFD8EDFF70048B82DC9 (void);
// 0x000004A6 System.Int64 Mediapipe.Detection::get_DetectionId()
extern void Detection_get_DetectionId_mBFC4E7FE96326BBE9A265D305F6117BF373A031C (void);
// 0x000004A7 System.Void Mediapipe.Detection::set_DetectionId(System.Int64)
extern void Detection_set_DetectionId_m999639076277B1F079CECE9BA157189DF690B87D (void);
// 0x000004A8 System.Boolean Mediapipe.Detection::get_HasDetectionId()
extern void Detection_get_HasDetectionId_m437570724A7066C0079CC8918055BE6C81312A63 (void);
// 0x000004A9 System.Void Mediapipe.Detection::ClearDetectionId()
extern void Detection_ClearDetectionId_m9CB64FD41C0C973CFD6AFAAA30170431AEC808F7 (void);
// 0x000004AA Google.Protobuf.Collections.RepeatedField`1<Mediapipe.Detection/Types/AssociatedDetection> Mediapipe.Detection::get_AssociatedDetections()
extern void Detection_get_AssociatedDetections_m969BB4B06366A94202105D65287D2A91F2D1C750 (void);
// 0x000004AB Google.Protobuf.Collections.RepeatedField`1<System.String> Mediapipe.Detection::get_DisplayName()
extern void Detection_get_DisplayName_mB10FCAFDA10A6817D3B6631F72A4D9A5488B6A96 (void);
// 0x000004AC System.Int64 Mediapipe.Detection::get_TimestampUsec()
extern void Detection_get_TimestampUsec_m56D5992B5F1D7EE39E6459492144689ABB07CAFD (void);
// 0x000004AD System.Void Mediapipe.Detection::set_TimestampUsec(System.Int64)
extern void Detection_set_TimestampUsec_m2F6ABA6B9FE81772F55F1ED3DC4708C11C7C01BC (void);
// 0x000004AE System.Boolean Mediapipe.Detection::get_HasTimestampUsec()
extern void Detection_get_HasTimestampUsec_m7DB9B72A5F8D4CC62935F9FA1F250D6CC38246DE (void);
// 0x000004AF System.Void Mediapipe.Detection::ClearTimestampUsec()
extern void Detection_ClearTimestampUsec_m5E7F1296F66D3F6C41227404FD63C067189A97D9 (void);
// 0x000004B0 System.Boolean Mediapipe.Detection::Equals(System.Object)
extern void Detection_Equals_mF155165DC1D42CDC67B1CA09A89384560162D4B0 (void);
// 0x000004B1 System.Boolean Mediapipe.Detection::Equals(Mediapipe.Detection)
extern void Detection_Equals_mD313C1A5318C95D4E59E243292D9C147B7A49460 (void);
// 0x000004B2 System.Int32 Mediapipe.Detection::GetHashCode()
extern void Detection_GetHashCode_mB7B8FB777B8621E0928027D44910CC0971BFA294 (void);
// 0x000004B3 System.String Mediapipe.Detection::ToString()
extern void Detection_ToString_mE695F914BD60F8E148D00D86E4FBB5C060007026 (void);
// 0x000004B4 System.Void Mediapipe.Detection::WriteTo(Google.Protobuf.CodedOutputStream)
extern void Detection_WriteTo_m871A5B247BDFCCA600B3C8FE18949C639DB691B9 (void);
// 0x000004B5 System.Int32 Mediapipe.Detection::CalculateSize()
extern void Detection_CalculateSize_m729B385BE443B130004A0016D33D88914B391E01 (void);
// 0x000004B6 System.Void Mediapipe.Detection::MergeFrom(Mediapipe.Detection)
extern void Detection_MergeFrom_m6FD510B8AA365A2AB532AF3FA7AC12E7CA62B444 (void);
// 0x000004B7 System.Void Mediapipe.Detection::MergeFrom(Google.Protobuf.CodedInputStream)
extern void Detection_MergeFrom_m677B85361862396FF00F51831D73EFF565CA7532 (void);
// 0x000004B8 System.Void Mediapipe.Detection::.cctor()
extern void Detection__cctor_m40A52CEBE01AD2AB0CE83012833D2BA2ED5E9DBE (void);
// 0x000004B9 Google.Protobuf.MessageParser`1<Mediapipe.Detection/Types/AssociatedDetection> Mediapipe.Detection/Types/AssociatedDetection::get_Parser()
extern void AssociatedDetection_get_Parser_m7F5CE7B641562230C7DFD50A6BFEEA486BEBAA08 (void);
// 0x000004BA Google.Protobuf.Reflection.MessageDescriptor Mediapipe.Detection/Types/AssociatedDetection::get_Descriptor()
extern void AssociatedDetection_get_Descriptor_m6B684E8503745EDC2FFFF2F30A7C254A11A570C4 (void);
// 0x000004BB Google.Protobuf.Reflection.MessageDescriptor Mediapipe.Detection/Types/AssociatedDetection::pb::Google.Protobuf.IMessage.get_Descriptor()
extern void AssociatedDetection_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_mC84E02347E27FAA941E93DFFD1481E7217F20F2E (void);
// 0x000004BC System.Void Mediapipe.Detection/Types/AssociatedDetection::.ctor()
extern void AssociatedDetection__ctor_mA166F2C623931F25847860332A520ABAA5CC9DB4 (void);
// 0x000004BD System.Void Mediapipe.Detection/Types/AssociatedDetection::.ctor(Mediapipe.Detection/Types/AssociatedDetection)
extern void AssociatedDetection__ctor_m1BFE2C623E6B59BB029246429752139FFA5455BC (void);
// 0x000004BE Mediapipe.Detection/Types/AssociatedDetection Mediapipe.Detection/Types/AssociatedDetection::Clone()
extern void AssociatedDetection_Clone_m11C498A5BFF91B15128640FE1FF6C186DC791115 (void);
// 0x000004BF System.Int32 Mediapipe.Detection/Types/AssociatedDetection::get_Id()
extern void AssociatedDetection_get_Id_mEC4F87C0236F9F3FD0005C23783F2253702FFFBF (void);
// 0x000004C0 System.Void Mediapipe.Detection/Types/AssociatedDetection::set_Id(System.Int32)
extern void AssociatedDetection_set_Id_mF0B0781948BBF847A3BCFCAC185091B2594A30A1 (void);
// 0x000004C1 System.Boolean Mediapipe.Detection/Types/AssociatedDetection::get_HasId()
extern void AssociatedDetection_get_HasId_m9E3DCFFA41329ECA799568E888B8E59CDB03C9EE (void);
// 0x000004C2 System.Void Mediapipe.Detection/Types/AssociatedDetection::ClearId()
extern void AssociatedDetection_ClearId_m1329E13167EA8CDBCFD82765534017CF7CCBC2F4 (void);
// 0x000004C3 System.Single Mediapipe.Detection/Types/AssociatedDetection::get_Confidence()
extern void AssociatedDetection_get_Confidence_m75A9322DD5BEFAA66F6C51D5952E588A297529A3 (void);
// 0x000004C4 System.Void Mediapipe.Detection/Types/AssociatedDetection::set_Confidence(System.Single)
extern void AssociatedDetection_set_Confidence_mBA28B3826F17A35A8C08C28BB4DE661DC788945C (void);
// 0x000004C5 System.Boolean Mediapipe.Detection/Types/AssociatedDetection::get_HasConfidence()
extern void AssociatedDetection_get_HasConfidence_m25B9B189095640C7063375061A00693721F7BB50 (void);
// 0x000004C6 System.Void Mediapipe.Detection/Types/AssociatedDetection::ClearConfidence()
extern void AssociatedDetection_ClearConfidence_m552D9205076452F3657B3C6A145064F6FE0CEEAD (void);
// 0x000004C7 System.Boolean Mediapipe.Detection/Types/AssociatedDetection::Equals(System.Object)
extern void AssociatedDetection_Equals_mD472A75917A686CB5371B84ADC4B8667091541FF (void);
// 0x000004C8 System.Boolean Mediapipe.Detection/Types/AssociatedDetection::Equals(Mediapipe.Detection/Types/AssociatedDetection)
extern void AssociatedDetection_Equals_m1069816F34AFE7A08AD3953CFC280417427FDA02 (void);
// 0x000004C9 System.Int32 Mediapipe.Detection/Types/AssociatedDetection::GetHashCode()
extern void AssociatedDetection_GetHashCode_mE8B82EDD364192C96CFECF67D9C44ACD43128F64 (void);
// 0x000004CA System.String Mediapipe.Detection/Types/AssociatedDetection::ToString()
extern void AssociatedDetection_ToString_m7AF4B716FBE2D34C584115309FD4DC10F3B1D396 (void);
// 0x000004CB System.Void Mediapipe.Detection/Types/AssociatedDetection::WriteTo(Google.Protobuf.CodedOutputStream)
extern void AssociatedDetection_WriteTo_m6CF6A142DD499EA8E2564F71291BE2FCD068DF87 (void);
// 0x000004CC System.Int32 Mediapipe.Detection/Types/AssociatedDetection::CalculateSize()
extern void AssociatedDetection_CalculateSize_m31A05FE7461704430FFD30B726F2EFE701EA7F05 (void);
// 0x000004CD System.Void Mediapipe.Detection/Types/AssociatedDetection::MergeFrom(Mediapipe.Detection/Types/AssociatedDetection)
extern void AssociatedDetection_MergeFrom_m1C3C649269388963B9FA33ED78AA4FF2FA0DB27D (void);
// 0x000004CE System.Void Mediapipe.Detection/Types/AssociatedDetection::MergeFrom(Google.Protobuf.CodedInputStream)
extern void AssociatedDetection_MergeFrom_m2BB477AACB83B0CF3CDF2C0161D97898E2152612 (void);
// 0x000004CF System.Void Mediapipe.Detection/Types/AssociatedDetection::.cctor()
extern void AssociatedDetection__cctor_mF73162ED5E8EF82717CB8D743D0B3A9697DAE81A (void);
// 0x000004D0 System.Void Mediapipe.Detection/Types/AssociatedDetection/<>c::.cctor()
extern void U3CU3Ec__cctor_m4784E724F54A57C00AA99199DC6CCE1C588247F8 (void);
// 0x000004D1 System.Void Mediapipe.Detection/Types/AssociatedDetection/<>c::.ctor()
extern void U3CU3Ec__ctor_m47E41794FCDFBA8A6E43F8071E323C2AFE80FA3D (void);
// 0x000004D2 Mediapipe.Detection/Types/AssociatedDetection Mediapipe.Detection/Types/AssociatedDetection/<>c::<.cctor>b__39_0()
extern void U3CU3Ec_U3C_cctorU3Eb__39_0_m712A1FE27414A54F9D2C7B0BDAB83A24A2C0E93E (void);
// 0x000004D3 System.Void Mediapipe.Detection/<>c::.cctor()
extern void U3CU3Ec__cctor_m76955C859E37EEF2711E99A6AE284AB4DA351550 (void);
// 0x000004D4 System.Void Mediapipe.Detection/<>c::.ctor()
extern void U3CU3Ec__ctor_m167260417D2CF2B3233A2CD043C6F77A89496671 (void);
// 0x000004D5 Mediapipe.Detection Mediapipe.Detection/<>c::<.cctor>b__91_0()
extern void U3CU3Ec_U3C_cctorU3Eb__91_0_m317030EDDF21A0236C6F6E9FE21F73D1621F90A3 (void);
// 0x000004D6 Google.Protobuf.MessageParser`1<Mediapipe.DetectionList> Mediapipe.DetectionList::get_Parser()
extern void DetectionList_get_Parser_mA6F1C81858C872CEA7512DAFFDE8F50AB9FEBD42 (void);
// 0x000004D7 Google.Protobuf.Reflection.MessageDescriptor Mediapipe.DetectionList::get_Descriptor()
extern void DetectionList_get_Descriptor_m5E47340D9DE96F758E57D0B4476FCF7824DB17E0 (void);
// 0x000004D8 Google.Protobuf.Reflection.MessageDescriptor Mediapipe.DetectionList::pb::Google.Protobuf.IMessage.get_Descriptor()
extern void DetectionList_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_mA7B3130A43EFA6FF2093563982F72C0A9A147022 (void);
// 0x000004D9 System.Void Mediapipe.DetectionList::.ctor()
extern void DetectionList__ctor_m06FDB5957ED0F6F0B39F5738E06C2F1159B37183 (void);
// 0x000004DA System.Void Mediapipe.DetectionList::.ctor(Mediapipe.DetectionList)
extern void DetectionList__ctor_mAE98482B38CBE076985F9128D7281E33546913D1 (void);
// 0x000004DB Mediapipe.DetectionList Mediapipe.DetectionList::Clone()
extern void DetectionList_Clone_m5331EB486F6B61CFB67E0BF4ECD4BE8AB966B807 (void);
// 0x000004DC Google.Protobuf.Collections.RepeatedField`1<Mediapipe.Detection> Mediapipe.DetectionList::get_Detection()
extern void DetectionList_get_Detection_m90A7E1B909449ED7EBF2074B32294A3063241791 (void);
// 0x000004DD System.Boolean Mediapipe.DetectionList::Equals(System.Object)
extern void DetectionList_Equals_m298DE49B1E060D9C24B467E5A55A8C1D145923D4 (void);
// 0x000004DE System.Boolean Mediapipe.DetectionList::Equals(Mediapipe.DetectionList)
extern void DetectionList_Equals_m6E7B500E4EADA19BAFBE795DFDCFBA9A8B44596F (void);
// 0x000004DF System.Int32 Mediapipe.DetectionList::GetHashCode()
extern void DetectionList_GetHashCode_m807A6737A246CB6010A539204908F6F27215C8EE (void);
// 0x000004E0 System.String Mediapipe.DetectionList::ToString()
extern void DetectionList_ToString_mB5437A519C9DDF1885D468B7324AADDE901CB12D (void);
// 0x000004E1 System.Void Mediapipe.DetectionList::WriteTo(Google.Protobuf.CodedOutputStream)
extern void DetectionList_WriteTo_m7BA51D678FE5CAD8ADD3A9FDEFE9211D36CB8420 (void);
// 0x000004E2 System.Int32 Mediapipe.DetectionList::CalculateSize()
extern void DetectionList_CalculateSize_mD096AE8E4FAC18AAA69EFAE4CACF35BDA4A4AC58 (void);
// 0x000004E3 System.Void Mediapipe.DetectionList::MergeFrom(Mediapipe.DetectionList)
extern void DetectionList_MergeFrom_mDF65E3B58421A40462E72B81742D574CC1C9458B (void);
// 0x000004E4 System.Void Mediapipe.DetectionList::MergeFrom(Google.Protobuf.CodedInputStream)
extern void DetectionList_MergeFrom_m630B2FE189B963D229CE290BA57F9AA276645AEC (void);
// 0x000004E5 System.Void Mediapipe.DetectionList::.cctor()
extern void DetectionList__cctor_mD43572ABD6868A4C397B472E91C90DDC8789A565 (void);
// 0x000004E6 System.Void Mediapipe.DetectionList/<>c::.cctor()
extern void U3CU3Ec__cctor_m102485CB67CBF844566CD1DEFA91C90784CF5622 (void);
// 0x000004E7 System.Void Mediapipe.DetectionList/<>c::.ctor()
extern void U3CU3Ec__ctor_mB5B25F654BEED6D58A60BA1092BE07B5E6335755 (void);
// 0x000004E8 Mediapipe.DetectionList Mediapipe.DetectionList/<>c::<.cctor>b__25_0()
extern void U3CU3Ec_U3C_cctorU3Eb__25_0_m78D642D8A85C09696E0F45C20DA6702A26903489 (void);
// 0x000004E9 Google.Protobuf.Reflection.FileDescriptor Mediapipe.LandmarkReflection::get_Descriptor()
extern void LandmarkReflection_get_Descriptor_m2D2FDF99733E2B6325737FD7641FBBC927162A18 (void);
// 0x000004EA System.Void Mediapipe.LandmarkReflection::.cctor()
extern void LandmarkReflection__cctor_mA62B0D521A665EE5962B46B9868B80F1D061EDAD (void);
// 0x000004EB Google.Protobuf.MessageParser`1<Mediapipe.Landmark> Mediapipe.Landmark::get_Parser()
extern void Landmark_get_Parser_mF9ED7F5C6B0659C92AE4CE5BFF7D3D27CED07589 (void);
// 0x000004EC Google.Protobuf.Reflection.MessageDescriptor Mediapipe.Landmark::get_Descriptor()
extern void Landmark_get_Descriptor_m245583FE6AD9B9D7C6E8DF399D32DE967D3856E5 (void);
// 0x000004ED Google.Protobuf.Reflection.MessageDescriptor Mediapipe.Landmark::pb::Google.Protobuf.IMessage.get_Descriptor()
extern void Landmark_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_mCC9FA090DAB3FCDAA2EBD01DE78F12FD8B4040F1 (void);
// 0x000004EE System.Void Mediapipe.Landmark::.ctor()
extern void Landmark__ctor_m32AB2F02006F1DA904DB9B95FBA6F54438CB7E09 (void);
// 0x000004EF System.Void Mediapipe.Landmark::.ctor(Mediapipe.Landmark)
extern void Landmark__ctor_mB5954D418050FA757FEEAA123D3DB388279E0D66 (void);
// 0x000004F0 Mediapipe.Landmark Mediapipe.Landmark::Clone()
extern void Landmark_Clone_m2F263E193E5C29A90092AE5EE1AC5B3880CAAAEC (void);
// 0x000004F1 System.Single Mediapipe.Landmark::get_X()
extern void Landmark_get_X_mCCC8565C94A49C95CE661F1F31887EE88251F2DE (void);
// 0x000004F2 System.Void Mediapipe.Landmark::set_X(System.Single)
extern void Landmark_set_X_m2A35E536CECCDC1F1C7217F6FD13A1AB310699C4 (void);
// 0x000004F3 System.Boolean Mediapipe.Landmark::get_HasX()
extern void Landmark_get_HasX_m33122BBA8B7B7D0EC394223AF2206A71FE7C428C (void);
// 0x000004F4 System.Void Mediapipe.Landmark::ClearX()
extern void Landmark_ClearX_m75472AA9F683DEC71B30978EA0A088121462A016 (void);
// 0x000004F5 System.Single Mediapipe.Landmark::get_Y()
extern void Landmark_get_Y_mA9262DF805305E3CB5BE75FB6F7C495A02FFCB15 (void);
// 0x000004F6 System.Void Mediapipe.Landmark::set_Y(System.Single)
extern void Landmark_set_Y_mE8411E61699805A916E515C267DF61824EABF79A (void);
// 0x000004F7 System.Boolean Mediapipe.Landmark::get_HasY()
extern void Landmark_get_HasY_mB40C8AD8D6CA8B14F187CDA443CD612633E5A4FB (void);
// 0x000004F8 System.Void Mediapipe.Landmark::ClearY()
extern void Landmark_ClearY_mFE69B3F9E85D3947683E92A28E5F6D43F0FC2BAA (void);
// 0x000004F9 System.Single Mediapipe.Landmark::get_Z()
extern void Landmark_get_Z_m31B36BF45DE419103F21FD96B032972214C2CC4A (void);
// 0x000004FA System.Void Mediapipe.Landmark::set_Z(System.Single)
extern void Landmark_set_Z_m19F0BBCBC95F9D1E333D08B38FC8C809D392C9DD (void);
// 0x000004FB System.Boolean Mediapipe.Landmark::get_HasZ()
extern void Landmark_get_HasZ_mF14860BF6E8C04369B1F238F087BB0D944722031 (void);
// 0x000004FC System.Void Mediapipe.Landmark::ClearZ()
extern void Landmark_ClearZ_m77CF2955E6B9A2F237E810067B15CD756B9B3A3A (void);
// 0x000004FD System.Single Mediapipe.Landmark::get_Visibility()
extern void Landmark_get_Visibility_mEE2C652EAB55A50697BDD510624975D04206A98B (void);
// 0x000004FE System.Void Mediapipe.Landmark::set_Visibility(System.Single)
extern void Landmark_set_Visibility_mB185A177B81BB5F6C26BABC659CE5BCB49330ACD (void);
// 0x000004FF System.Boolean Mediapipe.Landmark::get_HasVisibility()
extern void Landmark_get_HasVisibility_m448A4F9944944988226E9E9DAE6CA1BE16364CCE (void);
// 0x00000500 System.Void Mediapipe.Landmark::ClearVisibility()
extern void Landmark_ClearVisibility_m63565970A485B48F1E5A549D2A9468B6CB05EF7D (void);
// 0x00000501 System.Single Mediapipe.Landmark::get_Presence()
extern void Landmark_get_Presence_mEBE783B55F9F7880480EAFFBB56A9EAA6B6F0693 (void);
// 0x00000502 System.Void Mediapipe.Landmark::set_Presence(System.Single)
extern void Landmark_set_Presence_m1FA51C2A95825CEE140B99E2392A2EF1F719A2E4 (void);
// 0x00000503 System.Boolean Mediapipe.Landmark::get_HasPresence()
extern void Landmark_get_HasPresence_m0DEE9FD838DA5FECAE7E73CA83C130A2DC3558EF (void);
// 0x00000504 System.Void Mediapipe.Landmark::ClearPresence()
extern void Landmark_ClearPresence_m04EEA91E043A0665A4B35DCC26206F73D5643763 (void);
// 0x00000505 System.Boolean Mediapipe.Landmark::Equals(System.Object)
extern void Landmark_Equals_m5DF27AE33F5ADCA0F166401F6EF682568172E3CD (void);
// 0x00000506 System.Boolean Mediapipe.Landmark::Equals(Mediapipe.Landmark)
extern void Landmark_Equals_m5DCEFC736DAAB541ECAF07682E0CBA342E06F8C5 (void);
// 0x00000507 System.Int32 Mediapipe.Landmark::GetHashCode()
extern void Landmark_GetHashCode_m1D05AEC667DFD74EF7BD565F23D18E9F2D9EBABF (void);
// 0x00000508 System.String Mediapipe.Landmark::ToString()
extern void Landmark_ToString_m8A1ADCAF99F8EAB736244D2D64180F410747E9B6 (void);
// 0x00000509 System.Void Mediapipe.Landmark::WriteTo(Google.Protobuf.CodedOutputStream)
extern void Landmark_WriteTo_m2426178F2F1BE3E779166512562C66D5CE55E2DB (void);
// 0x0000050A System.Int32 Mediapipe.Landmark::CalculateSize()
extern void Landmark_CalculateSize_m57A5261BCE0E7D5FAD4404AB917F2F7D5709CA8A (void);
// 0x0000050B System.Void Mediapipe.Landmark::MergeFrom(Mediapipe.Landmark)
extern void Landmark_MergeFrom_m601FEE1790CD75767288FECA657CEEC9516EB0AB (void);
// 0x0000050C System.Void Mediapipe.Landmark::MergeFrom(Google.Protobuf.CodedInputStream)
extern void Landmark_MergeFrom_mA2E18E48D58A71220844E0C26DBA09D4E2DED0BB (void);
// 0x0000050D System.Void Mediapipe.Landmark::.cctor()
extern void Landmark__cctor_m9CE5019CADC8C750C9DD1BB9AB5230E3B8D3A9FD (void);
// 0x0000050E System.Void Mediapipe.Landmark/<>c::.cctor()
extern void U3CU3Ec__cctor_m3DC9E59ADA4F78ED23477189987FE20453DABF04 (void);
// 0x0000050F System.Void Mediapipe.Landmark/<>c::.ctor()
extern void U3CU3Ec__ctor_m9C3D3629FF351E8AA86889FE7EC2DF4F046F43B8 (void);
// 0x00000510 Mediapipe.Landmark Mediapipe.Landmark/<>c::<.cctor>b__66_0()
extern void U3CU3Ec_U3C_cctorU3Eb__66_0_m35D2ACA3993A2BB91BA9E105CAEF8A669C570322 (void);
// 0x00000511 Google.Protobuf.MessageParser`1<Mediapipe.LandmarkList> Mediapipe.LandmarkList::get_Parser()
extern void LandmarkList_get_Parser_m6F868AA7BF10DEB5CD13DD99219170AECF26168A (void);
// 0x00000512 Google.Protobuf.Reflection.MessageDescriptor Mediapipe.LandmarkList::get_Descriptor()
extern void LandmarkList_get_Descriptor_m87B88FA4DB78B78C66165964C21FBB884B6092E8 (void);
// 0x00000513 Google.Protobuf.Reflection.MessageDescriptor Mediapipe.LandmarkList::pb::Google.Protobuf.IMessage.get_Descriptor()
extern void LandmarkList_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m7BC795FB6D23D27211EB686C07888B2E824C929B (void);
// 0x00000514 System.Void Mediapipe.LandmarkList::.ctor()
extern void LandmarkList__ctor_mB39DEFB418B9FB30F8E826F7398A5D2F2993154B (void);
// 0x00000515 System.Void Mediapipe.LandmarkList::.ctor(Mediapipe.LandmarkList)
extern void LandmarkList__ctor_mAB0C124B8A91ECD95FEEA961A5B7AB35704CBF9A (void);
// 0x00000516 Mediapipe.LandmarkList Mediapipe.LandmarkList::Clone()
extern void LandmarkList_Clone_m162BFB1581722DA392C42B14FF4453FE63924E8A (void);
// 0x00000517 Google.Protobuf.Collections.RepeatedField`1<Mediapipe.Landmark> Mediapipe.LandmarkList::get_Landmark()
extern void LandmarkList_get_Landmark_mA401B3E0FDFEBDE27ABDD919A3B731360D64B4CA (void);
// 0x00000518 System.Boolean Mediapipe.LandmarkList::Equals(System.Object)
extern void LandmarkList_Equals_m67DF4B25C4F479352B3A65E7BE42E3FC87122CAA (void);
// 0x00000519 System.Boolean Mediapipe.LandmarkList::Equals(Mediapipe.LandmarkList)
extern void LandmarkList_Equals_mEF4EFD2347D65884B176A40B3F9C6A316D41C8B4 (void);
// 0x0000051A System.Int32 Mediapipe.LandmarkList::GetHashCode()
extern void LandmarkList_GetHashCode_m9BD8E4A4309FF6C0CEE9C524C47140FB34A2565E (void);
// 0x0000051B System.String Mediapipe.LandmarkList::ToString()
extern void LandmarkList_ToString_m8B21B5CB0BA7CB7F08FED7BA6D77FC136CF2DF3C (void);
// 0x0000051C System.Void Mediapipe.LandmarkList::WriteTo(Google.Protobuf.CodedOutputStream)
extern void LandmarkList_WriteTo_m31B2E7A45E2503C6D1600AB5EF7902F9B5245D07 (void);
// 0x0000051D System.Int32 Mediapipe.LandmarkList::CalculateSize()
extern void LandmarkList_CalculateSize_m53AFAD8AED8C89539F906D99555FDAFF55CC0A5D (void);
// 0x0000051E System.Void Mediapipe.LandmarkList::MergeFrom(Mediapipe.LandmarkList)
extern void LandmarkList_MergeFrom_m8FAB382380EFEDB9FDC963AB56EE75A624A26283 (void);
// 0x0000051F System.Void Mediapipe.LandmarkList::MergeFrom(Google.Protobuf.CodedInputStream)
extern void LandmarkList_MergeFrom_m84474E94FCBB33DCEFCAFE1F005BACDF7D381A67 (void);
// 0x00000520 System.Void Mediapipe.LandmarkList::.cctor()
extern void LandmarkList__cctor_m250046F24E0C7B375EBB230568D7F7952BF88F11 (void);
// 0x00000521 System.Void Mediapipe.LandmarkList/<>c::.cctor()
extern void U3CU3Ec__cctor_mC4A93586FA061998994507B0A58D45D87FD3322E (void);
// 0x00000522 System.Void Mediapipe.LandmarkList/<>c::.ctor()
extern void U3CU3Ec__ctor_m1DC117C8387614686AA191482CB46683CDD5E2DB (void);
// 0x00000523 Mediapipe.LandmarkList Mediapipe.LandmarkList/<>c::<.cctor>b__25_0()
extern void U3CU3Ec_U3C_cctorU3Eb__25_0_m5CE5F4A9C4580793761C70AA78C97B42D722EA4E (void);
// 0x00000524 Google.Protobuf.MessageParser`1<Mediapipe.NormalizedLandmark> Mediapipe.NormalizedLandmark::get_Parser()
extern void NormalizedLandmark_get_Parser_mAFB27D9C602A88961B6D0DFA8D58EF2CCD831740 (void);
// 0x00000525 Google.Protobuf.Reflection.MessageDescriptor Mediapipe.NormalizedLandmark::get_Descriptor()
extern void NormalizedLandmark_get_Descriptor_m10E3E9AEE9FA14C2A083D95360062C148C78E151 (void);
// 0x00000526 Google.Protobuf.Reflection.MessageDescriptor Mediapipe.NormalizedLandmark::pb::Google.Protobuf.IMessage.get_Descriptor()
extern void NormalizedLandmark_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_mDA42C87657A3D0946E43F20E4CC6D9255DBFB498 (void);
// 0x00000527 System.Void Mediapipe.NormalizedLandmark::.ctor()
extern void NormalizedLandmark__ctor_m749D7C799F92DEC13A50FBF151B384E803BFFF52 (void);
// 0x00000528 System.Void Mediapipe.NormalizedLandmark::.ctor(Mediapipe.NormalizedLandmark)
extern void NormalizedLandmark__ctor_mB9A73BE195E17D391BC26DC9C4A9FE7CD3FCF784 (void);
// 0x00000529 Mediapipe.NormalizedLandmark Mediapipe.NormalizedLandmark::Clone()
extern void NormalizedLandmark_Clone_mEA928C357031BFF03D7B3463A38391F589530EFA (void);
// 0x0000052A System.Single Mediapipe.NormalizedLandmark::get_X()
extern void NormalizedLandmark_get_X_mE40CC2764290F7C561315DA843933BE5F5B610D4 (void);
// 0x0000052B System.Void Mediapipe.NormalizedLandmark::set_X(System.Single)
extern void NormalizedLandmark_set_X_m04A3B6D5BE6EE5C0911C86C19BBD6A83C5819564 (void);
// 0x0000052C System.Boolean Mediapipe.NormalizedLandmark::get_HasX()
extern void NormalizedLandmark_get_HasX_m22D9F8C2D714ECF392ABA2B9AF56AD787605F5E1 (void);
// 0x0000052D System.Void Mediapipe.NormalizedLandmark::ClearX()
extern void NormalizedLandmark_ClearX_mF00D31AC4974CD5DFC81875A4A6FC3143DE11626 (void);
// 0x0000052E System.Single Mediapipe.NormalizedLandmark::get_Y()
extern void NormalizedLandmark_get_Y_mA4842900B26232E6A6A4C7D2B15DD3B681C963BD (void);
// 0x0000052F System.Void Mediapipe.NormalizedLandmark::set_Y(System.Single)
extern void NormalizedLandmark_set_Y_mA2BEC843600170080E1B7A49340593CABEBD59C0 (void);
// 0x00000530 System.Boolean Mediapipe.NormalizedLandmark::get_HasY()
extern void NormalizedLandmark_get_HasY_mCAC595075F3F60DA27721FE3108A3C8973EBDD9D (void);
// 0x00000531 System.Void Mediapipe.NormalizedLandmark::ClearY()
extern void NormalizedLandmark_ClearY_mBA1143EE451962D9CCD7D6490DEE4B109E402477 (void);
// 0x00000532 System.Single Mediapipe.NormalizedLandmark::get_Z()
extern void NormalizedLandmark_get_Z_mDE7FBFD528B8674FBA3AF82730582F8BDE63766D (void);
// 0x00000533 System.Void Mediapipe.NormalizedLandmark::set_Z(System.Single)
extern void NormalizedLandmark_set_Z_mAB1E4CD7C16459A82BB35887DD4B01D2021F6F97 (void);
// 0x00000534 System.Boolean Mediapipe.NormalizedLandmark::get_HasZ()
extern void NormalizedLandmark_get_HasZ_m9284EF2F9CC46B11BECC449FADC76878C096F40B (void);
// 0x00000535 System.Void Mediapipe.NormalizedLandmark::ClearZ()
extern void NormalizedLandmark_ClearZ_m43166BF7D6C2B8CC8455116E3ED711FFAD77E03B (void);
// 0x00000536 System.Single Mediapipe.NormalizedLandmark::get_Visibility()
extern void NormalizedLandmark_get_Visibility_m6EF72E21124BDBBACA7AC69388A7FADD0D5D8029 (void);
// 0x00000537 System.Void Mediapipe.NormalizedLandmark::set_Visibility(System.Single)
extern void NormalizedLandmark_set_Visibility_m2206EF29A95BE080A6C4ED5B6229ECFB54DD577B (void);
// 0x00000538 System.Boolean Mediapipe.NormalizedLandmark::get_HasVisibility()
extern void NormalizedLandmark_get_HasVisibility_mB321CAB39C8424E18D5965B70EDEF912174EC3B2 (void);
// 0x00000539 System.Void Mediapipe.NormalizedLandmark::ClearVisibility()
extern void NormalizedLandmark_ClearVisibility_m462FB2CAAF3A6987E414033FC435E9EEA260EC45 (void);
// 0x0000053A System.Single Mediapipe.NormalizedLandmark::get_Presence()
extern void NormalizedLandmark_get_Presence_m5E7E7245E080253A92C68D4DB6ED9682C16223C6 (void);
// 0x0000053B System.Void Mediapipe.NormalizedLandmark::set_Presence(System.Single)
extern void NormalizedLandmark_set_Presence_m08D304502FE3E079389B8B4D2D458DA702EFBAE5 (void);
// 0x0000053C System.Boolean Mediapipe.NormalizedLandmark::get_HasPresence()
extern void NormalizedLandmark_get_HasPresence_m35A999B1B070457E8ADBD7E84DC9F35F802F4FBB (void);
// 0x0000053D System.Void Mediapipe.NormalizedLandmark::ClearPresence()
extern void NormalizedLandmark_ClearPresence_m3BA6187034736183F1237A19186AB4F7C5D98B68 (void);
// 0x0000053E System.Boolean Mediapipe.NormalizedLandmark::Equals(System.Object)
extern void NormalizedLandmark_Equals_m7212B45EE8C95C27A16E61219CF0BC7B63508158 (void);
// 0x0000053F System.Boolean Mediapipe.NormalizedLandmark::Equals(Mediapipe.NormalizedLandmark)
extern void NormalizedLandmark_Equals_m694303789E20659F4755195A665597050EA7E661 (void);
// 0x00000540 System.Int32 Mediapipe.NormalizedLandmark::GetHashCode()
extern void NormalizedLandmark_GetHashCode_m2ABE8B45C235972D5D5E33F4E688B9B6014840F5 (void);
// 0x00000541 System.String Mediapipe.NormalizedLandmark::ToString()
extern void NormalizedLandmark_ToString_m2071AEC6E2374C9F11165F7CDFFE5720E273362E (void);
// 0x00000542 System.Void Mediapipe.NormalizedLandmark::WriteTo(Google.Protobuf.CodedOutputStream)
extern void NormalizedLandmark_WriteTo_m33870DE137A9B7ED976F9A092FFA7B9F0A1930B5 (void);
// 0x00000543 System.Int32 Mediapipe.NormalizedLandmark::CalculateSize()
extern void NormalizedLandmark_CalculateSize_mE3D06A71DF8BE59866DE7837D6A848673525A73A (void);
// 0x00000544 System.Void Mediapipe.NormalizedLandmark::MergeFrom(Mediapipe.NormalizedLandmark)
extern void NormalizedLandmark_MergeFrom_m903A7C27238CE6DC37619530278D38EC5E7124B1 (void);
// 0x00000545 System.Void Mediapipe.NormalizedLandmark::MergeFrom(Google.Protobuf.CodedInputStream)
extern void NormalizedLandmark_MergeFrom_m0348956EE9DEA0CDEE4760CCBBE0B275EE0DB93A (void);
// 0x00000546 System.Void Mediapipe.NormalizedLandmark::.cctor()
extern void NormalizedLandmark__cctor_m14CCB26F286090FB9D91171C19F33F8DF33201C5 (void);
// 0x00000547 System.Void Mediapipe.NormalizedLandmark/<>c::.cctor()
extern void U3CU3Ec__cctor_m72FBB40D58E81E1C5B74A34A01D00CB5F63CD5D1 (void);
// 0x00000548 System.Void Mediapipe.NormalizedLandmark/<>c::.ctor()
extern void U3CU3Ec__ctor_mDED913681E474688419F85C5971B7B8A3AB22E66 (void);
// 0x00000549 Mediapipe.NormalizedLandmark Mediapipe.NormalizedLandmark/<>c::<.cctor>b__66_0()
extern void U3CU3Ec_U3C_cctorU3Eb__66_0_mB5E0E49EE3A15F967842E7CB215964088BC14655 (void);
// 0x0000054A Google.Protobuf.MessageParser`1<Mediapipe.NormalizedLandmarkList> Mediapipe.NormalizedLandmarkList::get_Parser()
extern void NormalizedLandmarkList_get_Parser_m36AB90FD2B5A21789AD5354EA732F998E406B026 (void);
// 0x0000054B Google.Protobuf.Reflection.MessageDescriptor Mediapipe.NormalizedLandmarkList::get_Descriptor()
extern void NormalizedLandmarkList_get_Descriptor_m407AAD482B56B422CBC3A4D471D1929973691A5E (void);
// 0x0000054C Google.Protobuf.Reflection.MessageDescriptor Mediapipe.NormalizedLandmarkList::pb::Google.Protobuf.IMessage.get_Descriptor()
extern void NormalizedLandmarkList_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m36D51DA95E6A8A5FDBCD866B78A2B1A9F49EED5E (void);
// 0x0000054D System.Void Mediapipe.NormalizedLandmarkList::.ctor()
extern void NormalizedLandmarkList__ctor_m01D41F6D909C6F3B3B7EEE22709861C7B29D61FA (void);
// 0x0000054E System.Void Mediapipe.NormalizedLandmarkList::.ctor(Mediapipe.NormalizedLandmarkList)
extern void NormalizedLandmarkList__ctor_m75C1E44F5F089A869ED0E929367C75160499360C (void);
// 0x0000054F Mediapipe.NormalizedLandmarkList Mediapipe.NormalizedLandmarkList::Clone()
extern void NormalizedLandmarkList_Clone_m0858F45BAAB39546DBF5853A5FCB0E2395F34E54 (void);
// 0x00000550 Google.Protobuf.Collections.RepeatedField`1<Mediapipe.NormalizedLandmark> Mediapipe.NormalizedLandmarkList::get_Landmark()
extern void NormalizedLandmarkList_get_Landmark_mEAECE63E9BC383A7E6B2497999B936185BF3B192 (void);
// 0x00000551 System.Boolean Mediapipe.NormalizedLandmarkList::Equals(System.Object)
extern void NormalizedLandmarkList_Equals_m0B854C7B06A98D47E68E15B965FE8EDEE4445CDD (void);
// 0x00000552 System.Boolean Mediapipe.NormalizedLandmarkList::Equals(Mediapipe.NormalizedLandmarkList)
extern void NormalizedLandmarkList_Equals_mF6220EBEBE45837C5C7ABE8A60139A168CFD6555 (void);
// 0x00000553 System.Int32 Mediapipe.NormalizedLandmarkList::GetHashCode()
extern void NormalizedLandmarkList_GetHashCode_m8815B70BC600FCAC3B59DFF49C681B3351F4C493 (void);
// 0x00000554 System.String Mediapipe.NormalizedLandmarkList::ToString()
extern void NormalizedLandmarkList_ToString_mE0E29E7405F94D7B9B37CC75B53A64A473B142E9 (void);
// 0x00000555 System.Void Mediapipe.NormalizedLandmarkList::WriteTo(Google.Protobuf.CodedOutputStream)
extern void NormalizedLandmarkList_WriteTo_m7030BCD0F34648E08815E519717EF288531D0070 (void);
// 0x00000556 System.Int32 Mediapipe.NormalizedLandmarkList::CalculateSize()
extern void NormalizedLandmarkList_CalculateSize_mDBDAE3F1AA8E835537862295EC26955E3ECBAB71 (void);
// 0x00000557 System.Void Mediapipe.NormalizedLandmarkList::MergeFrom(Mediapipe.NormalizedLandmarkList)
extern void NormalizedLandmarkList_MergeFrom_m9E3B0324A50B637AEC4F25E875C6061F5FF0231F (void);
// 0x00000558 System.Void Mediapipe.NormalizedLandmarkList::MergeFrom(Google.Protobuf.CodedInputStream)
extern void NormalizedLandmarkList_MergeFrom_mC7D95F8D50AF1080D9B19AAC9DD77E7376766312 (void);
// 0x00000559 System.Void Mediapipe.NormalizedLandmarkList::.cctor()
extern void NormalizedLandmarkList__cctor_m92754432920CFA2FF5C2368A1E06DD368C5C787B (void);
// 0x0000055A System.Void Mediapipe.NormalizedLandmarkList/<>c::.cctor()
extern void U3CU3Ec__cctor_m63BCCF15DA557FB83F4815836D17A83BA5A6A6EC (void);
// 0x0000055B System.Void Mediapipe.NormalizedLandmarkList/<>c::.ctor()
extern void U3CU3Ec__ctor_m48437BE78DC81BDA3797AD8CE5633609FBFC6115 (void);
// 0x0000055C Mediapipe.NormalizedLandmarkList Mediapipe.NormalizedLandmarkList/<>c::<.cctor>b__25_0()
extern void U3CU3Ec_U3C_cctorU3Eb__25_0_m32AD412BD61826DAA043DD7BEED4D0BA6B028A1E (void);
// 0x0000055D Google.Protobuf.Reflection.FileDescriptor Mediapipe.LocationDataReflection::get_Descriptor()
extern void LocationDataReflection_get_Descriptor_m10DA1F84708031EE1FCC0B3D26ECB9D97930C128 (void);
// 0x0000055E System.Void Mediapipe.LocationDataReflection::.cctor()
extern void LocationDataReflection__cctor_mD1EA42E03A717749D160D543CA692204B40BFCC3 (void);
// 0x0000055F Google.Protobuf.MessageParser`1<Mediapipe.LocationData> Mediapipe.LocationData::get_Parser()
extern void LocationData_get_Parser_m1DB7673AEA9EB45522154A3818213A03C55AE1CF (void);
// 0x00000560 Google.Protobuf.Reflection.MessageDescriptor Mediapipe.LocationData::get_Descriptor()
extern void LocationData_get_Descriptor_m5D0CC50C75A8088D9CBF3D0D4FD78B1F246E562C (void);
// 0x00000561 Google.Protobuf.Reflection.MessageDescriptor Mediapipe.LocationData::pb::Google.Protobuf.IMessage.get_Descriptor()
extern void LocationData_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m86EAC335052E8D69DD1013885F1C07350EB24BA1 (void);
// 0x00000562 System.Void Mediapipe.LocationData::.ctor()
extern void LocationData__ctor_mB604C634DB577934AE64462669C6C3764CB2000D (void);
// 0x00000563 System.Void Mediapipe.LocationData::.ctor(Mediapipe.LocationData)
extern void LocationData__ctor_m6D1AEDF1838BB6FC4150FD00A021962307D60372 (void);
// 0x00000564 Mediapipe.LocationData Mediapipe.LocationData::Clone()
extern void LocationData_Clone_m79CB2770CCAA00A4C9BB99459225994D1291D8F9 (void);
// 0x00000565 Mediapipe.LocationData/Types/Format Mediapipe.LocationData::get_Format()
extern void LocationData_get_Format_m626AA11AF4DB0D37D8D4752EAE9A839966BA0DAE (void);
// 0x00000566 System.Void Mediapipe.LocationData::set_Format(Mediapipe.LocationData/Types/Format)
extern void LocationData_set_Format_mC9E05E4E4B7E37372DA2CFC6026172557FF01C83 (void);
// 0x00000567 System.Boolean Mediapipe.LocationData::get_HasFormat()
extern void LocationData_get_HasFormat_mBF23695BFD2D07ED04565B9BC937F5C62A5C5499 (void);
// 0x00000568 System.Void Mediapipe.LocationData::ClearFormat()
extern void LocationData_ClearFormat_mC84F4F1946BB3CE469972A09C41E2008F4B0A7CB (void);
// 0x00000569 Mediapipe.LocationData/Types/BoundingBox Mediapipe.LocationData::get_BoundingBox()
extern void LocationData_get_BoundingBox_mCF4DE5C5FBD9EF97DFAAD94C88E330985CA5A1EA (void);
// 0x0000056A System.Void Mediapipe.LocationData::set_BoundingBox(Mediapipe.LocationData/Types/BoundingBox)
extern void LocationData_set_BoundingBox_m9AB8A52AEA8A1D03DEA1B03F9C9FCA951A5C0411 (void);
// 0x0000056B System.Boolean Mediapipe.LocationData::get_HasBoundingBox()
extern void LocationData_get_HasBoundingBox_m09F4A29F1BEADDB5D2361A123E147121E27B388B (void);
// 0x0000056C System.Void Mediapipe.LocationData::ClearBoundingBox()
extern void LocationData_ClearBoundingBox_m5EDCE0CB5DC3978B1728178FDBDBA170265FC6F5 (void);
// 0x0000056D Mediapipe.LocationData/Types/RelativeBoundingBox Mediapipe.LocationData::get_RelativeBoundingBox()
extern void LocationData_get_RelativeBoundingBox_m3BD51E37829375F2CFD25FCDDBCB1497AE04C6BE (void);
// 0x0000056E System.Void Mediapipe.LocationData::set_RelativeBoundingBox(Mediapipe.LocationData/Types/RelativeBoundingBox)
extern void LocationData_set_RelativeBoundingBox_mB7C755136E92AA38F3EFFDECC061013257D4678A (void);
// 0x0000056F System.Boolean Mediapipe.LocationData::get_HasRelativeBoundingBox()
extern void LocationData_get_HasRelativeBoundingBox_m3F25BAECD64A2903CCEFBC84B485B8A60A52C47C (void);
// 0x00000570 System.Void Mediapipe.LocationData::ClearRelativeBoundingBox()
extern void LocationData_ClearRelativeBoundingBox_m30E69638BD31A7ABFF6EA8607F0B8716ED807B99 (void);
// 0x00000571 Mediapipe.LocationData/Types/BinaryMask Mediapipe.LocationData::get_Mask()
extern void LocationData_get_Mask_m6AE6D5787D703738447FAC8AC4A9EC70D3DD19CB (void);
// 0x00000572 System.Void Mediapipe.LocationData::set_Mask(Mediapipe.LocationData/Types/BinaryMask)
extern void LocationData_set_Mask_m6E13670DB817D20903AF0E4F9A85DB9D2D36ED01 (void);
// 0x00000573 System.Boolean Mediapipe.LocationData::get_HasMask()
extern void LocationData_get_HasMask_m666C811D63C01D22497C67D8DCE11407B3B9D955 (void);
// 0x00000574 System.Void Mediapipe.LocationData::ClearMask()
extern void LocationData_ClearMask_m76CE8607652C20FFBAEAE510127E861DDB3677CC (void);
// 0x00000575 Google.Protobuf.Collections.RepeatedField`1<Mediapipe.LocationData/Types/RelativeKeypoint> Mediapipe.LocationData::get_RelativeKeypoints()
extern void LocationData_get_RelativeKeypoints_m579BB8AFD5AD8037BE4E73C6CCDDFC3E943B13A8 (void);
// 0x00000576 System.Boolean Mediapipe.LocationData::Equals(System.Object)
extern void LocationData_Equals_m170A452AFFA5AD61A8E91F3A2C414E43B776262E (void);
// 0x00000577 System.Boolean Mediapipe.LocationData::Equals(Mediapipe.LocationData)
extern void LocationData_Equals_m898CD12FC26B6B0589CB403E9C6B29550877A69D (void);
// 0x00000578 System.Int32 Mediapipe.LocationData::GetHashCode()
extern void LocationData_GetHashCode_m307B9D8787153A4DFD1C54C01FE4D9BD05672255 (void);
// 0x00000579 System.String Mediapipe.LocationData::ToString()
extern void LocationData_ToString_m29E48F542440D30B1734C7DB05DFB9CCE5202A05 (void);
// 0x0000057A System.Void Mediapipe.LocationData::WriteTo(Google.Protobuf.CodedOutputStream)
extern void LocationData_WriteTo_mA5DA809FB04E3707BF837E4352734EC94D6B2B95 (void);
// 0x0000057B System.Int32 Mediapipe.LocationData::CalculateSize()
extern void LocationData_CalculateSize_m0E812979AB5E6E5CCEA276AEA562C84BB021E4CB (void);
// 0x0000057C System.Void Mediapipe.LocationData::MergeFrom(Mediapipe.LocationData)
extern void LocationData_MergeFrom_mCAA4BC513485F808DD7A20C6281A4B546A237D05 (void);
// 0x0000057D System.Void Mediapipe.LocationData::MergeFrom(Google.Protobuf.CodedInputStream)
extern void LocationData_MergeFrom_m388838B258FB88CE12864E45E68DD5F22274BE5C (void);
// 0x0000057E System.Void Mediapipe.LocationData::.cctor()
extern void LocationData__cctor_mCDCDE7352F03AE90A6E1FDCD4638AD330E622DA7 (void);
// 0x0000057F Google.Protobuf.MessageParser`1<Mediapipe.LocationData/Types/BoundingBox> Mediapipe.LocationData/Types/BoundingBox::get_Parser()
extern void BoundingBox_get_Parser_m1DBB7A33DE3D22DBCBB0FE66D5AD73A955632B55 (void);
// 0x00000580 Google.Protobuf.Reflection.MessageDescriptor Mediapipe.LocationData/Types/BoundingBox::get_Descriptor()
extern void BoundingBox_get_Descriptor_mBD9A19FFCF588B29131312BCA77DE09DBD172714 (void);
// 0x00000581 Google.Protobuf.Reflection.MessageDescriptor Mediapipe.LocationData/Types/BoundingBox::pb::Google.Protobuf.IMessage.get_Descriptor()
extern void BoundingBox_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_mA4774443BDC66A478BA04DBC6AAF7099823E19C3 (void);
// 0x00000582 System.Void Mediapipe.LocationData/Types/BoundingBox::.ctor()
extern void BoundingBox__ctor_m593B06506BFD3A2B0775E81312CF30B4E63C4D65 (void);
// 0x00000583 System.Void Mediapipe.LocationData/Types/BoundingBox::.ctor(Mediapipe.LocationData/Types/BoundingBox)
extern void BoundingBox__ctor_m627AE04BA213E57A63AEC13B676D7F5B74DC92FF (void);
// 0x00000584 Mediapipe.LocationData/Types/BoundingBox Mediapipe.LocationData/Types/BoundingBox::Clone()
extern void BoundingBox_Clone_m00AE731CD8CDFCDC53DA43A8065BEEC74E9DD5B0 (void);
// 0x00000585 System.Int32 Mediapipe.LocationData/Types/BoundingBox::get_Xmin()
extern void BoundingBox_get_Xmin_mB0E7C4136A3116133C616FBE6AB69DF02F7E38AD (void);
// 0x00000586 System.Void Mediapipe.LocationData/Types/BoundingBox::set_Xmin(System.Int32)
extern void BoundingBox_set_Xmin_m5A56BCD3C1DF932F600F6F1EBFADDFA5514B683F (void);
// 0x00000587 System.Boolean Mediapipe.LocationData/Types/BoundingBox::get_HasXmin()
extern void BoundingBox_get_HasXmin_m85AFBAFD6F8933D2C2014E99AFA9C125C74811D6 (void);
// 0x00000588 System.Void Mediapipe.LocationData/Types/BoundingBox::ClearXmin()
extern void BoundingBox_ClearXmin_m17EB77B29A2B29A9B182986F8C45D7C0DDE4238D (void);
// 0x00000589 System.Int32 Mediapipe.LocationData/Types/BoundingBox::get_Ymin()
extern void BoundingBox_get_Ymin_m7D882E1F987CF2E1A7DD727B945D47282F5CA0E9 (void);
// 0x0000058A System.Void Mediapipe.LocationData/Types/BoundingBox::set_Ymin(System.Int32)
extern void BoundingBox_set_Ymin_mD39ADF868ACD94870F82DEF9753EA086BDFC2F57 (void);
// 0x0000058B System.Boolean Mediapipe.LocationData/Types/BoundingBox::get_HasYmin()
extern void BoundingBox_get_HasYmin_m7FC954880EFE8595C0640F874A8E6A692A7C8BDF (void);
// 0x0000058C System.Void Mediapipe.LocationData/Types/BoundingBox::ClearYmin()
extern void BoundingBox_ClearYmin_m786E94CB3B96169321D4E023816E36B3E111D402 (void);
// 0x0000058D System.Int32 Mediapipe.LocationData/Types/BoundingBox::get_Width()
extern void BoundingBox_get_Width_m1E114AE64C14ADD858BE1EC661D984B2E4ACC4BE (void);
// 0x0000058E System.Void Mediapipe.LocationData/Types/BoundingBox::set_Width(System.Int32)
extern void BoundingBox_set_Width_mD94148B6E2CA61767FCCD4D2AD1235F13681B200 (void);
// 0x0000058F System.Boolean Mediapipe.LocationData/Types/BoundingBox::get_HasWidth()
extern void BoundingBox_get_HasWidth_m6407FE26791689503CD340ED0C8F89733D26AD44 (void);
// 0x00000590 System.Void Mediapipe.LocationData/Types/BoundingBox::ClearWidth()
extern void BoundingBox_ClearWidth_mF1280F4300C6409B29F083612CC43AE23EF27BBD (void);
// 0x00000591 System.Int32 Mediapipe.LocationData/Types/BoundingBox::get_Height()
extern void BoundingBox_get_Height_m783251050DD1092525DECAF847AB87ACB6869B12 (void);
// 0x00000592 System.Void Mediapipe.LocationData/Types/BoundingBox::set_Height(System.Int32)
extern void BoundingBox_set_Height_m16177A060133C9353788A680F943FB1772E1D52A (void);
// 0x00000593 System.Boolean Mediapipe.LocationData/Types/BoundingBox::get_HasHeight()
extern void BoundingBox_get_HasHeight_m90644E0F3320851369CEBD4E83C31F7E87030E2A (void);
// 0x00000594 System.Void Mediapipe.LocationData/Types/BoundingBox::ClearHeight()
extern void BoundingBox_ClearHeight_m39635CDC84989A610ECA78395F36D44D5E752E24 (void);
// 0x00000595 System.Boolean Mediapipe.LocationData/Types/BoundingBox::Equals(System.Object)
extern void BoundingBox_Equals_m59EADA8FA336A51CAA7BDAC92C9659F352E0EA82 (void);
// 0x00000596 System.Boolean Mediapipe.LocationData/Types/BoundingBox::Equals(Mediapipe.LocationData/Types/BoundingBox)
extern void BoundingBox_Equals_mA301D6545DCCF5CDDB551F3F5D3E0EA14BE4FDB9 (void);
// 0x00000597 System.Int32 Mediapipe.LocationData/Types/BoundingBox::GetHashCode()
extern void BoundingBox_GetHashCode_mA2E1A26961E791956B155392365ABE0CFD32C9F4 (void);
// 0x00000598 System.String Mediapipe.LocationData/Types/BoundingBox::ToString()
extern void BoundingBox_ToString_m59B089B9CD4F5D49F89881F869DC857AFC3B78A8 (void);
// 0x00000599 System.Void Mediapipe.LocationData/Types/BoundingBox::WriteTo(Google.Protobuf.CodedOutputStream)
extern void BoundingBox_WriteTo_m5893434F89C8673816C4FA7884F8BD8CEE7B8087 (void);
// 0x0000059A System.Int32 Mediapipe.LocationData/Types/BoundingBox::CalculateSize()
extern void BoundingBox_CalculateSize_m3FF05A537A0E9A624B5FABFE60EBC6FFB10F5A23 (void);
// 0x0000059B System.Void Mediapipe.LocationData/Types/BoundingBox::MergeFrom(Mediapipe.LocationData/Types/BoundingBox)
extern void BoundingBox_MergeFrom_m8FE73B1C9B61C5307C66B1C3D706836388AC89FC (void);
// 0x0000059C System.Void Mediapipe.LocationData/Types/BoundingBox::MergeFrom(Google.Protobuf.CodedInputStream)
extern void BoundingBox_MergeFrom_mC6EFA4D2E99DEC21D436A2A6D90362394A47BFFD (void);
// 0x0000059D System.Void Mediapipe.LocationData/Types/BoundingBox::.cctor()
extern void BoundingBox__cctor_m9303EC3C039DAC049D490D54C950382E9DBC8EBF (void);
// 0x0000059E System.Void Mediapipe.LocationData/Types/BoundingBox/<>c::.cctor()
extern void U3CU3Ec__cctor_m81878484AC353ABC492C7E5064C0C1D0612EDB5E (void);
// 0x0000059F System.Void Mediapipe.LocationData/Types/BoundingBox/<>c::.ctor()
extern void U3CU3Ec__ctor_mC5991B6DA7F718355E99B5617D95E206CB9F00B8 (void);
// 0x000005A0 Mediapipe.LocationData/Types/BoundingBox Mediapipe.LocationData/Types/BoundingBox/<>c::<.cctor>b__57_0()
extern void U3CU3Ec_U3C_cctorU3Eb__57_0_mE286EBC69DEE7A2FCB1A3B6B7A0B3FA52BB3A788 (void);
// 0x000005A1 Google.Protobuf.MessageParser`1<Mediapipe.LocationData/Types/RelativeBoundingBox> Mediapipe.LocationData/Types/RelativeBoundingBox::get_Parser()
extern void RelativeBoundingBox_get_Parser_mBFC6B6ABB1E1CB9EB3789ADD589F5E49E36EB648 (void);
// 0x000005A2 Google.Protobuf.Reflection.MessageDescriptor Mediapipe.LocationData/Types/RelativeBoundingBox::get_Descriptor()
extern void RelativeBoundingBox_get_Descriptor_m4674407D7470FC41C0DF2DB0D2C4838A8D6BE088 (void);
// 0x000005A3 Google.Protobuf.Reflection.MessageDescriptor Mediapipe.LocationData/Types/RelativeBoundingBox::pb::Google.Protobuf.IMessage.get_Descriptor()
extern void RelativeBoundingBox_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m6094EDE44BC491F9C1C3703354C5294F38DF4E72 (void);
// 0x000005A4 System.Void Mediapipe.LocationData/Types/RelativeBoundingBox::.ctor()
extern void RelativeBoundingBox__ctor_mA8BC438A7E04136689A5998E0D7EE0CD98C66B6A (void);
// 0x000005A5 System.Void Mediapipe.LocationData/Types/RelativeBoundingBox::.ctor(Mediapipe.LocationData/Types/RelativeBoundingBox)
extern void RelativeBoundingBox__ctor_m4944F2FED20D1A230D2BF9E5D6A32F725CFCD6EF (void);
// 0x000005A6 Mediapipe.LocationData/Types/RelativeBoundingBox Mediapipe.LocationData/Types/RelativeBoundingBox::Clone()
extern void RelativeBoundingBox_Clone_m0841DF2E2BFB704DFDCE1809E145778315BE1633 (void);
// 0x000005A7 System.Single Mediapipe.LocationData/Types/RelativeBoundingBox::get_Xmin()
extern void RelativeBoundingBox_get_Xmin_mA824E49A567DE0853D917FCDE2874703E15D860E (void);
// 0x000005A8 System.Void Mediapipe.LocationData/Types/RelativeBoundingBox::set_Xmin(System.Single)
extern void RelativeBoundingBox_set_Xmin_m08AB518F6ADFA534D1FD830A0905C12F4BB9F12C (void);
// 0x000005A9 System.Boolean Mediapipe.LocationData/Types/RelativeBoundingBox::get_HasXmin()
extern void RelativeBoundingBox_get_HasXmin_m653570E1DC863D11A5094DF3056B7EC51E060379 (void);
// 0x000005AA System.Void Mediapipe.LocationData/Types/RelativeBoundingBox::ClearXmin()
extern void RelativeBoundingBox_ClearXmin_m28CEF281DAEC7AF7C8ED9F0E471553717368B8C3 (void);
// 0x000005AB System.Single Mediapipe.LocationData/Types/RelativeBoundingBox::get_Ymin()
extern void RelativeBoundingBox_get_Ymin_m1C551336D05CF3E43CC9144052E48A7EF4F4A9EA (void);
// 0x000005AC System.Void Mediapipe.LocationData/Types/RelativeBoundingBox::set_Ymin(System.Single)
extern void RelativeBoundingBox_set_Ymin_m99DF07401CE9D7CEB9E91A800797855197BC9091 (void);
// 0x000005AD System.Boolean Mediapipe.LocationData/Types/RelativeBoundingBox::get_HasYmin()
extern void RelativeBoundingBox_get_HasYmin_m5D1543550F75D43D8017FE39E2AAE97B9DD26376 (void);
// 0x000005AE System.Void Mediapipe.LocationData/Types/RelativeBoundingBox::ClearYmin()
extern void RelativeBoundingBox_ClearYmin_mD759F2E5D785928C94DA4D2A89BD34A34BA7F11B (void);
// 0x000005AF System.Single Mediapipe.LocationData/Types/RelativeBoundingBox::get_Width()
extern void RelativeBoundingBox_get_Width_mF398F77C41779AD93A1F7CE577A383BE0791AAAD (void);
// 0x000005B0 System.Void Mediapipe.LocationData/Types/RelativeBoundingBox::set_Width(System.Single)
extern void RelativeBoundingBox_set_Width_mF74D991D6928BD39FCC5CAC080266847FE51A474 (void);
// 0x000005B1 System.Boolean Mediapipe.LocationData/Types/RelativeBoundingBox::get_HasWidth()
extern void RelativeBoundingBox_get_HasWidth_mFCD5FD1E1CBE9EB35FE211E3B1A50A4466927AD7 (void);
// 0x000005B2 System.Void Mediapipe.LocationData/Types/RelativeBoundingBox::ClearWidth()
extern void RelativeBoundingBox_ClearWidth_m72C70182A243C7353F98E8899B82EF42648B98E1 (void);
// 0x000005B3 System.Single Mediapipe.LocationData/Types/RelativeBoundingBox::get_Height()
extern void RelativeBoundingBox_get_Height_m3A22A85EFFBBAA0668FF26FC0A6341780BF070C7 (void);
// 0x000005B4 System.Void Mediapipe.LocationData/Types/RelativeBoundingBox::set_Height(System.Single)
extern void RelativeBoundingBox_set_Height_m90EC9B0116EFF1225C439DBEEDA5F3AE7A77A5DA (void);
// 0x000005B5 System.Boolean Mediapipe.LocationData/Types/RelativeBoundingBox::get_HasHeight()
extern void RelativeBoundingBox_get_HasHeight_m310674D4DB7791AF4D82443FB42B14202E19A072 (void);
// 0x000005B6 System.Void Mediapipe.LocationData/Types/RelativeBoundingBox::ClearHeight()
extern void RelativeBoundingBox_ClearHeight_mD70EE677AB7F7D7C9573EF087BD90D08794B41D7 (void);
// 0x000005B7 System.Boolean Mediapipe.LocationData/Types/RelativeBoundingBox::Equals(System.Object)
extern void RelativeBoundingBox_Equals_m4A16727F878D1A1CF29E042D8088470E058DD7DB (void);
// 0x000005B8 System.Boolean Mediapipe.LocationData/Types/RelativeBoundingBox::Equals(Mediapipe.LocationData/Types/RelativeBoundingBox)
extern void RelativeBoundingBox_Equals_m45535830904520641BA7D02C53DCBE8A548359B4 (void);
// 0x000005B9 System.Int32 Mediapipe.LocationData/Types/RelativeBoundingBox::GetHashCode()
extern void RelativeBoundingBox_GetHashCode_m6E36E0BA9CABBBBB75860C51EB1C253702E47E62 (void);
// 0x000005BA System.String Mediapipe.LocationData/Types/RelativeBoundingBox::ToString()
extern void RelativeBoundingBox_ToString_mD096BEBCC911A0831D49B8ADB2393668CB1E07AA (void);
// 0x000005BB System.Void Mediapipe.LocationData/Types/RelativeBoundingBox::WriteTo(Google.Protobuf.CodedOutputStream)
extern void RelativeBoundingBox_WriteTo_mB80D4F45F77E83F7B6A170F6A3BC48001C0FACF0 (void);
// 0x000005BC System.Int32 Mediapipe.LocationData/Types/RelativeBoundingBox::CalculateSize()
extern void RelativeBoundingBox_CalculateSize_mCE3CB30EDFEAE4B677F7EF03C94E74FD0F39566E (void);
// 0x000005BD System.Void Mediapipe.LocationData/Types/RelativeBoundingBox::MergeFrom(Mediapipe.LocationData/Types/RelativeBoundingBox)
extern void RelativeBoundingBox_MergeFrom_m0E9CE082728E5F9669448DDC651D3B424F8E444F (void);
// 0x000005BE System.Void Mediapipe.LocationData/Types/RelativeBoundingBox::MergeFrom(Google.Protobuf.CodedInputStream)
extern void RelativeBoundingBox_MergeFrom_m47BCC3409807A67E5FAEBF440FDD9155DFE0DA20 (void);
// 0x000005BF System.Void Mediapipe.LocationData/Types/RelativeBoundingBox::.cctor()
extern void RelativeBoundingBox__cctor_m73700068D4F921A0202218DB1A63F00E2ABDD272 (void);
// 0x000005C0 System.Void Mediapipe.LocationData/Types/RelativeBoundingBox/<>c::.cctor()
extern void U3CU3Ec__cctor_m9C4D9AA9735915DE0A42AB5D2DC837BB8102502F (void);
// 0x000005C1 System.Void Mediapipe.LocationData/Types/RelativeBoundingBox/<>c::.ctor()
extern void U3CU3Ec__ctor_m7551984D2E8DB82351E841CD613D0E035584802B (void);
// 0x000005C2 Mediapipe.LocationData/Types/RelativeBoundingBox Mediapipe.LocationData/Types/RelativeBoundingBox/<>c::<.cctor>b__57_0()
extern void U3CU3Ec_U3C_cctorU3Eb__57_0_mFD2480E32A339CFC34E7DDBF66CA0F54DC88414F (void);
// 0x000005C3 Google.Protobuf.MessageParser`1<Mediapipe.LocationData/Types/BinaryMask> Mediapipe.LocationData/Types/BinaryMask::get_Parser()
extern void BinaryMask_get_Parser_m35F8F45AF7AF30C764D7055D771C6380947236DB (void);
// 0x000005C4 Google.Protobuf.Reflection.MessageDescriptor Mediapipe.LocationData/Types/BinaryMask::get_Descriptor()
extern void BinaryMask_get_Descriptor_m5FC425BA1F607161F5D6A49572960035DCA88109 (void);
// 0x000005C5 Google.Protobuf.Reflection.MessageDescriptor Mediapipe.LocationData/Types/BinaryMask::pb::Google.Protobuf.IMessage.get_Descriptor()
extern void BinaryMask_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_mE6C87ED2CD0FE3FC60FC151C4801AF6A6C11E9A8 (void);
// 0x000005C6 System.Void Mediapipe.LocationData/Types/BinaryMask::.ctor()
extern void BinaryMask__ctor_m8F90D268C87AA0A2998B869A679E486B6784B069 (void);
// 0x000005C7 System.Void Mediapipe.LocationData/Types/BinaryMask::.ctor(Mediapipe.LocationData/Types/BinaryMask)
extern void BinaryMask__ctor_m03BA73D9ACA551B4101934F7A24C4AFB1061B712 (void);
// 0x000005C8 Mediapipe.LocationData/Types/BinaryMask Mediapipe.LocationData/Types/BinaryMask::Clone()
extern void BinaryMask_Clone_m686B51B7DC6D0BAB62F6C639B6ADE99AC8A9AA99 (void);
// 0x000005C9 System.Int32 Mediapipe.LocationData/Types/BinaryMask::get_Width()
extern void BinaryMask_get_Width_m4B4F724F4A91DB9B31EFC393F0291DB100363F7A (void);
// 0x000005CA System.Void Mediapipe.LocationData/Types/BinaryMask::set_Width(System.Int32)
extern void BinaryMask_set_Width_m6DB2CC176F291AB00E349FE3B1DCCE33A25C6F4C (void);
// 0x000005CB System.Boolean Mediapipe.LocationData/Types/BinaryMask::get_HasWidth()
extern void BinaryMask_get_HasWidth_m0BFF1D2FC1D77AA7448314E981282040CB7FB961 (void);
// 0x000005CC System.Void Mediapipe.LocationData/Types/BinaryMask::ClearWidth()
extern void BinaryMask_ClearWidth_m7C972B069D508D1FE0EA7991A4C5362201988D9D (void);
// 0x000005CD System.Int32 Mediapipe.LocationData/Types/BinaryMask::get_Height()
extern void BinaryMask_get_Height_m5236D71E1EFD17E480B9610DD4FCBC0428A8662A (void);
// 0x000005CE System.Void Mediapipe.LocationData/Types/BinaryMask::set_Height(System.Int32)
extern void BinaryMask_set_Height_mEE156A51EFACD8DD43A776861BF94C6B69F6E039 (void);
// 0x000005CF System.Boolean Mediapipe.LocationData/Types/BinaryMask::get_HasHeight()
extern void BinaryMask_get_HasHeight_m2EF3E270CA2CDF21602CF9FC65F5847AF7F943FC (void);
// 0x000005D0 System.Void Mediapipe.LocationData/Types/BinaryMask::ClearHeight()
extern void BinaryMask_ClearHeight_m0CB9F998280EEB1A27CA8EF7AD9776ECB07A5DD6 (void);
// 0x000005D1 Mediapipe.Rasterization Mediapipe.LocationData/Types/BinaryMask::get_Rasterization()
extern void BinaryMask_get_Rasterization_mB75FF0B494A6138FC4DCAA6B56AC31E2D588E326 (void);
// 0x000005D2 System.Void Mediapipe.LocationData/Types/BinaryMask::set_Rasterization(Mediapipe.Rasterization)
extern void BinaryMask_set_Rasterization_m66B08D6D62AC649A943C28F112A5CBADBB609ED2 (void);
// 0x000005D3 System.Boolean Mediapipe.LocationData/Types/BinaryMask::get_HasRasterization()
extern void BinaryMask_get_HasRasterization_m74406A4905ECED17BB98A8D103FF9DFADAF37AB1 (void);
// 0x000005D4 System.Void Mediapipe.LocationData/Types/BinaryMask::ClearRasterization()
extern void BinaryMask_ClearRasterization_m652D34C3A5C6168C317903658D48ED049EF71B9A (void);
// 0x000005D5 System.Boolean Mediapipe.LocationData/Types/BinaryMask::Equals(System.Object)
extern void BinaryMask_Equals_mBF78A6B0C1E5516AD385E4C4D22851C5BA57A5F1 (void);
// 0x000005D6 System.Boolean Mediapipe.LocationData/Types/BinaryMask::Equals(Mediapipe.LocationData/Types/BinaryMask)
extern void BinaryMask_Equals_m49D94332523281DBAC6789DA4D6C2179E28EF83A (void);
// 0x000005D7 System.Int32 Mediapipe.LocationData/Types/BinaryMask::GetHashCode()
extern void BinaryMask_GetHashCode_m9E4FFB7CE411C64CAD98DBA291AF0E05C61A8FE5 (void);
// 0x000005D8 System.String Mediapipe.LocationData/Types/BinaryMask::ToString()
extern void BinaryMask_ToString_mF795F2557EF8ED863BBD2F1C459778A61115700B (void);
// 0x000005D9 System.Void Mediapipe.LocationData/Types/BinaryMask::WriteTo(Google.Protobuf.CodedOutputStream)
extern void BinaryMask_WriteTo_mA261BA797008FF73839EBA7E0592F0DEBD415564 (void);
// 0x000005DA System.Int32 Mediapipe.LocationData/Types/BinaryMask::CalculateSize()
extern void BinaryMask_CalculateSize_mC22E19FAE984002D7163988734767B26FC5823D1 (void);
// 0x000005DB System.Void Mediapipe.LocationData/Types/BinaryMask::MergeFrom(Mediapipe.LocationData/Types/BinaryMask)
extern void BinaryMask_MergeFrom_m6BE953E5A769220962214F9F05E0FE319C1B652B (void);
// 0x000005DC System.Void Mediapipe.LocationData/Types/BinaryMask::MergeFrom(Google.Protobuf.CodedInputStream)
extern void BinaryMask_MergeFrom_m5DA0857C4C0936C91F2EB06AE3F299518918D776 (void);
// 0x000005DD System.Void Mediapipe.LocationData/Types/BinaryMask::.cctor()
extern void BinaryMask__cctor_m62770E71EE03C05597D6DBA03B6151EA8F144082 (void);
// 0x000005DE System.Void Mediapipe.LocationData/Types/BinaryMask/<>c::.cctor()
extern void U3CU3Ec__cctor_m53224E031342A9440AB1DB6183C97CF1380D503C (void);
// 0x000005DF System.Void Mediapipe.LocationData/Types/BinaryMask/<>c::.ctor()
extern void U3CU3Ec__ctor_mECF34F0FBCE97F2A2EC31002CF38B34B847F3DCC (void);
// 0x000005E0 Mediapipe.LocationData/Types/BinaryMask Mediapipe.LocationData/Types/BinaryMask/<>c::<.cctor>b__47_0()
extern void U3CU3Ec_U3C_cctorU3Eb__47_0_m0AC3A5539D747E60C82CC18CF720203910238B15 (void);
// 0x000005E1 Google.Protobuf.MessageParser`1<Mediapipe.LocationData/Types/RelativeKeypoint> Mediapipe.LocationData/Types/RelativeKeypoint::get_Parser()
extern void RelativeKeypoint_get_Parser_m9C9717BBDB4544FCBE2D5C5C6CF13B39F2D8D32F (void);
// 0x000005E2 Google.Protobuf.Reflection.MessageDescriptor Mediapipe.LocationData/Types/RelativeKeypoint::get_Descriptor()
extern void RelativeKeypoint_get_Descriptor_m2384BCFD3E8C51A97FB766A283D7C939F94A8EB9 (void);
// 0x000005E3 Google.Protobuf.Reflection.MessageDescriptor Mediapipe.LocationData/Types/RelativeKeypoint::pb::Google.Protobuf.IMessage.get_Descriptor()
extern void RelativeKeypoint_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_mC1A449B5DEFD4BEFAE1CB4C3A96741D3BC79B114 (void);
// 0x000005E4 System.Void Mediapipe.LocationData/Types/RelativeKeypoint::.ctor()
extern void RelativeKeypoint__ctor_m206C6B4CC8BFC83247B3CF0E9B6C1DB3EF9FD239 (void);
// 0x000005E5 System.Void Mediapipe.LocationData/Types/RelativeKeypoint::.ctor(Mediapipe.LocationData/Types/RelativeKeypoint)
extern void RelativeKeypoint__ctor_mEAE05077643B9B02D325976BFEA6673DEB47ACB9 (void);
// 0x000005E6 Mediapipe.LocationData/Types/RelativeKeypoint Mediapipe.LocationData/Types/RelativeKeypoint::Clone()
extern void RelativeKeypoint_Clone_mB4A646D4224167F8867687AA163FFDB1772B344B (void);
// 0x000005E7 System.Single Mediapipe.LocationData/Types/RelativeKeypoint::get_X()
extern void RelativeKeypoint_get_X_m8ACA03E4AFF97057B0433DA5C319705E4A4FAF1D (void);
// 0x000005E8 System.Void Mediapipe.LocationData/Types/RelativeKeypoint::set_X(System.Single)
extern void RelativeKeypoint_set_X_m9F9D071BF7DD14BF386A4714C41A6B720267CE33 (void);
// 0x000005E9 System.Boolean Mediapipe.LocationData/Types/RelativeKeypoint::get_HasX()
extern void RelativeKeypoint_get_HasX_m7A2FE60D3B3C41FA9FDC1A3ECEFF51EE45CF3B8F (void);
// 0x000005EA System.Void Mediapipe.LocationData/Types/RelativeKeypoint::ClearX()
extern void RelativeKeypoint_ClearX_mA536C8F3890EAC060547009BFB0E7159C702549E (void);
// 0x000005EB System.Single Mediapipe.LocationData/Types/RelativeKeypoint::get_Y()
extern void RelativeKeypoint_get_Y_m2EF4322968355594C301EACEE016973FB1656300 (void);
// 0x000005EC System.Void Mediapipe.LocationData/Types/RelativeKeypoint::set_Y(System.Single)
extern void RelativeKeypoint_set_Y_mDEC7C9A9B6012EFD34D87969D7D8B0A02EDCAB0F (void);
// 0x000005ED System.Boolean Mediapipe.LocationData/Types/RelativeKeypoint::get_HasY()
extern void RelativeKeypoint_get_HasY_mED67AC03587672E7511794925AB8F7BB85627D4E (void);
// 0x000005EE System.Void Mediapipe.LocationData/Types/RelativeKeypoint::ClearY()
extern void RelativeKeypoint_ClearY_mBDEF3488FCD528959DEA9E1D490013226E8C3ADC (void);
// 0x000005EF System.String Mediapipe.LocationData/Types/RelativeKeypoint::get_KeypointLabel()
extern void RelativeKeypoint_get_KeypointLabel_m9CF672A12F0D6CA820DFD71E5FDD421788558F52 (void);
// 0x000005F0 System.Void Mediapipe.LocationData/Types/RelativeKeypoint::set_KeypointLabel(System.String)
extern void RelativeKeypoint_set_KeypointLabel_m52406523D13778F223B2242F23EEFB030CAF2674 (void);
// 0x000005F1 System.Boolean Mediapipe.LocationData/Types/RelativeKeypoint::get_HasKeypointLabel()
extern void RelativeKeypoint_get_HasKeypointLabel_m46A28C06B23F21F9758222489D7FEB1691774F2B (void);
// 0x000005F2 System.Void Mediapipe.LocationData/Types/RelativeKeypoint::ClearKeypointLabel()
extern void RelativeKeypoint_ClearKeypointLabel_m3706CBCDA0D55D054051AFFF760EF0590F75698F (void);
// 0x000005F3 System.Single Mediapipe.LocationData/Types/RelativeKeypoint::get_Score()
extern void RelativeKeypoint_get_Score_m433B50EBB0F293FA3B86D4D9934E87503313B50C (void);
// 0x000005F4 System.Void Mediapipe.LocationData/Types/RelativeKeypoint::set_Score(System.Single)
extern void RelativeKeypoint_set_Score_m0BB4A989FAB1C79C9BE766FECA6B9F9CE6E6B131 (void);
// 0x000005F5 System.Boolean Mediapipe.LocationData/Types/RelativeKeypoint::get_HasScore()
extern void RelativeKeypoint_get_HasScore_m56D9CDFA50CB11D7F19BF1F8209DFBCA20987586 (void);
// 0x000005F6 System.Void Mediapipe.LocationData/Types/RelativeKeypoint::ClearScore()
extern void RelativeKeypoint_ClearScore_m05253CED8244B4BEE218FD9F8F1299A536A68F34 (void);
// 0x000005F7 System.Boolean Mediapipe.LocationData/Types/RelativeKeypoint::Equals(System.Object)
extern void RelativeKeypoint_Equals_m3F57F2FDEDA1A3E18FB46700957235704010CA1C (void);
// 0x000005F8 System.Boolean Mediapipe.LocationData/Types/RelativeKeypoint::Equals(Mediapipe.LocationData/Types/RelativeKeypoint)
extern void RelativeKeypoint_Equals_m704069F17BBCA172946EC75767DE0558DE5796C5 (void);
// 0x000005F9 System.Int32 Mediapipe.LocationData/Types/RelativeKeypoint::GetHashCode()
extern void RelativeKeypoint_GetHashCode_m52ACCE197F999EFC3DC86317A382DD805532C5B9 (void);
// 0x000005FA System.String Mediapipe.LocationData/Types/RelativeKeypoint::ToString()
extern void RelativeKeypoint_ToString_mF649593D1A01012190C83FCAC793FE30E0A2ADB7 (void);
// 0x000005FB System.Void Mediapipe.LocationData/Types/RelativeKeypoint::WriteTo(Google.Protobuf.CodedOutputStream)
extern void RelativeKeypoint_WriteTo_mBF98F261D95B71894CAA982839EC4F8A6D99ABC1 (void);
// 0x000005FC System.Int32 Mediapipe.LocationData/Types/RelativeKeypoint::CalculateSize()
extern void RelativeKeypoint_CalculateSize_m5B1B5C66064B9BF331664023DBEEBAC75DFD9169 (void);
// 0x000005FD System.Void Mediapipe.LocationData/Types/RelativeKeypoint::MergeFrom(Mediapipe.LocationData/Types/RelativeKeypoint)
extern void RelativeKeypoint_MergeFrom_m198362F71ABDF0359BA9874F2E7AF59F52B14C30 (void);
// 0x000005FE System.Void Mediapipe.LocationData/Types/RelativeKeypoint::MergeFrom(Google.Protobuf.CodedInputStream)
extern void RelativeKeypoint_MergeFrom_mE832CEDC0F6D893DA89014206624FDC0F0009AA9 (void);
// 0x000005FF System.Void Mediapipe.LocationData/Types/RelativeKeypoint::.cctor()
extern void RelativeKeypoint__cctor_mBB5BEE8C2F2D40C6483B4F8BF37E074FE36D05FB (void);
// 0x00000600 System.Void Mediapipe.LocationData/Types/RelativeKeypoint/<>c::.cctor()
extern void U3CU3Ec__cctor_mA54C7CE031E534AFE5F4D80063173765EDFA251B (void);
// 0x00000601 System.Void Mediapipe.LocationData/Types/RelativeKeypoint/<>c::.ctor()
extern void U3CU3Ec__ctor_m8708B900487EC35C2D89C9DE014FF940C9ECA8A6 (void);
// 0x00000602 Mediapipe.LocationData/Types/RelativeKeypoint Mediapipe.LocationData/Types/RelativeKeypoint/<>c::<.cctor>b__57_0()
extern void U3CU3Ec_U3C_cctorU3Eb__57_0_m4BB9334CFBC10EF019310D07BB29BBFA4B9F7E7C (void);
// 0x00000603 System.Void Mediapipe.LocationData/<>c::.cctor()
extern void U3CU3Ec__cctor_mBDCE1F165C8F6734DFE01FBD887723DC6B6D7569 (void);
// 0x00000604 System.Void Mediapipe.LocationData/<>c::.ctor()
extern void U3CU3Ec__ctor_m9231E3CA1ED8FB680CF1BC88F87E44BE2C261A73 (void);
// 0x00000605 Mediapipe.LocationData Mediapipe.LocationData/<>c::<.cctor>b__60_0()
extern void U3CU3Ec_U3C_cctorU3Eb__60_0_m77F9F0702A521CD1FB473BC21A785C3F058E63E0 (void);
// 0x00000606 Google.Protobuf.Reflection.FileDescriptor Mediapipe.MatrixDataReflection::get_Descriptor()
extern void MatrixDataReflection_get_Descriptor_m5CCFD7E6D7AC04D96C29BC6D156BC7E586176486 (void);
// 0x00000607 System.Void Mediapipe.MatrixDataReflection::.cctor()
extern void MatrixDataReflection__cctor_mA46816038F39D83AED26DF6DB57EB3DB7A0E3637 (void);
// 0x00000608 Google.Protobuf.MessageParser`1<Mediapipe.MatrixData> Mediapipe.MatrixData::get_Parser()
extern void MatrixData_get_Parser_m03064DB1EE5CC94DCDC8ED3B102D623572F48491 (void);
// 0x00000609 Google.Protobuf.Reflection.MessageDescriptor Mediapipe.MatrixData::get_Descriptor()
extern void MatrixData_get_Descriptor_m4B70A6055A0487BCC5E28D764D8CA416C5D013BD (void);
// 0x0000060A Google.Protobuf.Reflection.MessageDescriptor Mediapipe.MatrixData::pb::Google.Protobuf.IMessage.get_Descriptor()
extern void MatrixData_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m88D0497C1171A845B58B473980980E36D9CAA532 (void);
// 0x0000060B System.Void Mediapipe.MatrixData::.ctor()
extern void MatrixData__ctor_m08760B404A31B2AE0141BB152C5765B20FE3D999 (void);
// 0x0000060C System.Void Mediapipe.MatrixData::.ctor(Mediapipe.MatrixData)
extern void MatrixData__ctor_m190F97D56239F0C19F454C92501EABC70460EF0D (void);
// 0x0000060D Mediapipe.MatrixData Mediapipe.MatrixData::Clone()
extern void MatrixData_Clone_mCE53C6458C28502B3440957A054F15E1555A2906 (void);
// 0x0000060E System.Int32 Mediapipe.MatrixData::get_Rows()
extern void MatrixData_get_Rows_mF5D13C87219A58BC2045D5EAC64C202929F9044A (void);
// 0x0000060F System.Void Mediapipe.MatrixData::set_Rows(System.Int32)
extern void MatrixData_set_Rows_m782483E53038F4F47271A98FB550E881C2D76A01 (void);
// 0x00000610 System.Boolean Mediapipe.MatrixData::get_HasRows()
extern void MatrixData_get_HasRows_mF98D0F13962CCF8D765DB83EEF7D71CFA5A54A6E (void);
// 0x00000611 System.Void Mediapipe.MatrixData::ClearRows()
extern void MatrixData_ClearRows_mE0E54CD921FE4772B8F80BBDB6B3026AA9077887 (void);
// 0x00000612 System.Int32 Mediapipe.MatrixData::get_Cols()
extern void MatrixData_get_Cols_mB80AB3D775218CA37DB6A1B0F969E7E548C92208 (void);
// 0x00000613 System.Void Mediapipe.MatrixData::set_Cols(System.Int32)
extern void MatrixData_set_Cols_mF48CA74DBAE0214F2F37C586719CDFBDD7E74A1D (void);
// 0x00000614 System.Boolean Mediapipe.MatrixData::get_HasCols()
extern void MatrixData_get_HasCols_m00CB3A16878979C1DFD25CEA35933F5E78E23B29 (void);
// 0x00000615 System.Void Mediapipe.MatrixData::ClearCols()
extern void MatrixData_ClearCols_m05C85B672E3F314D316EAFF6C74CFC81278869DF (void);
// 0x00000616 Google.Protobuf.Collections.RepeatedField`1<System.Single> Mediapipe.MatrixData::get_PackedData()
extern void MatrixData_get_PackedData_mD14087D33C47D023620184D18822A5707340C9EE (void);
// 0x00000617 Mediapipe.MatrixData/Types/Layout Mediapipe.MatrixData::get_Layout()
extern void MatrixData_get_Layout_mEF36C6E7C872283A9F24E3E23D0D9EAAB8AFBA9A (void);
// 0x00000618 System.Void Mediapipe.MatrixData::set_Layout(Mediapipe.MatrixData/Types/Layout)
extern void MatrixData_set_Layout_m5471AA4CC088DCB316675F93588865D9D71742FC (void);
// 0x00000619 System.Boolean Mediapipe.MatrixData::get_HasLayout()
extern void MatrixData_get_HasLayout_m7CC39257BADB62DB00DE77BA7DB54198481073C9 (void);
// 0x0000061A System.Void Mediapipe.MatrixData::ClearLayout()
extern void MatrixData_ClearLayout_m2140EFC522A01F79894555FBD578D33E12C684B1 (void);
// 0x0000061B System.Boolean Mediapipe.MatrixData::Equals(System.Object)
extern void MatrixData_Equals_mAB317070368E9C8FAC655E625805350B1FB0C314 (void);
// 0x0000061C System.Boolean Mediapipe.MatrixData::Equals(Mediapipe.MatrixData)
extern void MatrixData_Equals_mFF2B5393EA1A1F13B59FE28679F4FD1946BA8D42 (void);
// 0x0000061D System.Int32 Mediapipe.MatrixData::GetHashCode()
extern void MatrixData_GetHashCode_m43DDD0B77ECB6989C34D865D720E8E4F60AB8113 (void);
// 0x0000061E System.String Mediapipe.MatrixData::ToString()
extern void MatrixData_ToString_m14D4258EAB83BC97E759D73DAD0B8BBB2B78DEB3 (void);
// 0x0000061F System.Void Mediapipe.MatrixData::WriteTo(Google.Protobuf.CodedOutputStream)
extern void MatrixData_WriteTo_m3B87386A2E13C7A8B682CCA7020DF01A57860798 (void);
// 0x00000620 System.Int32 Mediapipe.MatrixData::CalculateSize()
extern void MatrixData_CalculateSize_mB7488B8687D6F10921E34FCB8CCBA3C565BE5CD5 (void);
// 0x00000621 System.Void Mediapipe.MatrixData::MergeFrom(Mediapipe.MatrixData)
extern void MatrixData_MergeFrom_m0AB33B94558F79353F9AE8E471A759FA65E828FB (void);
// 0x00000622 System.Void Mediapipe.MatrixData::MergeFrom(Google.Protobuf.CodedInputStream)
extern void MatrixData_MergeFrom_m547674A1C5CD7A86BF22973FA6FDF51A6DD056F5 (void);
// 0x00000623 System.Void Mediapipe.MatrixData::.cctor()
extern void MatrixData__cctor_mA852AA412B44AF096654CF62CC6AFDD68A86F951 (void);
// 0x00000624 System.Void Mediapipe.MatrixData/<>c::.cctor()
extern void U3CU3Ec__cctor_mD011CE8C61A4BEB914C8360E14D19C1EB1280F08 (void);
// 0x00000625 System.Void Mediapipe.MatrixData/<>c::.ctor()
extern void U3CU3Ec__ctor_mD91D1C768B4235B1176259F1B4A58E49F749A1A9 (void);
// 0x00000626 Mediapipe.MatrixData Mediapipe.MatrixData/<>c::<.cctor>b__54_0()
extern void U3CU3Ec_U3C_cctorU3Eb__54_0_m75179DF0A43B6F14908A0FFE50DD65366F261042 (void);
// 0x00000627 Google.Protobuf.Reflection.FileDescriptor Mediapipe.MediapipeOptionsReflection::get_Descriptor()
extern void MediapipeOptionsReflection_get_Descriptor_m1539AE6F7DB58086644D716C90D4319D4BCDD589 (void);
// 0x00000628 System.Void Mediapipe.MediapipeOptionsReflection::.cctor()
extern void MediapipeOptionsReflection__cctor_m7C03A0863EE17286756848F4247FBE4412D67D55 (void);
// 0x00000629 Google.Protobuf.ExtensionSet`1<Mediapipe.MediaPipeOptions> Mediapipe.MediaPipeOptions::get__Extensions()
extern void MediaPipeOptions_get__Extensions_m5EDCC272A1B8D9D79F4467609283AC8351374889 (void);
// 0x0000062A Google.Protobuf.MessageParser`1<Mediapipe.MediaPipeOptions> Mediapipe.MediaPipeOptions::get_Parser()
extern void MediaPipeOptions_get_Parser_m5708023B661DED9AD0D27FB3C3C20D05D41F9482 (void);
// 0x0000062B Google.Protobuf.Reflection.MessageDescriptor Mediapipe.MediaPipeOptions::get_Descriptor()
extern void MediaPipeOptions_get_Descriptor_m2AD34E0B99F7D57488C31DC58C2DF66272366EC3 (void);
// 0x0000062C Google.Protobuf.Reflection.MessageDescriptor Mediapipe.MediaPipeOptions::pb::Google.Protobuf.IMessage.get_Descriptor()
extern void MediaPipeOptions_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_mF7ECCDF5483531F05891CB3CB88801E2FE1CCE36 (void);
// 0x0000062D System.Void Mediapipe.MediaPipeOptions::.ctor()
extern void MediaPipeOptions__ctor_m94ECAD8AA33546C341D7764097D40351B2E4AB54 (void);
// 0x0000062E System.Void Mediapipe.MediaPipeOptions::.ctor(Mediapipe.MediaPipeOptions)
extern void MediaPipeOptions__ctor_m54624D466F21E59CB280A7DB9FA5BA1B99ED1C1C (void);
// 0x0000062F Mediapipe.MediaPipeOptions Mediapipe.MediaPipeOptions::Clone()
extern void MediaPipeOptions_Clone_m4BDE025E942E472425387DEF8F63146B8C2B6EC4 (void);
// 0x00000630 System.Boolean Mediapipe.MediaPipeOptions::Equals(System.Object)
extern void MediaPipeOptions_Equals_m5A11BC1C09E74CC69DB83298DBA5003DD8A65366 (void);
// 0x00000631 System.Boolean Mediapipe.MediaPipeOptions::Equals(Mediapipe.MediaPipeOptions)
extern void MediaPipeOptions_Equals_mED7E91CF2EDBAFBCE5E1C2BA2374F0A4E5F48006 (void);
// 0x00000632 System.Int32 Mediapipe.MediaPipeOptions::GetHashCode()
extern void MediaPipeOptions_GetHashCode_m6C05D27EF2C543C70EC43C7DB712D028BDBE654E (void);
// 0x00000633 System.String Mediapipe.MediaPipeOptions::ToString()
extern void MediaPipeOptions_ToString_m46611F2B164EAE51886F328CCC4213104A41C208 (void);
// 0x00000634 System.Void Mediapipe.MediaPipeOptions::WriteTo(Google.Protobuf.CodedOutputStream)
extern void MediaPipeOptions_WriteTo_mB5CF6A852AD743614BD0B5CC247564DF783C1AC0 (void);
// 0x00000635 System.Int32 Mediapipe.MediaPipeOptions::CalculateSize()
extern void MediaPipeOptions_CalculateSize_m161CBE1D9EF8D1A82B6D3EFADF0533D1541F34D2 (void);
// 0x00000636 System.Void Mediapipe.MediaPipeOptions::MergeFrom(Mediapipe.MediaPipeOptions)
extern void MediaPipeOptions_MergeFrom_m169687073AE0664B8B7070D1578ACB9A006A28FC (void);
// 0x00000637 System.Void Mediapipe.MediaPipeOptions::MergeFrom(Google.Protobuf.CodedInputStream)
extern void MediaPipeOptions_MergeFrom_m0F16A641B688B2AACE098CE2DD7ED6AF2A33B500 (void);
// 0x00000638 TValue Mediapipe.MediaPipeOptions::GetExtension(Google.Protobuf.Extension`2<Mediapipe.MediaPipeOptions,TValue>)
// 0x00000639 Google.Protobuf.Collections.RepeatedField`1<TValue> Mediapipe.MediaPipeOptions::GetExtension(Google.Protobuf.RepeatedExtension`2<Mediapipe.MediaPipeOptions,TValue>)
// 0x0000063A Google.Protobuf.Collections.RepeatedField`1<TValue> Mediapipe.MediaPipeOptions::GetOrInitializeExtension(Google.Protobuf.RepeatedExtension`2<Mediapipe.MediaPipeOptions,TValue>)
// 0x0000063B System.Void Mediapipe.MediaPipeOptions::SetExtension(Google.Protobuf.Extension`2<Mediapipe.MediaPipeOptions,TValue>,TValue)
// 0x0000063C System.Boolean Mediapipe.MediaPipeOptions::HasExtension(Google.Protobuf.Extension`2<Mediapipe.MediaPipeOptions,TValue>)
// 0x0000063D System.Void Mediapipe.MediaPipeOptions::ClearExtension(Google.Protobuf.Extension`2<Mediapipe.MediaPipeOptions,TValue>)
// 0x0000063E System.Void Mediapipe.MediaPipeOptions::ClearExtension(Google.Protobuf.RepeatedExtension`2<Mediapipe.MediaPipeOptions,TValue>)
// 0x0000063F System.Void Mediapipe.MediaPipeOptions::.cctor()
extern void MediaPipeOptions__cctor_m7F965FC72ECCDD15394C89BFEC81EBF4905DA967 (void);
// 0x00000640 System.Void Mediapipe.MediaPipeOptions/<>c::.cctor()
extern void U3CU3Ec__cctor_m5D22BAF26B57E273903474C8689229B978182858 (void);
// 0x00000641 System.Void Mediapipe.MediaPipeOptions/<>c::.ctor()
extern void U3CU3Ec__ctor_m36646B8FD4D7499D410B9430484EFC45D1375DC2 (void);
// 0x00000642 Mediapipe.MediaPipeOptions Mediapipe.MediaPipeOptions/<>c::<.cctor>b__30_0()
extern void U3CU3Ec_U3C_cctorU3Eb__30_0_mBACAF8EF4A02BFD71B435692FF09001E52CB714A (void);
// 0x00000643 Google.Protobuf.Reflection.FileDescriptor Mediapipe.ModelMatrixReflection::get_Descriptor()
extern void ModelMatrixReflection_get_Descriptor_mD9C8C9BB935082E7DDF59C2535BF29D61EE05739 (void);
// 0x00000644 System.Void Mediapipe.ModelMatrixReflection::.cctor()
extern void ModelMatrixReflection__cctor_m71F86409CF9E479B7E72D29A85A039246F596905 (void);
// 0x00000645 Google.Protobuf.MessageParser`1<Mediapipe.TimedModelMatrixProto> Mediapipe.TimedModelMatrixProto::get_Parser()
extern void TimedModelMatrixProto_get_Parser_m36CE8F4E4483F9F8CC74D4D7F9ADA697BC7B56FD (void);
// 0x00000646 Google.Protobuf.Reflection.MessageDescriptor Mediapipe.TimedModelMatrixProto::get_Descriptor()
extern void TimedModelMatrixProto_get_Descriptor_mE852310570DE30AD5DC66BA7B814104562692F2A (void);
// 0x00000647 Google.Protobuf.Reflection.MessageDescriptor Mediapipe.TimedModelMatrixProto::pb::Google.Protobuf.IMessage.get_Descriptor()
extern void TimedModelMatrixProto_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m38D15F7A1B078003FDD4B34CD31563EB2018ACD6 (void);
// 0x00000648 System.Void Mediapipe.TimedModelMatrixProto::.ctor()
extern void TimedModelMatrixProto__ctor_mD36B8BF1A3255A20D213DB8179CE15F8B90FF922 (void);
// 0x00000649 System.Void Mediapipe.TimedModelMatrixProto::.ctor(Mediapipe.TimedModelMatrixProto)
extern void TimedModelMatrixProto__ctor_m4DFAF5A291060B008DC81DDEB764F5FFC7501617 (void);
// 0x0000064A Mediapipe.TimedModelMatrixProto Mediapipe.TimedModelMatrixProto::Clone()
extern void TimedModelMatrixProto_Clone_m000B8BFEEAA7E9C9B5F816E65A2E524BC67AAE48 (void);
// 0x0000064B Google.Protobuf.Collections.RepeatedField`1<System.Single> Mediapipe.TimedModelMatrixProto::get_MatrixEntries()
extern void TimedModelMatrixProto_get_MatrixEntries_m08C2E89BF4AB4BFAB2B8D9125AA8580C5F809657 (void);
// 0x0000064C System.Int64 Mediapipe.TimedModelMatrixProto::get_TimeMsec()
extern void TimedModelMatrixProto_get_TimeMsec_m4F95EE6B0A01EAD7B808C2E2E99DE3802FAB68F8 (void);
// 0x0000064D System.Void Mediapipe.TimedModelMatrixProto::set_TimeMsec(System.Int64)
extern void TimedModelMatrixProto_set_TimeMsec_mA0A9A1FEC24E1950CE80E25314D4E566A0C19F78 (void);
// 0x0000064E System.Boolean Mediapipe.TimedModelMatrixProto::get_HasTimeMsec()
extern void TimedModelMatrixProto_get_HasTimeMsec_m512CC886558F76F1E5BEF13F2CC883B4FAA27566 (void);
// 0x0000064F System.Void Mediapipe.TimedModelMatrixProto::ClearTimeMsec()
extern void TimedModelMatrixProto_ClearTimeMsec_mD9625E2C729D54656E3F68610553639CADF865B0 (void);
// 0x00000650 System.Int32 Mediapipe.TimedModelMatrixProto::get_Id()
extern void TimedModelMatrixProto_get_Id_mFF676A6D32478DDE6B829B500488D517AAE3B0E6 (void);
// 0x00000651 System.Void Mediapipe.TimedModelMatrixProto::set_Id(System.Int32)
extern void TimedModelMatrixProto_set_Id_m5AD58896F59AD912A0807193212D4858A0CFDF71 (void);
// 0x00000652 System.Boolean Mediapipe.TimedModelMatrixProto::get_HasId()
extern void TimedModelMatrixProto_get_HasId_mBA4F429F24ADCFAAEE54B59AAAF634996F31BEB7 (void);
// 0x00000653 System.Void Mediapipe.TimedModelMatrixProto::ClearId()
extern void TimedModelMatrixProto_ClearId_m2380F94A5FB0693F86B1C7856B0ED842B1C29156 (void);
// 0x00000654 System.Boolean Mediapipe.TimedModelMatrixProto::Equals(System.Object)
extern void TimedModelMatrixProto_Equals_m10573AEDA812F98D2A73FD3D99A8C7FBF788F49D (void);
// 0x00000655 System.Boolean Mediapipe.TimedModelMatrixProto::Equals(Mediapipe.TimedModelMatrixProto)
extern void TimedModelMatrixProto_Equals_m8EAAFDB34A86DB247D89A253C9B5A9F969D6AF72 (void);
// 0x00000656 System.Int32 Mediapipe.TimedModelMatrixProto::GetHashCode()
extern void TimedModelMatrixProto_GetHashCode_m97FD870375066873755CDB0CD8C667D72EF2E26B (void);
// 0x00000657 System.String Mediapipe.TimedModelMatrixProto::ToString()
extern void TimedModelMatrixProto_ToString_mF73FC8377B34FB364A3471DF34952AEDE0CCFAFB (void);
// 0x00000658 System.Void Mediapipe.TimedModelMatrixProto::WriteTo(Google.Protobuf.CodedOutputStream)
extern void TimedModelMatrixProto_WriteTo_mE3FC56B10FDEA6B63530855BADD6886B006346AC (void);
// 0x00000659 System.Int32 Mediapipe.TimedModelMatrixProto::CalculateSize()
extern void TimedModelMatrixProto_CalculateSize_mD7343FF3B98B11A38CB8AD302EBFE05EE5E5B118 (void);
// 0x0000065A System.Void Mediapipe.TimedModelMatrixProto::MergeFrom(Mediapipe.TimedModelMatrixProto)
extern void TimedModelMatrixProto_MergeFrom_m19C51BFF2F65BC61EC38D1EAA2E1C6212A57874E (void);
// 0x0000065B System.Void Mediapipe.TimedModelMatrixProto::MergeFrom(Google.Protobuf.CodedInputStream)
extern void TimedModelMatrixProto_MergeFrom_m6CFBC87CE67964F667F449E67BE3488D319F373C (void);
// 0x0000065C System.Void Mediapipe.TimedModelMatrixProto::.cctor()
extern void TimedModelMatrixProto__cctor_m80283009FD75785A998DA98A5200F0CFDF42F765 (void);
// 0x0000065D System.Void Mediapipe.TimedModelMatrixProto/<>c::.cctor()
extern void U3CU3Ec__cctor_m021C5F20882A26700651DFF95A3760DA0CB314DC (void);
// 0x0000065E System.Void Mediapipe.TimedModelMatrixProto/<>c::.ctor()
extern void U3CU3Ec__ctor_mC113B3BFF393A6EEFC1A3620DF5CACDA83A30DBE (void);
// 0x0000065F Mediapipe.TimedModelMatrixProto Mediapipe.TimedModelMatrixProto/<>c::<.cctor>b__44_0()
extern void U3CU3Ec_U3C_cctorU3Eb__44_0_mEDBD83BC00FB40FC45543FBA4F4CE7506EAE7BBF (void);
// 0x00000660 Google.Protobuf.MessageParser`1<Mediapipe.TimedModelMatrixProtoList> Mediapipe.TimedModelMatrixProtoList::get_Parser()
extern void TimedModelMatrixProtoList_get_Parser_mB272AC9FCB8C6A227D39C525CAB16E6EECE52FF4 (void);
// 0x00000661 Google.Protobuf.Reflection.MessageDescriptor Mediapipe.TimedModelMatrixProtoList::get_Descriptor()
extern void TimedModelMatrixProtoList_get_Descriptor_mECA3CCA908B1CBDF9C0AEF003078069FB3C832C9 (void);
// 0x00000662 Google.Protobuf.Reflection.MessageDescriptor Mediapipe.TimedModelMatrixProtoList::pb::Google.Protobuf.IMessage.get_Descriptor()
extern void TimedModelMatrixProtoList_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m251842E44E7AC4A3C848B85C7A2A80DC420118B3 (void);
// 0x00000663 System.Void Mediapipe.TimedModelMatrixProtoList::.ctor()
extern void TimedModelMatrixProtoList__ctor_mAFE5CBDBDF8ABDE5ED930F230AE053920BA6E6E3 (void);
// 0x00000664 System.Void Mediapipe.TimedModelMatrixProtoList::.ctor(Mediapipe.TimedModelMatrixProtoList)
extern void TimedModelMatrixProtoList__ctor_mFEE41524C860DD57FF001AC92B9BAAA89B731A2A (void);
// 0x00000665 Mediapipe.TimedModelMatrixProtoList Mediapipe.TimedModelMatrixProtoList::Clone()
extern void TimedModelMatrixProtoList_Clone_m78CD67CD3A408D9D83041DE3F507EE53552BAA05 (void);
// 0x00000666 Google.Protobuf.Collections.RepeatedField`1<Mediapipe.TimedModelMatrixProto> Mediapipe.TimedModelMatrixProtoList::get_ModelMatrix()
extern void TimedModelMatrixProtoList_get_ModelMatrix_mE3F2600E2A863BCB6B172313AA6D9C491A34D671 (void);
// 0x00000667 System.Boolean Mediapipe.TimedModelMatrixProtoList::Equals(System.Object)
extern void TimedModelMatrixProtoList_Equals_mE02E4E54D0605023A5A9B0D54412D1CCBBAC674B (void);
// 0x00000668 System.Boolean Mediapipe.TimedModelMatrixProtoList::Equals(Mediapipe.TimedModelMatrixProtoList)
extern void TimedModelMatrixProtoList_Equals_m4B7E6B649A3541ACE89D9FF48E789EC4EF7E9871 (void);
// 0x00000669 System.Int32 Mediapipe.TimedModelMatrixProtoList::GetHashCode()
extern void TimedModelMatrixProtoList_GetHashCode_m39E976CEB249D20888704E62BE2A62F7AF48717B (void);
// 0x0000066A System.String Mediapipe.TimedModelMatrixProtoList::ToString()
extern void TimedModelMatrixProtoList_ToString_m8A8860A75EBC20B5EECB9BD5D546DD26FA31AF23 (void);
// 0x0000066B System.Void Mediapipe.TimedModelMatrixProtoList::WriteTo(Google.Protobuf.CodedOutputStream)
extern void TimedModelMatrixProtoList_WriteTo_mF9E2D1D5B4B98D4ECCD1CCD3B66353DC9FC0C4A7 (void);
// 0x0000066C System.Int32 Mediapipe.TimedModelMatrixProtoList::CalculateSize()
extern void TimedModelMatrixProtoList_CalculateSize_mDFA55794734AFC296355D7F42C8E1DEF5DA7F5FB (void);
// 0x0000066D System.Void Mediapipe.TimedModelMatrixProtoList::MergeFrom(Mediapipe.TimedModelMatrixProtoList)
extern void TimedModelMatrixProtoList_MergeFrom_m92F8B84288012603724555016046AEB0B943A1A5 (void);
// 0x0000066E System.Void Mediapipe.TimedModelMatrixProtoList::MergeFrom(Google.Protobuf.CodedInputStream)
extern void TimedModelMatrixProtoList_MergeFrom_m4DF276D53054577C044EB5E3C85EC49FA2A4A248 (void);
// 0x0000066F System.Void Mediapipe.TimedModelMatrixProtoList::.cctor()
extern void TimedModelMatrixProtoList__cctor_m3A6D649A629118CB9E6C8ECF4E3FCD475946828A (void);
// 0x00000670 System.Void Mediapipe.TimedModelMatrixProtoList/<>c::.cctor()
extern void U3CU3Ec__cctor_m968DE43BBB4B56E884777685F250ACBFF2C5AF80 (void);
// 0x00000671 System.Void Mediapipe.TimedModelMatrixProtoList/<>c::.ctor()
extern void U3CU3Ec__ctor_m624E1851124903C3A4424C16D17DFA0EAC504136 (void);
// 0x00000672 Mediapipe.TimedModelMatrixProtoList Mediapipe.TimedModelMatrixProtoList/<>c::<.cctor>b__25_0()
extern void U3CU3Ec_U3C_cctorU3Eb__25_0_mAAFEE6408CC075C05B004657C1C9A38DEF115888 (void);
// 0x00000673 Google.Protobuf.MessageParser`1<Mediapipe.TimedVectorProto> Mediapipe.TimedVectorProto::get_Parser()
extern void TimedVectorProto_get_Parser_m293D5CC4345A4B3EE0AAE0EDE281055B88B66A2C (void);
// 0x00000674 Google.Protobuf.Reflection.MessageDescriptor Mediapipe.TimedVectorProto::get_Descriptor()
extern void TimedVectorProto_get_Descriptor_mE7B46225F5C8CCCA1B6EA7E0C56A0D85E6772B3B (void);
// 0x00000675 Google.Protobuf.Reflection.MessageDescriptor Mediapipe.TimedVectorProto::pb::Google.Protobuf.IMessage.get_Descriptor()
extern void TimedVectorProto_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_mD0373F1BCAF286F3781C73FF0343499BAFDB47C0 (void);
// 0x00000676 System.Void Mediapipe.TimedVectorProto::.ctor()
extern void TimedVectorProto__ctor_m94A7FB7ED78DA1A40E402F3BB017C3E5AE052F23 (void);
// 0x00000677 System.Void Mediapipe.TimedVectorProto::.ctor(Mediapipe.TimedVectorProto)
extern void TimedVectorProto__ctor_m7A2E61E4FE4C04BFDB2103BC80DA8857BE16A6AF (void);
// 0x00000678 Mediapipe.TimedVectorProto Mediapipe.TimedVectorProto::Clone()
extern void TimedVectorProto_Clone_mAA2BCF6CF36B58DD176F497A2D68C2EC34915E49 (void);
// 0x00000679 Google.Protobuf.Collections.RepeatedField`1<System.Single> Mediapipe.TimedVectorProto::get_VectorEntries()
extern void TimedVectorProto_get_VectorEntries_m51532A0FA06E931EA522A4D3349D25936382C809 (void);
// 0x0000067A System.Int64 Mediapipe.TimedVectorProto::get_TimeMsec()
extern void TimedVectorProto_get_TimeMsec_mD446B35B77A78C5650A600B269C2F0DEFDFB026C (void);
// 0x0000067B System.Void Mediapipe.TimedVectorProto::set_TimeMsec(System.Int64)
extern void TimedVectorProto_set_TimeMsec_mE20840FD069CE1985A8B70BFFE00F7978A847E7A (void);
// 0x0000067C System.Boolean Mediapipe.TimedVectorProto::get_HasTimeMsec()
extern void TimedVectorProto_get_HasTimeMsec_m5CB3B26297FFF5D8E4A04D54E20EE7AFAC520E71 (void);
// 0x0000067D System.Void Mediapipe.TimedVectorProto::ClearTimeMsec()
extern void TimedVectorProto_ClearTimeMsec_m1B1C0E25E3372C399F5D14B10F2638788BC4F348 (void);
// 0x0000067E System.Int32 Mediapipe.TimedVectorProto::get_Id()
extern void TimedVectorProto_get_Id_mE0A80B31956B38E6D78DDF13C03D7DB0B779037F (void);
// 0x0000067F System.Void Mediapipe.TimedVectorProto::set_Id(System.Int32)
extern void TimedVectorProto_set_Id_m9A8863F5728C3E38E681CC5E33F07E16739CD6DE (void);
// 0x00000680 System.Boolean Mediapipe.TimedVectorProto::get_HasId()
extern void TimedVectorProto_get_HasId_m3FB553154CCDF6ACC4F9706396968B1B2E514C80 (void);
// 0x00000681 System.Void Mediapipe.TimedVectorProto::ClearId()
extern void TimedVectorProto_ClearId_m40D90F7DE18B8A1340531B1C587E9FA67FBA3014 (void);
// 0x00000682 System.Boolean Mediapipe.TimedVectorProto::Equals(System.Object)
extern void TimedVectorProto_Equals_m32738DC0662B6DD5CF4D2E5E97F07CD4D8137803 (void);
// 0x00000683 System.Boolean Mediapipe.TimedVectorProto::Equals(Mediapipe.TimedVectorProto)
extern void TimedVectorProto_Equals_m699DE31F04C68A804A9C34511BD3F83CC63BF990 (void);
// 0x00000684 System.Int32 Mediapipe.TimedVectorProto::GetHashCode()
extern void TimedVectorProto_GetHashCode_mB2A72327CB3A9C28014E0DCCCCD9916B8184296C (void);
// 0x00000685 System.String Mediapipe.TimedVectorProto::ToString()
extern void TimedVectorProto_ToString_mD6CC9AC2C547209D3590CC7ED638AFAD4D4E1785 (void);
// 0x00000686 System.Void Mediapipe.TimedVectorProto::WriteTo(Google.Protobuf.CodedOutputStream)
extern void TimedVectorProto_WriteTo_mA00DD38C663E0821F0CF37CA65F2619F7B15396E (void);
// 0x00000687 System.Int32 Mediapipe.TimedVectorProto::CalculateSize()
extern void TimedVectorProto_CalculateSize_mCA0C76E99A476908DA0E9C2FA1CD7E00EE1314D5 (void);
// 0x00000688 System.Void Mediapipe.TimedVectorProto::MergeFrom(Mediapipe.TimedVectorProto)
extern void TimedVectorProto_MergeFrom_m210826B18001527082F4BD2726C8E7B1755A5387 (void);
// 0x00000689 System.Void Mediapipe.TimedVectorProto::MergeFrom(Google.Protobuf.CodedInputStream)
extern void TimedVectorProto_MergeFrom_m3497545D1E9A10B36DBE67104C293FB1B37FAB18 (void);
// 0x0000068A System.Void Mediapipe.TimedVectorProto::.cctor()
extern void TimedVectorProto__cctor_m09F76DABE7C25D66B1B3C0F840FBCFC43666CDB4 (void);
// 0x0000068B System.Void Mediapipe.TimedVectorProto/<>c::.cctor()
extern void U3CU3Ec__cctor_m36D6E9C0857A5415DBF499C4641E04C4BC374783 (void);
// 0x0000068C System.Void Mediapipe.TimedVectorProto/<>c::.ctor()
extern void U3CU3Ec__ctor_mD891B79761EF073DF171E16B795A3514101C9D8A (void);
// 0x0000068D Mediapipe.TimedVectorProto Mediapipe.TimedVectorProto/<>c::<.cctor>b__44_0()
extern void U3CU3Ec_U3C_cctorU3Eb__44_0_m03351A8842EF79476AD80BA810D53A56CCB21E43 (void);
// 0x0000068E Google.Protobuf.MessageParser`1<Mediapipe.TimedVectorProtoList> Mediapipe.TimedVectorProtoList::get_Parser()
extern void TimedVectorProtoList_get_Parser_m57230E782BD81BF10D658AD6152E603A87375005 (void);
// 0x0000068F Google.Protobuf.Reflection.MessageDescriptor Mediapipe.TimedVectorProtoList::get_Descriptor()
extern void TimedVectorProtoList_get_Descriptor_mFF25C9CC211D75AC65106C0F7579763068FCD00F (void);
// 0x00000690 Google.Protobuf.Reflection.MessageDescriptor Mediapipe.TimedVectorProtoList::pb::Google.Protobuf.IMessage.get_Descriptor()
extern void TimedVectorProtoList_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m09C86C6D3E06E328B2A05DD9F39A0CE14F67C98C (void);
// 0x00000691 System.Void Mediapipe.TimedVectorProtoList::.ctor()
extern void TimedVectorProtoList__ctor_m7AFAE896482EDF3711A6DA99A412D64487EF2F91 (void);
// 0x00000692 System.Void Mediapipe.TimedVectorProtoList::.ctor(Mediapipe.TimedVectorProtoList)
extern void TimedVectorProtoList__ctor_mBB786126903E375611CE37FB8B7ADB4030EA0C1E (void);
// 0x00000693 Mediapipe.TimedVectorProtoList Mediapipe.TimedVectorProtoList::Clone()
extern void TimedVectorProtoList_Clone_mA692C92F4F46F28CF480F28E95932FEE2130FDA8 (void);
// 0x00000694 Google.Protobuf.Collections.RepeatedField`1<Mediapipe.TimedVectorProto> Mediapipe.TimedVectorProtoList::get_VectorList()
extern void TimedVectorProtoList_get_VectorList_m4461A0ABEB8795C098A873555BED0ED73ADF7EEA (void);
// 0x00000695 System.Boolean Mediapipe.TimedVectorProtoList::Equals(System.Object)
extern void TimedVectorProtoList_Equals_m70165213BF6F474B1105E05389301339CF9CA6ED (void);
// 0x00000696 System.Boolean Mediapipe.TimedVectorProtoList::Equals(Mediapipe.TimedVectorProtoList)
extern void TimedVectorProtoList_Equals_m090C5D733BA5CA3D26DCFC59F13DD2C413025B4A (void);
// 0x00000697 System.Int32 Mediapipe.TimedVectorProtoList::GetHashCode()
extern void TimedVectorProtoList_GetHashCode_mBE5776B4DEF84AD23EAE943A16440C264D819347 (void);
// 0x00000698 System.String Mediapipe.TimedVectorProtoList::ToString()
extern void TimedVectorProtoList_ToString_m89D6D023EF065EB7214BD2F6C4A5B793AEC47BBA (void);
// 0x00000699 System.Void Mediapipe.TimedVectorProtoList::WriteTo(Google.Protobuf.CodedOutputStream)
extern void TimedVectorProtoList_WriteTo_m7883C3162842BB6B2D11C97A03E61ED8B0625074 (void);
// 0x0000069A System.Int32 Mediapipe.TimedVectorProtoList::CalculateSize()
extern void TimedVectorProtoList_CalculateSize_mC4270F8CBA1A98987570383F71806D85E13AC42F (void);
// 0x0000069B System.Void Mediapipe.TimedVectorProtoList::MergeFrom(Mediapipe.TimedVectorProtoList)
extern void TimedVectorProtoList_MergeFrom_m1C123548C738E2DEE42C6BDF207533DD92EE55D5 (void);
// 0x0000069C System.Void Mediapipe.TimedVectorProtoList::MergeFrom(Google.Protobuf.CodedInputStream)
extern void TimedVectorProtoList_MergeFrom_m5F9AFAA0BE3F80732ED106238D19067945C8E53A (void);
// 0x0000069D System.Void Mediapipe.TimedVectorProtoList::.cctor()
extern void TimedVectorProtoList__cctor_m745BAE5C5B8A8BB2D922AF86261FF88BB9A43258 (void);
// 0x0000069E System.Void Mediapipe.TimedVectorProtoList/<>c::.cctor()
extern void U3CU3Ec__cctor_m2D50D56B0E7F848BB9943F836A7E6ACD6E8B1CAB (void);
// 0x0000069F System.Void Mediapipe.TimedVectorProtoList/<>c::.ctor()
extern void U3CU3Ec__ctor_mA2733139AAB03874B7D6868B2FE3D01DB9CB190F (void);
// 0x000006A0 Mediapipe.TimedVectorProtoList Mediapipe.TimedVectorProtoList/<>c::<.cctor>b__25_0()
extern void U3CU3Ec_U3C_cctorU3Eb__25_0_mF710D86AB9B4657C5EFAB71C6EFDC5CFC2F1ACF0 (void);
// 0x000006A1 Google.Protobuf.Reflection.FileDescriptor Mediapipe.PacketFactoryReflection::get_Descriptor()
extern void PacketFactoryReflection_get_Descriptor_m1BB07D7607F88807B3994AB7F590485B274CA3A8 (void);
// 0x000006A2 System.Void Mediapipe.PacketFactoryReflection::.cctor()
extern void PacketFactoryReflection__cctor_m1D46448F82B565E2382622429C818F969DB90C6D (void);
// 0x000006A3 Google.Protobuf.ExtensionSet`1<Mediapipe.PacketFactoryOptions> Mediapipe.PacketFactoryOptions::get__Extensions()
extern void PacketFactoryOptions_get__Extensions_m60DE38FB47E4AD1A028014468E4604009DDEF545 (void);
// 0x000006A4 Google.Protobuf.MessageParser`1<Mediapipe.PacketFactoryOptions> Mediapipe.PacketFactoryOptions::get_Parser()
extern void PacketFactoryOptions_get_Parser_m1B98972F75A63C47F0B8A548C6AF1CF224F12113 (void);
// 0x000006A5 Google.Protobuf.Reflection.MessageDescriptor Mediapipe.PacketFactoryOptions::get_Descriptor()
extern void PacketFactoryOptions_get_Descriptor_mCEC94D2EB7FE66068BAE199FAD78CBA8520E4D70 (void);
// 0x000006A6 Google.Protobuf.Reflection.MessageDescriptor Mediapipe.PacketFactoryOptions::pb::Google.Protobuf.IMessage.get_Descriptor()
extern void PacketFactoryOptions_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m19C4EEC187A9464F2089B7C7F61BFD9C1D6BE8F8 (void);
// 0x000006A7 System.Void Mediapipe.PacketFactoryOptions::.ctor()
extern void PacketFactoryOptions__ctor_mC8536EB222928EED726DC5B86DDECD96B930DAB0 (void);
// 0x000006A8 System.Void Mediapipe.PacketFactoryOptions::.ctor(Mediapipe.PacketFactoryOptions)
extern void PacketFactoryOptions__ctor_m2D36055064EC0015518ED210217D3E3E04CB050F (void);
// 0x000006A9 Mediapipe.PacketFactoryOptions Mediapipe.PacketFactoryOptions::Clone()
extern void PacketFactoryOptions_Clone_m81FDE11474A9B170510320D3F0F5D581E367B408 (void);
// 0x000006AA System.Boolean Mediapipe.PacketFactoryOptions::Equals(System.Object)
extern void PacketFactoryOptions_Equals_mE75139A5773D01442C993D3D8C76EF65159320E9 (void);
// 0x000006AB System.Boolean Mediapipe.PacketFactoryOptions::Equals(Mediapipe.PacketFactoryOptions)
extern void PacketFactoryOptions_Equals_mB3693914B1867B7CB7DE1969EA290CEA81D43E79 (void);
// 0x000006AC System.Int32 Mediapipe.PacketFactoryOptions::GetHashCode()
extern void PacketFactoryOptions_GetHashCode_m21D7D747BBA561125D4372391A037A706D72734E (void);
// 0x000006AD System.String Mediapipe.PacketFactoryOptions::ToString()
extern void PacketFactoryOptions_ToString_m55FF16DB4F9E721D5593642FE08504579D0F4D53 (void);
// 0x000006AE System.Void Mediapipe.PacketFactoryOptions::WriteTo(Google.Protobuf.CodedOutputStream)
extern void PacketFactoryOptions_WriteTo_m9D660E8331CCC27AAE98146FD46C62C3E702FECD (void);
// 0x000006AF System.Int32 Mediapipe.PacketFactoryOptions::CalculateSize()
extern void PacketFactoryOptions_CalculateSize_m269B700BA480E8E38BF52DBF256336CF709EBD72 (void);
// 0x000006B0 System.Void Mediapipe.PacketFactoryOptions::MergeFrom(Mediapipe.PacketFactoryOptions)
extern void PacketFactoryOptions_MergeFrom_m0D0FDA7834EBF6390A437209C82B5E59A32E1843 (void);
// 0x000006B1 System.Void Mediapipe.PacketFactoryOptions::MergeFrom(Google.Protobuf.CodedInputStream)
extern void PacketFactoryOptions_MergeFrom_m6AB31546F93E43834CD182777A573B5FD0F0B284 (void);
// 0x000006B2 TValue Mediapipe.PacketFactoryOptions::GetExtension(Google.Protobuf.Extension`2<Mediapipe.PacketFactoryOptions,TValue>)
// 0x000006B3 Google.Protobuf.Collections.RepeatedField`1<TValue> Mediapipe.PacketFactoryOptions::GetExtension(Google.Protobuf.RepeatedExtension`2<Mediapipe.PacketFactoryOptions,TValue>)
// 0x000006B4 Google.Protobuf.Collections.RepeatedField`1<TValue> Mediapipe.PacketFactoryOptions::GetOrInitializeExtension(Google.Protobuf.RepeatedExtension`2<Mediapipe.PacketFactoryOptions,TValue>)
// 0x000006B5 System.Void Mediapipe.PacketFactoryOptions::SetExtension(Google.Protobuf.Extension`2<Mediapipe.PacketFactoryOptions,TValue>,TValue)
// 0x000006B6 System.Boolean Mediapipe.PacketFactoryOptions::HasExtension(Google.Protobuf.Extension`2<Mediapipe.PacketFactoryOptions,TValue>)
// 0x000006B7 System.Void Mediapipe.PacketFactoryOptions::ClearExtension(Google.Protobuf.Extension`2<Mediapipe.PacketFactoryOptions,TValue>)
// 0x000006B8 System.Void Mediapipe.PacketFactoryOptions::ClearExtension(Google.Protobuf.RepeatedExtension`2<Mediapipe.PacketFactoryOptions,TValue>)
// 0x000006B9 System.Void Mediapipe.PacketFactoryOptions::.cctor()
extern void PacketFactoryOptions__cctor_m4E7C68E575409A82D4F0E4ABA2B5F83CDDEAF4DE (void);
// 0x000006BA System.Void Mediapipe.PacketFactoryOptions/<>c::.cctor()
extern void U3CU3Ec__cctor_mCFFF90F9EFC8C6FE9FB5E388EB69C374C7894314 (void);
// 0x000006BB System.Void Mediapipe.PacketFactoryOptions/<>c::.ctor()
extern void U3CU3Ec__ctor_mD8F7D8C6A9A5F7096C8CC9B7DD27DBBEDF869E8E (void);
// 0x000006BC Mediapipe.PacketFactoryOptions Mediapipe.PacketFactoryOptions/<>c::<.cctor>b__30_0()
extern void U3CU3Ec_U3C_cctorU3Eb__30_0_mEA2F1E0FF3C5BAD770ACA147EC662CD100BA1ACE (void);
// 0x000006BD Google.Protobuf.MessageParser`1<Mediapipe.PacketFactoryConfig> Mediapipe.PacketFactoryConfig::get_Parser()
extern void PacketFactoryConfig_get_Parser_m81229A5DA8B4F72497F78F8C752358E20AA52C35 (void);
// 0x000006BE Google.Protobuf.Reflection.MessageDescriptor Mediapipe.PacketFactoryConfig::get_Descriptor()
extern void PacketFactoryConfig_get_Descriptor_m78E47195E796521090796DBDF8DF85ADAA985841 (void);
// 0x000006BF Google.Protobuf.Reflection.MessageDescriptor Mediapipe.PacketFactoryConfig::pb::Google.Protobuf.IMessage.get_Descriptor()
extern void PacketFactoryConfig_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m52BF184CA45BFF0F3BEFA95AFDC1E37AAFDF059D (void);
// 0x000006C0 System.Void Mediapipe.PacketFactoryConfig::.ctor()
extern void PacketFactoryConfig__ctor_mB19EA1662F94AA4F76B3E366F68C4BEF5DE34E8A (void);
// 0x000006C1 System.Void Mediapipe.PacketFactoryConfig::.ctor(Mediapipe.PacketFactoryConfig)
extern void PacketFactoryConfig__ctor_m16A584150178AEF55F2E4F659D408B01ABACB481 (void);
// 0x000006C2 Mediapipe.PacketFactoryConfig Mediapipe.PacketFactoryConfig::Clone()
extern void PacketFactoryConfig_Clone_m27D51A4834DC6F5343FF317372508AC2FA7B08EF (void);
// 0x000006C3 System.String Mediapipe.PacketFactoryConfig::get_PacketFactory()
extern void PacketFactoryConfig_get_PacketFactory_m432AA4EAF7D3C26A29D75CC3FBE3539BD2F8508E (void);
// 0x000006C4 System.Void Mediapipe.PacketFactoryConfig::set_PacketFactory(System.String)
extern void PacketFactoryConfig_set_PacketFactory_m1C46207C47EAABBA519A8564DDA9882FB113DE3D (void);
// 0x000006C5 System.Boolean Mediapipe.PacketFactoryConfig::get_HasPacketFactory()
extern void PacketFactoryConfig_get_HasPacketFactory_m8EDC6B395D2D72B7FB6EDC7163D8865B90B98BF1 (void);
// 0x000006C6 System.Void Mediapipe.PacketFactoryConfig::ClearPacketFactory()
extern void PacketFactoryConfig_ClearPacketFactory_m82423E2A65876B507F2260501E7B5FE4871B5FD4 (void);
// 0x000006C7 System.String Mediapipe.PacketFactoryConfig::get_OutputSidePacket()
extern void PacketFactoryConfig_get_OutputSidePacket_m1ADDCCAF3521D72719F51E594C5D93AE121FCE4D (void);
// 0x000006C8 System.Void Mediapipe.PacketFactoryConfig::set_OutputSidePacket(System.String)
extern void PacketFactoryConfig_set_OutputSidePacket_m2B7435F6070ED9DB2F2A4E1D2904C1A68A855B80 (void);
// 0x000006C9 System.Boolean Mediapipe.PacketFactoryConfig::get_HasOutputSidePacket()
extern void PacketFactoryConfig_get_HasOutputSidePacket_m99DB84E3F9189FAA1803283C8F711D1CCA87039E (void);
// 0x000006CA System.Void Mediapipe.PacketFactoryConfig::ClearOutputSidePacket()
extern void PacketFactoryConfig_ClearOutputSidePacket_mF630C0F005F394FA52F5FEEAC784C45B2A9F400D (void);
// 0x000006CB System.String Mediapipe.PacketFactoryConfig::get_ExternalOutput()
extern void PacketFactoryConfig_get_ExternalOutput_m524F197CF49D7DEE2E6D73FCDB1E318511763D84 (void);
// 0x000006CC System.Void Mediapipe.PacketFactoryConfig::set_ExternalOutput(System.String)
extern void PacketFactoryConfig_set_ExternalOutput_m16CFBCC7E111C99EB84BDF532FF6E71EBCC5AB6A (void);
// 0x000006CD System.Boolean Mediapipe.PacketFactoryConfig::get_HasExternalOutput()
extern void PacketFactoryConfig_get_HasExternalOutput_m584B2D31D33522EB2A8CF18EE764D3F79868F8AF (void);
// 0x000006CE System.Void Mediapipe.PacketFactoryConfig::ClearExternalOutput()
extern void PacketFactoryConfig_ClearExternalOutput_mDFEABBD4FE8E94CE783946760A19F73B23B7099D (void);
// 0x000006CF Mediapipe.PacketFactoryOptions Mediapipe.PacketFactoryConfig::get_Options()
extern void PacketFactoryConfig_get_Options_m892546CFD591060DAA650AB7E0DD477A9717F6CC (void);
// 0x000006D0 System.Void Mediapipe.PacketFactoryConfig::set_Options(Mediapipe.PacketFactoryOptions)
extern void PacketFactoryConfig_set_Options_m6EE8262386BD29700E596C1E2A6F9660D4207426 (void);
// 0x000006D1 System.Boolean Mediapipe.PacketFactoryConfig::get_HasOptions()
extern void PacketFactoryConfig_get_HasOptions_m79F66DDEB940C1F4E1516383115DF857D9EB8218 (void);
// 0x000006D2 System.Void Mediapipe.PacketFactoryConfig::ClearOptions()
extern void PacketFactoryConfig_ClearOptions_mFF0B8AD05101A7B34DEB196164800C630203DF72 (void);
// 0x000006D3 System.Boolean Mediapipe.PacketFactoryConfig::Equals(System.Object)
extern void PacketFactoryConfig_Equals_m2F80DE3C163CE19D35574287766C1E85FEBA44A0 (void);
// 0x000006D4 System.Boolean Mediapipe.PacketFactoryConfig::Equals(Mediapipe.PacketFactoryConfig)
extern void PacketFactoryConfig_Equals_m0FEDD1353DBB452259CACE6FFF69AAC41D5EDC3D (void);
// 0x000006D5 System.Int32 Mediapipe.PacketFactoryConfig::GetHashCode()
extern void PacketFactoryConfig_GetHashCode_m037E8FB9D77E8B663BB9490F53F7746B5954FD01 (void);
// 0x000006D6 System.String Mediapipe.PacketFactoryConfig::ToString()
extern void PacketFactoryConfig_ToString_m081F5FBD927673C09D7F63168C6A45C50CBBF37C (void);
// 0x000006D7 System.Void Mediapipe.PacketFactoryConfig::WriteTo(Google.Protobuf.CodedOutputStream)
extern void PacketFactoryConfig_WriteTo_m182894E780B31E210F4E177DAE8F2438CEC90D56 (void);
// 0x000006D8 System.Int32 Mediapipe.PacketFactoryConfig::CalculateSize()
extern void PacketFactoryConfig_CalculateSize_m989B4B743104D755C0E33BBFD98C7F2B734AC4D5 (void);
// 0x000006D9 System.Void Mediapipe.PacketFactoryConfig::MergeFrom(Mediapipe.PacketFactoryConfig)
extern void PacketFactoryConfig_MergeFrom_mE285F8F83524EF28E8C0F1B20CCC431D5E9D61A8 (void);
// 0x000006DA System.Void Mediapipe.PacketFactoryConfig::MergeFrom(Google.Protobuf.CodedInputStream)
extern void PacketFactoryConfig_MergeFrom_mABEF4433CD66253E3B53FE07C3937BC2E9A8513D (void);
// 0x000006DB System.Void Mediapipe.PacketFactoryConfig::.cctor()
extern void PacketFactoryConfig__cctor_m91D07E016A6E203AC2C98267755ED68A66AE9879 (void);
// 0x000006DC System.Void Mediapipe.PacketFactoryConfig/<>c::.cctor()
extern void U3CU3Ec__cctor_m5A99475F562F36BE8ADBA919636A35F46035EB07 (void);
// 0x000006DD System.Void Mediapipe.PacketFactoryConfig/<>c::.ctor()
extern void U3CU3Ec__ctor_mEBED62C34AD5CCC041D25181A3038E1BCE4F57F8 (void);
// 0x000006DE Mediapipe.PacketFactoryConfig Mediapipe.PacketFactoryConfig/<>c::<.cctor>b__55_0()
extern void U3CU3Ec_U3C_cctorU3Eb__55_0_m06C25A7D8D61224A8E6F75CBA553029880D6693D (void);
// 0x000006DF Google.Protobuf.MessageParser`1<Mediapipe.PacketManagerConfig> Mediapipe.PacketManagerConfig::get_Parser()
extern void PacketManagerConfig_get_Parser_m11CE5669B52F0C46E21EA3692708F2657B344F20 (void);
// 0x000006E0 Google.Protobuf.Reflection.MessageDescriptor Mediapipe.PacketManagerConfig::get_Descriptor()
extern void PacketManagerConfig_get_Descriptor_m9E0E19AD41EB3F4EA79BF043F3F84A9980419CB2 (void);
// 0x000006E1 Google.Protobuf.Reflection.MessageDescriptor Mediapipe.PacketManagerConfig::pb::Google.Protobuf.IMessage.get_Descriptor()
extern void PacketManagerConfig_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m5D187D712B9E1E6A9E3B27A0C8E53BC412B3372D (void);
// 0x000006E2 System.Void Mediapipe.PacketManagerConfig::.ctor()
extern void PacketManagerConfig__ctor_m882F227FF8DBA0BF8E8743CE05BDC22CE76E7644 (void);
// 0x000006E3 System.Void Mediapipe.PacketManagerConfig::.ctor(Mediapipe.PacketManagerConfig)
extern void PacketManagerConfig__ctor_m9878FDFCDCEC6AF84637F38F1CBEA9DD5F5EE570 (void);
// 0x000006E4 Mediapipe.PacketManagerConfig Mediapipe.PacketManagerConfig::Clone()
extern void PacketManagerConfig_Clone_m4E2F5485BEF8CE62F49391651D7448EDC63F3093 (void);
// 0x000006E5 Google.Protobuf.Collections.RepeatedField`1<Mediapipe.PacketFactoryConfig> Mediapipe.PacketManagerConfig::get_Packet()
extern void PacketManagerConfig_get_Packet_m32D6AD32AFBA86132A69C07EC032A4F8DCBF79C3 (void);
// 0x000006E6 System.Boolean Mediapipe.PacketManagerConfig::Equals(System.Object)
extern void PacketManagerConfig_Equals_m11D2FCCE2BC53BE6796B761C6DF983D05199371B (void);
// 0x000006E7 System.Boolean Mediapipe.PacketManagerConfig::Equals(Mediapipe.PacketManagerConfig)
extern void PacketManagerConfig_Equals_m448C26325B5F0F63AD1511314AD36AE88FF75B53 (void);
// 0x000006E8 System.Int32 Mediapipe.PacketManagerConfig::GetHashCode()
extern void PacketManagerConfig_GetHashCode_mF97E5240FC7AC7599CF9878408E09169FD99F992 (void);
// 0x000006E9 System.String Mediapipe.PacketManagerConfig::ToString()
extern void PacketManagerConfig_ToString_mB91FC8BD3FD4FA215BB48D84E60A40ECD8FE301A (void);
// 0x000006EA System.Void Mediapipe.PacketManagerConfig::WriteTo(Google.Protobuf.CodedOutputStream)
extern void PacketManagerConfig_WriteTo_mC1BB483BA2A79844AAC30C9902ADD2178C765B9A (void);
// 0x000006EB System.Int32 Mediapipe.PacketManagerConfig::CalculateSize()
extern void PacketManagerConfig_CalculateSize_m9B17003F4E07D2FBA15072AA9FA611E9808C9CEE (void);
// 0x000006EC System.Void Mediapipe.PacketManagerConfig::MergeFrom(Mediapipe.PacketManagerConfig)
extern void PacketManagerConfig_MergeFrom_mFCFF46E5C7B7FF00042DB64CA4117549B4DA6379 (void);
// 0x000006ED System.Void Mediapipe.PacketManagerConfig::MergeFrom(Google.Protobuf.CodedInputStream)
extern void PacketManagerConfig_MergeFrom_m47C7CA9A0C39ABF84078F6AE1383CD0D40816174 (void);
// 0x000006EE System.Void Mediapipe.PacketManagerConfig::.cctor()
extern void PacketManagerConfig__cctor_m96C7834A426C31FB2FF996C26C63835E3D7BC974 (void);
// 0x000006EF System.Void Mediapipe.PacketManagerConfig/<>c::.cctor()
extern void U3CU3Ec__cctor_mE9FC1493E664445AEF6DB6FFD6E0AA5040C8EBB4 (void);
// 0x000006F0 System.Void Mediapipe.PacketManagerConfig/<>c::.ctor()
extern void U3CU3Ec__ctor_m747130ADF16A252D428EFD36E215B0A0B61CE5C7 (void);
// 0x000006F1 Mediapipe.PacketManagerConfig Mediapipe.PacketManagerConfig/<>c::<.cctor>b__25_0()
extern void U3CU3Ec_U3C_cctorU3Eb__25_0_m52AFA52E9EC5CD5D052E7C0A665D1E5260E990F1 (void);
// 0x000006F2 Google.Protobuf.Reflection.FileDescriptor Mediapipe.PacketGeneratorReflection::get_Descriptor()
extern void PacketGeneratorReflection_get_Descriptor_mC95A1DE28E3DABB652C27D99BF92F449C18B160B (void);
// 0x000006F3 System.Void Mediapipe.PacketGeneratorReflection::.cctor()
extern void PacketGeneratorReflection__cctor_mAFEA5BC53DD28EF0CCCCC108C9CD1DB2181D6249 (void);
// 0x000006F4 Google.Protobuf.ExtensionSet`1<Mediapipe.PacketGeneratorOptions> Mediapipe.PacketGeneratorOptions::get__Extensions()
extern void PacketGeneratorOptions_get__Extensions_m4778AF35D0CADC27D1735741F17556260D66C471 (void);
// 0x000006F5 Google.Protobuf.MessageParser`1<Mediapipe.PacketGeneratorOptions> Mediapipe.PacketGeneratorOptions::get_Parser()
extern void PacketGeneratorOptions_get_Parser_mC6F315DEBB7D778C70563F4543099732E4302310 (void);
// 0x000006F6 Google.Protobuf.Reflection.MessageDescriptor Mediapipe.PacketGeneratorOptions::get_Descriptor()
extern void PacketGeneratorOptions_get_Descriptor_m10CC59CD22798F54F0DF2A34A4CD87EB5420FF68 (void);
// 0x000006F7 Google.Protobuf.Reflection.MessageDescriptor Mediapipe.PacketGeneratorOptions::pb::Google.Protobuf.IMessage.get_Descriptor()
extern void PacketGeneratorOptions_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_mFA5549DF6A9568DAF3F0A09D542C4D5BBFEAE78A (void);
// 0x000006F8 System.Void Mediapipe.PacketGeneratorOptions::.ctor()
extern void PacketGeneratorOptions__ctor_mC663EB5FC659CA75449E64A002B4CC10C15360AF (void);
// 0x000006F9 System.Void Mediapipe.PacketGeneratorOptions::.ctor(Mediapipe.PacketGeneratorOptions)
extern void PacketGeneratorOptions__ctor_m74543604138A39F4D112E514CE0C9DD4B53BCF39 (void);
// 0x000006FA Mediapipe.PacketGeneratorOptions Mediapipe.PacketGeneratorOptions::Clone()
extern void PacketGeneratorOptions_Clone_m058684255081A07E04B4D5EBB30D264D95C2C2BB (void);
// 0x000006FB System.Boolean Mediapipe.PacketGeneratorOptions::Equals(System.Object)
extern void PacketGeneratorOptions_Equals_m7695E0C7DB1C89DF8F186CB8265472DA3469D9F7 (void);
// 0x000006FC System.Boolean Mediapipe.PacketGeneratorOptions::Equals(Mediapipe.PacketGeneratorOptions)
extern void PacketGeneratorOptions_Equals_mB3A3471F6EE5C8A3374CBE6DDAD98102A07FBF37 (void);
// 0x000006FD System.Int32 Mediapipe.PacketGeneratorOptions::GetHashCode()
extern void PacketGeneratorOptions_GetHashCode_m23D64E3A72F45C6FF8E682520689880DDC187C9F (void);
// 0x000006FE System.String Mediapipe.PacketGeneratorOptions::ToString()
extern void PacketGeneratorOptions_ToString_mE964D3F9F6EC1FEA51DD82143A1F65695F253A18 (void);
// 0x000006FF System.Void Mediapipe.PacketGeneratorOptions::WriteTo(Google.Protobuf.CodedOutputStream)
extern void PacketGeneratorOptions_WriteTo_mA2C4C9ABD31A321D10AFA8D9841EEAAA230FE944 (void);
// 0x00000700 System.Int32 Mediapipe.PacketGeneratorOptions::CalculateSize()
extern void PacketGeneratorOptions_CalculateSize_m52382B6749DB349EF633CC5ECFC63AF2A00C82CE (void);
// 0x00000701 System.Void Mediapipe.PacketGeneratorOptions::MergeFrom(Mediapipe.PacketGeneratorOptions)
extern void PacketGeneratorOptions_MergeFrom_m554F607119A2A19278375B13E27CD2AF9B9D9AC5 (void);
// 0x00000702 System.Void Mediapipe.PacketGeneratorOptions::MergeFrom(Google.Protobuf.CodedInputStream)
extern void PacketGeneratorOptions_MergeFrom_m0CADE6A187BDA996B2480109B50A87D215468C2C (void);
// 0x00000703 TValue Mediapipe.PacketGeneratorOptions::GetExtension(Google.Protobuf.Extension`2<Mediapipe.PacketGeneratorOptions,TValue>)
// 0x00000704 Google.Protobuf.Collections.RepeatedField`1<TValue> Mediapipe.PacketGeneratorOptions::GetExtension(Google.Protobuf.RepeatedExtension`2<Mediapipe.PacketGeneratorOptions,TValue>)
// 0x00000705 Google.Protobuf.Collections.RepeatedField`1<TValue> Mediapipe.PacketGeneratorOptions::GetOrInitializeExtension(Google.Protobuf.RepeatedExtension`2<Mediapipe.PacketGeneratorOptions,TValue>)
// 0x00000706 System.Void Mediapipe.PacketGeneratorOptions::SetExtension(Google.Protobuf.Extension`2<Mediapipe.PacketGeneratorOptions,TValue>,TValue)
// 0x00000707 System.Boolean Mediapipe.PacketGeneratorOptions::HasExtension(Google.Protobuf.Extension`2<Mediapipe.PacketGeneratorOptions,TValue>)
// 0x00000708 System.Void Mediapipe.PacketGeneratorOptions::ClearExtension(Google.Protobuf.Extension`2<Mediapipe.PacketGeneratorOptions,TValue>)
// 0x00000709 System.Void Mediapipe.PacketGeneratorOptions::ClearExtension(Google.Protobuf.RepeatedExtension`2<Mediapipe.PacketGeneratorOptions,TValue>)
// 0x0000070A System.Void Mediapipe.PacketGeneratorOptions::.cctor()
extern void PacketGeneratorOptions__cctor_m1E4A3CD0D1B9F83B19D19D5757F562F49995E7CE (void);
// 0x0000070B System.Void Mediapipe.PacketGeneratorOptions/<>c::.cctor()
extern void U3CU3Ec__cctor_m186865A85B26E8B572E1B9D5D993983DE0154450 (void);
// 0x0000070C System.Void Mediapipe.PacketGeneratorOptions/<>c::.ctor()
extern void U3CU3Ec__ctor_m4079A03D3B93B3A72E54D2AB03E6D4F0F54A0FB3 (void);
// 0x0000070D Mediapipe.PacketGeneratorOptions Mediapipe.PacketGeneratorOptions/<>c::<.cctor>b__30_0()
extern void U3CU3Ec_U3C_cctorU3Eb__30_0_m4314F404BA59DCE90DDCCE5F6063963DE36982E9 (void);
// 0x0000070E Google.Protobuf.MessageParser`1<Mediapipe.PacketGeneratorConfig> Mediapipe.PacketGeneratorConfig::get_Parser()
extern void PacketGeneratorConfig_get_Parser_m7B8D015FE1EEEB6818CCF0006BBDDFC3F309BD34 (void);
// 0x0000070F Google.Protobuf.Reflection.MessageDescriptor Mediapipe.PacketGeneratorConfig::get_Descriptor()
extern void PacketGeneratorConfig_get_Descriptor_mA8E045699A747056B6927B72B44B025D1ED045DA (void);
// 0x00000710 Google.Protobuf.Reflection.MessageDescriptor Mediapipe.PacketGeneratorConfig::pb::Google.Protobuf.IMessage.get_Descriptor()
extern void PacketGeneratorConfig_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_mBA6ABA69D1E99F666D3CEBCF5276482C0089440D (void);
// 0x00000711 System.Void Mediapipe.PacketGeneratorConfig::.ctor()
extern void PacketGeneratorConfig__ctor_m3DD06F90644406E25B61D2CD6105787D7A59DC2E (void);
// 0x00000712 System.Void Mediapipe.PacketGeneratorConfig::.ctor(Mediapipe.PacketGeneratorConfig)
extern void PacketGeneratorConfig__ctor_m6380447A5817E1DB9BA8031BC40A897E3EC7FADB (void);
// 0x00000713 Mediapipe.PacketGeneratorConfig Mediapipe.PacketGeneratorConfig::Clone()
extern void PacketGeneratorConfig_Clone_m7862B8297F86CA31E1A69BC23BBA6819771AB397 (void);
// 0x00000714 System.String Mediapipe.PacketGeneratorConfig::get_PacketGenerator()
extern void PacketGeneratorConfig_get_PacketGenerator_m992D054B0C10110EEE8CB8A3FA9533A4F53A74B2 (void);
// 0x00000715 System.Void Mediapipe.PacketGeneratorConfig::set_PacketGenerator(System.String)
extern void PacketGeneratorConfig_set_PacketGenerator_m354D3E2DA87E98A12223838512C56043AACDC01F (void);
// 0x00000716 System.Boolean Mediapipe.PacketGeneratorConfig::get_HasPacketGenerator()
extern void PacketGeneratorConfig_get_HasPacketGenerator_m532EFF69B798BB837F79A513F185294151C88F11 (void);
// 0x00000717 System.Void Mediapipe.PacketGeneratorConfig::ClearPacketGenerator()
extern void PacketGeneratorConfig_ClearPacketGenerator_m8F3C91BC62A1B11E3AFAB4AC8CDB798997E4927A (void);
// 0x00000718 Google.Protobuf.Collections.RepeatedField`1<System.String> Mediapipe.PacketGeneratorConfig::get_InputSidePacket()
extern void PacketGeneratorConfig_get_InputSidePacket_m0A3AE6A3601D07A81352FFD4C776228323960B7B (void);
// 0x00000719 Google.Protobuf.Collections.RepeatedField`1<System.String> Mediapipe.PacketGeneratorConfig::get_ExternalInput()
extern void PacketGeneratorConfig_get_ExternalInput_mC2AE53911DDD18A8433EE0A769D3300BE66D6907 (void);
// 0x0000071A Google.Protobuf.Collections.RepeatedField`1<System.String> Mediapipe.PacketGeneratorConfig::get_OutputSidePacket()
extern void PacketGeneratorConfig_get_OutputSidePacket_m488C93E85C0A7A580449AFF8E53A21D8521CAAF8 (void);
// 0x0000071B Google.Protobuf.Collections.RepeatedField`1<System.String> Mediapipe.PacketGeneratorConfig::get_ExternalOutput()
extern void PacketGeneratorConfig_get_ExternalOutput_m8681B870FD63D25DD093CD1001210AAF2371ED21 (void);
// 0x0000071C Mediapipe.PacketGeneratorOptions Mediapipe.PacketGeneratorConfig::get_Options()
extern void PacketGeneratorConfig_get_Options_mD583370058BA43BDC9FFBD0AF4E9121B64681847 (void);
// 0x0000071D System.Void Mediapipe.PacketGeneratorConfig::set_Options(Mediapipe.PacketGeneratorOptions)
extern void PacketGeneratorConfig_set_Options_m72F65FBB17D99B6F3E2E06E3F99425223A140663 (void);
// 0x0000071E System.Boolean Mediapipe.PacketGeneratorConfig::get_HasOptions()
extern void PacketGeneratorConfig_get_HasOptions_m7A6B1FDEB2175A5FC8135348F8EA37B8941F21BB (void);
// 0x0000071F System.Void Mediapipe.PacketGeneratorConfig::ClearOptions()
extern void PacketGeneratorConfig_ClearOptions_m397FE10C8A8D3BC5CF70E4D76CA66579F88946AF (void);
// 0x00000720 System.Boolean Mediapipe.PacketGeneratorConfig::Equals(System.Object)
extern void PacketGeneratorConfig_Equals_mFF71F177C5357C6ADF18AE90B67B1B030D2B230D (void);
// 0x00000721 System.Boolean Mediapipe.PacketGeneratorConfig::Equals(Mediapipe.PacketGeneratorConfig)
extern void PacketGeneratorConfig_Equals_mAFF8CA1140FFA35F3A7F59B55F34C582BB5BECC7 (void);
// 0x00000722 System.Int32 Mediapipe.PacketGeneratorConfig::GetHashCode()
extern void PacketGeneratorConfig_GetHashCode_m67C2EE0089FDA8A17452E8789A9226BF7497F2EB (void);
// 0x00000723 System.String Mediapipe.PacketGeneratorConfig::ToString()
extern void PacketGeneratorConfig_ToString_mF8E0F155AB39F730DC8D028AEBF3F122AC9CF76C (void);
// 0x00000724 System.Void Mediapipe.PacketGeneratorConfig::WriteTo(Google.Protobuf.CodedOutputStream)
extern void PacketGeneratorConfig_WriteTo_mAD2E8F441993B1349F73DB355982F3018A9826DB (void);
// 0x00000725 System.Int32 Mediapipe.PacketGeneratorConfig::CalculateSize()
extern void PacketGeneratorConfig_CalculateSize_m42D473EA3CB305950C349CC960667081855F7408 (void);
// 0x00000726 System.Void Mediapipe.PacketGeneratorConfig::MergeFrom(Mediapipe.PacketGeneratorConfig)
extern void PacketGeneratorConfig_MergeFrom_m56A617696F27E0E76700262130434884884C864A (void);
// 0x00000727 System.Void Mediapipe.PacketGeneratorConfig::MergeFrom(Google.Protobuf.CodedInputStream)
extern void PacketGeneratorConfig_MergeFrom_m972AAB330709CD60D6DF3A296350D7FE9154B6D9 (void);
// 0x00000728 System.Void Mediapipe.PacketGeneratorConfig::.cctor()
extern void PacketGeneratorConfig__cctor_mFBDEB1E40903E08E7FD2FAC611014A680BEACC32 (void);
// 0x00000729 System.Void Mediapipe.PacketGeneratorConfig/<>c::.cctor()
extern void U3CU3Ec__cctor_mEE67B3B661901A8BC6025CF19F1144BB06497D45 (void);
// 0x0000072A System.Void Mediapipe.PacketGeneratorConfig/<>c::.ctor()
extern void U3CU3Ec__ctor_mB9514EB904F901E35D475C4C403C3D4CF5B6217E (void);
// 0x0000072B Mediapipe.PacketGeneratorConfig Mediapipe.PacketGeneratorConfig/<>c::<.cctor>b__57_0()
extern void U3CU3Ec_U3C_cctorU3Eb__57_0_m291094B7EF2EA14F0D2CB07F2D3FB3E98978B5F4 (void);
// 0x0000072C Google.Protobuf.Reflection.FileDescriptor Mediapipe.RasterizationReflection::get_Descriptor()
extern void RasterizationReflection_get_Descriptor_mED8B75EE232C007AB76BA566165C6783907DB56B (void);
// 0x0000072D System.Void Mediapipe.RasterizationReflection::.cctor()
extern void RasterizationReflection__cctor_m325F48A90228FB52DAC3930158A973B499922D52 (void);
// 0x0000072E Google.Protobuf.MessageParser`1<Mediapipe.Rasterization> Mediapipe.Rasterization::get_Parser()
extern void Rasterization_get_Parser_mF17E0B95B210885A54766F4E357E35BBE3FE9AE5 (void);
// 0x0000072F Google.Protobuf.Reflection.MessageDescriptor Mediapipe.Rasterization::get_Descriptor()
extern void Rasterization_get_Descriptor_m722F6AA571B13EF64011AA0035AF1A22A6ABD84D (void);
// 0x00000730 Google.Protobuf.Reflection.MessageDescriptor Mediapipe.Rasterization::pb::Google.Protobuf.IMessage.get_Descriptor()
extern void Rasterization_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m792160B1139E20599CE99C4B313DBE88FAD22FA8 (void);
// 0x00000731 System.Void Mediapipe.Rasterization::.ctor()
extern void Rasterization__ctor_m55FC6BC6C94655B9C8CE0385CB44C1E81716DBB7 (void);
// 0x00000732 System.Void Mediapipe.Rasterization::.ctor(Mediapipe.Rasterization)
extern void Rasterization__ctor_mD3697404E5C77CE873F448E92932C6CC0C18681C (void);
// 0x00000733 Mediapipe.Rasterization Mediapipe.Rasterization::Clone()
extern void Rasterization_Clone_m72998D69821C34E51500317806C6B8EF5C86EC6C (void);
// 0x00000734 Google.Protobuf.Collections.RepeatedField`1<Mediapipe.Rasterization/Types/Interval> Mediapipe.Rasterization::get_Interval()
extern void Rasterization_get_Interval_m2CB53A337C5EA0AB68072018D2A5C64B1BC58423 (void);
// 0x00000735 System.Boolean Mediapipe.Rasterization::Equals(System.Object)
extern void Rasterization_Equals_mA13E8A4D5D7B8915FF57D3B9C72C364DB37A2F1A (void);
// 0x00000736 System.Boolean Mediapipe.Rasterization::Equals(Mediapipe.Rasterization)
extern void Rasterization_Equals_m7DAE4E1872E2285E1981E16DC001030FC42A1EFB (void);
// 0x00000737 System.Int32 Mediapipe.Rasterization::GetHashCode()
extern void Rasterization_GetHashCode_m7C644190483001AC74DD0288F66F99E3993351F5 (void);
// 0x00000738 System.String Mediapipe.Rasterization::ToString()
extern void Rasterization_ToString_m0020CC92D76AC7E76D9770A619E69E0C9E22B2B3 (void);
// 0x00000739 System.Void Mediapipe.Rasterization::WriteTo(Google.Protobuf.CodedOutputStream)
extern void Rasterization_WriteTo_mA8B9BAFEBF0E6AAC7056B5922700F05AAFDFD6E4 (void);
// 0x0000073A System.Int32 Mediapipe.Rasterization::CalculateSize()
extern void Rasterization_CalculateSize_m136A9B8A9D2FBF3A682F7BF2053C1588FA35AB5D (void);
// 0x0000073B System.Void Mediapipe.Rasterization::MergeFrom(Mediapipe.Rasterization)
extern void Rasterization_MergeFrom_m70BDB2F744263C31C8E738B4122D7FDE11E0225C (void);
// 0x0000073C System.Void Mediapipe.Rasterization::MergeFrom(Google.Protobuf.CodedInputStream)
extern void Rasterization_MergeFrom_mF5B9FC0CBCF3484AD858AC847AB6712619FADE13 (void);
// 0x0000073D System.Void Mediapipe.Rasterization::.cctor()
extern void Rasterization__cctor_m61CAE0FE43758C68B8B29ADB512E5B0F243BDFF2 (void);
// 0x0000073E Google.Protobuf.MessageParser`1<Mediapipe.Rasterization/Types/Interval> Mediapipe.Rasterization/Types/Interval::get_Parser()
extern void Interval_get_Parser_m9BB1C9B7DBEA3582477E6ECE45458AC973D0FF08 (void);
// 0x0000073F Google.Protobuf.Reflection.MessageDescriptor Mediapipe.Rasterization/Types/Interval::get_Descriptor()
extern void Interval_get_Descriptor_m13DAA3A651E004E4B34DE9C8844C58178454629F (void);
// 0x00000740 Google.Protobuf.Reflection.MessageDescriptor Mediapipe.Rasterization/Types/Interval::pb::Google.Protobuf.IMessage.get_Descriptor()
extern void Interval_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m9B390C253B12875EFCB1597893655A61167839F2 (void);
// 0x00000741 System.Void Mediapipe.Rasterization/Types/Interval::.ctor()
extern void Interval__ctor_m32A3957EBFFE3B98D584CE5835A00D77CEA1F3BE (void);
// 0x00000742 System.Void Mediapipe.Rasterization/Types/Interval::.ctor(Mediapipe.Rasterization/Types/Interval)
extern void Interval__ctor_m5E2DC10FD5A9373055DC59F91A2DF4AE47E32644 (void);
// 0x00000743 Mediapipe.Rasterization/Types/Interval Mediapipe.Rasterization/Types/Interval::Clone()
extern void Interval_Clone_m07446964FC63DB770BDDB2850439BF01C88B3E53 (void);
// 0x00000744 System.Int32 Mediapipe.Rasterization/Types/Interval::get_Y()
extern void Interval_get_Y_mE60537C37315F397FFFE75FC400739106D8154DE (void);
// 0x00000745 System.Void Mediapipe.Rasterization/Types/Interval::set_Y(System.Int32)
extern void Interval_set_Y_m9056F9DD99CE66F68D1682111DBBA63CA322BA3B (void);
// 0x00000746 System.Boolean Mediapipe.Rasterization/Types/Interval::get_HasY()
extern void Interval_get_HasY_m2F6B1FFAA8CF48ED2CBE05E4283259D739D8C9D4 (void);
// 0x00000747 System.Void Mediapipe.Rasterization/Types/Interval::ClearY()
extern void Interval_ClearY_m9A1D81264BB6A7171F645F141D26B16B11FF59A1 (void);
// 0x00000748 System.Int32 Mediapipe.Rasterization/Types/Interval::get_LeftX()
extern void Interval_get_LeftX_mECB95906533EC0F1C6F866388AA6DD6D65D4F6B2 (void);
// 0x00000749 System.Void Mediapipe.Rasterization/Types/Interval::set_LeftX(System.Int32)
extern void Interval_set_LeftX_mE7317A98C5B6B365086EEFD0A750A5C407822256 (void);
// 0x0000074A System.Boolean Mediapipe.Rasterization/Types/Interval::get_HasLeftX()
extern void Interval_get_HasLeftX_m4CD8A6DC318305E3C87C472F77441861E05C3E9C (void);
// 0x0000074B System.Void Mediapipe.Rasterization/Types/Interval::ClearLeftX()
extern void Interval_ClearLeftX_m18139A501E8C9A583BEA41E83544720908938204 (void);
// 0x0000074C System.Int32 Mediapipe.Rasterization/Types/Interval::get_RightX()
extern void Interval_get_RightX_mF210A4D27C03E8CF92AFA170A6A961E61073FB7F (void);
// 0x0000074D System.Void Mediapipe.Rasterization/Types/Interval::set_RightX(System.Int32)
extern void Interval_set_RightX_mBB294AAC7E8A210CD10F7C4B77BEACBE22DD5F6A (void);
// 0x0000074E System.Boolean Mediapipe.Rasterization/Types/Interval::get_HasRightX()
extern void Interval_get_HasRightX_m6942D9B35707AAD4B6353A8ED6D18747F3670B9E (void);
// 0x0000074F System.Void Mediapipe.Rasterization/Types/Interval::ClearRightX()
extern void Interval_ClearRightX_m6EF10A9D5EA463F0C7E06C15932A66A8A7974662 (void);
// 0x00000750 System.Boolean Mediapipe.Rasterization/Types/Interval::Equals(System.Object)
extern void Interval_Equals_m694D5F474327657E1C30F07C2AF9F10C9B24F43F (void);
// 0x00000751 System.Boolean Mediapipe.Rasterization/Types/Interval::Equals(Mediapipe.Rasterization/Types/Interval)
extern void Interval_Equals_mFE99EB6740C0735C445F4AADA222DD792A4E98E4 (void);
// 0x00000752 System.Int32 Mediapipe.Rasterization/Types/Interval::GetHashCode()
extern void Interval_GetHashCode_mC8E3DB33E588E151786C01B8160CE01B2898412A (void);
// 0x00000753 System.String Mediapipe.Rasterization/Types/Interval::ToString()
extern void Interval_ToString_m01323C62EB2CA2032A43994675B1F80BC6868CCC (void);
// 0x00000754 System.Void Mediapipe.Rasterization/Types/Interval::WriteTo(Google.Protobuf.CodedOutputStream)
extern void Interval_WriteTo_m501236942A2D0E72E74799CD107E6C04E780731B (void);
// 0x00000755 System.Int32 Mediapipe.Rasterization/Types/Interval::CalculateSize()
extern void Interval_CalculateSize_m1A75A81A7A6EB5AB4EDF015348B58F3CC4C7DE95 (void);
// 0x00000756 System.Void Mediapipe.Rasterization/Types/Interval::MergeFrom(Mediapipe.Rasterization/Types/Interval)
extern void Interval_MergeFrom_m8E8A02B6433AD6798D67E8F86A48B73039E311C1 (void);
// 0x00000757 System.Void Mediapipe.Rasterization/Types/Interval::MergeFrom(Google.Protobuf.CodedInputStream)
extern void Interval_MergeFrom_m08CA5B392DC43CB8FC5959A8E28146884E6C7376 (void);
// 0x00000758 System.Void Mediapipe.Rasterization/Types/Interval::.cctor()
extern void Interval__cctor_m69FE3EEDBBE88E37B805160892C7C88F6335A8D2 (void);
// 0x00000759 System.Void Mediapipe.Rasterization/Types/Interval/<>c::.cctor()
extern void U3CU3Ec__cctor_m4D8B1239895A7ECC649E59B2460B5218E66AEDC8 (void);
// 0x0000075A System.Void Mediapipe.Rasterization/Types/Interval/<>c::.ctor()
extern void U3CU3Ec__ctor_m1107224156F8BAC6FC35668C2FA35993C8A59EAA (void);
// 0x0000075B Mediapipe.Rasterization/Types/Interval Mediapipe.Rasterization/Types/Interval/<>c::<.cctor>b__48_0()
extern void U3CU3Ec_U3C_cctorU3Eb__48_0_m8D40D169037998FA085C6B31D7B3F70EBD780099 (void);
// 0x0000075C System.Void Mediapipe.Rasterization/<>c::.cctor()
extern void U3CU3Ec__cctor_m1207FFA106C945365F362EBB9CAC8E845AB64190 (void);
// 0x0000075D System.Void Mediapipe.Rasterization/<>c::.ctor()
extern void U3CU3Ec__ctor_mE3CA274DBB36540EE7BA9F1E4A90F76BC5110DB6 (void);
// 0x0000075E Mediapipe.Rasterization Mediapipe.Rasterization/<>c::<.cctor>b__26_0()
extern void U3CU3Ec_U3C_cctorU3Eb__26_0_m50047FFDC32D1DB5383C34C58540F56FD8161860 (void);
// 0x0000075F Google.Protobuf.Reflection.FileDescriptor Mediapipe.RectReflection::get_Descriptor()
extern void RectReflection_get_Descriptor_mF9B24077B7445DB6263D0732DD142E06A8F2DC25 (void);
// 0x00000760 System.Void Mediapipe.RectReflection::.cctor()
extern void RectReflection__cctor_mA8E9429E6566D2A8A554D052934AEB9425BA8632 (void);
// 0x00000761 Google.Protobuf.MessageParser`1<Mediapipe.Rect> Mediapipe.Rect::get_Parser()
extern void Rect_get_Parser_m98235111ED335DB876DFFC45B10E69E395B391EA (void);
// 0x00000762 Google.Protobuf.Reflection.MessageDescriptor Mediapipe.Rect::get_Descriptor()
extern void Rect_get_Descriptor_m462AC6B7F7026C17CB1E522A321AE1C81E1C4FDD (void);
// 0x00000763 Google.Protobuf.Reflection.MessageDescriptor Mediapipe.Rect::pb::Google.Protobuf.IMessage.get_Descriptor()
extern void Rect_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_mFAD8D84C1243E352DB00110738FD51648FC9C1C5 (void);
// 0x00000764 System.Void Mediapipe.Rect::.ctor()
extern void Rect__ctor_mA1A953C00E9D1E93F5FE705F28869BF03281E90E (void);
// 0x00000765 System.Void Mediapipe.Rect::.ctor(Mediapipe.Rect)
extern void Rect__ctor_m3ABE7AB6875A28451F7D68FCD2316A5FFD6A33CF (void);
// 0x00000766 Mediapipe.Rect Mediapipe.Rect::Clone()
extern void Rect_Clone_m32E8FD503464BCE245D992DC44220A17FCD192CF (void);
// 0x00000767 System.Int32 Mediapipe.Rect::get_XCenter()
extern void Rect_get_XCenter_m7B125BE5C07A964D070D0F3C8ECDBC8F87CD8F0B (void);
// 0x00000768 System.Void Mediapipe.Rect::set_XCenter(System.Int32)
extern void Rect_set_XCenter_m2120BA596A84FA7E3598D3F4A6F6624D04786635 (void);
// 0x00000769 System.Boolean Mediapipe.Rect::get_HasXCenter()
extern void Rect_get_HasXCenter_m3218ED325A15C35B1E3EDAF3DBA662856BC70E1F (void);
// 0x0000076A System.Void Mediapipe.Rect::ClearXCenter()
extern void Rect_ClearXCenter_m907A2A14A98D9BE8337AB11E1E11BA6D82CA38C9 (void);
// 0x0000076B System.Int32 Mediapipe.Rect::get_YCenter()
extern void Rect_get_YCenter_m4CA9C08BD965EA7E5DEC233AB2FF595EC39CBDD3 (void);
// 0x0000076C System.Void Mediapipe.Rect::set_YCenter(System.Int32)
extern void Rect_set_YCenter_m88D22E5D111BDF8E7243CA4BECED6916C1B0CCCD (void);
// 0x0000076D System.Boolean Mediapipe.Rect::get_HasYCenter()
extern void Rect_get_HasYCenter_m70A28BCAF43C97F630C363D2B815D0796102BAAB (void);
// 0x0000076E System.Void Mediapipe.Rect::ClearYCenter()
extern void Rect_ClearYCenter_mFC560214642EE24645202364FB8544833F84C9D9 (void);
// 0x0000076F System.Int32 Mediapipe.Rect::get_Height()
extern void Rect_get_Height_m354CC79D4285C99A61C349C319082E927EBDD731 (void);
// 0x00000770 System.Void Mediapipe.Rect::set_Height(System.Int32)
extern void Rect_set_Height_mABA93868A4D03A9434064DEB6CCC2F86B0B94A79 (void);
// 0x00000771 System.Boolean Mediapipe.Rect::get_HasHeight()
extern void Rect_get_HasHeight_m75F4767CFD54C653B3CAB9B5E4D4C16B6B938B8F (void);
// 0x00000772 System.Void Mediapipe.Rect::ClearHeight()
extern void Rect_ClearHeight_m5311A4542887AA3987EAEB56C1EE174BBC46EF90 (void);
// 0x00000773 System.Int32 Mediapipe.Rect::get_Width()
extern void Rect_get_Width_m8899354713E96B92C8D358518DA1928042653B6E (void);
// 0x00000774 System.Void Mediapipe.Rect::set_Width(System.Int32)
extern void Rect_set_Width_m850CC0CCC9F1EA823228AE735E63F0923A862EB6 (void);
// 0x00000775 System.Boolean Mediapipe.Rect::get_HasWidth()
extern void Rect_get_HasWidth_mF1BDF1A0C5E255A2C508BF92ADB03587421A4387 (void);
// 0x00000776 System.Void Mediapipe.Rect::ClearWidth()
extern void Rect_ClearWidth_m2A0EF58EE901101AFA25FA5ED9C7EE9F7990E0D3 (void);
// 0x00000777 System.Single Mediapipe.Rect::get_Rotation()
extern void Rect_get_Rotation_m38557895C2DD75F0C8C844AB4541EBC45CE1DB05 (void);
// 0x00000778 System.Void Mediapipe.Rect::set_Rotation(System.Single)
extern void Rect_set_Rotation_m34CBE8690264C435A2874A794027194AF54EAE53 (void);
// 0x00000779 System.Boolean Mediapipe.Rect::get_HasRotation()
extern void Rect_get_HasRotation_m44421D8283397F738D6AFDF92CD6577589047A7E (void);
// 0x0000077A System.Void Mediapipe.Rect::ClearRotation()
extern void Rect_ClearRotation_m935A6E03D8225670E9D90057502E99B479893CBA (void);
// 0x0000077B System.Int64 Mediapipe.Rect::get_RectId()
extern void Rect_get_RectId_mCBCDFF45E1CCF88B409AE312B9B794C2A5C699F6 (void);
// 0x0000077C System.Void Mediapipe.Rect::set_RectId(System.Int64)
extern void Rect_set_RectId_m4D54E87B39A1FB478BC1C2607000735210C6230E (void);
// 0x0000077D System.Boolean Mediapipe.Rect::get_HasRectId()
extern void Rect_get_HasRectId_m9EADD9046C8F9FD205BD27AD74070F47223038C6 (void);
// 0x0000077E System.Void Mediapipe.Rect::ClearRectId()
extern void Rect_ClearRectId_m2582A0FD1E0F7B073ACCB7C566AD4CD67810589B (void);
// 0x0000077F System.Boolean Mediapipe.Rect::Equals(System.Object)
extern void Rect_Equals_m8D8318A274A804C8F7EC5995536C2C5AA121DAE4 (void);
// 0x00000780 System.Boolean Mediapipe.Rect::Equals(Mediapipe.Rect)
extern void Rect_Equals_m8DDA30D9B6EFF7387D1E42982812D783565B971A (void);
// 0x00000781 System.Int32 Mediapipe.Rect::GetHashCode()
extern void Rect_GetHashCode_m894D11D8F145F7800CD41C46D4923F6701EA73BF (void);
// 0x00000782 System.String Mediapipe.Rect::ToString()
extern void Rect_ToString_m9026BF1BE9688FBA3BBAA49AACA2F0624BE20546 (void);
// 0x00000783 System.Void Mediapipe.Rect::WriteTo(Google.Protobuf.CodedOutputStream)
extern void Rect_WriteTo_m91BA7F768E68CEA159FE04DC30808C0E98FDBBF2 (void);
// 0x00000784 System.Int32 Mediapipe.Rect::CalculateSize()
extern void Rect_CalculateSize_m458355AAEB1961AA3E49528C669FC23E89FF06F0 (void);
// 0x00000785 System.Void Mediapipe.Rect::MergeFrom(Mediapipe.Rect)
extern void Rect_MergeFrom_m04A5B2CFE20DC31FF14B0EB09212D6B01842C229 (void);
// 0x00000786 System.Void Mediapipe.Rect::MergeFrom(Google.Protobuf.CodedInputStream)
extern void Rect_MergeFrom_m7AF35F7AF17270258910584DC176688CC509CA98 (void);
// 0x00000787 System.Void Mediapipe.Rect::.cctor()
extern void Rect__cctor_mA75B58B48EED3CF9E565852DB6D5448583F56927 (void);
// 0x00000788 System.Void Mediapipe.Rect/<>c::.cctor()
extern void U3CU3Ec__cctor_m4A4770B331822B562EE5EE193C32750E283BE3DF (void);
// 0x00000789 System.Void Mediapipe.Rect/<>c::.ctor()
extern void U3CU3Ec__ctor_m5459C7F8EE7CCCE8F83DB2117E7FABB6F056F5AC (void);
// 0x0000078A Mediapipe.Rect Mediapipe.Rect/<>c::<.cctor>b__75_0()
extern void U3CU3Ec_U3C_cctorU3Eb__75_0_m6AB82AD17BC161E3224D5F4D5F27712D66289770 (void);
// 0x0000078B Google.Protobuf.MessageParser`1<Mediapipe.NormalizedRect> Mediapipe.NormalizedRect::get_Parser()
extern void NormalizedRect_get_Parser_m8C13751E7D2D14ABE164CF14742E5A87A26F6275 (void);
// 0x0000078C Google.Protobuf.Reflection.MessageDescriptor Mediapipe.NormalizedRect::get_Descriptor()
extern void NormalizedRect_get_Descriptor_m14C5F42230492AD20E3F307E87F2849CCC0C32CC (void);
// 0x0000078D Google.Protobuf.Reflection.MessageDescriptor Mediapipe.NormalizedRect::pb::Google.Protobuf.IMessage.get_Descriptor()
extern void NormalizedRect_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_mA0E18336F15C80009F18BBBBFF8B939AEE6AAF3E (void);
// 0x0000078E System.Void Mediapipe.NormalizedRect::.ctor()
extern void NormalizedRect__ctor_m3BDEDD59629DCC814DE948B03777DC7E9919548F (void);
// 0x0000078F System.Void Mediapipe.NormalizedRect::.ctor(Mediapipe.NormalizedRect)
extern void NormalizedRect__ctor_m8EA938ADE44DBD9BA74A4EA26BFF929BA398F502 (void);
// 0x00000790 Mediapipe.NormalizedRect Mediapipe.NormalizedRect::Clone()
extern void NormalizedRect_Clone_m8B1DF6560577979C5C4AD7B9924DE8A9682BDA7D (void);
// 0x00000791 System.Single Mediapipe.NormalizedRect::get_XCenter()
extern void NormalizedRect_get_XCenter_m402549B47AE4A14920F5BD959658802A52DAC077 (void);
// 0x00000792 System.Void Mediapipe.NormalizedRect::set_XCenter(System.Single)
extern void NormalizedRect_set_XCenter_mB4D666921287D5399935D8B0A221EAE4C874BE6E (void);
// 0x00000793 System.Boolean Mediapipe.NormalizedRect::get_HasXCenter()
extern void NormalizedRect_get_HasXCenter_m4A29DB626B114ECBABE4872E2C8A39D22D2B66CF (void);
// 0x00000794 System.Void Mediapipe.NormalizedRect::ClearXCenter()
extern void NormalizedRect_ClearXCenter_m6BB5D3B7A5D3DEE0964F2ADB0EFB015498C971D3 (void);
// 0x00000795 System.Single Mediapipe.NormalizedRect::get_YCenter()
extern void NormalizedRect_get_YCenter_mCE364A74C667791CE1D87E9C9747595959BA42BC (void);
// 0x00000796 System.Void Mediapipe.NormalizedRect::set_YCenter(System.Single)
extern void NormalizedRect_set_YCenter_mAB38FF130FD73E0B3F64FEFEE9023D2385CA9170 (void);
// 0x00000797 System.Boolean Mediapipe.NormalizedRect::get_HasYCenter()
extern void NormalizedRect_get_HasYCenter_m81A458F976C4C1A6E5663AD88DDB06271AD4DA0B (void);
// 0x00000798 System.Void Mediapipe.NormalizedRect::ClearYCenter()
extern void NormalizedRect_ClearYCenter_m964FB56A39DF73D2EBABDF9B3AB8E4B525BF68BB (void);
// 0x00000799 System.Single Mediapipe.NormalizedRect::get_Height()
extern void NormalizedRect_get_Height_m749962C2051B61123CF5A9F3821B0D87B5ECA65F (void);
// 0x0000079A System.Void Mediapipe.NormalizedRect::set_Height(System.Single)
extern void NormalizedRect_set_Height_mF1E3A66160401E5A69416DCD3039CB901259811F (void);
// 0x0000079B System.Boolean Mediapipe.NormalizedRect::get_HasHeight()
extern void NormalizedRect_get_HasHeight_m18CC31339EED4358DD685551B8315F114CAA4F43 (void);
// 0x0000079C System.Void Mediapipe.NormalizedRect::ClearHeight()
extern void NormalizedRect_ClearHeight_m53F519FBB27E41F1015BBF0F9B4F943CB99D521E (void);
// 0x0000079D System.Single Mediapipe.NormalizedRect::get_Width()
extern void NormalizedRect_get_Width_m6FE64C8C882BBDF83DBEC78ECBA73E6850D66F11 (void);
// 0x0000079E System.Void Mediapipe.NormalizedRect::set_Width(System.Single)
extern void NormalizedRect_set_Width_mFE5B0905BFE8C200A855A21F8E53C2BD5676968E (void);
// 0x0000079F System.Boolean Mediapipe.NormalizedRect::get_HasWidth()
extern void NormalizedRect_get_HasWidth_mC2EC84AA440FC725023F4DF01D0C720428F9C933 (void);
// 0x000007A0 System.Void Mediapipe.NormalizedRect::ClearWidth()
extern void NormalizedRect_ClearWidth_m57AD3453E31C13D518C962CB8E89D347AC9A05F5 (void);
// 0x000007A1 System.Single Mediapipe.NormalizedRect::get_Rotation()
extern void NormalizedRect_get_Rotation_m061EFC7302DC903A7829CB4001298B5D6FEC949A (void);
// 0x000007A2 System.Void Mediapipe.NormalizedRect::set_Rotation(System.Single)
extern void NormalizedRect_set_Rotation_mE9365876335B35270FE3DFC096232081F0424E0E (void);
// 0x000007A3 System.Boolean Mediapipe.NormalizedRect::get_HasRotation()
extern void NormalizedRect_get_HasRotation_m34EC10DD68E324B40C0087FA8C6330363BA95900 (void);
// 0x000007A4 System.Void Mediapipe.NormalizedRect::ClearRotation()
extern void NormalizedRect_ClearRotation_m7743803A9EE445B91B7637283BAC2D3783CC3323 (void);
// 0x000007A5 System.Int64 Mediapipe.NormalizedRect::get_RectId()
extern void NormalizedRect_get_RectId_m3FEE336967193E05AC2ED9E4AFE4FD0A941B689C (void);
// 0x000007A6 System.Void Mediapipe.NormalizedRect::set_RectId(System.Int64)
extern void NormalizedRect_set_RectId_m368D8164D61251708A194018D3E79AE0A5760E79 (void);
// 0x000007A7 System.Boolean Mediapipe.NormalizedRect::get_HasRectId()
extern void NormalizedRect_get_HasRectId_mF05F42B5D6694AE579E7434BF4AC33E45BA6CE34 (void);
// 0x000007A8 System.Void Mediapipe.NormalizedRect::ClearRectId()
extern void NormalizedRect_ClearRectId_m1C7E71F564CAE806DEC00BFA1C8EC5E9A7C76006 (void);
// 0x000007A9 System.Boolean Mediapipe.NormalizedRect::Equals(System.Object)
extern void NormalizedRect_Equals_m5A0389CD1BF8AFEEBA109FE3BC72A84F2B69E2BE (void);
// 0x000007AA System.Boolean Mediapipe.NormalizedRect::Equals(Mediapipe.NormalizedRect)
extern void NormalizedRect_Equals_m4650183705C1C20A3686FCE49AA990E2CCFA9167 (void);
// 0x000007AB System.Int32 Mediapipe.NormalizedRect::GetHashCode()
extern void NormalizedRect_GetHashCode_m2835A32974C581C5AACF9EA8640D3499FAE29B5A (void);
// 0x000007AC System.String Mediapipe.NormalizedRect::ToString()
extern void NormalizedRect_ToString_mD9680EE1C02BBFA4E938610290B3E2AAA4AAA896 (void);
// 0x000007AD System.Void Mediapipe.NormalizedRect::WriteTo(Google.Protobuf.CodedOutputStream)
extern void NormalizedRect_WriteTo_m223F03100F382A13B791A585F64ABB578CB91BF3 (void);
// 0x000007AE System.Int32 Mediapipe.NormalizedRect::CalculateSize()
extern void NormalizedRect_CalculateSize_mBE22B1A36E88C86ED0DB31FBEE65AB6B6A35CB63 (void);
// 0x000007AF System.Void Mediapipe.NormalizedRect::MergeFrom(Mediapipe.NormalizedRect)
extern void NormalizedRect_MergeFrom_mDBA715172C2C850F99074679573E1BB7FF7589E1 (void);
// 0x000007B0 System.Void Mediapipe.NormalizedRect::MergeFrom(Google.Protobuf.CodedInputStream)
extern void NormalizedRect_MergeFrom_m0469B3573A2B64594C4690D3E91CD1C41EFD382E (void);
// 0x000007B1 System.Void Mediapipe.NormalizedRect::.cctor()
extern void NormalizedRect__cctor_mCE05B178C2744B9C6ED2B2B85E6FC100EF16D42E (void);
// 0x000007B2 System.Void Mediapipe.NormalizedRect/<>c::.cctor()
extern void U3CU3Ec__cctor_m5C21ECDB600B1693862D47658B399EB51C1D68F6 (void);
// 0x000007B3 System.Void Mediapipe.NormalizedRect/<>c::.ctor()
extern void U3CU3Ec__ctor_mBF363638E935466AD719821B3D5B7CC45532C0D9 (void);
// 0x000007B4 Mediapipe.NormalizedRect Mediapipe.NormalizedRect/<>c::<.cctor>b__75_0()
extern void U3CU3Ec_U3C_cctorU3Eb__75_0_m034146C904E2BE74D27E9BAC8D5B181291971C7C (void);
// 0x000007B5 Google.Protobuf.Reflection.FileDescriptor Mediapipe.StatusHandlerReflection::get_Descriptor()
extern void StatusHandlerReflection_get_Descriptor_m7EEC754F9336FC07559A4B3701EF8BD34C8187A5 (void);
// 0x000007B6 System.Void Mediapipe.StatusHandlerReflection::.cctor()
extern void StatusHandlerReflection__cctor_m4790B66CFE436359ADF4912A63A22D1FD66CF4C1 (void);
// 0x000007B7 Google.Protobuf.MessageParser`1<Mediapipe.StatusHandlerConfig> Mediapipe.StatusHandlerConfig::get_Parser()
extern void StatusHandlerConfig_get_Parser_m0A4A67DD4165867318F98B1B6C915263D380FE23 (void);
// 0x000007B8 Google.Protobuf.Reflection.MessageDescriptor Mediapipe.StatusHandlerConfig::get_Descriptor()
extern void StatusHandlerConfig_get_Descriptor_m4E6AA499370AD10699F80524D6AF4F7D89865B64 (void);
// 0x000007B9 Google.Protobuf.Reflection.MessageDescriptor Mediapipe.StatusHandlerConfig::pb::Google.Protobuf.IMessage.get_Descriptor()
extern void StatusHandlerConfig_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_mA1108C988D2F9AD80EEE293BA13D2762F0396EF3 (void);
// 0x000007BA System.Void Mediapipe.StatusHandlerConfig::.ctor()
extern void StatusHandlerConfig__ctor_m0C87DFFEF9B52D562AD68762DD83C895E084F1EC (void);
// 0x000007BB System.Void Mediapipe.StatusHandlerConfig::.ctor(Mediapipe.StatusHandlerConfig)
extern void StatusHandlerConfig__ctor_mC337F4A5270315F9DF7FE4372468EDE59F5D9B1B (void);
// 0x000007BC Mediapipe.StatusHandlerConfig Mediapipe.StatusHandlerConfig::Clone()
extern void StatusHandlerConfig_Clone_mD7BF122D61DDC2B08ACA4521444F692937EB82C1 (void);
// 0x000007BD System.String Mediapipe.StatusHandlerConfig::get_StatusHandler()
extern void StatusHandlerConfig_get_StatusHandler_m09C76869F2B90C570AFF218077E95724D57F9242 (void);
// 0x000007BE System.Void Mediapipe.StatusHandlerConfig::set_StatusHandler(System.String)
extern void StatusHandlerConfig_set_StatusHandler_mC8C9625D755F9B836F6A6B7FF1E34C3646A2BDFF (void);
// 0x000007BF System.Boolean Mediapipe.StatusHandlerConfig::get_HasStatusHandler()
extern void StatusHandlerConfig_get_HasStatusHandler_mC33C08CC036C1996A6A7D6734109E8C0D70A529F (void);
// 0x000007C0 System.Void Mediapipe.StatusHandlerConfig::ClearStatusHandler()
extern void StatusHandlerConfig_ClearStatusHandler_m07CC6662A252212369F7D57B3E3A518A8F8DCA2B (void);
// 0x000007C1 Google.Protobuf.Collections.RepeatedField`1<System.String> Mediapipe.StatusHandlerConfig::get_InputSidePacket()
extern void StatusHandlerConfig_get_InputSidePacket_m88B8E7820B498FB8B3282A614871D62F9790E044 (void);
// 0x000007C2 Google.Protobuf.Collections.RepeatedField`1<System.String> Mediapipe.StatusHandlerConfig::get_ExternalInput()
extern void StatusHandlerConfig_get_ExternalInput_m60A2B1A8CB82571741D813C858F7871C9A8D7802 (void);
// 0x000007C3 Mediapipe.MediaPipeOptions Mediapipe.StatusHandlerConfig::get_Options()
extern void StatusHandlerConfig_get_Options_m9556B0C27EF60D5311FAB6BF7DE543D21FFF7135 (void);
// 0x000007C4 System.Void Mediapipe.StatusHandlerConfig::set_Options(Mediapipe.MediaPipeOptions)
extern void StatusHandlerConfig_set_Options_m5EE39D6ABEBEAB79EDE6EB845713E23037AC4820 (void);
// 0x000007C5 System.Boolean Mediapipe.StatusHandlerConfig::get_HasOptions()
extern void StatusHandlerConfig_get_HasOptions_m7598A008AB1AECA47E1D63037E89206E9E712DAD (void);
// 0x000007C6 System.Void Mediapipe.StatusHandlerConfig::ClearOptions()
extern void StatusHandlerConfig_ClearOptions_mE947E969C224326EA97B7903F732FEC89BFF1176 (void);
// 0x000007C7 System.Boolean Mediapipe.StatusHandlerConfig::Equals(System.Object)
extern void StatusHandlerConfig_Equals_mE15F141F7DD9A8E50CE22E98E94843557421C569 (void);
// 0x000007C8 System.Boolean Mediapipe.StatusHandlerConfig::Equals(Mediapipe.StatusHandlerConfig)
extern void StatusHandlerConfig_Equals_mD1A6F9828313594E5C7EF144F5119F54EEE0435A (void);
// 0x000007C9 System.Int32 Mediapipe.StatusHandlerConfig::GetHashCode()
extern void StatusHandlerConfig_GetHashCode_mF6A6AD649A9387349F6B83D7844CDAECC44AA4C4 (void);
// 0x000007CA System.String Mediapipe.StatusHandlerConfig::ToString()
extern void StatusHandlerConfig_ToString_m128817CC910667012E7587B84888E8EB925DE190 (void);
// 0x000007CB System.Void Mediapipe.StatusHandlerConfig::WriteTo(Google.Protobuf.CodedOutputStream)
extern void StatusHandlerConfig_WriteTo_m160C08E410413EC1615505EF407B79F1557ACE34 (void);
// 0x000007CC System.Int32 Mediapipe.StatusHandlerConfig::CalculateSize()
extern void StatusHandlerConfig_CalculateSize_mA82A950CEEDBF1E241EFEF0B855551966114782D (void);
// 0x000007CD System.Void Mediapipe.StatusHandlerConfig::MergeFrom(Mediapipe.StatusHandlerConfig)
extern void StatusHandlerConfig_MergeFrom_m6F38CBD207080A9DE0CF68449CCC1AA881877239 (void);
// 0x000007CE System.Void Mediapipe.StatusHandlerConfig::MergeFrom(Google.Protobuf.CodedInputStream)
extern void StatusHandlerConfig_MergeFrom_mAAE549448B55E008DBF0395D6BB87C183EF0ACCE (void);
// 0x000007CF System.Void Mediapipe.StatusHandlerConfig::.cctor()
extern void StatusHandlerConfig__cctor_mAC512BE0806197983D1A42BF1E7DDBDD7100756B (void);
// 0x000007D0 System.Void Mediapipe.StatusHandlerConfig/<>c::.cctor()
extern void U3CU3Ec__cctor_m21D5794CD8B001010BB8BD89B14BFDF7F0AACECA (void);
// 0x000007D1 System.Void Mediapipe.StatusHandlerConfig/<>c::.ctor()
extern void U3CU3Ec__ctor_m26EA9557C770A91C908FF088A128F912825D6F72 (void);
// 0x000007D2 Mediapipe.StatusHandlerConfig Mediapipe.StatusHandlerConfig/<>c::<.cctor>b__47_0()
extern void U3CU3Ec_U3C_cctorU3Eb__47_0_m52B50B745248EBEB07E844523F4248CBF7F2934C (void);
// 0x000007D3 Google.Protobuf.Reflection.FileDescriptor Mediapipe.StickerBufferReflection::get_Descriptor()
extern void StickerBufferReflection_get_Descriptor_mB3FE44F901AC3DB3714B7D33084016FD50A91D5D (void);
// 0x000007D4 System.Void Mediapipe.StickerBufferReflection::.cctor()
extern void StickerBufferReflection__cctor_m8F46B00F2E0C5322E9DD9CBB62766CC1FC2F50C5 (void);
// 0x000007D5 Google.Protobuf.MessageParser`1<Mediapipe.Sticker> Mediapipe.Sticker::get_Parser()
extern void Sticker_get_Parser_mC18FF538852660F306ACCD7DC67172B8C29EC13B (void);
// 0x000007D6 Google.Protobuf.Reflection.MessageDescriptor Mediapipe.Sticker::get_Descriptor()
extern void Sticker_get_Descriptor_mA98C9309CD1B3867D18CAE0376F1E3FB2D72E607 (void);
// 0x000007D7 Google.Protobuf.Reflection.MessageDescriptor Mediapipe.Sticker::pb::Google.Protobuf.IMessage.get_Descriptor()
extern void Sticker_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m04DD9EAF9FD06741B9792A4F74D74481EF989D5A (void);
// 0x000007D8 System.Void Mediapipe.Sticker::.ctor()
extern void Sticker__ctor_m9F07EA56D9484EC587B82322B0E0718DF88BF800 (void);
// 0x000007D9 System.Void Mediapipe.Sticker::.ctor(Mediapipe.Sticker)
extern void Sticker__ctor_m2E998EAE290A49D1067301693237681798D172D9 (void);
// 0x000007DA Mediapipe.Sticker Mediapipe.Sticker::Clone()
extern void Sticker_Clone_mC616C2DA4B6978BDCE602EC25D2AEBEAC58CC7B2 (void);
// 0x000007DB System.Int32 Mediapipe.Sticker::get_Id()
extern void Sticker_get_Id_m6A6F305A3A97EA660205F61C2E87E84EC7E66B23 (void);
// 0x000007DC System.Void Mediapipe.Sticker::set_Id(System.Int32)
extern void Sticker_set_Id_m888A78065D7262BFE9DA550FA12D51ABFBA10279 (void);
// 0x000007DD System.Boolean Mediapipe.Sticker::get_HasId()
extern void Sticker_get_HasId_mDBBD47AE5C6B20B0448521A24696CF329162CB90 (void);
// 0x000007DE System.Void Mediapipe.Sticker::ClearId()
extern void Sticker_ClearId_m9A821A6CA49F2B6488490BC5012C1A372AA225B2 (void);
// 0x000007DF System.Single Mediapipe.Sticker::get_X()
extern void Sticker_get_X_m6A2F7BCF32D0E5A6D91635B7D813FBD1A4F339C3 (void);
// 0x000007E0 System.Void Mediapipe.Sticker::set_X(System.Single)
extern void Sticker_set_X_mF9572948EB2207976B7E87C8CFF59194E31E6AD3 (void);
// 0x000007E1 System.Boolean Mediapipe.Sticker::get_HasX()
extern void Sticker_get_HasX_m9C9CB7821D3DC1F5702936CBD837599F52F8DF5A (void);
// 0x000007E2 System.Void Mediapipe.Sticker::ClearX()
extern void Sticker_ClearX_m81A2073EC1E6E28D522BA83A09D25B59054C10B4 (void);
// 0x000007E3 System.Single Mediapipe.Sticker::get_Y()
extern void Sticker_get_Y_m1CCB1E96BC8AA1023A9CEA6AAC206F4ADA001939 (void);
// 0x000007E4 System.Void Mediapipe.Sticker::set_Y(System.Single)
extern void Sticker_set_Y_mCA7DAB1AA71BA2D84DC59B5A93111F63C38FD900 (void);
// 0x000007E5 System.Boolean Mediapipe.Sticker::get_HasY()
extern void Sticker_get_HasY_mB877A78ECFA2DAC7E9F88B467A270282ABBEE4BF (void);
// 0x000007E6 System.Void Mediapipe.Sticker::ClearY()
extern void Sticker_ClearY_mCCDB6ED5BBCC5470C536E9308EEDBF03EBC5A1FC (void);
// 0x000007E7 System.Single Mediapipe.Sticker::get_Rotation()
extern void Sticker_get_Rotation_mB99D72196F6DABFD1AE85692ED9168248F3D294E (void);
// 0x000007E8 System.Void Mediapipe.Sticker::set_Rotation(System.Single)
extern void Sticker_set_Rotation_m1AEF75979F94284AB27A69B73EF48FC7F19EFFF0 (void);
// 0x000007E9 System.Boolean Mediapipe.Sticker::get_HasRotation()
extern void Sticker_get_HasRotation_m2AD7D36D27D501DA435A9DC0E79060FBDF555034 (void);
// 0x000007EA System.Void Mediapipe.Sticker::ClearRotation()
extern void Sticker_ClearRotation_m706F6BE276F0822E52C34373C1A3A5DB0C7B3D9E (void);
// 0x000007EB System.Single Mediapipe.Sticker::get_Scale()
extern void Sticker_get_Scale_m8AD71F2543FC2FD340F7D4F24A942AD203511E61 (void);
// 0x000007EC System.Void Mediapipe.Sticker::set_Scale(System.Single)
extern void Sticker_set_Scale_mBE3F4173B6290B822D8FC31CED729E9244AE67F1 (void);
// 0x000007ED System.Boolean Mediapipe.Sticker::get_HasScale()
extern void Sticker_get_HasScale_mA12ED4EA0E7C5023BF873151CC5B39F6781F0BC7 (void);
// 0x000007EE System.Void Mediapipe.Sticker::ClearScale()
extern void Sticker_ClearScale_m66C7A603A0A374BFD1C1973F4F3C30399BD6260A (void);
// 0x000007EF System.Int32 Mediapipe.Sticker::get_RenderId()
extern void Sticker_get_RenderId_mE15BE0AF84C1C3C46063C7DAD1C62AE1E51A9E81 (void);
// 0x000007F0 System.Void Mediapipe.Sticker::set_RenderId(System.Int32)
extern void Sticker_set_RenderId_m07E792919CA92E8B7FB3100549FF931DDF619792 (void);
// 0x000007F1 System.Boolean Mediapipe.Sticker::get_HasRenderId()
extern void Sticker_get_HasRenderId_m56B83DEFDAE76BF517607A917E0079CDB7EF7613 (void);
// 0x000007F2 System.Void Mediapipe.Sticker::ClearRenderId()
extern void Sticker_ClearRenderId_mEA835CE5755D482612166621EAC9FF4C05607150 (void);
// 0x000007F3 System.Boolean Mediapipe.Sticker::Equals(System.Object)
extern void Sticker_Equals_m35D29CDEEB1B41EC384FEC46477C4042D40B05AF (void);
// 0x000007F4 System.Boolean Mediapipe.Sticker::Equals(Mediapipe.Sticker)
extern void Sticker_Equals_m337E02F0A28B1447BA8B24631C35FF1C286E60C9 (void);
// 0x000007F5 System.Int32 Mediapipe.Sticker::GetHashCode()
extern void Sticker_GetHashCode_m24AFE04034BF649FC23E1E49AD29DEAEEE7C04AB (void);
// 0x000007F6 System.String Mediapipe.Sticker::ToString()
extern void Sticker_ToString_m044B37DFDBABC8E510CCDB2944D1A7819B5149D0 (void);
// 0x000007F7 System.Void Mediapipe.Sticker::WriteTo(Google.Protobuf.CodedOutputStream)
extern void Sticker_WriteTo_m0BAF019B7204C3399E1FA485E981DC36F94648DB (void);
// 0x000007F8 System.Int32 Mediapipe.Sticker::CalculateSize()
extern void Sticker_CalculateSize_mB443FFC825293F7F0ED3AD2A415E4621EF3EA59B (void);
// 0x000007F9 System.Void Mediapipe.Sticker::MergeFrom(Mediapipe.Sticker)
extern void Sticker_MergeFrom_mCF73E0750E5110F3AF4A75B6A09824D7B4484C9B (void);
// 0x000007FA System.Void Mediapipe.Sticker::MergeFrom(Google.Protobuf.CodedInputStream)
extern void Sticker_MergeFrom_m9CADCC2E1344332FBCAC86D3FA7488F908175131 (void);
// 0x000007FB System.Void Mediapipe.Sticker::.cctor()
extern void Sticker__cctor_m0C4755987159010446621AD1084BF59744AC27A3 (void);
// 0x000007FC System.Void Mediapipe.Sticker/<>c::.cctor()
extern void U3CU3Ec__cctor_m8C392E929527BF44523EE264246A456292378A8A (void);
// 0x000007FD System.Void Mediapipe.Sticker/<>c::.ctor()
extern void U3CU3Ec__ctor_m5932EC36633AA073587C123500337B40C96E3CBC (void);
// 0x000007FE Mediapipe.Sticker Mediapipe.Sticker/<>c::<.cctor>b__75_0()
extern void U3CU3Ec_U3C_cctorU3Eb__75_0_mED5F2CB9683632BE9CC3F22608AD48689E3EC8DA (void);
// 0x000007FF Google.Protobuf.MessageParser`1<Mediapipe.StickerRoll> Mediapipe.StickerRoll::get_Parser()
extern void StickerRoll_get_Parser_m0307904586A1C10AEF41DFADF886EE6122652E4B (void);
// 0x00000800 Google.Protobuf.Reflection.MessageDescriptor Mediapipe.StickerRoll::get_Descriptor()
extern void StickerRoll_get_Descriptor_m6774AF565D11E97EFBD6FC4D76F55E6F501127E9 (void);
// 0x00000801 Google.Protobuf.Reflection.MessageDescriptor Mediapipe.StickerRoll::pb::Google.Protobuf.IMessage.get_Descriptor()
extern void StickerRoll_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_mD48A89BFE8E416135A4F58D39F4CAAB1DEF88FF9 (void);
// 0x00000802 System.Void Mediapipe.StickerRoll::.ctor()
extern void StickerRoll__ctor_m7D0E0B4B49A31F27819E3777EDFB6B58A11174DB (void);
// 0x00000803 System.Void Mediapipe.StickerRoll::.ctor(Mediapipe.StickerRoll)
extern void StickerRoll__ctor_mC0E9A98013535B1F5E13F0E01117B200EC09E1C7 (void);
// 0x00000804 Mediapipe.StickerRoll Mediapipe.StickerRoll::Clone()
extern void StickerRoll_Clone_m096072A72723DB3BF8F7311D5E14236BAEC77850 (void);
// 0x00000805 Google.Protobuf.Collections.RepeatedField`1<Mediapipe.Sticker> Mediapipe.StickerRoll::get_Sticker()
extern void StickerRoll_get_Sticker_mA5E92ADF3019AE63B200B754B6EEEFEAE3259DE7 (void);
// 0x00000806 System.Boolean Mediapipe.StickerRoll::Equals(System.Object)
extern void StickerRoll_Equals_m3E5A8335E874E955AA1A7EB2D2400B4BC6A36F93 (void);
// 0x00000807 System.Boolean Mediapipe.StickerRoll::Equals(Mediapipe.StickerRoll)
extern void StickerRoll_Equals_m1583815F5C84B1A33349A25E7C1D10D9B454B786 (void);
// 0x00000808 System.Int32 Mediapipe.StickerRoll::GetHashCode()
extern void StickerRoll_GetHashCode_mB2561E014D54E102F9091D03556DCF6D1A678F56 (void);
// 0x00000809 System.String Mediapipe.StickerRoll::ToString()
extern void StickerRoll_ToString_m18198D57FDEDBBC074B162CC75414E505B20431E (void);
// 0x0000080A System.Void Mediapipe.StickerRoll::WriteTo(Google.Protobuf.CodedOutputStream)
extern void StickerRoll_WriteTo_m152931EE67A3A849500DCF04913BBB3CEB5B549E (void);
// 0x0000080B System.Int32 Mediapipe.StickerRoll::CalculateSize()
extern void StickerRoll_CalculateSize_m57AA5A87B5891693324E260A333D8B70D013EC0C (void);
// 0x0000080C System.Void Mediapipe.StickerRoll::MergeFrom(Mediapipe.StickerRoll)
extern void StickerRoll_MergeFrom_m7339523DC45A8AF327B08EEC2E9BC55C83E0381A (void);
// 0x0000080D System.Void Mediapipe.StickerRoll::MergeFrom(Google.Protobuf.CodedInputStream)
extern void StickerRoll_MergeFrom_m46C03127E58A827A900A8602C569D8E8343441F5 (void);
// 0x0000080E System.Void Mediapipe.StickerRoll::.cctor()
extern void StickerRoll__cctor_m60812142AD83B7368DC33F75737FC58454BAD465 (void);
// 0x0000080F System.Void Mediapipe.StickerRoll/<>c::.cctor()
extern void U3CU3Ec__cctor_m96046462691983A53ED7AEF1E0B189075CAAE3C8 (void);
// 0x00000810 System.Void Mediapipe.StickerRoll/<>c::.ctor()
extern void U3CU3Ec__ctor_mF9AA2BD3B61FDD07E3CE2E593B209201D824F673 (void);
// 0x00000811 Mediapipe.StickerRoll Mediapipe.StickerRoll/<>c::<.cctor>b__25_0()
extern void U3CU3Ec_U3C_cctorU3Eb__25_0_mEBF95CA4F792EE23C899F16109098062B3FBA6CC (void);
// 0x00000812 Google.Protobuf.Reflection.FileDescriptor Mediapipe.StreamHandlerReflection::get_Descriptor()
extern void StreamHandlerReflection_get_Descriptor_m4C0AAD33D6DEA9D36A1BA53C7C942B4C093402C3 (void);
// 0x00000813 System.Void Mediapipe.StreamHandlerReflection::.cctor()
extern void StreamHandlerReflection__cctor_m53BADF26D07943262104CFE505C2DB0A1B7C4FD4 (void);
// 0x00000814 Google.Protobuf.MessageParser`1<Mediapipe.InputStreamHandlerConfig> Mediapipe.InputStreamHandlerConfig::get_Parser()
extern void InputStreamHandlerConfig_get_Parser_m5A2FBD44467CDAEF2AECF4DA25B1405747213C16 (void);
// 0x00000815 Google.Protobuf.Reflection.MessageDescriptor Mediapipe.InputStreamHandlerConfig::get_Descriptor()
extern void InputStreamHandlerConfig_get_Descriptor_m5762E4DFAD2A565C5E1557DFC43F67F8141B224A (void);
// 0x00000816 Google.Protobuf.Reflection.MessageDescriptor Mediapipe.InputStreamHandlerConfig::pb::Google.Protobuf.IMessage.get_Descriptor()
extern void InputStreamHandlerConfig_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_mAEC6F813A8A47C89CA2CB0AC7F1D8FA774614736 (void);
// 0x00000817 System.Void Mediapipe.InputStreamHandlerConfig::.ctor()
extern void InputStreamHandlerConfig__ctor_mDA2A0ADCFBE563186C6F1E56BDBDC3F19522EF52 (void);
// 0x00000818 System.Void Mediapipe.InputStreamHandlerConfig::.ctor(Mediapipe.InputStreamHandlerConfig)
extern void InputStreamHandlerConfig__ctor_m2A5888DCE80A2B2DB059CB563795F28C050114F9 (void);
// 0x00000819 Mediapipe.InputStreamHandlerConfig Mediapipe.InputStreamHandlerConfig::Clone()
extern void InputStreamHandlerConfig_Clone_m1C7D8601234B62154567A71C893796A3DD5BCFB8 (void);
// 0x0000081A System.String Mediapipe.InputStreamHandlerConfig::get_InputStreamHandler()
extern void InputStreamHandlerConfig_get_InputStreamHandler_m81B4AD2B70646F815C786C397D4CC53CA43EF132 (void);
// 0x0000081B System.Void Mediapipe.InputStreamHandlerConfig::set_InputStreamHandler(System.String)
extern void InputStreamHandlerConfig_set_InputStreamHandler_m606ACF5FE64DBEE4114CB0C942BEE8077A75053B (void);
// 0x0000081C System.Boolean Mediapipe.InputStreamHandlerConfig::get_HasInputStreamHandler()
extern void InputStreamHandlerConfig_get_HasInputStreamHandler_m1A01EFA0B87D41AFE8E27F9383AFDE538A3743DB (void);
// 0x0000081D System.Void Mediapipe.InputStreamHandlerConfig::ClearInputStreamHandler()
extern void InputStreamHandlerConfig_ClearInputStreamHandler_m778DA275FBA99054E9B2367B45EE2DA982BE3CB3 (void);
// 0x0000081E Mediapipe.MediaPipeOptions Mediapipe.InputStreamHandlerConfig::get_Options()
extern void InputStreamHandlerConfig_get_Options_mA1AB139E91179028EEEFB75E9A33D4A2E2671CDC (void);
// 0x0000081F System.Void Mediapipe.InputStreamHandlerConfig::set_Options(Mediapipe.MediaPipeOptions)
extern void InputStreamHandlerConfig_set_Options_mFCC50B13F986F7B1D27CD77A461DC1D865A446E7 (void);
// 0x00000820 System.Boolean Mediapipe.InputStreamHandlerConfig::get_HasOptions()
extern void InputStreamHandlerConfig_get_HasOptions_mAEE226DDE7B0313B7AE4E7FFDF8389CD28EDF15E (void);
// 0x00000821 System.Void Mediapipe.InputStreamHandlerConfig::ClearOptions()
extern void InputStreamHandlerConfig_ClearOptions_mC4B0ECD6F0DA723730F3E45FFDBE9D5AC42DFED4 (void);
// 0x00000822 System.Boolean Mediapipe.InputStreamHandlerConfig::Equals(System.Object)
extern void InputStreamHandlerConfig_Equals_m920E22E1FFCB71B026DD2541AE7343A1DC541032 (void);
// 0x00000823 System.Boolean Mediapipe.InputStreamHandlerConfig::Equals(Mediapipe.InputStreamHandlerConfig)
extern void InputStreamHandlerConfig_Equals_mE3FF7A087C37572FAEDD71033B5856E49AA4BAE7 (void);
// 0x00000824 System.Int32 Mediapipe.InputStreamHandlerConfig::GetHashCode()
extern void InputStreamHandlerConfig_GetHashCode_m58B4E8D647A7C52BAC4306EBEBE35E370DA58F4B (void);
// 0x00000825 System.String Mediapipe.InputStreamHandlerConfig::ToString()
extern void InputStreamHandlerConfig_ToString_m5A51A1B49D862152FB108FDD9E7EC26DBDD754C7 (void);
// 0x00000826 System.Void Mediapipe.InputStreamHandlerConfig::WriteTo(Google.Protobuf.CodedOutputStream)
extern void InputStreamHandlerConfig_WriteTo_mCE51105C65479A27CC3078A2C761EAA386676EF1 (void);
// 0x00000827 System.Int32 Mediapipe.InputStreamHandlerConfig::CalculateSize()
extern void InputStreamHandlerConfig_CalculateSize_m4825B60F49C6987E66E0EA4124F5371E7517963C (void);
// 0x00000828 System.Void Mediapipe.InputStreamHandlerConfig::MergeFrom(Mediapipe.InputStreamHandlerConfig)
extern void InputStreamHandlerConfig_MergeFrom_m5313E6C7EFF4B95CD73FCE7ED2B19FE92CA5B1A2 (void);
// 0x00000829 System.Void Mediapipe.InputStreamHandlerConfig::MergeFrom(Google.Protobuf.CodedInputStream)
extern void InputStreamHandlerConfig_MergeFrom_mB0FF550FD90B38F91C9A89A027AF419A29D5B3E8 (void);
// 0x0000082A System.Void Mediapipe.InputStreamHandlerConfig::.cctor()
extern void InputStreamHandlerConfig__cctor_mBEEAD6E3A616FBB95B12BD9C5D5D30F135045A13 (void);
// 0x0000082B System.Void Mediapipe.InputStreamHandlerConfig/<>c::.cctor()
extern void U3CU3Ec__cctor_m77D23CC30896FBBB46C3C36A07D8CBC0EEFC144D (void);
// 0x0000082C System.Void Mediapipe.InputStreamHandlerConfig/<>c::.ctor()
extern void U3CU3Ec__ctor_mDEAC2631E57057D9364409F52CF7A7B54FFA9257 (void);
// 0x0000082D Mediapipe.InputStreamHandlerConfig Mediapipe.InputStreamHandlerConfig/<>c::<.cctor>b__37_0()
extern void U3CU3Ec_U3C_cctorU3Eb__37_0_m54CAD05E92A63061CFEDF3C7E6709AAEC0FEF8C4 (void);
// 0x0000082E Google.Protobuf.MessageParser`1<Mediapipe.OutputStreamHandlerConfig> Mediapipe.OutputStreamHandlerConfig::get_Parser()
extern void OutputStreamHandlerConfig_get_Parser_mF9FC43981A45E193014FE6F36605CA5C04425B87 (void);
// 0x0000082F Google.Protobuf.Reflection.MessageDescriptor Mediapipe.OutputStreamHandlerConfig::get_Descriptor()
extern void OutputStreamHandlerConfig_get_Descriptor_m6BE140D5A9CCCA6B38B0D55F52758042C0B6C4CC (void);
// 0x00000830 Google.Protobuf.Reflection.MessageDescriptor Mediapipe.OutputStreamHandlerConfig::pb::Google.Protobuf.IMessage.get_Descriptor()
extern void OutputStreamHandlerConfig_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m418EA3EC2CCB02290E49C1ED5F5EA44EBAEBFD85 (void);
// 0x00000831 System.Void Mediapipe.OutputStreamHandlerConfig::.ctor()
extern void OutputStreamHandlerConfig__ctor_m05C62D4D22FF9FF66AF9BB724990B2FFEF12520F (void);
// 0x00000832 System.Void Mediapipe.OutputStreamHandlerConfig::.ctor(Mediapipe.OutputStreamHandlerConfig)
extern void OutputStreamHandlerConfig__ctor_m2C42D8AEFEC3AEA97E86245C7244C6186B1EC37B (void);
// 0x00000833 Mediapipe.OutputStreamHandlerConfig Mediapipe.OutputStreamHandlerConfig::Clone()
extern void OutputStreamHandlerConfig_Clone_mC359C6B6F92C04CD25AA67A7F2E95F90D5837248 (void);
// 0x00000834 System.String Mediapipe.OutputStreamHandlerConfig::get_OutputStreamHandler()
extern void OutputStreamHandlerConfig_get_OutputStreamHandler_m43DC2A2E1275BAF6942DE4496C3EA63FB48B9CA9 (void);
// 0x00000835 System.Void Mediapipe.OutputStreamHandlerConfig::set_OutputStreamHandler(System.String)
extern void OutputStreamHandlerConfig_set_OutputStreamHandler_m025DA963BA4230051CCBBE42AAC1F436A8E79117 (void);
// 0x00000836 System.Boolean Mediapipe.OutputStreamHandlerConfig::get_HasOutputStreamHandler()
extern void OutputStreamHandlerConfig_get_HasOutputStreamHandler_m40386B074A20D5C724123228433177B0E12CA440 (void);
// 0x00000837 System.Void Mediapipe.OutputStreamHandlerConfig::ClearOutputStreamHandler()
extern void OutputStreamHandlerConfig_ClearOutputStreamHandler_m1B66407BF083E4781A6BB19D8C0502845610C3E0 (void);
// 0x00000838 Google.Protobuf.Collections.RepeatedField`1<System.String> Mediapipe.OutputStreamHandlerConfig::get_InputSidePacket()
extern void OutputStreamHandlerConfig_get_InputSidePacket_mBA953460BDC3DA9997FF3F586E6309158DE8DC7F (void);
// 0x00000839 Mediapipe.MediaPipeOptions Mediapipe.OutputStreamHandlerConfig::get_Options()
extern void OutputStreamHandlerConfig_get_Options_m30F898BA1440FCA35ED951AAE22D321B96D38D9F (void);
// 0x0000083A System.Void Mediapipe.OutputStreamHandlerConfig::set_Options(Mediapipe.MediaPipeOptions)
extern void OutputStreamHandlerConfig_set_Options_m8DC46CFA8205EB344A8D9359CF8235A2D1FF5894 (void);
// 0x0000083B System.Boolean Mediapipe.OutputStreamHandlerConfig::get_HasOptions()
extern void OutputStreamHandlerConfig_get_HasOptions_mE26B8E990BC6C61A018DDEE4DFCC044A5DFD6057 (void);
// 0x0000083C System.Void Mediapipe.OutputStreamHandlerConfig::ClearOptions()
extern void OutputStreamHandlerConfig_ClearOptions_mE6629E3A15B7C6E8DA899E244B1473B6F533849C (void);
// 0x0000083D System.Boolean Mediapipe.OutputStreamHandlerConfig::Equals(System.Object)
extern void OutputStreamHandlerConfig_Equals_m6C32151DC752E25C8DEDB017643B7FA8BF3F356C (void);
// 0x0000083E System.Boolean Mediapipe.OutputStreamHandlerConfig::Equals(Mediapipe.OutputStreamHandlerConfig)
extern void OutputStreamHandlerConfig_Equals_m82C75029D3E419F810CAD772CCDE87705D433C2B (void);
// 0x0000083F System.Int32 Mediapipe.OutputStreamHandlerConfig::GetHashCode()
extern void OutputStreamHandlerConfig_GetHashCode_m858E4EF857A0EBEA85DA7F18FC322F4B7CF519C1 (void);
// 0x00000840 System.String Mediapipe.OutputStreamHandlerConfig::ToString()
extern void OutputStreamHandlerConfig_ToString_mD615009A9DBB0AF31B5EC2F952482129EC268201 (void);
// 0x00000841 System.Void Mediapipe.OutputStreamHandlerConfig::WriteTo(Google.Protobuf.CodedOutputStream)
extern void OutputStreamHandlerConfig_WriteTo_m97B3BB527940366F4AD1983486997DF660920C14 (void);
// 0x00000842 System.Int32 Mediapipe.OutputStreamHandlerConfig::CalculateSize()
extern void OutputStreamHandlerConfig_CalculateSize_mF73E0648CDAF78E335B21A903AEAB5223BB210BC (void);
// 0x00000843 System.Void Mediapipe.OutputStreamHandlerConfig::MergeFrom(Mediapipe.OutputStreamHandlerConfig)
extern void OutputStreamHandlerConfig_MergeFrom_mBB90784471D1BF4F6D21792DA33C740B5AE4489F (void);
// 0x00000844 System.Void Mediapipe.OutputStreamHandlerConfig::MergeFrom(Google.Protobuf.CodedInputStream)
extern void OutputStreamHandlerConfig_MergeFrom_mEBEB8A78D238C7BCBA267A3982A7B82F4AB8CE39 (void);
// 0x00000845 System.Void Mediapipe.OutputStreamHandlerConfig::.cctor()
extern void OutputStreamHandlerConfig__cctor_m01950CF61EBD9FD05C693E916B7AB1AAC1CF3293 (void);
// 0x00000846 System.Void Mediapipe.OutputStreamHandlerConfig/<>c::.cctor()
extern void U3CU3Ec__cctor_mBCCE7EE4E6CD62CCA394447265181A61E2FA3C2E (void);
// 0x00000847 System.Void Mediapipe.OutputStreamHandlerConfig/<>c::.ctor()
extern void U3CU3Ec__ctor_m26C569D51FD6D3D8605EFEA5916DAF2C77E7F5C8 (void);
// 0x00000848 Mediapipe.OutputStreamHandlerConfig Mediapipe.OutputStreamHandlerConfig/<>c::<.cctor>b__42_0()
extern void U3CU3Ec_U3C_cctorU3Eb__42_0_m8134538B3AC9021C2DC4C0C3CFE11F0483497A41 (void);
// 0x00000849 System.Void Mediapipe.ResourceHandle::.ctor()
extern void ResourceHandle__ctor_mEB4B6FCE5E50639F862EB76EBAB98ECBC01DF3DB (void);
// 0x0000084A System.Void Mediapipe.ResourceHandle::.ctor(System.IntPtr,System.Boolean)
extern void ResourceHandle__ctor_m14DF1AEA141B44C3D966A8B5A030265E9525CFEA (void);
// 0x0000084B System.Void Mediapipe.ResourceHandle::Finalize()
extern void ResourceHandle_Finalize_m9B7446267C0A4C56D100BAE004C75C4B0D5CC78B (void);
// 0x0000084C System.Void Mediapipe.ResourceHandle::Dispose()
extern void ResourceHandle_Dispose_mB99FA1A03549996F8F341A0E7062DF78EC3694AD (void);
// 0x0000084D System.Void Mediapipe.ResourceHandle::Dispose(System.Boolean)
// 0x0000084E System.IntPtr Mediapipe.ResourceHandle::GetPtr()
extern void ResourceHandle_GetPtr_m9A89B139A0EB33847D14249403BA3312CB520F81 (void);
// 0x0000084F System.Void Mediapipe.ResourceHandle::TakeOwnership(System.IntPtr)
extern void ResourceHandle_TakeOwnership_m746D2259DC0B38680DE32BCDE1AB1215C6E5D937 (void);
// 0x00000850 System.Void Mediapipe.ResourceHandle::ReleaseOwnership()
extern void ResourceHandle_ReleaseOwnership_m77DB1D0688CE2CC1D1EC51CB40DDB70A80578009 (void);
// 0x00000851 System.IntPtr Mediapipe.ResourceHandle::ReleasePtr()
extern void ResourceHandle_ReleasePtr_m02393C40EA7BA4A89BCA4BF19307ACA5872A604F (void);
// 0x00000852 System.Boolean Mediapipe.ResourceHandle::OwnsResource()
extern void ResourceHandle_OwnsResource_mCA1B1D4BAB2C213DC1E66815098E5A90B4E000FD (void);
// 0x00000853 Mediapipe.ResourceManager/PathResolver Mediapipe.AssetBundleManager::get_pathResolver()
extern void AssetBundleManager_get_pathResolver_m8D06EBBF1D16D9F0D530782B3CEA907E3679BD84 (void);
// 0x00000854 Mediapipe.ResourceManager/ResourceProvider Mediapipe.AssetBundleManager::get_resourceProvider()
extern void AssetBundleManager_get_resourceProvider_mDBE671E26545611BEAE69CDBB7E0EC4C2E5DD7A1 (void);
// 0x00000855 System.Void Mediapipe.AssetBundleManager::.ctor()
extern void AssetBundleManager__ctor_m5D0F4D0F9138FAC218B2D563A7824B50C49B787E (void);
// 0x00000856 System.Void Mediapipe.AssetBundleManager::.ctor(System.String,System.String)
extern void AssetBundleManager__ctor_m28B63C1531EE393AC8C55C48CF7A7D7644C5490F (void);
// 0x00000857 System.Boolean Mediapipe.AssetBundleManager::IsPrepared(System.String)
extern void AssetBundleManager_IsPrepared_m4DE649572FA2049D080D084A691BE81E541AE0F0 (void);
// 0x00000858 System.String Mediapipe.AssetBundleManager::get_CacheRootPath()
extern void AssetBundleManager_get_CacheRootPath_mA0AC06F5010C52B6A9378571469F18BAD800E6A1 (void);
// 0x00000859 System.Void Mediapipe.AssetBundleManager::set_CacheRootPath(System.String)
extern void AssetBundleManager_set_CacheRootPath_m62D305D7C69D8AB3D6CC17C05F00B61EB29BE7DF (void);
// 0x0000085A System.String Mediapipe.AssetBundleManager::get_AssetBundlePath()
extern void AssetBundleManager_get_AssetBundlePath_mD41F1371DC761CEB935B497AF6251DB6D250533D (void);
// 0x0000085B System.Void Mediapipe.AssetBundleManager::set_AssetBundlePath(System.String)
extern void AssetBundleManager_set_AssetBundlePath_mE42D6A7BC33AC00DA860220226F4F54292EBF5DF (void);
// 0x0000085C UnityEngine.AssetBundle Mediapipe.AssetBundleManager::get_assetBundle()
extern void AssetBundleManager_get_assetBundle_mDC75A1793A1CBDAC61570F8F386F1AA1D1C928B6 (void);
// 0x0000085D System.Void Mediapipe.AssetBundleManager::set_assetBundle(UnityEngine.AssetBundle)
extern void AssetBundleManager_set_assetBundle_m69BFB10AD41A82965239CA847947EDEE62771947 (void);
// 0x0000085E System.Void Mediapipe.AssetBundleManager::ClearAllCacheFiles()
extern void AssetBundleManager_ClearAllCacheFiles_m8361B7BAE41DBC7576CFEFFD50CCA4AC1EB0F832 (void);
// 0x0000085F System.Void Mediapipe.AssetBundleManager::LoadAssetBundle()
extern void AssetBundleManager_LoadAssetBundle_mD8015C302101C1C31C4F656FFDBFB4147F1A9C0C (void);
// 0x00000860 System.Threading.Tasks.Task Mediapipe.AssetBundleManager::LoadAssetBundleAsync()
extern void AssetBundleManager_LoadAssetBundleAsync_mE25419B8260B8C411CED0EDF65F03336E5A25DBD (void);
// 0x00000861 System.Void Mediapipe.AssetBundleManager::PrepareAsset(System.String,System.String,System.Boolean)
extern void AssetBundleManager_PrepareAsset_m864B915C4A1B897ADC3523B0F9C7672FFDAEB7BD (void);
// 0x00000862 System.Threading.Tasks.Task Mediapipe.AssetBundleManager::PrepareAssetAsync(System.String,System.String,System.Boolean)
extern void AssetBundleManager_PrepareAssetAsync_m0CED483ECF346567C5E1328E28E459262DD0131D (void);
// 0x00000863 System.String Mediapipe.AssetBundleManager::PathToResourceAsFile(System.String)
extern void AssetBundleManager_PathToResourceAsFile_mE4EA5782FC953FF16031FD0EB659BD758E0EDC7E (void);
// 0x00000864 System.Boolean Mediapipe.AssetBundleManager::GetResourceContents(System.String,System.IntPtr)
extern void AssetBundleManager_GetResourceContents_mDFE89208C34422AEE3F365704F969ADFA843BA84 (void);
// 0x00000865 System.String Mediapipe.AssetBundleManager::GetCacheFilePathFor(System.String)
extern void AssetBundleManager_GetCacheFilePathFor_m90FD4FC61CFBF384DF90FEA3247773869ABF3AB2 (void);
// 0x00000866 System.Void Mediapipe.AssetBundleManager::WriteCacheFile(UnityEngine.TextAsset,System.String,System.Boolean)
extern void AssetBundleManager_WriteCacheFile_m54C06D18FBFD36C6ABC3589C5D31145972FFE996 (void);
// 0x00000867 System.Threading.Tasks.Task Mediapipe.AssetBundleManager::WriteCacheFileAsync(UnityEngine.TextAsset,System.String,System.Boolean)
extern void AssetBundleManager_WriteCacheFileAsync_m5F88F8554EB84D0743BA2BD9B7D0E75A26F0E316 (void);
// 0x00000868 System.Void Mediapipe.AssetBundleManager::.cctor()
extern void AssetBundleManager__cctor_m904B43C51174C89D272E9A293857B27D8FC8F416 (void);
// 0x00000869 System.Void Mediapipe.AssetBundleManager/<LoadAssetBundleAsync>d__21::MoveNext()
extern void U3CLoadAssetBundleAsyncU3Ed__21_MoveNext_mD431D8ABB551632415D2A78E9E170537E6469379 (void);
// 0x0000086A System.Void Mediapipe.AssetBundleManager/<LoadAssetBundleAsync>d__21::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CLoadAssetBundleAsyncU3Ed__21_SetStateMachine_mB1E3AA674C432E165404B70B653A6052F97B5070 (void);
// 0x0000086B System.Void Mediapipe.AssetBundleManager/<PrepareAssetAsync>d__23::MoveNext()
extern void U3CPrepareAssetAsyncU3Ed__23_MoveNext_m2CEF64D04C86A028B3B09E52518AC86BF7CDA47F (void);
// 0x0000086C System.Void Mediapipe.AssetBundleManager/<PrepareAssetAsync>d__23::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CPrepareAssetAsyncU3Ed__23_SetStateMachine_m290CC6603F5D2FC97EC17298E510722817A5A414 (void);
// 0x0000086D System.Void Mediapipe.AssetBundleManager/<WriteCacheFileAsync>d__28::MoveNext()
extern void U3CWriteCacheFileAsyncU3Ed__28_MoveNext_mFB1FC8D21EDC7484C932074DCB1F0C9EE72581BF (void);
// 0x0000086E System.Void Mediapipe.AssetBundleManager/<WriteCacheFileAsync>d__28::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CWriteCacheFileAsyncU3Ed__28_SetStateMachine_m4078D3327DCD2BBFBF231DE1709C9CD3B7EAD5C4 (void);
// 0x0000086F System.Void Mediapipe.AsyncOperationAwaiter`1::.ctor(T)
// 0x00000870 System.Boolean Mediapipe.AsyncOperationAwaiter`1::get_IsCompleted()
// 0x00000871 T Mediapipe.AsyncOperationAwaiter`1::GetResult()
// 0x00000872 System.Void Mediapipe.AsyncOperationAwaiter`1::OnCompleted(System.Action)
// 0x00000873 System.Void Mediapipe.AsyncOperationAwaiter`1/<>c__DisplayClass5_0::.ctor()
// 0x00000874 System.Void Mediapipe.AsyncOperationAwaiter`1/<>c__DisplayClass5_0::<OnCompleted>b__0(UnityEngine.AsyncOperation)
// 0x00000875 Mediapipe.AsyncOperationAwaiter`1<T> Mediapipe.AsyncOperationExtension::GetAwaiter(T)
// 0x00000876 Mediapipe.ResourceManager/PathResolver Mediapipe.LocalAssetManager::get_pathResolver()
extern void LocalAssetManager_get_pathResolver_mC7A793AD5B6F288F15B8ED05A3520F41512BD8DA (void);
// 0x00000877 Mediapipe.ResourceManager/ResourceProvider Mediapipe.LocalAssetManager::get_resourceProvider()
extern void LocalAssetManager_get_resourceProvider_mF559E1B02CA6310C6F48DC6990562E8B6E05E007 (void);
// 0x00000878 System.Boolean Mediapipe.LocalAssetManager::IsPrepared(System.String)
extern void LocalAssetManager_IsPrepared_m56ECCDA53BEEC7AB396D637AE5CD897C2C78864E (void);
// 0x00000879 System.String Mediapipe.LocalAssetManager::get_RootPath()
extern void LocalAssetManager_get_RootPath_m46618459F21AD293211F44DFE7243D50F3ED8C60 (void);
// 0x0000087A System.Void Mediapipe.LocalAssetManager::PrepareAsset(System.String,System.String,System.Boolean)
extern void LocalAssetManager_PrepareAsset_m28813A11DAF363D54F7B61F87B7615B05D22D202 (void);
// 0x0000087B System.Threading.Tasks.Task Mediapipe.LocalAssetManager::PrepareAssetAsync(System.String,System.String,System.Boolean)
extern void LocalAssetManager_PrepareAssetAsync_mB0FE9494A0DA547BA1823704C614DADD52EB629F (void);
// 0x0000087C System.String Mediapipe.LocalAssetManager::PathToResourceAsFile(System.String)
extern void LocalAssetManager_PathToResourceAsFile_mD93EA6538E2E77CCD4ACB981201294DFFDF0C3A6 (void);
// 0x0000087D System.Boolean Mediapipe.LocalAssetManager::GetResourceContents(System.String,System.IntPtr)
extern void LocalAssetManager_GetResourceContents_m36D0D7964026D770978C9BECA4F18D510A46648A (void);
// 0x0000087E System.String Mediapipe.LocalAssetManager::GetCacheFilePathFor(System.String)
extern void LocalAssetManager_GetCacheFilePathFor_mB212F0779C0802E7594F39621194BDD1BF9FA8EC (void);
// 0x0000087F System.Void Mediapipe.LocalAssetManager::.ctor()
extern void LocalAssetManager__ctor_m7E6B3A0E203D21C624332321ED18C8A1289C35EC (void);
// 0x00000880 System.Void Mediapipe.LocalAssetManager::.cctor()
extern void LocalAssetManager__cctor_m598D766545234C7DF39B3FBA816A1F24B483BED8 (void);
// 0x00000881 System.Void Mediapipe.LocalAssetManager/<PrepareAssetAsync>d__9::MoveNext()
extern void U3CPrepareAssetAsyncU3Ed__9_MoveNext_m0EF27ED0CD2CC24F50203DAE10881054342915B0 (void);
// 0x00000882 System.Void Mediapipe.LocalAssetManager/<PrepareAssetAsync>d__9::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CPrepareAssetAsyncU3Ed__9_SetStateMachine_mC7A20B6945A994F96677055B1D8DE081F0F89849 (void);
// 0x00000883 Mediapipe.ResourceManager/PathResolver Mediapipe.ResourceManager::get_pathResolver()
// 0x00000884 Mediapipe.ResourceManager/ResourceProvider Mediapipe.ResourceManager::get_resourceProvider()
// 0x00000885 System.Void Mediapipe.ResourceManager::.ctor()
extern void ResourceManager__ctor_m34A7AB9E318E70364155F06B3D37F2203132DA60 (void);
// 0x00000886 System.Boolean Mediapipe.ResourceManager::IsPrepared(System.String)
// 0x00000887 System.Void Mediapipe.ResourceManager::PrepareAsset(System.String,System.String,System.Boolean)
// 0x00000888 System.Void Mediapipe.ResourceManager::PrepareAsset(System.String,System.Boolean)
extern void ResourceManager_PrepareAsset_mF4FF9B09507DB5DB5D30427A2DC96ADA79E0902E (void);
// 0x00000889 System.Threading.Tasks.Task Mediapipe.ResourceManager::PrepareAssetAsync(System.String,System.String,System.Boolean)
// 0x0000088A System.Threading.Tasks.Task Mediapipe.ResourceManager::PrepareAssetAsync(System.String,System.Boolean)
extern void ResourceManager_PrepareAssetAsync_m2DDB5F5EFD5BA1BE652D35340DD628941FBB0E9E (void);
// 0x0000088B System.String Mediapipe.ResourceManager::GetAssetNameFromPath(System.String)
extern void ResourceManager_GetAssetNameFromPath_m2C7FE8490B5337FA3322C7E6AA1AF22385F48F84 (void);
// 0x0000088C System.Void Mediapipe.ResourceManager/PathResolver::.ctor(System.Object,System.IntPtr)
extern void PathResolver__ctor_m2CBCAC18A130A9ACDE6A589DECD3FF8433CB8D80 (void);
// 0x0000088D System.String Mediapipe.ResourceManager/PathResolver::Invoke(System.String)
extern void PathResolver_Invoke_m7268AF680F591B483C2F32B54BCF6C11A4FBB607 (void);
// 0x0000088E System.IAsyncResult Mediapipe.ResourceManager/PathResolver::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern void PathResolver_BeginInvoke_m0DA05233E279C32CB1B8CF5F4C4B7D71C21F977E (void);
// 0x0000088F System.String Mediapipe.ResourceManager/PathResolver::EndInvoke(System.IAsyncResult)
extern void PathResolver_EndInvoke_m9264EA6918879B0F5CADBF4236289C02B70523F6 (void);
// 0x00000890 System.Void Mediapipe.ResourceManager/ResourceProvider::.ctor(System.Object,System.IntPtr)
extern void ResourceProvider__ctor_m31D3EDD7BDF5AD400A08B8BFC98CF553BAACFEBD (void);
// 0x00000891 System.Boolean Mediapipe.ResourceManager/ResourceProvider::Invoke(System.String,System.IntPtr)
extern void ResourceProvider_Invoke_mF5795AEB7F87C37BE3BC99516515191AA3FE31C6 (void);
// 0x00000892 System.IAsyncResult Mediapipe.ResourceManager/ResourceProvider::BeginInvoke(System.String,System.IntPtr,System.AsyncCallback,System.Object)
extern void ResourceProvider_BeginInvoke_m319DDE3FA962F705DCE00DF8CA2E14D0603541E8 (void);
// 0x00000893 System.Boolean Mediapipe.ResourceManager/ResourceProvider::EndInvoke(System.IAsyncResult)
extern void ResourceProvider_EndInvoke_m60F666704EC5F92E1808247A966C8E22C9F52162 (void);
// 0x00000894 Google.Protobuf.Reflection.FileDescriptor Mediapipe.FaceGeometry.FaceGeometryReflection::get_Descriptor()
extern void FaceGeometryReflection_get_Descriptor_mEF894D394F60CAA7DA98B4BF777BA84109579AFB (void);
// 0x00000895 System.Void Mediapipe.FaceGeometry.FaceGeometryReflection::.cctor()
extern void FaceGeometryReflection__cctor_m24B3C4844194978F63FAB30AB7CC54555541AF52 (void);
// 0x00000896 Google.Protobuf.MessageParser`1<Mediapipe.FaceGeometry.FaceGeometry> Mediapipe.FaceGeometry.FaceGeometry::get_Parser()
extern void FaceGeometry_get_Parser_m84F9AF8D024653DBBA02B0F72E23FB8A52E6F4E0 (void);
// 0x00000897 Google.Protobuf.Reflection.MessageDescriptor Mediapipe.FaceGeometry.FaceGeometry::get_Descriptor()
extern void FaceGeometry_get_Descriptor_mC186813A0E14794CC712F7C993A3EA7D81790654 (void);
// 0x00000898 Google.Protobuf.Reflection.MessageDescriptor Mediapipe.FaceGeometry.FaceGeometry::pb::Google.Protobuf.IMessage.get_Descriptor()
extern void FaceGeometry_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m36EA1A8AE55FE125B566F37B54D7430005916494 (void);
// 0x00000899 System.Void Mediapipe.FaceGeometry.FaceGeometry::.ctor()
extern void FaceGeometry__ctor_m3C8727C2045277EA8A051205D4FC9A5CE3D5E89A (void);
// 0x0000089A System.Void Mediapipe.FaceGeometry.FaceGeometry::.ctor(Mediapipe.FaceGeometry.FaceGeometry)
extern void FaceGeometry__ctor_m2C60845DDA8A847E19706B9BD1CCF8064A1C5124 (void);
// 0x0000089B Mediapipe.FaceGeometry.FaceGeometry Mediapipe.FaceGeometry.FaceGeometry::Clone()
extern void FaceGeometry_Clone_m8489E2E228E407688DA6FEDFFC665B20256CC9BE (void);
// 0x0000089C Mediapipe.FaceGeometry.Mesh3d Mediapipe.FaceGeometry.FaceGeometry::get_Mesh()
extern void FaceGeometry_get_Mesh_mDD024BFCD1BC8CA25DC919C700E4320E1ED8F4FE (void);
// 0x0000089D System.Void Mediapipe.FaceGeometry.FaceGeometry::set_Mesh(Mediapipe.FaceGeometry.Mesh3d)
extern void FaceGeometry_set_Mesh_m93B518A98300BF6B789FB23D370D151647AFE844 (void);
// 0x0000089E System.Boolean Mediapipe.FaceGeometry.FaceGeometry::get_HasMesh()
extern void FaceGeometry_get_HasMesh_m568BC0CD80EB1AABC3578A5CE5EE2B065324E2D0 (void);
// 0x0000089F System.Void Mediapipe.FaceGeometry.FaceGeometry::ClearMesh()
extern void FaceGeometry_ClearMesh_m7CD19E757E9FD9D40EEE8C790A17A34F9EB32C86 (void);
// 0x000008A0 Mediapipe.MatrixData Mediapipe.FaceGeometry.FaceGeometry::get_PoseTransformMatrix()
extern void FaceGeometry_get_PoseTransformMatrix_mD90833ABA53C6B4ED3DA6E4949FC21613446008A (void);
// 0x000008A1 System.Void Mediapipe.FaceGeometry.FaceGeometry::set_PoseTransformMatrix(Mediapipe.MatrixData)
extern void FaceGeometry_set_PoseTransformMatrix_m513DD035B20C4296E3D68FDAC90EAA2CE18BCEC7 (void);
// 0x000008A2 System.Boolean Mediapipe.FaceGeometry.FaceGeometry::get_HasPoseTransformMatrix()
extern void FaceGeometry_get_HasPoseTransformMatrix_m560FE1CBC44455CB770549D97071BCE38B0021BD (void);
// 0x000008A3 System.Void Mediapipe.FaceGeometry.FaceGeometry::ClearPoseTransformMatrix()
extern void FaceGeometry_ClearPoseTransformMatrix_m5E342DF34AA1B1C802830EE2BEB6C53AD40BAD4F (void);
// 0x000008A4 System.Boolean Mediapipe.FaceGeometry.FaceGeometry::Equals(System.Object)
extern void FaceGeometry_Equals_m24D19204C5A3E02D27CB6F1EF569A1D60B36AD7F (void);
// 0x000008A5 System.Boolean Mediapipe.FaceGeometry.FaceGeometry::Equals(Mediapipe.FaceGeometry.FaceGeometry)
extern void FaceGeometry_Equals_m4AB38A52827525069D398725064F885308DF8426 (void);
// 0x000008A6 System.Int32 Mediapipe.FaceGeometry.FaceGeometry::GetHashCode()
extern void FaceGeometry_GetHashCode_m9A30D9E1C207E32C0035016651B8292E1AC0978D (void);
// 0x000008A7 System.String Mediapipe.FaceGeometry.FaceGeometry::ToString()
extern void FaceGeometry_ToString_m8A03508AD29218B0490EEC9FCC3090B9FA68C618 (void);
// 0x000008A8 System.Void Mediapipe.FaceGeometry.FaceGeometry::WriteTo(Google.Protobuf.CodedOutputStream)
extern void FaceGeometry_WriteTo_mA3D5E866007FE45435FD2F0C241F5BFF9000DA46 (void);
// 0x000008A9 System.Int32 Mediapipe.FaceGeometry.FaceGeometry::CalculateSize()
extern void FaceGeometry_CalculateSize_m8244CFFD3C4A8DA2ABB011819E670C9B6002EC6D (void);
// 0x000008AA System.Void Mediapipe.FaceGeometry.FaceGeometry::MergeFrom(Mediapipe.FaceGeometry.FaceGeometry)
extern void FaceGeometry_MergeFrom_mE6D2821557D75E5A220BBAD1AF0533C69708A99B (void);
// 0x000008AB System.Void Mediapipe.FaceGeometry.FaceGeometry::MergeFrom(Google.Protobuf.CodedInputStream)
extern void FaceGeometry_MergeFrom_m635F8DC14C15F10F7ADD712FBE7899E7DF8B918C (void);
// 0x000008AC System.Void Mediapipe.FaceGeometry.FaceGeometry::.cctor()
extern void FaceGeometry__cctor_m80298DB8E5F50F7A58970C3DBA002848BF4569E6 (void);
// 0x000008AD System.Void Mediapipe.FaceGeometry.FaceGeometry/<>c::.cctor()
extern void U3CU3Ec__cctor_mD7684E999F848E5E818C330CB66CC6C5B38E751E (void);
// 0x000008AE System.Void Mediapipe.FaceGeometry.FaceGeometry/<>c::.ctor()
extern void U3CU3Ec__ctor_m5B0F45FF93FB9AE889C4654C54DAFDBC93D9B5A1 (void);
// 0x000008AF Mediapipe.FaceGeometry.FaceGeometry Mediapipe.FaceGeometry.FaceGeometry/<>c::<.cctor>b__36_0()
extern void U3CU3Ec_U3C_cctorU3Eb__36_0_m9403D33D924F4DF1BEFAE14D42CDBC533BA26B60 (void);
// 0x000008B0 Google.Protobuf.Reflection.FileDescriptor Mediapipe.FaceGeometry.Mesh3DReflection::get_Descriptor()
extern void Mesh3DReflection_get_Descriptor_m3064E3A93064960806ED6EF889AE43C9498B47A8 (void);
// 0x000008B1 System.Void Mediapipe.FaceGeometry.Mesh3DReflection::.cctor()
extern void Mesh3DReflection__cctor_mD223615FABAE9A99ECD65CC7CBE49E822F230CC8 (void);
// 0x000008B2 Google.Protobuf.MessageParser`1<Mediapipe.FaceGeometry.Mesh3d> Mediapipe.FaceGeometry.Mesh3d::get_Parser()
extern void Mesh3d_get_Parser_m24646561AE2461D22BFFB8FDDEED3CA3B9A1A36D (void);
// 0x000008B3 Google.Protobuf.Reflection.MessageDescriptor Mediapipe.FaceGeometry.Mesh3d::get_Descriptor()
extern void Mesh3d_get_Descriptor_m1D093CEFDCECDC44BA24719A0B300B9D7947E15A (void);
// 0x000008B4 Google.Protobuf.Reflection.MessageDescriptor Mediapipe.FaceGeometry.Mesh3d::pb::Google.Protobuf.IMessage.get_Descriptor()
extern void Mesh3d_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_mBBF9A0FED0C08E972BFFEFA023D47624EEC83327 (void);
// 0x000008B5 System.Void Mediapipe.FaceGeometry.Mesh3d::.ctor()
extern void Mesh3d__ctor_m9F9250640B8B59C63500CA172405221839AC8659 (void);
// 0x000008B6 System.Void Mediapipe.FaceGeometry.Mesh3d::.ctor(Mediapipe.FaceGeometry.Mesh3d)
extern void Mesh3d__ctor_m3DA3D0DF3B76850845F5492101B043CDFA74891B (void);
// 0x000008B7 Mediapipe.FaceGeometry.Mesh3d Mediapipe.FaceGeometry.Mesh3d::Clone()
extern void Mesh3d_Clone_m1480A74D40F2C4A9F767C9BADE298FA2C929E2FE (void);
// 0x000008B8 Mediapipe.FaceGeometry.Mesh3d/Types/VertexType Mediapipe.FaceGeometry.Mesh3d::get_VertexType()
extern void Mesh3d_get_VertexType_m3FDB29436DA076950A70926DC5E35B66FE3AB38D (void);
// 0x000008B9 System.Void Mediapipe.FaceGeometry.Mesh3d::set_VertexType(Mediapipe.FaceGeometry.Mesh3d/Types/VertexType)
extern void Mesh3d_set_VertexType_m5E86A37793BE879843800CDFBA072E549094C042 (void);
// 0x000008BA System.Boolean Mediapipe.FaceGeometry.Mesh3d::get_HasVertexType()
extern void Mesh3d_get_HasVertexType_m2FC415F35052C6FBEAF491A8B24C94D501386D92 (void);
// 0x000008BB System.Void Mediapipe.FaceGeometry.Mesh3d::ClearVertexType()
extern void Mesh3d_ClearVertexType_m0636B3AF901073C2526A37C1F2A09715A3AF9EDE (void);
// 0x000008BC Mediapipe.FaceGeometry.Mesh3d/Types/PrimitiveType Mediapipe.FaceGeometry.Mesh3d::get_PrimitiveType()
extern void Mesh3d_get_PrimitiveType_mB2F31009B4E5D7EA7FC125C5DEB58D58A4C7949D (void);
// 0x000008BD System.Void Mediapipe.FaceGeometry.Mesh3d::set_PrimitiveType(Mediapipe.FaceGeometry.Mesh3d/Types/PrimitiveType)
extern void Mesh3d_set_PrimitiveType_m9C7F073EB49745074A4FD40110DC8FB3E1014407 (void);
// 0x000008BE System.Boolean Mediapipe.FaceGeometry.Mesh3d::get_HasPrimitiveType()
extern void Mesh3d_get_HasPrimitiveType_m9DF161B713DA4A600B333477279B51FB178438B4 (void);
// 0x000008BF System.Void Mediapipe.FaceGeometry.Mesh3d::ClearPrimitiveType()
extern void Mesh3d_ClearPrimitiveType_mDA655E945A9E7C5224BC7B466BEB4DC2C0237280 (void);
// 0x000008C0 Google.Protobuf.Collections.RepeatedField`1<System.Single> Mediapipe.FaceGeometry.Mesh3d::get_VertexBuffer()
extern void Mesh3d_get_VertexBuffer_m61C7625831070AC8D548C717D61B5160C718B6ED (void);
// 0x000008C1 Google.Protobuf.Collections.RepeatedField`1<System.UInt32> Mediapipe.FaceGeometry.Mesh3d::get_IndexBuffer()
extern void Mesh3d_get_IndexBuffer_mFCDEEC12BF5491CBED10689092AB07BACE7DF791 (void);
// 0x000008C2 System.Boolean Mediapipe.FaceGeometry.Mesh3d::Equals(System.Object)
extern void Mesh3d_Equals_m8608984036EA7B53D8E397986337302F7A26916D (void);
// 0x000008C3 System.Boolean Mediapipe.FaceGeometry.Mesh3d::Equals(Mediapipe.FaceGeometry.Mesh3d)
extern void Mesh3d_Equals_m1A13E2324B6A21935A44DF3AF37B6F090083DFC2 (void);
// 0x000008C4 System.Int32 Mediapipe.FaceGeometry.Mesh3d::GetHashCode()
extern void Mesh3d_GetHashCode_m385ABBC9D72FB51065C89DB3616E642F47066A0A (void);
// 0x000008C5 System.String Mediapipe.FaceGeometry.Mesh3d::ToString()
extern void Mesh3d_ToString_m2628330180A999E8A61BC16A9DC80C69E4B9C03C (void);
// 0x000008C6 System.Void Mediapipe.FaceGeometry.Mesh3d::WriteTo(Google.Protobuf.CodedOutputStream)
extern void Mesh3d_WriteTo_m5CBA34FA8D18DFAB07AC9AC358FEFB2626FDE216 (void);
// 0x000008C7 System.Int32 Mediapipe.FaceGeometry.Mesh3d::CalculateSize()
extern void Mesh3d_CalculateSize_m1AC813F566F4AAE361BA291AC9FEB73457651D91 (void);
// 0x000008C8 System.Void Mediapipe.FaceGeometry.Mesh3d::MergeFrom(Mediapipe.FaceGeometry.Mesh3d)
extern void Mesh3d_MergeFrom_mA837C24529468595959D161C916756CA53E4BFDD (void);
// 0x000008C9 System.Void Mediapipe.FaceGeometry.Mesh3d::MergeFrom(Google.Protobuf.CodedInputStream)
extern void Mesh3d_MergeFrom_mDBAB758798822F0692FBBADAFDABE64BE546153E (void);
// 0x000008CA System.Void Mediapipe.FaceGeometry.Mesh3d::.cctor()
extern void Mesh3d__cctor_m7CDC3E0A8B20D3F98FE8464F198F91CEF01C4651 (void);
// 0x000008CB System.Void Mediapipe.FaceGeometry.Mesh3d/<>c::.cctor()
extern void U3CU3Ec__cctor_m5C16FD7585F3041FA5D626C2F5134B7314C92079 (void);
// 0x000008CC System.Void Mediapipe.FaceGeometry.Mesh3d/<>c::.ctor()
extern void U3CU3Ec__ctor_m4B37574669A9C61778546DAB7A78D91CCEA85945 (void);
// 0x000008CD Mediapipe.FaceGeometry.Mesh3d Mediapipe.FaceGeometry.Mesh3d/<>c::<.cctor>b__50_0()
extern void U3CU3Ec_U3C_cctorU3Eb__50_0_m1097B4EF560DD0F3EC417196F17DE6CC204A6F02 (void);
static Il2CppMethodPointer s_methodPointers[2253] = 
{
	EmbeddedAttribute__ctor_mFD149DC63111CEA4726998614A96442713AD600F,
	IsUnmanagedAttribute__ctor_m45D6F01B7AD5A98E821F1DA8A4E6B7281B8E8C79,
	NULL,
	AnnotationController_ScaleVector_mA6A1B303D73EE79175C490D1101B000AA224B62F,
	AnnotationController_GetPositionFromNormalizedPoint_mA8FE8268BAB0015082254E4C206DC6CEB877EEDC,
	AnnotationController_GetPosition_m22613159F002193250F9829E14F25AC93FDD0565,
	AnnotationController_GetPosition_mB946751E773391FD51AEE00303BB14A2C97C9B8E,
	AnnotationController_GetPositionsFromNormalizedRect_m1880507A736BE35F694432C774FCE21007A68FB6,
	AnnotationController_GetPositions_mA87C2E5977845B30D8BD7D92F6EB08AE18554E88,
	AnnotationController_GetPositions_m002727B3E76D92E2D884A9C7408CF6F5B3A4ED9E,
	AnnotationController_GetDistance_mB3B606B0082950C0229985887B4065AC14E214A1,
	AnnotationController__ctor_m91527FA9EB5B41FA7474B9BC2906D391EDCA87AD,
	CircleAnnotationController_Awake_mCDCA10265B7B08F36EC318E0C200EBDAE40ED1A3,
	CircleAnnotationController_Clear_m874E57F539654541E673F490F630BB344280FBA8,
	CircleAnnotationController_Draw_mD6FD8CC9F1B1134FE28A74118462508A7637D8E3,
	CircleAnnotationController__ctor_m5E7F8FDC9F4586B94447E81F4E5F09DD44C7EC3D,
	ClassificationAnnotationController_Clear_m6A787687B93BF33ECDF9CDE14516F127F6B05BB1,
	ClassificationAnnotationController_Draw_m39C7805E97E187BCA7DB77F591F370B6AF7F9E1B,
	ClassificationAnnotationController__ctor_m199E7565128A0C23B5250703E0B3C638895F00D5,
	DetectionAnnotationController_Awake_mD5BD0D8D23BE5190BDA473C6E9F414C7B15F8074,
	DetectionAnnotationController_OnDestroy_mECC90E8F61C02E69302EC82F66CF710E8695D520,
	DetectionAnnotationController_Clear_m9917051AE6144BE8A36EA25E5CBCB57EC0082A48,
	DetectionAnnotationController_Draw_mC635729B32BFFE0573AC09E0F568F240BDB5B918,
	DetectionAnnotationController_DrawRectAndLabel_m335B34E8F5111E5CBCCCDC12C46A6F4936E5DDC2,
	DetectionAnnotationController_DrawRelativeKeypoints_m8C014F231F5A4DCBA767F8819F6EB9DAA66DE554,
	DetectionAnnotationController__ctor_m70E884B362DA793AB008F7A44CE3E597EC828764,
	DetectionListAnnotationController_Draw_m231C288C83D30FD15FD1C87C17AFD9E58033C26F,
	DetectionListAnnotationController__ctor_m38E68DF023D6D7144EADBFFA4ED38147A3AB6C60,
	EdgeAnnotationController_Clear_m35C257ED7D47DF6E266FBD472774E0F26B0ABE4D,
	EdgeAnnotationController_Draw_mDCCF112E0A995942C044D1B8D17D5AD439A6F080,
	EdgeAnnotationController_Draw_mE4FFDF92737C3CA440C302D4F10A95111B820C41,
	EdgeAnnotationController_Draw_m572DF445739E37D19FF04546A008D71F69F4B737,
	EdgeAnnotationController__ctor_mB893C7C2E5FC5BF68C89ABB3323304148A23C0BF,
	EdgeAnnotationController__cctor_mE76D0BC832C0F3C2A0572399BFB104A983F67973,
	FaceLandmarkListAnnotationController_get_Connections_m15A7FEAC348237A285AA44D935DCE9CF5BC86F50,
	FaceLandmarkListAnnotationController_get_NodeSize_m9E30F5E2E0844E89FD3D9A028B8B8DF44507CFCE,
	FaceLandmarkListAnnotationController__ctor_mC199284B683342CCE4F3057699736E57F6B251CE,
	FaceLandmarkListAnnotationController__cctor_mFFF8D3A9B2940E9A693A40E9B83E53B8A1F7EA75,
	FullBodyPoseLandmarkListAnnotationController_get_Connections_m54A6734258122B53B38819FA9E1FAA196B1E3ADD,
	FullBodyPoseLandmarkListAnnotationController_get_NodeSize_m32FF5D69EE61A449C9C4EE0275523D71D276A19C,
	FullBodyPoseLandmarkListAnnotationController__ctor_m64E26DF784F76F5BAD8834655A470A0F6A463B90,
	FullBodyPoseLandmarkListAnnotationController__cctor_mFA255E10AD607DB63486E637E9FE08B3A70D4735,
	HandLandmarkListAnnotationController_get_Connections_mF282E0CA0D45FEE4BF7FA08750824207B9351F37,
	HandLandmarkListAnnotationController_get_NodeSize_m77725524772C22CD775660855100A13906F1AE0D,
	HandLandmarkListAnnotationController__ctor_m1CD1741D3944A10B233676042DA460286A2D8C63,
	HandLandmarkListAnnotationController__cctor_m7992BD4835558328C92E46F00715D49CFE5E09A0,
	LandmarkListAnnotationController_Awake_m26810925771B118CF1FC849EDF5BE47BAF041866,
	LandmarkListAnnotationController_OnDestroy_mC0B69B9D32E4F94378B050A392F099D72414CA72,
	LandmarkListAnnotationController_Clear_m4102D6A3768C843BE73D619F305A471DE1D18D21,
	LandmarkListAnnotationController_Draw_mCA206AD88A5552F61C67D0DD7CB65718AEFF31E9,
	NULL,
	NULL,
	LandmarkListAnnotationController_get_EdgeSize_m5458F9000E0C6912812E34CD4EA14859BC3949C8,
	LandmarkListAnnotationController_isEmpty_mDFF2CF713A34ED30E756AD573576BD183B21C7C2,
	LandmarkListAnnotationController__ctor_m278A21D9E55C456BD6E81F0AF9649C446CD7B012,
	U3CU3Ec__cctor_mE58891B03C85CFB11891A9E26D0E6BF20935C5C0,
	U3CU3Ec__ctor_mFF3B99818696F77BB36E5CE0E1632EA54B9798D4,
	U3CU3Ec_U3CisEmptyU3Eb__15_0_mEC716E744D88196469528450F57828EF688C33E8,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	MaskAnnotationController_Clear_m0EC8E8F3CFB3CEFCC10FEB8A14038254EC5F5A3C,
	MaskAnnotationController_Draw_m531F4070BF7AA113B8AAC414FCCA0AC556FE08A5,
	MaskAnnotationController_SetMask_m95191E76F668E0855000110D97BA0348DD01FA64,
	MaskAnnotationController_GetNearestRange_m7BF3A8665F1D4D756C866D940ABD8659A48E7D14,
	MaskAnnotationController__ctor_m425C5578FB544FEF95D1C604CFBE529E39B139B0,
	NodeAnnotationController_Clear_m09268CFAD7F9AEEA39B290E176D384EC1B7C09F0,
	NodeAnnotationController_Draw_mB5824A0C834CEBBF000D9C9B44EA5E59C1E38C5A,
	NodeAnnotationController_Draw_m2CFD5978D6F6A64AAC99AF8ADDEB36AE12B38E40,
	NodeAnnotationController__ctor_mA3FACFE1BEFE9173D43FAC39F1E1B21B25B82768,
	PoseLandmarkListAnnotationController_get_Connections_m4632729106C8CD1307DD9445E86C0C93F4AD27E9,
	PoseLandmarkListAnnotationController_get_NodeSize_m5ACFCB49553DDC8C55B0D7912F401E89EDCF348A,
	PoseLandmarkListAnnotationController__ctor_m9621D67A36A00C9583B93F7CAC64033318E8E2D7,
	PoseLandmarkListAnnotationController__cctor_mCFE6BDC12124548334EEA7056E8E957F3DCC0725,
	RectAnnotationController_Clear_mCAAE3F48534569E674C8872069DD036628E045AC,
	RectAnnotationController_Draw_m94915D81C3FB8600B4B962801BC28C41B2826C28,
	RectAnnotationController__ctor_m07B8A633B5379A1BB3CDE0059F03894A41FBF758,
	RectListAnnotationController_Draw_mD5379AC3DFF9997053512DBEF8F1D019E3BF8E07,
	RectListAnnotationController__ctor_mEF32CC85C836EF6601E10488CF00C48C60686A1A,
	DisposableObject_get_isDisposed_mBAC3411868F2F3866C93C0FD29569C5C6A589E4E,
	DisposableObject_set_isDisposed_m4810231F19F321DDBB7ACEB59D310DA3AF8689FA,
	DisposableObject_get_isOwner_m05D0D142AD0928EFB4EBF5C9BE83A8C4D003B7C5,
	DisposableObject_set_isOwner_m14F37B0D67E76791FAD35719A01F7924A796DE42,
	DisposableObject__ctor_m3D3FF015DD820E8D044BE2029F67E8769AB3A908,
	DisposableObject__ctor_mCC479EC8FD88BF3ADC5D5238D2FD08B7EB77EF28,
	DisposableObject_Dispose_m6A068C5925EC39A121CCB9881C074EF27C77A84B,
	DisposableObject_Dispose_m729100105D4D3C69B47D47B52FDB6078E832DC1E,
	DisposableObject_Finalize_m61E5D0FAA3769F9C54F2AF70D87ECA0BC1424F1C,
	DisposableObject_DisposeManaged_mF57755111B4ADFD17F40662B7F5464EB746D1232,
	DisposableObject_DisposeUnmanaged_m8DF53D7CF53D62539AE5180B87D9C834382E4590,
	DisposableObject_TransferOwnership_mF63D4787258D48201930A2CF3BEE7A47430A2EFC,
	DisposableObject_ThrowIfDisposed_m44AC87144CFB5BCB096E952AF55DC17B1917E26F,
	NULL,
	NULL,
	NULL,
	NULL,
	InternalException__ctor_mA799316BDDBC4BEBABD6DDF7EC64148566AA7203,
	MediaPipeException__ctor_m38FF115395F8A5B1926A12CADF964D3FFA4B6462,
	MediaPipePluginException__ctor_m9A533AFB6CAC664271755038902A1B978ECD5D44,
	MpResourceHandle__ctor_m84EC3037371F2F7B1DFC32D8D9370BAC6821705A,
	MpResourceHandle__ctor_m86544707DB7A9AA7351DB6D2A2491B6BD97385A0,
	MpResourceHandle_get_mpPtr_mA93C5D418EFA29D6C0FB55B776C4F4FA1416498C,
	MpResourceHandle_ReleaseMpResource_m1374ACB47C279F5BA463FD3FC8306A9E0E9E78E7,
	MpResourceHandle_OwnsResource_m5CA80083503C7210737E3FD843AF0324BD6AEEA1,
	MpResourceHandle_DisposeUnmanaged_mE90C9069D41976D29D6022E25A9814E3309DDB1A,
	MpResourceHandle_ReleaseMpPtr_mAF6A25D3E070825A6DB44923275C01C45C8CEE5B,
	NULL,
	MpResourceHandle_MarshalStringFromNative_m9BF8B0C0534BCDC70741E1AD47C8639B527616AF,
	StringOutFunc__ctor_mA1CDFE1C87059E6CCA17F75ACFECBF6B05B4693E,
	StringOutFunc_Invoke_mC2B207EB6C49C01D5589BE4A28CB108639B3C606,
	StringOutFunc_BeginInvoke_m6C5BE29024026EC369CBDF2951F49A766E686299,
	StringOutFunc_EndInvoke_mA2FFD153F9910DB743310669FA6A07215304C498,
	SharedPtrHandle__ctor_mA38B222513827FF11119BB27C36C05573D2495D1,
	NULL,
	NULL,
	UniquePtrHandle__ctor_m422414A0665FCE76EE6CC557727CD1A9523B15EF,
	NULL,
	NULL,
	Glog_Initialize_mEA148EF01B31968CE8A1B2B4B3C05887A8E13294,
	Glog_Shutdown_m7FEA643AF98D2D93BE2BE591DDCAC0491DDA204F,
	Glog_Log_m7EBF9C4A6CE2922A03DF5784E6B16F89A93584AC,
	Glog_FlushLogFiles_m99F284D2286E6B9365CC355150FE2481032938D1,
	Glog__ctor_m8FBD33C65B740B811C7858EE8D7373A107E49AB3,
	Protobuf__cctor_m8563A723AE3C71192A61E05DD1604B109DF496A0,
	NULL,
	NULL,
	Protobuf_LogProtobufMessage_m1B59C21023D21E2AB5CA458EC71B9F43DA4E587D,
	Protobuf_FormatProtobufLogLevel_mD7C7A738B5069D575A88EB32A862234D6E26CADB,
	Protobuf__ctor_m450C7237AC8ABE0A9B482F1B6C550368AF9CD751,
	ProtobufLogHandler__ctor_m9710EFC55EEE9F88A73A00BC3C8C92FFDD59C17B,
	ProtobufLogHandler_Invoke_mA37425C68AA303DA4CF3B7538C68B0ABE51CA648,
	ProtobufLogHandler_BeginInvoke_mD8C54C91E7F9F0BCDB53884070279A724A0AC53F,
	ProtobufLogHandler_EndInvoke_mB4A85E6157160DC1F0D249E2B7EB52E4E911ACCA,
	StdString__ctor_m05D13A243A1ABD5F541B48B0890087853BF469D9,
	StdString__ctor_mFE5502F4BEEF68B901A1526DFB601D4FBC240C30,
	StdString_DeleteMpPtr_mDEAFCE78D7148065C66167238E51FB5589C6F093,
	StdString_Swap_m3995E0894B6AAD06C25494E2F795A2D9E05C48B3,
	Format_FromPixels32_mBA75BC96B4A07BD34189075F273B236DEC47F7D6,
	Format_FromBytePtr_m241B5F9B3CA63A791E94B49E80FD4EAE6453619A,
	Format_FromSRGBOrSRGBA_m91CFC61B21D1369DC2497631690B0B1BE2A1E761,
	Format__ctor_m23F96F92A6A488E27E4A130F4A7D1D3A8A5668A1,
	CalculatorGraph__ctor_m36D4D86B01B255D9402C869FF30ABB19AE0EDE1C,
	CalculatorGraph__ctor_m22A32EAA02E54FF25DCB234289DBD50A6A8E3FD9,
	CalculatorGraph__ctor_m2CC73C9FE7F8E19141AACF74C322B482C089C530,
	CalculatorGraph__ctor_mC912C0B993206DED797465F65749660EF5BB3E7E,
	CalculatorGraph_DeleteMpPtr_mC89CA3B9EA6119419ACD668538E1DBC1FEA1232B,
	CalculatorGraph_Initialize_m8C661AF74FE4395742FBC329C8F513CDD022FA2F,
	CalculatorGraph_Initialize_m898F1A6588B7861E73B60666C8938551BA66DE8A,
	CalculatorGraph_Config_mC7AE9FF00C912A256893678836B00E6DF9BF06D1,
	CalculatorGraph_ObserveOutputStream_m829748E3A903D01A0F061EF1CB5B06C911E3D285,
	NULL,
	NULL,
	CalculatorGraph_Run_m76946CA8F7B01409B68DA01F0ED26FD26CEFC25C,
	CalculatorGraph_Run_m7DFB3A09CBB32849E1140DC62397EF1D687A9A05,
	CalculatorGraph_StartRun_m03C22F09F5A6C5AB8286ADFAF71A70E942654F20,
	CalculatorGraph_StartRun_mFC0C313F1EE00E6B807B5F9F38E8D83BA8EB0652,
	CalculatorGraph_WaitUntilIdle_m23B76876DD0079A039C5D8884DAF708F02CA0626,
	CalculatorGraph_WaitUntilDone_mB927ED4B8553C4D9D18A78D7F35F99C8EE296A94,
	CalculatorGraph_HasError_m729D4E56E080DDB79B0465E1E3B0BFDDE64077EA,
	NULL,
	CalculatorGraph_SetInputStreamMaxQueueSize_m9CE86A65512DA9341B41DCEAF7BF4DC06ED76853,
	CalculatorGraph_CloseInputStream_m0B1B320656765ACDB380A8CD68A46E5D975646E5,
	CalculatorGraph_CloseAllPacketSources_m3C6F94EFFEC33EED48F154E750C7B7C8DF75627E,
	CalculatorGraph_Cancel_mB2521E5ACF6E453321D059307B614354D923C995,
	CalculatorGraph_GraphInputStreamsClosed_m1DB432A0F05381983D76E04902D30311EC35410B,
	CalculatorGraph_IsNodeThrottled_m101F9F7708F3AFA3FA9D5FC62F52C3930A0CF259,
	CalculatorGraph_UnthrottleSources_m3E9FBFB1917AAAC09EA2BDF85239523DF2F1656C,
	CalculatorGraph_GetGpuResources_m66F59E3EA07F1B06158CEC08E8B008AF3E15BC41,
	CalculatorGraph_SetGpuResources_m642B43908918A1405EA7CC51249E282AC8A5A153,
	NativePacketCallback__ctor_m81ABAA8A6C21D63E15EE423115105AE8D9ADBDA1,
	NativePacketCallback_Invoke_m6F8D1AD548AD6CEEB9673C0AD8B6D12731949760,
	NativePacketCallback_BeginInvoke_mEB623D88F7259AF8A65F0A10C8A4DFABA1870D07,
	NativePacketCallback_EndInvoke_mA0C8A7CED34494EC24029BAE390F7FB145489A97,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	CalculatorGraphConfigExtension_ParseFromTextFormat_mC08DFC40A3661668ABA0670D41E52A2DD78F74D6,
	ImageFormat__ctor_m420FD54FCB00542E8EB1EE191B34605A8DF7C700,
	ImageFrame__ctor_m29D5F204DF5BA3E608B4A89A37601FB2A3B37BA3,
	ImageFrame__ctor_m2010F6431940810F02B702E606A183C2D680ADDC,
	ImageFrame__ctor_m01A45B52CABA11826A12FC34559CEFCBF12337E3,
	ImageFrame__ctor_m93B466136759DF1B0715B77DFF30F203CC854C85,
	ImageFrame__ctor_mC57C2480EABF2293E41108FCA225FA28D61C87DB,
	ImageFrame_DeleteMpPtr_m494FDB3BF93AB7FFE38984E082187397A016A795,
	ImageFrame_ReleasePixelData_mDF81706134475903E835D1F61193281D8FB96A78,
	ImageFrame_IsEmpty_m7912CACE26E03C46CB8F83F88C320066A102D3BF,
	ImageFrame_IsContiguous_m58B08C6C48E05CB2F01D445A7CF6A04FE8B61E8C,
	ImageFrame_IsAligned_m6F6B228BEA320E63BA6BAD80E2A19D00291DC4EB,
	ImageFrame_Format_m6B77B6314857CBE754511605C80D1857A945EDCB,
	ImageFrame_Width_m3304C2896325DA21083D049787DD1099E7D93055,
	ImageFrame_Height_m6A48AB4A397EA894CC934B93EEC339404E15A11C,
	ImageFrame_ChannelSize_m3124500B1632D837AA9E93DD0184079ACAA5E5E6,
	ImageFrame_NumberOfChannels_m2E5790091664B6745BF9BA2F8CBB71AB9350CC82,
	ImageFrame_ByteDepth_m638AE8635AB0A084AA6A129E7C3D2E14F0CBB45C,
	ImageFrame_WidthStep_m909F70841936B4AED0662AF47C6C338CA608AB6E,
	ImageFrame_MutablePixelData_mF01774C70E45D02B97516D6068A3F743BCF2EDBF,
	ImageFrame_PixelDataSize_mAFF965177FDBE24FD084798066E749CFE27FFA5D,
	ImageFrame_PixelDataSizeStoredContiguously_m68E91BE457D940A9330D008BF27272C369337171,
	ImageFrame_SetToZero_m161454E46ECE6342D6BD5CA522D06975E3B003D0,
	ImageFrame_SetAlignmentPaddingAreas_m5F12CAEDC2D558C3D8DD96E1FB4CF51BFF2A3218,
	ImageFrame_CopyToByteBuffer_m9AA396B301E4C66B72243E42EB86049E3AC068EC,
	ImageFrame_CopyToUshortBuffer_m960C7E177961D859DCFCF95D9845864CF2342524,
	ImageFrame_CopyToFloatBuffer_m4C3FDC93762FDC679FA0AE44621347FC08ADD11D,
	ImageFrame_GetColor32s_mD2096829CCF07C9F53B7A33C849225B33E3B56F3,
	ImageFrame_FromPixels32_m9120673B8A0650BD04B61D67A06C46308ED5F1C8,
	NULL,
	NULL,
	ImageFrame__cctor_m5A12C8FDAC49DA6B2E4C371E598DCA3B73C7171E,
	Deleter__ctor_mBB58A201C41ABF007FA3CEDE78267CABDDC5D4FA,
	Deleter_Invoke_mC17A2FF230D6866E5DD2D6E540AE5C33CF61D6E0,
	Deleter_BeginInvoke_m9C7D93CB27971E6A5FF8501E19796058A67A7A9E,
	Deleter_EndInvoke_m32533BA7EA9D551EED7D9D7F03A16C3194C46E20,
	CopyToBufferHandler__ctor_m594E51AF66793E7829C89EEB20453C8F968537DB,
	CopyToBufferHandler_Invoke_mDAD87CAE73D1954DA71BBEFA9855119C09FE841A,
	CopyToBufferHandler_BeginInvoke_mD3B015953708075E3B6178F48ACB01C63C108843,
	CopyToBufferHandler_EndInvoke_m099390B5454C2A85CA354C983188E93BE6B20FDF,
	NULL,
	NULL,
	NULL,
	BoolPacket__ctor_mEE22B0E58DF3C687C6080F4BBDFEC9711B37CDC7,
	BoolPacket__ctor_m4B7F1FC58A8A9D29A6F3E41EC836BC7F2167D7E9,
	BoolPacket__ctor_m015E8D32154E855F3BB265B46FF981DEF11BF0B1,
	BoolPacket__ctor_m6E643109D20FB0EF20CE617FCF3FCC2F0B4BB579,
	BoolPacket_Get_m8573DF1E3D437F3529FAF8F1B61A67BBCDD737EA,
	BoolPacket_Consume_mFB8814960CB85F85438EDAB217DC842F1EAF1B19,
	BoolPacket_ValidateAsType_m701BB77A9DC1C68E0A90C14519B9DC0FA4EFEA4B,
	ClassificationListPacket__ctor_m7D8BB7BF96CE4DD6909F0530D057940631992B90,
	ClassificationListPacket__ctor_m6F5ABE97EF75801E06D7A4386EBEDC772E0ED256,
	ClassificationListPacket_Get_mDFE6549E5F24ACBABA44FFF1C1951DB61F065693,
	ClassificationListPacket_Consume_mFB138C8195DAFEC6FF40197FAF671488A2DDA3C5,
	ClassificationListVectorPacket__ctor_mF0C5750DB5C5F62D1701CD7FCAF5515FE11F2DBD,
	ClassificationListVectorPacket__ctor_m4EB266F98C0E27EECD91DE06821806D532FE6652,
	ClassificationListVectorPacket_Get_m602FBADF65C1389449E17EBE9942BA2396BE3022,
	ClassificationListVectorPacket_Consume_m2401F688ECBFB4B789DE258D12FFD58561E497DB,
	DetectionPacket__ctor_m70A631AC1200AA90328E94CA6D3F4A0114AAA712,
	DetectionPacket__ctor_m14188B6220F109A7CBFABAC440E7A7D3C8B3CB0F,
	DetectionPacket_Get_mC7A86B2FB0B47FA4394CED9D64B16FEB93E78A28,
	DetectionPacket_Consume_mD410C3EB7420273D6012988B5879E444F4E574E4,
	DetectionVectorPacket__ctor_m84905BC9D1D2C722F8032AE4ECCC5CCE338A8FAA,
	DetectionVectorPacket__ctor_m4923465D388C8D4BFA389B0D863E06E184EFE9A8,
	DetectionVectorPacket_Get_m1A52D9A60177DFEB75BAA4994935CC8A3CAE250E,
	DetectionVectorPacket_Consume_mEE7E7CC4C5AFB803328A2859E8B4543F6FF5A659,
	EglSurfaceHolderPacket__ctor_mF78E88A8A6359A8555991B23D381286563B6B6C5,
	EglSurfaceHolderPacket__ctor_m98FC5A830FB4C6ADA9046027C1701F0EC8338DF2,
	EglSurfaceHolderPacket__ctor_m1DD5B3B240C421D80FBAB771553F6CCE05310E09,
	EglSurfaceHolderPacket_Get_m5E1464B27335BCC765CAE7E534B3C96A6C07A4BC,
	EglSurfaceHolderPacket_Consume_m90583CEE4077F1D4E1C0C50B8CD12FB27F5A56C9,
	EglSurfaceHolderPacket_ValidateAsType_mFF906C70E7C7D117568FB88CE0927923E0A364FF,
	FaceGeometryPacket__ctor_mEC53EE49D2C63E4BBF95061B9FB86A46616C29F1,
	FaceGeometryPacket__ctor_m18C4AF0AC5FA7EB672A326BB4ED3AF752BA60DB3,
	FaceGeometryPacket_Get_mFC9FE7711C8944067DEB26E2577063A83EAF9FB4,
	FaceGeometryPacket_Consume_mFB59B93264C62ED1AE5BC253AE7BBFE24176BA31,
	FaceGeometryVectorPacket__ctor_m459EF509CE7172963988C5CBED487B0DD00FE586,
	FaceGeometryVectorPacket__ctor_mAF6EBA85D11220797E9D56C0376C1DD1CA057354,
	FaceGeometryVectorPacket_Get_mAFF8E0CB41F0EC5D7AEBB7570708995518689178,
	FaceGeometryVectorPacket_Consume_mC1E76CEC7018B01D128568996AE42067B734FC7F,
	FloatArrayPacket_get_Length_m68103A1AFD1107D6D687F4ECA1A8E7B6AE758A48,
	FloatArrayPacket_set_Length_mF1B7847E79DDB1D84031110539EDE34E93D52948,
	FloatArrayPacket__ctor_m476339A32806070580F3F2CF5219999A1E8606A4,
	FloatArrayPacket__ctor_m8B32604AA9F13944B583A056BEF28D67DD491538,
	FloatArrayPacket__ctor_m4744778764B78D9BD731E13059EA1BAEF3EDA1F6,
	FloatArrayPacket__ctor_m4EBD038F4964B10FC499B8592CE160B87BE75AE7,
	FloatArrayPacket_Get_m999ED4EFEFBAB026EECEA379BC1D52DA8F69DC3B,
	FloatArrayPacket_GetArrayPtr_mD4732D5649A29E5EB6F371204B874C3130F7DE29,
	FloatArrayPacket_Consume_mF5A75E832D4CAF3CE1640E95E7B37E5F7AFB8EA8,
	FloatArrayPacket_ValidateAsType_mFEE6608EB63B92AE9C5D4EB76D312C11456D8326,
	FloatPacket__ctor_m94D4DD12FE67B6672394A8C29EC5D1B8C926DC15,
	FloatPacket__ctor_m3EB8CB7BC8960F8FD755711692AB320BA8006E28,
	FloatPacket__ctor_m69CBE1B718A00C9D85C78338A78A62B0AF78E612,
	FloatPacket__ctor_mE750051038E9A2C9665C8C7BB94ECF78A76703F2,
	FloatPacket_Get_m8B69FF4C7DB98F3186125A496545A5F7745B2BB8,
	FloatPacket_Consume_mC8157E32C5E845CCFDE3C3129A4FEEAAC3D06D55,
	FloatPacket_ValidateAsType_m22D1704DDEF951D702B4232135EEE089028479E4,
	GpuBufferPacket__ctor_m88A14BF857318BC48B291942A1F9241E3A877E55,
	GpuBufferPacket__ctor_m41B47DB9422377B8160066B021C6F0D4DA4A8605,
	GpuBufferPacket__ctor_mCB128ED7406480F17F4C10EB124B01078D8B64E2,
	GpuBufferPacket__ctor_m264281472954E7D49DA678A561026D588FA77F34,
	GpuBufferPacket_Get_m8250DCA09877B9706616E7530AC4FD462E884714,
	GpuBufferPacket_Consume_m54A8F02F46DAD644282B922D1A0F7F6910B76164,
	GpuBufferPacket_ValidateAsType_mB2969F3867B4DA5E0BB15F7539E6B69397A8DDDE,
	ImageFramePacket__ctor_m8C42C8E1ACE52FE6DAE2FC8FF41373D1CBD90B7E,
	ImageFramePacket__ctor_mE506EA2475806BD9B8E02FB8DA65B64CCCD4A22D,
	ImageFramePacket__ctor_m62F88D26F8094EB837D8E940DF278B2F60625F3F,
	ImageFramePacket__ctor_mFDDD14887EE56428667A17AC7F93EE832D80DDA3,
	ImageFramePacket_Get_m5F81B855F3053CCE9F6BE213009E9A7DE9F3C1C0,
	ImageFramePacket_Consume_m005203727D1EBD3C486737CAB25330B2C1369B6C,
	ImageFramePacket_ValidateAsType_m7510228BF540F55078E09A0BE57F7F4FE5F049F3,
	IntPacket__ctor_m07BF80086BBB1A25E7371F8CB381665EF923ACF0,
	IntPacket__ctor_mE962567B69D3B89693E8C3FC2AA941B57E130E41,
	IntPacket__ctor_m76E92AD15634EC1F08604A6FDA03AA78EB87E7BB,
	IntPacket__ctor_mD7F292657D6386C4F0A424EC6ED5DD5D68728278,
	IntPacket_Get_mBBC1199637935AD3AA6F65FACFF20E908DEA929D,
	IntPacket_Consume_mF8B617979BE7123778BCF55BA71807FC225BB9C5,
	IntPacket_ValidateAsType_m239AB41E9DF7DD535579BB42F79D0D00D314238E,
	LandmarkListPacket__ctor_mB519037236737ED36145D0C242A3056941A6DC05,
	LandmarkListPacket__ctor_m7908377EC47937ADD6B5EB95D3408FC55E142BB5,
	LandmarkListPacket_Get_m9CA05B24C36E55F4F7D7A048A2B60C7A60C2DBE7,
	LandmarkListPacket_Consume_mD2AD293B62BF76437A17145F58F3A94B6F5E8C4C,
	LandmarkListVectorPacket__ctor_mB8751CC00D67D9A53EF23E429AA8E3D3C9149371,
	LandmarkListVectorPacket__ctor_m5292AE0ACD60A81C934E1DC644231E4FE8F5E1AE,
	LandmarkListVectorPacket_Get_mB39D04D8428292BB3D3C0988B306FDCE8F9262EB,
	LandmarkListVectorPacket_Consume_m723B97443E78FBA6B1C6951F777165B6A8EC033E,
	NormalizedLandmarkListPacket__ctor_m8146FE9538D4496BA288556EAEBE3935B694DDC6,
	NormalizedLandmarkListPacket__ctor_mF7DCC4B6BA9677D972F2E26AB50EC451DFFA266A,
	NormalizedLandmarkListPacket_Get_m966758F9DE1713DF2CE377746E3841CEF7D9724B,
	NormalizedLandmarkListPacket_Consume_m804E7B6D85E27A1F59B17DF57579C6C4EBCB1D12,
	NormalizedLandmarkListVectorPacket__ctor_m8A2AA4FE3890246F4048F9CFEE4A447D665614A2,
	NormalizedLandmarkListVectorPacket__ctor_m2998B1890D97FEEECA1DFCCA4FE6893372398719,
	NormalizedLandmarkListVectorPacket_Get_mFFC22EC67235BEF8E943C80E310437DF9441744B,
	NormalizedLandmarkListVectorPacket_Consume_m1807E6D5795354CD0B1B2AE28D733EAC91C8A2E7,
	NormalizedRectPacket__ctor_m6C1495F3EA126FB7D6BA735EF64BC57E82022F59,
	NormalizedRectPacket__ctor_mF4FBD2D256CFF83792D7499056418C9E26AA93AC,
	NormalizedRectPacket_Get_m55E780812C034845667FDD2D3686F47C74CF4707,
	NormalizedRectPacket_Consume_mF3DB38A19AA0DDFE20C1FA218A13E9B53A3B160A,
	NormalizedRectVectorPacket__ctor_m8866E2D88AE2CF73A4B70F93A8C7D6C5F311FACF,
	NormalizedRectVectorPacket__ctor_mDA9127A3C79348C5FA80FCAE21C39DAB13AEB5EC,
	NormalizedRectVectorPacket_Get_m435F2E28DF7D9CF437739CAD8349EAEA10B6591A,
	NormalizedRectVectorPacket_Consume_m9C00B4AB8B7FB901A474BAB2DDFC725501156704,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	RectPacket__ctor_m397951D4B853FF046034D31D43F4A6412CE4FAA8,
	RectPacket__ctor_mD63909E2139E69328D20043C87EB456EFDA5708D,
	RectPacket_Get_mCF71D733DA3BADB44C7C308643EB57A13DF7CFE9,
	RectPacket_Consume_mE49B0F253C83AADA2E1618C7E42EB571CBD05038,
	RectVectorPacket__ctor_m20C7C5CDA3D814F26D2CE1B506D54E54E437484C,
	RectVectorPacket__ctor_m3EBD3B7353132A2164E58446C744D9DB848AF54B,
	RectVectorPacket_Get_m0558DC78485DC33CD2B83E42995ECF9730CC6546,
	RectVectorPacket_Consume_m10DDE90499EADC09D44FD47F2ADA86F448B42ACD,
	SidePacket__ctor_m94BBD634075945D876D1165B0124C48A9F55B60F,
	SidePacket_DeleteMpPtr_m42E775911F7B60DBCF1080984362B84614054183,
	SidePacket_get_size_m158F3F0476D058B162A51B385A7AE98FD8D71A4F,
	NULL,
	NULL,
	SidePacket_Erase_m6D37DF450F205FE9E170530996746C3646DF215C,
	SidePacket_Clear_mE1D68C3A1CBB09B03A8990C3ADDA38F406A60910,
	StringPacket__ctor_mBB141B0BB40EC079E68BEC133596BFE7A050ED8F,
	StringPacket__ctor_m0C225D27A264A54D3FD0EBC4B1D87DE740073B45,
	StringPacket__ctor_m7628BDAA9CD2CAFBBD4F9D101B6FA8C63B8FE9F7,
	StringPacket__ctor_m471CF1CECC04CD64E658768C22B5D62CFCE447FE,
	StringPacket__ctor_m5C56F71BEE91B48AFE934A6511B3C8621D7B658B,
	StringPacket__ctor_mDAD7F17DE476143ABF079FE0DD4A1232D3846869,
	StringPacket_Get_mCB1372C9A02FCE412CFC0707AA28351DD979DC88,
	StringPacket_GetByteArray_m8ABA453AEB363023B50B507D9F764921FA93FF31,
	StringPacket_Consume_m47C8076F9CF78B27C2D82CF00BAF3E194180FC44,
	StringPacket_ValidateAsType_m5B09B9DA8991CF004E238BBEAB7770226FC227D0,
	TimedModelMatrixProtoListPacket__ctor_m95CAC2FCEBDBE47EAB99458705A3379863B487A4,
	TimedModelMatrixProtoListPacket__ctor_m4E6BFC9894B85C14E95D5BD63C9F5BF8300C180F,
	TimedModelMatrixProtoListPacket_Get_m6C96E21574E7978CDA1C1FCAC67B09BA22182913,
	TimedModelMatrixProtoListPacket_Consume_m22BDBFF6C7A401480A6EA851F01313BF06E3E88D,
	Status__ctor_m9C7377654210F203C94885752B242DBE89331CEE,
	Status_DeleteMpPtr_m419E44E0CD2FB970DA950DDDC296F4E48DED3334,
	Status_IsOk_m1AB7D6193A62F662ECA6115FE48501605923C4BF,
	Status_get_ok_mA24DE1BC9E9B794EF40FC34F7D7DE10A75D9BA89,
	Status_AssertOk_m5D7CAE9C845DFCF680FA8FA02535448DB73378BA,
	Status_get_code_mE216C5A6ECB9C9C2FBDEEDBD596EE8FB937786A4,
	Status_get_rawCode_m722ABD74814432559C14E91A51A5F3AB2A831959,
	Status_ToString_m43D732AB911552FBFE10F1442A4F6EF700EF883C,
	Status_Build_m6A3A71C812358028B629FB901002FA5E4FB9CBC6,
	Status_Ok_m562FAD5DC80FDFD4E4E08B39C0E551AB38471A5C,
	Status_FailedPrecondition_m9206B46B17436E94E7C15CDFB5BBD621FF546DE9,
	Status_GetPtr_mB5ACB15D7C5332303206547CF6AF7E1AFF3DC267,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	StatusOrGpuBuffer__ctor_mE24B41F053AF28D40D7C96CD0FE0FD6A2553127A,
	StatusOrGpuBuffer_DeleteMpPtr_m3505E083EC078519ACCA212C5F64D42E6BEA17E0,
	StatusOrGpuBuffer_get_ok_m4FEBB5F31D15FE3BCAE7C459EFE808C37DF3F5A2,
	StatusOrGpuBuffer_get_status_m0D1E1EA95E28140596F8A0C44B83646E5C427CCA,
	StatusOrGpuBuffer_Value_m2A0536109A5976811746230BD1E97ECF21A8EEFB,
	StatusOrGpuResources__ctor_mCB2C02477107410DAA5414C8A11A91A8097D353F,
	StatusOrGpuResources_DeleteMpPtr_m0D0DE02E8D3FE8B4B015EAD3AA48D341E5AF1013,
	StatusOrGpuResources_get_ok_mBA8B9F8EBC2098830E59F865216B96CB517DB317,
	StatusOrGpuResources_get_status_m0EF93F6A9D499FACF5692B14F9C7E545AD272802,
	StatusOrGpuResources_Value_m00F3851C340D6EE8526D1A4972AE71CDBA1845C4,
	StatusOrImageFrame__ctor_m3F3BD600C6E96CE4AD3C44AF0C081B0FE38324CE,
	StatusOrImageFrame_DeleteMpPtr_m9D13A0FCD189EBBB32A8A33DE3C064F2AE110709,
	StatusOrImageFrame_get_ok_mCBCE759B46C402A2F70758C29CEA35C2F0FC1554,
	StatusOrImageFrame_get_status_mC983700682CC7620FDDBC833B1BBD953FA5A8711,
	StatusOrImageFrame_Value_m6757DFC34202610E8539EF99F2348260F22F8367,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Timestamp__ctor_m8984121477483A8AECBFAA63B42671654ADF76E5,
	Timestamp__ctor_mD6BA157AECDF613EFD3712BC891FFC56C803E382,
	Timestamp_DeleteMpPtr_mFF8FC74D2902C2D070B2EEA3BF00D873E1B8841A,
	Timestamp_Equals_mB921E5812E7C119E66951F63F023AF46F76360EE,
	Timestamp_Equals_m09DAA18C300695E041E4478D89F943395F29B095,
	Timestamp_op_Equality_m693EB078DE5DFFDCF5D4B4EC21090A2A5BD9A5DF,
	Timestamp_op_Inequality_m84514AD05FB1FFBF9B68A722A83D82773A316694,
	Timestamp_GetHashCode_mB39CB76DB3A664F4327A75F5E6FC33FAA4CEE584,
	Timestamp_Value_m8ED1D5B544ADFBDFE6C22A54A51129354234A1A0,
	Timestamp_Seconds_mD2208527892D8BBC5B0B0507DFC8C86003DEB0A2,
	Timestamp_Microseconds_m652EC31FBA7E8DA3D765846C9F2E52B1BB01184C,
	Timestamp_IsSpecialValue_mB59E9DA7BA75120FFC78B38C3932A6CAA5E9408F,
	Timestamp_IsRangeValue_mA39BAEAA089FC7EA88A5A6E1F3D3A0D2971AA6F4,
	Timestamp_IsAllowedInStream_m28787D306FDFC76500F1CECBAFDD0A2527D5ECF9,
	Timestamp_DebugString_mB94B553296CD865FC8C4C15B8D281D0597D35B3B,
	Timestamp_NextAllowedInStream_m1CC8BB280F0D28A05D4314FEAAFEE5DEC4CC81B2,
	Timestamp_PreviousAllowedInStream_m69CB5A37663015FDE11837E5F7019F943FEBB63B,
	Timestamp_FromSeconds_m95F2743A6A8BEF54DF7A735D878F6B94673E7146,
	Timestamp_Unset_m73DB62A88E38F14EF156DA76FA6C3471C4699878,
	Timestamp_Unstarted_mDDB33C7B62A030C0907F9BDA2BC7906FD5B80F34,
	Timestamp_PreStream_m15B60672CFE0A4243100214754A47C0B16DB8901,
	Timestamp_Min_m7E9A6D79B9FA7C9C969290FE78245318AC5D16EA,
	Timestamp_Max_m070F224F1AF233AFE6FBDC66006A9643DD0554E4,
	Timestamp_PostStream_m0FD73F66D53B8E005C47FB371073535B644F3B53,
	Timestamp_OneOverPostStream_m7E87501F1887BEF9ECC5D7BFFB47F0708CF693D3,
	Timestamp_Done_m22451488C91102C475D8C2D015E2EC148AD5A263,
	Tool_GetUnusedNodeName_m0C955B325CBB0F5B42B07737B763A30997BFD14C,
	Tool_GetUnusedSidePacketName_mF68C95622DA9603667EA97F2C388C430E54AB73B,
	Tool_CanonicalNodeName_mD3E38F7DAA65E9484FF8EB5CE66CD1F24C409FC9,
	Tool_ParseNameFromStream_mC1D798DD23698FE600D0E04A45906A0F5D774D1D,
	Tool_ParseTagIndex_m057EA602EE210C4A8B59B78835444EE7B70CC0FE,
	Tool_ParseTagIndexFromStream_m1861ED05301E17B8823410C368E96FDE62C799CC,
	Tool_CatTag_mCE0C8D85484E79F321B34BAFC74B6F33D07F6DF1,
	Tool_CatStream_m5ED50ED47178AABD3A742F702E71F7F072FBFEC0,
	Tool_ValidateName_mE9A665D09188303F3C615AB000E086A6461DA7A3,
	Tool_ValidateNumber_m5C9A486C7A5FF2A390FAF79A7E664AE667028EEB,
	Tool_ValidateTag_mB6C5A26CCEDBFF11F24EF88C2BB928560D273D56,
	Tool_ParseTagAndName_m94B1F5FDCB7815D93AF05CF1BD7B4140D26393D1,
	Tool_ParseTagIndexName_m24DA0B173A21A2445AFE1955C39A1B7DC2F82D27,
	Tool_ParseTagIndex_mBAA2C082D2408EAFA201F0FBD943892E0FF0BDDB,
	Tool__cctor_m74F926ED19E84CAD3BF399CCB7C1062350978DE5,
	U3CU3Ec__cctor_mF3525D366FAF1C69C7DB2BD020B1B2C24881EBF1,
	U3CU3Ec__ctor_m8B39BB6CF13D89A43CA2C80F9099F71F40A683B3,
	U3CU3Ec_U3CGetUnusedNodeNameU3Eb__0_0_m1573C18DBAFEE5305A01C13A0ADFD7617D56ABA6,
	U3CU3Ec_U3CGetUnusedNodeNameU3Eb__0_1_m973A43ED4082D09D59069B0CD717E9AA10439EF7,
	U3CU3Ec_U3CGetUnusedSidePacketNameU3Eb__1_0_m032B4D03AD11DAFBA1F2305761CBBBFE2BBC52BD,
	U3CU3Ec_U3CGetUnusedSidePacketNameU3Eb__1_1_m31C41D012EC747D3B46B661601D6B0D37AA4051A,
	U3CU3Ec_U3CCanonicalNodeNameU3Eb__2_0_m09CC4407B697A7EDE5149AF583946FE8D0EB10B5,
	U3CU3Ec__DisplayClass2_0__ctor_m23CF51FC77B997BE7F8EDBC2930024FE9E0865CF,
	U3CU3Ec__DisplayClass2_0_U3CCanonicalNodeNameU3Eb__1_mFF877148EC50CBF017DFA3AC2C7B076B8592640C,
	U3CU3Ec__DisplayClass2_0_U3CCanonicalNodeNameU3Eb__2_m3D71E7837D418B4AE8BED40F3FBF79EEB1090097,
	Internal__cctor_m2BD3907D1F71437953C5947E0E835AB04BADF8EC,
	Egl_getCurrentContext_mB4CF66E727F4062DD38E8C856F6474C2934C3D6B,
	Egl__ctor_m3BDF96C8F9476CFD7D587C692B4E7954F97FA070,
	EglSurfaceHolder__ctor_m7BE3959ACDF959BEA69F2267CED3236502E36447,
	EglSurfaceHolder__ctor_mC65B9953DB0EADC85668F3D0B6C3018C6733900F,
	EglSurfaceHolder_DisposeManaged_mEEB3A6C584E34F4F5825167BF0FD40984291984F,
	EglSurfaceHolder_DeleteMpPtr_mF106A2DDC3996BFAE265D62C20F3139209F4C341,
	EglSurfaceHolder_get_uniquePtr_m23F917F234894D1E453FBA278CBDE7DDD8EFFC49,
	EglSurfaceHolder_FlipY_m74D0271352027D3A368987992330EA00255441C4,
	EglSurfaceHolder_SetFlipY_m83B3559FF1CB915DCF329C75726FC71531B92ECB,
	EglSurfaceHolder_SetSurface_mCCA2BDCB0580B8DB530640DC190D29D8D61E0FA8,
	UniquePtr__ctor_m711D1B92C36E741A5DBF23A2AA8797A3E399D15A,
	UniquePtr_DeleteMpPtr_mEE74CCF03702726FC9E702E9FC3D1E5AD9A93510,
	UniquePtr_Get_m9FE36CBFADFCD46B550EAB9A0BE4499FAB6AD63A,
	UniquePtr_Release_m51B4FAA53C6A85E42FD520D460A786A828C4E937,
	Gl_Flush_mFFD91F9F1B554478D1FFCDDFCE26F081C1EA762F,
	Gl_ReadPixels_m8A6F20357EFBF3D510DF8B17EE0D3A20185C39DD,
	Gl__ctor_mD8F762B01CC4DA9A9609808494E569FEACB51C8D,
	Gl__cctor_m849DB06EA463BBB4C25700A5D083CABB48992D1F,
	GlCalculatorHelper__ctor_mED9CE6D90FC19E484067DFC4FFBAF051FA4A7592,
	GlCalculatorHelper_DeleteMpPtr_m6E2BE53BB044DB3D3014B937AAF80905212B3321,
	GlCalculatorHelper_InitializeForTest_m3D82F897DD6397CF04573B3D55086806220A186A,
	GlCalculatorHelper_RunInGlContext_m854262F1C00A435A27E0F029A3DA335CC85D6377,
	GlCalculatorHelper_RunInGlContext_m14CC71E506057D595A328999456DE5DC480BFEF9,
	GlCalculatorHelper_CreateSourceTexture_m4B7E02C15E08649463AF29A0448FF6B4BC989078,
	GlCalculatorHelper_CreateSourceTexture_mA608BEEC8A3D3789D432DD612DBDC669E659B215,
	GlCalculatorHelper_CreateDestinationTexture_m671F45F0B3205A64A7A80E597A9882736337F47A,
	GlCalculatorHelper_CreateDestinationTexture_m30E9A4995EA72C0D40EF92977855DDF5490DDFC0,
	GlCalculatorHelper_get_framebuffer_mCE417D5C2D12B89AD7E6531F45C6B26831DB1F08,
	GlCalculatorHelper_BindFramebuffer_mFB23876074DCA60C97303E4DE2D45E3321772729,
	GlCalculatorHelper_GetGlContext_m8BADA680F11266E543F52D1CCA4047E88090F634,
	GlCalculatorHelper_Initialized_mF4D1152677917FB45DE9E4CF255385C7D0EE04F2,
	NativeGlStatusFunction__ctor_m4E4DBC119EA964E1487480F33DF30E248B37ED0D,
	NativeGlStatusFunction_Invoke_m4903685B192FB0C37BF8BA15A25725489CE9913F,
	NativeGlStatusFunction_BeginInvoke_m916161DD9E8A043DFCBA94AE466D439B734F78C4,
	NativeGlStatusFunction_EndInvoke_m9F18E6DA0B6441B5E8457E5D42A314C2EECFE27A,
	GlStatusFunction__ctor_m197CC04C90CAE808E7AE290D518447591C3B7DB6,
	GlStatusFunction_Invoke_m12BC53A91B6D252C89C941F220446CFF06E8C245,
	GlStatusFunction_BeginInvoke_m5D5DEC877E7D08432AD8446D197085585CFF7705,
	GlStatusFunction_EndInvoke_m38AEFE4208B350E20FDB98ECDBBF4A881320F33D,
	U3CU3Ec__DisplayClass6_0__ctor_m7C001C7A63DA237AD4C4632470DBD4C776DC46D3,
	U3CU3Ec__DisplayClass6_0_U3CRunInGlContextU3Eb__0_m1DFECA19C476C7A6BB493FA7CC03EA093EF7410A,
	GlContext_GetCurrent_mB9A6B5CDABBDA48C1836129B5AF00F5973F82772,
	GlContext__ctor_m1C07CDE642639B52680E89B12FE36BCC9264F06E,
	GlContext_DisposeManaged_m6B35429B3CC257381398D44A37D63D2FDF82DCBC,
	GlContext_DeleteMpPtr_m40E799D4782C27A8E4C78407C4BEE0B393721BAF,
	GlContext_get_sharedPtr_mBE1A1440C22EA958C65B1304EC7349DA290D0D23,
	GlContext_get_eglDisplay_m13BE65FAFE297827F6515C99EFB2F5EC3D175F17,
	GlContext_get_eglConfig_mA6E26C9E54F1AD4EFB40BBF07C868F8ADD3143DC,
	GlContext_get_eglContext_m8D9B78C2AB3E803AF4884E0C145E5084415D78B9,
	GlContext_IsCurrent_m873729CAEF4CB78A38FD49BA0F88B235B83295A3,
	GlContext_get_glMajorVersion_mC9F14DE57BB0B56BA6385D653B6C5551B1FF5490,
	GlContext_get_glMinorVersion_m2ABD04E5FBE49FEE33603695580B503D32C3615B,
	GlContext_get_glFinishCount_m216DE26ADF237DDBDB4AFF32899E0B0F81949ACF,
	SharedPtr__ctor_m14C3EE05599C49269C8D16730873C3261D653CA2,
	SharedPtr_DeleteMpPtr_m2ECD289E992B3D2E2A7382AD9DBD253CDCDF9AE1,
	SharedPtr_Get_mC3E26F745D81CCC1728BCB1A7FE99AD85AF8CDC4,
	SharedPtr_Reset_mA4990583161BE5BF3685740D631C9706AA21E1C1,
	GlSyncPoint__ctor_m6456B57D6B4D4ED7021648601CA6BE899AFC904C,
	GlSyncPoint_DisposeManaged_m1C6D43F20A6EACCCAADBDAE3356F6C0B41FDE63D,
	GlSyncPoint_DeleteMpPtr_m30254F0135C6FCB8173C42B2548E4CF7CBD3FFAB,
	GlSyncPoint_get_sharedPtr_mDB32F436A87D83EDB577F21F646321B467CE28BF,
	GlSyncPoint_Wait_m3232AF4DDE782E1BE174B8C3AEFF50CDC02CD7DB,
	GlSyncPoint_WaitOnGpu_m063A587E38D216FE3EE27962942EC90A7B7FC9C0,
	GlSyncPoint_IsReady_m6A324F61F14F7AE4E66477CD17F983B4EE9B4825,
	GlSyncPoint_GetContext_mF01089C943E1EFC34B59BE388BE77D376E4F78A7,
	SharedPtr__ctor_m5BD3C4AE88D2601F50DEB1C411E9465267CF6E37,
	SharedPtr_DeleteMpPtr_m44ADA2336F2C5CF6303A5D83528EEBD8BB121D72,
	SharedPtr_Get_m37B1B746F7047DED6894561A572BD8C6EB101412,
	SharedPtr_Reset_mD72AB969653217D5A30D22E0BF88F4AD69500814,
	GlTexture__ctor_mAA5D2577B3C6E8794DF8D61ECBEFC56C00EAD57F,
	GlTexture__ctor_m7F5DCB7763F6FAE5FCBF5A6E1591AFE1B2B3FC63,
	GlTexture__ctor_m2AEF92C3D88B651E1EF05501A3AD0AD69337C3BA,
	GlTexture_DeleteMpPtr_m41955E8A8D79B8DA9955E8965E53529F4B212A6C,
	GlTexture_get_width_mD683963793D7AFDBA0D21208045F50F17D3C9F2D,
	GlTexture_get_height_mBD9DA13F674A06BDE9A6B5B3D91F2A7C9F840CCB,
	GlTexture_get_target_mE253484EB717A8FFA08A0E6C5D9478ED4F6D977F,
	GlTexture_get_name_m3ECBC06AFCE90777A0C246C37D4DE832CED8BBF4,
	GlTexture_Release_m21906AC252786712676B7C1317F6B4356755D04C,
	GlTexture_GetGpuBufferFrame_mA23FBDF0EC29307AADD4238465576290D14596DE,
	GlTextureBuffer__ctor_mBBBA010E5D01643C0BC9E1DD19983E90AF5BA629,
	GlTextureBuffer__ctor_mA401218742C0C229E0369388ABE52BC83793E731,
	GlTextureBuffer__ctor_mF3409802CBB5671728673B27E741FD72C0B7F1A3,
	GlTextureBuffer_DisposeManaged_m6D138C3B48CFFAA645F8B5EDC935A8A8E79CF0AD,
	GlTextureBuffer_DeleteMpPtr_m21B7F529A2C68F78AE9653548425E1D8B10A38A3,
	GlTextureBuffer_get_sharedPtr_m8A649180BB71728A92A8DC2D89097F1F7803D47E,
	GlTextureBuffer_Name_mEC35FAD74F98B6AA3DEBF2E743A294023FC4977E,
	GlTextureBuffer_Target_m472E0209AB400AED6EF4908DAC90822B7AAE2A75,
	GlTextureBuffer_Width_m96DBFCCD80CE8B28BEDFF56785980B0ED79404A8,
	GlTextureBuffer_Height_mD5ECD427E4F9A12CB109E7F8A34C092A42CF7A4F,
	GlTextureBuffer_Format_m3743C4D5D3A1A2373EC70771EB71E0FCD6AC4261,
	GlTextureBuffer_WaitUntilComplete_mCFE1A82B0E7402E29C15949658E77FA261D2700C,
	GlTextureBuffer_WaitOnGpu_mB01F327D8C651A198325AD2E3C9E6D88B995AD6A,
	GlTextureBuffer_Reuse_mBFB7E77D05EA45583F0A993F4F1C69A97C8FC59D,
	GlTextureBuffer_Updated_m35E5A32106B24F0369F29971FCDB1E5BDA2318AB,
	GlTextureBuffer_DidRead_mE802E78D5C5D53D1595A1EE8142204074F8C9BB9,
	GlTextureBuffer_WaitForConsumers_mAF1873E2A6E07C943119CA7327C661DBB5BA4106,
	GlTextureBuffer_WaitForConsumersOnGpu_m168E33BD1D767B334D311F9CB530605041DA4528,
	GlTextureBuffer_GetProducerContext_m82BACB68186137B343003A742E74378AA8B1B11B,
	DeletionCallback__ctor_mEE936A181548630722C48E8C936D92774AD5B704,
	DeletionCallback_Invoke_m8A004DD58A6F0CFEEF0C779C0D48CB2AF676CE1D,
	DeletionCallback_BeginInvoke_m71EA016BFDCA213EBDB28D4D4583867D4FB4CC89,
	DeletionCallback_EndInvoke_mE7A9FCABB86464713FDBCFE233BE225351E9D257,
	SharedPtr__ctor_m98F6E0D93E8BAAD61638CC2119F3A5D60453FF0C,
	SharedPtr_DeleteMpPtr_m15B99543BE899251E82EB06C391FE369A9A098CB,
	SharedPtr_Get_m5F37E1C54DB721F377C4598E5A4D108775DBB1D0,
	SharedPtr_Reset_m221F2D07E60E20B9762F4A6C1D3414BC20B3818D,
	GpuBuffer__ctor_mEDFB9403435ECE8E7CB8F155F2C98BE3E96F0533,
	GpuBuffer__ctor_m2141005B1A7037462F5444ABC626B1A8101561D9,
	GpuBuffer_DeleteMpPtr_mF427246C893B1357C2EB0BBF92F1B587F47D0C98,
	GpuBuffer_GetGlTextureBuffer_mB806BF061030AE2938A473769CF49B9BCDABB219,
	GpuBuffer_Format_mF0146FDAD099AC813D712C3C74FEE82480FEB05B,
	GpuBuffer_Width_m3662757DE67BF205D717BA97F5863C9B02FCF204,
	GpuBuffer_Height_mF70307C036418D29D6F7402A067F271AE56C886A,
	GpuBufferFormatExtension_ImageFormatFor_m0DC71BAD40318374571505A1C1A3DA9009A74CE1,
	GpuBufferFormatExtension_GlTextureInfoFor_mF1C0CB8FEABA534B7FECD9FF2A531F6B027F2FFF,
	GpuResources__ctor_m741ACB7606DC6CA9CC05041EEF5C8AF3B1803A35,
	GpuResources_DisposeManaged_m4C220E1A1701B1CD894D4149702ECBFC1EDB9EEF,
	GpuResources_DeleteMpPtr_m670CBED6D7322E5348FA1A5A162668CF966BFE86,
	GpuResources_get_sharedPtr_m11E75840CD47CCEC222918BA72FCEC8FE911610E,
	GpuResources_Create_mA6F2EEBA031CF7A093B228AD50C928269CA17B4A,
	GpuResources_Create_m6D68A86FD785FC29E089FF15F6015EBD3E6FA805,
	SharedPtr__ctor_m0A2FC31F39BC8B901D3EA97233C382620227B590,
	SharedPtr_DeleteMpPtr_mE4D3FEF627D2FF2F766A4B7F7EE0057C6AA4E7AD,
	SharedPtr_Get_mA20A8481C67F4A1E9D7E40E3E346A2EEED900639,
	SharedPtr_Reset_m3E67A24946A739AC0F99DD184CA993D253E8793B,
	MpReturnCodeExtension_Assert_mD720CAA4396143E0113F15BBE369066AB04044EC,
	SafeNativeMethods_absl_Status__ok_mEC744B480B0D1DBFE29DEEB2F6B3E2505AB267D2,
	SafeNativeMethods_absl_Status__raw_code_m2C0BE3BD9F5F3CC78A27750FF59AE0984ECB0ADF,
	SafeNativeMethods_mp_CalculatorGraph__HasError_mAEF2E518E58094A550DC430EC3DF6CF766E4747E,
	SafeNativeMethods_mp_CalculatorGraph__HasInputStream__PKc_m96D2D15C893403E4B1C0E39BFAB0EC612BE4401E,
	SafeNativeMethods_mp_CalculatorGraph__GraphInputStreamsClosed_m95C8D9340C9890B856AF401D82BF7A036E6C051B,
	SafeNativeMethods_mp_CalculatorGraph__IsNodeThrottled__i_m5F78ED89AF3A67340AFD7B2E6455E1E762B5624B,
	SafeNativeMethods_mp_CalculatorGraph__UnthrottleSources_m85801679DD5D2FCA1D2E08343B21159B7139DCE4,
	SafeNativeMethods_mp_ImageFrame__IsEmpty_m3B0B75877931357EC87CBFAAD90B872D961AAE95,
	SafeNativeMethods_mp_ImageFrame__IsContiguous_mA9B0642B4CAB465D52FE42516282F230740FADE6,
	SafeNativeMethods_mp_ImageFrame__IsAligned__ui_m43CD5220D418D6B7BE55768C3110E97C166F14B0,
	SafeNativeMethods_mp_ImageFrame__Format_m20A45FAAEDDA6825FF189F1EABBC5B28BDFE8C3D,
	SafeNativeMethods_mp_ImageFrame__Width_m4CAB7749017189207C979B206F8658296D2B2836,
	SafeNativeMethods_mp_ImageFrame__Height_mEA6B4E9A8FC6737B62F44C45D58090085FA9CAFF,
	SafeNativeMethods_mp_ImageFrame__ChannelSize_m30EE73317EF93BD9090A7EFC9A2B1FAECD4AE9A8,
	SafeNativeMethods_mp_ImageFrame__NumberOfChannels_mEFF318A9468A0E2DA8C68017E7D0E215EBD78308,
	SafeNativeMethods_mp_ImageFrame__ByteDepth_m892DDB59B17B0BD2927C32801E0E69C341C0DEE9,
	SafeNativeMethods_mp_ImageFrame__WidthStep_m871665FBF157028D03B465834FE21593D5821A94,
	SafeNativeMethods_mp_ImageFrame__MutablePixelData_mAFAB8BB5635D6966958D11B8959F5B8BA44926A9,
	SafeNativeMethods_mp_ImageFrame__PixelDataSize_mFD9AF92BD01CA7EB665BD7BF85B6E7365C143522,
	SafeNativeMethods_mp_ImageFrame__PixelDataSizeStoredContiguously_m863D9349265A0D3ED2BBE719B24F68DE905ABE76,
	SafeNativeMethods_mp_StatusOrImageFrame__ok_m35BBD386B1234436B420858C9D3D5D6B327B6148,
	SafeNativeMethods_mp_StatusOrPoller__ok_m83A6EBC921C0A01BD97B741C036D07F85617F22E,
	SafeNativeMethods_mp_SidePacket__clear_m27EDF042F43DBEF61B67D83530C54985CA3B64B7,
	SafeNativeMethods_mp_SidePacket__size_m586B60EBBD006C99029D9AF619D576A06D08D429,
	SafeNativeMethods_mp_Timestamp__Value_m5941A9E3BFEE21D0CADF5B08442F665953571F86,
	SafeNativeMethods_mp_Timestamp__Seconds_mB0EDE5B4BDAD18EC5F0CE34ACD5912A257A32E2A,
	SafeNativeMethods_mp_Timestamp__Microseconds_mAD701EF0D7E21E16B797ABF5121772B012AE389B,
	SafeNativeMethods_mp_Timestamp__IsSpecialValue_m48F93E35FE1EF4CDCF4BF2D27232BF3492470546,
	SafeNativeMethods_mp_Timestamp__IsRangeValue_mDBB73A6533348F04CA3CC012BBBB75D512EBF413,
	SafeNativeMethods_mp_Timestamp__IsAllowedInStream_m4608AF1406219709EAFEDCEFFC32422E45FF4762,
	SafeNativeMethods_mp_EglSurfaceHolderUniquePtr__get_m4CDE3F55FDD11B6CD970031672CF95833F0457C4,
	SafeNativeMethods_mp_EglSurfaceHolderUniquePtr__release_m8FD831BE5E967AE82ABD6053E513020830AEBFE7,
	SafeNativeMethods_mp_EglSurfaceHolder__SetFlipY__b_m956C018704280E5DB6E6CE238A534A255E75726B,
	SafeNativeMethods_mp_EglSurfaceHolder__flip_y_mA5D085F3C2DA3AAAA274B503F7214FDC1295BF97,
	SafeNativeMethods_mp_GlCalculatorHelper__framebuffer_mD0F283F9F9E6CDF8BE22307C5BEEBA49A9459836,
	SafeNativeMethods_mp_GlCalculatorHelper__GetGlContext_mB6E5385FB4F38E276A960C2FDD34D587DA1A564E,
	SafeNativeMethods_mp_GlCalculatorHelper__Initialized_mDBF658A570ED0B9AE41F8B45F0D20AADF03C8972,
	SafeNativeMethods_mp_SharedGlContext__get_m77A246A1DD76F2827D8B943E7CD34A126175E78D,
	SafeNativeMethods_mp_GlContext__egl_display_mDFD0D5C5B325AEBFA773B9E5C3ABF5AA820B87F3,
	SafeNativeMethods_mp_GlContext__egl_config_mB1C4067B8E251256C9496B33C5A7F6A769C6655F,
	SafeNativeMethods_mp_GlContext__egl_context_mCF562A477272774F36F248EC01A2F6064BED010D,
	SafeNativeMethods_mp_GlContext__IsCurrent_m0FC507CDDA5C4664FE126E841D8F0722B0C53140,
	SafeNativeMethods_mp_GlContext__gl_major_version_m58A4039D6307CA75B143E2870B38F5B5FBC7F75E,
	SafeNativeMethods_mp_GlContext__gl_minor_version_mA60ED176CF817E57BF86A0159B08CBF2C2A53A7F,
	SafeNativeMethods_mp_GlContext__gl_finish_count_mB59F2811EEEBA1D9081BE3B121B1498755FC4345,
	SafeNativeMethods_mp_GlSyncToken__get_mD56895E3208FF0DA701F53B1466B46F8692FA7BD,
	SafeNativeMethods_mp_GlTextureBuffer__name_m479C783C3F41F317595CC2E7B2DB7A3AF9FA8E1D,
	SafeNativeMethods_mp_GlTextureBuffer__target_m8C6D07CAF145D1A5B247CA83A2FE0DCAA5B5D20F,
	SafeNativeMethods_mp_GlTextureBuffer__width_m6091187038D09CF89A8A2AF1131D2976521273AD,
	SafeNativeMethods_mp_GlTextureBuffer__height_mD60419D574FF25DCC3129D5FDE07E723528057D8,
	SafeNativeMethods_mp_GlTextureBuffer__format_mFDEBCD3289458EAA2A2FDE199F91881080664091,
	SafeNativeMethods_mp_GlTextureBuffer__GetProducerContext_m9DD4394D5FE1BAB9A3F68C647BD1D447B650D567,
	SafeNativeMethods_mp_SharedGlTextureBuffer__get_m685ABA5BC586433E9EE5A45A4493E792B016091A,
	SafeNativeMethods_mp_GlTexture__width_mBA5184A37E978EF66138FD29C6650414AE70C4ED,
	SafeNativeMethods_mp_GlTexture__height_m53E669E4DEF2FDFCA16B8BF1FACB7D8A11C82498,
	SafeNativeMethods_mp_GlTexture__target_m4F2BBDF563DAAD3D406BB3EAB8BCDEC80F2689AD,
	SafeNativeMethods_mp_GlTexture__name_m474E34F1F07E7912813EB4C81ED3592A9B161798,
	SafeNativeMethods_eglGetCurrentContext_mD72A32A6D2BDDA00FB1349C41203B8555F0C9D96,
	SafeNativeMethods_mp__ImageFormatForGpuBufferFormat__ui_mCC9CC4A423B2816E2CE1D6B73E64093C1EEEFC84,
	SafeNativeMethods_mp__GpuBufferFormatForImageFormat__ui_m75341A8F99649E1037C95A7A8330159F7B2548E8,
	SafeNativeMethods_mp_GpuBuffer__GetGlTextureBufferSharedPtr_m98B37FA49D421C1A905EDD5A99C782375B1024F9,
	SafeNativeMethods_mp_GpuBuffer__width_m35DD1BFB04D5A3630332B5D88A386B2DBD43A590,
	SafeNativeMethods_mp_GpuBuffer__height_m022ABAE0EC201C187B9F7B6C1579A3268718EAC5,
	SafeNativeMethods_mp_GpuBuffer__format_mB81DF79DCDEECE226598CB31499B471DAAD9F1D3,
	SafeNativeMethods_mp_StatusOrGpuBuffer__ok_m9B5B4651065A8B7C4BFC89B0A2B8DFC56FA29DD3,
	SafeNativeMethods_mp_SharedGpuResources__get_m57FBD88F3363794B30704C7872639B40D0A31F1C,
	SafeNativeMethods_mp_StatusOrGpuResources__ok_mCBB427A2A37D0C223EF03087F10344B4DE79A43B,
	SafeNativeMethods_mp__SetCustomGlobalResourceProvider__P_mB87C4A8C9CF2BE4E84286CB3259095A473F6BE8F,
	SafeNativeMethods_mp__SetCustomGlobalPathResolver__P_mE4C2A9785019DF67D40E2D5F0F984EAE0AD77DDC,
	UnsafeNativeMethods_absl_Status__i_PKc_mB82E4A27B452A24BC4372A917C898EBF5D6D99C0,
	UnsafeNativeMethods_absl_Status__delete_m2714B82E6E5767C3ABE751D42445E7D46675327B,
	UnsafeNativeMethods_absl_Status__ToString_m8951B57C56E90D52EC1A777539F4A2125CEF3D1E,
	UnsafeNativeMethods_google_InitGoogleLogging__PKc_m7247C0BF7BCCA6C8768CE87F5EB4F0DC2177CCC8,
	UnsafeNativeMethods_google_ShutdownGoogleLogging_m713B45D64FA6740EA54746169BB39B940EC42DE9,
	UnsafeNativeMethods_glog_LOG_INFO__PKc_m80B39329DB19DC3D4FF4AA04D76F3E4440BE3D0A,
	UnsafeNativeMethods_glog_LOG_WARNING__PKc_m9575584BC9A435A4ECA4ED13F73E3BE35027A187,
	UnsafeNativeMethods_glog_LOG_ERROR__PKc_m2B1BCDD27FDE5BED60F513FC9B34E2A1A9ABC93E,
	UnsafeNativeMethods_glog_LOG_FATAL__PKc_m16E265733740AC679C4A4BE789014FD2368061D5,
	UnsafeNativeMethods_google_FlushLogFiles_m00DB44E58A14845083702AFACC409F087B05A6C9,
	UnsafeNativeMethods_google_protobuf__SetLogHandler__PF_m96F435E92258861055A5641775CF6265129FA9D9,
	UnsafeNativeMethods_mp_api_SerializedProto__delete_mC22C4B39E8D7514D85038932FE42184A8A44F18A,
	UnsafeNativeMethods_mp_api_SerializedProtoVector__delete_mB1009B2243E779FB9DA5DE805DBFEC575E20AFEA,
	UnsafeNativeMethods_mp_Packet__GetClassificationList_m5D5B5B259BA062A8428477492A3C96BB3643A69C,
	UnsafeNativeMethods_mp_Packet__GetClassificationListVector_mBC4F2A6BE4F04333EA316763EA522EB6A128F187,
	UnsafeNativeMethods_mp_Packet__GetDetection_mCA5167FA259DE1FA1BA36991C515D0A661C98A73,
	UnsafeNativeMethods_mp_Packet__GetDetectionVector_mBA6AF4461617DB866FEF4C4CCE067CD8DAB778CA,
	UnsafeNativeMethods_mp_Packet__GetLandmarkList_mC1C750C9718BB6E9AB90FE514367F98C3E88016E,
	UnsafeNativeMethods_mp_Packet__GetLandmarkListVector_mC6F29366A62516AEB8617EBA337E88258112EFCD,
	UnsafeNativeMethods_mp_Packet__GetNormalizedLandmarkList_m5EF611C26E7162F20FD2AEB51DF01558308B88E1,
	UnsafeNativeMethods_mp_Packet__GetNormalizedLandmarkListVector_mE680E5DB73904DFAB7B3545C50112033A7E2ED67,
	UnsafeNativeMethods_mp_Packet__GetRect_mA049CCB1123F24642E514987AFC72A3F355A931A,
	UnsafeNativeMethods_mp_Packet__GetRectVector_mA647B744426461E0170868EF986FF01793DFF02E,
	UnsafeNativeMethods_mp_Packet__GetNormalizedRect_m5E23A7F00AAA100FD73AA55D5AA08989CC881D5A,
	UnsafeNativeMethods_mp_Packet__GetNormalizedRectVector_mC34B528BB4B4519ABC6CC9B3594AC8794C93DAC8,
	UnsafeNativeMethods_mp_Packet__GetTimedModelMatrixProtoList_mF0BB4C96FAB580F5C37788678A7EE0811A20CB7F,
	UnsafeNativeMethods_mp_Packet__GetFaceGeometry_m93386E3813301804D984AF23AEC6B712DC448ECE,
	UnsafeNativeMethods_mp_Packet__GetFaceGeometryVector_mD84B81BAD735BD5A64A2A705800424841FF21E53,
	UnsafeNativeMethods_delete_array__PKc_mB7F674A345E1C9862F94699A91B4965A13E801FE,
	UnsafeNativeMethods_std_string__delete_m6B1FB2B41291BEB1CE151B1A1B90D23657491B4D,
	UnsafeNativeMethods_std_string__PKc_i_m3F75B67B76097D20533314BE303411E4749D1DEB,
	UnsafeNativeMethods_std_string__swap__Rstr_mE68CFEE2B4838EE29B606D0E38B0086A06C3B8F8,
	UnsafeNativeMethods_mp_CalculatorGraph___m7FFC20DF00A0E3D5583A0989E91858A69DBFC673,
	UnsafeNativeMethods_mp_CalculatorGraph__PKc_m329C0B20CD5E38149E5AC76B025827B0D925C777,
	UnsafeNativeMethods_mp_CalculatorGraph__PKc_i_m1C2E1FE59994A24961B38D3961F5C16005E4C9CB,
	UnsafeNativeMethods_mp_CalculatorGraph__delete_m4F90E4052AEB8AC600A1EE995DE948B638583172,
	UnsafeNativeMethods_mp_CalculatorGraph__Initialize__PKc_i_m78A6F16896B73BAC036E293A9E30A66370E14683,
	UnsafeNativeMethods_mp_CalculatorGraph__Initialize__PKc_i_Rsp_m11B851A7306D03C2F01C4687AD14EB2AD1AEE212,
	UnsafeNativeMethods_mp_CalculatorGraph__Config_m6A9EAAADF8F36D3AEF4838D18A5B97F505CA4D66,
	UnsafeNativeMethods_mp_CalculatorGraph__ObserveOutputStream__PKc_PF_m609221A7AAA13C9FBDD87CECA529658FC865A24B,
	UnsafeNativeMethods_mp_CalculatorGraph__AddOutputStreamPoller__PKc_m3B501FE83F370CCFA39FF12049AA9B8260A320B7,
	UnsafeNativeMethods_mp_CalculatorGraph__Run__Rsp_mA406BF45E78BC89D290CE6EDA1D0EAC294B8987C,
	UnsafeNativeMethods_mp_CalculatorGraph__StartRun__Rsp_m6EFEC73224583BF5FD5B7A1C9562D94D1D4C8B19,
	UnsafeNativeMethods_mp_CalculatorGraph__WaitUntilIdle_m5836C5B6885E9E0D5FA95257337273895E1B216A,
	UnsafeNativeMethods_mp_CalculatorGraph__WaitUntilDone_m01DD698F4634390750656E085F47398555310280,
	UnsafeNativeMethods_mp_CalculatorGraph__AddPacketToInputStream__PKc_Ppacket_mD46F59A711F3260B006E08516C0A87D78AF4CCC3,
	UnsafeNativeMethods_mp_CalculatorGraph__SetInputStreamMaxQueueSize__PKc_i_mD9A2A728135B91D91148BB502E27F6A99CC75272,
	UnsafeNativeMethods_mp_CalculatorGraph__CloseInputStream__PKc_m72329320798825CF335CCA402B3A4BE94CE659BE,
	UnsafeNativeMethods_mp_CalculatorGraph__CloseAllPacketSources_m4B78C1B5E16004B23D9D3BC97A666241446CF33B,
	UnsafeNativeMethods_mp_CalculatorGraph__Cancel_m31D80E21BEE4DA0CA352FD5D3288099ECB9DFAA1,
	UnsafeNativeMethods_mp_CalculatorGraph__GetGpuResources_m6AB83B12E22D06B4542FDDF59E3849B48EF32009,
	UnsafeNativeMethods_mp_CalculatorGraph__SetGpuResources__SPgpu_mD550E6BD978D9EE8C9FCB458CB8C18E519DD77C5,
	UnsafeNativeMethods_mp_api__ConvertFromCalculatorGraphConfigTextFormat_mEC8C2CA29B20E95D6F69B5D01E7A25FEE1306050,
	UnsafeNativeMethods_mp_ImageFrame___mA06D1D408059353336E7DADD697E12AEE592B9C9,
	UnsafeNativeMethods_mp_ImageFrame__ui_i_i_ui_m80132AFA17D9384B6314144D18126A205B6CE629,
	UnsafeNativeMethods_mp_ImageFrame__ui_i_i_i_Pui8_PF_m951B34241067B94DD839F7245FB354B8CAEE2B33,
	UnsafeNativeMethods_mp_ImageFrame__delete_m633ABD20C43834CC5CFF3BB5AB772F49126A7C3E,
	UnsafeNativeMethods_mp_ImageFrame__SetToZero_m57776266CD9A1126948AB95987EEF125667BE2F9,
	UnsafeNativeMethods_mp_ImageFrame__SetAlignmentPaddingAreas_m9B581E7702727EFF945566989EAB34CA36029E73,
	UnsafeNativeMethods_mp_ImageFrame__CopyToBuffer__Pui8_i_m04B90E082C813A3D32C91D98F466DE61DBE09C9E,
	UnsafeNativeMethods_mp_ImageFrame__CopyToBuffer__Pui16_i_mF73153B91A88324107CAA664D57ECC18DA8A4929,
	UnsafeNativeMethods_mp_ImageFrame__CopyToBuffer__Pf_i_m5B626C3F783B9714F178EFD0560B2CAADCE82387,
	UnsafeNativeMethods_mp_StatusOrImageFrame__delete_mC4D146E8E63A9E69BE226B36EDBD0C7B0E257CF7,
	UnsafeNativeMethods_mp_StatusOrImageFrame__status_m8814246D65E05FEB29751A9228737687FDA2F43A,
	UnsafeNativeMethods_mp_StatusOrImageFrame__value_mBB34AC5304F76D102DA54AA9324D41386A850759,
	UnsafeNativeMethods_mp__MakeImageFramePacket__Pif_mE778FC600DFBAB5FEEA64D23594176FD404DB5DD,
	UnsafeNativeMethods_mp__MakeImageFramePacket_At__Pif_Rt_mEDE9F3DC30DD60B1F1B5D43029628162BC25E618,
	UnsafeNativeMethods_mp_Packet__ConsumeImageFrame_mFE66F0BB2E99E16A494E589FE93930D4B4F3EC2B,
	UnsafeNativeMethods_mp_Packet__GetImageFrame_mAFFEEEC5E9AE7853BCDBEF383C1E5773FF2A36B9,
	UnsafeNativeMethods_mp_Packet__ValidateAsImageFrame_mBEA2DC0EAC6996B6CF44F64ED525F7946F477997,
	UnsafeNativeMethods_mp_OutputStreamPoller__delete_mF80A88EB6F8AEEF3601FC8A9BC993B47CD0D8EDE,
	UnsafeNativeMethods_mp_OutputStreamPoller__Reset_m24569A5627802636296D36F6700ACCDB684E4C8A,
	UnsafeNativeMethods_mp_OutputStreamPoller__Next_Ppacket_mD2F54CDCB8BD9834FEA9E5CFD3CC8281CA4AECD8,
	UnsafeNativeMethods_mp_OutputStreamPoller__SetMaxQueueSize_m5E4C00650A5637BD5B58C6B1C6B20669C2C12F8C,
	UnsafeNativeMethods_mp_OutputStreamPoller__QueueSize_mB59D18B9A8C93F800BDB95705B0A7B7B5C5C7373,
	UnsafeNativeMethods_mp_StatusOrPoller__delete_m783FE25074696766E55842007A3DD8D5B3151D03,
	UnsafeNativeMethods_mp_StatusOrPoller__status_mB98DDA33E18CBF2A8918ACFD99E34D556EFFA8D1,
	UnsafeNativeMethods_mp_StatusOrPoller__value_mD44249D0D2D220280F8DAEC85AFEE3210DACECFC,
	UnsafeNativeMethods_mp_Packet___m4B27218CA7EAE8669B1445DBF18849A654B18914,
	UnsafeNativeMethods_mp_Packet__delete_m4259B2C7C37715CA2C47B439DCC152F4985D17EB,
	UnsafeNativeMethods_mp_Packet__At__Rt_m2CA92FA4A50BAA10DE9F8C740666E19B30F4FE96,
	UnsafeNativeMethods_mp_Packet__ValidateAsProtoMessageLite_m2B98615C611E32D9D9A74379B6B910936B0D99F4,
	UnsafeNativeMethods_mp_Packet__Timestamp_m689EF52DE8336D6936127AB4FB2F172241C504A3,
	UnsafeNativeMethods_mp_Packet__DebugString_m5453F6427CC9F8E19279E4EA657D0954F58D13FD,
	UnsafeNativeMethods_mp_Packet__RegisteredTypeName_mD801D6208E8FF92A78BC25FDD3F08B38C0221A42,
	UnsafeNativeMethods_mp_Packet__DebugTypeName_m3CF6B3782848CF204F773730E55C99A878A773EB,
	UnsafeNativeMethods_mp__MakeBoolPacket__b_mC3D4996305DDE3045E015C65FE8144E3B1DBA22E,
	UnsafeNativeMethods_mp__MakeBoolPacket_At__b_Rt_mFAC5E3C58B53F3E16C9190BADE565D6017608850,
	UnsafeNativeMethods_mp_Packet__GetBool_m2EC3BF6749448AA1575613423B66076567BCCEE7,
	UnsafeNativeMethods_mp_Packet__ValidateAsBool_m172217551337C215F886BFEE43BF596D9ADDE840,
	UnsafeNativeMethods_mp__MakeFloatPacket__f_m25C2E700AE041259CD826F002DF694B21A1CDA73,
	UnsafeNativeMethods_mp__MakeFloatPacket_At__f_Rt_m398C11302C7E534056394D2C0793D06643FE65F1,
	UnsafeNativeMethods_mp_Packet__GetFloat_m3DF61A639355E715A082DE0139AF4EC96FF00FD7,
	UnsafeNativeMethods_mp_Packet__ValidateAsFloat_m4E211787F621A579F30BEC34C7A22BD819A9DE6A,
	UnsafeNativeMethods_mp__MakeIntPacket__i_mD539128EF054E5532E7940C8FB4642262915B3F6,
	UnsafeNativeMethods_mp__MakeIntPacket_At__i_Rt_m5315ECFC5DDB21123C2AD6505C8C7BD04946113C,
	UnsafeNativeMethods_mp_Packet__GetInt_m279BC5C3E5B49E90E524E9BF7682FAF427D3C74C,
	UnsafeNativeMethods_mp_Packet__ValidateAsInt_mDFBB7CAF5C4460501B979651F006807B8FAD4B5D,
	UnsafeNativeMethods_mp__MakeFloatArrayPacket__Pf_i_mEAA64058B9C860B8011985E7C7A2A70B7D330C9E,
	UnsafeNativeMethods_mp__MakeFloatArrayPacket_At__Pf_i_Rt_mA760CF1527D55C13EFCF9D81A70D7DFFF5A68427,
	UnsafeNativeMethods_mp_Packet__GetFloatArray_m6FCAFB32B0AB5682A91C38FDB9113660BFC9D045,
	UnsafeNativeMethods_mp_Packet__ValidateAsFloatArray_m68959F5932769B7D9C01A3B339287059EF5F7051,
	UnsafeNativeMethods_mp__MakeStringPacket__PKc_m9CB04B7CC2B5980591C6DD4681E67931F0BCEBC3,
	UnsafeNativeMethods_mp__MakeStringPacket_At__PKc_Rt_mA35B8CB6736C0E66156E45128375581B0F83AADA,
	UnsafeNativeMethods_mp__MakeStringPacket__PKc_i_mD2991AB834F1A47CD5EB185B64EE75AF8651EA82,
	UnsafeNativeMethods_mp__MakeStringPacket_At__PKc_i_Rt_m32DDCC8982FFADEF18FE28C0CEAB7FA203598089,
	UnsafeNativeMethods_mp_Packet__GetString_m6A01C86A275F3A0DFE5A623DBA21D6C959B42D98,
	UnsafeNativeMethods_mp_Packet__GetByteString_m03B679BA78DCCE37F60AF59BC9EE7FF4BE1F1652,
	UnsafeNativeMethods_mp_Packet__ValidateAsString_m41ECBB9C8AF0ED93C63FC6DC390ECE79F7D1BA33,
	UnsafeNativeMethods_mp_SidePacket___m105DEB8D0482F8BEFE6B74880846081FCD914C30,
	UnsafeNativeMethods_mp_SidePacket__delete_mB0AB6C5D00E85998CE0781C3E5E774184652FE50,
	UnsafeNativeMethods_mp_SidePacket__emplace__PKc_Rp_m65F3E6A67163EABD3CC8534A1662D6AE1EADA1BE,
	UnsafeNativeMethods_mp_SidePacket__at__PKc_m44BE563F3160D472928CDE52BF897D28C4702331,
	UnsafeNativeMethods_mp_SidePacket__erase__PKc_m1EB44CE19489868A1DA2DE63C2157C96E3CD8FEF,
	UnsafeNativeMethods_mp_Timestamp__l_m05D27598D3A65F6F974231A155EE46E678070DBA,
	UnsafeNativeMethods_mp_Timestamp__delete_mEB3830A001AF05B5D8319E20471716AA7774D4EB,
	UnsafeNativeMethods_mp_Timestamp__DebugString_mFAD408E42CA854250F504DB2D4294AE8F3F94FF2,
	UnsafeNativeMethods_mp_Timestamp__NextAllowedInStream_mC7E9A0F316FFDFFDFD6D04890BFECB9DC55BB22A,
	UnsafeNativeMethods_mp_Timestamp__PreviousAllowedInStream_m7B02969BE69F1114CD9631E2236B743417C23179,
	UnsafeNativeMethods_mp_Timestamp_FromSeconds__d_m5804EC6D41E66A6DB34CBEFD8DFDA7BFF40104E8,
	UnsafeNativeMethods_mp_Timestamp_Unset_mD15DA5F84FC6FE6A1649313F714C63EB97985EE3,
	UnsafeNativeMethods_mp_Timestamp_Unstarted_m629139CD13BD3C6004CE0B30AF045B321610E65D,
	UnsafeNativeMethods_mp_Timestamp_PreStream_m8B9544DC5A11273F31DC7D4F0E0908BA15530EFC,
	UnsafeNativeMethods_mp_Timestamp_Min_m79F65301A63D19211BA106BB0364539DF07AD123,
	UnsafeNativeMethods_mp_Timestamp_Max_m7151A86A5B0D6CA573BB0E58C4F7AB15BB1D9679,
	UnsafeNativeMethods_mp_Timestamp_PostStream_mE373877C8728C18F15D7FC2EA3478374F882A4DB,
	UnsafeNativeMethods_mp_Timestamp_OneOverPostStream_m198726C8309D7E9DFD81CFD5A924109CBE638798,
	UnsafeNativeMethods_mp_Timestamp_Done_m0E665BE4D0BD4A62195A021E7575EFFE4028C649,
	UnsafeNativeMethods_mp_EglSurfaceHolderUniquePtr___mFA1960DBD060B9DD0714F9DFAE51C4D924541EEC,
	UnsafeNativeMethods_mp_EglSurfaceHolderUniquePtr__delete_mA3702C0E386123FD244B6B6779D532DDCBA56FED,
	UnsafeNativeMethods_mp_EglSurfaceHolder__SetSurface__P_Pgc_m7D9ADF0C33B15FD538BFE52B0C9E1D12335C42FD,
	UnsafeNativeMethods_mp__MakeEglSurfaceHolderUniquePtrPacket__Reshup_m61EA050A5C212CC77AF491C042B3EB21584B78A6,
	UnsafeNativeMethods_mp_Packet__GetEglSurfaceHolderUniquePtr_mCA5F9F06A0B3D1C8E16C458D3A6E90A95DC9CBF4,
	UnsafeNativeMethods_mp_Packet__ValidateAsEglSurfaceHolderUniquePtr_mF81CDC357FE5EA028BA317FCAF78B28516FC139C,
	UnsafeNativeMethods_mp_GlCalculatorHelper___mD9794B062CC2A2861E91913A68EBA24797832503,
	UnsafeNativeMethods_mp_GlCalculatorHelper__delete_mC36BC1ADF60A447B21C608334F90DAF714BDBB50,
	UnsafeNativeMethods_mp_GlCalculatorHelper__InitializeForTest__Pgr_m4E62C0E3703796B16C2EE5FE95D84350C0C46C25,
	UnsafeNativeMethods_mp_GlCalculatorHelper__RunInGlContext__PF_mD7818FFEFEE3F379478842EEBC4AF5AC99FBBA26,
	UnsafeNativeMethods_mp_GlCalculatorHelper__CreateSourceTexture__Rif_m08794BCA2130AF6D4CF01EA0787CEBBD5FB0C5E2,
	UnsafeNativeMethods_mp_GlCalculatorHelper__CreateSourceTexture__Rgb_mC62659CA1113C1599CE628C1682D568A6646E433,
	UnsafeNativeMethods_mp_GlCalculatorHelper__CreateDestinationTexture__i_i_ui_m955037C794329EB4F3C07F7198206F26EBCE8104,
	UnsafeNativeMethods_mp_GlCalculatorHelper__CreateDestinationTexture__Rgb_mA8F4C5AAF049376D6BD5C3D1EEB6C80E4492F420,
	UnsafeNativeMethods_mp_GlCalculatorHelper__BindFrameBuffer__Rtexture_mFF1C7E100E489A7C94A71F8367B2B33220E476A6,
	UnsafeNativeMethods_mp_SharedGlContext__delete_m516471F482EE3DCAB00F1AD7AA55FD85A03AA0B5,
	UnsafeNativeMethods_mp_SharedGlContext__reset_m982C4B36DF337D091A57B1535F351E82981CD103,
	UnsafeNativeMethods_mp_GlContext_GetCurrent_mA74C4F85595901A7D6A900E3AF34F0AE68C3E0D5,
	UnsafeNativeMethods_mp_GlContext_Create__P_b_m0163CB3DC08ECA02C9AB2EBBA6B28AB7242457B4,
	UnsafeNativeMethods_mp_GlContext_Create__Rgc_b_m47970266FC551B1062583F9FDDC0C6725B8C5CF4,
	UnsafeNativeMethods_mp_GlContext_Create__ui_b_mA82830BB2C8B7ECFFA412AD51528FCBE33C88E35,
	UnsafeNativeMethods_mp_GlSyncToken__delete_m134132AEBC805F6AF3957668AD15F9909C100DEF,
	UnsafeNativeMethods_mp_GlSyncToken__reset_mD8A9583F43E7F7D8F5CC3696450C605DC02C33A8,
	UnsafeNativeMethods_mp_GlSyncPoint__Wait_m31E26352E617FB81B42288C708295D0AB95F5BD8,
	UnsafeNativeMethods_mp_GlSyncPoint__WaitOnGpu_mD6A37C1C2ED9A7E1C23F1FFFF08F8A4D91BC79CC,
	UnsafeNativeMethods_mp_GlSyncPoint__IsReady_m4C8F9A53EF337E3BAAAF12B00B6EB747F78CE1DC,
	UnsafeNativeMethods_mp_GlSyncPoint__GetContext_m5136BE6D034EC4AC9EA35BEFFAD0F3541110998B,
	UnsafeNativeMethods_mp_GlTextureBuffer__WaitUntilComplete_m4233E6A8A9400BD9362A697949ECE362AD9A3DAC,
	UnsafeNativeMethods_mp_GlTextureBuffer__WaitOnGpu_m79EFB9905F183C292E68C34D93BA48867A3DFE52,
	UnsafeNativeMethods_mp_GlTextureBuffer__Reuse_m0EBB634379283DD74A401E5695298FD2DBBEA8C4,
	UnsafeNativeMethods_mp_GlTextureBuffer__Updated__Pgst_m4415F8C0D5D254DF566E81E7E17D65F6611BBE71,
	UnsafeNativeMethods_mp_GlTextureBuffer__DidRead__Pgst_m26D4FE5DD8591985820F7DDC07D8243FC05F6CBD,
	UnsafeNativeMethods_mp_GlTextureBuffer__WaitForConsumers_m158EF85652DCC48F4B34A3B23F7730CCBB54FD15,
	UnsafeNativeMethods_mp_GlTextureBuffer__WaitForConsumersOnGpu_mED94677CBF0899C734031EE6503B752124878927,
	UnsafeNativeMethods_mp_SharedGlTextureBuffer__delete_mBB962EAC919950600E9A4904B58333CAF4500442,
	UnsafeNativeMethods_mp_SharedGlTextureBuffer__reset_mA0236B1D07EE119BF809E6F8EDBE8CF978F02E1E,
	UnsafeNativeMethods_mp_SharedGlTextureBuffer__ui_ui_i_i_ui_PF_PSgc_mD32CEEB07332C225968A2E51ADE24F31EF0436FF,
	UnsafeNativeMethods_mp_GlTexture___m20BE7519E2CF13EA0206A14894C4432B6CEF1F23,
	UnsafeNativeMethods_mp_GlTexture__ui_i_i_mB10E1E2EA1848D47D096D9FCFD7AC2B84E5BB209,
	UnsafeNativeMethods_mp_GlTexture__delete_mE7BE0DD7AED9B9BFB8C9B80C295A71824ADAA3B0,
	UnsafeNativeMethods_mp_GlTexture__Release_m9D098B1E50A054D2EF6331D23AC28A2EAAD3A4CE,
	UnsafeNativeMethods_mp_GlTexture__GetGpuBufferFrame_mC9CAE089643F63706F7B9FB64FE9D56A425AFAC1,
	UnsafeNativeMethods_glFlush_m31511D22964C1C47F83A18E543AD163B2EEE80CD,
	UnsafeNativeMethods_glReadPixels_mB6CD520FF4A77019A700CDEB6646E61DED73EC85,
	UnsafeNativeMethods_mp__GlTextureInfoForGpuBufferFormat__ui_i_ui_m702EC5C6A8CDE05A30017A55B335D8F9814AAD20,
	UnsafeNativeMethods_mp_GlTextureInfo__delete_mD15DB3B56AA9D273779773AD51CD4C6B09D9272F,
	UnsafeNativeMethods_mp_GpuBuffer__PSgtb_mB8E0C0C78133F955B567785F8DF9E55A74748DFC,
	UnsafeNativeMethods_mp_GpuBuffer__delete_m074BA5E059620B6D6A4DF0724B7A9FB4F66A46CE,
	UnsafeNativeMethods_mp_StatusOrGpuBuffer__delete_m6A3EC340F49BEAA2300BDEB0C03317291DF0876B,
	UnsafeNativeMethods_mp_StatusOrGpuBuffer__status_m7C19FE633B1262D3116585E157D55B4AB19A4D25,
	UnsafeNativeMethods_mp_StatusOrGpuBuffer__value_m76C20DD7EC1309D1390043ACDDE80BF5B158996E,
	UnsafeNativeMethods_mp__MakeGpuBufferPacket__Rgb_mCC7FC6DB7296AFDB4D47AFEFD64C62311E4E1603,
	UnsafeNativeMethods_mp__MakeGpuBufferPacket_At__Rgb_Rts_mF6EEF02FF77BBE000B28E57FFC0D09A6C597AAFC,
	UnsafeNativeMethods_mp_Packet__ConsumeGpuBuffer_m89C334399B51256A2F44D1796606990AF4CA8C1D,
	UnsafeNativeMethods_mp_Packet__GetGpuBuffer_mF8BC0FC62B3F8B2EEF660D2ABDA9F03F728D711B,
	UnsafeNativeMethods_mp_Packet__ValidateAsGpuBuffer_m1D904C4703C863F78041212A3F652058EAB5968B,
	UnsafeNativeMethods_mp_SharedGpuResources__delete_m0BF26B12CB62E8B39C2BED656B9EE6E9CA52B7EF,
	UnsafeNativeMethods_mp_SharedGpuResources__reset_m77D76C52F21147E6E907A5D33EA785FE41BDBB9A,
	UnsafeNativeMethods_mp_GpuResources_Create_m81993A00DBB989A6CA6318779B2032FFC31A45F6,
	UnsafeNativeMethods_mp_GpuResources_Create__Pv_m72245C30025BEB85BF129709A9D8EF44142DE4BD,
	UnsafeNativeMethods_mp_StatusOrGpuResources__delete_mFC0AF350ACBA574EB87AB173FFFB45499E8B3D4B,
	UnsafeNativeMethods_mp_StatusOrGpuResources__status_m11FFFF3B4A64584B2A6AFDC566946EAA1A7C5FB8,
	UnsafeNativeMethods_mp_StatusOrGpuResources__value_m11B00CBAC6CBDC555D3EA34EC21289985BE45A5A,
	CalculatorReflection_get_Descriptor_mCDC5E5411464321E441FCA5E702FEB9B91F2053A,
	CalculatorReflection__cctor_mA59ED6EB26C0896EFFC8B607E5953F24EE892579,
	ExecutorConfig_get_Parser_m4061A739D47DCBF6051EC347E17AB3824F0309EA,
	ExecutorConfig_get_Descriptor_mBDD68DADEAA889FE416B0F65B6ACCEC4AE81DB73,
	ExecutorConfig_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_mBEC46DF521D3037149CD6112E6E58C12E1FD3FBE,
	ExecutorConfig__ctor_m3100C42DF28C84401E6BEF2F84829F2EBBC6A79C,
	ExecutorConfig__ctor_mD8F55C2743A66A7A21FF05F95AC46741582F3F16,
	ExecutorConfig_Clone_mBCF44B0A1985F000BBEC12B6FF68B32D57D3AB29,
	ExecutorConfig_get_Name_mCDD3B5CE8F632D79AEFE09086A0C5E1A6C1CD4AB,
	ExecutorConfig_set_Name_m030BE0B5394956C7D0BA0A7D0BA2623DAF4AB0AF,
	ExecutorConfig_get_Type_m9D4C633477ECD18E4CCAB745D9090547DBB11748,
	ExecutorConfig_set_Type_mD55EFA000FE493B28505A7CB9B256722B1154F24,
	ExecutorConfig_get_Options_m47FB86097FA6BE4FA3F16E2C9548046A5A08760A,
	ExecutorConfig_set_Options_m2F020AF03F033A468C9788B097981EDBB54DE2F1,
	ExecutorConfig_Equals_m3951D8E5FFF9897153DCBB93E5DE556807BBE51D,
	ExecutorConfig_Equals_mFD6B3AC46F2086E4F1F526630E167EC0C8543318,
	ExecutorConfig_GetHashCode_m7BDD5B881BD685E1ED277BBBCFD80C4986FFE164,
	ExecutorConfig_ToString_mF0A8963B6D15984AFB1E8BFF76FABB4F9F173917,
	ExecutorConfig_WriteTo_mD1274E15A379E9C0DD0C28CE7EB63EAAA74C67D4,
	ExecutorConfig_CalculateSize_mA55EE68840B8DD1CC6F4AE0974D39CFC0AA61E9C,
	ExecutorConfig_MergeFrom_m5C5312A118CD794BA85F9895A164F3B9D27864D8,
	ExecutorConfig_MergeFrom_mB9D3A9DC69128998567149232D6167740C1D86B0,
	ExecutorConfig__cctor_mB6A2622F6DA7E9C26FD5784793F08474FA116C8F,
	U3CU3Ec__cctor_mF55D6DAFEBD23B5E894241270D8ED12D312F6AC7,
	U3CU3Ec__ctor_mC2141556671D9CE21521511489272A6D16177989,
	U3CU3Ec_U3C_cctorU3Eb__35_0_mB2CC60AD96D63C4FE75FD13C750E41EBEE101412,
	InputCollection_get_Parser_mE378D9F9332EFCF03788A8188CA64ED6FB1F7F30,
	InputCollection_get_Descriptor_m53F257A5D4B3C8390B661C52C9BDB22198A32FCE,
	InputCollection_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m5B17F58E47F5B31C88D78D46FEB89225FDF83680,
	InputCollection__ctor_m6C6B1C4710314CCF5B8B7924C90E035C6FAB2DD8,
	InputCollection__ctor_m0D27217EBD9D8F66693BFD6643F490520429AA4D,
	InputCollection_Clone_m1B45CE9CCB12659381144DB30162D6975759F471,
	InputCollection_get_Name_mDE372674C6096FF2DCCA764D10D98D2FAA5BC3F6,
	InputCollection_set_Name_mAF13645B09F23DA5A9BB95AE01316CA53A9954EF,
	InputCollection_get_SidePacketName_m6F5B9464B72B54C2786C5EB8DD78FECC3D5CF2A9,
	InputCollection_get_ExternalInputName_mBF5636DF6433CB626FA7B198305E9A315AF0A540,
	InputCollection_get_InputType_m01D40EBAC557D3A3440C1E8FC084866F8CC2E7A7,
	InputCollection_set_InputType_m046259B7E53CF8711B283C6B32EF40644A50F54C,
	InputCollection_get_FileName_m37189943C7703CC0BDEFA64D296DC0AD416E0B82,
	InputCollection_set_FileName_m90482CDBF38046F894E3EE6816DF83DDDAFC3F85,
	InputCollection_Equals_mFED987F83BABE5D3B89DDA35313DD18A2746CEBB,
	InputCollection_Equals_m240639A5CB435A8E02FD15D41B30E9AFB7C9D2FB,
	InputCollection_GetHashCode_mAECDBE9B9032BA48A73EC8D59C4ED5F1D473E5B7,
	InputCollection_ToString_m7FA0413087B5A0AD62F527DD3F15ECDCB9981DEF,
	InputCollection_WriteTo_m606B5E61E0DD7AC8D05F5C64A3EDD68625ACA63C,
	InputCollection_CalculateSize_mB74CC608EB1D546473A2A7D93F6037CFA54E60FD,
	InputCollection_MergeFrom_mD3C06AAE9303FC3CD361361748B7CD0BE5C46D38,
	InputCollection_MergeFrom_mFADA258A81187638466BFEF0AFC6C8351A94374B,
	InputCollection__cctor_mBBE11626BE60F7C2C8892719A0DACC27BE20143D,
	U3CU3Ec__cctor_m775626A98B13AA2C3AF7D4B66B3BBE5B35960637,
	U3CU3Ec__ctor_m4519A711962554F775A3B093EABF5B4C3AC7DB6E,
	U3CU3Ec_U3C_cctorU3Eb__46_0_mE37B8A95569CD0FC845A9D68DDBD2612B13205DC,
	InputCollectionSet_get_Parser_m3DEC565028824E563F99C174DB57A62670D0A5BD,
	InputCollectionSet_get_Descriptor_m234566FE3F857EA8CF17D175DA30CCA9D7BA74DA,
	InputCollectionSet_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_mECA590FBA2BD56B8274FCB46219A1B321EACB1B6,
	InputCollectionSet__ctor_m106104988333DB6E965B4966687686D1085762F5,
	InputCollectionSet__ctor_m5B651C10C9348EA4E9BAAEED0053D0D7A17CD82C,
	InputCollectionSet_Clone_mE409D46CDF2E45AFCF1BCCFA1245ED50361A972D,
	InputCollectionSet_get_InputCollection_mC05A051CC8BCA271CFE62488B2B8C053955619DD,
	InputCollectionSet_Equals_mD0F5B94B7B317360CA85154981CC2B5193459D97,
	InputCollectionSet_Equals_mD2DF8C4AB1E38FE7EB5F3AE5991D7C2E7B686019,
	InputCollectionSet_GetHashCode_m6AC079DDE2E44C5212E7E62F1FF43C86B2B37D5E,
	InputCollectionSet_ToString_m9CABAE3D201DE47B317FA7DC708336FFF7E16607,
	InputCollectionSet_WriteTo_m929290F3104395CB0869AADAC6EAC99A02107476,
	InputCollectionSet_CalculateSize_m3035110C513A4AC4869481684000D6E163838F20,
	InputCollectionSet_MergeFrom_m57652BBC66362FB0B8E52A4C24A972069DCA12A3,
	InputCollectionSet_MergeFrom_m3ECEAC1E1ED1FF19AE13930016EE8AF9744C6C32,
	InputCollectionSet__cctor_mC175F4CC4961FE477BC6F94B31F6777BB2B564A0,
	U3CU3Ec__cctor_mAC32A0CDE2B86354240EF13EA670CD7AE3FB5F67,
	U3CU3Ec__ctor_m0757AF253EDE03DEEA8E6285493C7BB08EC02170,
	U3CU3Ec_U3C_cctorU3Eb__25_0_m5AC5917E1E50A6FB568D1FA34C9EFD309AD37692,
	InputStreamInfo_get_Parser_m29EEEEE0106399720E60958EFE1DE729B252297B,
	InputStreamInfo_get_Descriptor_m522BCB77CE097CFA0B2926952ECAC0B025EF1257,
	InputStreamInfo_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m1C7871CD59A7B9AC8F5FEEA72AAF44A9C903584C,
	InputStreamInfo__ctor_mD1115B096AA44CBE3BAE01F934CB1DC0E2C8AA78,
	InputStreamInfo__ctor_m1B40C1CDA73DFEAD87E29B99BCEA761AE4C7A703,
	InputStreamInfo_Clone_mDFE3D66297DD000E2BA4F70D7856F18919BB7D44,
	InputStreamInfo_get_TagIndex_m2DFDC9D8A61379DD1F3DF69E0412B7CBE5DEF833,
	InputStreamInfo_set_TagIndex_m499BCC10AE86C87A0E24617B6CEABD0CFB18FF2D,
	InputStreamInfo_get_BackEdge_m7116667E66A9D5F7E01BE058DAF8D6D0875B75E2,
	InputStreamInfo_set_BackEdge_m619978F90472531E677D24A0DBE7A2578DA1D868,
	InputStreamInfo_Equals_mD3672C103FBD9DF61B4EB0D8DD7B63EF8863A7C3,
	InputStreamInfo_Equals_m936804A131005237445BABB5F4EDFC06C448B018,
	InputStreamInfo_GetHashCode_m3AE4587C347B5910CAF5CC12E662B2064136D676,
	InputStreamInfo_ToString_m7F4E5F79EAC6EFBC730259B570B113870ACEA553,
	InputStreamInfo_WriteTo_mD01FACD335355F68B1D530EA10BF2E346EB45567,
	InputStreamInfo_CalculateSize_mD3CDD8A026DEA91C9FE050645038DD708196E41E,
	InputStreamInfo_MergeFrom_m9AE0F0C70340F4B1399211E4364CC883F752D325,
	InputStreamInfo_MergeFrom_mD104C197A6E64A40CB866B1ADC51E93F42761B5E,
	InputStreamInfo__cctor_m49F60A4A40008C1C7DAEEC941126B8D134974888,
	U3CU3Ec__cctor_mAA9349BC4F34FC226908D2CFAE77AC9D0D85E723,
	U3CU3Ec__ctor_m6068A300BACB163E7D442C7FD0FD6A809D9E7A78,
	U3CU3Ec_U3C_cctorU3Eb__30_0_m6DFD9BB113EC492435F9DA60F2655AFAB899D070,
	ProfilerConfig_get_Parser_mDC3680A806A01DC409BA9CB9CB0E0F0E82FD098C,
	ProfilerConfig_get_Descriptor_m45D4BC75A233D5863820E0519FBD549ACB4A5C8B,
	ProfilerConfig_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m354E93AA3D4D9902C2302405BA82693B9C0E80C5,
	ProfilerConfig__ctor_m023B7A29789ACE07320D0B31C8CA499115617D79,
	ProfilerConfig__ctor_m262E9327CC6CA09F9622D776162028033D1BE963,
	ProfilerConfig_Clone_m9412AE0F8B8C1AF838F6267D697F1ED44148B3D5,
	ProfilerConfig_get_HistogramIntervalSizeUsec_m6D91E433129C5662A8BFDC98BE3E1E12FD2B0CF0,
	ProfilerConfig_set_HistogramIntervalSizeUsec_mD21DBA09EE8366CEF3D89A419A85EEFEF5355C9A,
	ProfilerConfig_get_NumHistogramIntervals_mB094148194DDAD421C145C61F8AAC4BE518EAF0B,
	ProfilerConfig_set_NumHistogramIntervals_m63112C698F0F95060925ADBC162C0D3D1D3C10C4,
	ProfilerConfig_get_EnableInputOutputLatency_mEBD9F1BB647CC19BCBEC981FA54C22CECDC9536F,
	ProfilerConfig_set_EnableInputOutputLatency_m0D0C6D17E5901859534BC1C76E9ED8C87FF00A1B,
	ProfilerConfig_get_EnableProfiler_m6370DE235049E6426DA0950488C35C4A2C199949,
	ProfilerConfig_set_EnableProfiler_m3D3EAB2ABB953A6A48D76EAFFD5407F7D7B27408,
	ProfilerConfig_get_EnableStreamLatency_m98BF49C9827E6A20CD87B8A6224C671FC9EDE895,
	ProfilerConfig_set_EnableStreamLatency_m8ECB2C951B166BE71D746D1B04B4ADF63DBA2151,
	ProfilerConfig_get_UsePacketTimestampForAddedPacket_m18282A570D68BD141087F3F2D3FDDD7DA2F580D4,
	ProfilerConfig_set_UsePacketTimestampForAddedPacket_m0CCCCBDDB2434D8B2A8DC7AE3462289C12F3D0CD,
	ProfilerConfig_get_TraceLogCapacity_m33F9AE0545DF9C0E5CE9BBC60A6A6C5AF744FB65,
	ProfilerConfig_set_TraceLogCapacity_mFDE761A2BFB9A850CAFA2FFC9B229B7F7C130612,
	ProfilerConfig_get_TraceEventTypesDisabled_m138212D8581E0D2A4F1AE7D5554CB319107D2437,
	ProfilerConfig_get_TraceLogPath_m8A2EE82AAC47E9EC2BC0343DA658940E9BA6D1AE,
	ProfilerConfig_set_TraceLogPath_m38F963E88EBE4BF9CC9FC459BA314A056AB3082B,
	ProfilerConfig_get_TraceLogCount_m05C36326E91640453E292A0F8F2AB5437026B071,
	ProfilerConfig_set_TraceLogCount_m239C502EB594621DCB8B0384DED343B7C9761B73,
	ProfilerConfig_get_TraceLogIntervalUsec_m9793B4062D176C4063A08813D4914FE6DC1BB943,
	ProfilerConfig_set_TraceLogIntervalUsec_mA68DC25CF8409BE740B33AAAD986DA96E5CFE961,
	ProfilerConfig_get_TraceLogMarginUsec_mF9B6487ADDF8B789026A27F3CE43BCB86B3A6695,
	ProfilerConfig_set_TraceLogMarginUsec_m54122925CADA5CEF944B50BD8EDD09F5B8B09A95,
	ProfilerConfig_get_TraceLogDurationEvents_m8C35A76E9947A90E9469ED45AA523DC5E5D54BE9,
	ProfilerConfig_set_TraceLogDurationEvents_m0D3E0D43649B7C7F2CF18B6FFF2D520EE7DD9347,
	ProfilerConfig_get_TraceLogIntervalCount_mCDAD806631E8824097362B69D313800F629D81FF,
	ProfilerConfig_set_TraceLogIntervalCount_mBA8BC1B5CCF34D31AF7503DC9426317E3CB68F27,
	ProfilerConfig_get_TraceLogDisabled_m04F92FC63CCC927214FEA89C9CE5A07F6B353E54,
	ProfilerConfig_set_TraceLogDisabled_m1A04F77DA8620C6AB5C0CFF6A2027EF882B3495B,
	ProfilerConfig_get_TraceEnabled_mA8A9B99D538EF79A8B1DFC93CA2C48A4DEDD251C,
	ProfilerConfig_set_TraceEnabled_m8DC1CBC71241FBF4C147B2B9FFE8423A505A0761,
	ProfilerConfig_get_TraceLogInstantEvents_mB12FA539BAA5FD750193D355F42FE76E81FB9BDC,
	ProfilerConfig_set_TraceLogInstantEvents_mEF5BF40D40A904DF92022204AD51FCDE5C06C840,
	ProfilerConfig_Equals_mB3F233DC0F6A2B5D3A0A3E9E77F192F684323BD3,
	ProfilerConfig_Equals_m8F7A20EE6CD5D39FCC8B178E93A994BE8E2E49B2,
	ProfilerConfig_GetHashCode_m54E95CD9692BE7C31B86676C97728DD0E633F982,
	ProfilerConfig_ToString_m3B748ED748B01176BCB4494E2D750DAD217AC2A9,
	ProfilerConfig_WriteTo_m358D63D2EC966F259D61B184EFE314BD91EFC003,
	ProfilerConfig_CalculateSize_mAD4E24F9EC25953D971CBFDCA5B9E122B8D29EED,
	ProfilerConfig_MergeFrom_m832DCC37551A1090377D636FBF3741DD738508C8,
	ProfilerConfig_MergeFrom_m83749E2EB94F44989249949BF89412B0BE6EEF1B,
	ProfilerConfig__cctor_m67023CC8F429220EC22D3415EBA8A5F458FA5C78,
	U3CU3Ec__cctor_m790ED7413C6278199A79F0F96D1EC732307F5A67,
	U3CU3Ec__ctor_mD6DCE79502F7D20A567B9CC11BAB432934475D42,
	U3CU3Ec_U3C_cctorU3Eb__105_0_mF0A38142138B6391679CCCD4E7C69A42D23B1403,
	CalculatorGraphConfig_get_Parser_m4A079EC8E8CAAD2D0103348A0B7BCDB227430C32,
	CalculatorGraphConfig_get_Descriptor_m8D7BBE111C92901AE6C097AD989429407C1F665C,
	CalculatorGraphConfig_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m06B01AB068DC73424EA701B7C22D5F72E1B71561,
	CalculatorGraphConfig__ctor_m2B2B9C3D725A811963E37BFACE3320D1B01AAD6B,
	CalculatorGraphConfig__ctor_mC7561D31FBB0998EA48446E954D238BB32F6D258,
	CalculatorGraphConfig_Clone_m17FBA506A44FF42CB08BBA16B66D753A23FD7F8E,
	CalculatorGraphConfig_get_Node_m321554CDAD36FEDC9405A18B6A66EC9BE760ED0B,
	CalculatorGraphConfig_get_PacketFactory_m273E2DE8AC98B7F5CD1AD128CB48AEE53D72A88B,
	CalculatorGraphConfig_get_PacketGenerator_m69B3E7B6CB9B1493B8767D1C3AC0BB55DF38F977,
	CalculatorGraphConfig_get_NumThreads_m6A39F58BDA93AFAE00A004C84E3493636EAF87F5,
	CalculatorGraphConfig_set_NumThreads_mD0064766BD1FE929CC790875C973DC449C8C432B,
	CalculatorGraphConfig_get_StatusHandler_m743365040F37E3C159D6BECE1663E0D6E9F1B9D4,
	CalculatorGraphConfig_get_InputStream_mBB8EA0317D48A82B675083F8B9051A23AC0A3F44,
	CalculatorGraphConfig_get_OutputStream_m3BD29DC90D9B7A21DDE753EF4ED1E3838D9472BA,
	CalculatorGraphConfig_get_InputSidePacket_mBC81A12B242533F7A6D150F599C3C9D65CDEDEE2,
	CalculatorGraphConfig_get_OutputSidePacket_mABD36DE826A9956AB94F62ECB0869787802DE7DC,
	CalculatorGraphConfig_get_MaxQueueSize_mAA37C40F789D6FD921090A133602AA7C623855FA,
	CalculatorGraphConfig_set_MaxQueueSize_m4D8AB2D205D8AFC0FCB0372935FF294608D8A3EC,
	CalculatorGraphConfig_get_ReportDeadlock_mD7E9AA636A64D20F511DCA8DA40722006E60ABCF,
	CalculatorGraphConfig_set_ReportDeadlock_mC86981168F02A6C2B86CEAD791ADE82F37BB22D4,
	CalculatorGraphConfig_get_InputStreamHandler_m3D3C8636240AACE7647CCA94C9D7BDCC0311A1D4,
	CalculatorGraphConfig_set_InputStreamHandler_m38F86ED4E5D6A6FD8C6CF0F6AF0C7507BADC137A,
	CalculatorGraphConfig_get_OutputStreamHandler_mC802C414AB6F0D4BF2F154FD58814D8BE70948CF,
	CalculatorGraphConfig_set_OutputStreamHandler_mAA9AAC5111D30D2D0BA230E7D05C6DB295CB2C3A,
	CalculatorGraphConfig_get_Executor_mFB5C9FA0B856C97131B7A3C2C0BC8F995D779BF4,
	CalculatorGraphConfig_get_ProfilerConfig_m983F9645276D8632486E927C5429AF98558B4A60,
	CalculatorGraphConfig_set_ProfilerConfig_mB1FF19F6A1B86A3D51E5B09663EC1A41555ACDEE,
	CalculatorGraphConfig_get_Package_mAB118379EA357AD0EF33CBA05CEBD93657E5BCEB,
	CalculatorGraphConfig_set_Package_m0D1CA3C3DB1518FECAB2F2FE03F689CAD6C54EA5,
	CalculatorGraphConfig_get_Type_m31C5992F7EC58444CD9558F43B966B065959B42C,
	CalculatorGraphConfig_set_Type_m322E27DA217F05E23F477EB05F65EBD02E9481F5,
	CalculatorGraphConfig_get_Options_m71FCD390FB456C70704A92CF88DAEE4E76AD5EB3,
	CalculatorGraphConfig_set_Options_m13AD3E5B15DAB480E5A357BA396E1ED9EDDDFE48,
	CalculatorGraphConfig_get_GraphOptions_mECAFC93F8B1268731A1707A6ED6FFC1884AC8D82,
	CalculatorGraphConfig_Equals_m64171AE8626030825A893CF84181F3DB88686A78,
	CalculatorGraphConfig_Equals_m34E76CADA083771B042327F2E6A05098D82BC128,
	CalculatorGraphConfig_GetHashCode_mB45EBF4282A3719E6557603FD57749C19E2315C2,
	CalculatorGraphConfig_ToString_mA48B2E2730D472DDD57D0D36B0A0350E05BEE497,
	CalculatorGraphConfig_WriteTo_m0937B7E7FB01FA089C7EABB73DF1ACCCDB08D5EC,
	CalculatorGraphConfig_CalculateSize_m47318B281CC424A1787F9864750F2A6056BA8EEC,
	CalculatorGraphConfig_MergeFrom_m6A80FA1E5F6CD141888C842AD93B8979616CDB2F,
	CalculatorGraphConfig_MergeFrom_m75F230C7A43154402BAF91F2D7837F2D0C79BA4E,
	CalculatorGraphConfig__cctor_mD017EEE41436B98F035D39AD96FF27B6712B7DC9,
	Node_get_Parser_mF9303F20AC373AE8D8C58D0154E2DC2CEB82D5E2,
	Node_get_Descriptor_m6CFD9547D6F0F7ADAC34C66B7A492A7D95FB2AB4,
	Node_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_mE6DF0D8937057AA16D69FC87BE227924E24249BD,
	Node__ctor_m5A45C5EA9496703F28618D3FDCD3D9603F43A1C4,
	Node__ctor_mEF6F5AF4FEF24FF0B17E590D1A99D89ED393FFD4,
	Node_Clone_m4A14B5A6E780D1C69310984C336E4E8A91214084,
	Node_get_Name_mE894E2493AAADAF47A6ACFC738E7E94394E92030,
	Node_set_Name_m8DFF5AA51DB6CA486897A1AD8EB12023A5304A19,
	Node_get_Calculator_m463A3226E13F44A829387F15848B98FC4A16C1C7,
	Node_set_Calculator_mC243D9CA49A3FB38A38C0933167CB9CDB46FC711,
	Node_get_InputStream_m9EB8711512F3633C6BD12A4A0F6112AA8DFDCE27,
	Node_get_OutputStream_m25F9F90483C30DE82663F105D859B577F6080224,
	Node_get_InputSidePacket_mF7C54227234D3F8C8528FD3EBAD858D13496B22D,
	Node_get_OutputSidePacket_m359AFD1F4BED5CD417EE684572BD56646A435B46,
	Node_get_Options_mBB7686196682BE9E2D6AA4FF11935614E986DB7F,
	Node_set_Options_m76F61CD0BA18EAF0EDEF0206766E3A3389F538D3,
	Node_get_NodeOptions_mB3CF30AF1990E0FC4D3E59FB3202AEBC9F941B48,
	Node_get_SourceLayer_m97D929C5A0664473E9D33926576185747B896168,
	Node_set_SourceLayer_m4BA33A971551752CC6538799F21C042183F0D21E,
	Node_get_BufferSizeHint_m6CE45C732EADE72554171AFC981B89AF38B75D8B,
	Node_set_BufferSizeHint_mEE66DD42B144153E83E60EADCF0F1F0DF983A47A,
	Node_get_InputStreamHandler_m14731A3E0A58CB1461B29F0B3EBC409DF9DD5F68,
	Node_set_InputStreamHandler_m64D09F558F91F81782A5880C38AA90C176FABAE9,
	Node_get_OutputStreamHandler_m1B9F69D2D3E527A8DF6A9B112E47761326287E5E,
	Node_set_OutputStreamHandler_m68BAFF770395CA21744A219A0C2151210F96FAC8,
	Node_get_InputStreamInfo_mAA1B70E6EFDED9D84D9974493E3B2EAC855FC811,
	Node_get_Executor_mBA165D27539BF3A41818F70D820AADE881BAAC2F,
	Node_set_Executor_m2EA39E8873CE41463AAFBAE6D649B7D875982C79,
	Node_get_ProfilerConfig_m7B4BB9BC3DEE985FCB1F3136BB3910935463A41B,
	Node_set_ProfilerConfig_mF0C2D1AD53CDDC63846F085DF9CD5E88FD901E51,
	Node_get_MaxInFlight_m59FAF8A41BCCDA9D0317A51BD8A23EAF250D45B0,
	Node_set_MaxInFlight_m1215966893B59017C75BABDFBFD6BB7CA53E5FE8,
	Node_get_ExternalInput_m730411592F0348DC416C05E41B00742BEF6CBE86,
	Node_Equals_m63B25BF70104CFF3B8CB2C3A3E9DFCAF0D225024,
	Node_Equals_mCAA8CD4AAA40FC247C882742F39D72EE8EF7C59B,
	Node_GetHashCode_m426A32AEA408311AC8B43458F2470D78F00F996D,
	Node_ToString_m6C22CB185110B11818FDB6AE1D71D1DF73DD72F7,
	Node_WriteTo_m2EAD9127103E77DC06897434BD2F8E070F00E0B8,
	Node_CalculateSize_m2DC063FE5B6CBB1980AA72F0AA9D7564C541C6DE,
	Node_MergeFrom_m1D3E147794EB001C4E41FECDBD9EDA04D1803378,
	Node_MergeFrom_mD3BB28988C717504CE6F43642FF64E4EAF34D512,
	Node__cctor_m0900A6CC307EEA0D0C17645D288DA0F1A84B1878,
	U3CU3Ec__cctor_m6691D8B8CB759B58A74F3BE7BDF3C07F45DE956B,
	U3CU3Ec__ctor_m0317D08A4EE3CD97EC4391D557A983CC5CB2BA52,
	U3CU3Ec_U3C_cctorU3Eb__105_0_m2D64BA391FF1273A1B4191466C9A88B866AD20AC,
	U3CU3Ec__cctor_mE98B01E5F1D1FE7993C97A269FA57D8D15EFDB5E,
	U3CU3Ec__ctor_m6130553500819B4B8B66AD28388D63ED6E86FD16,
	U3CU3Ec_U3C_cctorU3Eb__116_0_mFF593F9603CDCFB39AC1E7420E4C59FD7FB80B9F,
	CalculatorOptionsReflection_get_Descriptor_mF20E4E31A159BA940BC5DE49E78A9E78C49C509E,
	CalculatorOptionsReflection__cctor_mED04E49048569790FA0216FC4F03B95556ADC6DC,
	CalculatorOptions_get__Extensions_mE3A439A85F848172CE7F64F8874C64C06B8E3DEA,
	CalculatorOptions_get_Parser_mC4CDAC265B77C1107C13C864C75ACFDF5A9FF065,
	CalculatorOptions_get_Descriptor_mA68C7E9135E7BAFC404E03C5EA5D641EFE9B1290,
	CalculatorOptions_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m0BFBA8271EF58F841A7563515256852FBE2F4BDB,
	CalculatorOptions__ctor_m9A66335BA5FAB6F4F057182BAF253B3A586C55FA,
	CalculatorOptions__ctor_m022E05735F6CE2036FC8135812D6AFAF853F2D16,
	CalculatorOptions_Clone_m717403D7365C851A4F8558834B83CD542CEE0571,
	CalculatorOptions_get_MergeFields_mEFD21DBB2DF9935DB3A43AC109265E0F81CB1A2D,
	CalculatorOptions_set_MergeFields_m1D48D2A31F24665AAD61B8BF1D7366DB1F5368DB,
	CalculatorOptions_get_HasMergeFields_mDB7E473865D6C54CDDB7D25808BDCD53C9830F77,
	CalculatorOptions_ClearMergeFields_mFD3A53D320B135AFE511B684F9919729200C090D,
	CalculatorOptions_Equals_m1A6E5C0DB2FEB400FA0D5F76665FA4F37BDE8B03,
	CalculatorOptions_Equals_m1511C43D99252F232CAF2CE903A223A9B7924B87,
	CalculatorOptions_GetHashCode_m5879C42E02EDE756A47DFDE711951052D2BBF4D1,
	CalculatorOptions_ToString_mB5C0791D4DB12DAAC8A21F528A1F1DA7BFC06ADF,
	CalculatorOptions_WriteTo_m55AAD179414854C4BD37ED1895396F9EBBFEB1E7,
	CalculatorOptions_CalculateSize_m9612BCA7552905D230E60EEAAFBAF60DA3767572,
	CalculatorOptions_MergeFrom_mB9BBBF0230136216E2CB33D75343BC9CBD5EDE97,
	CalculatorOptions_MergeFrom_m8F46A283DBD6012F2F985C795D06856AD6CD0E72,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	CalculatorOptions__cctor_mF9865E242081176ED6E2A94CBA6753CD693436E0,
	U3CU3Ec__cctor_mA0E14D23F686BCB83934F9854B23DCEC99A64551,
	U3CU3Ec__ctor_mC6FB7BDDD3FE3C4369F030777A5E466797FA4DE0,
	U3CU3Ec_U3C_cctorU3Eb__40_0_mFD0888555A378F870EF0328048AD9049F02310B5,
	ClassificationReflection_get_Descriptor_m78DA35CEFDADB39F032CB4D99EA9793A989B1497,
	ClassificationReflection__cctor_m101442F33A51F897212863098A5C46FE43E6D64D,
	Classification_get_Parser_m9991AAB25C6135C1B640B62FEDFFE2DA1857227F,
	Classification_get_Descriptor_m4CAC2D180BF16B61FA4C5E9CE3133EB293E521C9,
	Classification_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m305A4A472F71460AB4A2568C30080DB74B512AAF,
	Classification__ctor_mA0FD3F6BC90023C990B4B88F09F9CA77B8DBF669,
	Classification__ctor_mEA745239FD6942EA8EBFCC2C66AA5C12645FD828,
	Classification_Clone_m75EB64A47676D4684C1EF7F86F3053F9360ECD5B,
	Classification_get_Index_mFE79FC35324E0D817C664BDD0BE2F28CC1F350E1,
	Classification_set_Index_m2CD1CA4910F5EF83DA679F897E99D253160239DB,
	Classification_get_HasIndex_mB9C7ADCAEBFA0B0F6EFC3E044B66650D4520709E,
	Classification_ClearIndex_m19867CF6E45A80A42275612C11C9AAAE7B684CA6,
	Classification_get_Score_m6CCD9B79AF875C575F96962C002A8566EFD48129,
	Classification_set_Score_m547A53C0E75905C5C480A3FF93AB8C7564FDE6C5,
	Classification_get_HasScore_mD6871B3DBB90DCCEA9FCA2B0058C6DA7384AF390,
	Classification_ClearScore_m16E3BC42F4227EB6B6ECADA43520EF7363DA1278,
	Classification_get_Label_m969CB2659140B662BC7811109153905CE067E5C6,
	Classification_set_Label_mA88854B632D97297FA3F3DA8C3F54278FAA2415D,
	Classification_get_HasLabel_m97DB7171CA85E47C60E2976C6D05C28AAEF7C72E,
	Classification_ClearLabel_m6F8D5E7CF22D2A405DA815F24248399CDCA66134,
	Classification_get_DisplayName_m664B8F81ADA4E88E59E8ABF475C6531255034C38,
	Classification_set_DisplayName_mB976A5F49F03AC4C602AF90287E7826493EA0935,
	Classification_get_HasDisplayName_m92482B43EB3E4D20E7A9997C53A69CB92E11FBE5,
	Classification_ClearDisplayName_mAA08FE13D01E327D673D03547E9AAB60250C59C4,
	Classification_Equals_mA50D3D642CB1069D2540F7FECA301BCB9E7ABF8F,
	Classification_Equals_m46491A3E7C368EAD299737A771C47E78F8744E88,
	Classification_GetHashCode_mC63AA2FB66A9CB60B0969C8410E40986ED657247,
	Classification_ToString_m6D17450C980C2A8E9082FC523C0A78943D27E6F0,
	Classification_WriteTo_m53061790010FE08E18DB482140160B2063EB583C,
	Classification_CalculateSize_mFF773292996625892D0F77C07A43D0B65C7C931D,
	Classification_MergeFrom_m8D981270B6DB8836F391140E0597189E1FCC74E3,
	Classification_MergeFrom_m928242B2CE6F22D7577F5ACB8A01B507B34995C2,
	Classification__cctor_m529CD66124B6A29D2F21337C4E9441621CFF5338,
	U3CU3Ec__cctor_mFAC5FD5F4F89E867E169C84412DDC0A3FC50BB93,
	U3CU3Ec__ctor_m71F748620202B31A7FAF60806A2D7B340999BE9B,
	U3CU3Ec_U3C_cctorU3Eb__57_0_m28BAE58C6629166341F472CFDAB8D83DAEF7BDD6,
	ClassificationList_get_Parser_m1C0072871396300B488BBE724F48C1AE9EF52CBE,
	ClassificationList_get_Descriptor_m6289F12482426E42F5A3784CB4F956036ECF8C16,
	ClassificationList_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m993852CE5EC970083F2B7CC7E1294261153A32B4,
	ClassificationList__ctor_m99E71A343606FDE18A16A30431F3FB10C8E85400,
	ClassificationList__ctor_mCF0BED5C2EF4578C32BAF890A31DF4548E394480,
	ClassificationList_Clone_m1BEC3137BD0BD0ADC62E511E6D6C00FC616B30A3,
	ClassificationList_get_Classification_m93E3EFFC59DD0782B82B986DF08D5E6D8B0C765E,
	ClassificationList_Equals_m2EBF80429667EEC0655AFE46B55349A6A4ECAB79,
	ClassificationList_Equals_mC9CB38807A2EC3B87251459B9C540C3DB2D8AA91,
	ClassificationList_GetHashCode_mA2375D105268BBA492A629CB6D74F776ED246C01,
	ClassificationList_ToString_m8A6C7A594C9DA6CFAEB291FAF1C4419350870DE2,
	ClassificationList_WriteTo_m5C28B1914E8CBA3CDD0CC26E009A49FDACE1E1B1,
	ClassificationList_CalculateSize_m06E066709F39339F2F9880D3EB7A9CCDD1950F88,
	ClassificationList_MergeFrom_m771CE3B97011B6A02527AA5F8AB056CC280A4759,
	ClassificationList_MergeFrom_m11F8284D369551729C93A32BC16394DA6990280C,
	ClassificationList__cctor_m115DB60C39CB0C78C2E3A7D9E9FA3F8185E3688A,
	U3CU3Ec__cctor_m473E5C6703238A211F37B203C89988DE76245581,
	U3CU3Ec__ctor_mB16914BE2CAD61F52D33CD22926EBE47055DCD2A,
	U3CU3Ec_U3C_cctorU3Eb__25_0_m5C9E9AEF6312BEE63E13A9513E60921E769A132C,
	DetectionReflection_get_Descriptor_mCB26B9F43F22487C01ECA623532A1729500844A9,
	DetectionReflection__cctor_m29247C93D4909A3D8F3CEAD3A22700D443C8D0DD,
	Detection_get_Parser_m8804F3493EB34E33CB9D3403761A2080CDEE91A8,
	Detection_get_Descriptor_m960643CC637EA36DD9CC7991397DCB69E3D1C7B4,
	Detection_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_mA9EC20DA8799833311A701FAA77BCC77ADEA0273,
	Detection__ctor_m55A7113BDFA8EF7BE08FA4B349F34A3565313C2C,
	Detection__ctor_m9A9FD199DC21FB37D14C03361D20D3D8E727FA25,
	Detection_Clone_mD7CBC5BAEBC3C6C75A793B8F3C68C9302369C26B,
	Detection_get_Label_m859E003D211E67426B3E718E37573AB8BC57A0F6,
	Detection_get_LabelId_m94CA1879A1F03B68D28ED95D3A9015C5A86AAE50,
	Detection_get_Score_mA75239AC61ECD75F9C6A8C3BA58205E95795DD34,
	Detection_get_LocationData_mE9510CB14E4E89A7B989F268825E8B0B28109247,
	Detection_set_LocationData_m7DF88785B38E16CB61F810B2D6B2E1DF294FED01,
	Detection_get_HasLocationData_mC91AB3C89B4B520E89B47280EA1C02AA1AE97693,
	Detection_ClearLocationData_m1F6F3AB12BA2553C34D4438B8DAA691413B2F900,
	Detection_get_FeatureTag_mAD5D24D4F9AFFAEC840EEB8681C65D66CF87C321,
	Detection_set_FeatureTag_m7FD28F6460CEC89731D435C5B244ADD03F61366E,
	Detection_get_HasFeatureTag_m33BE0FB333C13D56A374E18FCE67433FFA6B7D37,
	Detection_ClearFeatureTag_m2E2491436E69DB5B53F661B3E472B28FAEA25EFE,
	Detection_get_TrackId_mA0AEE8F82BBE355D60BB0A82E88F898C5EDFD1A8,
	Detection_set_TrackId_m2B5BB3099BA7A0265DB14ECF2916DC6ACBAE94E8,
	Detection_get_HasTrackId_mB016B407BBFBE43EC46B4C5DAEDE7D0AF914D620,
	Detection_ClearTrackId_m966839C219ACA6DCC0636BFD8EDFF70048B82DC9,
	Detection_get_DetectionId_mBFC4E7FE96326BBE9A265D305F6117BF373A031C,
	Detection_set_DetectionId_m999639076277B1F079CECE9BA157189DF690B87D,
	Detection_get_HasDetectionId_m437570724A7066C0079CC8918055BE6C81312A63,
	Detection_ClearDetectionId_m9CB64FD41C0C973CFD6AFAAA30170431AEC808F7,
	Detection_get_AssociatedDetections_m969BB4B06366A94202105D65287D2A91F2D1C750,
	Detection_get_DisplayName_mB10FCAFDA10A6817D3B6631F72A4D9A5488B6A96,
	Detection_get_TimestampUsec_m56D5992B5F1D7EE39E6459492144689ABB07CAFD,
	Detection_set_TimestampUsec_m2F6ABA6B9FE81772F55F1ED3DC4708C11C7C01BC,
	Detection_get_HasTimestampUsec_m7DB9B72A5F8D4CC62935F9FA1F250D6CC38246DE,
	Detection_ClearTimestampUsec_m5E7F1296F66D3F6C41227404FD63C067189A97D9,
	Detection_Equals_mF155165DC1D42CDC67B1CA09A89384560162D4B0,
	Detection_Equals_mD313C1A5318C95D4E59E243292D9C147B7A49460,
	Detection_GetHashCode_mB7B8FB777B8621E0928027D44910CC0971BFA294,
	Detection_ToString_mE695F914BD60F8E148D00D86E4FBB5C060007026,
	Detection_WriteTo_m871A5B247BDFCCA600B3C8FE18949C639DB691B9,
	Detection_CalculateSize_m729B385BE443B130004A0016D33D88914B391E01,
	Detection_MergeFrom_m6FD510B8AA365A2AB532AF3FA7AC12E7CA62B444,
	Detection_MergeFrom_m677B85361862396FF00F51831D73EFF565CA7532,
	Detection__cctor_m40A52CEBE01AD2AB0CE83012833D2BA2ED5E9DBE,
	AssociatedDetection_get_Parser_m7F5CE7B641562230C7DFD50A6BFEEA486BEBAA08,
	AssociatedDetection_get_Descriptor_m6B684E8503745EDC2FFFF2F30A7C254A11A570C4,
	AssociatedDetection_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_mC84E02347E27FAA941E93DFFD1481E7217F20F2E,
	AssociatedDetection__ctor_mA166F2C623931F25847860332A520ABAA5CC9DB4,
	AssociatedDetection__ctor_m1BFE2C623E6B59BB029246429752139FFA5455BC,
	AssociatedDetection_Clone_m11C498A5BFF91B15128640FE1FF6C186DC791115,
	AssociatedDetection_get_Id_mEC4F87C0236F9F3FD0005C23783F2253702FFFBF,
	AssociatedDetection_set_Id_mF0B0781948BBF847A3BCFCAC185091B2594A30A1,
	AssociatedDetection_get_HasId_m9E3DCFFA41329ECA799568E888B8E59CDB03C9EE,
	AssociatedDetection_ClearId_m1329E13167EA8CDBCFD82765534017CF7CCBC2F4,
	AssociatedDetection_get_Confidence_m75A9322DD5BEFAA66F6C51D5952E588A297529A3,
	AssociatedDetection_set_Confidence_mBA28B3826F17A35A8C08C28BB4DE661DC788945C,
	AssociatedDetection_get_HasConfidence_m25B9B189095640C7063375061A00693721F7BB50,
	AssociatedDetection_ClearConfidence_m552D9205076452F3657B3C6A145064F6FE0CEEAD,
	AssociatedDetection_Equals_mD472A75917A686CB5371B84ADC4B8667091541FF,
	AssociatedDetection_Equals_m1069816F34AFE7A08AD3953CFC280417427FDA02,
	AssociatedDetection_GetHashCode_mE8B82EDD364192C96CFECF67D9C44ACD43128F64,
	AssociatedDetection_ToString_m7AF4B716FBE2D34C584115309FD4DC10F3B1D396,
	AssociatedDetection_WriteTo_m6CF6A142DD499EA8E2564F71291BE2FCD068DF87,
	AssociatedDetection_CalculateSize_m31A05FE7461704430FFD30B726F2EFE701EA7F05,
	AssociatedDetection_MergeFrom_m1C3C649269388963B9FA33ED78AA4FF2FA0DB27D,
	AssociatedDetection_MergeFrom_m2BB477AACB83B0CF3CDF2C0161D97898E2152612,
	AssociatedDetection__cctor_mF73162ED5E8EF82717CB8D743D0B3A9697DAE81A,
	U3CU3Ec__cctor_m4784E724F54A57C00AA99199DC6CCE1C588247F8,
	U3CU3Ec__ctor_m47E41794FCDFBA8A6E43F8071E323C2AFE80FA3D,
	U3CU3Ec_U3C_cctorU3Eb__39_0_m712A1FE27414A54F9D2C7B0BDAB83A24A2C0E93E,
	U3CU3Ec__cctor_m76955C859E37EEF2711E99A6AE284AB4DA351550,
	U3CU3Ec__ctor_m167260417D2CF2B3233A2CD043C6F77A89496671,
	U3CU3Ec_U3C_cctorU3Eb__91_0_m317030EDDF21A0236C6F6E9FE21F73D1621F90A3,
	DetectionList_get_Parser_mA6F1C81858C872CEA7512DAFFDE8F50AB9FEBD42,
	DetectionList_get_Descriptor_m5E47340D9DE96F758E57D0B4476FCF7824DB17E0,
	DetectionList_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_mA7B3130A43EFA6FF2093563982F72C0A9A147022,
	DetectionList__ctor_m06FDB5957ED0F6F0B39F5738E06C2F1159B37183,
	DetectionList__ctor_mAE98482B38CBE076985F9128D7281E33546913D1,
	DetectionList_Clone_m5331EB486F6B61CFB67E0BF4ECD4BE8AB966B807,
	DetectionList_get_Detection_m90A7E1B909449ED7EBF2074B32294A3063241791,
	DetectionList_Equals_m298DE49B1E060D9C24B467E5A55A8C1D145923D4,
	DetectionList_Equals_m6E7B500E4EADA19BAFBE795DFDCFBA9A8B44596F,
	DetectionList_GetHashCode_m807A6737A246CB6010A539204908F6F27215C8EE,
	DetectionList_ToString_mB5437A519C9DDF1885D468B7324AADDE901CB12D,
	DetectionList_WriteTo_m7BA51D678FE5CAD8ADD3A9FDEFE9211D36CB8420,
	DetectionList_CalculateSize_mD096AE8E4FAC18AAA69EFAE4CACF35BDA4A4AC58,
	DetectionList_MergeFrom_mDF65E3B58421A40462E72B81742D574CC1C9458B,
	DetectionList_MergeFrom_m630B2FE189B963D229CE290BA57F9AA276645AEC,
	DetectionList__cctor_mD43572ABD6868A4C397B472E91C90DDC8789A565,
	U3CU3Ec__cctor_m102485CB67CBF844566CD1DEFA91C90784CF5622,
	U3CU3Ec__ctor_mB5B25F654BEED6D58A60BA1092BE07B5E6335755,
	U3CU3Ec_U3C_cctorU3Eb__25_0_m78D642D8A85C09696E0F45C20DA6702A26903489,
	LandmarkReflection_get_Descriptor_m2D2FDF99733E2B6325737FD7641FBBC927162A18,
	LandmarkReflection__cctor_mA62B0D521A665EE5962B46B9868B80F1D061EDAD,
	Landmark_get_Parser_mF9ED7F5C6B0659C92AE4CE5BFF7D3D27CED07589,
	Landmark_get_Descriptor_m245583FE6AD9B9D7C6E8DF399D32DE967D3856E5,
	Landmark_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_mCC9FA090DAB3FCDAA2EBD01DE78F12FD8B4040F1,
	Landmark__ctor_m32AB2F02006F1DA904DB9B95FBA6F54438CB7E09,
	Landmark__ctor_mB5954D418050FA757FEEAA123D3DB388279E0D66,
	Landmark_Clone_m2F263E193E5C29A90092AE5EE1AC5B3880CAAAEC,
	Landmark_get_X_mCCC8565C94A49C95CE661F1F31887EE88251F2DE,
	Landmark_set_X_m2A35E536CECCDC1F1C7217F6FD13A1AB310699C4,
	Landmark_get_HasX_m33122BBA8B7B7D0EC394223AF2206A71FE7C428C,
	Landmark_ClearX_m75472AA9F683DEC71B30978EA0A088121462A016,
	Landmark_get_Y_mA9262DF805305E3CB5BE75FB6F7C495A02FFCB15,
	Landmark_set_Y_mE8411E61699805A916E515C267DF61824EABF79A,
	Landmark_get_HasY_mB40C8AD8D6CA8B14F187CDA443CD612633E5A4FB,
	Landmark_ClearY_mFE69B3F9E85D3947683E92A28E5F6D43F0FC2BAA,
	Landmark_get_Z_m31B36BF45DE419103F21FD96B032972214C2CC4A,
	Landmark_set_Z_m19F0BBCBC95F9D1E333D08B38FC8C809D392C9DD,
	Landmark_get_HasZ_mF14860BF6E8C04369B1F238F087BB0D944722031,
	Landmark_ClearZ_m77CF2955E6B9A2F237E810067B15CD756B9B3A3A,
	Landmark_get_Visibility_mEE2C652EAB55A50697BDD510624975D04206A98B,
	Landmark_set_Visibility_mB185A177B81BB5F6C26BABC659CE5BCB49330ACD,
	Landmark_get_HasVisibility_m448A4F9944944988226E9E9DAE6CA1BE16364CCE,
	Landmark_ClearVisibility_m63565970A485B48F1E5A549D2A9468B6CB05EF7D,
	Landmark_get_Presence_mEBE783B55F9F7880480EAFFBB56A9EAA6B6F0693,
	Landmark_set_Presence_m1FA51C2A95825CEE140B99E2392A2EF1F719A2E4,
	Landmark_get_HasPresence_m0DEE9FD838DA5FECAE7E73CA83C130A2DC3558EF,
	Landmark_ClearPresence_m04EEA91E043A0665A4B35DCC26206F73D5643763,
	Landmark_Equals_m5DF27AE33F5ADCA0F166401F6EF682568172E3CD,
	Landmark_Equals_m5DCEFC736DAAB541ECAF07682E0CBA342E06F8C5,
	Landmark_GetHashCode_m1D05AEC667DFD74EF7BD565F23D18E9F2D9EBABF,
	Landmark_ToString_m8A1ADCAF99F8EAB736244D2D64180F410747E9B6,
	Landmark_WriteTo_m2426178F2F1BE3E779166512562C66D5CE55E2DB,
	Landmark_CalculateSize_m57A5261BCE0E7D5FAD4404AB917F2F7D5709CA8A,
	Landmark_MergeFrom_m601FEE1790CD75767288FECA657CEEC9516EB0AB,
	Landmark_MergeFrom_mA2E18E48D58A71220844E0C26DBA09D4E2DED0BB,
	Landmark__cctor_m9CE5019CADC8C750C9DD1BB9AB5230E3B8D3A9FD,
	U3CU3Ec__cctor_m3DC9E59ADA4F78ED23477189987FE20453DABF04,
	U3CU3Ec__ctor_m9C3D3629FF351E8AA86889FE7EC2DF4F046F43B8,
	U3CU3Ec_U3C_cctorU3Eb__66_0_m35D2ACA3993A2BB91BA9E105CAEF8A669C570322,
	LandmarkList_get_Parser_m6F868AA7BF10DEB5CD13DD99219170AECF26168A,
	LandmarkList_get_Descriptor_m87B88FA4DB78B78C66165964C21FBB884B6092E8,
	LandmarkList_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m7BC795FB6D23D27211EB686C07888B2E824C929B,
	LandmarkList__ctor_mB39DEFB418B9FB30F8E826F7398A5D2F2993154B,
	LandmarkList__ctor_mAB0C124B8A91ECD95FEEA961A5B7AB35704CBF9A,
	LandmarkList_Clone_m162BFB1581722DA392C42B14FF4453FE63924E8A,
	LandmarkList_get_Landmark_mA401B3E0FDFEBDE27ABDD919A3B731360D64B4CA,
	LandmarkList_Equals_m67DF4B25C4F479352B3A65E7BE42E3FC87122CAA,
	LandmarkList_Equals_mEF4EFD2347D65884B176A40B3F9C6A316D41C8B4,
	LandmarkList_GetHashCode_m9BD8E4A4309FF6C0CEE9C524C47140FB34A2565E,
	LandmarkList_ToString_m8B21B5CB0BA7CB7F08FED7BA6D77FC136CF2DF3C,
	LandmarkList_WriteTo_m31B2E7A45E2503C6D1600AB5EF7902F9B5245D07,
	LandmarkList_CalculateSize_m53AFAD8AED8C89539F906D99555FDAFF55CC0A5D,
	LandmarkList_MergeFrom_m8FAB382380EFEDB9FDC963AB56EE75A624A26283,
	LandmarkList_MergeFrom_m84474E94FCBB33DCEFCAFE1F005BACDF7D381A67,
	LandmarkList__cctor_m250046F24E0C7B375EBB230568D7F7952BF88F11,
	U3CU3Ec__cctor_mC4A93586FA061998994507B0A58D45D87FD3322E,
	U3CU3Ec__ctor_m1DC117C8387614686AA191482CB46683CDD5E2DB,
	U3CU3Ec_U3C_cctorU3Eb__25_0_m5CE5F4A9C4580793761C70AA78C97B42D722EA4E,
	NormalizedLandmark_get_Parser_mAFB27D9C602A88961B6D0DFA8D58EF2CCD831740,
	NormalizedLandmark_get_Descriptor_m10E3E9AEE9FA14C2A083D95360062C148C78E151,
	NormalizedLandmark_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_mDA42C87657A3D0946E43F20E4CC6D9255DBFB498,
	NormalizedLandmark__ctor_m749D7C799F92DEC13A50FBF151B384E803BFFF52,
	NormalizedLandmark__ctor_mB9A73BE195E17D391BC26DC9C4A9FE7CD3FCF784,
	NormalizedLandmark_Clone_mEA928C357031BFF03D7B3463A38391F589530EFA,
	NormalizedLandmark_get_X_mE40CC2764290F7C561315DA843933BE5F5B610D4,
	NormalizedLandmark_set_X_m04A3B6D5BE6EE5C0911C86C19BBD6A83C5819564,
	NormalizedLandmark_get_HasX_m22D9F8C2D714ECF392ABA2B9AF56AD787605F5E1,
	NormalizedLandmark_ClearX_mF00D31AC4974CD5DFC81875A4A6FC3143DE11626,
	NormalizedLandmark_get_Y_mA4842900B26232E6A6A4C7D2B15DD3B681C963BD,
	NormalizedLandmark_set_Y_mA2BEC843600170080E1B7A49340593CABEBD59C0,
	NormalizedLandmark_get_HasY_mCAC595075F3F60DA27721FE3108A3C8973EBDD9D,
	NormalizedLandmark_ClearY_mBA1143EE451962D9CCD7D6490DEE4B109E402477,
	NormalizedLandmark_get_Z_mDE7FBFD528B8674FBA3AF82730582F8BDE63766D,
	NormalizedLandmark_set_Z_mAB1E4CD7C16459A82BB35887DD4B01D2021F6F97,
	NormalizedLandmark_get_HasZ_m9284EF2F9CC46B11BECC449FADC76878C096F40B,
	NormalizedLandmark_ClearZ_m43166BF7D6C2B8CC8455116E3ED711FFAD77E03B,
	NormalizedLandmark_get_Visibility_m6EF72E21124BDBBACA7AC69388A7FADD0D5D8029,
	NormalizedLandmark_set_Visibility_m2206EF29A95BE080A6C4ED5B6229ECFB54DD577B,
	NormalizedLandmark_get_HasVisibility_mB321CAB39C8424E18D5965B70EDEF912174EC3B2,
	NormalizedLandmark_ClearVisibility_m462FB2CAAF3A6987E414033FC435E9EEA260EC45,
	NormalizedLandmark_get_Presence_m5E7E7245E080253A92C68D4DB6ED9682C16223C6,
	NormalizedLandmark_set_Presence_m08D304502FE3E079389B8B4D2D458DA702EFBAE5,
	NormalizedLandmark_get_HasPresence_m35A999B1B070457E8ADBD7E84DC9F35F802F4FBB,
	NormalizedLandmark_ClearPresence_m3BA6187034736183F1237A19186AB4F7C5D98B68,
	NormalizedLandmark_Equals_m7212B45EE8C95C27A16E61219CF0BC7B63508158,
	NormalizedLandmark_Equals_m694303789E20659F4755195A665597050EA7E661,
	NormalizedLandmark_GetHashCode_m2ABE8B45C235972D5D5E33F4E688B9B6014840F5,
	NormalizedLandmark_ToString_m2071AEC6E2374C9F11165F7CDFFE5720E273362E,
	NormalizedLandmark_WriteTo_m33870DE137A9B7ED976F9A092FFA7B9F0A1930B5,
	NormalizedLandmark_CalculateSize_mE3D06A71DF8BE59866DE7837D6A848673525A73A,
	NormalizedLandmark_MergeFrom_m903A7C27238CE6DC37619530278D38EC5E7124B1,
	NormalizedLandmark_MergeFrom_m0348956EE9DEA0CDEE4760CCBBE0B275EE0DB93A,
	NormalizedLandmark__cctor_m14CCB26F286090FB9D91171C19F33F8DF33201C5,
	U3CU3Ec__cctor_m72FBB40D58E81E1C5B74A34A01D00CB5F63CD5D1,
	U3CU3Ec__ctor_mDED913681E474688419F85C5971B7B8A3AB22E66,
	U3CU3Ec_U3C_cctorU3Eb__66_0_mB5E0E49EE3A15F967842E7CB215964088BC14655,
	NormalizedLandmarkList_get_Parser_m36AB90FD2B5A21789AD5354EA732F998E406B026,
	NormalizedLandmarkList_get_Descriptor_m407AAD482B56B422CBC3A4D471D1929973691A5E,
	NormalizedLandmarkList_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m36D51DA95E6A8A5FDBCD866B78A2B1A9F49EED5E,
	NormalizedLandmarkList__ctor_m01D41F6D909C6F3B3B7EEE22709861C7B29D61FA,
	NormalizedLandmarkList__ctor_m75C1E44F5F089A869ED0E929367C75160499360C,
	NormalizedLandmarkList_Clone_m0858F45BAAB39546DBF5853A5FCB0E2395F34E54,
	NormalizedLandmarkList_get_Landmark_mEAECE63E9BC383A7E6B2497999B936185BF3B192,
	NormalizedLandmarkList_Equals_m0B854C7B06A98D47E68E15B965FE8EDEE4445CDD,
	NormalizedLandmarkList_Equals_mF6220EBEBE45837C5C7ABE8A60139A168CFD6555,
	NormalizedLandmarkList_GetHashCode_m8815B70BC600FCAC3B59DFF49C681B3351F4C493,
	NormalizedLandmarkList_ToString_mE0E29E7405F94D7B9B37CC75B53A64A473B142E9,
	NormalizedLandmarkList_WriteTo_m7030BCD0F34648E08815E519717EF288531D0070,
	NormalizedLandmarkList_CalculateSize_mDBDAE3F1AA8E835537862295EC26955E3ECBAB71,
	NormalizedLandmarkList_MergeFrom_m9E3B0324A50B637AEC4F25E875C6061F5FF0231F,
	NormalizedLandmarkList_MergeFrom_mC7D95F8D50AF1080D9B19AAC9DD77E7376766312,
	NormalizedLandmarkList__cctor_m92754432920CFA2FF5C2368A1E06DD368C5C787B,
	U3CU3Ec__cctor_m63BCCF15DA557FB83F4815836D17A83BA5A6A6EC,
	U3CU3Ec__ctor_m48437BE78DC81BDA3797AD8CE5633609FBFC6115,
	U3CU3Ec_U3C_cctorU3Eb__25_0_m32AD412BD61826DAA043DD7BEED4D0BA6B028A1E,
	LocationDataReflection_get_Descriptor_m10DA1F84708031EE1FCC0B3D26ECB9D97930C128,
	LocationDataReflection__cctor_mD1EA42E03A717749D160D543CA692204B40BFCC3,
	LocationData_get_Parser_m1DB7673AEA9EB45522154A3818213A03C55AE1CF,
	LocationData_get_Descriptor_m5D0CC50C75A8088D9CBF3D0D4FD78B1F246E562C,
	LocationData_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m86EAC335052E8D69DD1013885F1C07350EB24BA1,
	LocationData__ctor_mB604C634DB577934AE64462669C6C3764CB2000D,
	LocationData__ctor_m6D1AEDF1838BB6FC4150FD00A021962307D60372,
	LocationData_Clone_m79CB2770CCAA00A4C9BB99459225994D1291D8F9,
	LocationData_get_Format_m626AA11AF4DB0D37D8D4752EAE9A839966BA0DAE,
	LocationData_set_Format_mC9E05E4E4B7E37372DA2CFC6026172557FF01C83,
	LocationData_get_HasFormat_mBF23695BFD2D07ED04565B9BC937F5C62A5C5499,
	LocationData_ClearFormat_mC84F4F1946BB3CE469972A09C41E2008F4B0A7CB,
	LocationData_get_BoundingBox_mCF4DE5C5FBD9EF97DFAAD94C88E330985CA5A1EA,
	LocationData_set_BoundingBox_m9AB8A52AEA8A1D03DEA1B03F9C9FCA951A5C0411,
	LocationData_get_HasBoundingBox_m09F4A29F1BEADDB5D2361A123E147121E27B388B,
	LocationData_ClearBoundingBox_m5EDCE0CB5DC3978B1728178FDBDBA170265FC6F5,
	LocationData_get_RelativeBoundingBox_m3BD51E37829375F2CFD25FCDDBCB1497AE04C6BE,
	LocationData_set_RelativeBoundingBox_mB7C755136E92AA38F3EFFDECC061013257D4678A,
	LocationData_get_HasRelativeBoundingBox_m3F25BAECD64A2903CCEFBC84B485B8A60A52C47C,
	LocationData_ClearRelativeBoundingBox_m30E69638BD31A7ABFF6EA8607F0B8716ED807B99,
	LocationData_get_Mask_m6AE6D5787D703738447FAC8AC4A9EC70D3DD19CB,
	LocationData_set_Mask_m6E13670DB817D20903AF0E4F9A85DB9D2D36ED01,
	LocationData_get_HasMask_m666C811D63C01D22497C67D8DCE11407B3B9D955,
	LocationData_ClearMask_m76CE8607652C20FFBAEAE510127E861DDB3677CC,
	LocationData_get_RelativeKeypoints_m579BB8AFD5AD8037BE4E73C6CCDDFC3E943B13A8,
	LocationData_Equals_m170A452AFFA5AD61A8E91F3A2C414E43B776262E,
	LocationData_Equals_m898CD12FC26B6B0589CB403E9C6B29550877A69D,
	LocationData_GetHashCode_m307B9D8787153A4DFD1C54C01FE4D9BD05672255,
	LocationData_ToString_m29E48F542440D30B1734C7DB05DFB9CCE5202A05,
	LocationData_WriteTo_mA5DA809FB04E3707BF837E4352734EC94D6B2B95,
	LocationData_CalculateSize_m0E812979AB5E6E5CCEA276AEA562C84BB021E4CB,
	LocationData_MergeFrom_mCAA4BC513485F808DD7A20C6281A4B546A237D05,
	LocationData_MergeFrom_m388838B258FB88CE12864E45E68DD5F22274BE5C,
	LocationData__cctor_mCDCDE7352F03AE90A6E1FDCD4638AD330E622DA7,
	BoundingBox_get_Parser_m1DBB7A33DE3D22DBCBB0FE66D5AD73A955632B55,
	BoundingBox_get_Descriptor_mBD9A19FFCF588B29131312BCA77DE09DBD172714,
	BoundingBox_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_mA4774443BDC66A478BA04DBC6AAF7099823E19C3,
	BoundingBox__ctor_m593B06506BFD3A2B0775E81312CF30B4E63C4D65,
	BoundingBox__ctor_m627AE04BA213E57A63AEC13B676D7F5B74DC92FF,
	BoundingBox_Clone_m00AE731CD8CDFCDC53DA43A8065BEEC74E9DD5B0,
	BoundingBox_get_Xmin_mB0E7C4136A3116133C616FBE6AB69DF02F7E38AD,
	BoundingBox_set_Xmin_m5A56BCD3C1DF932F600F6F1EBFADDFA5514B683F,
	BoundingBox_get_HasXmin_m85AFBAFD6F8933D2C2014E99AFA9C125C74811D6,
	BoundingBox_ClearXmin_m17EB77B29A2B29A9B182986F8C45D7C0DDE4238D,
	BoundingBox_get_Ymin_m7D882E1F987CF2E1A7DD727B945D47282F5CA0E9,
	BoundingBox_set_Ymin_mD39ADF868ACD94870F82DEF9753EA086BDFC2F57,
	BoundingBox_get_HasYmin_m7FC954880EFE8595C0640F874A8E6A692A7C8BDF,
	BoundingBox_ClearYmin_m786E94CB3B96169321D4E023816E36B3E111D402,
	BoundingBox_get_Width_m1E114AE64C14ADD858BE1EC661D984B2E4ACC4BE,
	BoundingBox_set_Width_mD94148B6E2CA61767FCCD4D2AD1235F13681B200,
	BoundingBox_get_HasWidth_m6407FE26791689503CD340ED0C8F89733D26AD44,
	BoundingBox_ClearWidth_mF1280F4300C6409B29F083612CC43AE23EF27BBD,
	BoundingBox_get_Height_m783251050DD1092525DECAF847AB87ACB6869B12,
	BoundingBox_set_Height_m16177A060133C9353788A680F943FB1772E1D52A,
	BoundingBox_get_HasHeight_m90644E0F3320851369CEBD4E83C31F7E87030E2A,
	BoundingBox_ClearHeight_m39635CDC84989A610ECA78395F36D44D5E752E24,
	BoundingBox_Equals_m59EADA8FA336A51CAA7BDAC92C9659F352E0EA82,
	BoundingBox_Equals_mA301D6545DCCF5CDDB551F3F5D3E0EA14BE4FDB9,
	BoundingBox_GetHashCode_mA2E1A26961E791956B155392365ABE0CFD32C9F4,
	BoundingBox_ToString_m59B089B9CD4F5D49F89881F869DC857AFC3B78A8,
	BoundingBox_WriteTo_m5893434F89C8673816C4FA7884F8BD8CEE7B8087,
	BoundingBox_CalculateSize_m3FF05A537A0E9A624B5FABFE60EBC6FFB10F5A23,
	BoundingBox_MergeFrom_m8FE73B1C9B61C5307C66B1C3D706836388AC89FC,
	BoundingBox_MergeFrom_mC6EFA4D2E99DEC21D436A2A6D90362394A47BFFD,
	BoundingBox__cctor_m9303EC3C039DAC049D490D54C950382E9DBC8EBF,
	U3CU3Ec__cctor_m81878484AC353ABC492C7E5064C0C1D0612EDB5E,
	U3CU3Ec__ctor_mC5991B6DA7F718355E99B5617D95E206CB9F00B8,
	U3CU3Ec_U3C_cctorU3Eb__57_0_mE286EBC69DEE7A2FCB1A3B6B7A0B3FA52BB3A788,
	RelativeBoundingBox_get_Parser_mBFC6B6ABB1E1CB9EB3789ADD589F5E49E36EB648,
	RelativeBoundingBox_get_Descriptor_m4674407D7470FC41C0DF2DB0D2C4838A8D6BE088,
	RelativeBoundingBox_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m6094EDE44BC491F9C1C3703354C5294F38DF4E72,
	RelativeBoundingBox__ctor_mA8BC438A7E04136689A5998E0D7EE0CD98C66B6A,
	RelativeBoundingBox__ctor_m4944F2FED20D1A230D2BF9E5D6A32F725CFCD6EF,
	RelativeBoundingBox_Clone_m0841DF2E2BFB704DFDCE1809E145778315BE1633,
	RelativeBoundingBox_get_Xmin_mA824E49A567DE0853D917FCDE2874703E15D860E,
	RelativeBoundingBox_set_Xmin_m08AB518F6ADFA534D1FD830A0905C12F4BB9F12C,
	RelativeBoundingBox_get_HasXmin_m653570E1DC863D11A5094DF3056B7EC51E060379,
	RelativeBoundingBox_ClearXmin_m28CEF281DAEC7AF7C8ED9F0E471553717368B8C3,
	RelativeBoundingBox_get_Ymin_m1C551336D05CF3E43CC9144052E48A7EF4F4A9EA,
	RelativeBoundingBox_set_Ymin_m99DF07401CE9D7CEB9E91A800797855197BC9091,
	RelativeBoundingBox_get_HasYmin_m5D1543550F75D43D8017FE39E2AAE97B9DD26376,
	RelativeBoundingBox_ClearYmin_mD759F2E5D785928C94DA4D2A89BD34A34BA7F11B,
	RelativeBoundingBox_get_Width_mF398F77C41779AD93A1F7CE577A383BE0791AAAD,
	RelativeBoundingBox_set_Width_mF74D991D6928BD39FCC5CAC080266847FE51A474,
	RelativeBoundingBox_get_HasWidth_mFCD5FD1E1CBE9EB35FE211E3B1A50A4466927AD7,
	RelativeBoundingBox_ClearWidth_m72C70182A243C7353F98E8899B82EF42648B98E1,
	RelativeBoundingBox_get_Height_m3A22A85EFFBBAA0668FF26FC0A6341780BF070C7,
	RelativeBoundingBox_set_Height_m90EC9B0116EFF1225C439DBEEDA5F3AE7A77A5DA,
	RelativeBoundingBox_get_HasHeight_m310674D4DB7791AF4D82443FB42B14202E19A072,
	RelativeBoundingBox_ClearHeight_mD70EE677AB7F7D7C9573EF087BD90D08794B41D7,
	RelativeBoundingBox_Equals_m4A16727F878D1A1CF29E042D8088470E058DD7DB,
	RelativeBoundingBox_Equals_m45535830904520641BA7D02C53DCBE8A548359B4,
	RelativeBoundingBox_GetHashCode_m6E36E0BA9CABBBBB75860C51EB1C253702E47E62,
	RelativeBoundingBox_ToString_mD096BEBCC911A0831D49B8ADB2393668CB1E07AA,
	RelativeBoundingBox_WriteTo_mB80D4F45F77E83F7B6A170F6A3BC48001C0FACF0,
	RelativeBoundingBox_CalculateSize_mCE3CB30EDFEAE4B677F7EF03C94E74FD0F39566E,
	RelativeBoundingBox_MergeFrom_m0E9CE082728E5F9669448DDC651D3B424F8E444F,
	RelativeBoundingBox_MergeFrom_m47BCC3409807A67E5FAEBF440FDD9155DFE0DA20,
	RelativeBoundingBox__cctor_m73700068D4F921A0202218DB1A63F00E2ABDD272,
	U3CU3Ec__cctor_m9C4D9AA9735915DE0A42AB5D2DC837BB8102502F,
	U3CU3Ec__ctor_m7551984D2E8DB82351E841CD613D0E035584802B,
	U3CU3Ec_U3C_cctorU3Eb__57_0_mFD2480E32A339CFC34E7DDBF66CA0F54DC88414F,
	BinaryMask_get_Parser_m35F8F45AF7AF30C764D7055D771C6380947236DB,
	BinaryMask_get_Descriptor_m5FC425BA1F607161F5D6A49572960035DCA88109,
	BinaryMask_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_mE6C87ED2CD0FE3FC60FC151C4801AF6A6C11E9A8,
	BinaryMask__ctor_m8F90D268C87AA0A2998B869A679E486B6784B069,
	BinaryMask__ctor_m03BA73D9ACA551B4101934F7A24C4AFB1061B712,
	BinaryMask_Clone_m686B51B7DC6D0BAB62F6C639B6ADE99AC8A9AA99,
	BinaryMask_get_Width_m4B4F724F4A91DB9B31EFC393F0291DB100363F7A,
	BinaryMask_set_Width_m6DB2CC176F291AB00E349FE3B1DCCE33A25C6F4C,
	BinaryMask_get_HasWidth_m0BFF1D2FC1D77AA7448314E981282040CB7FB961,
	BinaryMask_ClearWidth_m7C972B069D508D1FE0EA7991A4C5362201988D9D,
	BinaryMask_get_Height_m5236D71E1EFD17E480B9610DD4FCBC0428A8662A,
	BinaryMask_set_Height_mEE156A51EFACD8DD43A776861BF94C6B69F6E039,
	BinaryMask_get_HasHeight_m2EF3E270CA2CDF21602CF9FC65F5847AF7F943FC,
	BinaryMask_ClearHeight_m0CB9F998280EEB1A27CA8EF7AD9776ECB07A5DD6,
	BinaryMask_get_Rasterization_mB75FF0B494A6138FC4DCAA6B56AC31E2D588E326,
	BinaryMask_set_Rasterization_m66B08D6D62AC649A943C28F112A5CBADBB609ED2,
	BinaryMask_get_HasRasterization_m74406A4905ECED17BB98A8D103FF9DFADAF37AB1,
	BinaryMask_ClearRasterization_m652D34C3A5C6168C317903658D48ED049EF71B9A,
	BinaryMask_Equals_mBF78A6B0C1E5516AD385E4C4D22851C5BA57A5F1,
	BinaryMask_Equals_m49D94332523281DBAC6789DA4D6C2179E28EF83A,
	BinaryMask_GetHashCode_m9E4FFB7CE411C64CAD98DBA291AF0E05C61A8FE5,
	BinaryMask_ToString_mF795F2557EF8ED863BBD2F1C459778A61115700B,
	BinaryMask_WriteTo_mA261BA797008FF73839EBA7E0592F0DEBD415564,
	BinaryMask_CalculateSize_mC22E19FAE984002D7163988734767B26FC5823D1,
	BinaryMask_MergeFrom_m6BE953E5A769220962214F9F05E0FE319C1B652B,
	BinaryMask_MergeFrom_m5DA0857C4C0936C91F2EB06AE3F299518918D776,
	BinaryMask__cctor_m62770E71EE03C05597D6DBA03B6151EA8F144082,
	U3CU3Ec__cctor_m53224E031342A9440AB1DB6183C97CF1380D503C,
	U3CU3Ec__ctor_mECF34F0FBCE97F2A2EC31002CF38B34B847F3DCC,
	U3CU3Ec_U3C_cctorU3Eb__47_0_m0AC3A5539D747E60C82CC18CF720203910238B15,
	RelativeKeypoint_get_Parser_m9C9717BBDB4544FCBE2D5C5C6CF13B39F2D8D32F,
	RelativeKeypoint_get_Descriptor_m2384BCFD3E8C51A97FB766A283D7C939F94A8EB9,
	RelativeKeypoint_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_mC1A449B5DEFD4BEFAE1CB4C3A96741D3BC79B114,
	RelativeKeypoint__ctor_m206C6B4CC8BFC83247B3CF0E9B6C1DB3EF9FD239,
	RelativeKeypoint__ctor_mEAE05077643B9B02D325976BFEA6673DEB47ACB9,
	RelativeKeypoint_Clone_mB4A646D4224167F8867687AA163FFDB1772B344B,
	RelativeKeypoint_get_X_m8ACA03E4AFF97057B0433DA5C319705E4A4FAF1D,
	RelativeKeypoint_set_X_m9F9D071BF7DD14BF386A4714C41A6B720267CE33,
	RelativeKeypoint_get_HasX_m7A2FE60D3B3C41FA9FDC1A3ECEFF51EE45CF3B8F,
	RelativeKeypoint_ClearX_mA536C8F3890EAC060547009BFB0E7159C702549E,
	RelativeKeypoint_get_Y_m2EF4322968355594C301EACEE016973FB1656300,
	RelativeKeypoint_set_Y_mDEC7C9A9B6012EFD34D87969D7D8B0A02EDCAB0F,
	RelativeKeypoint_get_HasY_mED67AC03587672E7511794925AB8F7BB85627D4E,
	RelativeKeypoint_ClearY_mBDEF3488FCD528959DEA9E1D490013226E8C3ADC,
	RelativeKeypoint_get_KeypointLabel_m9CF672A12F0D6CA820DFD71E5FDD421788558F52,
	RelativeKeypoint_set_KeypointLabel_m52406523D13778F223B2242F23EEFB030CAF2674,
	RelativeKeypoint_get_HasKeypointLabel_m46A28C06B23F21F9758222489D7FEB1691774F2B,
	RelativeKeypoint_ClearKeypointLabel_m3706CBCDA0D55D054051AFFF760EF0590F75698F,
	RelativeKeypoint_get_Score_m433B50EBB0F293FA3B86D4D9934E87503313B50C,
	RelativeKeypoint_set_Score_m0BB4A989FAB1C79C9BE766FECA6B9F9CE6E6B131,
	RelativeKeypoint_get_HasScore_m56D9CDFA50CB11D7F19BF1F8209DFBCA20987586,
	RelativeKeypoint_ClearScore_m05253CED8244B4BEE218FD9F8F1299A536A68F34,
	RelativeKeypoint_Equals_m3F57F2FDEDA1A3E18FB46700957235704010CA1C,
	RelativeKeypoint_Equals_m704069F17BBCA172946EC75767DE0558DE5796C5,
	RelativeKeypoint_GetHashCode_m52ACCE197F999EFC3DC86317A382DD805532C5B9,
	RelativeKeypoint_ToString_mF649593D1A01012190C83FCAC793FE30E0A2ADB7,
	RelativeKeypoint_WriteTo_mBF98F261D95B71894CAA982839EC4F8A6D99ABC1,
	RelativeKeypoint_CalculateSize_m5B1B5C66064B9BF331664023DBEEBAC75DFD9169,
	RelativeKeypoint_MergeFrom_m198362F71ABDF0359BA9874F2E7AF59F52B14C30,
	RelativeKeypoint_MergeFrom_mE832CEDC0F6D893DA89014206624FDC0F0009AA9,
	RelativeKeypoint__cctor_mBB5BEE8C2F2D40C6483B4F8BF37E074FE36D05FB,
	U3CU3Ec__cctor_mA54C7CE031E534AFE5F4D80063173765EDFA251B,
	U3CU3Ec__ctor_m8708B900487EC35C2D89C9DE014FF940C9ECA8A6,
	U3CU3Ec_U3C_cctorU3Eb__57_0_m4BB9334CFBC10EF019310D07BB29BBFA4B9F7E7C,
	U3CU3Ec__cctor_mBDCE1F165C8F6734DFE01FBD887723DC6B6D7569,
	U3CU3Ec__ctor_m9231E3CA1ED8FB680CF1BC88F87E44BE2C261A73,
	U3CU3Ec_U3C_cctorU3Eb__60_0_m77F9F0702A521CD1FB473BC21A785C3F058E63E0,
	MatrixDataReflection_get_Descriptor_m5CCFD7E6D7AC04D96C29BC6D156BC7E586176486,
	MatrixDataReflection__cctor_mA46816038F39D83AED26DF6DB57EB3DB7A0E3637,
	MatrixData_get_Parser_m03064DB1EE5CC94DCDC8ED3B102D623572F48491,
	MatrixData_get_Descriptor_m4B70A6055A0487BCC5E28D764D8CA416C5D013BD,
	MatrixData_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m88D0497C1171A845B58B473980980E36D9CAA532,
	MatrixData__ctor_m08760B404A31B2AE0141BB152C5765B20FE3D999,
	MatrixData__ctor_m190F97D56239F0C19F454C92501EABC70460EF0D,
	MatrixData_Clone_mCE53C6458C28502B3440957A054F15E1555A2906,
	MatrixData_get_Rows_mF5D13C87219A58BC2045D5EAC64C202929F9044A,
	MatrixData_set_Rows_m782483E53038F4F47271A98FB550E881C2D76A01,
	MatrixData_get_HasRows_mF98D0F13962CCF8D765DB83EEF7D71CFA5A54A6E,
	MatrixData_ClearRows_mE0E54CD921FE4772B8F80BBDB6B3026AA9077887,
	MatrixData_get_Cols_mB80AB3D775218CA37DB6A1B0F969E7E548C92208,
	MatrixData_set_Cols_mF48CA74DBAE0214F2F37C586719CDFBDD7E74A1D,
	MatrixData_get_HasCols_m00CB3A16878979C1DFD25CEA35933F5E78E23B29,
	MatrixData_ClearCols_m05C85B672E3F314D316EAFF6C74CFC81278869DF,
	MatrixData_get_PackedData_mD14087D33C47D023620184D18822A5707340C9EE,
	MatrixData_get_Layout_mEF36C6E7C872283A9F24E3E23D0D9EAAB8AFBA9A,
	MatrixData_set_Layout_m5471AA4CC088DCB316675F93588865D9D71742FC,
	MatrixData_get_HasLayout_m7CC39257BADB62DB00DE77BA7DB54198481073C9,
	MatrixData_ClearLayout_m2140EFC522A01F79894555FBD578D33E12C684B1,
	MatrixData_Equals_mAB317070368E9C8FAC655E625805350B1FB0C314,
	MatrixData_Equals_mFF2B5393EA1A1F13B59FE28679F4FD1946BA8D42,
	MatrixData_GetHashCode_m43DDD0B77ECB6989C34D865D720E8E4F60AB8113,
	MatrixData_ToString_m14D4258EAB83BC97E759D73DAD0B8BBB2B78DEB3,
	MatrixData_WriteTo_m3B87386A2E13C7A8B682CCA7020DF01A57860798,
	MatrixData_CalculateSize_mB7488B8687D6F10921E34FCB8CCBA3C565BE5CD5,
	MatrixData_MergeFrom_m0AB33B94558F79353F9AE8E471A759FA65E828FB,
	MatrixData_MergeFrom_m547674A1C5CD7A86BF22973FA6FDF51A6DD056F5,
	MatrixData__cctor_mA852AA412B44AF096654CF62CC6AFDD68A86F951,
	U3CU3Ec__cctor_mD011CE8C61A4BEB914C8360E14D19C1EB1280F08,
	U3CU3Ec__ctor_mD91D1C768B4235B1176259F1B4A58E49F749A1A9,
	U3CU3Ec_U3C_cctorU3Eb__54_0_m75179DF0A43B6F14908A0FFE50DD65366F261042,
	MediapipeOptionsReflection_get_Descriptor_m1539AE6F7DB58086644D716C90D4319D4BCDD589,
	MediapipeOptionsReflection__cctor_m7C03A0863EE17286756848F4247FBE4412D67D55,
	MediaPipeOptions_get__Extensions_m5EDCC272A1B8D9D79F4467609283AC8351374889,
	MediaPipeOptions_get_Parser_m5708023B661DED9AD0D27FB3C3C20D05D41F9482,
	MediaPipeOptions_get_Descriptor_m2AD34E0B99F7D57488C31DC58C2DF66272366EC3,
	MediaPipeOptions_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_mF7ECCDF5483531F05891CB3CB88801E2FE1CCE36,
	MediaPipeOptions__ctor_m94ECAD8AA33546C341D7764097D40351B2E4AB54,
	MediaPipeOptions__ctor_m54624D466F21E59CB280A7DB9FA5BA1B99ED1C1C,
	MediaPipeOptions_Clone_m4BDE025E942E472425387DEF8F63146B8C2B6EC4,
	MediaPipeOptions_Equals_m5A11BC1C09E74CC69DB83298DBA5003DD8A65366,
	MediaPipeOptions_Equals_mED7E91CF2EDBAFBCE5E1C2BA2374F0A4E5F48006,
	MediaPipeOptions_GetHashCode_m6C05D27EF2C543C70EC43C7DB712D028BDBE654E,
	MediaPipeOptions_ToString_m46611F2B164EAE51886F328CCC4213104A41C208,
	MediaPipeOptions_WriteTo_mB5CF6A852AD743614BD0B5CC247564DF783C1AC0,
	MediaPipeOptions_CalculateSize_m161CBE1D9EF8D1A82B6D3EFADF0533D1541F34D2,
	MediaPipeOptions_MergeFrom_m169687073AE0664B8B7070D1578ACB9A006A28FC,
	MediaPipeOptions_MergeFrom_m0F16A641B688B2AACE098CE2DD7ED6AF2A33B500,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	MediaPipeOptions__cctor_m7F965FC72ECCDD15394C89BFEC81EBF4905DA967,
	U3CU3Ec__cctor_m5D22BAF26B57E273903474C8689229B978182858,
	U3CU3Ec__ctor_m36646B8FD4D7499D410B9430484EFC45D1375DC2,
	U3CU3Ec_U3C_cctorU3Eb__30_0_mBACAF8EF4A02BFD71B435692FF09001E52CB714A,
	ModelMatrixReflection_get_Descriptor_mD9C8C9BB935082E7DDF59C2535BF29D61EE05739,
	ModelMatrixReflection__cctor_m71F86409CF9E479B7E72D29A85A039246F596905,
	TimedModelMatrixProto_get_Parser_m36CE8F4E4483F9F8CC74D4D7F9ADA697BC7B56FD,
	TimedModelMatrixProto_get_Descriptor_mE852310570DE30AD5DC66BA7B814104562692F2A,
	TimedModelMatrixProto_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m38D15F7A1B078003FDD4B34CD31563EB2018ACD6,
	TimedModelMatrixProto__ctor_mD36B8BF1A3255A20D213DB8179CE15F8B90FF922,
	TimedModelMatrixProto__ctor_m4DFAF5A291060B008DC81DDEB764F5FFC7501617,
	TimedModelMatrixProto_Clone_m000B8BFEEAA7E9C9B5F816E65A2E524BC67AAE48,
	TimedModelMatrixProto_get_MatrixEntries_m08C2E89BF4AB4BFAB2B8D9125AA8580C5F809657,
	TimedModelMatrixProto_get_TimeMsec_m4F95EE6B0A01EAD7B808C2E2E99DE3802FAB68F8,
	TimedModelMatrixProto_set_TimeMsec_mA0A9A1FEC24E1950CE80E25314D4E566A0C19F78,
	TimedModelMatrixProto_get_HasTimeMsec_m512CC886558F76F1E5BEF13F2CC883B4FAA27566,
	TimedModelMatrixProto_ClearTimeMsec_mD9625E2C729D54656E3F68610553639CADF865B0,
	TimedModelMatrixProto_get_Id_mFF676A6D32478DDE6B829B500488D517AAE3B0E6,
	TimedModelMatrixProto_set_Id_m5AD58896F59AD912A0807193212D4858A0CFDF71,
	TimedModelMatrixProto_get_HasId_mBA4F429F24ADCFAAEE54B59AAAF634996F31BEB7,
	TimedModelMatrixProto_ClearId_m2380F94A5FB0693F86B1C7856B0ED842B1C29156,
	TimedModelMatrixProto_Equals_m10573AEDA812F98D2A73FD3D99A8C7FBF788F49D,
	TimedModelMatrixProto_Equals_m8EAAFDB34A86DB247D89A253C9B5A9F969D6AF72,
	TimedModelMatrixProto_GetHashCode_m97FD870375066873755CDB0CD8C667D72EF2E26B,
	TimedModelMatrixProto_ToString_mF73FC8377B34FB364A3471DF34952AEDE0CCFAFB,
	TimedModelMatrixProto_WriteTo_mE3FC56B10FDEA6B63530855BADD6886B006346AC,
	TimedModelMatrixProto_CalculateSize_mD7343FF3B98B11A38CB8AD302EBFE05EE5E5B118,
	TimedModelMatrixProto_MergeFrom_m19C51BFF2F65BC61EC38D1EAA2E1C6212A57874E,
	TimedModelMatrixProto_MergeFrom_m6CFBC87CE67964F667F449E67BE3488D319F373C,
	TimedModelMatrixProto__cctor_m80283009FD75785A998DA98A5200F0CFDF42F765,
	U3CU3Ec__cctor_m021C5F20882A26700651DFF95A3760DA0CB314DC,
	U3CU3Ec__ctor_mC113B3BFF393A6EEFC1A3620DF5CACDA83A30DBE,
	U3CU3Ec_U3C_cctorU3Eb__44_0_mEDBD83BC00FB40FC45543FBA4F4CE7506EAE7BBF,
	TimedModelMatrixProtoList_get_Parser_mB272AC9FCB8C6A227D39C525CAB16E6EECE52FF4,
	TimedModelMatrixProtoList_get_Descriptor_mECA3CCA908B1CBDF9C0AEF003078069FB3C832C9,
	TimedModelMatrixProtoList_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m251842E44E7AC4A3C848B85C7A2A80DC420118B3,
	TimedModelMatrixProtoList__ctor_mAFE5CBDBDF8ABDE5ED930F230AE053920BA6E6E3,
	TimedModelMatrixProtoList__ctor_mFEE41524C860DD57FF001AC92B9BAAA89B731A2A,
	TimedModelMatrixProtoList_Clone_m78CD67CD3A408D9D83041DE3F507EE53552BAA05,
	TimedModelMatrixProtoList_get_ModelMatrix_mE3F2600E2A863BCB6B172313AA6D9C491A34D671,
	TimedModelMatrixProtoList_Equals_mE02E4E54D0605023A5A9B0D54412D1CCBBAC674B,
	TimedModelMatrixProtoList_Equals_m4B7E6B649A3541ACE89D9FF48E789EC4EF7E9871,
	TimedModelMatrixProtoList_GetHashCode_m39E976CEB249D20888704E62BE2A62F7AF48717B,
	TimedModelMatrixProtoList_ToString_m8A8860A75EBC20B5EECB9BD5D546DD26FA31AF23,
	TimedModelMatrixProtoList_WriteTo_mF9E2D1D5B4B98D4ECCD1CCD3B66353DC9FC0C4A7,
	TimedModelMatrixProtoList_CalculateSize_mDFA55794734AFC296355D7F42C8E1DEF5DA7F5FB,
	TimedModelMatrixProtoList_MergeFrom_m92F8B84288012603724555016046AEB0B943A1A5,
	TimedModelMatrixProtoList_MergeFrom_m4DF276D53054577C044EB5E3C85EC49FA2A4A248,
	TimedModelMatrixProtoList__cctor_m3A6D649A629118CB9E6C8ECF4E3FCD475946828A,
	U3CU3Ec__cctor_m968DE43BBB4B56E884777685F250ACBFF2C5AF80,
	U3CU3Ec__ctor_m624E1851124903C3A4424C16D17DFA0EAC504136,
	U3CU3Ec_U3C_cctorU3Eb__25_0_mAAFEE6408CC075C05B004657C1C9A38DEF115888,
	TimedVectorProto_get_Parser_m293D5CC4345A4B3EE0AAE0EDE281055B88B66A2C,
	TimedVectorProto_get_Descriptor_mE7B46225F5C8CCCA1B6EA7E0C56A0D85E6772B3B,
	TimedVectorProto_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_mD0373F1BCAF286F3781C73FF0343499BAFDB47C0,
	TimedVectorProto__ctor_m94A7FB7ED78DA1A40E402F3BB017C3E5AE052F23,
	TimedVectorProto__ctor_m7A2E61E4FE4C04BFDB2103BC80DA8857BE16A6AF,
	TimedVectorProto_Clone_mAA2BCF6CF36B58DD176F497A2D68C2EC34915E49,
	TimedVectorProto_get_VectorEntries_m51532A0FA06E931EA522A4D3349D25936382C809,
	TimedVectorProto_get_TimeMsec_mD446B35B77A78C5650A600B269C2F0DEFDFB026C,
	TimedVectorProto_set_TimeMsec_mE20840FD069CE1985A8B70BFFE00F7978A847E7A,
	TimedVectorProto_get_HasTimeMsec_m5CB3B26297FFF5D8E4A04D54E20EE7AFAC520E71,
	TimedVectorProto_ClearTimeMsec_m1B1C0E25E3372C399F5D14B10F2638788BC4F348,
	TimedVectorProto_get_Id_mE0A80B31956B38E6D78DDF13C03D7DB0B779037F,
	TimedVectorProto_set_Id_m9A8863F5728C3E38E681CC5E33F07E16739CD6DE,
	TimedVectorProto_get_HasId_m3FB553154CCDF6ACC4F9706396968B1B2E514C80,
	TimedVectorProto_ClearId_m40D90F7DE18B8A1340531B1C587E9FA67FBA3014,
	TimedVectorProto_Equals_m32738DC0662B6DD5CF4D2E5E97F07CD4D8137803,
	TimedVectorProto_Equals_m699DE31F04C68A804A9C34511BD3F83CC63BF990,
	TimedVectorProto_GetHashCode_mB2A72327CB3A9C28014E0DCCCCD9916B8184296C,
	TimedVectorProto_ToString_mD6CC9AC2C547209D3590CC7ED638AFAD4D4E1785,
	TimedVectorProto_WriteTo_mA00DD38C663E0821F0CF37CA65F2619F7B15396E,
	TimedVectorProto_CalculateSize_mCA0C76E99A476908DA0E9C2FA1CD7E00EE1314D5,
	TimedVectorProto_MergeFrom_m210826B18001527082F4BD2726C8E7B1755A5387,
	TimedVectorProto_MergeFrom_m3497545D1E9A10B36DBE67104C293FB1B37FAB18,
	TimedVectorProto__cctor_m09F76DABE7C25D66B1B3C0F840FBCFC43666CDB4,
	U3CU3Ec__cctor_m36D6E9C0857A5415DBF499C4641E04C4BC374783,
	U3CU3Ec__ctor_mD891B79761EF073DF171E16B795A3514101C9D8A,
	U3CU3Ec_U3C_cctorU3Eb__44_0_m03351A8842EF79476AD80BA810D53A56CCB21E43,
	TimedVectorProtoList_get_Parser_m57230E782BD81BF10D658AD6152E603A87375005,
	TimedVectorProtoList_get_Descriptor_mFF25C9CC211D75AC65106C0F7579763068FCD00F,
	TimedVectorProtoList_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m09C86C6D3E06E328B2A05DD9F39A0CE14F67C98C,
	TimedVectorProtoList__ctor_m7AFAE896482EDF3711A6DA99A412D64487EF2F91,
	TimedVectorProtoList__ctor_mBB786126903E375611CE37FB8B7ADB4030EA0C1E,
	TimedVectorProtoList_Clone_mA692C92F4F46F28CF480F28E95932FEE2130FDA8,
	TimedVectorProtoList_get_VectorList_m4461A0ABEB8795C098A873555BED0ED73ADF7EEA,
	TimedVectorProtoList_Equals_m70165213BF6F474B1105E05389301339CF9CA6ED,
	TimedVectorProtoList_Equals_m090C5D733BA5CA3D26DCFC59F13DD2C413025B4A,
	TimedVectorProtoList_GetHashCode_mBE5776B4DEF84AD23EAE943A16440C264D819347,
	TimedVectorProtoList_ToString_m89D6D023EF065EB7214BD2F6C4A5B793AEC47BBA,
	TimedVectorProtoList_WriteTo_m7883C3162842BB6B2D11C97A03E61ED8B0625074,
	TimedVectorProtoList_CalculateSize_mC4270F8CBA1A98987570383F71806D85E13AC42F,
	TimedVectorProtoList_MergeFrom_m1C123548C738E2DEE42C6BDF207533DD92EE55D5,
	TimedVectorProtoList_MergeFrom_m5F9AFAA0BE3F80732ED106238D19067945C8E53A,
	TimedVectorProtoList__cctor_m745BAE5C5B8A8BB2D922AF86261FF88BB9A43258,
	U3CU3Ec__cctor_m2D50D56B0E7F848BB9943F836A7E6ACD6E8B1CAB,
	U3CU3Ec__ctor_mA2733139AAB03874B7D6868B2FE3D01DB9CB190F,
	U3CU3Ec_U3C_cctorU3Eb__25_0_mF710D86AB9B4657C5EFAB71C6EFDC5CFC2F1ACF0,
	PacketFactoryReflection_get_Descriptor_m1BB07D7607F88807B3994AB7F590485B274CA3A8,
	PacketFactoryReflection__cctor_m1D46448F82B565E2382622429C818F969DB90C6D,
	PacketFactoryOptions_get__Extensions_m60DE38FB47E4AD1A028014468E4604009DDEF545,
	PacketFactoryOptions_get_Parser_m1B98972F75A63C47F0B8A548C6AF1CF224F12113,
	PacketFactoryOptions_get_Descriptor_mCEC94D2EB7FE66068BAE199FAD78CBA8520E4D70,
	PacketFactoryOptions_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m19C4EEC187A9464F2089B7C7F61BFD9C1D6BE8F8,
	PacketFactoryOptions__ctor_mC8536EB222928EED726DC5B86DDECD96B930DAB0,
	PacketFactoryOptions__ctor_m2D36055064EC0015518ED210217D3E3E04CB050F,
	PacketFactoryOptions_Clone_m81FDE11474A9B170510320D3F0F5D581E367B408,
	PacketFactoryOptions_Equals_mE75139A5773D01442C993D3D8C76EF65159320E9,
	PacketFactoryOptions_Equals_mB3693914B1867B7CB7DE1969EA290CEA81D43E79,
	PacketFactoryOptions_GetHashCode_m21D7D747BBA561125D4372391A037A706D72734E,
	PacketFactoryOptions_ToString_m55FF16DB4F9E721D5593642FE08504579D0F4D53,
	PacketFactoryOptions_WriteTo_m9D660E8331CCC27AAE98146FD46C62C3E702FECD,
	PacketFactoryOptions_CalculateSize_m269B700BA480E8E38BF52DBF256336CF709EBD72,
	PacketFactoryOptions_MergeFrom_m0D0FDA7834EBF6390A437209C82B5E59A32E1843,
	PacketFactoryOptions_MergeFrom_m6AB31546F93E43834CD182777A573B5FD0F0B284,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	PacketFactoryOptions__cctor_m4E7C68E575409A82D4F0E4ABA2B5F83CDDEAF4DE,
	U3CU3Ec__cctor_mCFFF90F9EFC8C6FE9FB5E388EB69C374C7894314,
	U3CU3Ec__ctor_mD8F7D8C6A9A5F7096C8CC9B7DD27DBBEDF869E8E,
	U3CU3Ec_U3C_cctorU3Eb__30_0_mEA2F1E0FF3C5BAD770ACA147EC662CD100BA1ACE,
	PacketFactoryConfig_get_Parser_m81229A5DA8B4F72497F78F8C752358E20AA52C35,
	PacketFactoryConfig_get_Descriptor_m78E47195E796521090796DBDF8DF85ADAA985841,
	PacketFactoryConfig_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m52BF184CA45BFF0F3BEFA95AFDC1E37AAFDF059D,
	PacketFactoryConfig__ctor_mB19EA1662F94AA4F76B3E366F68C4BEF5DE34E8A,
	PacketFactoryConfig__ctor_m16A584150178AEF55F2E4F659D408B01ABACB481,
	PacketFactoryConfig_Clone_m27D51A4834DC6F5343FF317372508AC2FA7B08EF,
	PacketFactoryConfig_get_PacketFactory_m432AA4EAF7D3C26A29D75CC3FBE3539BD2F8508E,
	PacketFactoryConfig_set_PacketFactory_m1C46207C47EAABBA519A8564DDA9882FB113DE3D,
	PacketFactoryConfig_get_HasPacketFactory_m8EDC6B395D2D72B7FB6EDC7163D8865B90B98BF1,
	PacketFactoryConfig_ClearPacketFactory_m82423E2A65876B507F2260501E7B5FE4871B5FD4,
	PacketFactoryConfig_get_OutputSidePacket_m1ADDCCAF3521D72719F51E594C5D93AE121FCE4D,
	PacketFactoryConfig_set_OutputSidePacket_m2B7435F6070ED9DB2F2A4E1D2904C1A68A855B80,
	PacketFactoryConfig_get_HasOutputSidePacket_m99DB84E3F9189FAA1803283C8F711D1CCA87039E,
	PacketFactoryConfig_ClearOutputSidePacket_mF630C0F005F394FA52F5FEEAC784C45B2A9F400D,
	PacketFactoryConfig_get_ExternalOutput_m524F197CF49D7DEE2E6D73FCDB1E318511763D84,
	PacketFactoryConfig_set_ExternalOutput_m16CFBCC7E111C99EB84BDF532FF6E71EBCC5AB6A,
	PacketFactoryConfig_get_HasExternalOutput_m584B2D31D33522EB2A8CF18EE764D3F79868F8AF,
	PacketFactoryConfig_ClearExternalOutput_mDFEABBD4FE8E94CE783946760A19F73B23B7099D,
	PacketFactoryConfig_get_Options_m892546CFD591060DAA650AB7E0DD477A9717F6CC,
	PacketFactoryConfig_set_Options_m6EE8262386BD29700E596C1E2A6F9660D4207426,
	PacketFactoryConfig_get_HasOptions_m79F66DDEB940C1F4E1516383115DF857D9EB8218,
	PacketFactoryConfig_ClearOptions_mFF0B8AD05101A7B34DEB196164800C630203DF72,
	PacketFactoryConfig_Equals_m2F80DE3C163CE19D35574287766C1E85FEBA44A0,
	PacketFactoryConfig_Equals_m0FEDD1353DBB452259CACE6FFF69AAC41D5EDC3D,
	PacketFactoryConfig_GetHashCode_m037E8FB9D77E8B663BB9490F53F7746B5954FD01,
	PacketFactoryConfig_ToString_m081F5FBD927673C09D7F63168C6A45C50CBBF37C,
	PacketFactoryConfig_WriteTo_m182894E780B31E210F4E177DAE8F2438CEC90D56,
	PacketFactoryConfig_CalculateSize_m989B4B743104D755C0E33BBFD98C7F2B734AC4D5,
	PacketFactoryConfig_MergeFrom_mE285F8F83524EF28E8C0F1B20CCC431D5E9D61A8,
	PacketFactoryConfig_MergeFrom_mABEF4433CD66253E3B53FE07C3937BC2E9A8513D,
	PacketFactoryConfig__cctor_m91D07E016A6E203AC2C98267755ED68A66AE9879,
	U3CU3Ec__cctor_m5A99475F562F36BE8ADBA919636A35F46035EB07,
	U3CU3Ec__ctor_mEBED62C34AD5CCC041D25181A3038E1BCE4F57F8,
	U3CU3Ec_U3C_cctorU3Eb__55_0_m06C25A7D8D61224A8E6F75CBA553029880D6693D,
	PacketManagerConfig_get_Parser_m11CE5669B52F0C46E21EA3692708F2657B344F20,
	PacketManagerConfig_get_Descriptor_m9E0E19AD41EB3F4EA79BF043F3F84A9980419CB2,
	PacketManagerConfig_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m5D187D712B9E1E6A9E3B27A0C8E53BC412B3372D,
	PacketManagerConfig__ctor_m882F227FF8DBA0BF8E8743CE05BDC22CE76E7644,
	PacketManagerConfig__ctor_m9878FDFCDCEC6AF84637F38F1CBEA9DD5F5EE570,
	PacketManagerConfig_Clone_m4E2F5485BEF8CE62F49391651D7448EDC63F3093,
	PacketManagerConfig_get_Packet_m32D6AD32AFBA86132A69C07EC032A4F8DCBF79C3,
	PacketManagerConfig_Equals_m11D2FCCE2BC53BE6796B761C6DF983D05199371B,
	PacketManagerConfig_Equals_m448C26325B5F0F63AD1511314AD36AE88FF75B53,
	PacketManagerConfig_GetHashCode_mF97E5240FC7AC7599CF9878408E09169FD99F992,
	PacketManagerConfig_ToString_mB91FC8BD3FD4FA215BB48D84E60A40ECD8FE301A,
	PacketManagerConfig_WriteTo_mC1BB483BA2A79844AAC30C9902ADD2178C765B9A,
	PacketManagerConfig_CalculateSize_m9B17003F4E07D2FBA15072AA9FA611E9808C9CEE,
	PacketManagerConfig_MergeFrom_mFCFF46E5C7B7FF00042DB64CA4117549B4DA6379,
	PacketManagerConfig_MergeFrom_m47C7CA9A0C39ABF84078F6AE1383CD0D40816174,
	PacketManagerConfig__cctor_m96C7834A426C31FB2FF996C26C63835E3D7BC974,
	U3CU3Ec__cctor_mE9FC1493E664445AEF6DB6FFD6E0AA5040C8EBB4,
	U3CU3Ec__ctor_m747130ADF16A252D428EFD36E215B0A0B61CE5C7,
	U3CU3Ec_U3C_cctorU3Eb__25_0_m52AFA52E9EC5CD5D052E7C0A665D1E5260E990F1,
	PacketGeneratorReflection_get_Descriptor_mC95A1DE28E3DABB652C27D99BF92F449C18B160B,
	PacketGeneratorReflection__cctor_mAFEA5BC53DD28EF0CCCCC108C9CD1DB2181D6249,
	PacketGeneratorOptions_get__Extensions_m4778AF35D0CADC27D1735741F17556260D66C471,
	PacketGeneratorOptions_get_Parser_mC6F315DEBB7D778C70563F4543099732E4302310,
	PacketGeneratorOptions_get_Descriptor_m10CC59CD22798F54F0DF2A34A4CD87EB5420FF68,
	PacketGeneratorOptions_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_mFA5549DF6A9568DAF3F0A09D542C4D5BBFEAE78A,
	PacketGeneratorOptions__ctor_mC663EB5FC659CA75449E64A002B4CC10C15360AF,
	PacketGeneratorOptions__ctor_m74543604138A39F4D112E514CE0C9DD4B53BCF39,
	PacketGeneratorOptions_Clone_m058684255081A07E04B4D5EBB30D264D95C2C2BB,
	PacketGeneratorOptions_Equals_m7695E0C7DB1C89DF8F186CB8265472DA3469D9F7,
	PacketGeneratorOptions_Equals_mB3A3471F6EE5C8A3374CBE6DDAD98102A07FBF37,
	PacketGeneratorOptions_GetHashCode_m23D64E3A72F45C6FF8E682520689880DDC187C9F,
	PacketGeneratorOptions_ToString_mE964D3F9F6EC1FEA51DD82143A1F65695F253A18,
	PacketGeneratorOptions_WriteTo_mA2C4C9ABD31A321D10AFA8D9841EEAAA230FE944,
	PacketGeneratorOptions_CalculateSize_m52382B6749DB349EF633CC5ECFC63AF2A00C82CE,
	PacketGeneratorOptions_MergeFrom_m554F607119A2A19278375B13E27CD2AF9B9D9AC5,
	PacketGeneratorOptions_MergeFrom_m0CADE6A187BDA996B2480109B50A87D215468C2C,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	PacketGeneratorOptions__cctor_m1E4A3CD0D1B9F83B19D19D5757F562F49995E7CE,
	U3CU3Ec__cctor_m186865A85B26E8B572E1B9D5D993983DE0154450,
	U3CU3Ec__ctor_m4079A03D3B93B3A72E54D2AB03E6D4F0F54A0FB3,
	U3CU3Ec_U3C_cctorU3Eb__30_0_m4314F404BA59DCE90DDCCE5F6063963DE36982E9,
	PacketGeneratorConfig_get_Parser_m7B8D015FE1EEEB6818CCF0006BBDDFC3F309BD34,
	PacketGeneratorConfig_get_Descriptor_mA8E045699A747056B6927B72B44B025D1ED045DA,
	PacketGeneratorConfig_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_mBA6ABA69D1E99F666D3CEBCF5276482C0089440D,
	PacketGeneratorConfig__ctor_m3DD06F90644406E25B61D2CD6105787D7A59DC2E,
	PacketGeneratorConfig__ctor_m6380447A5817E1DB9BA8031BC40A897E3EC7FADB,
	PacketGeneratorConfig_Clone_m7862B8297F86CA31E1A69BC23BBA6819771AB397,
	PacketGeneratorConfig_get_PacketGenerator_m992D054B0C10110EEE8CB8A3FA9533A4F53A74B2,
	PacketGeneratorConfig_set_PacketGenerator_m354D3E2DA87E98A12223838512C56043AACDC01F,
	PacketGeneratorConfig_get_HasPacketGenerator_m532EFF69B798BB837F79A513F185294151C88F11,
	PacketGeneratorConfig_ClearPacketGenerator_m8F3C91BC62A1B11E3AFAB4AC8CDB798997E4927A,
	PacketGeneratorConfig_get_InputSidePacket_m0A3AE6A3601D07A81352FFD4C776228323960B7B,
	PacketGeneratorConfig_get_ExternalInput_mC2AE53911DDD18A8433EE0A769D3300BE66D6907,
	PacketGeneratorConfig_get_OutputSidePacket_m488C93E85C0A7A580449AFF8E53A21D8521CAAF8,
	PacketGeneratorConfig_get_ExternalOutput_m8681B870FD63D25DD093CD1001210AAF2371ED21,
	PacketGeneratorConfig_get_Options_mD583370058BA43BDC9FFBD0AF4E9121B64681847,
	PacketGeneratorConfig_set_Options_m72F65FBB17D99B6F3E2E06E3F99425223A140663,
	PacketGeneratorConfig_get_HasOptions_m7A6B1FDEB2175A5FC8135348F8EA37B8941F21BB,
	PacketGeneratorConfig_ClearOptions_m397FE10C8A8D3BC5CF70E4D76CA66579F88946AF,
	PacketGeneratorConfig_Equals_mFF71F177C5357C6ADF18AE90B67B1B030D2B230D,
	PacketGeneratorConfig_Equals_mAFF8CA1140FFA35F3A7F59B55F34C582BB5BECC7,
	PacketGeneratorConfig_GetHashCode_m67C2EE0089FDA8A17452E8789A9226BF7497F2EB,
	PacketGeneratorConfig_ToString_mF8E0F155AB39F730DC8D028AEBF3F122AC9CF76C,
	PacketGeneratorConfig_WriteTo_mAD2E8F441993B1349F73DB355982F3018A9826DB,
	PacketGeneratorConfig_CalculateSize_m42D473EA3CB305950C349CC960667081855F7408,
	PacketGeneratorConfig_MergeFrom_m56A617696F27E0E76700262130434884884C864A,
	PacketGeneratorConfig_MergeFrom_m972AAB330709CD60D6DF3A296350D7FE9154B6D9,
	PacketGeneratorConfig__cctor_mFBDEB1E40903E08E7FD2FAC611014A680BEACC32,
	U3CU3Ec__cctor_mEE67B3B661901A8BC6025CF19F1144BB06497D45,
	U3CU3Ec__ctor_mB9514EB904F901E35D475C4C403C3D4CF5B6217E,
	U3CU3Ec_U3C_cctorU3Eb__57_0_m291094B7EF2EA14F0D2CB07F2D3FB3E98978B5F4,
	RasterizationReflection_get_Descriptor_mED8B75EE232C007AB76BA566165C6783907DB56B,
	RasterizationReflection__cctor_m325F48A90228FB52DAC3930158A973B499922D52,
	Rasterization_get_Parser_mF17E0B95B210885A54766F4E357E35BBE3FE9AE5,
	Rasterization_get_Descriptor_m722F6AA571B13EF64011AA0035AF1A22A6ABD84D,
	Rasterization_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m792160B1139E20599CE99C4B313DBE88FAD22FA8,
	Rasterization__ctor_m55FC6BC6C94655B9C8CE0385CB44C1E81716DBB7,
	Rasterization__ctor_mD3697404E5C77CE873F448E92932C6CC0C18681C,
	Rasterization_Clone_m72998D69821C34E51500317806C6B8EF5C86EC6C,
	Rasterization_get_Interval_m2CB53A337C5EA0AB68072018D2A5C64B1BC58423,
	Rasterization_Equals_mA13E8A4D5D7B8915FF57D3B9C72C364DB37A2F1A,
	Rasterization_Equals_m7DAE4E1872E2285E1981E16DC001030FC42A1EFB,
	Rasterization_GetHashCode_m7C644190483001AC74DD0288F66F99E3993351F5,
	Rasterization_ToString_m0020CC92D76AC7E76D9770A619E69E0C9E22B2B3,
	Rasterization_WriteTo_mA8B9BAFEBF0E6AAC7056B5922700F05AAFDFD6E4,
	Rasterization_CalculateSize_m136A9B8A9D2FBF3A682F7BF2053C1588FA35AB5D,
	Rasterization_MergeFrom_m70BDB2F744263C31C8E738B4122D7FDE11E0225C,
	Rasterization_MergeFrom_mF5B9FC0CBCF3484AD858AC847AB6712619FADE13,
	Rasterization__cctor_m61CAE0FE43758C68B8B29ADB512E5B0F243BDFF2,
	Interval_get_Parser_m9BB1C9B7DBEA3582477E6ECE45458AC973D0FF08,
	Interval_get_Descriptor_m13DAA3A651E004E4B34DE9C8844C58178454629F,
	Interval_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m9B390C253B12875EFCB1597893655A61167839F2,
	Interval__ctor_m32A3957EBFFE3B98D584CE5835A00D77CEA1F3BE,
	Interval__ctor_m5E2DC10FD5A9373055DC59F91A2DF4AE47E32644,
	Interval_Clone_m07446964FC63DB770BDDB2850439BF01C88B3E53,
	Interval_get_Y_mE60537C37315F397FFFE75FC400739106D8154DE,
	Interval_set_Y_m9056F9DD99CE66F68D1682111DBBA63CA322BA3B,
	Interval_get_HasY_m2F6B1FFAA8CF48ED2CBE05E4283259D739D8C9D4,
	Interval_ClearY_m9A1D81264BB6A7171F645F141D26B16B11FF59A1,
	Interval_get_LeftX_mECB95906533EC0F1C6F866388AA6DD6D65D4F6B2,
	Interval_set_LeftX_mE7317A98C5B6B365086EEFD0A750A5C407822256,
	Interval_get_HasLeftX_m4CD8A6DC318305E3C87C472F77441861E05C3E9C,
	Interval_ClearLeftX_m18139A501E8C9A583BEA41E83544720908938204,
	Interval_get_RightX_mF210A4D27C03E8CF92AFA170A6A961E61073FB7F,
	Interval_set_RightX_mBB294AAC7E8A210CD10F7C4B77BEACBE22DD5F6A,
	Interval_get_HasRightX_m6942D9B35707AAD4B6353A8ED6D18747F3670B9E,
	Interval_ClearRightX_m6EF10A9D5EA463F0C7E06C15932A66A8A7974662,
	Interval_Equals_m694D5F474327657E1C30F07C2AF9F10C9B24F43F,
	Interval_Equals_mFE99EB6740C0735C445F4AADA222DD792A4E98E4,
	Interval_GetHashCode_mC8E3DB33E588E151786C01B8160CE01B2898412A,
	Interval_ToString_m01323C62EB2CA2032A43994675B1F80BC6868CCC,
	Interval_WriteTo_m501236942A2D0E72E74799CD107E6C04E780731B,
	Interval_CalculateSize_m1A75A81A7A6EB5AB4EDF015348B58F3CC4C7DE95,
	Interval_MergeFrom_m8E8A02B6433AD6798D67E8F86A48B73039E311C1,
	Interval_MergeFrom_m08CA5B392DC43CB8FC5959A8E28146884E6C7376,
	Interval__cctor_m69FE3EEDBBE88E37B805160892C7C88F6335A8D2,
	U3CU3Ec__cctor_m4D8B1239895A7ECC649E59B2460B5218E66AEDC8,
	U3CU3Ec__ctor_m1107224156F8BAC6FC35668C2FA35993C8A59EAA,
	U3CU3Ec_U3C_cctorU3Eb__48_0_m8D40D169037998FA085C6B31D7B3F70EBD780099,
	U3CU3Ec__cctor_m1207FFA106C945365F362EBB9CAC8E845AB64190,
	U3CU3Ec__ctor_mE3CA274DBB36540EE7BA9F1E4A90F76BC5110DB6,
	U3CU3Ec_U3C_cctorU3Eb__26_0_m50047FFDC32D1DB5383C34C58540F56FD8161860,
	RectReflection_get_Descriptor_mF9B24077B7445DB6263D0732DD142E06A8F2DC25,
	RectReflection__cctor_mA8E9429E6566D2A8A554D052934AEB9425BA8632,
	Rect_get_Parser_m98235111ED335DB876DFFC45B10E69E395B391EA,
	Rect_get_Descriptor_m462AC6B7F7026C17CB1E522A321AE1C81E1C4FDD,
	Rect_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_mFAD8D84C1243E352DB00110738FD51648FC9C1C5,
	Rect__ctor_mA1A953C00E9D1E93F5FE705F28869BF03281E90E,
	Rect__ctor_m3ABE7AB6875A28451F7D68FCD2316A5FFD6A33CF,
	Rect_Clone_m32E8FD503464BCE245D992DC44220A17FCD192CF,
	Rect_get_XCenter_m7B125BE5C07A964D070D0F3C8ECDBC8F87CD8F0B,
	Rect_set_XCenter_m2120BA596A84FA7E3598D3F4A6F6624D04786635,
	Rect_get_HasXCenter_m3218ED325A15C35B1E3EDAF3DBA662856BC70E1F,
	Rect_ClearXCenter_m907A2A14A98D9BE8337AB11E1E11BA6D82CA38C9,
	Rect_get_YCenter_m4CA9C08BD965EA7E5DEC233AB2FF595EC39CBDD3,
	Rect_set_YCenter_m88D22E5D111BDF8E7243CA4BECED6916C1B0CCCD,
	Rect_get_HasYCenter_m70A28BCAF43C97F630C363D2B815D0796102BAAB,
	Rect_ClearYCenter_mFC560214642EE24645202364FB8544833F84C9D9,
	Rect_get_Height_m354CC79D4285C99A61C349C319082E927EBDD731,
	Rect_set_Height_mABA93868A4D03A9434064DEB6CCC2F86B0B94A79,
	Rect_get_HasHeight_m75F4767CFD54C653B3CAB9B5E4D4C16B6B938B8F,
	Rect_ClearHeight_m5311A4542887AA3987EAEB56C1EE174BBC46EF90,
	Rect_get_Width_m8899354713E96B92C8D358518DA1928042653B6E,
	Rect_set_Width_m850CC0CCC9F1EA823228AE735E63F0923A862EB6,
	Rect_get_HasWidth_mF1BDF1A0C5E255A2C508BF92ADB03587421A4387,
	Rect_ClearWidth_m2A0EF58EE901101AFA25FA5ED9C7EE9F7990E0D3,
	Rect_get_Rotation_m38557895C2DD75F0C8C844AB4541EBC45CE1DB05,
	Rect_set_Rotation_m34CBE8690264C435A2874A794027194AF54EAE53,
	Rect_get_HasRotation_m44421D8283397F738D6AFDF92CD6577589047A7E,
	Rect_ClearRotation_m935A6E03D8225670E9D90057502E99B479893CBA,
	Rect_get_RectId_mCBCDFF45E1CCF88B409AE312B9B794C2A5C699F6,
	Rect_set_RectId_m4D54E87B39A1FB478BC1C2607000735210C6230E,
	Rect_get_HasRectId_m9EADD9046C8F9FD205BD27AD74070F47223038C6,
	Rect_ClearRectId_m2582A0FD1E0F7B073ACCB7C566AD4CD67810589B,
	Rect_Equals_m8D8318A274A804C8F7EC5995536C2C5AA121DAE4,
	Rect_Equals_m8DDA30D9B6EFF7387D1E42982812D783565B971A,
	Rect_GetHashCode_m894D11D8F145F7800CD41C46D4923F6701EA73BF,
	Rect_ToString_m9026BF1BE9688FBA3BBAA49AACA2F0624BE20546,
	Rect_WriteTo_m91BA7F768E68CEA159FE04DC30808C0E98FDBBF2,
	Rect_CalculateSize_m458355AAEB1961AA3E49528C669FC23E89FF06F0,
	Rect_MergeFrom_m04A5B2CFE20DC31FF14B0EB09212D6B01842C229,
	Rect_MergeFrom_m7AF35F7AF17270258910584DC176688CC509CA98,
	Rect__cctor_mA75B58B48EED3CF9E565852DB6D5448583F56927,
	U3CU3Ec__cctor_m4A4770B331822B562EE5EE193C32750E283BE3DF,
	U3CU3Ec__ctor_m5459C7F8EE7CCCE8F83DB2117E7FABB6F056F5AC,
	U3CU3Ec_U3C_cctorU3Eb__75_0_m6AB82AD17BC161E3224D5F4D5F27712D66289770,
	NormalizedRect_get_Parser_m8C13751E7D2D14ABE164CF14742E5A87A26F6275,
	NormalizedRect_get_Descriptor_m14C5F42230492AD20E3F307E87F2849CCC0C32CC,
	NormalizedRect_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_mA0E18336F15C80009F18BBBBFF8B939AEE6AAF3E,
	NormalizedRect__ctor_m3BDEDD59629DCC814DE948B03777DC7E9919548F,
	NormalizedRect__ctor_m8EA938ADE44DBD9BA74A4EA26BFF929BA398F502,
	NormalizedRect_Clone_m8B1DF6560577979C5C4AD7B9924DE8A9682BDA7D,
	NormalizedRect_get_XCenter_m402549B47AE4A14920F5BD959658802A52DAC077,
	NormalizedRect_set_XCenter_mB4D666921287D5399935D8B0A221EAE4C874BE6E,
	NormalizedRect_get_HasXCenter_m4A29DB626B114ECBABE4872E2C8A39D22D2B66CF,
	NormalizedRect_ClearXCenter_m6BB5D3B7A5D3DEE0964F2ADB0EFB015498C971D3,
	NormalizedRect_get_YCenter_mCE364A74C667791CE1D87E9C9747595959BA42BC,
	NormalizedRect_set_YCenter_mAB38FF130FD73E0B3F64FEFEE9023D2385CA9170,
	NormalizedRect_get_HasYCenter_m81A458F976C4C1A6E5663AD88DDB06271AD4DA0B,
	NormalizedRect_ClearYCenter_m964FB56A39DF73D2EBABDF9B3AB8E4B525BF68BB,
	NormalizedRect_get_Height_m749962C2051B61123CF5A9F3821B0D87B5ECA65F,
	NormalizedRect_set_Height_mF1E3A66160401E5A69416DCD3039CB901259811F,
	NormalizedRect_get_HasHeight_m18CC31339EED4358DD685551B8315F114CAA4F43,
	NormalizedRect_ClearHeight_m53F519FBB27E41F1015BBF0F9B4F943CB99D521E,
	NormalizedRect_get_Width_m6FE64C8C882BBDF83DBEC78ECBA73E6850D66F11,
	NormalizedRect_set_Width_mFE5B0905BFE8C200A855A21F8E53C2BD5676968E,
	NormalizedRect_get_HasWidth_mC2EC84AA440FC725023F4DF01D0C720428F9C933,
	NormalizedRect_ClearWidth_m57AD3453E31C13D518C962CB8E89D347AC9A05F5,
	NormalizedRect_get_Rotation_m061EFC7302DC903A7829CB4001298B5D6FEC949A,
	NormalizedRect_set_Rotation_mE9365876335B35270FE3DFC096232081F0424E0E,
	NormalizedRect_get_HasRotation_m34EC10DD68E324B40C0087FA8C6330363BA95900,
	NormalizedRect_ClearRotation_m7743803A9EE445B91B7637283BAC2D3783CC3323,
	NormalizedRect_get_RectId_m3FEE336967193E05AC2ED9E4AFE4FD0A941B689C,
	NormalizedRect_set_RectId_m368D8164D61251708A194018D3E79AE0A5760E79,
	NormalizedRect_get_HasRectId_mF05F42B5D6694AE579E7434BF4AC33E45BA6CE34,
	NormalizedRect_ClearRectId_m1C7E71F564CAE806DEC00BFA1C8EC5E9A7C76006,
	NormalizedRect_Equals_m5A0389CD1BF8AFEEBA109FE3BC72A84F2B69E2BE,
	NormalizedRect_Equals_m4650183705C1C20A3686FCE49AA990E2CCFA9167,
	NormalizedRect_GetHashCode_m2835A32974C581C5AACF9EA8640D3499FAE29B5A,
	NormalizedRect_ToString_mD9680EE1C02BBFA4E938610290B3E2AAA4AAA896,
	NormalizedRect_WriteTo_m223F03100F382A13B791A585F64ABB578CB91BF3,
	NormalizedRect_CalculateSize_mBE22B1A36E88C86ED0DB31FBEE65AB6B6A35CB63,
	NormalizedRect_MergeFrom_mDBA715172C2C850F99074679573E1BB7FF7589E1,
	NormalizedRect_MergeFrom_m0469B3573A2B64594C4690D3E91CD1C41EFD382E,
	NormalizedRect__cctor_mCE05B178C2744B9C6ED2B2B85E6FC100EF16D42E,
	U3CU3Ec__cctor_m5C21ECDB600B1693862D47658B399EB51C1D68F6,
	U3CU3Ec__ctor_mBF363638E935466AD719821B3D5B7CC45532C0D9,
	U3CU3Ec_U3C_cctorU3Eb__75_0_m034146C904E2BE74D27E9BAC8D5B181291971C7C,
	StatusHandlerReflection_get_Descriptor_m7EEC754F9336FC07559A4B3701EF8BD34C8187A5,
	StatusHandlerReflection__cctor_m4790B66CFE436359ADF4912A63A22D1FD66CF4C1,
	StatusHandlerConfig_get_Parser_m0A4A67DD4165867318F98B1B6C915263D380FE23,
	StatusHandlerConfig_get_Descriptor_m4E6AA499370AD10699F80524D6AF4F7D89865B64,
	StatusHandlerConfig_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_mA1108C988D2F9AD80EEE293BA13D2762F0396EF3,
	StatusHandlerConfig__ctor_m0C87DFFEF9B52D562AD68762DD83C895E084F1EC,
	StatusHandlerConfig__ctor_mC337F4A5270315F9DF7FE4372468EDE59F5D9B1B,
	StatusHandlerConfig_Clone_mD7BF122D61DDC2B08ACA4521444F692937EB82C1,
	StatusHandlerConfig_get_StatusHandler_m09C76869F2B90C570AFF218077E95724D57F9242,
	StatusHandlerConfig_set_StatusHandler_mC8C9625D755F9B836F6A6B7FF1E34C3646A2BDFF,
	StatusHandlerConfig_get_HasStatusHandler_mC33C08CC036C1996A6A7D6734109E8C0D70A529F,
	StatusHandlerConfig_ClearStatusHandler_m07CC6662A252212369F7D57B3E3A518A8F8DCA2B,
	StatusHandlerConfig_get_InputSidePacket_m88B8E7820B498FB8B3282A614871D62F9790E044,
	StatusHandlerConfig_get_ExternalInput_m60A2B1A8CB82571741D813C858F7871C9A8D7802,
	StatusHandlerConfig_get_Options_m9556B0C27EF60D5311FAB6BF7DE543D21FFF7135,
	StatusHandlerConfig_set_Options_m5EE39D6ABEBEAB79EDE6EB845713E23037AC4820,
	StatusHandlerConfig_get_HasOptions_m7598A008AB1AECA47E1D63037E89206E9E712DAD,
	StatusHandlerConfig_ClearOptions_mE947E969C224326EA97B7903F732FEC89BFF1176,
	StatusHandlerConfig_Equals_mE15F141F7DD9A8E50CE22E98E94843557421C569,
	StatusHandlerConfig_Equals_mD1A6F9828313594E5C7EF144F5119F54EEE0435A,
	StatusHandlerConfig_GetHashCode_mF6A6AD649A9387349F6B83D7844CDAECC44AA4C4,
	StatusHandlerConfig_ToString_m128817CC910667012E7587B84888E8EB925DE190,
	StatusHandlerConfig_WriteTo_m160C08E410413EC1615505EF407B79F1557ACE34,
	StatusHandlerConfig_CalculateSize_mA82A950CEEDBF1E241EFEF0B855551966114782D,
	StatusHandlerConfig_MergeFrom_m6F38CBD207080A9DE0CF68449CCC1AA881877239,
	StatusHandlerConfig_MergeFrom_mAAE549448B55E008DBF0395D6BB87C183EF0ACCE,
	StatusHandlerConfig__cctor_mAC512BE0806197983D1A42BF1E7DDBDD7100756B,
	U3CU3Ec__cctor_m21D5794CD8B001010BB8BD89B14BFDF7F0AACECA,
	U3CU3Ec__ctor_m26EA9557C770A91C908FF088A128F912825D6F72,
	U3CU3Ec_U3C_cctorU3Eb__47_0_m52B50B745248EBEB07E844523F4248CBF7F2934C,
	StickerBufferReflection_get_Descriptor_mB3FE44F901AC3DB3714B7D33084016FD50A91D5D,
	StickerBufferReflection__cctor_m8F46B00F2E0C5322E9DD9CBB62766CC1FC2F50C5,
	Sticker_get_Parser_mC18FF538852660F306ACCD7DC67172B8C29EC13B,
	Sticker_get_Descriptor_mA98C9309CD1B3867D18CAE0376F1E3FB2D72E607,
	Sticker_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m04DD9EAF9FD06741B9792A4F74D74481EF989D5A,
	Sticker__ctor_m9F07EA56D9484EC587B82322B0E0718DF88BF800,
	Sticker__ctor_m2E998EAE290A49D1067301693237681798D172D9,
	Sticker_Clone_mC616C2DA4B6978BDCE602EC25D2AEBEAC58CC7B2,
	Sticker_get_Id_m6A6F305A3A97EA660205F61C2E87E84EC7E66B23,
	Sticker_set_Id_m888A78065D7262BFE9DA550FA12D51ABFBA10279,
	Sticker_get_HasId_mDBBD47AE5C6B20B0448521A24696CF329162CB90,
	Sticker_ClearId_m9A821A6CA49F2B6488490BC5012C1A372AA225B2,
	Sticker_get_X_m6A2F7BCF32D0E5A6D91635B7D813FBD1A4F339C3,
	Sticker_set_X_mF9572948EB2207976B7E87C8CFF59194E31E6AD3,
	Sticker_get_HasX_m9C9CB7821D3DC1F5702936CBD837599F52F8DF5A,
	Sticker_ClearX_m81A2073EC1E6E28D522BA83A09D25B59054C10B4,
	Sticker_get_Y_m1CCB1E96BC8AA1023A9CEA6AAC206F4ADA001939,
	Sticker_set_Y_mCA7DAB1AA71BA2D84DC59B5A93111F63C38FD900,
	Sticker_get_HasY_mB877A78ECFA2DAC7E9F88B467A270282ABBEE4BF,
	Sticker_ClearY_mCCDB6ED5BBCC5470C536E9308EEDBF03EBC5A1FC,
	Sticker_get_Rotation_mB99D72196F6DABFD1AE85692ED9168248F3D294E,
	Sticker_set_Rotation_m1AEF75979F94284AB27A69B73EF48FC7F19EFFF0,
	Sticker_get_HasRotation_m2AD7D36D27D501DA435A9DC0E79060FBDF555034,
	Sticker_ClearRotation_m706F6BE276F0822E52C34373C1A3A5DB0C7B3D9E,
	Sticker_get_Scale_m8AD71F2543FC2FD340F7D4F24A942AD203511E61,
	Sticker_set_Scale_mBE3F4173B6290B822D8FC31CED729E9244AE67F1,
	Sticker_get_HasScale_mA12ED4EA0E7C5023BF873151CC5B39F6781F0BC7,
	Sticker_ClearScale_m66C7A603A0A374BFD1C1973F4F3C30399BD6260A,
	Sticker_get_RenderId_mE15BE0AF84C1C3C46063C7DAD1C62AE1E51A9E81,
	Sticker_set_RenderId_m07E792919CA92E8B7FB3100549FF931DDF619792,
	Sticker_get_HasRenderId_m56B83DEFDAE76BF517607A917E0079CDB7EF7613,
	Sticker_ClearRenderId_mEA835CE5755D482612166621EAC9FF4C05607150,
	Sticker_Equals_m35D29CDEEB1B41EC384FEC46477C4042D40B05AF,
	Sticker_Equals_m337E02F0A28B1447BA8B24631C35FF1C286E60C9,
	Sticker_GetHashCode_m24AFE04034BF649FC23E1E49AD29DEAEEE7C04AB,
	Sticker_ToString_m044B37DFDBABC8E510CCDB2944D1A7819B5149D0,
	Sticker_WriteTo_m0BAF019B7204C3399E1FA485E981DC36F94648DB,
	Sticker_CalculateSize_mB443FFC825293F7F0ED3AD2A415E4621EF3EA59B,
	Sticker_MergeFrom_mCF73E0750E5110F3AF4A75B6A09824D7B4484C9B,
	Sticker_MergeFrom_m9CADCC2E1344332FBCAC86D3FA7488F908175131,
	Sticker__cctor_m0C4755987159010446621AD1084BF59744AC27A3,
	U3CU3Ec__cctor_m8C392E929527BF44523EE264246A456292378A8A,
	U3CU3Ec__ctor_m5932EC36633AA073587C123500337B40C96E3CBC,
	U3CU3Ec_U3C_cctorU3Eb__75_0_mED5F2CB9683632BE9CC3F22608AD48689E3EC8DA,
	StickerRoll_get_Parser_m0307904586A1C10AEF41DFADF886EE6122652E4B,
	StickerRoll_get_Descriptor_m6774AF565D11E97EFBD6FC4D76F55E6F501127E9,
	StickerRoll_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_mD48A89BFE8E416135A4F58D39F4CAAB1DEF88FF9,
	StickerRoll__ctor_m7D0E0B4B49A31F27819E3777EDFB6B58A11174DB,
	StickerRoll__ctor_mC0E9A98013535B1F5E13F0E01117B200EC09E1C7,
	StickerRoll_Clone_m096072A72723DB3BF8F7311D5E14236BAEC77850,
	StickerRoll_get_Sticker_mA5E92ADF3019AE63B200B754B6EEEFEAE3259DE7,
	StickerRoll_Equals_m3E5A8335E874E955AA1A7EB2D2400B4BC6A36F93,
	StickerRoll_Equals_m1583815F5C84B1A33349A25E7C1D10D9B454B786,
	StickerRoll_GetHashCode_mB2561E014D54E102F9091D03556DCF6D1A678F56,
	StickerRoll_ToString_m18198D57FDEDBBC074B162CC75414E505B20431E,
	StickerRoll_WriteTo_m152931EE67A3A849500DCF04913BBB3CEB5B549E,
	StickerRoll_CalculateSize_m57AA5A87B5891693324E260A333D8B70D013EC0C,
	StickerRoll_MergeFrom_m7339523DC45A8AF327B08EEC2E9BC55C83E0381A,
	StickerRoll_MergeFrom_m46C03127E58A827A900A8602C569D8E8343441F5,
	StickerRoll__cctor_m60812142AD83B7368DC33F75737FC58454BAD465,
	U3CU3Ec__cctor_m96046462691983A53ED7AEF1E0B189075CAAE3C8,
	U3CU3Ec__ctor_mF9AA2BD3B61FDD07E3CE2E593B209201D824F673,
	U3CU3Ec_U3C_cctorU3Eb__25_0_mEBF95CA4F792EE23C899F16109098062B3FBA6CC,
	StreamHandlerReflection_get_Descriptor_m4C0AAD33D6DEA9D36A1BA53C7C942B4C093402C3,
	StreamHandlerReflection__cctor_m53BADF26D07943262104CFE505C2DB0A1B7C4FD4,
	InputStreamHandlerConfig_get_Parser_m5A2FBD44467CDAEF2AECF4DA25B1405747213C16,
	InputStreamHandlerConfig_get_Descriptor_m5762E4DFAD2A565C5E1557DFC43F67F8141B224A,
	InputStreamHandlerConfig_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_mAEC6F813A8A47C89CA2CB0AC7F1D8FA774614736,
	InputStreamHandlerConfig__ctor_mDA2A0ADCFBE563186C6F1E56BDBDC3F19522EF52,
	InputStreamHandlerConfig__ctor_m2A5888DCE80A2B2DB059CB563795F28C050114F9,
	InputStreamHandlerConfig_Clone_m1C7D8601234B62154567A71C893796A3DD5BCFB8,
	InputStreamHandlerConfig_get_InputStreamHandler_m81B4AD2B70646F815C786C397D4CC53CA43EF132,
	InputStreamHandlerConfig_set_InputStreamHandler_m606ACF5FE64DBEE4114CB0C942BEE8077A75053B,
	InputStreamHandlerConfig_get_HasInputStreamHandler_m1A01EFA0B87D41AFE8E27F9383AFDE538A3743DB,
	InputStreamHandlerConfig_ClearInputStreamHandler_m778DA275FBA99054E9B2367B45EE2DA982BE3CB3,
	InputStreamHandlerConfig_get_Options_mA1AB139E91179028EEEFB75E9A33D4A2E2671CDC,
	InputStreamHandlerConfig_set_Options_mFCC50B13F986F7B1D27CD77A461DC1D865A446E7,
	InputStreamHandlerConfig_get_HasOptions_mAEE226DDE7B0313B7AE4E7FFDF8389CD28EDF15E,
	InputStreamHandlerConfig_ClearOptions_mC4B0ECD6F0DA723730F3E45FFDBE9D5AC42DFED4,
	InputStreamHandlerConfig_Equals_m920E22E1FFCB71B026DD2541AE7343A1DC541032,
	InputStreamHandlerConfig_Equals_mE3FF7A087C37572FAEDD71033B5856E49AA4BAE7,
	InputStreamHandlerConfig_GetHashCode_m58B4E8D647A7C52BAC4306EBEBE35E370DA58F4B,
	InputStreamHandlerConfig_ToString_m5A51A1B49D862152FB108FDD9E7EC26DBDD754C7,
	InputStreamHandlerConfig_WriteTo_mCE51105C65479A27CC3078A2C761EAA386676EF1,
	InputStreamHandlerConfig_CalculateSize_m4825B60F49C6987E66E0EA4124F5371E7517963C,
	InputStreamHandlerConfig_MergeFrom_m5313E6C7EFF4B95CD73FCE7ED2B19FE92CA5B1A2,
	InputStreamHandlerConfig_MergeFrom_mB0FF550FD90B38F91C9A89A027AF419A29D5B3E8,
	InputStreamHandlerConfig__cctor_mBEEAD6E3A616FBB95B12BD9C5D5D30F135045A13,
	U3CU3Ec__cctor_m77D23CC30896FBBB46C3C36A07D8CBC0EEFC144D,
	U3CU3Ec__ctor_mDEAC2631E57057D9364409F52CF7A7B54FFA9257,
	U3CU3Ec_U3C_cctorU3Eb__37_0_m54CAD05E92A63061CFEDF3C7E6709AAEC0FEF8C4,
	OutputStreamHandlerConfig_get_Parser_mF9FC43981A45E193014FE6F36605CA5C04425B87,
	OutputStreamHandlerConfig_get_Descriptor_m6BE140D5A9CCCA6B38B0D55F52758042C0B6C4CC,
	OutputStreamHandlerConfig_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m418EA3EC2CCB02290E49C1ED5F5EA44EBAEBFD85,
	OutputStreamHandlerConfig__ctor_m05C62D4D22FF9FF66AF9BB724990B2FFEF12520F,
	OutputStreamHandlerConfig__ctor_m2C42D8AEFEC3AEA97E86245C7244C6186B1EC37B,
	OutputStreamHandlerConfig_Clone_mC359C6B6F92C04CD25AA67A7F2E95F90D5837248,
	OutputStreamHandlerConfig_get_OutputStreamHandler_m43DC2A2E1275BAF6942DE4496C3EA63FB48B9CA9,
	OutputStreamHandlerConfig_set_OutputStreamHandler_m025DA963BA4230051CCBBE42AAC1F436A8E79117,
	OutputStreamHandlerConfig_get_HasOutputStreamHandler_m40386B074A20D5C724123228433177B0E12CA440,
	OutputStreamHandlerConfig_ClearOutputStreamHandler_m1B66407BF083E4781A6BB19D8C0502845610C3E0,
	OutputStreamHandlerConfig_get_InputSidePacket_mBA953460BDC3DA9997FF3F586E6309158DE8DC7F,
	OutputStreamHandlerConfig_get_Options_m30F898BA1440FCA35ED951AAE22D321B96D38D9F,
	OutputStreamHandlerConfig_set_Options_m8DC46CFA8205EB344A8D9359CF8235A2D1FF5894,
	OutputStreamHandlerConfig_get_HasOptions_mE26B8E990BC6C61A018DDEE4DFCC044A5DFD6057,
	OutputStreamHandlerConfig_ClearOptions_mE6629E3A15B7C6E8DA899E244B1473B6F533849C,
	OutputStreamHandlerConfig_Equals_m6C32151DC752E25C8DEDB017643B7FA8BF3F356C,
	OutputStreamHandlerConfig_Equals_m82C75029D3E419F810CAD772CCDE87705D433C2B,
	OutputStreamHandlerConfig_GetHashCode_m858E4EF857A0EBEA85DA7F18FC322F4B7CF519C1,
	OutputStreamHandlerConfig_ToString_mD615009A9DBB0AF31B5EC2F952482129EC268201,
	OutputStreamHandlerConfig_WriteTo_m97B3BB527940366F4AD1983486997DF660920C14,
	OutputStreamHandlerConfig_CalculateSize_mF73E0648CDAF78E335B21A903AEAB5223BB210BC,
	OutputStreamHandlerConfig_MergeFrom_mBB90784471D1BF4F6D21792DA33C740B5AE4489F,
	OutputStreamHandlerConfig_MergeFrom_mEBEB8A78D238C7BCBA267A3982A7B82F4AB8CE39,
	OutputStreamHandlerConfig__cctor_m01950CF61EBD9FD05C693E916B7AB1AAC1CF3293,
	U3CU3Ec__cctor_mBCCE7EE4E6CD62CCA394447265181A61E2FA3C2E,
	U3CU3Ec__ctor_m26C569D51FD6D3D8605EFEA5916DAF2C77E7F5C8,
	U3CU3Ec_U3C_cctorU3Eb__42_0_m8134538B3AC9021C2DC4C0C3CFE11F0483497A41,
	ResourceHandle__ctor_mEB4B6FCE5E50639F862EB76EBAB98ECBC01DF3DB,
	ResourceHandle__ctor_m14DF1AEA141B44C3D966A8B5A030265E9525CFEA,
	ResourceHandle_Finalize_m9B7446267C0A4C56D100BAE004C75C4B0D5CC78B,
	ResourceHandle_Dispose_mB99FA1A03549996F8F341A0E7062DF78EC3694AD,
	NULL,
	ResourceHandle_GetPtr_m9A89B139A0EB33847D14249403BA3312CB520F81,
	ResourceHandle_TakeOwnership_m746D2259DC0B38680DE32BCDE1AB1215C6E5D937,
	ResourceHandle_ReleaseOwnership_m77DB1D0688CE2CC1D1EC51CB40DDB70A80578009,
	ResourceHandle_ReleasePtr_m02393C40EA7BA4A89BCA4BF19307ACA5872A604F,
	ResourceHandle_OwnsResource_mCA1B1D4BAB2C213DC1E66815098E5A90B4E000FD,
	AssetBundleManager_get_pathResolver_m8D06EBBF1D16D9F0D530782B3CEA907E3679BD84,
	AssetBundleManager_get_resourceProvider_mDBE671E26545611BEAE69CDBB7E0EC4C2E5DD7A1,
	AssetBundleManager__ctor_m5D0F4D0F9138FAC218B2D563A7824B50C49B787E,
	AssetBundleManager__ctor_m28B63C1531EE393AC8C55C48CF7A7D7644C5490F,
	AssetBundleManager_IsPrepared_m4DE649572FA2049D080D084A691BE81E541AE0F0,
	AssetBundleManager_get_CacheRootPath_mA0AC06F5010C52B6A9378571469F18BAD800E6A1,
	AssetBundleManager_set_CacheRootPath_m62D305D7C69D8AB3D6CC17C05F00B61EB29BE7DF,
	AssetBundleManager_get_AssetBundlePath_mD41F1371DC761CEB935B497AF6251DB6D250533D,
	AssetBundleManager_set_AssetBundlePath_mE42D6A7BC33AC00DA860220226F4F54292EBF5DF,
	AssetBundleManager_get_assetBundle_mDC75A1793A1CBDAC61570F8F386F1AA1D1C928B6,
	AssetBundleManager_set_assetBundle_m69BFB10AD41A82965239CA847947EDEE62771947,
	AssetBundleManager_ClearAllCacheFiles_m8361B7BAE41DBC7576CFEFFD50CCA4AC1EB0F832,
	AssetBundleManager_LoadAssetBundle_mD8015C302101C1C31C4F656FFDBFB4147F1A9C0C,
	AssetBundleManager_LoadAssetBundleAsync_mE25419B8260B8C411CED0EDF65F03336E5A25DBD,
	AssetBundleManager_PrepareAsset_m864B915C4A1B897ADC3523B0F9C7672FFDAEB7BD,
	AssetBundleManager_PrepareAssetAsync_m0CED483ECF346567C5E1328E28E459262DD0131D,
	AssetBundleManager_PathToResourceAsFile_mE4EA5782FC953FF16031FD0EB659BD758E0EDC7E,
	AssetBundleManager_GetResourceContents_mDFE89208C34422AEE3F365704F969ADFA843BA84,
	AssetBundleManager_GetCacheFilePathFor_m90FD4FC61CFBF384DF90FEA3247773869ABF3AB2,
	AssetBundleManager_WriteCacheFile_m54C06D18FBFD36C6ABC3589C5D31145972FFE996,
	AssetBundleManager_WriteCacheFileAsync_m5F88F8554EB84D0743BA2BD9B7D0E75A26F0E316,
	AssetBundleManager__cctor_m904B43C51174C89D272E9A293857B27D8FC8F416,
	U3CLoadAssetBundleAsyncU3Ed__21_MoveNext_mD431D8ABB551632415D2A78E9E170537E6469379,
	U3CLoadAssetBundleAsyncU3Ed__21_SetStateMachine_mB1E3AA674C432E165404B70B653A6052F97B5070,
	U3CPrepareAssetAsyncU3Ed__23_MoveNext_m2CEF64D04C86A028B3B09E52518AC86BF7CDA47F,
	U3CPrepareAssetAsyncU3Ed__23_SetStateMachine_m290CC6603F5D2FC97EC17298E510722817A5A414,
	U3CWriteCacheFileAsyncU3Ed__28_MoveNext_mFB1FC8D21EDC7484C932074DCB1F0C9EE72581BF,
	U3CWriteCacheFileAsyncU3Ed__28_SetStateMachine_m4078D3327DCD2BBFBF231DE1709C9CD3B7EAD5C4,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	LocalAssetManager_get_pathResolver_mC7A793AD5B6F288F15B8ED05A3520F41512BD8DA,
	LocalAssetManager_get_resourceProvider_mF559E1B02CA6310C6F48DC6990562E8B6E05E007,
	LocalAssetManager_IsPrepared_m56ECCDA53BEEC7AB396D637AE5CD897C2C78864E,
	LocalAssetManager_get_RootPath_m46618459F21AD293211F44DFE7243D50F3ED8C60,
	LocalAssetManager_PrepareAsset_m28813A11DAF363D54F7B61F87B7615B05D22D202,
	LocalAssetManager_PrepareAssetAsync_mB0FE9494A0DA547BA1823704C614DADD52EB629F,
	LocalAssetManager_PathToResourceAsFile_mD93EA6538E2E77CCD4ACB981201294DFFDF0C3A6,
	LocalAssetManager_GetResourceContents_m36D0D7964026D770978C9BECA4F18D510A46648A,
	LocalAssetManager_GetCacheFilePathFor_mB212F0779C0802E7594F39621194BDD1BF9FA8EC,
	LocalAssetManager__ctor_m7E6B3A0E203D21C624332321ED18C8A1289C35EC,
	LocalAssetManager__cctor_m598D766545234C7DF39B3FBA816A1F24B483BED8,
	U3CPrepareAssetAsyncU3Ed__9_MoveNext_m0EF27ED0CD2CC24F50203DAE10881054342915B0,
	U3CPrepareAssetAsyncU3Ed__9_SetStateMachine_mC7A20B6945A994F96677055B1D8DE081F0F89849,
	NULL,
	NULL,
	ResourceManager__ctor_m34A7AB9E318E70364155F06B3D37F2203132DA60,
	NULL,
	NULL,
	ResourceManager_PrepareAsset_mF4FF9B09507DB5DB5D30427A2DC96ADA79E0902E,
	NULL,
	ResourceManager_PrepareAssetAsync_m2DDB5F5EFD5BA1BE652D35340DD628941FBB0E9E,
	ResourceManager_GetAssetNameFromPath_m2C7FE8490B5337FA3322C7E6AA1AF22385F48F84,
	PathResolver__ctor_m2CBCAC18A130A9ACDE6A589DECD3FF8433CB8D80,
	PathResolver_Invoke_m7268AF680F591B483C2F32B54BCF6C11A4FBB607,
	PathResolver_BeginInvoke_m0DA05233E279C32CB1B8CF5F4C4B7D71C21F977E,
	PathResolver_EndInvoke_m9264EA6918879B0F5CADBF4236289C02B70523F6,
	ResourceProvider__ctor_m31D3EDD7BDF5AD400A08B8BFC98CF553BAACFEBD,
	ResourceProvider_Invoke_mF5795AEB7F87C37BE3BC99516515191AA3FE31C6,
	ResourceProvider_BeginInvoke_m319DDE3FA962F705DCE00DF8CA2E14D0603541E8,
	ResourceProvider_EndInvoke_m60F666704EC5F92E1808247A966C8E22C9F52162,
	FaceGeometryReflection_get_Descriptor_mEF894D394F60CAA7DA98B4BF777BA84109579AFB,
	FaceGeometryReflection__cctor_m24B3C4844194978F63FAB30AB7CC54555541AF52,
	FaceGeometry_get_Parser_m84F9AF8D024653DBBA02B0F72E23FB8A52E6F4E0,
	FaceGeometry_get_Descriptor_mC186813A0E14794CC712F7C993A3EA7D81790654,
	FaceGeometry_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_m36EA1A8AE55FE125B566F37B54D7430005916494,
	FaceGeometry__ctor_m3C8727C2045277EA8A051205D4FC9A5CE3D5E89A,
	FaceGeometry__ctor_m2C60845DDA8A847E19706B9BD1CCF8064A1C5124,
	FaceGeometry_Clone_m8489E2E228E407688DA6FEDFFC665B20256CC9BE,
	FaceGeometry_get_Mesh_mDD024BFCD1BC8CA25DC919C700E4320E1ED8F4FE,
	FaceGeometry_set_Mesh_m93B518A98300BF6B789FB23D370D151647AFE844,
	FaceGeometry_get_HasMesh_m568BC0CD80EB1AABC3578A5CE5EE2B065324E2D0,
	FaceGeometry_ClearMesh_m7CD19E757E9FD9D40EEE8C790A17A34F9EB32C86,
	FaceGeometry_get_PoseTransformMatrix_mD90833ABA53C6B4ED3DA6E4949FC21613446008A,
	FaceGeometry_set_PoseTransformMatrix_m513DD035B20C4296E3D68FDAC90EAA2CE18BCEC7,
	FaceGeometry_get_HasPoseTransformMatrix_m560FE1CBC44455CB770549D97071BCE38B0021BD,
	FaceGeometry_ClearPoseTransformMatrix_m5E342DF34AA1B1C802830EE2BEB6C53AD40BAD4F,
	FaceGeometry_Equals_m24D19204C5A3E02D27CB6F1EF569A1D60B36AD7F,
	FaceGeometry_Equals_m4AB38A52827525069D398725064F885308DF8426,
	FaceGeometry_GetHashCode_m9A30D9E1C207E32C0035016651B8292E1AC0978D,
	FaceGeometry_ToString_m8A03508AD29218B0490EEC9FCC3090B9FA68C618,
	FaceGeometry_WriteTo_mA3D5E866007FE45435FD2F0C241F5BFF9000DA46,
	FaceGeometry_CalculateSize_m8244CFFD3C4A8DA2ABB011819E670C9B6002EC6D,
	FaceGeometry_MergeFrom_mE6D2821557D75E5A220BBAD1AF0533C69708A99B,
	FaceGeometry_MergeFrom_m635F8DC14C15F10F7ADD712FBE7899E7DF8B918C,
	FaceGeometry__cctor_m80298DB8E5F50F7A58970C3DBA002848BF4569E6,
	U3CU3Ec__cctor_mD7684E999F848E5E818C330CB66CC6C5B38E751E,
	U3CU3Ec__ctor_m5B0F45FF93FB9AE889C4654C54DAFDBC93D9B5A1,
	U3CU3Ec_U3C_cctorU3Eb__36_0_m9403D33D924F4DF1BEFAE14D42CDBC533BA26B60,
	Mesh3DReflection_get_Descriptor_m3064E3A93064960806ED6EF889AE43C9498B47A8,
	Mesh3DReflection__cctor_mD223615FABAE9A99ECD65CC7CBE49E822F230CC8,
	Mesh3d_get_Parser_m24646561AE2461D22BFFB8FDDEED3CA3B9A1A36D,
	Mesh3d_get_Descriptor_m1D093CEFDCECDC44BA24719A0B300B9D7947E15A,
	Mesh3d_pbU3AU3AGoogle_Protobuf_IMessage_get_Descriptor_mBBF9A0FED0C08E972BFFEFA023D47624EEC83327,
	Mesh3d__ctor_m9F9250640B8B59C63500CA172405221839AC8659,
	Mesh3d__ctor_m3DA3D0DF3B76850845F5492101B043CDFA74891B,
	Mesh3d_Clone_m1480A74D40F2C4A9F767C9BADE298FA2C929E2FE,
	Mesh3d_get_VertexType_m3FDB29436DA076950A70926DC5E35B66FE3AB38D,
	Mesh3d_set_VertexType_m5E86A37793BE879843800CDFBA072E549094C042,
	Mesh3d_get_HasVertexType_m2FC415F35052C6FBEAF491A8B24C94D501386D92,
	Mesh3d_ClearVertexType_m0636B3AF901073C2526A37C1F2A09715A3AF9EDE,
	Mesh3d_get_PrimitiveType_mB2F31009B4E5D7EA7FC125C5DEB58D58A4C7949D,
	Mesh3d_set_PrimitiveType_m9C7F073EB49745074A4FD40110DC8FB3E1014407,
	Mesh3d_get_HasPrimitiveType_m9DF161B713DA4A600B333477279B51FB178438B4,
	Mesh3d_ClearPrimitiveType_mDA655E945A9E7C5224BC7B466BEB4DC2C0237280,
	Mesh3d_get_VertexBuffer_m61C7625831070AC8D548C717D61B5160C718B6ED,
	Mesh3d_get_IndexBuffer_mFCDEEC12BF5491CBED10689092AB07BACE7DF791,
	Mesh3d_Equals_m8608984036EA7B53D8E397986337302F7A26916D,
	Mesh3d_Equals_m1A13E2324B6A21935A44DF3AF37B6F090083DFC2,
	Mesh3d_GetHashCode_m385ABBC9D72FB51065C89DB3616E642F47066A0A,
	Mesh3d_ToString_m2628330180A999E8A61BC16A9DC80C69E4B9C03C,
	Mesh3d_WriteTo_m5CBA34FA8D18DFAB07AC9AC358FEFB2626FDE216,
	Mesh3d_CalculateSize_m1AC813F566F4AAE361BA291AC9FEB73457651D91,
	Mesh3d_MergeFrom_mA837C24529468595959D161C916756CA53E4BFDD,
	Mesh3d_MergeFrom_mDBAB758798822F0692FBBADAFDABE64BE546153E,
	Mesh3d__cctor_m7CDC3E0A8B20D3F98FE8464F198F91CEF01C4651,
	U3CU3Ec__cctor_m5C16FD7585F3041FA5D626C2F5134B7314C92079,
	U3CU3Ec__ctor_m4B37574669A9C61778546DAB7A78D91CCEA85945,
	U3CU3Ec_U3C_cctorU3Eb__50_0_m1097B4EF560DD0F3EC417196F17DE6CC204A6F02,
};
extern void U3CLoadAssetBundleAsyncU3Ed__21_MoveNext_mD431D8ABB551632415D2A78E9E170537E6469379_AdjustorThunk (void);
extern void U3CLoadAssetBundleAsyncU3Ed__21_SetStateMachine_mB1E3AA674C432E165404B70B653A6052F97B5070_AdjustorThunk (void);
extern void U3CPrepareAssetAsyncU3Ed__23_MoveNext_m2CEF64D04C86A028B3B09E52518AC86BF7CDA47F_AdjustorThunk (void);
extern void U3CPrepareAssetAsyncU3Ed__23_SetStateMachine_m290CC6603F5D2FC97EC17298E510722817A5A414_AdjustorThunk (void);
extern void U3CWriteCacheFileAsyncU3Ed__28_MoveNext_mFB1FC8D21EDC7484C932074DCB1F0C9EE72581BF_AdjustorThunk (void);
extern void U3CWriteCacheFileAsyncU3Ed__28_SetStateMachine_m4078D3327DCD2BBFBF231DE1709C9CD3B7EAD5C4_AdjustorThunk (void);
extern void U3CPrepareAssetAsyncU3Ed__9_MoveNext_m0EF27ED0CD2CC24F50203DAE10881054342915B0_AdjustorThunk (void);
extern void U3CPrepareAssetAsyncU3Ed__9_SetStateMachine_mC7A20B6945A994F96677055B1D8DE081F0F89849_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[8] = 
{
	{ 0x06000869, U3CLoadAssetBundleAsyncU3Ed__21_MoveNext_mD431D8ABB551632415D2A78E9E170537E6469379_AdjustorThunk },
	{ 0x0600086A, U3CLoadAssetBundleAsyncU3Ed__21_SetStateMachine_mB1E3AA674C432E165404B70B653A6052F97B5070_AdjustorThunk },
	{ 0x0600086B, U3CPrepareAssetAsyncU3Ed__23_MoveNext_m2CEF64D04C86A028B3B09E52518AC86BF7CDA47F_AdjustorThunk },
	{ 0x0600086C, U3CPrepareAssetAsyncU3Ed__23_SetStateMachine_m290CC6603F5D2FC97EC17298E510722817A5A414_AdjustorThunk },
	{ 0x0600086D, U3CWriteCacheFileAsyncU3Ed__28_MoveNext_mFB1FC8D21EDC7484C932074DCB1F0C9EE72581BF_AdjustorThunk },
	{ 0x0600086E, U3CWriteCacheFileAsyncU3Ed__28_SetStateMachine_m4078D3327DCD2BBFBF231DE1709C9CD3B7EAD5C4_AdjustorThunk },
	{ 0x06000881, U3CPrepareAssetAsyncU3Ed__9_MoveNext_m0EF27ED0CD2CC24F50203DAE10881054342915B0_AdjustorThunk },
	{ 0x06000882, U3CPrepareAssetAsyncU3Ed__9_SetStateMachine_mC7A20B6945A994F96677055B1D8DE081F0F89849_AdjustorThunk },
};
static const int32_t s_InvokerIndices[2253] = 
{
	1450,
	1450,
	1450,
	1176,
	294,
	461,
	461,
	42,
	409,
	409,
	459,
	1450,
	1450,
	1450,
	342,
	1450,
	1450,
	800,
	1450,
	1450,
	1450,
	1450,
	505,
	505,
	505,
	1450,
	505,
	1450,
	1450,
	337,
	504,
	515,
	1450,
	2458,
	1416,
	1405,
	1450,
	2458,
	1416,
	1405,
	1450,
	2458,
	1416,
	1405,
	1450,
	2458,
	1450,
	1450,
	1450,
	505,
	1416,
	1405,
	1405,
	1109,
	1450,
	2458,
	1450,
	1109,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	1450,
	161,
	51,
	393,
	1450,
	1450,
	341,
	341,
	1450,
	1416,
	1405,
	1450,
	2458,
	1450,
	505,
	1450,
	505,
	1450,
	1436,
	1261,
	1436,
	1261,
	1450,
	1261,
	1450,
	1261,
	1450,
	1450,
	1450,
	1450,
	1450,
	1407,
	1450,
	1450,
	1436,
	1244,
	1244,
	1244,
	1261,
	780,
	1407,
	1450,
	1436,
	1450,
	1450,
	1450,
	1002,
	799,
	565,
	234,
	555,
	780,
	1407,
	1450,
	780,
	1407,
	1407,
	2223,
	2458,
	2208,
	2418,
	1450,
	2458,
	-1,
	-1,
	1764,
	2353,
	1450,
	799,
	316,
	69,
	1244,
	780,
	1244,
	1450,
	1244,
	1550,
	1518,
	1518,
	1450,
	1450,
	1244,
	1244,
	1244,
	1450,
	1002,
	605,
	1416,
	605,
	-1,
	-1,
	1416,
	1002,
	1416,
	1002,
	1416,
	1416,
	1436,
	-1,
	604,
	1002,
	1416,
	1450,
	1436,
	1098,
	1436,
	1416,
	1002,
	799,
	979,
	399,
	980,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	2085,
	1450,
	1450,
	780,
	474,
	306,
	148,
	1450,
	2419,
	1436,
	1436,
	1098,
	1405,
	1405,
	1405,
	1405,
	1405,
	1405,
	1405,
	1407,
	1405,
	1405,
	1450,
	1450,
	999,
	999,
	999,
	1003,
	1704,
	-1,
	-1,
	2458,
	799,
	1237,
	399,
	1244,
	799,
	371,
	129,
	943,
	-1,
	-1,
	-1,
	1450,
	780,
	1261,
	809,
	1436,
	1416,
	1416,
	1450,
	780,
	1416,
	1416,
	1450,
	780,
	1416,
	1416,
	1450,
	780,
	1416,
	1416,
	1450,
	780,
	1416,
	1416,
	1450,
	780,
	1244,
	1416,
	1416,
	1416,
	1450,
	780,
	1416,
	1416,
	1450,
	780,
	1416,
	1416,
	1405,
	1235,
	1450,
	780,
	1244,
	800,
	1416,
	1407,
	1416,
	1416,
	1450,
	780,
	1263,
	815,
	1439,
	1416,
	1416,
	1450,
	780,
	1244,
	800,
	1416,
	1416,
	1416,
	1450,
	780,
	1244,
	800,
	1416,
	1416,
	1416,
	1450,
	780,
	1235,
	743,
	1405,
	1416,
	1416,
	1450,
	780,
	1416,
	1416,
	1450,
	780,
	1416,
	1416,
	1450,
	780,
	1416,
	1416,
	1450,
	780,
	1416,
	1416,
	1450,
	780,
	1416,
	1416,
	1450,
	780,
	1416,
	1416,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	1450,
	780,
	1416,
	1416,
	1450,
	780,
	1416,
	1416,
	1450,
	1450,
	1405,
	-1,
	-1,
	943,
	1450,
	1450,
	780,
	1244,
	1244,
	800,
	800,
	1416,
	1416,
	1416,
	1416,
	1450,
	780,
	1416,
	1416,
	780,
	1450,
	1436,
	1436,
	1450,
	1405,
	1405,
	1416,
	1862,
	2361,
	2086,
	1407,
	-1,
	-1,
	-1,
	-1,
	-1,
	1237,
	1450,
	1436,
	1416,
	1416,
	1237,
	1450,
	1436,
	1416,
	1416,
	1237,
	1450,
	1436,
	1416,
	1416,
	-1,
	-1,
	-1,
	-1,
	-1,
	1237,
	1236,
	1450,
	1109,
	1109,
	2143,
	2143,
	1405,
	1406,
	1400,
	1406,
	1436,
	1436,
	1436,
	1416,
	1416,
	1416,
	2350,
	2445,
	2445,
	2445,
	2445,
	2445,
	2445,
	2445,
	2445,
	2085,
	2085,
	2082,
	2356,
	2257,
	2257,
	2082,
	2068,
	2420,
	2420,
	2420,
	1969,
	1769,
	1969,
	2458,
	2458,
	1450,
	1002,
	1109,
	1002,
	1002,
	542,
	1450,
	1064,
	1064,
	2458,
	2442,
	1450,
	780,
	1450,
	1450,
	1450,
	1407,
	1436,
	1261,
	779,
	780,
	1450,
	1407,
	1407,
	2458,
	1500,
	1450,
	2458,
	1450,
	1450,
	1244,
	1002,
	1002,
	1002,
	1002,
	393,
	1002,
	1405,
	1244,
	1416,
	1436,
	799,
	1407,
	605,
	980,
	799,
	1416,
	605,
	1002,
	1450,
	1407,
	2445,
	780,
	1450,
	1450,
	1407,
	1407,
	1407,
	1407,
	1436,
	1405,
	1405,
	1406,
	780,
	1450,
	1407,
	1450,
	1237,
	1450,
	1450,
	1407,
	1450,
	1450,
	1436,
	1416,
	1237,
	1450,
	1407,
	1450,
	1450,
	474,
	780,
	1450,
	1405,
	1405,
	1405,
	1405,
	1450,
	1416,
	780,
	47,
	91,
	1450,
	1450,
	1407,
	1405,
	1405,
	1405,
	1405,
	1405,
	1450,
	1450,
	1450,
	1244,
	1244,
	1450,
	1450,
	1416,
	799,
	776,
	233,
	1244,
	780,
	1450,
	1407,
	1450,
	780,
	1244,
	1450,
	1416,
	1405,
	1405,
	1405,
	2308,
	1801,
	1237,
	1450,
	1450,
	1407,
	2445,
	2355,
	1237,
	1450,
	1407,
	1450,
	2418,
	2379,
	2310,
	2379,
	2139,
	2379,
	2137,
	2379,
	2379,
	2379,
	1815,
	2310,
	2310,
	2310,
	2036,
	2036,
	2036,
	2310,
	2330,
	2310,
	2036,
	2379,
	2379,
	2419,
	2310,
	2322,
	2287,
	2322,
	2379,
	2379,
	2379,
	2330,
	2330,
	2216,
	2379,
	2310,
	2330,
	2379,
	2330,
	2330,
	2330,
	2330,
	2379,
	2310,
	2310,
	2322,
	2330,
	2310,
	2310,
	2310,
	2310,
	2310,
	2330,
	2330,
	2310,
	2310,
	2310,
	2310,
	2442,
	2308,
	2308,
	2330,
	2310,
	2310,
	2310,
	2379,
	2330,
	2379,
	2420,
	2420,
	1811,
	2419,
	2036,
	2044,
	2440,
	2312,
	2312,
	2312,
	2312,
	2308,
	2312,
	2419,
	2419,
	2036,
	2036,
	2036,
	2036,
	2036,
	2036,
	2036,
	2036,
	2036,
	2036,
	2036,
	2036,
	2036,
	2036,
	2036,
	2419,
	2419,
	1823,
	2215,
	2304,
	2041,
	1823,
	2419,
	1645,
	1557,
	2036,
	1647,
	1819,
	1816,
	1816,
	2036,
	2036,
	1646,
	1645,
	1819,
	2036,
	2310,
	2036,
	1816,
	2041,
	2304,
	1554,
	1487,
	2419,
	2310,
	2310,
	1817,
	1817,
	1817,
	2419,
	2036,
	2036,
	2036,
	1816,
	2036,
	2036,
	2036,
	2419,
	2310,
	1816,
	2037,
	2036,
	2419,
	2036,
	2036,
	2304,
	2419,
	1816,
	2036,
	2036,
	2036,
	2036,
	2036,
	2047,
	1834,
	2036,
	2036,
	2048,
	1835,
	2036,
	2036,
	2031,
	1810,
	2036,
	2036,
	1823,
	1659,
	2036,
	2036,
	2041,
	1828,
	1823,
	1659,
	2036,
	1814,
	2036,
	2304,
	2419,
	1820,
	1819,
	1819,
	2035,
	2419,
	2036,
	2036,
	2036,
	2027,
	2304,
	2304,
	2304,
	2304,
	2304,
	2304,
	2304,
	2304,
	2304,
	2419,
	1644,
	2036,
	2036,
	2036,
	2304,
	2419,
	2038,
	1819,
	1816,
	1816,
	1555,
	1816,
	2038,
	2419,
	2419,
	2304,
	2047,
	1821,
	1813,
	2419,
	2419,
	2310,
	2310,
	2036,
	2036,
	2310,
	2310,
	2310,
	2038,
	2038,
	2310,
	2310,
	2419,
	2419,
	1477,
	2304,
	1642,
	2419,
	2310,
	2036,
	2458,
	1500,
	1642,
	2419,
	2036,
	2419,
	2419,
	2036,
	2036,
	2036,
	1816,
	2036,
	2036,
	2036,
	2419,
	2419,
	2304,
	2036,
	2419,
	2036,
	2036,
	2445,
	2458,
	2445,
	2445,
	1416,
	1450,
	1244,
	1416,
	1416,
	1244,
	1416,
	1244,
	1416,
	1244,
	1109,
	1109,
	1405,
	1416,
	1244,
	1405,
	1244,
	1244,
	2458,
	2458,
	1450,
	1416,
	2445,
	2445,
	1416,
	1450,
	1244,
	1416,
	1416,
	1244,
	1416,
	1416,
	1405,
	1235,
	1416,
	1244,
	1109,
	1109,
	1405,
	1416,
	1244,
	1405,
	1244,
	1244,
	2458,
	2458,
	1450,
	1416,
	2445,
	2445,
	1416,
	1450,
	1244,
	1416,
	1416,
	1109,
	1109,
	1405,
	1416,
	1244,
	1405,
	1244,
	1244,
	2458,
	2458,
	1450,
	1416,
	2445,
	2445,
	1416,
	1450,
	1244,
	1416,
	1416,
	1244,
	1436,
	1261,
	1109,
	1109,
	1405,
	1416,
	1244,
	1405,
	1244,
	1244,
	2458,
	2458,
	1450,
	1416,
	2445,
	2445,
	1416,
	1450,
	1244,
	1416,
	1406,
	1236,
	1406,
	1236,
	1436,
	1261,
	1436,
	1261,
	1436,
	1261,
	1436,
	1261,
	1406,
	1236,
	1416,
	1416,
	1244,
	1405,
	1235,
	1406,
	1236,
	1406,
	1236,
	1436,
	1261,
	1405,
	1235,
	1436,
	1261,
	1436,
	1261,
	1436,
	1261,
	1109,
	1109,
	1405,
	1416,
	1244,
	1405,
	1244,
	1244,
	2458,
	2458,
	1450,
	1416,
	2445,
	2445,
	1416,
	1450,
	1244,
	1416,
	1416,
	1416,
	1416,
	1405,
	1235,
	1416,
	1416,
	1416,
	1416,
	1416,
	1405,
	1235,
	1436,
	1261,
	1416,
	1244,
	1416,
	1244,
	1416,
	1416,
	1244,
	1416,
	1244,
	1416,
	1244,
	1416,
	1244,
	1416,
	1109,
	1109,
	1405,
	1416,
	1244,
	1405,
	1244,
	1244,
	2458,
	2445,
	2445,
	1416,
	1450,
	1244,
	1416,
	1416,
	1244,
	1416,
	1244,
	1416,
	1416,
	1416,
	1416,
	1416,
	1244,
	1416,
	1405,
	1235,
	1405,
	1235,
	1416,
	1244,
	1416,
	1244,
	1416,
	1416,
	1244,
	1416,
	1244,
	1405,
	1235,
	1416,
	1109,
	1109,
	1405,
	1416,
	1244,
	1405,
	1244,
	1244,
	2458,
	2458,
	1450,
	1416,
	2458,
	1450,
	1416,
	2445,
	2458,
	1416,
	2445,
	2445,
	1416,
	1450,
	1244,
	1416,
	1436,
	1261,
	1436,
	1450,
	1109,
	1109,
	1405,
	1416,
	1244,
	1405,
	1244,
	1244,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	2458,
	2458,
	1450,
	1416,
	2445,
	2458,
	2445,
	2445,
	1416,
	1450,
	1244,
	1416,
	1405,
	1235,
	1436,
	1450,
	1439,
	1263,
	1436,
	1450,
	1416,
	1244,
	1436,
	1450,
	1416,
	1244,
	1436,
	1450,
	1109,
	1109,
	1405,
	1416,
	1244,
	1405,
	1244,
	1244,
	2458,
	2458,
	1450,
	1416,
	2445,
	2445,
	1416,
	1450,
	1244,
	1416,
	1416,
	1109,
	1109,
	1405,
	1416,
	1244,
	1405,
	1244,
	1244,
	2458,
	2458,
	1450,
	1416,
	2445,
	2458,
	2445,
	2445,
	1416,
	1450,
	1244,
	1416,
	1416,
	1416,
	1416,
	1416,
	1244,
	1436,
	1450,
	1416,
	1244,
	1436,
	1450,
	1416,
	1244,
	1436,
	1450,
	1406,
	1236,
	1436,
	1450,
	1416,
	1416,
	1406,
	1236,
	1436,
	1450,
	1109,
	1109,
	1405,
	1416,
	1244,
	1405,
	1244,
	1244,
	2458,
	2445,
	2445,
	1416,
	1450,
	1244,
	1416,
	1405,
	1235,
	1436,
	1450,
	1439,
	1263,
	1436,
	1450,
	1109,
	1109,
	1405,
	1416,
	1244,
	1405,
	1244,
	1244,
	2458,
	2458,
	1450,
	1416,
	2458,
	1450,
	1416,
	2445,
	2445,
	1416,
	1450,
	1244,
	1416,
	1416,
	1109,
	1109,
	1405,
	1416,
	1244,
	1405,
	1244,
	1244,
	2458,
	2458,
	1450,
	1416,
	2445,
	2458,
	2445,
	2445,
	1416,
	1450,
	1244,
	1416,
	1439,
	1263,
	1436,
	1450,
	1439,
	1263,
	1436,
	1450,
	1439,
	1263,
	1436,
	1450,
	1439,
	1263,
	1436,
	1450,
	1439,
	1263,
	1436,
	1450,
	1109,
	1109,
	1405,
	1416,
	1244,
	1405,
	1244,
	1244,
	2458,
	2458,
	1450,
	1416,
	2445,
	2445,
	1416,
	1450,
	1244,
	1416,
	1416,
	1109,
	1109,
	1405,
	1416,
	1244,
	1405,
	1244,
	1244,
	2458,
	2458,
	1450,
	1416,
	2445,
	2445,
	1416,
	1450,
	1244,
	1416,
	1439,
	1263,
	1436,
	1450,
	1439,
	1263,
	1436,
	1450,
	1439,
	1263,
	1436,
	1450,
	1439,
	1263,
	1436,
	1450,
	1439,
	1263,
	1436,
	1450,
	1109,
	1109,
	1405,
	1416,
	1244,
	1405,
	1244,
	1244,
	2458,
	2458,
	1450,
	1416,
	2445,
	2445,
	1416,
	1450,
	1244,
	1416,
	1416,
	1109,
	1109,
	1405,
	1416,
	1244,
	1405,
	1244,
	1244,
	2458,
	2458,
	1450,
	1416,
	2445,
	2458,
	2445,
	2445,
	1416,
	1450,
	1244,
	1416,
	1405,
	1235,
	1436,
	1450,
	1416,
	1244,
	1436,
	1450,
	1416,
	1244,
	1436,
	1450,
	1416,
	1244,
	1436,
	1450,
	1416,
	1109,
	1109,
	1405,
	1416,
	1244,
	1405,
	1244,
	1244,
	2458,
	2445,
	2445,
	1416,
	1450,
	1244,
	1416,
	1405,
	1235,
	1436,
	1450,
	1405,
	1235,
	1436,
	1450,
	1405,
	1235,
	1436,
	1450,
	1405,
	1235,
	1436,
	1450,
	1109,
	1109,
	1405,
	1416,
	1244,
	1405,
	1244,
	1244,
	2458,
	2458,
	1450,
	1416,
	2445,
	2445,
	1416,
	1450,
	1244,
	1416,
	1439,
	1263,
	1436,
	1450,
	1439,
	1263,
	1436,
	1450,
	1439,
	1263,
	1436,
	1450,
	1439,
	1263,
	1436,
	1450,
	1109,
	1109,
	1405,
	1416,
	1244,
	1405,
	1244,
	1244,
	2458,
	2458,
	1450,
	1416,
	2445,
	2445,
	1416,
	1450,
	1244,
	1416,
	1405,
	1235,
	1436,
	1450,
	1405,
	1235,
	1436,
	1450,
	1416,
	1244,
	1436,
	1450,
	1109,
	1109,
	1405,
	1416,
	1244,
	1405,
	1244,
	1244,
	2458,
	2458,
	1450,
	1416,
	2445,
	2445,
	1416,
	1450,
	1244,
	1416,
	1439,
	1263,
	1436,
	1450,
	1439,
	1263,
	1436,
	1450,
	1416,
	1244,
	1436,
	1450,
	1439,
	1263,
	1436,
	1450,
	1109,
	1109,
	1405,
	1416,
	1244,
	1405,
	1244,
	1244,
	2458,
	2458,
	1450,
	1416,
	2458,
	1450,
	1416,
	2445,
	2458,
	2445,
	2445,
	1416,
	1450,
	1244,
	1416,
	1405,
	1235,
	1436,
	1450,
	1405,
	1235,
	1436,
	1450,
	1416,
	1405,
	1235,
	1436,
	1450,
	1109,
	1109,
	1405,
	1416,
	1244,
	1405,
	1244,
	1244,
	2458,
	2458,
	1450,
	1416,
	2445,
	2458,
	1416,
	2445,
	2445,
	1416,
	1450,
	1244,
	1416,
	1109,
	1109,
	1405,
	1416,
	1244,
	1405,
	1244,
	1244,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	2458,
	2458,
	1450,
	1416,
	2445,
	2458,
	2445,
	2445,
	1416,
	1450,
	1244,
	1416,
	1416,
	1406,
	1236,
	1436,
	1450,
	1405,
	1235,
	1436,
	1450,
	1109,
	1109,
	1405,
	1416,
	1244,
	1405,
	1244,
	1244,
	2458,
	2458,
	1450,
	1416,
	2445,
	2445,
	1416,
	1450,
	1244,
	1416,
	1416,
	1109,
	1109,
	1405,
	1416,
	1244,
	1405,
	1244,
	1244,
	2458,
	2458,
	1450,
	1416,
	2445,
	2445,
	1416,
	1450,
	1244,
	1416,
	1416,
	1406,
	1236,
	1436,
	1450,
	1405,
	1235,
	1436,
	1450,
	1109,
	1109,
	1405,
	1416,
	1244,
	1405,
	1244,
	1244,
	2458,
	2458,
	1450,
	1416,
	2445,
	2445,
	1416,
	1450,
	1244,
	1416,
	1416,
	1109,
	1109,
	1405,
	1416,
	1244,
	1405,
	1244,
	1244,
	2458,
	2458,
	1450,
	1416,
	2445,
	2458,
	1416,
	2445,
	2445,
	1416,
	1450,
	1244,
	1416,
	1109,
	1109,
	1405,
	1416,
	1244,
	1405,
	1244,
	1244,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	2458,
	2458,
	1450,
	1416,
	2445,
	2445,
	1416,
	1450,
	1244,
	1416,
	1416,
	1244,
	1436,
	1450,
	1416,
	1244,
	1436,
	1450,
	1416,
	1244,
	1436,
	1450,
	1416,
	1244,
	1436,
	1450,
	1109,
	1109,
	1405,
	1416,
	1244,
	1405,
	1244,
	1244,
	2458,
	2458,
	1450,
	1416,
	2445,
	2445,
	1416,
	1450,
	1244,
	1416,
	1416,
	1109,
	1109,
	1405,
	1416,
	1244,
	1405,
	1244,
	1244,
	2458,
	2458,
	1450,
	1416,
	2445,
	2458,
	1416,
	2445,
	2445,
	1416,
	1450,
	1244,
	1416,
	1109,
	1109,
	1405,
	1416,
	1244,
	1405,
	1244,
	1244,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	2458,
	2458,
	1450,
	1416,
	2445,
	2445,
	1416,
	1450,
	1244,
	1416,
	1416,
	1244,
	1436,
	1450,
	1416,
	1416,
	1416,
	1416,
	1416,
	1244,
	1436,
	1450,
	1109,
	1109,
	1405,
	1416,
	1244,
	1405,
	1244,
	1244,
	2458,
	2458,
	1450,
	1416,
	2445,
	2458,
	2445,
	2445,
	1416,
	1450,
	1244,
	1416,
	1416,
	1109,
	1109,
	1405,
	1416,
	1244,
	1405,
	1244,
	1244,
	2458,
	2445,
	2445,
	1416,
	1450,
	1244,
	1416,
	1405,
	1235,
	1436,
	1450,
	1405,
	1235,
	1436,
	1450,
	1405,
	1235,
	1436,
	1450,
	1109,
	1109,
	1405,
	1416,
	1244,
	1405,
	1244,
	1244,
	2458,
	2458,
	1450,
	1416,
	2458,
	1450,
	1416,
	2445,
	2458,
	2445,
	2445,
	1416,
	1450,
	1244,
	1416,
	1405,
	1235,
	1436,
	1450,
	1405,
	1235,
	1436,
	1450,
	1405,
	1235,
	1436,
	1450,
	1405,
	1235,
	1436,
	1450,
	1439,
	1263,
	1436,
	1450,
	1406,
	1236,
	1436,
	1450,
	1109,
	1109,
	1405,
	1416,
	1244,
	1405,
	1244,
	1244,
	2458,
	2458,
	1450,
	1416,
	2445,
	2445,
	1416,
	1450,
	1244,
	1416,
	1439,
	1263,
	1436,
	1450,
	1439,
	1263,
	1436,
	1450,
	1439,
	1263,
	1436,
	1450,
	1439,
	1263,
	1436,
	1450,
	1439,
	1263,
	1436,
	1450,
	1406,
	1236,
	1436,
	1450,
	1109,
	1109,
	1405,
	1416,
	1244,
	1405,
	1244,
	1244,
	2458,
	2458,
	1450,
	1416,
	2445,
	2458,
	2445,
	2445,
	1416,
	1450,
	1244,
	1416,
	1416,
	1244,
	1436,
	1450,
	1416,
	1416,
	1416,
	1244,
	1436,
	1450,
	1109,
	1109,
	1405,
	1416,
	1244,
	1405,
	1244,
	1244,
	2458,
	2458,
	1450,
	1416,
	2445,
	2458,
	2445,
	2445,
	1416,
	1450,
	1244,
	1416,
	1405,
	1235,
	1436,
	1450,
	1439,
	1263,
	1436,
	1450,
	1439,
	1263,
	1436,
	1450,
	1439,
	1263,
	1436,
	1450,
	1439,
	1263,
	1436,
	1450,
	1405,
	1235,
	1436,
	1450,
	1109,
	1109,
	1405,
	1416,
	1244,
	1405,
	1244,
	1244,
	2458,
	2458,
	1450,
	1416,
	2445,
	2445,
	1416,
	1450,
	1244,
	1416,
	1416,
	1109,
	1109,
	1405,
	1416,
	1244,
	1405,
	1244,
	1244,
	2458,
	2458,
	1450,
	1416,
	2445,
	2458,
	2445,
	2445,
	1416,
	1450,
	1244,
	1416,
	1416,
	1244,
	1436,
	1450,
	1416,
	1244,
	1436,
	1450,
	1109,
	1109,
	1405,
	1416,
	1244,
	1405,
	1244,
	1244,
	2458,
	2458,
	1450,
	1416,
	2445,
	2445,
	1416,
	1450,
	1244,
	1416,
	1416,
	1244,
	1436,
	1450,
	1416,
	1416,
	1244,
	1436,
	1450,
	1109,
	1109,
	1405,
	1416,
	1244,
	1405,
	1244,
	1244,
	2458,
	2458,
	1450,
	1416,
	1450,
	780,
	1450,
	1450,
	1261,
	1407,
	1237,
	1450,
	1407,
	1436,
	1416,
	1416,
	1450,
	800,
	1109,
	1416,
	1244,
	1416,
	1244,
	1416,
	1244,
	1450,
	1450,
	1416,
	505,
	409,
	2356,
	2142,
	2356,
	505,
	409,
	2458,
	1450,
	1244,
	1450,
	1244,
	1450,
	1244,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	1416,
	1416,
	1109,
	1416,
	505,
	409,
	2356,
	2142,
	2356,
	1450,
	2458,
	1450,
	1244,
	1416,
	1416,
	1450,
	1109,
	505,
	802,
	409,
	606,
	2356,
	799,
	1002,
	407,
	1002,
	799,
	653,
	252,
	1109,
	2445,
	2458,
	2445,
	2445,
	1416,
	1450,
	1244,
	1416,
	1416,
	1244,
	1436,
	1450,
	1416,
	1244,
	1436,
	1450,
	1109,
	1109,
	1405,
	1416,
	1244,
	1405,
	1244,
	1244,
	2458,
	2458,
	1450,
	1416,
	2445,
	2458,
	2445,
	2445,
	1416,
	1450,
	1244,
	1416,
	1405,
	1235,
	1436,
	1450,
	1405,
	1235,
	1436,
	1450,
	1416,
	1416,
	1109,
	1109,
	1405,
	1416,
	1244,
	1405,
	1244,
	1244,
	2458,
	2458,
	1450,
	1416,
};
static const Il2CppTokenIndexMethodTuple s_reversePInvokeIndices[6] = 
{
	{ 0x06000086, 10,  (void**)&Protobuf_LogProtobufMessage_m1B59C21023D21E2AB5CA458EC71B9F43DA4E587D_RuntimeMethod_var, 0 },
	{ 0x060000C3, 3,  (void**)&ImageFrame_ReleasePixelData_mDF81706134475903E835D1F61193281D8FB96A78_RuntimeMethod_var, 0 },
	{ 0x06000863, 1,  (void**)&AssetBundleManager_PathToResourceAsFile_mE4EA5782FC953FF16031FD0EB659BD758E0EDC7E_RuntimeMethod_var, 0 },
	{ 0x06000864, 0,  (void**)&AssetBundleManager_GetResourceContents_mDFE89208C34422AEE3F365704F969ADFA843BA84_RuntimeMethod_var, 0 },
	{ 0x0600087C, 6,  (void**)&LocalAssetManager_PathToResourceAsFile_mD93EA6538E2E77CCD4ACB981201294DFFDF0C3A6_RuntimeMethod_var, 0 },
	{ 0x0600087D, 5,  (void**)&LocalAssetManager_GetResourceContents_m36D0D7964026D770978C9BECA4F18D510A46648A_RuntimeMethod_var, 0 },
};
static const Il2CppTokenRangePair s_rgctxIndices[41] = 
{
	{ 0x0200000F, { 0, 4 } },
	{ 0x02000029, { 14, 3 } },
	{ 0x02000044, { 18, 1 } },
	{ 0x0200004C, { 21, 2 } },
	{ 0x02000050, { 23, 4 } },
	{ 0x020000E3, { 55, 4 } },
	{ 0x06000084, { 4, 1 } },
	{ 0x06000085, { 5, 4 } },
	{ 0x0600009E, { 9, 3 } },
	{ 0x0600009F, { 12, 2 } },
	{ 0x060000D8, { 17, 1 } },
	{ 0x06000160, { 19, 2 } },
	{ 0x0600044D, { 27, 1 } },
	{ 0x0600044E, { 28, 1 } },
	{ 0x0600044F, { 29, 1 } },
	{ 0x06000450, { 30, 1 } },
	{ 0x06000451, { 31, 1 } },
	{ 0x06000452, { 32, 1 } },
	{ 0x06000453, { 33, 1 } },
	{ 0x06000638, { 34, 1 } },
	{ 0x06000639, { 35, 1 } },
	{ 0x0600063A, { 36, 1 } },
	{ 0x0600063B, { 37, 1 } },
	{ 0x0600063C, { 38, 1 } },
	{ 0x0600063D, { 39, 1 } },
	{ 0x0600063E, { 40, 1 } },
	{ 0x060006B2, { 41, 1 } },
	{ 0x060006B3, { 42, 1 } },
	{ 0x060006B4, { 43, 1 } },
	{ 0x060006B5, { 44, 1 } },
	{ 0x060006B6, { 45, 1 } },
	{ 0x060006B7, { 46, 1 } },
	{ 0x060006B8, { 47, 1 } },
	{ 0x06000703, { 48, 1 } },
	{ 0x06000704, { 49, 1 } },
	{ 0x06000705, { 50, 1 } },
	{ 0x06000706, { 51, 1 } },
	{ 0x06000707, { 52, 1 } },
	{ 0x06000708, { 53, 1 } },
	{ 0x06000709, { 54, 1 } },
	{ 0x06000875, { 59, 2 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[61] = 
{
	{ (Il2CppRGCTXDataType)3, 10614 },
	{ (Il2CppRGCTXDataType)2, 432 },
	{ (Il2CppRGCTXDataType)3, 4942 },
	{ (Il2CppRGCTXDataType)3, 4943 },
	{ (Il2CppRGCTXDataType)3, 6420 },
	{ (Il2CppRGCTXDataType)2, 2532 },
	{ (Il2CppRGCTXDataType)3, 5058 },
	{ (Il2CppRGCTXDataType)3, 10862 },
	{ (Il2CppRGCTXDataType)3, 5059 },
	{ (Il2CppRGCTXDataType)2, 765 },
	{ (Il2CppRGCTXDataType)3, 122 },
	{ (Il2CppRGCTXDataType)3, 123 },
	{ (Il2CppRGCTXDataType)2, 3285 },
	{ (Il2CppRGCTXDataType)3, 8322 },
	{ (Il2CppRGCTXDataType)1, 508 },
	{ (Il2CppRGCTXDataType)2, 508 },
	{ (Il2CppRGCTXDataType)3, 7019 },
	{ (Il2CppRGCTXDataType)2, 3795 },
	{ (Il2CppRGCTXDataType)2, 2959 },
	{ (Il2CppRGCTXDataType)1, 277 },
	{ (Il2CppRGCTXDataType)2, 277 },
	{ (Il2CppRGCTXDataType)3, 8334 },
	{ (Il2CppRGCTXDataType)3, 8333 },
	{ (Il2CppRGCTXDataType)3, 8335 },
	{ (Il2CppRGCTXDataType)2, 3294 },
	{ (Il2CppRGCTXDataType)2, 2940 },
	{ (Il2CppRGCTXDataType)3, 7011 },
	{ (Il2CppRGCTXDataType)3, 10452 },
	{ (Il2CppRGCTXDataType)3, 10451 },
	{ (Il2CppRGCTXDataType)3, 10479 },
	{ (Il2CppRGCTXDataType)3, 10521 },
	{ (Il2CppRGCTXDataType)3, 10493 },
	{ (Il2CppRGCTXDataType)3, 10418 },
	{ (Il2CppRGCTXDataType)3, 10419 },
	{ (Il2CppRGCTXDataType)3, 10464 },
	{ (Il2CppRGCTXDataType)3, 10463 },
	{ (Il2CppRGCTXDataType)3, 10485 },
	{ (Il2CppRGCTXDataType)3, 10522 },
	{ (Il2CppRGCTXDataType)3, 10499 },
	{ (Il2CppRGCTXDataType)3, 10425 },
	{ (Il2CppRGCTXDataType)3, 10426 },
	{ (Il2CppRGCTXDataType)3, 10474 },
	{ (Il2CppRGCTXDataType)3, 10473 },
	{ (Il2CppRGCTXDataType)3, 10490 },
	{ (Il2CppRGCTXDataType)3, 10524 },
	{ (Il2CppRGCTXDataType)3, 10504 },
	{ (Il2CppRGCTXDataType)3, 10432 },
	{ (Il2CppRGCTXDataType)3, 10433 },
	{ (Il2CppRGCTXDataType)3, 10476 },
	{ (Il2CppRGCTXDataType)3, 10475 },
	{ (Il2CppRGCTXDataType)3, 10491 },
	{ (Il2CppRGCTXDataType)3, 10525 },
	{ (Il2CppRGCTXDataType)3, 10505 },
	{ (Il2CppRGCTXDataType)3, 10434 },
	{ (Il2CppRGCTXDataType)3, 10435 },
	{ (Il2CppRGCTXDataType)2, 362 },
	{ (Il2CppRGCTXDataType)2, 774 },
	{ (Il2CppRGCTXDataType)3, 292 },
	{ (Il2CppRGCTXDataType)3, 293 },
	{ (Il2CppRGCTXDataType)2, 854 },
	{ (Il2CppRGCTXDataType)3, 986 },
};
extern const CustomAttributesCacheGenerator g_MediaPipeSDK_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_MediaPipeSDK_CodeGenModule;
const Il2CppCodeGenModule g_MediaPipeSDK_CodeGenModule = 
{
	"MediaPipeSDK.dll",
	2253,
	s_methodPointers,
	8,
	s_adjustorThunks,
	s_InvokerIndices,
	6,
	s_reversePInvokeIndices,
	41,
	s_rgctxIndices,
	61,
	s_rgctxValues,
	NULL,
	g_MediaPipeSDK_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
