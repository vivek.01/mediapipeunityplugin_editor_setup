﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;


namespace Mediapipe
{
    class Vrikshasana : MonoBehaviour
    {
        public static Text aIText;
        public static float timeRemaining = 10;
        public static bool timerIsRunning = false;
        public static Text timeText;
        public static bool staticFlag4 = false;
        public static float averageMarks=0;
        public static List<GameObject> NodesC;
        public static List<GameObject> EdgesC;
        public static bool startFlag = false;

        List<List<float>> keyPointsC;

        private void Start()
        {

        }
        void Update()
        {

        }
        //private void Awake()
        //{
        //   aIText.text = GameObject.FindGameObjectWithTag("AIText").GetComponent<Text>().text;
        //}
        //public static List<GameObject> refNodeGameObjects;

        static void nodeColour(int[] nodeArr, float r, float g, float b)
        {
            if (startFlag == true)
            {
                foreach (int i in nodeArr)
                {
                    //NodesC[i].GetComponent<Renderer>().material.color = new Color(255, 255, 255, 255);
                    NodesC[i].GetComponent<Renderer>().material.color = new Color(r, g, b, 255);
                    Transform ChildNoderAnnotation = NodesC[i].transform.GetChild(0);
                    ChildNoderAnnotation.GetComponent<Transform>().localScale = new Vector3(2, 2, 2);
                    ChildNoderAnnotation.GetComponent<Renderer>().material.color = new Color(r, g, b, 0.4f);
                }
            }
        }
        static void edgeColour(int[] edgeArr, int r, int g, int b)
        {
            foreach (int i in edgeArr)
            {
                EdgesC[i].GetComponent<LineRenderer>().material.color = new Color(r, g, b, 200);
            }
        }

        static double distance(List<float> p1, List<float> p2)
        {
            return Math.Sqrt(Math.Pow(p1[1] - p2[1], 2) + Math.Pow(p1[2] - p2[2], 2));
        }
        static double angleBetween(List<float> p1, List<float> o, List<float> p2)
        {
            var p1Vector = new UnityEngine.Vector3(p1[1], p1[2], 0.0f);
            var p2Vector = new UnityEngine.Vector3(p2[1], p2[2], 0.0f);
            var oVector = new UnityEngine.Vector3(o[1], o[2], 0.0f);



            UnityEngine.Vector3 edgeba = oVector - p1Vector;
            UnityEngine.Vector3 edgebc = oVector - p2Vector;
            float angle = UnityEngine.Vector3.Angle(edgebc, edgeba);



            return angle;
        }
        static bool inRange(double value, float minValue, float maxValue)
        {
            if (value < maxValue && value > minValue)
            {
                return true;
            }
            return false;
        }
        internal static void Start(List<List<float>> keyPoints, List<GameObject> Nodes, List<GameObject> Edges, Text aIText)
        {
            NodesC = Nodes;
            EdgesC = Edges;
            //aIText.text = GameObject.FindGameObjectWithTag("AIText").GetComponent<Text>().text;
            string recomendationText = "";
            bool flag1 = false;
            bool flag2 = false;
            bool flag3 = false;
            int marks = 0;
            int expertMarks = 30;
            int intermediateMarks = 20;
            int beginnerMarks = 10;
            Tuple<float, float, float> expertColour = new Tuple<float, float, float>(0,255,0);
            Tuple<float, float, float> intermediateColour = new Tuple<float, float, float>(0,0,255);
            Tuple<float, float, float> beginnerColour = new Tuple<float, float, float>(0, 0, 255);
            Tuple<float, float, float> failureColour = new Tuple<float, float, float>(255, 0, 0);



            var rightShoulderArmAngle = angleBetween(keyPoints[11], keyPoints[12], keyPoints[14]);
            var leftShoulderArmAngle = angleBetween(keyPoints[12], keyPoints[11], keyPoints[13]);
            var rightElbowAngle = angleBetween(keyPoints[12], keyPoints[14], keyPoints[16]);
            var leftElbowAngle = angleBetween(keyPoints[11], keyPoints[13], keyPoints[15]);



            if (keyPoints[16][2] > keyPoints[5][2] && keyPoints[17][2] > keyPoints[2][2] && (distance(keyPoints[21], keyPoints[22])) < 25)
            {
                if (rightShoulderArmAngle>70&&rightShoulderArmAngle<93 &&
                    rightElbowAngle>165&&
                    leftShoulderArmAngle>70 && leftShoulderArmAngle<93&&
                    leftElbowAngle>165 )
                {
                    //Expert
                    flag1 = true;
                    marks = marks + expertMarks;
                    Debug.Log("Great job!! with your hands");
                    recomendationText = "Great job!! with your hands\n";


                    nodeColour(new int[] { 11, 12, 13, 14, 15, 16 }, expertColour.Item1, expertColour.Item2, expertColour.Item3);
                    //Nodes[11].transform.GetChild(0).GetComponent<ParticleSystem>().Play();
                    //Nodes[12].transform.GetChild(0).GetComponent<ParticleSystem>().Play();
                    //Nodes[13].transform.GetChild(0).GetComponent<ParticleSystem>().Play();
                    //Nodes[14].transform.GetChild(0).GetComponent<ParticleSystem>().Play();
                    //Nodes[15].transform.GetChild(0).GetComponent<ParticleSystem>().Play();
                    //Nodes[16].transform.GetChild(0).GetComponent<ParticleSystem>().Play();

                    //edgeColour(new int[] { 0, 1, 2, 3 }, 0, 0, 225);

                }
                else if (rightShoulderArmAngle < 97 &&
                    rightElbowAngle > 157 && 
                    leftShoulderArmAngle < 97 &&
                    leftElbowAngle > 157 )
                {
                    
                    //Intermediate
                    flag1 = true;
                    marks = marks + intermediateMarks;
                    Debug.Log("Good job! streach your arms further.");
                    recomendationText = "Good job! streach your arms further.\n";


                    nodeColour(new int[] { 11, 12, 13, 14, 15, 16 }, intermediateColour.Item1, intermediateColour.Item2, intermediateColour.Item3);
                    //Nodes[11].transform.GetChild(0).GetComponent<ParticleSystem>().Play();
                    //Nodes[12].transform.GetChild(0).GetComponent<ParticleSystem>().Play();
                    //Nodes[13].transform.GetChild(0).GetComponent<ParticleSystem>().Play();
                    //Nodes[14].transform.GetChild(0).GetComponent<ParticleSystem>().Play();
                    //Nodes[15].transform.GetChild(0).GetComponent<ParticleSystem>().Play();
                    //Nodes[16].transform.GetChild(0).GetComponent<ParticleSystem>().Play();

                    //edgeColour(new int[] { 0, 1, 2, 3 }, 0, 0, 225);

                    
                }
                else if ( rightShoulderArmAngle <= 100 &&
                    rightElbowAngle > 150 && 
                    leftShoulderArmAngle <= 100 &&
                    leftElbowAngle > 150 )
                {
                    
                    //Beginner
                    flag1 = true;
                    marks = marks + beginnerMarks;
                    Debug.Log("Good job! Stretch your arms further.");
                    recomendationText = "Good job! Stretch your arms further.\n";


                    nodeColour(new int[] { 11, 12, 13, 14, 15, 16 }, beginnerColour.Item1, beginnerColour.Item2, beginnerColour.Item3);
                    //Nodes[11].transform.GetChild(0).GetComponent<ParticleSystem>().Play();
                    //Nodes[12].transform.GetChild(0).GetComponent<ParticleSystem>().Play();
                    //Nodes[13].transform.GetChild(0).GetComponent<ParticleSystem>().Play();
                    //Nodes[14].transform.GetChild(0).GetComponent<ParticleSystem>().Play();
                    //Nodes[15].transform.GetChild(0).GetComponent<ParticleSystem>().Play();
                    //Nodes[16].transform.GetChild(0).GetComponent<ParticleSystem>().Play();

                    //edgeColour(new int[] { 0, 1, 2, 3 }, 0, 0, 225);

                    
                }
                else
                {
                    Debug.Log("Stretch arms straight.");
                    recomendationText = "Stretch arms straight.\n";

                    nodeColour(new int[] { 11, 12, 13, 14 }, failureColour.Item1, failureColour.Item2, failureColour.Item3);

                    nodeColour(new int[] { 15, 16 }, failureColour.Item1, failureColour.Item2, failureColour.Item3);
                    //Nodes[11].transform.GetChild(0).GetComponent<ParticleSystem>().Stop();
                    //Nodes[12].transform.GetChild(0).GetComponent<ParticleSystem>().Stop();
                    //Nodes[13].transform.GetChild(0).GetComponent<ParticleSystem>().Stop();
                    //Nodes[14].transform.GetChild(0).GetComponent<ParticleSystem>().Stop();

                    //edgeColour(new int[] { 0, 1, 2, 3 }, 255, 0, 0);

                }
            }
            else
            {
                Debug.Log("Join your hands above your head");
                recomendationText = "Join your hands above your head.\n";

                nodeColour(new int[] { 11, 12, 13, 14, 15, 16 }, failureColour.Item1, failureColour.Item2, failureColour.Item3);
                //Nodes[11].transform.GetChild(0).GetComponent<ParticleSystem>().Stop();
                //Nodes[12].transform.GetChild(0).GetComponent<ParticleSystem>().Stop();
                //Nodes[13].transform.GetChild(0).GetComponent<ParticleSystem>().Stop();
                //Nodes[14].transform.GetChild(0).GetComponent<ParticleSystem>().Stop();
                //Nodes[15].transform.GetChild(0).GetComponent<ParticleSystem>().Stop();
                //Nodes[16].transform.GetChild(0).GetComponent<ParticleSystem>().Stop();

                //edgeColour(new int[] { 0, 1, 2, 3 }, 255, 0, 0);
            }


            var rightAbdomenLegAngle = angleBetween(keyPoints[23], keyPoints[24], keyPoints[26]);
            var rightKneeAngle = angleBetween(keyPoints[24], keyPoints[26], keyPoints[28]);
            var leftAbdomenLegAngle = angleBetween(keyPoints[24], keyPoints[23], keyPoints[25]);
            var leftKneeAngle = angleBetween(keyPoints[23], keyPoints[25], keyPoints[27]);



            // && keyPoints[30][1] < keyPoints[23][1] && keyPoints[30][1] > keyPoints[8][1]
            if (rightAbdomenLegAngle >= 135 && rightKneeAngle <= 31 && keyPoints[28][2] >= keyPoints[25][2])
            {
                //Expert
                flag2 = true;
                marks = marks + expertMarks;
                Debug.Log("Great! Right leg in place.");
                recomendationText = recomendationText + "Great! Right leg in place.\n";

                nodeColour(new int[] { 24, 26, 28, 30 }, expertColour.Item1, expertColour.Item2, expertColour.Item3);

                //edgeColour(new int[] { 13, 14, 15, 16, 17 }, 0, 0, 225);
            }
            else if (rightAbdomenLegAngle>=131 && rightAbdomenLegAngle<135 &&
                rightKneeAngle>31 && rightKneeAngle<=36
                 && keyPoints[28][2] >= keyPoints[25][2])
            {
                //Intermediate
                flag2 = true;
                marks = marks + intermediateMarks;
                Debug.Log("Good! pull your foot closer.");
                recomendationText = recomendationText + "Good! pull your foot closer.\n";

                nodeColour(new int[] { 24, 26, 28, 30 }, intermediateColour.Item1, intermediateColour.Item2, intermediateColour.Item3);
            }
            else if (rightAbdomenLegAngle >= 125 && rightAbdomenLegAngle < 131 &&
                rightKneeAngle > 36 && rightKneeAngle < 42
                 && keyPoints[28][2] >= keyPoints[25][2])
            {
                //Beginner
                flag2 = true;
                marks = marks + beginnerMarks;
                Debug.Log("Good! pull your foot closer.");
                recomendationText = recomendationText + "Good! pull your foot closer.\n";

                nodeColour(new int[] { 24, 26, 28, 30 }, beginnerColour.Item1, beginnerColour.Item2, beginnerColour.Item3);
            }
            else if (rightKneeAngle >= 42 && keyPoints[28][2] <= keyPoints[25][2] && rightKneeAngle < 110)
            {
                Debug.Log("Pull your foot closer and place on left tigh");
                recomendationText = recomendationText + "Pull your foot closer and place on left tigh.\n";
               
                nodeColour(new int[] { 24 }, 0, 0, 225);
                
                nodeColour(new int[] { 26, 28, 30, 32 }, 255, 0, 0);
 
                //Nodes[26].transform.GetChild(0).GetComponent<ParticleSystem>().Stop();
                //Nodes[28].transform.GetChild(0).GetComponent<ParticleSystem>().Stop();
              
            }
            else
            {

                Debug.Log("Place your right foot on left tigh");
                recomendationText = recomendationText + "Place your right foot on left tigh.\n";

                nodeColour(new int[] { 24, 26, 28, 30, 32 }, failureColour.Item1, failureColour.Item2, failureColour.Item3);
                //Nodes[24].transform.GetChild(0).GetComponent<ParticleSystem>().Stop();
                //Nodes[26].transform.GetChild(0).GetComponent<ParticleSystem>().Stop();
                //Nodes[28].transform.GetChild(0).GetComponent<ParticleSystem>().Stop();

                //edgeColour(new int[] { 13, 14, 15, 16, 17 }, 255, 0, 0);
            }


            if (leftAbdomenLegAngle > 88 && leftAbdomenLegAngle <= 92 && leftKneeAngle >= 178)
            {
                //Expert
                marks = marks + expertMarks;
                flag3 = true;
                Debug.Log("Great! Left leg is straight.");
                recomendationText = recomendationText + "Great! Left leg is straight.\n";

                nodeColour(new int[] { 23, 25, 27, 29, 31 }, expertColour.Item1, expertColour.Item2, expertColour.Item3);

                //edgeColour(new int[] { 8, 9, 10, 11, 12 }, 0, 0, 225);

            }
            else if (leftAbdomenLegAngle > 85 && leftAbdomenLegAngle <= 95 && leftKneeAngle >= 175)
            {
                //Intermediate
                flag3 = true;
                marks = marks + intermediateMarks;
                Debug.Log("Good! straighten your leg more.");
                recomendationText = recomendationText + "Good! straighten your leg more.\n";

                nodeColour(new int[] { 23, 25, 27, 29, 31 }, intermediateColour.Item1, intermediateColour.Item2, intermediateColour.Item3);

                //edgeColour(new int[] { 8, 9, 10, 11, 12 }, 0, 0, 225);

            }
            else if (leftAbdomenLegAngle > 83 && leftAbdomenLegAngle <= 98 && leftKneeAngle >= 172)
            {
                //Beginner
                flag3 = true;
                marks = marks + beginnerMarks;

                Debug.Log("Good! straighten your leg more.");
                recomendationText = recomendationText + "Good! straighten your leg more.\n";

                nodeColour(new int[] { 23, 25, 27, 29, 31 }, beginnerColour.Item1, beginnerColour.Item2, beginnerColour.Item3);


                //edgeColour(new int[] { 8, 9, 10, 11, 12 }, 0, 0, 225);

            }
            else
            {

                Debug.Log("Straighten your left leg.");
                recomendationText = recomendationText + "Straighten your left leg.\n";

                nodeColour(new int[] { 23, 25, 27, 29, 31 }, failureColour.Item1, failureColour.Item2, failureColour.Item3);
                //Nodes[23].transform.GetChild(0).GetComponent<ParticleSystem>().Stop();
                //Nodes[25].transform.GetChild(0).GetComponent<ParticleSystem>().Stop();
                //Nodes[27].transform.GetChild(0).GetComponent<ParticleSystem>().Stop();

                //edgeColour(new int[] { 8, 9, 10, 11, 12 }, 255, 0, 0);

            }
            Debug.Log("Rec=============================");
            Debug.Log(recomendationText);
            aIText.text = recomendationText;

            if (marks == 90)
            {
                marks = 100;
            }



            Debug.Log("--------------------------------------------------------------------");
            if (flag1 == true && flag2 == true && flag3 == true)
            {
                if (staticFlag4 == false)

                {
                    staticFlag4 = true;
                    startFlag = true;
                    GameObject.FindGameObjectWithTag("timeText").GetComponent<AudioSource>().Play();
                    timerIsRunning = true;
                    timeText = GameObject.FindGameObjectWithTag("timeText").GetComponent<Text>();
                    Debug.Log("Rec------------------------------------------------------------TIm,e=============================");
                    Debug.Log("Rec------------------------------------------------------------TIm,e=============================");
                }
            }
            Debug.Log("timer is running " + timerIsRunning);      
            if (timerIsRunning && flag1 == true && flag2 == true && flag3 == true)
            {
                timeText = GameObject.FindGameObjectWithTag("timeText").GetComponent<Text>();
                Debug.Log("Rec-----------------update---------------------TIm,e=============================");
                if (timeRemaining > 0)
                {
                    timeRemaining -= Time.deltaTime;
                    timeText.text = timeRemaining.ToString();
                    averageMarks = (averageMarks + marks) / 2;
                }
                //else if (0< timeRemaining && timeRemaining  < 0.2)
                //{

                //}
                else
                {
                    Debug.Log("Time has run out!");
                    Debug.Log("Average Marks - " + averageMarks);
                    timeRemaining = 0;
                    GameObject.FindGameObjectWithTag("AIText").GetComponent<AudioSource>().Play();
                    timerIsRunning = false;
                }
            }

        }

    }
}
