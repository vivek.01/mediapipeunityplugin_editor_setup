using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Edge = System.Tuple<int ,int>;
using UnityEngine.UI;


namespace Mediapipe {
  public abstract class LandmarkListAnnotationController : AnnotationController {
    [SerializeField] protected GameObject nodePrefab = null;
    [SerializeField] protected GameObject edgePrefab = null;
    [SerializeField] protected float NodeScale = 0.5f;

    public List<GameObject> Nodes;
    private List<GameObject> Edges;
        Text rightElboAngleOK;
        Text LeftElboAngleOK;
        Text RightSholderOK;
        Text LeftSholderOK;
        Text rightHipAngleOK;
        Text LeftHipAngleOK;
        Text LeftKneeAngleOK;
        Text RightKneeAngleOK;
        Text aIText;
        int[,] keyPointsArr = new int[33,3];
        List<List<float>> keyPoints = new List<List<float>>();
        void Awake() {
      Nodes = new List<GameObject>(NodeSize);
      for (var i = 0; i < NodeSize; i++) {
                GameObject Node = Instantiate(nodePrefab);
                Node.name = "Point" + i;
                //Node.transform.GetChild(0).GetComponent<ParticleSystem>().Stop();
        Nodes.Add(Node);
      }
      Edges = new List<GameObject>(EdgeSize);
      for (var i = 0; i < EdgeSize; i++) {
                GameObject Edge = Instantiate(edgePrefab);
                Edge.name = "Line" + i;
                Edges.Add(Edge);
      }
            //rightElboAngleOK = GameObject.FindGameObjectWithTag("rightElboAngleOK").GetComponent<Text>();
            //LeftElboAngleOK = GameObject.FindGameObjectWithTag("LeftElboAngleOK").GetComponent<Text>();
            //RightSholderOK = GameObject.FindGameObjectWithTag("RightSholderOK").GetComponent<Text>();
            //LeftSholderOK = GameObject.FindGameObjectWithTag(" LeftSholderOK").GetComponent<Text>();
            //rightHipAngleOK = GameObject.FindGameObjectWithTag(" rightHipAngleOK").GetComponent<Text>();
            //LeftHipAngleOK = GameObject.FindGameObjectWithTag("LeftHipAngleOK").GetComponent<Text>();
            //LeftKneeAngleOK = GameObject.FindGameObjectWithTag("LeftKneeAngleOK").GetComponent<Text>();
            //RightKneeAngleOK = GameObject.FindGameObjectWithTag("RightKneeAngleOK").GetComponent<Text>();
            aIText = GameObject.FindGameObjectWithTag("AIText").GetComponent<Text>();

        }
        private void Start()
        {
            for (int i = 0; i < 10; i++)
            {
                Nodes[i].GetComponent<MeshRenderer>().enabled = false;
            }
            for (int i = 17; i < 23; i++)
            {
                Nodes[i].GetComponent<MeshRenderer>().enabled = false;
            }
            Nodes[0].GetComponent<Renderer>().material.color = new Color(255,255,255, 255);

        }
        private void Update()
        {
            //for (int ii=0; ii < 33; ii++)
            //{

            //    keyPointsArr[ii, 0] = ii;
            //    keyPointsArr[ii, 1] = (int)Nodes[ii].transform.localPosition.x;
            //    keyPointsArr[ii, 2] = (int)Nodes[ii].transform.localPosition.y;


            //}
            //for (int i = 0; i < keyPointsArr.GetLength(0); i++)
            //{
            //    for (int j = 0; j < keyPointsArr.GetLength(1); j++)
            //    {
            //        Debug.Log("p " + keyPointsArr[i, j]);
            //    }
            //    Debug.Log("------");
            //}

            //int[,] keyPointsArr = new int[,] { { 0, 751, 268 }, { 1, 762, 265 }, { 2, 764, 267 }, { 3, 766, 268 }, { 4, 762, 265 }, { 5, 763, 266 }, { 6, 764, 267 }, { 7, 773, 282 }, { 8, 771, 279 }, { 9, 749, 280 }, { 10, 748, 280 }, { 11, 762, 324 }, { 12, 751, 317 }, { 13, 736, 248 }, { 14, 734, 251 }, { 15, 732, 178 }, { 16, 728, 184 }, { 17, 730, 162 }, { 18, 727, 167 }, { 19, 737, 160 }, { 20, 733, 164 }, { 21, 739, 166 }, { 22, 735, 171 }, { 23, 758, 469 }, { 24, 733, 467 }, { 25, 829, 553 }, { 26, 635, 512 }, { 27, 911, 623 }, { 28, 634, 612 }, { 29, 929, 635 }, { 30, 643, 632 }, { 31, 884, 653 }, { 32, 591, 635 } };


            //for (int i = 0; i < keyPointsArr.GetLength(0); i++)
            //{

            //    keyPoints.Add(new List<float> { keyPointsArr[i, 0], keyPointsArr[i, 1], keyPointsArr[i, 2] });

            //}


            //for (int i = 0; i < keyPointsArr.GetLength(0); i++)
            //{
            //    for (int j = 0; j < keyPointsArr.GetLength(1); j++)
            //    {
            //        Debug.Log("p "+ keyPointsArr[i, j]);
            //    }
            //    Debug.Log("------");
            //}
            keyPoints.Clear();
            for (var i = 0; i < NodeSize; i++)
            {
                float xx = Nodes[i].transform.localPosition.x;
                float yy = Nodes[i].transform.position.y;

           
                List<float> aa = new List<float>();
                aa.Add(i);
                aa.Add(xx);
                aa.Add(yy);
                keyPoints.Add(aa);
               

            }
            //Debug.Log("sddsdsdsds");
            //Vrukshasana.Start(keyPoints, Nodes, Edges,aIText);
            Vrikshasana.Start(keyPoints, Nodes, Edges, aIText);
            //Tadasana.Start(keyPoints, Nodes, Edges, aIText);

            /*
            Vector3 edge1412 = Nodes[14].transform.position - Nodes[12].transform.position;
            Vector3 edge1416 = Nodes[14].transform.position - Nodes[16].transform.position;
            float angle_RightElbo = Vector3.Angle(edge1416, edge1412);
            Debug.Log("rightElboAngle-----------" + angle_RightElbo);
            Vector3 edge1311 = Nodes[13].transform.position - Nodes[11].transform.position;
            Vector3 edge1315 = Nodes[13].transform.position - Nodes[15].transform.position;
            float angle_LeftElbo = Vector3.Angle(edge1315, edge1311);
            Debug.Log("LeftElboAngle-----------" + angle_LeftElbo);
            Vector3 edge1214 = Nodes[12].transform.position - Nodes[14].transform.position;
            Vector3 edge1211 = Nodes[12].transform.position - Nodes[11].transform.position;
            float angle_RightSholder = Vector3.Angle(edge1211, edge1214);
            Debug.Log("RightSholder-----------" + angle_RightSholder);
            Vector3 edge1113 = Nodes[11].transform.position - Nodes[13].transform.position;
            Vector3 edge1112 = Nodes[11].transform.position - Nodes[12].transform.position;
            float angle_LeftSholder = Vector3.Angle(edge1112, edge1113);
            Debug.Log("LeftSholder-----------" + angle_LeftSholder);
            Vector3 edge2423 = Nodes[24].transform.position - Nodes[23].transform.position;
            Vector3 edge2426 = Nodes[24].transform.position - Nodes[26].transform.position;
            float angle_RightHip = Vector3.Angle(edge2426, edge2423);
            Debug.Log("rightHipAngle-----------" + angle_RightHip);
            Vector3 edge2324 = Nodes[23].transform.position - Nodes[24].transform.position;
            Vector3 edge2325 = Nodes[23].transform.position - Nodes[25].transform.position;
            float angle_LeftHip = Vector3.Angle(edge2325, edge2324);
            Debug.Log("LeftHipAngle-----------" + angle_LeftHip);
            Vector3 edge2523 = Nodes[25].transform.position - Nodes[23].transform.position;
            Vector3 edge2527 = Nodes[25].transform.position - Nodes[27].transform.position;
            float angle_LeftKnee = Vector3.Angle(edge2527, edge2523);
            Debug.Log("LeftKneeAngle-----------" + angle_LeftKnee);
            Vector3 edge2624 = Nodes[26].transform.position - Nodes[24].transform.position;
            Vector3 edge2628 = Nodes[26].transform.position - Nodes[28].transform.position;
            float angle_RightKnee = Vector3.Angle(edge2628, edge2624);
            Debug.Log("RightKneeAngle-----------" + angle_RightKnee);


            if (angle_RightSholder < 100 && angle_LeftSholder < 100 && angle_RightElbo > 150 && angle_LeftElbo > 150)
            {
                Debug.Log("OK");
                rightElboAngleOK.text = "OK";
                LeftElboAngleOK.text = "OK";
                RightSholderOK.text = "OK";
                LeftSholderOK.text = "OK";
                
            }
            else
            {
                Debug.Log("Not Ok");
                rightElboAngleOK.text = "NotOK";
                LeftElboAngleOK.text = "NotOK";
                RightSholderOK.text = "NotOK";
                LeftSholderOK.text = "NotOK";
            }



            if (angle_RightHip > 135 && angle_RightKnee < 50)
            {
                Debug.Log("OK");
                rightHipAngleOK.text = "OK";
                RightKneeAngleOK.text = "OK";
            }
            else
            {
                Debug.Log("Not OK");
                rightHipAngleOK.text = "NotOK";
                RightKneeAngleOK.text = "NotOK";
            }



            if (angle_LeftHip > 80 && angle_LeftHip < 100 && angle_LeftKnee > 170)
            {
                Debug.Log("Ok");
                LeftHipAngleOK.text = "OK";
                LeftKneeAngleOK.text = "OK";
            }
            else
            {
                Debug.Log("Not Ok");
                LeftHipAngleOK.text = "NotOK";
                LeftKneeAngleOK.text = "NotOK";
            }*/

        }

        void OnDestroy() {
      foreach (var landmark in Nodes) {
        Destroy(landmark);
      }

      foreach (var line in Edges) {
        Destroy(line);
      }
    }

    public override void Clear() {
      foreach (var landmark in Nodes) {
        landmark.GetComponent<NodeAnnotationController>().Clear();
      }

      foreach (var line in Edges) {
        line.GetComponent<EdgeAnnotationController>().Clear();
      }
    }

    /// <summary>
    ///   Renders landmarks on a screen.
    ///   It is assumed that the screen is vertical to terrain and not inverted.
    /// </summary>
    /// <param name="isFlipped">
    ///   if true, x axis is oriented from right to left (top-right point is (0, 0) and bottom-left is (1, 1))
    /// </param>
    /// <remarks>
    ///   In <paramref name="landmarkList" />, y-axis is oriented from top to bottom.
    /// </remarks>
    public void Draw(Transform screenTransform, NormalizedLandmarkList landmarkList, bool isFlipped = false) {
      if (isEmpty(landmarkList)) {
        Clear();
        return;
      }

      for (var i = 0; i < NodeSize; i++) {
        var landmark = landmarkList.Landmark[i];
        var node = Nodes[i];

        node.GetComponent<NodeAnnotationController>().Draw(screenTransform, landmark, isFlipped, NodeScale);
      }

      for (var i = 0; i < EdgeSize; i++) {
        var connection = Connections[i];
        var edge = Edges[i];

        var a = Nodes[connection.Item1];
        var b = Nodes[connection.Item2];

        edge.GetComponent<EdgeAnnotationController>().Draw(screenTransform, a, b);
      }
    }

    protected abstract IList<Edge> Connections { get; }

    protected abstract int NodeSize { get; }

    protected int EdgeSize {
      get { return Connections.Count; }
    }

    private bool isEmpty(NormalizedLandmarkList landmarkList) {
      return landmarkList.Landmark.All(landmark => {
        return landmark.X == 0 && landmark.Y == 0 && landmark.Z == 0;
      });
    }
  }
}
