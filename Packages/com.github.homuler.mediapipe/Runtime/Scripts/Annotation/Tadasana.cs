﻿using System;
using System.Collections.Generic;
using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Mediapipe

{
    class Tadasana : MonoBehaviour
    {
        public static Text aIText;
        public static float timeRemaining = 10;
        public static bool timerIsRunning = false;
        public static Text timeText;
        public static bool staticFlag4 = false;
        public static float averageMarks;
        public static List<GameObject> NodesC;
        static double distance(List<float> p1, List<float> p2)
        {
            return Math.Sqrt(Math.Pow(p1[1] - p2[1], 2) + Math.Pow(p1[2] - p2[2], 2));
        }
        static double angleBetween(List<float> p1, List<float> o, List<float> p2)
        {

            var p1Vector = new UnityEngine.Vector3(p1[1], p1[2], 0.0f);
            var p2Vector = new UnityEngine.Vector3(p2[1], p2[2], 0.0f);
            var oVector = new UnityEngine.Vector3(o[1], o[2], 0.0f);

            UnityEngine.Vector3 edgeba = oVector - p1Vector;
            UnityEngine.Vector3 edgebc = oVector - p2Vector;
            float angle = UnityEngine.Vector3.Angle(edgebc, edgeba);



            return angle;
        }
        static void nodeColour(int[] nodeArr, int r, int g, int b)
        {
            foreach (int i in nodeArr)
            {
                NodesC[i].GetComponent<Renderer>().material.color = new Color(r, g, b, 225);
                Transform ChildNoderAnnotation = NodesC[i].transform.GetChild(0);
                ChildNoderAnnotation.GetComponent<Transform>().localScale = new Vector3(3, 3, 3);
                ChildNoderAnnotation.GetComponent<Renderer>().material.color = new Color(r, g, b, 0.4f);
            }
        }


        internal static void Start(List<List<float>> keyPoints, List<GameObject> Nodes, List<GameObject> Edges, Text aIText)
        {
            NodesC = Nodes;
            string recomendationText = "";
            int marks = 0;
            int expertMarks = 30;
            int intermediateMarks = 20;
            int beginnerMarks = 10;
            bool flag1 = false;
            bool flag2 = false;
            bool flag3 = false;
            Tuple<int, int, int> expertColour = new Tuple<int, int, int>(0, 255, 0);
            Tuple<int, int, int> intermediateColour = new Tuple<int, int, int>(255, 255, 0);
            Tuple<int, int, int> beginnerColour = new Tuple<int, int, int>(255, 255, 0);
            Tuple<int, int, int> failureColour = new Tuple<int, int, int>(255, 0, 0);
            
            var rightShoulderArmAngle = angleBetween(keyPoints[11], keyPoints[12], keyPoints[14]);
            var leftShoulderArmAngle = angleBetween(keyPoints[12], keyPoints[11], keyPoints[13]);
            var rightElbowAngle = angleBetween(keyPoints[12], keyPoints[14], keyPoints[16]);
            var leftElbowAngle = angleBetween(keyPoints[11], keyPoints[13], keyPoints[15]);
            Console.WriteLine(rightElbowAngle + "-" + leftShoulderArmAngle);

            var distFingersRight = distance(keyPoints[15], keyPoints[16]);


            var minDistFingers = distance(keyPoints[16], keyPoints[18]) + distance(keyPoints[15], keyPoints[17]);
            if (rightShoulderArmAngle < 93 && rightShoulderArmAngle>87 &&
                leftShoulderArmAngle < 92 && rightShoulderArmAngle>87 && 
                rightElbowAngle > 157 && rightElbowAngle<163 && 
                leftElbowAngle > 157 && leftElbowAngle<163 &&
                keyPoints[16][2] > keyPoints[5][2] && keyPoints[17][2] > keyPoints[2][2])
            {
                //Expert
                if (distFingersRight >= minDistFingers && distFingersRight < 2*minDistFingers && keyPoints[20][2]>=keyPoints[16][2] && keyPoints[17][2] >= keyPoints[15][2])
                {
                    marks += expertMarks;
                    flag1 = true;
                    Console.WriteLine("Hands interlocked above head.");
                    recomendationText = "Hands interlocked above head.\n";
                    nodeColour(new int[] {11,12,13,14,15,16,17,18,19,20,21,22 }, expertColour.Item1, expertColour.Item2, expertColour.Item3);

                }
                else
                {
                    marks += expertMarks-5;
                    Console.WriteLine("InterLock your hand above your head.");
                    recomendationText = "InterLock your hand above your head.\n";
                    nodeColour(new int[] { 15, 16, 17, 18, 19, 20, 21, 22 }, failureColour.Item1, failureColour.Item2, failureColour.Item3);
                    nodeColour(new int[] { 11, 12, 13, 14 }, expertColour.Item1, expertColour.Item2, expertColour.Item3);
                }
            }
            else if (rightShoulderArmAngle < 96 && rightShoulderArmAngle > 84 &&
                leftShoulderArmAngle < 96 && rightShoulderArmAngle > 84 &&
                rightElbowAngle > 154 && rightElbowAngle < 166 &&
                leftElbowAngle > 154 && leftElbowAngle < 166 &&
                keyPoints[16][2] > keyPoints[5][2] && keyPoints[17][2] > keyPoints[2][2])
            {
                //Intermediate
                if (distFingersRight >= minDistFingers && distFingersRight < 2* minDistFingers && keyPoints[20][2] >= keyPoints[16][2] && keyPoints[17][2] >= keyPoints[15][2])
                {
                    marks += intermediateMarks;
                    flag1 = true;
                    Console.WriteLine("Hands interlocked above head.");
                    recomendationText = "Hands interlocked above head.\n";
                    nodeColour(new int[] { 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22 }, intermediateColour.Item1, intermediateColour.Item2, intermediateColour.Item3);
                }
                else
                {
                    marks += intermediateMarks-5;
                    Console.WriteLine("InterLock your hand above your head.");
                    recomendationText = "InterLock your hand above your head.\n";
                    nodeColour(new int[] {15, 16, 17, 18, 19, 20, 21, 22 }, failureColour.Item1, failureColour.Item2, failureColour.Item3);
                    nodeColour(new int[] { 11, 12, 13, 14 }, intermediateColour.Item1, intermediateColour.Item2, intermediateColour.Item3);

                }
            }
            else if (rightShoulderArmAngle < 100 && rightShoulderArmAngle > 80 &&
                leftShoulderArmAngle < 100 && rightShoulderArmAngle > 80 &&
                rightElbowAngle > 150 && rightElbowAngle < 170 &&
                leftElbowAngle > 150 && leftElbowAngle < 170 &&
                keyPoints[16][2] > keyPoints[5][2] && keyPoints[17][2] > keyPoints[2][2])
            {
                //Beginner
                if (distFingersRight >= minDistFingers && distFingersRight < 2*minDistFingers && keyPoints[20][2] >= keyPoints[16][2] && keyPoints[17][2] >= keyPoints[15][2])
                {
                    marks += beginnerMarks;
                    flag1 = true;
                    Console.WriteLine("Hands interlocked above head.");
                    recomendationText = "Hands interlocked above head.\n";
                    nodeColour(new int[] { 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22 }, beginnerColour.Item1, beginnerColour.Item2, beginnerColour.Item3);
                }
                else
                {
                    marks += beginnerMarks - 5;
                    Console.WriteLine("InterLock your hand above your head.");
                    recomendationText = "InterLock your hand above your head.\n";
                    nodeColour(new int[] { 15,16, 17, 18, 19, 20, 21, 22 }, failureColour.Item1, failureColour.Item2, failureColour.Item3);
                    nodeColour(new int[] { 11,12,13,14 }, beginnerColour.Item1, beginnerColour.Item2, beginnerColour.Item3);
                }
            }
            else
            {
                Console.WriteLine("bring Your hands above your head and interlock");
                recomendationText = "bring Your hands above your head and interlock\n";
                nodeColour(new int[] { 11,12,13,14,15,16, 17, 18, 19, 20, 21, 22 }, failureColour.Item1, failureColour.Item2, failureColour.Item3);


            }
            var rightAbdomenLegAngle = angleBetween(keyPoints[23], keyPoints[24], keyPoints[26]);
            var rightKneeAngle = angleBetween(keyPoints[24], keyPoints[26], keyPoints[28]);
            var leftAbdomenLegAngle = angleBetween(keyPoints[24], keyPoints[23], keyPoints[25]);
            var leftKneeAngle = angleBetween(keyPoints[23], keyPoints[25], keyPoints[27]);

            if (rightAbdomenLegAngle > 87 && rightAbdomenLegAngle < 93 && 
                rightKneeAngle > 177)
            {
                //Expert
                marks += expertMarks;
                flag2 = true;
                Console.WriteLine("Right leg is straight");
                recomendationText += "Right leg is straight\n";
                nodeColour(new int[] { 24,26,28 }, expertColour.Item1, expertColour.Item2, expertColour.Item3);


            }
            else if (rightAbdomenLegAngle > 84 && rightAbdomenLegAngle < 96 &&
                rightKneeAngle > 174)
            {

                //Intermediate
                flag2 = true;
                marks += intermediateMarks;
                Console.WriteLine("Right leg is straight");
                recomendationText += "Right leg is straight\n";
                nodeColour(new int[] { 24, 26, 28 }, intermediateColour.Item1, intermediateColour.Item2, intermediateColour.Item3);

            }
            else if (rightAbdomenLegAngle > 80 && rightAbdomenLegAngle < 100 &&
                rightKneeAngle > 170)
            {
                //Beginner
                flag2 = true;
                marks += beginnerMarks;
                Console.WriteLine("Right leg is straight");
                recomendationText += "Right leg is straight\n";
                nodeColour(new int[] { 24, 26, 28 }, beginnerColour.Item1, beginnerColour.Item2, beginnerColour.Item3);

            }
            else
            {
                Console.WriteLine("Right leg is not straight");
                recomendationText += "Right leg is not straight\n";
                nodeColour(new int[] { 24, 26, 28 }, failureColour.Item1, failureColour.Item2, failureColour.Item3);
            }


            if (leftAbdomenLegAngle < 93 && leftAbdomenLegAngle>87 && 
                leftKneeAngle > 177)
            {
                //Expert
                marks += expertMarks;
                flag3 = true;
                Console.WriteLine("Left leg is straight");
                recomendationText += "Left leg is straight\n";
                nodeColour(new int[] { 23,25,27 }, expertColour.Item1, expertColour.Item2, expertColour.Item3);
            }
            else if (leftAbdomenLegAngle < 96 && leftAbdomenLegAngle > 84 &&
                leftKneeAngle > 174)
            {
                //Intermediate
                marks += intermediateMarks;
                flag3 = true;
                Console.WriteLine("Left leg is straight");
                recomendationText += "Left leg is straight\n";
                nodeColour(new int[] { 23, 25, 27 }, intermediateColour.Item1, intermediateColour.Item2, intermediateColour.Item3);
            }
            else if (leftAbdomenLegAngle < 100 && leftAbdomenLegAngle > 80 &&
                leftKneeAngle > 170)
            {
                //Beginner
                marks += beginnerMarks;
                flag3 = true;
                Console.WriteLine("Left leg is straight");
                recomendationText += "Left leg is straight\n";
                nodeColour(new int[] { 23, 25, 27 }, beginnerColour.Item1, beginnerColour.Item2, beginnerColour.Item3);
            }
            else
            {
                Console.WriteLine("Left leg is not straight");
                recomendationText += "Left leg is not straight\n";
                nodeColour(new int[] { 23, 25, 27 }, failureColour.Item1, failureColour.Item2, failureColour.Item3);
            }

            Debug.Log(recomendationText);
            aIText.text = recomendationText;

            var rightToeHeelAngel = angleBetween(keyPoints[28], keyPoints[32], keyPoints[30]);
            var leftToeHeelAngle = angleBetween(keyPoints[27], keyPoints[31], keyPoints[29]);
            if (rightToeHeelAngel < 13 && leftToeHeelAngle < 13)
            {
                Console.WriteLine("Feets are in position.");
            }
            else
            {
                Console.WriteLine("Lift up your heel up.");
            }

            if (marks == 90)
            {
                marks = 100;
            }



            Debug.Log(" "+flag1 +flag2 + flag3);
            Debug.Log("--------------------------------------------------------------------");
            if (flag1 == true && flag2 == true && flag3 == true)
            {
                if (staticFlag4 == false)

                {
                    staticFlag4 = true;
                    GameObject.FindGameObjectWithTag("timeText").GetComponent<AudioSource>().Play();
                    timerIsRunning = true;
                    timeText = GameObject.FindGameObjectWithTag("timeText").GetComponent<Text>();

                    Debug.Log("Rec------------------------------------------------------------Time=============================");
                }
            }
            if (timerIsRunning && flag1 == true && flag2 == true && flag3 == true)
            {
                timeText = GameObject.FindGameObjectWithTag("timeText").GetComponent<Text>();
                Debug.Log("Rec-----------------update---------------------TIm,e=============================");
                if (timeRemaining > 0 && flag1 == true && flag2 == true && flag3 == true)
                {
                    timeRemaining -= Time.deltaTime;
                    timeText.text = timeRemaining.ToString();

                    averageMarks = (averageMarks + marks) / 2;
                }
                //else if (0< timeRemaining && timeRemaining  < 0.2)
                //{

                //}
                else
                {
                    Debug.Log("Time has run out!");
                    timeRemaining = 0;
                    GameObject.FindGameObjectWithTag("AIText").GetComponent<AudioSource>().Play();
                    timerIsRunning = false;
                }
            }
        }
    }
}
