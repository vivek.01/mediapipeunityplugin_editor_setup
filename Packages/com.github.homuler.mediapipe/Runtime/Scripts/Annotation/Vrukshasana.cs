using System;
using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
namespace Mediapipe
{
    class Vrukshasana : MonoBehaviour
    {
        public static Text aIText;
        public static float timeRemaining = 10;
        public static bool timerIsRunning = false;
        public static Text timeText;
        public static bool staticFlag4 = false;
        private void Start()
        {
           
        }
        void Update()
        {
            
        }
        //private void Awake()
        //{
        //   aIText.text = GameObject.FindGameObjectWithTag("AIText").GetComponent<Text>().text;
        //}
        //public static List<GameObject> refNodeGameObjects;

        static double distance(List<float> p1, List<float> p2)
        {
            return Math.Sqrt(Math.Pow(p1[1] - p2[1], 2) + Math.Pow(p1[2] - p2[2], 2));
        }
        static double angleBetween(List<float> p1, List<float> o, List<float> p2)
        {
            /*
            var a = np.array(new int[] { p1[1], p1[2] });
            var b = np.array(new int[] { o[1], o[2] });
            var c = np.array(new int[] { p2[1], p2[2] });

 

            var ba = a - b;
            var bc = c - b;
            var cosine_angle = np.dot(ba, bc) / (np.linalg.norm(ba) * np.linalg.norm(bc));
            var angle = np.arccos(cosine_angle);
            return ((double)np.degrees(angle));
            */
            //Debug.Log("p1-- "+p1[0]+" -- "+p1[1]+" -- "+p1[2]);
            //Debug.Log("o-- " + o[0] + " -- " + o[1] + " -- " + o[2]);
            //Debug.Log("p2-- " + p2[0] + " -- " + p2[1] + " -- " + p2[2]);



            var p1Vector = new UnityEngine.Vector3(p1[1], p1[2], 0.0f);
            var p2Vector = new UnityEngine.Vector3(p2[1], p2[2], 0.0f);
            var oVector = new UnityEngine.Vector3(o[1], o[2], 0.0f);



            UnityEngine.Vector3 edgeba = oVector - p1Vector;
            UnityEngine.Vector3 edgebc = oVector - p2Vector;
            float angle = UnityEngine.Vector3.Angle(edgebc, edgeba);



            return angle;
        }




        internal static void Start(List<List<float>> keyPoints, List<GameObject> Nodes, List<GameObject> Edges, Text aIText)
        {
            //aIText.text = GameObject.FindGameObjectWithTag("AIText").GetComponent<Text>().text;
            string recomendationText= "";
            bool flag1 = false;
            bool flag2 = false;
            bool flag3 = false;
            
            var rightShoulderArmAngle = angleBetween(keyPoints[11], keyPoints[12], keyPoints[14]);
            var leftShoulderArmAngle = angleBetween(keyPoints[12], keyPoints[11], keyPoints[13]);
            var rightElbowAngle = angleBetween(keyPoints[12], keyPoints[14], keyPoints[16]);
            var leftElbowAngle = angleBetween(keyPoints[11], keyPoints[13], keyPoints[15]);
            //Debug.Log("Distance =--- " + distance(keyPoints[21], keyPoints[22]));
            if (keyPoints[16][2] > keyPoints[5][2] && keyPoints[17][2] > keyPoints[2][2] && (distance(keyPoints[21], keyPoints[22])) < 25)
            {
                if (rightShoulderArmAngle < 100 && leftShoulderArmAngle < 100 && rightElbowAngle > 150 && leftElbowAngle > 150)
                {
                    flag1 = true;
                    Debug.Log("Hands doing namaskara, and arms streached.");
                    recomendationText = "Hands doing namaskara, and arms streached.\n";
                    //aIText.text = "Hands doing namaskara, and arms streached.\n";
                    //These are for 11,12,13,14,15,16 i.e shoulder,wrists and  elbow as blue
                    Nodes[11].GetComponent<Renderer>().material.color = new Color(0, 0, 225, 225);
                    Nodes[12].GetComponent<Renderer>().material.color = new Color(0, 0, 225, 225);
                    Nodes[13].GetComponent<Renderer>().material.color = new Color(0, 0, 225, 225);
                    Nodes[14].GetComponent<Renderer>().material.color = new Color(0, 0, 225, 225);
                    Nodes[15].GetComponent<Renderer>().material.color = new Color(0, 0, 225, 225);
                    Nodes[16].GetComponent<Renderer>().material.color = new Color(0, 0, 225, 225);
                    //Nodes[11].transform.GetChild(0).GetComponent<ParticleSystem>().Play();
                    //Nodes[12].transform.GetChild(0).GetComponent<ParticleSystem>().Play();
                    //Nodes[13].transform.GetChild(0).GetComponent<ParticleSystem>().Play();
                    //Nodes[14].transform.GetChild(0).GetComponent<ParticleSystem>().Play();
                    //Nodes[15].transform.GetChild(0).GetComponent<ParticleSystem>().Play();
                    //Nodes[16].transform.GetChild(0).GetComponent<ParticleSystem>().Play();
                    //Edges[0].GetComponent<LineRenderer>().material.color = new Color(0, 0, 225, 225);
                    //Edges[1].GetComponent<LineRenderer>().material.color = new Color(0, 0, 225, 225);
                    //Edges[2].GetComponent<LineRenderer>().material.color = new Color(0, 0, 225, 225);
                    //Edges[3].GetComponent<LineRenderer>().material.color = new Color(0, 0, 225, 225);

                }
                else
                {
                    Debug.Log("Streach armsand keep  ur arms straight");
                    recomendationText = "Streach armsand keep  ur arms straight.\n";
                    //aIText.text = "Streach armsand keep  ur arms straight\n";
                    //These are for 11,12,13,14 i.e shoulder and elbow as red
                    //These are for 15,16 i.e wrists blue
                    Nodes[11].GetComponent<Renderer>().material.color = new Color(225, 0, 0, 225);
                    Nodes[12].GetComponent<Renderer>().material.color = new Color(225, 0, 0, 225);
                    Nodes[13].GetComponent<Renderer>().material.color = new Color(225, 0, 0, 225);
                    Nodes[14].GetComponent<Renderer>().material.color = new Color(225, 0, 0, 225);
                    Nodes[15].GetComponent<Renderer>().material.color = new Color(0, 0, 225, 225);
                    Nodes[16].GetComponent<Renderer>().material.color = new Color(0, 0, 225, 225);
                    Nodes[11].transform.GetChild(0).GetComponent<ParticleSystem>().Stop();
                    Nodes[12].transform.GetChild(0).GetComponent<ParticleSystem>().Stop();
                    Nodes[13].transform.GetChild(0).GetComponent<ParticleSystem>().Stop();
                    Nodes[14].transform.GetChild(0).GetComponent<ParticleSystem>().Stop();
                    //Nodes[15].transform.GetChild(0).GetComponent<ParticleSystem>().Play();
                    //Nodes[16].transform.GetChild(0).GetComponent<ParticleSystem>().Play();
                    //Edges[0].GetComponent<LineRenderer>().material.color = new Color(255, 0, 0, 225);
                    //Edges[1].GetComponent<LineRenderer>().material.color = new Color(255, 0, 0, 225);
                    //Edges[2].GetComponent<LineRenderer>().material.color = new Color(255, 0, 0, 225);
                    //Edges[3].GetComponent<LineRenderer>().material.color = new Color(255, 0, 0, 225);

                }
            }
            else
            {
                Debug.Log("Join your hands; above your head");
                recomendationText = "Join your hands; above your head.\n";
                //aIText.text = "Join your hands; above your head\n";
                //These are for 11,12,13,14,15,16 i.e shoulder,wrists and  elbow as red
                Nodes[11].GetComponent<Renderer>().material.color = new Color(225, 0, 0, 225);
                Nodes[12].GetComponent<Renderer>().material.color = new Color(225, 0, 0, 225);
                Nodes[13].GetComponent<Renderer>().material.color = new Color(225, 0, 0, 225);
                Nodes[14].GetComponent<Renderer>().material.color = new Color(225, 0, 0, 225);
                Nodes[15].GetComponent<Renderer>().material.color = new Color(225, 0, 0, 225);
                Nodes[16].GetComponent<Renderer>().material.color = new Color(225, 0, 0, 225);
                Nodes[11].transform.GetChild(0).GetComponent<ParticleSystem>().Stop();
                Nodes[12].transform.GetChild(0).GetComponent<ParticleSystem>().Stop();
                Nodes[13].transform.GetChild(0).GetComponent<ParticleSystem>().Stop();
                Nodes[14].transform.GetChild(0).GetComponent<ParticleSystem>().Stop();
                Nodes[15].transform.GetChild(0).GetComponent<ParticleSystem>().Stop();
                Nodes[16].transform.GetChild(0).GetComponent<ParticleSystem>().Stop();
                //Edges[0].GetComponent<LineRenderer>().material.color = new Color(255, 0, 0, 225);
                //Edges[1].GetComponent<LineRenderer>().material.color = new Color(255, 0, 0, 225);
                //Edges[2].GetComponent<LineRenderer>().material.color = new Color(255, 0, 0, 225);
                //Edges[3].GetComponent<LineRenderer>().material.color = new Color(255, 0, 0, 225);
            }


            var rightAbdomenLegAngle = angleBetween(keyPoints[23], keyPoints[24], keyPoints[26]);
            var rightKneeAngle = angleBetween(keyPoints[24], keyPoints[26], keyPoints[28]);
            var leftAbdomenLegAngle = angleBetween(keyPoints[24], keyPoints[23], keyPoints[25]);
            var leftKneeAngle = angleBetween(keyPoints[23], keyPoints[25], keyPoints[27]);



            // && keyPoints[30][1] < keyPoints[23][1] && keyPoints[30][1] > keyPoints[8][1]
            if (rightAbdomenLegAngle > 130 && rightKneeAngle < 50 && keyPoints[28][2] >= keyPoints[25][2] )
            {
                flag2 = true;
                Debug.Log("Right leg in position");
                recomendationText = recomendationText+ "Right leg in position.\n";
                //aIText.text = aIText + "Right leg in position\n";
                //These are for 24,26 i.e right hip and right knee as blue
                Nodes[24].GetComponent<Renderer>().material.color = new Color(0, 0, 225, 225);
                Nodes[26].GetComponent<Renderer>().material.color = new Color(0, 0, 225, 225);
                Nodes[28].GetComponent<Renderer>().material.color = new Color(0, 0, 225, 225);
                Nodes[30].GetComponent<Renderer>().material.color = new Color(0, 0, 225, 225);
                Nodes[32].GetComponent<Renderer>().material.color = new Color(0, 0, 225, 225);
                //Nodes[24].transform.GetChild(0).GetComponent<ParticleSystem>().Play();
                //Nodes[26].transform.GetChild(0).GetComponent<ParticleSystem>().Play();
                //Nodes[28].transform.GetChild(0).GetComponent<ParticleSystem>().Play();
            //    Edges[13].GetComponent<LineRenderer>().material.color = new Color(0, 0, 225, 225);
            //    Edges[14].GetComponent<LineRenderer>().material.color = new Color(0, 0, 225, 225);
            //    Edges[15].GetComponent<LineRenderer>().material.color = new Color(0, 0, 225, 225);
            //    Edges[16].GetComponent<LineRenderer>().material.color = new Color(0, 0, 225, 225);
            //    Edges[17].GetComponent<LineRenderer>().material.color = new Color(0, 0, 225, 225);
            }
            else if (rightKneeAngle >= 40&& keyPoints[28][2] <= keyPoints[25][2]&&rightKneeAngle <110 )
            {
                // &&
                Debug.Log("Pull your toes upwards and place on left le tigh");
                recomendationText = recomendationText+"Pull your toes upwards and place on left le tigh.\n";
                //aIText.text = aIText + "Pull your toes upwards and place on left le tigh\n";
                // 24 i.e right hip is blue
                // 26 i.e right knee is red
                Nodes[24].GetComponent<Renderer>().material.color = new Color(0, 0, 255, 225);
                Nodes[26].GetComponent<Renderer>().material.color = new Color(225, 0, 0, 225);
                Nodes[28].GetComponent<Renderer>().material.color = new Color(225, 0, 0, 225);
                Nodes[30].GetComponent<Renderer>().material.color = new Color(225, 0, 0, 225);
                Nodes[32].GetComponent<Renderer>().material.color = new Color(225, 0, 0, 225);
                //Nodes[24].transform.GetChild(0).GetComponent<ParticleSystem>().Play();
                Nodes[26].transform.GetChild(0).GetComponent<ParticleSystem>().Stop();
                Nodes[28].transform.GetChild(0).GetComponent<ParticleSystem>().Stop();
                //Edges[13].GetComponent<LineRenderer>().material.color = new Color(0, 0, 255, 225);
                //Edges[14].GetComponent<LineRenderer>().material.color = new Color(255, 0, 0, 225);
                //Edges[15].GetComponent<LineRenderer>().material.color = new Color(255, 0, 0, 225);
                //Edges[16].GetComponent<LineRenderer>().material.color = new Color(255, 0, 0, 225);
                //Edges[17].GetComponent<LineRenderer>().material.color = new Color(255, 0, 0, 225);
            }
            else
            {

                Debug.Log("Place your right leg on left leg tigh");
                recomendationText = recomendationText+ "Place your right leg on left leg tigh.\n";
                //aIText.text = aIText + "Place your right leg on left leg tigh\n";
                //These are for 24,26  i.e right hip and right knee as red
                Nodes[24].GetComponent<Renderer>().material.color = new Color(225, 0, 0, 225);
                Nodes[26].GetComponent<Renderer>().material.color = new Color(225, 0, 0, 225);
                Nodes[28].GetComponent<Renderer>().material.color = new Color(225, 0, 0, 225);
                Nodes[30].GetComponent<Renderer>().material.color = new Color(225, 0, 0, 225);
                Nodes[32].GetComponent<Renderer>().material.color = new Color(225, 0, 0, 225);
                Nodes[24].transform.GetChild(0).GetComponent<ParticleSystem>().Stop();
                Nodes[26].transform.GetChild(0).GetComponent<ParticleSystem>().Stop();
                Nodes[28].transform.GetChild(0).GetComponent<ParticleSystem>().Stop();
                //Edges[13].GetComponent<LineRenderer>().material.color = new Color(255, 0, 0, 225);
                //Edges[14].GetComponent<LineRenderer>().material.color = new Color(255, 0, 0, 225);
                //Edges[15].GetComponent<LineRenderer>().material.color = new Color(255, 0, 0, 225);
                //Edges[16].GetComponent<LineRenderer>().material.color = new Color(255, 0, 0, 225);
                //Edges[17].GetComponent<LineRenderer>().material.color = new Color(255, 0, 0, 225);
            }
            if (leftAbdomenLegAngle > 80 && leftAbdomenLegAngle < 100 && leftKneeAngle > 170)
            {
                flag3 = true;
                Debug.Log("Left leg is straight");
                recomendationText = recomendationText+ "Left leg is straight.\n";
                //aIText.text = aIText + "Left leg is straight\n";
                //These are for 23,25 left and left knee as blue
                Nodes[23].GetComponent<Renderer>().material.color = new Color(0, 0, 225, 225);
                Nodes[25].GetComponent<Renderer>().material.color = new Color(0, 0, 225, 225);
                Nodes[27].GetComponent<Renderer>().material.color = new Color(0, 0, 225, 225);
                Nodes[29].GetComponent<Renderer>().material.color = new Color(0, 0, 225, 225);
                Nodes[31].GetComponent<Renderer>().material.color = new Color(0, 0, 225, 225);
                //Nodes[23].transform.GetChild(0).GetComponent<ParticleSystem>().Play();
                //Nodes[25].transform.GetChild(0).GetComponent<ParticleSystem>().Play();
                //Nodes[27].transform.GetChild(0).GetComponent<ParticleSystem>().Play();
                //Edges[8].GetComponent<LineRenderer>().material.color = new Color(0, 0, 225, 225);
                //Edges[9].GetComponent<LineRenderer>().material.color = new Color(0, 0, 225, 225);
                //Edges[0].GetComponent<LineRenderer>().material.color = new Color(0, 0, 225, 225);
                //Edges[11].GetComponent<LineRenderer>().material.color = new Color(0, 0, 225, 225);
                //Edges[12].GetComponent<LineRenderer>().material.color = new Color(0, 0, 225, 225);
            }
            else
            {

                Debug.Log("Left leg is not straight");
                recomendationText = recomendationText+ "Left leg is not straight.\n";
                //aIText.text = aIText + "Left leg is not straight\n";
                //These are for 23,25 left and left knee as red
                Nodes[23].GetComponent<Renderer>().material.color = new Color(225, 0, 0, 225);
                Nodes[25].GetComponent<Renderer>().material.color = new Color(225, 0, 0, 225);
                Nodes[27].GetComponent<Renderer>().material.color = new Color(225, 0, 0, 225);
                Nodes[29].GetComponent<Renderer>().material.color = new Color(225, 0, 0, 225);
                Nodes[31].GetComponent<Renderer>().material.color = new Color(225, 0, 0, 225);
                Nodes[23].transform.GetChild(0).GetComponent<ParticleSystem>().Stop();
                Nodes[25].transform.GetChild(0).GetComponent<ParticleSystem>().Stop();
                Nodes[27].transform.GetChild(0).GetComponent<ParticleSystem>().Stop();
                //Edges[8].GetComponent<LineRenderer>().material.color = new Color(255, 0, 0, 225);
                //Edges[9].GetComponent<LineRenderer>().material.color = new Color(255, 0, 0, 225);
                //Edges[10].GetComponent<LineRenderer>().material.color = new Color(255, 0, 0, 225);
                //Edges[11].GetComponent<LineRenderer>().material.color = new Color(255, 0, 0, 225);
                //Edges[12].GetComponent<LineRenderer>().material.color = new Color(255, 0, 0, 225);

            }
            Debug.Log("Rec=============================");
            Debug.Log(recomendationText);
            aIText.text = recomendationText;

            Debug.Log("--------------------------------------------------------------------");
            if(flag1 == true && flag2 == true && flag3 == true)
            {
                if (staticFlag4 == false)

                {
                    staticFlag4 = true;
                    GameObject.FindGameObjectWithTag("timeText").GetComponent<AudioSource>().Play();
                    timerIsRunning = true;
                    timeText = GameObject.FindGameObjectWithTag("timeText").GetComponent<Text>();
                    Debug.Log("Rec------------------------------------------------------------TIm,e=============================");
                    Debug.Log("Rec------------------------------------------------------------TIm,e=============================");
                }
            }
            Debug.Log("timer is running " + timerIsRunning);
            if (timerIsRunning)
            {
                 timeText = GameObject.FindGameObjectWithTag("timeText").GetComponent<Text>();
                Debug.Log("Rec-----------------update---------------------TIm,e=============================");
                if (timeRemaining > 0)
                {
                    timeRemaining -= Time.deltaTime;
                    timeText.text = timeRemaining.ToString();
                }
                //else if (0< timeRemaining && timeRemaining  < 0.2)
                //{
                   
                //}
                else
                {
                    Debug.Log("Time has run out!");
                    timeRemaining = 0;
                    GameObject.FindGameObjectWithTag("AIText").GetComponent<AudioSource>().Play();
                    timerIsRunning = false;
                }
            }

        }

       

    }
}
